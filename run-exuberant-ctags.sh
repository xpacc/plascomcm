#! /bin/bash

# What is this?
# -------------
# https://en.wikipedia.org/wiki/Ctags

# It works with this particular implementation:
# http://ctags.sourceforge.net/ 

ctags \
  --langmap=fortran:.fpp,c:.c.h \
  --recurse \
  .
