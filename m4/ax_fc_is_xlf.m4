# Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
# License: MIT, http://opensource.org/licenses/MIT
#
dnl
AC_DEFUN([AX_FC_IS_XLF],[
AC_MSG_CHECKING([whether we are using the XLF Fortran compiler])
# AC_LANG_PUSH([Fortran])
# AC_COMPILE_IFELSE([AC_LANG_PROGRAM([],[
# !GCC$ ATTRIBUTES foobar
# ])],[ax_fc_compiler_gnu=no],[ax_fc_compiler_gnu=yes])
# AC_LANG_POP([Fortran])

#AC_LANG([Fortran])
# Run test in a separate directory to ensure that the ls check for
# the module extension won't find another file that matches the check
#if test ! -d conftest.dir ; then mkdir conftest.dir ; fi
#cd conftest.dir
#ax_FCFLAGS_save=$FCFLAGS
#for ax_flag in '--version' '-qversion' ; do
#  FCFLAGS="$ax_FCFLAGS_save -c $ax_flag"
#  AC_COMPILE_IFELSE([
#    program main
#    write (*,'(A)') 'Hello, World!'
#    end program],[AC_MSG_RESULT([$FCFLAGS true])],[AC_MSG_RESULT([$FCFLAGS false])])
# done

# FCFLAGS="$FCFLAGS -c"
# AC_MSG_RESULT([FCFLAGS = $FCFLAGS])
# FCFLAGS=$ax_FCFLAGS_save
# cd ..
# rm -rf conftest.dir

ax_xlf_output=`$FC -qversion 2>>config.log`
ax_xlf_test=`echo $ax_xlf_output | grep IBM | wc -l`
if test $ax_xlf_test -ge 1; then
  ax_fc_compiler_xlf=yes
else
  ax_fc_compiler_xlf=no
fi
AC_MSG_RESULT([$ax_fc_compiler_xlf])
])
