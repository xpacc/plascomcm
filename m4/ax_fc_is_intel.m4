# Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
# License: MIT, http://opensource.org/licenses/MIT
#
dnl
dnl The test provided by Autoconf for whether the Fortran compiler is
dnl the GNU Fortran compiler is incorrect.  That test simply looks at
dnl whether a test for a C preprocessor macro causes code to be included
dnl that will cause the compiler to fail.  Unfortunately, the code that
dnl is tested is invalid Fortran, so almost any compiler will cause this
dnl test to report that the compiler is a GNU Fortran compiler.
AC_DEFUN([AX_FC_IS_INTEL],[
AC_MSG_CHECKING([whether we are using the Intel Fortran compiler])
# AC_LANG_PUSH([Fortran])
# AC_COMPILE_IFELSE([AC_LANG_PROGRAM([],[
# !GCC$ ATTRIBUTES foobar
# ])],[ax_fc_compiler_gnu=no],[ax_fc_compiler_gnu=yes])
# AC_LANG_POP([Fortran])
ax_intel_output=`$FC --version 2>>config.log`
ax_intel_test=`echo $ax_intel_output | grep Intel | wc -l`
if test $ax_intel_test -ge 1; then
  ax_fc_compiler_intel=yes
else
  ax_fc_compiler_intel=no
fi
AC_MSG_RESULT([$ax_fc_compiler_intel])
])
