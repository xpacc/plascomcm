# Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
# License: MIT, http://opensource.org/licenses/MIT
#
AC_DEFUN([AX_PROG_FPP],[
   AC_ARG_VAR([FPP],[Fortran preprocessor])
   AC_MSG_CHECKING([for Fortran preprocessor])
   ax_found_fpp=no
   # The exact form of this test is important - the Intel Fortran compiler
   # issues odd output unless the free-form option is included in this case
   cat > conftest.fpp <<_ACEOF
#define MAX_VAL 3
module myparms
    integer (n=MAX_VAL)
    Syntax error
end module myparms
_ACEOF
   for FPP in "$FC -E -P" "$FC -E" "$FC -E --traditional-cpp" "$FC -E -P $ac_cv_fc_freeform" "fpp" ; do
      rm -f conftest.err conftest.i conftest.txt conftest.s
      # Note that we might not have an fpp in the path
      if $FPP conftest.fpp > conftest.txt 2>/dev/null ; then
          # Check output
	  if grep 'integer (n=3)' conftest.txt >/dev/null 2>&1 ; then
	      ax_found_fpp=yes
	      break;
	  fi
      else
          :
      fi
   done
   if test "$ax_found_fpp" = "yes" ; then
       AC_MSG_RESULT($FPP)
   else
       AC_MSG_RESULT([none found])
       unset FPP
   fi
])
