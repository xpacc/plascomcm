# Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
# License: MIT, http://opensource.org/licenses/MIT
#
dnl
dnl The test provided by Autoconf for whether the Fortran compiler is
dnl the GNU Fortran compiler is incorrect.  That test simply looks at
dnl whether a test for a C preprocessor macro causes code to be included
dnl that will cause the compiler to fail.  Unfortunately, the code that
dnl is tested is invalid Fortran, so almost any compiler will cause this
dnl test to report that the compiler is a GNU Fortran compiler.
AC_DEFUN([AX_FC_IS_GNU],[
AC_MSG_CHECKING([whether we are using the GNU Fortran compiler, using a correct test])

# Check the output for the GNU string, being aware that the XLF compiler will
# silently ignore any unrecognized flags and pass them to ld.  If XLF call the
# the GNU ld, then GNU appears in the output
ax_gnu_output=`$FC --version 2>>config.log`
ax_test=`echo $ax_gnu_output | grep "GNU ld" | wc -l`
ax_gnu_test=`echo $ax_gnu_output | grep GNU | wc -l`

# Default
ax_fc_compiler_gnu=no

# Perform the test
if test $ax_test -eq 0; then
  if test $ax_gnu_test -ge 1; then
    ax_fc_compiler_gnu=yes
  else
    ax_fc_compiler_gnu=no
  fi
fi
AC_MSG_RESULT([$ax_fc_compiler_gnu])
])
