# Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
# License: MIT, http://opensource.org/licenses/MIT
#
AC_DEFUN([AX_ENABLE_TESTS], [

  AC_ARG_ENABLE([tests], [AS_HELP_STRING([--enable-tests], [Enable building of tests])],
    [], [enable_tests="no"])

  AS_IF(
    [test "$enable_tests" != "yes" && test "$enable_tests" != "no"],
    [AC_MSG_ERROR([Unrecognized value for argument to --enable-tests. Argument must be 'yes' or 'no'.])]
  )

  AM_CONDITIONAL([BUILD_TESTS], [test "$enable_tests" = "yes"])

  TEST_FPPFLAGS=-DBUILD_TESTS
  TEST_FCFLAGS=
  TEST_CFLAGS=-DBUILD_TESTS
  TEST_LIBS=

])
