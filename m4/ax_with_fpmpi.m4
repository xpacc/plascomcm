# Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
# License: MIT, http://opensource.org/licenses/MIT
#
# Add option to support fpmpi
AC_DEFUN([AX_WITH_FPMPI],[
AC_ARG_WITH([fpmpi],
 [AS_HELP_STRING([--with-fpmpi],[Compile with FPMPI support])],,[with_fpmpi=no])
#
if test "X$with_fpmpi" != no ; then
    ax_fpmpilib=-lfpmpi
    if test "X$with_fpmpi" != yes ; then
        if test -d "$with_fpmpi" ; then
	    ax_fpmpilib="-L$with_fpmpi -lfpmpi"
        fi
    fi
    # Check to see if we can compile and link
    AC_MSG_CHECKING([whether can link with fpmpi])
    AC_LANG_PUSH([C])
    saveLibs=$LIBS
    LIBS="$LIBS $ax_fpmpilib"
    AC_LINK_IFELSE([AC_LANG_PROGRAM([
#include "mpi.h"
extern int fpmpi_ProfControl;],
[MPI_Init(0,0);fpmpi_ProfControl=0;])],
    [has_fpmpi=yes],[has_fpmpi=no])
    AC_LANG_POP
    AC_MSG_RESULT([$has_fpmpi])
    if test "X$has_fpmpi" = "Xno" ; then
        LIBS="$saveLibs"
    fi
fi
])
