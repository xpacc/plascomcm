# Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
# License: MIT, http://opensource.org/licenses/MIT
#
#
dnl AX_WITH_TAU([CC FC CXX],[Program features])
dnl
dnl The first argument allows you to select just the compilers that you need
dnl (separated by spaces)
dnl
dnl The second argument allows you to select the TAU features to include.
dnl For example, [papi,mpi,openmp] (separated by spaces)
dnl
AC_DEFUN([AX_WITH_TAU],[
AC_ARG_WITH([tau],
 [AS_HELP_STRING([--with-tau],[Compile with TAU support])],,[with_tau=no])
if test "x$with_tau" != "xno" ; then
  FCFLAGS="$FCFLAGS -DHAVE_TAU"
  ifelse([$1],[],[ax_tau_clist="CC FC CXX"],[ax_tau_clist="$1"])
  # Check that the tau compile scripts are available
  for taucc in $ax_tau_clist ; do
  case $taucc in
  CC)
  AC_CHECK_PROGS([TAU_CC],[tau_cc.sh])
  if test "X$TAU_CC" = "X" ; then
      AC_MSG_ERROR([Unable to find tau_cc.sh])
  fi
  CC=$TAU_CC
  ;;
  CXX)
  AC_CHECK_PROGS([TAU_CXX],[tau_cxx.sh])
  if test "X$TAU_CXX" = "X" ; then
      AC_MSG_ERROR([Unable to find tau_cxx.sh])
  fi
  CXX=$TAU_CXX
  ;;
  FC)
  AC_CHECK_PROGS([TAU_FC],[tau_f90.sh])
  if test "X$TAU_FC" = "X" ; then
      AC_MSG_ERROR([Unable to find tau_90.sh])
  fi
  FC=$TAU_FC
  ;;
  *)
  AC_MSG_ERROR([Unrecognized compiler $taucc for [AX_WITH_TAU]])
  ;;
  esac
  done
# Need to find TAU_MAKEFILE, set somewhere (Makefile.am?)
# TAU options: -optVerbose, -optKeepFiles, -optTauSelectFile
# Commandline to compilers or TAU_OPTIONS environment variable
# If TAU_MAKEFILE is not defined, try the following:
#   Extract the directory from the compiler script
#   See if there is a "minimal" tau Makefile; e.g., Makefile.tau-cray
#   Try to add the requested features.  For now, hope that they are
#   ordered as
#      papi mpi openmp pthread pdt
# Note that pdt might be required.
# For now, this is too hard, so point the user in the right direction.
  if test "X$TAU_MAKEFILE" = "X" ; then
     taulibdir=""
     if test "X$TAU_CC" != "X" ; then
         AC_PATH_PROG(taubindir,$TAU_CC)
     elif test "X$TAU_FC" != "X" ; then
         AC_PATH_PROG(taubindir,$TAU_FC)
     else
         AC_PATH_PROG(taubindir,$TAU_CXX)
     fi
     if test "X$taubindir" != "X" ; then
         taulibdir=`echo $taubindir | sed -e 's%/bin/tau.*\.sh%/lib%'`
         if test "X$taulibdir" != "X" ; then
             if test -d $taulibdir ; then
                  ls $taulibdir/Makefile*
             fi
         fi
     fi
     AC_MSG_ERROR([You must set TAU_MAKEFILE.  One of the above files may be appropriate.])
  fi
fi
AM_CONDITIONAL([USE_TAU],[test x$with_tau = xyes])
])
