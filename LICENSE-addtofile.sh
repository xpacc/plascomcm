#!/bin/bash

usage() {
    cat << EOF
    Usage: $0 options fname

    OPTIONS:
        -h Show this usage message
        -C Add the license as a C-style comment ( /* <text> */ )
        -F Add the license as a Fortran-style comment ( ! <text> )
        -S Add the license as a shell-style comment ( # <text> )
        -M Add the license as a Matlab-style comment ( % <text> )
EOF
}


comment=""
commentend=""

while getopts "hCFSM" o; do
    case "${o}" in
        C) # C-style comment
            if [$comment == ""];
            then
                comment="\/\* "
                commentend=" \*\/"
            fi
            ;;
        F) # Fortran-style comment
            if [$comment == ""]
            then
                comment="! "
            fi
            ;;
        S) # Shell-style comment
            if [$comment == ""]
            then
                comment="# "
            fi
            ;;
        M) # Matlab-style comment
            if [$comment == ""]
            then
                comment="% "
            fi
            ;;
        h) # show help
            usage
            ;;
        \?) # error, show help
            usage
            break
            ;;
    esac
done
shift $((OPTIND-1))

echo "type = $comment"
echo $1

#sed -i "" "1s/^/${comment} Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC ${commentend}\'$'\n${comment} License: MIT, http:\/\/opensource.org\/licenses\/MIT ${commentend}/" $1
str1="Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC"
str2="License: MIT, http:\/\/opensource.org\/licenses\/MIT"

fullstr1="$comment$str1$commentend"
fullstr2="$comment$str2$commentend"
lf=$'\n'
sed -i "" "1s/^/$fullstr1\\$lf$fullstr2\\$lf/" $1
