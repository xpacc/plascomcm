#
# Add system level tests
# These are driven by shell scripts
#
# Note:
# In the original autotools build system, all the tests are run by runall...
# Here, I am populating each test into a list and calling run one-by-one, which 
# is exactly what runall does. This gives me finer control over the tests.
#
message("Adding system level tests")
add_subdirectory(utils)

#
# system tests are executed with a shell script
#
# set some variables
set(abs_top_builddir ${CMAKE_BINARY_DIR})
# hacks to convert cmake bools to ints needed by scripts
# this should not propagate back up
set(HAVE_HDF5 "0")
set(AXISYMMETRIC "0")
set(HAVE_CANTERA "0")
set(BUILD_ELECTRIC "0")
set(HAVE_BATCH "0")
if(BatchTestSubmission)
  set(HAVE_BATCH "1")
endif()
if(useHDF)
  set(HAVE_HDF5 "1")
endif()
if(Axisymmetric)
  set(AXISYMMETRIC "1")
endif()
if(Combustion)
  set(HAVE_CANTERA "1")
endif()
if(EF-Solver)
  set(BUILD_ELECTRIC "1")
endif()
#
# how do we submit jobs to the batch queue?
# Here the user can set the variable BatchTestSubmission or
# we can attempt to look for acceptable options and do the obvious thing
#
set(BatchTestSubmission "$ENV{BatchTestSubmission}")
if(BatchTestSubmission STREQUAL "")

  set(batchOptions srun ibrun aprun)
  set(batchSubmit "batchSubmit-NOTFOUND")

  foreach(exec ${batchOptions})
    if(batchSubmit STREQUAL "batchSubmit-NOTFOUND")
      find_program(batchSubmit "${exec}")
    endif()
  endforeach()
#
  if(batchSubmit STREQUAL "batchSubmit-NOTFOUND")
    message("Could not figure out how to submit jobs to a queue, tests will be
             run on the front end with ${MPIEXEC}")
  else()
    message("Using ${batchSubmit} to run tests")
    set(MPIEXEC ${batchSubmit})
  endif()
else()
  set(MPIEXEC ${BatchTestSubmission})
endif()
#
# process input files
#
configure_file(run.in run @ONLY)
configure_file(testdefs.in testdefs @ONLY)
# these may be obsolete now...
configure_file(runall.in runall @ONLY)
#configure_file(autorun.in autorun @ONLY)
#
# add appropriate tests
#
set(systemTests CompareTests InputTests PLOT3DIOTests FluidTests GeometryTests)

if(Axisymmetric)
  list(APPEND systemTests AxisymmetricTests)
endif()
if(useHDF)
  list(APPEND systemTests HDF5IOTests)
  list(APPEND systemTests HDF5UtilsTests)
endif()
if(Combustion)
  list(APPEND systemTests ChemistryTests)
endif()
if(EF-Solver)
  list(APPEND systemTests ElectricTests)
endif()
#
# add a test that creates the links we need
#
add_test(NAME setupTestData COMMAND ${CMAKE_COMMAND} -E create_symlink
         ${CMAKE_CURRENT_SOURCE_DIR}/data ${CMAKE_CURRENT_BINARY_DIR}/data)
#
# find the run shell script
#
find_program(runTests run HINTS ${CMAKE_CURRENT_BINARY_DIR} REQUIRED)
#
# add tests
#
# each test in systemTests is actually a suite of tests, so the first test actually runs 
# the test suite and then subsequent tests check the results of specific tests
# from the suite with the testresults tool.
# This has the convienent side effect of putting the PlasComCM stdout from the test logfile
# into Testing/Temporary/LastTest.log, which makes debugging failing tests easier.
#
if(runTests)
  foreach(testSuite ${systemTests})
    # Add test suite, this runs the test
    message("Adding test suite ${testSuite}")
    add_test(${testSuite} ${runTests} ${CMAKE_CURRENT_SOURCE_DIR}/${testSuite})
    # populate a list of actual tests in the suite
    execute_process(COMMAND grep "^[^#;]" ${CMAKE_CURRENT_SOURCE_DIR}/${testSuite} 
                    COMMAND grep AddTest
                    OUTPUT_VARIABLE allTests)
    string(REPLACE "AddTest" "" allTests ${allTests})
    string(REGEX REPLACE "\n" "" allTests ${allTests})
    separate_arguments(allTests)
    foreach(indTest ${allTests})
      message("\t individual test to be checked is ${indTest}")
      add_test(${testSuite}:${indTest} ${checkTests} ${testSuite}.${indTest} 
               ${CMAKE_CURRENT_BINARY_DIR}/run.log)
      set_tests_properties(${testSuite}:${indTest} PROPERTIES DEPENDS ${testSuite})
    endforeach()
  endforeach()
endif()

#add_test(pftest_alltests ${PROJECT_BINARY_DIR}/pftest_alltests)
