program CheckGeometry

  use ModCompare
  use ModParseArguments
  use ModPLOT3D_IO
  use ModStdIO
  implicit none

  integer, parameter :: rk = selected_real_kind(15, 307)
  integer, parameter :: PATH_LENGTH = 256

  character(len=1), dimension(3), parameter :: XYZNames = ['x', 'y', 'z']
  character(len=32), dimension(4) :: QNames2D
  character(len=32), dimension(5) :: QNames3D

  integer :: i
  character(len=CMD_ARG_LENGTH), dimension(:), allocatable :: RawArguments, Arguments
  character(len=PATH_LENGTH) :: TestName

  real(rk), parameter :: Pi = 3.1415926535897932385_rk

  allocate(RawArguments(command_argument_count()))
  do i = 1, size(RawArguments)
    call get_command_argument(i, RawArguments(i))
  end do

  call ParseArguments(RawArguments, Arguments=Arguments, MinArguments=1, MaxArguments=1)

  QNames2D(1) = "densities"
  QNames2D(2:3) = ["x momenta", "y momenta"]
  QNames2D(4) = "energies"

  QNames3D(1) = "densities"
  QNames3D(2:4) = ["x momenta", "y momenta", "z momenta"]
  QNames3D(5) = "energies"

  TestName = Arguments(1)

  select case (TestName)
  case ('GenerateUniformGrid2D')
    call CheckResults_GenerateUniformGrid2D()
  case ('GenerateUniformGrid3D')
    call CheckResults_GenerateUniformGrid3D()
  case ('GenerateCylindricalGrid2D')
    call CheckResults_GenerateCylindricalGrid2D()
  case ('GenerateCylindricalGrid3D')
    call CheckResults_GenerateCylindricalGrid3D()
  case ('GenerateCylinderUniformCells')
    call CheckResults_GenerateCylinderUniformCells()
  case ('GenerateMultipleGrids')
    call CheckResults_GenerateMultipleGrids()
  case ('RunSimWithGeneratedGrid')
    call CheckResults_RunSimWithGeneratedGrid()
  case ('SetIBlankRange2D')
    call CheckResults_SetIBlankRange2D()
  case ('SetIBlankRange3D')
    call CheckResults_SetIBlankRange3D()
  case ('MoveGrid2D')
    call CheckResults_MoveGrid2D()
  case ('MoveGrid3D')
    call CheckResults_MoveGrid3D()
  case ('RotateGrid2D')
    call CheckResults_RotateGrid2D()
  case ('RotateGrid3D')
    call CheckResults_RotateGrid3D()
  case ('ScaleGrid2D')
    call CheckResults_ScaleGrid2D()
  case ('ScaleGrid3D')
    call CheckResults_ScaleGrid3D()
  case default
    write (ERROR_UNIT, '(3a)') "ERROR: Incorrect test name '", trim(TestName), "'."
    stop 2
  end select

contains

  subroutine CheckResults_GenerateUniformGrid2D()

    integer :: i, j, l
    integer :: nDimsGridFile
    integer :: nDims
    integer :: nGrids
    integer, pointer :: nPoints(:,:)
    character(len=2) :: CoordPrecision
    character(len=2) :: GridFormat
    character(len=2) :: VolumeFormat
    character(len=2) :: UseIBlank
    real(rk), pointer :: XYZ(:,:,:,:,:)
    integer, pointer :: IBlank(:,:,:,:)
    character(len=PATH_LENGTH) :: GridFile
    logical :: Exists
    integer :: N(2)
    real(rk), allocatable :: XYZ_actual(:,:,:), XYZ_expected(:,:,:)
    real(rk) :: U, V
    logical :: Equal
    real(rk) :: Tolerance
    real(rk) :: Difference

    nullify(nPoints)
    nullify(XYZ)

    GridFile = "RocFlo-CM.00000000.xyz"

    inquire(file=GridFile, exist=Exists)
    if (.not. Exists) then
      write (ERROR_UNIT, '(3a)') "ERROR: Could not locate file '", trim(GridFile), "'."
      stop 2
    end if

    call Read_Grid(nDimsGridFile, nGrids, nPoints, XYZ, IBlank, CoordPrecision, GridFormat, VolumeFormat, &
      UseIBlank, GridFile, 0)

    nDims = 2
    N = [20,40]

    if (.not. all(nPoints(1,:nDims) == N)) then
      write (ERROR_UNIT, '(a)') "ERROR: Grid has incorrect size."
      stop 2
    end if

    allocate(XYZ_actual(N(1),N(2),nDims))

    XYZ_actual = XYZ(1,:,:,1,:nDims)

    allocate(XYZ_expected(N(1),N(2),nDims))

    do j = 1, N(2)
      do i = 1, N(1)
        U = real(i-1,kind=rk)/real(N(1)-1,kind=rk)
        V = real(j-1,kind=rk)/real(N(2)-1,kind=rk)
        XYZ_expected(i,j,:) = [1._rk,2._rk] * (1._rk + [U,V])
      end do
    end do

!     XYZ(1,:,:,1,:nDims) = XYZ_expected
!     GridFile = "grid_expected.xyz"
!     call Write_Grid(nDimsGridFile, nGrids, nPoints, XYZ, IBlank, CoordPrecision, GridFormat, &
!       VolumeFormat, UseIBlank, GridFile)
!     stop 2

    do l = 1, nDims
      Equal = AlmostEqual(XYZ_expected(:,:,l), XYZ_actual(:,:,l), CompareNorm=COMPARE_NORM_P, &
        P=2, Tolerance=1.e-12_rk, Difference=Difference)
      if (.not. Equal) then
        write (*, '(3a)') "Expected and actual grids have different ", XYZNames(l), &
          " coordinates."
        write (*, '(a,e17.10)') "Difference: ", Difference
        stop 1
      end if
    end do

    deallocate(XYZ)
    deallocate(IBlank)
    deallocate(nPoints)

  end subroutine CheckResults_GenerateUniformGrid2D

  subroutine CheckResults_GenerateUniformGrid3D()

    integer :: i, j, k, l
    integer :: nDimsGridFile
    integer :: nDims
    integer :: nGrids
    integer, pointer :: nPoints(:,:)
    character(len=2) :: CoordPrecision
    character(len=2) :: GridFormat
    character(len=2) :: VolumeFormat
    character(len=2) :: UseIBlank
    real(rk), pointer :: XYZ(:,:,:,:,:)
    integer, pointer :: IBlank(:,:,:,:)
    character(len=PATH_LENGTH) :: GridFile
    logical :: Exists
    integer :: N(3)
    real(rk), allocatable :: XYZ_actual(:,:,:,:), XYZ_expected(:,:,:,:)
    real(rk) :: U, V, W
    logical :: Equal
    real(rk) :: Tolerance
    real(rk) :: Difference

    nullify(nPoints)
    nullify(XYZ)

    GridFile = "RocFlo-CM.00000000.xyz"

    inquire(file=GridFile, exist=Exists)
    if (.not. Exists) then
      write (ERROR_UNIT, '(3a)') "ERROR: Could not locate file '", trim(GridFile), "'."
      stop 2
    end if

    call Read_Grid(nDimsGridFile, nGrids, nPoints, XYZ, IBlank, CoordPrecision, GridFormat, &
      VolumeFormat, UseIBlank, GridFile, 0)

    nDims = 3
    N = [20,40,60]

    if (.not. all(nPoints(1,:nDims) == N)) then
      write (ERROR_UNIT, '(a)') "ERROR: Grid has incorrect size."
      stop 2
    end if

    allocate(XYZ_actual(N(1),N(2),N(3),nDims))

    XYZ_actual = XYZ(1,:,:,:,:nDims)

    allocate(XYZ_expected(N(1),N(2),N(3),nDims))

    do k = 1, N(3)
      do j = 1, N(2)
        do i = 1, N(1)
          U = real(i-1,kind=rk)/real(N(1)-1,kind=rk)
          V = real(j-1,kind=rk)/real(N(2)-1,kind=rk)
          W = real(k-1,kind=rk)/real(N(3)-1,kind=rk)
          XYZ_expected(i,j,k,:) = [1._rk,2._rk,3._rk] * (1._rk + [U,V,W])
        end do
      end do
    end do

!     XYZ(1,:,:,:,:nDims) = XYZ_expected
!     GridFile = "grid_expected.xyz"
!     call Write_Grid(nDimsGridFile, nGrids, nPoints, XYZ, IBlank, CoordPrecision, GridFormat, &
!       VolumeFormat, UseIBlank, GridFile)
!     stop 2

    do l = 1, nDims
      Equal = AlmostEqual(XYZ_expected(:,:,:,l), XYZ_actual(:,:,:,l), CompareNorm=COMPARE_NORM_P, &
        P=2, Tolerance=1.e-12_rk, Difference=Difference)
      if (.not. Equal) then
        write (*, '(3a)') "Expected and actual grids have different ", XYZNames(l), &
          " coordinates."
        write (*, '(a,e17.10)') "Difference: ", Difference
        stop 1
      end if
    end do

    deallocate(XYZ)
    deallocate(IBlank)
    deallocate(nPoints)

  end subroutine CheckResults_GenerateUniformGrid3D

  subroutine CheckResults_GenerateCylindricalGrid2D()

    integer :: i, j, l
    integer :: nDimsGridFile
    integer :: nDims
    integer :: nGrids
    integer, pointer :: nPoints(:,:)
    character(len=2) :: CoordPrecision
    character(len=2) :: GridFormat
    character(len=2) :: VolumeFormat
    character(len=2) :: UseIBlank
    real(rk), pointer :: XYZ(:,:,:,:,:)
    integer, pointer :: IBlank(:,:,:,:)
    character(len=PATH_LENGTH) :: GridFile
    logical :: Exists
    integer :: N(2)
    real(rk), allocatable :: XYZ_actual(:,:,:), XYZ_expected(:,:,:)
    real(rk) :: U, V
    real(rk) :: x0, y0
    real(rk) :: r, theta
    logical :: Equal
    real(rk) :: Tolerance
    real(rk) :: Difference

    nullify(nPoints)
    nullify(XYZ)

    GridFile = "RocFlo-CM.00000000.xyz"

    inquire(file=GridFile, exist=Exists)
    if (.not. Exists) then
      write (ERROR_UNIT, '(3a)') "ERROR: Could not locate file '", trim(GridFile), "'."
      stop 2
    end if

    call Read_Grid(nDimsGridFile, nGrids, nPoints, XYZ, IBlank, CoordPrecision, GridFormat, &
      VolumeFormat, UseIBlank, GridFile, 0)

    nDims = 2
    N = [20,100]

    if (.not. all(nPoints(1,:nDims) == N)) then
      write (ERROR_UNIT, '(a)') "ERROR: Grid has incorrect size."
      stop 2
    end if

    allocate(XYZ_actual(N(1),N(2),nDims))

    XYZ_actual = XYZ(1,:,:,1,:nDims)

    allocate(XYZ_expected(N(1),N(2),nDims))

    x0 = 1._rk
    y0 = 2._rk

    do j = 1, N(2)
      do i = 1, N(1)
        U = real(i-1,kind=rk)/real(N(1)-1,kind=rk)
        V = real(j-1,kind=rk)/real(N(2)-1,kind=rk)
        r = 1._rk + (3._rk - 1._rk) * U
        theta = -0.5_rk * Pi + 1.5_rk * Pi * V
        XYZ_expected(i,j,:) = [x0 + r*cos(theta), y0 + r*sin(theta)]
      end do
    end do

!     XYZ(1,:,:,1,:nDims) = XYZ_expected
!     GridFile = "grid_expected.xyz"
!     call Write_Grid(nDimsGridFile, nGrids, nPoints, XYZ, IBlank, CoordPrecision, GridFormat, &
!       VolumeFormat, UseIBlank, GridFile)
!     stop 2

    do l = 1, nDims
      Equal = AlmostEqual(XYZ_expected(:,:,l), XYZ_actual(:,:,l), CompareNorm=COMPARE_NORM_P, &
        P=2, Tolerance=1.e-12_rk, Difference=Difference)
      if (.not. Equal) then
        write (*, '(3a)') "Expected and actual grids have different ", XYZNames(l), &
          " coordinates."
        write (*, '(a,e17.10)') "Difference: ", Difference
        stop 1
      end if
    end do

    deallocate(XYZ)
    deallocate(IBlank)
    deallocate(nPoints)

  end subroutine CheckResults_GenerateCylindricalGrid2D

  subroutine CheckResults_GenerateCylindricalGrid3D()

    integer :: i, j, k, l
    integer :: nDimsGridFile
    integer :: nDims
    integer :: nGrids
    integer, pointer :: nPoints(:,:)
    character(len=2) :: CoordPrecision
    character(len=2) :: GridFormat
    character(len=2) :: VolumeFormat
    character(len=2) :: UseIBlank
    real(rk), pointer :: XYZ(:,:,:,:,:)
    integer, pointer :: IBlank(:,:,:,:)
    character(len=PATH_LENGTH) :: GridFile
    logical :: Exists
    integer :: N(3)
    real(rk), allocatable :: XYZ_actual(:,:,:,:), XYZ_expected(:,:,:,:)
    real(rk) :: U, V, W
    real(rk) :: x0, y0
    real(rk) :: r, theta
    logical :: Equal
    real(rk) :: Tolerance
    real(rk) :: Difference

    nullify(nPoints)
    nullify(XYZ)

    GridFile = "RocFlo-CM.00000000.xyz"

    inquire(file=GridFile, exist=Exists)
    if (.not. Exists) then
      write (ERROR_UNIT, '(3a)') "ERROR: Could not locate file '", trim(GridFile), "'."
      stop 2
    end if

    call Read_Grid(nDimsGridFile, nGrids, nPoints, XYZ, IBlank, CoordPrecision, GridFormat, &
      VolumeFormat, UseIBlank, GridFile, 0)

    nDims = 3
    N = [20,100,20]

    if (.not. all(nPoints(1,:nDims) == N)) then
      write (ERROR_UNIT, '(a)') "ERROR: Grid has incorrect size."
      stop 2
    end if

    allocate(XYZ_actual(N(1),N(2),N(3),nDims))

    XYZ_actual = XYZ(1,:,:,:,:nDims)

    allocate(XYZ_expected(N(1),N(2),N(3),nDims))

    x0 = 1._rk
    y0 = 2._rk

    do k = 1, N(3)
      do j = 1, N(2)
        do i = 1, N(1)
          U = real(i-1,kind=rk)/real(N(1)-1,kind=rk)
          V = real(j-1,kind=rk)/real(N(2)-1,kind=rk)
          W = real(k-1,kind=rk)/real(N(3)-1,kind=rk)
          r = 1._rk + (3._rk - 1._rk) * U
          theta = -0.5_rk * Pi + 1.5_rk * Pi * V
          XYZ_expected(i,j,k,:) = [x0 + r*cos(theta), y0 + r*sin(theta), 2._rk * (W-0.5_rk)]
        end do
      end do
    end do

!     XYZ(1,:,:,:,:nDims) = XYZ_expected
!     GridFile = "grid_expected.xyz"
!     call Write_Grid(nDimsGridFile, nGrids, nPoints, XYZ, IBlank, CoordPrecision, GridFormat, &
!       VolumeFormat, UseIBlank, GridFile)
!     stop 2

    do l = 1, nDims
      Equal = AlmostEqual(XYZ_expected(:,:,:,l), XYZ_actual(:,:,:,l), CompareNorm=COMPARE_NORM_P, &
        P=2, Tolerance=1.e-12_rk, Difference=Difference)
      if (.not. Equal) then
        write (*, '(3a)') "Expected and actual grids have different ", XYZNames(l), &
          " coordinates."
        write (*, '(a,e17.10)') "Difference: ", Difference
        stop 1
      end if
    end do

    deallocate(XYZ)
    deallocate(IBlank)
    deallocate(nPoints)

  end subroutine CheckResults_GenerateCylindricalGrid3D

  subroutine CheckResults_GenerateCylinderUniformCells()

    integer :: i, j, l
    integer :: nDimsGridFile
    integer :: nDims
    integer :: nGrids
    integer, pointer :: nPoints(:,:)
    character(len=2) :: CoordPrecision
    character(len=2) :: GridFormat
    character(len=2) :: VolumeFormat
    character(len=2) :: UseIBlank
    real(rk), pointer :: XYZ(:,:,:,:,:)
    integer, pointer :: IBlank(:,:,:,:)
    character(len=PATH_LENGTH) :: GridFile
    logical :: Exists
    integer :: N(2)
    real(rk), allocatable :: XYZ_actual(:,:,:), XYZ_expected(:,:,:)
    real(rk) :: U, V
    real(rk) :: r, theta
    logical :: Equal
    real(rk) :: Tolerance
    real(rk) :: Difference

    nullify(nPoints)
    nullify(XYZ)

    GridFile = "RocFlo-CM.00000000.xyz"

    inquire(file=GridFile, exist=Exists)
    if (.not. Exists) then
      write (ERROR_UNIT, '(3a)') "ERROR: Could not locate file '", trim(GridFile), "'."
      stop 2
    end if

    call Read_Grid(nDimsGridFile, nGrids, nPoints, XYZ, IBlank, CoordPrecision, GridFormat, &
      VolumeFormat, UseIBlank, GridFile, 0)

    nDims = 2
    N = [20,100]

    if (.not. all(nPoints(1,:nDims) == N)) then
      write (ERROR_UNIT, '(a)') "ERROR: Grid has incorrect size."
      stop 2
    end if

    allocate(XYZ_actual(N(1),N(2),nDims))

    XYZ_actual = XYZ(1,:,:,1,:nDims)

    allocate(XYZ_expected(N(1),N(2),nDims))

    do j = 1, N(2)
      do i = 1, N(1)
        U = real(i-1,kind=rk)/real(N(1)-1,kind=rk)
        V = real(j-1,kind=rk)/real(N(2)-1,kind=rk)
        r = exp(U)
        theta = 2._rk * Pi * V
        XYZ_expected(i,j,:) = [r*cos(theta), r*sin(theta)]
      end do
    end do

!     XYZ(1,:,:,1,:nDims) = XYZ_expected
!     GridFile = "grid_expected.xyz"
!     call Write_Grid(nDimsGridFile, nGrids, nPoints, XYZ, IBlank, CoordPrecision, GridFormat, &
!       VolumeFormat, UseIBlank, GridFile)
!     stop 2

    do l = 1, nDims
      Equal = AlmostEqual(XYZ_expected(:,:,l), XYZ_actual(:,:,l), CompareNorm=COMPARE_NORM_P, &
        P=2, Tolerance=1.e-12_rk, Difference=Difference)
      if (.not. Equal) then
        write (*, '(3a)') "Expected and actual grids have different ", XYZNames(l), &
          " coordinates."
        write (*, '(a,e17.10)') "Difference: ", Difference
        stop 1
      end if
    end do

    deallocate(XYZ)
    deallocate(IBlank)
    deallocate(nPoints)

  end subroutine CheckResults_GenerateCylinderUniformCells

  subroutine CheckResults_GenerateMultipleGrids()

    integer :: i, j, l, m
    integer :: nDimsGridFile
    integer :: nDims
    integer :: nGrids
    integer, pointer :: nPoints(:,:)
    character(len=2) :: CoordPrecision
    character(len=2) :: GridFormat
    character(len=2) :: VolumeFormat
    character(len=2) :: UseIBlank
    real(rk), pointer :: XYZ(:,:,:,:,:)
    integer, pointer :: IBlank(:,:,:,:)
    character(len=PATH_LENGTH) :: GridFile
    logical :: Exists
    integer :: N(2,2)
    real(rk), allocatable :: XYZ_actual(:,:,:,:), XYZ_expected(:,:,:,:)
    real(rk), allocatable :: ExpectedValues(:,:), ActualValues(:,:)
    real(rk) :: U, V
    real(rk) :: r, theta
    logical :: Equal
    real(rk) :: Tolerance
    real(rk) :: Difference

    nullify(nPoints)
    nullify(XYZ)

    GridFile = "RocFlo-CM.00000000.xyz"

    inquire(file=GridFile, exist=Exists)
    if (.not. Exists) then
      write (ERROR_UNIT, '(3a)') "ERROR: Could not locate file '", trim(GridFile), "'."
      stop 2
    end if

    call Read_Grid(nDimsGridFile, nGrids, nPoints, XYZ, IBlank, CoordPrecision, GridFormat, &
      VolumeFormat, UseIBlank, GridFile, 0)

    nDims = 2
    N(:,1) = [40,40]
    N(:,2) = [20,100]

    if (.not. all(nPoints(1,:nDims) == N(:,1)) .or. .not. all(nPoints(2,:nDims) == N(:,2))) then
      write (ERROR_UNIT, '(a)') "ERROR: Grid has incorrect size."
      stop 2
    end if

    allocate(XYZ_actual(maxval(N(1,:)),maxval(N(2,:)),nDims,nGrids))

    XYZ_actual(:,:,:,1) = XYZ(1,:,:,1,:nDims)
    XYZ_actual(:,:,:,2) = XYZ(2,:,:,1,:nDims)

    allocate(XYZ_expected(maxval(N(1,:)),maxval(N(2,:)),nDims,nGrids))

    do j = 1, N(2,1)
      do i = 1, N(1,1)
        U = real(i-1,kind=rk)/real(N(1,1)-1,kind=rk)
        V = real(j-1,kind=rk)/real(N(2,1)-1,kind=rk)
        XYZ_expected(i,j,:,1) = 6._rk * ([U,V] - 0.5_rk)
      end do
    end do

    do j = 1, N(2,2)
      do i = 1, N(1,2)
        U = real(i-1,kind=rk)/real(N(1,2)-1,kind=rk)
        V = real(j-1,kind=rk)/real(N(2,2)-1,kind=rk)
        r = 0.5_rk + (2._rk - 0.5_rk) * U
        theta = 1.5_rk * Pi * (V - 0.5_rk)
        XYZ_expected(i,j,:,2) = [r*cos(theta), r*sin(theta)]
      end do
    end do

!     XYZ(1,:,:,1,:nDims) = XYZ_expected(:,:,:,1)
!     XYZ(2,:,:,1,:nDims) = XYZ_expected(:,:,:,2)
!     GridFile = "grid_expected.xyz"
!     call Write_Grid(nDimsGridFile, nGrids, nPoints, XYZ, IBlank, CoordPrecision, GridFormat, &
!       VolumeFormat, UseIBlank, GridFile)
!     stop 2

    do m = 1, nGrids
      allocate(ExpectedValues(N(1,m),N(2,m)))
      allocate(ActualValues(N(1,m),N(2,m)))
      do l = 1, nDims
        ExpectedValues = XYZ_expected(:N(1,m),:N(2,m),l,m)
        ActualValues = XYZ_actual(:N(1,m),:N(2,m),l,m)
        Equal = AlmostEqual(ExpectedValues, ActualValues, CompareNorm=COMPARE_NORM_P, &
          P=2, Tolerance=1.e-12_rk, Difference=Difference)
        if (.not. Equal) then
          write (*, '(3a)') "Expected and actual grids have different ", XYZNames(l), &
            " coordinates."
          write (*, '(a,e17.10)') "Difference: ", Difference
          stop 1
        end if
      end do
      deallocate(ExpectedValues)
      deallocate(ActualValues)
    end do

    deallocate(XYZ)
    deallocate(IBlank)
    deallocate(nPoints)

  end subroutine CheckResults_GenerateMultipleGrids

  subroutine CheckResults_RunSimWithGeneratedGrid()

    integer :: i, j, l
    integer :: nDimsGridFile
    integer :: nDims
    integer :: nCVs
    integer :: nGrids
    integer, pointer :: nPoints(:,:)
    character(len=2) :: CoordPrecision
    character(len=2) :: GridFormat
    character(len=2) :: VolumeFormat
    character(len=2) :: UseIBlank
    real(rk), pointer :: XYZ(:,:,:,:,:)
    integer, pointer :: IBlank(:,:,:,:)
    real(rk), pointer :: Q(:,:,:,:,:)
    character(len=PATH_LENGTH) :: GridFile
    character(len=PATH_LENGTH) :: SolutionFile
    logical :: Exists
    integer :: N(2)
    real(rk), allocatable :: XYZ_actual(:,:,:), XYZ_expected(:,:,:)
    real(rk), allocatable :: Q_actual(:,:,:), Q_expected(:,:,:)
    real(rk) :: Tau(4)
    real(rk) :: U, V
    logical :: Equal
    real(rk) :: Tolerance
    real(rk) :: Difference
    real(rk) :: Gamma

    nullify(nPoints)
    nullify(XYZ)
    nullify(Q)

    GridFile = "RocFlo-CM.00000000.xyz"
    SolutionFile = "RocFlo-CM.00000200.q"

    inquire(file=GridFile, exist=Exists)
    if (.not. Exists) then
      write (ERROR_UNIT, '(3a)') "ERROR: Could not locate file '", trim(GridFile), "'."
      stop 2
    end if
    inquire(file=SolutionFile, exist=Exists)
    if (.not. Exists) then
      write (ERROR_UNIT, '(3a)') "ERROR: Could not locate file '", trim(SolutionFile), "'."
      stop 2
    end if

    call Read_Grid(nDimsGridFile, nGrids, nPoints, XYZ, IBlank, CoordPrecision, GridFormat, &
      VolumeFormat, UseIBlank, GridFile, 0)
    call Read_Soln(nDimsGridFile, nGrids, nPoints, Q, Tau, CoordPrecision, GridFormat, &
      VolumeFormat, SolutionFile, 0)

    nDims = 2
    nCVs = nDims + 2
    N = nPoints(1,:nDims)
    N = [40,40]

    if (.not. all(nPoints(1,:nDims) == N)) then
      write (ERROR_UNIT, '(a)') "ERROR: Grid has incorrect size."
      stop 2
    end if

    allocate(XYZ_actual(N(1),N(2),nDims))

    XYZ_actual = XYZ(1,:,:,1,:nDims)

    allocate(Q_actual(N(1),N(2),nCVs))

    Q_actual(:,:,:3) = Q(1,:,:,1,:3)
    Q_actual(:,:,4) = Q(1,:,:,1,5)

    allocate(XYZ_expected(N(1),N(2),nDims))

    do j = 1, N(2)
      do i = 1, N(1)
        U = real(i-1,kind=rk)/real(N(1)-1,kind=rk)
        V = real(j-1,kind=rk)/real(N(2)-1,kind=rk)
        XYZ_expected(i,j,:) = 2._rk * ([U,V] - 0.5_rk)
      end do
    end do

    allocate(Q_expected(N(1),N(2),nCVs))

    Gamma = 1.4_rk

    Q_expected(:,:,1) = 1._rk
    Q_expected(:,:,2) = 0.2_rk
    Q_expected(:,:,3) = 0._rk
    Q_expected(:,:,4) = 1._rk/(Gamma*(Gamma-1._rk)) + 0.5_rk * 0.2_rk**2

!     XYZ(1,:,:,1,:nDims) = XYZ_expected
!     GridFile = "grid_expected.xyz"
!     call Write_Grid(nDimsGridFile, nGrids, nPoints, XYZ, IBlank, CoordPrecision, GridFormat, &
!       VolumeFormat, UseIBlank, GridFile)
!     Q(1,:,:,1,:3) = Q_expected(:,:,:3)
!     Q(1,:,:,1,5) = Q_expected(:,:,4)
!     SolutionFile = "solution_expected.xyz"
!     call Write_Soln(nDimsGridFile, nGrids, nPoints, Q, Tau, CoordPrecision, GridFormat, &
!       VolumeFormat, SolutionFile)
!     stop 2

    do l = 1, nDims
      Equal = AlmostEqual(XYZ_expected(:,:,l), XYZ_actual(:,:,l), CompareNorm=COMPARE_NORM_P, &
        P=2, Tolerance=1.e-12_rk, Difference=Difference)
      if (.not. Equal) then
        write (*, '(3a)') "Expected and actual grids have different ", XYZNames(l), &
          " coordinates."
        write (*, '(a,e17.10)') "Difference: ", Difference
        stop 1
      end if
    end do

    do l = 1, nCVs
      Equal = AlmostEqual(Q_expected(:,:,l), Q_actual(:,:,l), CompareNorm=COMPARE_NORM_P, &
        P=2, Tolerance=1.e-12_rk, Difference=Difference)
      if (.not. Equal) then
        write (*, '(3a)') "Expected and actual solutions have different ", QNames2D(l), "."
        write (*, '(a,e17.10)') "Difference: ", Difference
        stop 1
      end if
    end do

    deallocate(Q)
    deallocate(XYZ)
    deallocate(IBlank)
    deallocate(nPoints)

  end subroutine CheckResults_RunSimWithGeneratedGrid

  subroutine CheckResults_SetIBlankRange2D()

    integer :: nDimsGridFile
    integer :: nDims
    integer :: nGrids
    integer, pointer :: nPoints(:,:)
    character(len=2) :: CoordPrecision
    character(len=2) :: GridFormat
    character(len=2) :: VolumeFormat
    character(len=2) :: UseIBlank
    real(rk), pointer :: XYZ(:,:,:,:,:)
    integer, pointer :: IBlank(:,:,:,:)
    character(len=PATH_LENGTH) :: GridFile
    logical :: Exists
    integer :: N(2)
    integer, allocatable :: IBlank_actual(:,:), IBlank_expected(:,:)
    logical :: Equal
    real(rk) :: Tolerance
    real(rk) :: Difference

    nullify(nPoints)
    nullify(XYZ)

    GridFile = "RocFlo-CM.00000000.xyz"

    inquire(file=GridFile, exist=Exists)
    if (.not. Exists) then
      write (ERROR_UNIT, '(3a)') "ERROR: Could not locate file '", trim(GridFile), "'."
      stop 2
    end if

    call Read_Grid(nDimsGridFile, nGrids, nPoints, XYZ, IBlank, CoordPrecision, GridFormat, &
      VolumeFormat, UseIBlank, GridFile, 0)

    nDims = 2
    N = [30,60]

    if (.not. all(nPoints(1,:nDims) == N)) then
      write (ERROR_UNIT, '(a)') "ERROR: Grid has incorrect size."
      stop 2
    end if

    allocate(IBlank_actual(N(1),N(2)))

    IBlank_actual = IBlank(1,:,:,1)

    allocate(IBlank_expected(N(1),N(2)))

    IBlank_expected = 1
    IBlank_expected(11:20,21:40) = 0

!     IBlank(1,:,:,1) = IBlank_expected
!     GridFile = "grid_expected.xyz"
!     call Write_Grid(nDimsGridFile, nGrids, nPoints, XYZ, IBlank, CoordPrecision, GridFormat, &
!       VolumeFormat, UseIBlank, GridFile)
!     stop 2

    Equal = all(IBlank_expected == IBlank_actual)
    if (.not. Equal) then
      write (*, '(a)') "Expected and actual grids have different IBlank values."
      stop 1
    end if

    deallocate(XYZ)
    deallocate(IBlank)
    deallocate(nPoints)

  end subroutine CheckResults_SetIBlankRange2D

  subroutine CheckResults_SetIBlankRange3D()

    integer :: nDimsGridFile
    integer :: nDims
    integer :: nGrids
    integer, pointer :: nPoints(:,:)
    character(len=2) :: CoordPrecision
    character(len=2) :: GridFormat
    character(len=2) :: VolumeFormat
    character(len=2) :: UseIBlank
    real(rk), pointer :: XYZ(:,:,:,:,:)
    integer, pointer :: IBlank(:,:,:,:)
    character(len=PATH_LENGTH) :: GridFile
    logical :: Exists
    integer :: N(3)
    integer, allocatable :: IBlank_actual(:,:,:), IBlank_expected(:,:,:)
    logical :: Equal
    real(rk) :: Tolerance
    real(rk) :: Difference

    nullify(nPoints)
    nullify(XYZ)

    GridFile = "RocFlo-CM.00000000.xyz"

    inquire(file=GridFile, exist=Exists)
    if (.not. Exists) then
      write (ERROR_UNIT, '(3a)') "ERROR: Could not locate file '", trim(GridFile), "'."
      stop 2
    end if

    call Read_Grid(nDimsGridFile, nGrids, nPoints, XYZ, IBlank, CoordPrecision, GridFormat, &
      VolumeFormat, UseIBlank, GridFile, 0)

    nDims = 3
    N = [30,60,90]

    if (.not. all(nPoints(1,:nDims) == N)) then
      write (ERROR_UNIT, '(a)') "ERROR: Grid has incorrect size."
      stop 2
    end if

    allocate(IBlank_actual(N(1),N(2),N(3)))

    IBlank_actual = IBlank(1,:,:,:)

    allocate(IBlank_expected(N(1),N(2),N(3)))

    IBlank_expected = 1
    IBlank_expected(11:20,21:40,31:60) = 0

!     IBlank(1,:,:,:) = IBlank_expected
!     GridFile = "grid_expected.xyz"
!     call Write_Grid(nDimsGridFile, nGrids, nPoints, XYZ, IBlank, CoordPrecision, GridFormat, &
!       VolumeFormat, UseIBlank, GridFile)
!     stop 2

    Equal = all(IBlank_expected == IBlank_actual)
    if (.not. Equal) then
      write (*, '(a)') "Expected and actual grids have different IBlank values."
      stop 1
    end if

    deallocate(XYZ)
    deallocate(IBlank)
    deallocate(nPoints)

  end subroutine CheckResults_SetIBlankRange3D

  subroutine CheckResults_MoveGrid2D()

    integer :: i, j, l
    integer :: nDimsGridFile
    integer :: nDims
    integer :: nGrids
    integer, pointer :: nPoints(:,:)
    character(len=2) :: CoordPrecision
    character(len=2) :: GridFormat
    character(len=2) :: VolumeFormat
    character(len=2) :: UseIBlank
    real(rk), pointer :: XYZ(:,:,:,:,:)
    integer, pointer :: IBlank(:,:,:,:)
    character(len=PATH_LENGTH) :: GridFile
    logical :: Exists
    integer :: N(2)
    real(rk), allocatable :: XYZ_actual(:,:,:), XYZ_expected(:,:,:)
    real(rk) :: U, V
    logical :: Equal
    real(rk) :: Tolerance
    real(rk) :: Difference

    nullify(nPoints)
    nullify(XYZ)

    GridFile = "RocFlo-CM.00000000.xyz"

    inquire(file=GridFile, exist=Exists)
    if (.not. Exists) then
      write (ERROR_UNIT, '(3a)') "ERROR: Could not locate file '", trim(GridFile), "'."
      stop 2
    end if

    call Read_Grid(nDimsGridFile, nGrids, nPoints, XYZ, IBlank, CoordPrecision, GridFormat, VolumeFormat, &
      UseIBlank, GridFile, 0)

    nDims = 2
    N = [20,40]

    if (.not. all(nPoints(1,:nDims) == N)) then
      write (ERROR_UNIT, '(a)') "ERROR: Grid has incorrect size."
      stop 2
    end if

    allocate(XYZ_actual(N(1),N(2),nDims))

    XYZ_actual = XYZ(1,:,:,1,:nDims)

    allocate(XYZ_expected(N(1),N(2),nDims))

    do j = 1, N(2)
      do i = 1, N(1)
        U = real(i-1,kind=rk)/real(N(1)-1,kind=rk)
        V = real(j-1,kind=rk)/real(N(2)-1,kind=rk)
        XYZ_expected(i,j,:) = [1._rk,2._rk] * (1._rk + [U,V])
      end do
    end do

!     XYZ(1,:,:,1,:nDims) = XYZ_expected
!     GridFile = "grid_expected.xyz"
!     call Write_Grid(nDimsGridFile, nGrids, nPoints, XYZ, IBlank, CoordPrecision, GridFormat, &
!       VolumeFormat, UseIBlank, GridFile)
!     stop 2

    do l = 1, nDims
      Equal = AlmostEqual(XYZ_expected(:,:,l), XYZ_actual(:,:,l), CompareNorm=COMPARE_NORM_P, &
        P=2, Tolerance=1.e-12_rk, Difference=Difference)
      if (.not. Equal) then
        write (*, '(3a)') "Expected and actual grids have different ", XYZNames(l), &
          " coordinates."
        write (*, '(a,e17.10)') "Difference: ", Difference
        stop 1
      end if
    end do

    deallocate(XYZ)
    deallocate(IBlank)
    deallocate(nPoints)

  end subroutine CheckResults_MoveGrid2D

  subroutine CheckResults_MoveGrid3D()

    integer :: i, j, k, l
    integer :: nDimsGridFile
    integer :: nDims
    integer :: nGrids
    integer, pointer :: nPoints(:,:)
    character(len=2) :: CoordPrecision
    character(len=2) :: GridFormat
    character(len=2) :: VolumeFormat
    character(len=2) :: UseIBlank
    real(rk), pointer :: XYZ(:,:,:,:,:)
    integer, pointer :: IBlank(:,:,:,:)
    character(len=PATH_LENGTH) :: GridFile
    logical :: Exists
    integer :: N(3)
    real(rk), allocatable :: XYZ_actual(:,:,:,:), XYZ_expected(:,:,:,:)
    real(rk) :: U, V, W
    logical :: Equal
    real(rk) :: Tolerance
    real(rk) :: Difference

    nullify(nPoints)
    nullify(XYZ)

    GridFile = "RocFlo-CM.00000000.xyz"

    inquire(file=GridFile, exist=Exists)
    if (.not. Exists) then
      write (ERROR_UNIT, '(3a)') "ERROR: Could not locate file '", trim(GridFile), "'."
      stop 2
    end if

    call Read_Grid(nDimsGridFile, nGrids, nPoints, XYZ, IBlank, CoordPrecision, GridFormat, VolumeFormat, &
      UseIBlank, GridFile, 0)

    nDims = 3
    N = [20,40,60]

    if (.not. all(nPoints(1,:nDims) == N)) then
      write (ERROR_UNIT, '(a)') "ERROR: Grid has incorrect size."
      stop 2
    end if

    allocate(XYZ_actual(N(1),N(2),N(3),nDims))

    XYZ_actual = XYZ(1,:,:,:,:nDims)

    allocate(XYZ_expected(N(1),N(2),N(3),nDims))

    do k = 1, N(3)
      do j = 1, N(2)
        do i = 1, N(1)
          U = real(i-1,kind=rk)/real(N(1)-1,kind=rk)
          V = real(j-1,kind=rk)/real(N(2)-1,kind=rk)
          W = real(k-1,kind=rk)/real(N(3)-1,kind=rk)
          XYZ_expected(i,j,k,:) = [1._rk,2._rk,3._rk] * (1._rk + [U,V,W])
        end do
      end do
    end do

!     XYZ(1,:,:,:,:nDims) = XYZ_expected
!     GridFile = "grid_expected.xyz"
!     call Write_Grid(nDimsGridFile, nGrids, nPoints, XYZ, IBlank, CoordPrecision, GridFormat, &
!       VolumeFormat, UseIBlank, GridFile)
!     stop 2

    do l = 1, nDims
      Equal = AlmostEqual(XYZ_expected(:,:,:,l), XYZ_actual(:,:,:,l), CompareNorm=COMPARE_NORM_P, &
        P=2, Tolerance=1.e-12_rk, Difference=Difference)
      if (.not. Equal) then
        write (*, '(3a)') "Expected and actual grids have different ", XYZNames(l), &
          " coordinates."
        write (*, '(a,e17.10)') "Difference: ", Difference
        stop 1
      end if
    end do

    deallocate(XYZ)
    deallocate(IBlank)
    deallocate(nPoints)

  end subroutine CheckResults_MoveGrid3D

  subroutine CheckResults_RotateGrid2D()

    integer :: i, j, l
    integer :: nDimsGridFile
    integer :: nDims
    integer :: nGrids
    integer, pointer :: nPoints(:,:)
    character(len=2) :: CoordPrecision
    character(len=2) :: GridFormat
    character(len=2) :: VolumeFormat
    character(len=2) :: UseIBlank
    real(rk), pointer :: XYZ(:,:,:,:,:)
    integer, pointer :: IBlank(:,:,:,:)
    character(len=PATH_LENGTH) :: GridFile
    logical :: Exists
    integer :: N(2)
    real(rk), allocatable :: XYZ_actual(:,:,:), XYZ_expected(:,:,:)
    real(rk) :: U, V
    logical :: Equal
    real(rk) :: Tolerance
    real(rk) :: Difference

    nullify(nPoints)
    nullify(XYZ)

    GridFile = "RocFlo-CM.00000000.xyz"

    inquire(file=GridFile, exist=Exists)
    if (.not. Exists) then
      write (ERROR_UNIT, '(3a)') "ERROR: Could not locate file '", trim(GridFile), "'."
      stop 2
    end if

    call Read_Grid(nDimsGridFile, nGrids, nPoints, XYZ, IBlank, CoordPrecision, GridFormat, VolumeFormat, &
      UseIBlank, GridFile, 0)

    nDims = 2
    N = [20,40]

    if (.not. all(nPoints(1,:nDims) == N)) then
      write (ERROR_UNIT, '(a)') "ERROR: Grid has incorrect size."
      stop 2
    end if

    allocate(XYZ_actual(N(1),N(2),nDims))

    XYZ_actual = XYZ(1,:,:,1,:nDims)

    allocate(XYZ_expected(N(1),N(2),nDims))

    do j = 1, N(2)
      do i = 1, N(1)
        U = real(i-1,kind=rk)/real(N(1)-1,kind=rk)
        V = real(j-1,kind=rk)/real(N(2)-1,kind=rk)
        XYZ_expected(i,j,:) = [3._rk, 6._rk] + [-1._rk * U,-2._rk * V]
      end do
    end do

!     XYZ(1,:,:,1,:nDims) = XYZ_expected
!     GridFile = "grid_expected.xyz"
!     call Write_Grid(nDimsGridFile, nGrids, nPoints, XYZ, IBlank, CoordPrecision, GridFormat, &
!       VolumeFormat, UseIBlank, GridFile)
!     stop 2

    do l = 1, nDims
      Equal = AlmostEqual(XYZ_expected(:,:,l), XYZ_actual(:,:,l), CompareNorm=COMPARE_NORM_P, &
        P=2, Tolerance=1.e-12_rk, Difference=Difference)
      if (.not. Equal) then
        write (*, '(3a)') "Expected and actual grids have different ", XYZNames(l), &
          " coordinates."
        write (*, '(a,e17.10)') "Difference: ", Difference
        stop 1
      end if
    end do

    deallocate(XYZ)
    deallocate(IBlank)
    deallocate(nPoints)

  end subroutine CheckResults_RotateGrid2D

  subroutine CheckResults_RotateGrid3D()

    integer :: i, j, k, l
    integer :: nDimsGridFile
    integer :: nDims
    integer :: nGrids
    integer, pointer :: nPoints(:,:)
    character(len=2) :: CoordPrecision
    character(len=2) :: GridFormat
    character(len=2) :: VolumeFormat
    character(len=2) :: UseIBlank
    real(rk), pointer :: XYZ(:,:,:,:,:)
    integer, pointer :: IBlank(:,:,:,:)
    character(len=PATH_LENGTH) :: GridFile
    logical :: Exists
    integer :: N(3)
    real(rk), allocatable :: XYZ_actual(:,:,:,:), XYZ_expected(:,:,:,:)
    real(rk) :: U, V, W
    logical :: Equal
    real(rk) :: Tolerance
    real(rk) :: Difference

    nullify(nPoints)
    nullify(XYZ)

    GridFile = "RocFlo-CM.00000000.xyz"

    inquire(file=GridFile, exist=Exists)
    if (.not. Exists) then
      write (ERROR_UNIT, '(3a)') "ERROR: Could not locate file '", trim(GridFile), "'."
      stop 2
    end if

    call Read_Grid(nDimsGridFile, nGrids, nPoints, XYZ, IBlank, CoordPrecision, GridFormat, VolumeFormat, &
      UseIBlank, GridFile, 0)

    nDims = 3
    N = [20,40,60]

    if (.not. all(nPoints(1,:nDims) == N)) then
      write (ERROR_UNIT, '(a)') "ERROR: Grid has incorrect size."
      stop 2
    end if

    allocate(XYZ_actual(N(1),N(2),N(3),nDims))

    XYZ_actual = XYZ(1,:,:,:,:nDims)

    allocate(XYZ_expected(N(1),N(2),N(3),nDims))

    do k = 1, N(3)
      do j = 1, N(2)
        do i = 1, N(1)
          U = real(i-1,kind=rk)/real(N(1)-1,kind=rk)
          V = real(j-1,kind=rk)/real(N(2)-1,kind=rk)
          W = real(k-1,kind=rk)/real(N(3)-1,kind=rk)
          XYZ_expected(i,j,k,:) = [4._rk, 5._rk, 9._rk] + [-2._rk * V,-1._rk * U, -3._rk * W]
        end do
      end do
    end do

!     XYZ(1,:,:,:,:nDims) = XYZ_expected
!     GridFile = "grid_expected.xyz"
!     call Write_Grid(nDimsGridFile, nGrids, nPoints, XYZ, IBlank, CoordPrecision, GridFormat, &
!       VolumeFormat, UseIBlank, GridFile)
!     stop 2

    do l = 1, nDims
      Equal = AlmostEqual(XYZ_expected(:,:,:,l), XYZ_actual(:,:,:,l), CompareNorm=COMPARE_NORM_P, &
        P=2, Tolerance=1.e-12_rk, Difference=Difference)
      if (.not. Equal) then
        write (*, '(3a)') "Expected and actual grids have different ", XYZNames(l), &
          " coordinates."
        write (*, '(a,e17.10)') "Difference: ", Difference
        stop 1
      end if
    end do

    deallocate(XYZ)
    deallocate(IBlank)
    deallocate(nPoints)

  end subroutine CheckResults_RotateGrid3D

  subroutine CheckResults_ScaleGrid2D()

    integer :: i, j, l
    integer :: nDimsGridFile
    integer :: nDims
    integer :: nGrids
    integer, pointer :: nPoints(:,:)
    character(len=2) :: CoordPrecision
    character(len=2) :: GridFormat
    character(len=2) :: VolumeFormat
    character(len=2) :: UseIBlank
    real(rk), pointer :: XYZ(:,:,:,:,:)
    integer, pointer :: IBlank(:,:,:,:)
    character(len=PATH_LENGTH) :: GridFile
    logical :: Exists
    integer :: N(2)
    real(rk), allocatable :: XYZ_actual(:,:,:), XYZ_expected(:,:,:)
    real(rk) :: U, V
    logical :: Equal
    real(rk) :: Tolerance
    real(rk) :: Difference

    nullify(nPoints)
    nullify(XYZ)

    GridFile = "RocFlo-CM.00000000.xyz"

    inquire(file=GridFile, exist=Exists)
    if (.not. Exists) then
      write (ERROR_UNIT, '(3a)') "ERROR: Could not locate file '", trim(GridFile), "'."
      stop 2
    end if

    call Read_Grid(nDimsGridFile, nGrids, nPoints, XYZ, IBlank, CoordPrecision, GridFormat, VolumeFormat, &
      UseIBlank, GridFile, 0)

    nDims = 2
    N = [20,40]

    if (.not. all(nPoints(1,:nDims) == N)) then
      write (ERROR_UNIT, '(a)') "ERROR: Grid has incorrect size."
      stop 2
    end if

    allocate(XYZ_actual(N(1),N(2),nDims))

    XYZ_actual = XYZ(1,:,:,1,:nDims)

    allocate(XYZ_expected(N(1),N(2),nDims))

    do j = 1, N(2)
      do i = 1, N(1)
        U = real(i-1,kind=rk)/real(N(1)-1,kind=rk)
        V = real(j-1,kind=rk)/real(N(2)-1,kind=rk)
        XYZ_expected(i,j,:) = [1._rk, 2._rk] + [2._rk, 6._rk] * [U,V]
      end do
    end do

!     XYZ(1,:,:,1,:nDims) = XYZ_expected
!     GridFile = "grid_expected.xyz"
!     call Write_Grid(nDimsGridFile, nGrids, nPoints, XYZ, IBlank, CoordPrecision, GridFormat, &
!       VolumeFormat, UseIBlank, GridFile)
!     stop 2

    do l = 1, nDims
      Equal = AlmostEqual(XYZ_expected(:,:,l), XYZ_actual(:,:,l), CompareNorm=COMPARE_NORM_P, &
        P=2, Tolerance=1.e-12_rk, Difference=Difference)
      if (.not. Equal) then
        write (*, '(3a)') "Expected and actual grids have different ", XYZNames(l), &
          " coordinates."
        write (*, '(a,e17.10)') "Difference: ", Difference
        stop 1
      end if
    end do

    deallocate(XYZ)
    deallocate(IBlank)
    deallocate(nPoints)

  end subroutine CheckResults_ScaleGrid2D

  subroutine CheckResults_ScaleGrid3D()

    integer :: i, j, k, l
    integer :: nDimsGridFile
    integer :: nDims
    integer :: nGrids
    integer, pointer :: nPoints(:,:)
    character(len=2) :: CoordPrecision
    character(len=2) :: GridFormat
    character(len=2) :: VolumeFormat
    character(len=2) :: UseIBlank
    real(rk), pointer :: XYZ(:,:,:,:,:)
    integer, pointer :: IBlank(:,:,:,:)
    character(len=PATH_LENGTH) :: GridFile
    logical :: Exists
    integer :: N(3)
    real(rk), allocatable :: XYZ_actual(:,:,:,:), XYZ_expected(:,:,:,:)
    real(rk) :: U, V, W
    logical :: Equal
    real(rk) :: Tolerance
    real(rk) :: Difference

    nullify(nPoints)
    nullify(XYZ)

    GridFile = "RocFlo-CM.00000000.xyz"

    inquire(file=GridFile, exist=Exists)
    if (.not. Exists) then
      write (ERROR_UNIT, '(3a)') "ERROR: Could not locate file '", trim(GridFile), "'."
      stop 2
    end if

    call Read_Grid(nDimsGridFile, nGrids, nPoints, XYZ, IBlank, CoordPrecision, GridFormat, VolumeFormat, &
      UseIBlank, GridFile, 0)

    nDims = 3
    N = [20,40,60]

    if (.not. all(nPoints(1,:nDims) == N)) then
      write (ERROR_UNIT, '(a)') "ERROR: Grid has incorrect size."
      stop 2
    end if

    allocate(XYZ_actual(N(1),N(2),N(3),nDims))

    XYZ_actual = XYZ(1,:,:,:,:nDims)

    allocate(XYZ_expected(N(1),N(2),N(3),nDims))

    do k = 1, N(3)
      do j = 1, N(2)
        do i = 1, N(1)
          U = real(i-1,kind=rk)/real(N(1)-1,kind=rk)
          V = real(j-1,kind=rk)/real(N(2)-1,kind=rk)
          W = real(k-1,kind=rk)/real(N(3)-1,kind=rk)
          XYZ_expected(i,j,k,:) = [1._rk, 2._rk, 3._rk] + [2._rk, 6._rk, 12._rk] * [U,V,W]
        end do
      end do
    end do

!     XYZ(1,:,:,:,:nDims) = XYZ_expected
!     GridFile = "grid_expected.xyz"
!     call Write_Grid(nDimsGridFile, nGrids, nPoints, XYZ, IBlank, CoordPrecision, GridFormat, &
!       VolumeFormat, UseIBlank, GridFile)
!     stop 2

    do l = 1, nDims
      Equal = AlmostEqual(XYZ_expected(:,:,:,l), XYZ_actual(:,:,:,l), CompareNorm=COMPARE_NORM_P, &
        P=2, Tolerance=1.e-12_rk, Difference=Difference)
      if (.not. Equal) then
        write (*, '(3a)') "Expected and actual grids have different ", XYZNames(l), &
          " coordinates."
        write (*, '(a,e17.10)') "Difference: ", Difference
        stop 1
      end if
    end do

    deallocate(XYZ)
    deallocate(IBlank)
    deallocate(nPoints)

  end subroutine CheckResults_ScaleGrid3D

end program CheckGeometry
