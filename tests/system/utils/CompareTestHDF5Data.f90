program CompareTestHDF5Data

  use ModBC
  use ModParseArguments
  use ModStdIO
  use ModHDF5_IO
  implicit none

  integer, parameter :: MAX_ND = 3
  integer, parameter :: rk = kind(0.d0)

  integer :: i
  character(len=CMD_ARG_LENGTH), dimension(:), allocatable :: RawArguments, Arguments

  allocate(RawArguments(command_argument_count()))
  do i = 1, size(RawArguments)
    call get_command_argument(i, RawArguments(i))
  end do

  call ParseArguments(RawArguments, Arguments=Arguments, MinArguments=1, MaxArguments=1)

  select case (trim(Arguments(1)))
  case ("CompareHDF5Grid2D")
    call CompareHDF5Grid2D()
  case ("CompareHDF5Grid3D")
    call CompareHDF5Grid3D()
  case ("CompareHDF5CV2D")
    call CompareHDF5CV2D()
  case ("CompareHDF5CV3D")
    call CompareHDF5CV3D()
  case ("CompareHDF5CVTarget2D")
    call CompareHDF5CVTarget2D()
  case ("CompareHDF5CVTarget3D")
    call CompareHDF5CVTarget3D()
  case ("CompareHDF5Aux2D")
    call CompareHDF5Aux2D()
  case ("CompareHDF5Aux3D")
    call CompareHDF5Aux3D()
  case ("CompareHDF5AuxTarget2D")
    call CompareHDF5AuxTarget2D()
  case ("CompareHDF5AuxTarget3D")
    call CompareHDF5AuxTarget3D()
  case default
    write (ERROR_UNIT, '(3a)') "ERROR: Unknown case ID '", trim(Arguments(1)), "'."
    stop 1
  end select

contains

  subroutine CompareHDF5Grid2D

    integer, parameter :: nGrids = 2
    integer, parameter :: nDims = 2

    integer :: i, j, m
    real(rk) :: U, V
    integer, dimension(:,:), pointer :: nPoints
    real(rk), dimension(nGrids,nDims) :: Lengths
    real(rk), dimension(:,:,:,:,:), pointer :: X
    real(rk), dimension(:,:,:,:,:), pointer :: XCompare
    integer, dimension(:,:,:,:), pointer :: IBlank
    integer, dimension(:,:,:,:), pointer :: IBlankCompare
    character(len=80) :: HDF5File

    allocate(nPoints(nGrids,MAX_ND))

    nPoints = 1
    nPoints(1,:nDims) = [20,30]
    nPoints(2,:nDims) = [10,20]

    Lengths(1,:) = [2._rk, 3._rk]
    Lengths(2,:) = [1._rk, 2._rk]

    allocate(X(nGrids,maxval(nPoints(:,1)),maxval(nPoints(:,2)),maxval(nPoints(:,3)),nDims))
    X = 0._rk

    do m = 1, nGrids
      do j = 1, nPoints(m,2)
        do i = 1, nPoints(m,1)
          U = real(i-1, kind=rk)/real(nPoints(m,1)-1, kind=rk)
          V = real(j-1, kind=rk)/real(nPoints(m,2)-1, kind=rk)
          X(m,i,j,1,:) = Lengths(m,:) * ([U,V]-0.5_rk)
        end do
      end do
    end do

    allocate(IBlank(nGrids,maxval(nPoints(:,1)),maxval(nPoints(:,2)),maxval(nPoints(:,3))))
    IBlank = 1

    HDF5File = "grid_2d.h5"
    call WriteHDF5File(HDF5File, nDims=nDims, nGrids=nGrids, nPoints=nPoints, X=X, IBlank=IBlank)

    allocate(XCompare(nGrids,maxval(nPoints(:,1)),maxval(nPoints(:,2)),maxval(nPoints(:,3)),nDims))
    allocate(IBlankCompare(nGrids,maxval(nPoints(:,1)),maxval(nPoints(:,2)),maxval(nPoints(:,3))))

    ! Perturb the coordinates slightly
    XCompare = X
    do m = 1, nGrids
      do j = 1, nPoints(m,2)
        do i = 1, nPoints(m,1)
          XCompare(m,i,j,1,:) = XCompare(m,i,j,1,:) + 1.e-10_rk
        end do
      end do
    end do

    HDF5File = "grid_2d_almost.h5"
    call WriteHDF5File(HDF5File, nDims=nDims, nGrids=nGrids, nPoints=nPoints, X=XCompare, IBlank=IBlank)

    ! Perturb the coordinates by a large amount
    XCompare = X
    do m = 1, nGrids
      do j = 1, nPoints(m,2)
        do i = 1, nPoints(m,1)
          XCompare(m,i,j,1,:) = XCompare(m,i,j,1,:) + 1._rk
        end do
      end do
    end do

    HDF5File = "grid_2d_different_xyz.h5"
    call WriteHDF5File(HDF5File, nDims=nDims, nGrids=nGrids, nPoints=nPoints, X=XCompare, IBlank=IBlank)

    ! Modify the IBlank values
    IBlankCompare = 0

    HDF5File = "grid_2d_different_iblank.h5"
    call WriteHDF5File(HDF5File, nDims=nDims, nGrids=nGrids, nPoints=nPoints, X=X, IBlank=IBlankCompare)

    deallocate(X)
    deallocate(XCompare)
    deallocate(IBlank)
    deallocate(IBlankCompare)

  end subroutine CompareHDF5Grid2D

  subroutine CompareHDF5Grid3D

    integer, parameter :: nGrids = 2
    integer, parameter :: nDims = 3

    integer :: i, j, k, m
    real(rk) :: U, V, W
    integer, dimension(:,:), pointer :: nPoints
    real(rk), dimension(nGrids,nDims) :: Lengths
    real(rk), dimension(:,:,:,:,:), pointer :: X
    real(rk), dimension(:,:,:,:,:), pointer :: XCompare
    integer, dimension(:,:,:,:), pointer :: IBlank
    integer, dimension(:,:,:,:), pointer :: IBlankCompare
    character(len=80) :: HDF5File

    allocate(nPoints(nGrids,MAX_ND))

    nPoints = 1
    nPoints(1,:nDims) = [20,30,40]
    nPoints(2,:nDims) = [10,20,30]

    Lengths(1,:) = [2._rk, 3._rk, 4._rk]
    Lengths(2,:) = [1._rk, 2._rk, 3._rk]

    allocate(X(nGrids,maxval(nPoints(:,1)),maxval(nPoints(:,2)),maxval(nPoints(:,3)),nDims))
    X = 0._rk

    do m = 1, nGrids
      do k = 1, nPoints(m,3)
        do j = 1, nPoints(m,2)
          do i = 1, nPoints(m,1)
            U = real(i-1, kind=rk)/real(nPoints(m,1)-1, kind=rk)
            V = real(j-1, kind=rk)/real(nPoints(m,2)-1, kind=rk)
            W = real(k-1, kind=rk)/real(nPoints(m,3)-1, kind=rk)
            X(m,i,j,k,:) = Lengths(m,:) * ([U,V,W]-0.5_rk)
          end do
        end do
      end do
    end do

    allocate(IBlank(nGrids,maxval(nPoints(:,1)),maxval(nPoints(:,2)),maxval(nPoints(:,3))))
    IBlank = 1

    HDF5File = "grid_3d.h5"
    call WriteHDF5File(HDF5File, nDims=nDims, nGrids=nGrids, nPoints=nPoints, X=X, IBlank=IBlank)

    allocate(XCompare(nGrids,maxval(nPoints(:,1)),maxval(nPoints(:,2)),maxval(nPoints(:,3)),nDims))
    allocate(IBlankCompare(nGrids,maxval(nPoints(:,1)),maxval(nPoints(:,2)),maxval(nPoints(:,3))))

    ! Perturb the coordinates slightly
    XCompare = X
    do m = 1, nGrids
      do k = 1, nPoints(m,3)
        do j = 1, nPoints(m,2)
          do i = 1, nPoints(m,1)
            XCompare(m,i,j,k,:) = XCompare(m,i,j,k,:) + 1.e-10_rk
          end do
        end do
      end do
    end do

    HDF5File = "grid_3d_almost.h5"
    call WriteHDF5File(HDF5File, nDims=nDims, nGrids=nGrids, nPoints=nPoints, X=XCompare, IBlank=IBlank)

    ! Perturb the coordinates by a large amount
    XCompare = X
    do m = 1, nGrids
      do k = 1, nPoints(m,3)
        do j = 1, nPoints(m,2)
          do i = 1, nPoints(m,1)
            XCompare(m,i,j,k,:) = XCompare(m,i,j,k,:) + 1._rk
          end do
        end do
      end do
    end do

    HDF5File = "grid_3d_different_xyz.h5"
    call WriteHDF5File(HDF5File, nDims=nDims, nGrids=nGrids, nPoints=nPoints, X=XCompare, IBlank=IBlank)

    ! Modify the IBlank values
    IBlankCompare = 0

    HDF5File = "grid_3d_different_iblank.h5"
    call WriteHDF5File(HDF5File, nDims=nDims, nGrids=nGrids, nPoints=nPoints, X=X, IBlank=IBlankCompare)

    deallocate(X)
    deallocate(XCompare)
    deallocate(IBlank)
    deallocate(IBlankCompare)

  end subroutine CompareHDF5Grid3D

  subroutine CompareHDF5CV2D

    integer, parameter :: nGrids = 2
    integer, parameter :: nDims = 2

    integer :: i, j, m
    integer, dimension(:,:), pointer :: nPoints
    real(rk), dimension(:,:,:,:,:), pointer :: Q
    real(rk), dimension(:,:,:,:,:), pointer :: QCompare
    character(len=80) :: HDF5File

    allocate(nPoints(nGrids,MAX_ND))

    nPoints = 1
    nPoints(1,:nDims) = [20,30]
    nPoints(2,:nDims) = [10,20]

    allocate(Q(nGrids,maxval(nPoints(:,1)),maxval(nPoints(:,2)),maxval(nPoints(:,3)),nDims+2))
    Q = 0._rk

    do m = 1, nGrids
      do j = 1, nPoints(m,2)
        do i = 1, nPoints(m,1)
          Q(m,i,j,1,:) = [1._rk, 2._rk, 3._rk, 4._rk]
        end do
      end do
    end do

    HDF5File = "cv_2d.h5"
    call WriteHDF5File(HDF5File, nDims=nDims, nGrids=nGrids, nPoints=nPoints, Q=Q)

    allocate(QCompare(nGrids,maxval(nPoints(:,1)),maxval(nPoints(:,2)),maxval(nPoints(:,3)),nDims+2))

    ! Perturb the solution slightly
    QCompare = Q
    do m = 1, nGrids
      do j = 1, nPoints(m,2)
        do i = 1, nPoints(m,1)
          QCompare(m,i,j,1,:) = QCompare(m,i,j,1,:) + 1.e-10_rk
        end do
      end do
    end do

    HDF5File = "cv_2d_almost.h5"
    call WriteHDF5File(HDF5File, nDims=nDims, nGrids=nGrids, nPoints=nPoints, Q=QCompare)

    ! Perturb the density by a large amount
    QCompare = Q
    do m = 1, nGrids
      do j = 1, nPoints(m,2)
        do i = 1, nPoints(m,1)
          QCompare(m,i,j,1,1) = QCompare(m,i,j,1,1) + 1._rk
        end do
      end do
    end do

    HDF5File = "cv_2d_different_density.h5"
    call WriteHDF5File(HDF5File, nDims=nDims, nGrids=nGrids, nPoints=nPoints, Q=QCompare)

    ! Perturb the x momentum by a large amount
    QCompare = Q
    do m = 1, nGrids
      do j = 1, nPoints(m,2)
        do i = 1, nPoints(m,1)
          QCompare(m,i,j,1,2) = QCompare(m,i,j,1,2) + 1._rk
        end do
      end do
    end do

    HDF5File = "cv_2d_different_x_momentum.h5"
    call WriteHDF5File(HDF5File, nDims=nDims, nGrids=nGrids, nPoints=nPoints, Q=QCompare)

    ! Perturb the y momentum by a large amount
    QCompare = Q
    do m = 1, nGrids
      do j = 1, nPoints(m,2)
        do i = 1, nPoints(m,1)
          QCompare(m,i,j,1,3) = QCompare(m,i,j,1,3) + 1._rk
        end do
      end do
    end do

    HDF5File = "cv_2d_different_y_momentum.h5"
    call WriteHDF5File(HDF5File, nDims=nDims, nGrids=nGrids, nPoints=nPoints, Q=QCompare)

    ! Perturb the energy by a large amount
    QCompare = Q
    do m = 1, nGrids
      do j = 1, nPoints(m,2)
        do i = 1, nPoints(m,1)
          QCompare(m,i,j,1,4) = QCompare(m,i,j,1,4) + 1._rk
        end do
      end do
    end do

    HDF5File = "cv_2d_different_energy.h5"
    call WriteHDF5File(HDF5File, nDims=nDims, nGrids=nGrids, nPoints=nPoints, Q=QCompare)

    deallocate(Q)
    deallocate(QCompare)

  end subroutine CompareHDF5CV2D

  subroutine CompareHDF5CV3D

    integer, parameter :: nGrids = 2
    integer, parameter :: nDims = 3

    integer :: i, j, k, m
    integer, dimension(:,:), pointer :: nPoints
    real(rk), dimension(:,:,:,:,:), pointer :: Q
    real(rk), dimension(:,:,:,:,:), pointer :: QCompare
    character(len=80) :: HDF5File

    allocate(nPoints(nGrids,MAX_ND))

    nPoints = 1
    nPoints(1,:nDims) = [20,30,40]
    nPoints(2,:nDims) = [10,20,30]

    allocate(Q(nGrids,maxval(nPoints(:,1)),maxval(nPoints(:,2)),maxval(nPoints(:,3)),nDims+2))
    Q = 0._rk

    do m = 1, nGrids
      do k = 1, nPoints(m,3)
        do j = 1, nPoints(m,2)
          do i = 1, nPoints(m,1)
            Q(m,i,j,k,:) = [1._rk, 2._rk, 3._rk, 4._rk, 5._rk]
          end do
        end do
      end do
    end do

    HDF5File = "cv_3d.h5"
    call WriteHDF5File(HDF5File, nDims=nDims, nGrids=nGrids, nPoints=nPoints, Q=Q)

    allocate(QCompare(nGrids,maxval(nPoints(:,1)),maxval(nPoints(:,2)),maxval(nPoints(:,3)),nDims+2))

    ! Perturb the solution slightly
    QCompare = Q
    do m = 1, nGrids
      do j = 1, nPoints(m,2)
        do i = 1, nPoints(m,1)
          QCompare(m,i,j,1,:) = QCompare(m,i,j,1,:) + 1.e-10_rk
        end do
      end do
    end do

    HDF5File = "cv_3d_almost.h5"
    call WriteHDF5File(HDF5File, nDims=nDims, nGrids=nGrids, nPoints=nPoints, Q=QCompare)

    ! Perturb the density by a large amount
    QCompare = Q
    do m = 1, nGrids
      do k = 1, nPoints(m,3)
        do j = 1, nPoints(m,2)
          do i = 1, nPoints(m,1)
            QCompare(m,i,j,k,1) = QCompare(m,i,j,k,1) + 1._rk
          end do
        end do
      end do
    end do

    HDF5File = "cv_3d_different_density.h5"
    call WriteHDF5File(HDF5File, nDims=nDims, nGrids=nGrids, nPoints=nPoints, Q=QCompare)

    ! Perturb the x momentum by a large amount
    QCompare = Q
    do m = 1, nGrids
      do k = 1, nPoints(m,3)
        do j = 1, nPoints(m,2)
          do i = 1, nPoints(m,1)
            QCompare(m,i,j,k,2) = QCompare(m,i,j,k,2) + 1._rk
          end do
        end do
      end do
    end do

    HDF5File = "cv_3d_different_x_momentum.h5"
    call WriteHDF5File(HDF5File, nDims=nDims, nGrids=nGrids, nPoints=nPoints, Q=QCompare)

    ! Perturb the y momentum by a large amount
    QCompare = Q
    do m = 1, nGrids
      do k = 1, nPoints(m,3)
        do j = 1, nPoints(m,2)
          do i = 1, nPoints(m,1)
            QCompare(m,i,j,k,3) = QCompare(m,i,j,k,3) + 1._rk
          end do
        end do
      end do
    end do

    HDF5File = "cv_3d_different_y_momentum.h5"
    call WriteHDF5File(HDF5File, nDims=nDims, nGrids=nGrids, nPoints=nPoints, Q=QCompare)

    ! Perturb the energy by a large amount
    QCompare = Q
    do m = 1, nGrids
      do k = 1, nPoints(m,3)
        do j = 1, nPoints(m,2)
          do i = 1, nPoints(m,1)
            QCompare(m,i,j,k,4) = QCompare(m,i,j,k,4) + 1._rk
          end do
        end do
      end do
    end do

    HDF5File = "cv_3d_different_z_momentum.h5"
    call WriteHDF5File(HDF5File, nDims=nDims, nGrids=nGrids, nPoints=nPoints, Q=QCompare)

    ! Perturb the energy by a large amount
    QCompare = Q
    do m = 1, nGrids
      do k = 1, nPoints(m,3)
        do j = 1, nPoints(m,2)
          do i = 1, nPoints(m,1)
            QCompare(m,i,j,k,5) = QCompare(m,i,j,k,5) + 1._rk
          end do
        end do
      end do
    end do

    HDF5File = "cv_3d_different_energy.h5"
    call WriteHDF5File(HDF5File, nDims=nDims, nGrids=nGrids, nPoints=nPoints, Q=QCompare)

    deallocate(Q)
    deallocate(QCompare)

  end subroutine CompareHDF5CV3D

  subroutine CompareHDF5CVTarget2D

    integer, parameter :: nGrids = 2
    integer, parameter :: nDims = 2

    integer :: i, j, m
    integer, dimension(:,:), pointer :: nPoints
    real(rk), dimension(:,:,:,:,:), pointer :: Q
    real(rk), dimension(:,:,:,:,:), pointer :: QCompare
    character(len=80) :: HDF5File

    allocate(nPoints(nGrids,MAX_ND))

    nPoints = 1
    nPoints(1,:nDims) = [20,30]
    nPoints(2,:nDims) = [10,20]

    allocate(Q(nGrids,maxval(nPoints(:,1)),maxval(nPoints(:,2)),maxval(nPoints(:,3)),nDims+2))
    Q = 0._rk

    do m = 1, nGrids
      do j = 1, nPoints(m,2)
        do i = 1, nPoints(m,1)
          Q(m,i,j,1,:) = [1._rk, 2._rk, 3._rk, 4._rk]
        end do
      end do
    end do

    HDF5File = "cvt_2d.h5"
    call WriteHDF5File(HDF5File, nDims=nDims, nGrids=nGrids, nPoints=nPoints, QTarget=Q)

    allocate(QCompare(nGrids,maxval(nPoints(:,1)),maxval(nPoints(:,2)),maxval(nPoints(:,3)),nDims+2))

    ! Perturb the solution slightly
    QCompare = Q
    do m = 1, nGrids
      do j = 1, nPoints(m,2)
        do i = 1, nPoints(m,1)
          QCompare(m,i,j,1,:) = QCompare(m,i,j,1,:) + 1.e-10_rk
        end do
      end do
    end do

    HDF5File = "cvt_2d_almost.h5"
    call WriteHDF5File(HDF5File, nDims=nDims, nGrids=nGrids, nPoints=nPoints, QTarget=QCompare)

    ! Perturb the density by a large amount
    QCompare = Q
    do m = 1, nGrids
      do j = 1, nPoints(m,2)
        do i = 1, nPoints(m,1)
          QCompare(m,i,j,1,1) = QCompare(m,i,j,1,1) + 1._rk
        end do
      end do
    end do

    HDF5File = "cvt_2d_different_density.h5"
    call WriteHDF5File(HDF5File, nDims=nDims, nGrids=nGrids, nPoints=nPoints, QTarget=QCompare)

    ! Perturb the x momentum by a large amount
    QCompare = Q
    do m = 1, nGrids
      do j = 1, nPoints(m,2)
        do i = 1, nPoints(m,1)
          QCompare(m,i,j,1,2) = QCompare(m,i,j,1,2) + 1._rk
        end do
      end do
    end do

    HDF5File = "cvt_2d_different_x_momentum.h5"
    call WriteHDF5File(HDF5File, nDims=nDims, nGrids=nGrids, nPoints=nPoints, QTarget=QCompare)

    ! Perturb the y momentum by a large amount
    QCompare = Q
    do m = 1, nGrids
      do j = 1, nPoints(m,2)
        do i = 1, nPoints(m,1)
          QCompare(m,i,j,1,3) = QCompare(m,i,j,1,3) + 1._rk
        end do
      end do
    end do

    HDF5File = "cvt_2d_different_y_momentum.h5"
    call WriteHDF5File(HDF5File, nDims=nDims, nGrids=nGrids, nPoints=nPoints, QTarget=QCompare)

    ! Perturb the energy by a large amount
    QCompare = Q
    do m = 1, nGrids
      do j = 1, nPoints(m,2)
        do i = 1, nPoints(m,1)
          QCompare(m,i,j,1,4) = QCompare(m,i,j,1,4) + 1._rk
        end do
      end do
    end do

    HDF5File = "cvt_2d_different_energy.h5"
    call WriteHDF5File(HDF5File, nDims=nDims, nGrids=nGrids, nPoints=nPoints, QTarget=QCompare)

    deallocate(Q)
    deallocate(QCompare)

  end subroutine CompareHDF5CVTarget2D

  subroutine CompareHDF5CVTarget3D

    integer, parameter :: nGrids = 2
    integer, parameter :: nDims = 3

    integer :: i, j, k, m
    integer, dimension(:,:), pointer :: nPoints
    real(rk), dimension(:,:,:,:,:), pointer :: Q
    real(rk), dimension(:,:,:,:,:), pointer :: QCompare
    character(len=80) :: HDF5File

    allocate(nPoints(nGrids,MAX_ND))

    nPoints = 1
    nPoints(1,:nDims) = [20,30,40]
    nPoints(2,:nDims) = [10,20,30]

    allocate(Q(nGrids,maxval(nPoints(:,1)),maxval(nPoints(:,2)),maxval(nPoints(:,3)),nDims+2))
    Q = 0._rk

    do m = 1, nGrids
      do k = 1, nPoints(m,3)
        do j = 1, nPoints(m,2)
          do i = 1, nPoints(m,1)
            Q(m,i,j,k,:) = [1._rk, 2._rk, 3._rk, 4._rk, 5._rk]
          end do
        end do
      end do
    end do

    HDF5File = "cvt_3d.h5"
    call WriteHDF5File(HDF5File, nDims=nDims, nGrids=nGrids, nPoints=nPoints, QTarget=Q)

    allocate(QCompare(nGrids,maxval(nPoints(:,1)),maxval(nPoints(:,2)),maxval(nPoints(:,3)),nDims+2))

    ! Perturb the solution slightly
    QCompare = Q
    do m = 1, nGrids
      do j = 1, nPoints(m,2)
        do i = 1, nPoints(m,1)
          QCompare(m,i,j,1,:) = QCompare(m,i,j,1,:) + 1.e-10_rk
        end do
      end do
    end do

    HDF5File = "cvt_3d_almost.h5"
    call WriteHDF5File(HDF5File, nDims=nDims, nGrids=nGrids, nPoints=nPoints, QTarget=QCompare)

    ! Perturb the density by a large amount
    QCompare = Q
    do m = 1, nGrids
      do k = 1, nPoints(m,3)
        do j = 1, nPoints(m,2)
          do i = 1, nPoints(m,1)
            QCompare(m,i,j,k,1) = QCompare(m,i,j,k,1) + 1._rk
          end do
        end do
      end do
    end do

    HDF5File = "cvt_3d_different_density.h5"
    call WriteHDF5File(HDF5File, nDims=nDims, nGrids=nGrids, nPoints=nPoints, QTarget=QCompare)

    ! Perturb the x momentum by a large amount
    QCompare = Q
    do m = 1, nGrids
      do k = 1, nPoints(m,3)
        do j = 1, nPoints(m,2)
          do i = 1, nPoints(m,1)
            QCompare(m,i,j,k,2) = QCompare(m,i,j,k,2) + 1._rk
          end do
        end do
      end do
    end do

    HDF5File = "cvt_3d_different_x_momentum.h5"
    call WriteHDF5File(HDF5File, nDims=nDims, nGrids=nGrids, nPoints=nPoints, QTarget=QCompare)

    ! Perturb the y momentum by a large amount
    QCompare = Q
    do m = 1, nGrids
      do k = 1, nPoints(m,3)
        do j = 1, nPoints(m,2)
          do i = 1, nPoints(m,1)
            QCompare(m,i,j,k,3) = QCompare(m,i,j,k,3) + 1._rk
          end do
        end do
      end do
    end do

    HDF5File = "cvt_3d_different_y_momentum.h5"
    call WriteHDF5File(HDF5File, nDims=nDims, nGrids=nGrids, nPoints=nPoints, QTarget=QCompare)

    ! Perturb the energy by a large amount
    QCompare = Q
    do m = 1, nGrids
      do k = 1, nPoints(m,3)
        do j = 1, nPoints(m,2)
          do i = 1, nPoints(m,1)
            QCompare(m,i,j,k,4) = QCompare(m,i,j,k,4) + 1._rk
          end do
        end do
      end do
    end do

    HDF5File = "cvt_3d_different_z_momentum.h5"
    call WriteHDF5File(HDF5File, nDims=nDims, nGrids=nGrids, nPoints=nPoints, QTarget=QCompare)

    ! Perturb the energy by a large amount
    QCompare = Q
    do m = 1, nGrids
      do k = 1, nPoints(m,3)
        do j = 1, nPoints(m,2)
          do i = 1, nPoints(m,1)
            QCompare(m,i,j,k,5) = QCompare(m,i,j,k,5) + 1._rk
          end do
        end do
      end do
    end do

    HDF5File = "cvt_3d_different_energy.h5"
    call WriteHDF5File(HDF5File, nDims=nDims, nGrids=nGrids, nPoints=nPoints, QTarget=QCompare)

    deallocate(Q)
    deallocate(QCompare)

  end subroutine CompareHDF5CVTarget3D

  subroutine CompareHDF5Aux2D

    integer, parameter :: nGrids = 2
    integer, parameter :: nDims = 2

    integer :: i, j, m
    integer, dimension(:,:), pointer :: nPoints
    real(rk), dimension(:,:,:,:,:), pointer :: QAux
    real(rk), dimension(:,:,:,:,:), pointer :: QAuxCompare
    character(len=80) :: HDF5File

    allocate(nPoints(nGrids,MAX_ND))

    nPoints = 1
    nPoints(1,:nDims) = [20,30]
    nPoints(2,:nDims) = [10,20]

    allocate(QAux(nGrids,maxval(nPoints(:,1)),maxval(nPoints(:,2)),maxval(nPoints(:,3)),2))
    QAux = 0._rk

    do m = 1, nGrids
      do j = 1, nPoints(m,2)
        do i = 1, nPoints(m,1)
          QAux(m,i,j,1,:) = [1._rk, 2._rk]
        end do
      end do
    end do

    HDF5File = "aux_2d.h5"
    call WriteHDF5File(HDF5File, nDims=nDims, nGrids=nGrids, nPoints=nPoints, QAux=QAux)

    allocate(QAuxCompare(nGrids,maxval(nPoints(:,1)),maxval(nPoints(:,2)),maxval(nPoints(:,3)),2))

    ! Perturb the values slightly
    QAuxCompare = QAux
    do m = 1, nGrids
      do j = 1, nPoints(m,2)
        do i = 1, nPoints(m,1)
          QAuxCompare(m,i,j,1,:) = QAuxCompare(m,i,j,1,:) + 1.e-10_rk
        end do
      end do
    end do

    HDF5File = "aux_2d_almost.h5"
    call WriteHDF5File(HDF5File, nDims=nDims, nGrids=nGrids, nPoints=nPoints, QAux=QAuxCompare)

    ! Perturb the values by a large amount
    QAuxCompare = QAux
    do m = 1, nGrids
      do j = 1, nPoints(m,2)
        do i = 1, nPoints(m,1)
          QAuxCompare(m,i,j,1,:) = QAuxCompare(m,i,j,1,:) + 1._rk
        end do
      end do
    end do

    HDF5File = "aux_2d_different.h5"
    call WriteHDF5File(HDF5File, nDims=nDims, nGrids=nGrids, nPoints=nPoints, QAux=QAuxCompare)

    deallocate(QAux)
    deallocate(QAuxCompare)

  end subroutine CompareHDF5Aux2D

  subroutine CompareHDF5Aux3D

    integer, parameter :: nGrids = 2
    integer, parameter :: nDims = 3

    integer :: i, j, k, m
    integer, dimension(:,:), pointer :: nPoints
    real(rk), dimension(:,:,:,:,:), pointer :: QAux
    real(rk), dimension(:,:,:,:,:), pointer :: QAuxCompare
    character(len=80) :: HDF5File

    allocate(nPoints(nGrids,MAX_ND))

    nPoints = 1
    nPoints(1,:nDims) = [20,30,40]
    nPoints(2,:nDims) = [10,20,30]

    allocate(QAux(nGrids,maxval(nPoints(:,1)),maxval(nPoints(:,2)),maxval(nPoints(:,3)),2))
    QAux = 0._rk

    do m = 1, nGrids
      do k = 1, nPoints(m,3)
        do j = 1, nPoints(m,2)
          do i = 1, nPoints(m,1)
            QAux(m,i,j,k,:) = [1._rk, 2._rk]
          end do
        end do
      end do
    end do

    HDF5File = "aux_3d.h5"
    call WriteHDF5File(HDF5File, nDims=nDims, nGrids=nGrids, nPoints=nPoints, QAux=QAux)

    allocate(QAuxCompare(nGrids,maxval(nPoints(:,1)),maxval(nPoints(:,2)),maxval(nPoints(:,3)),2))

    ! Perturb the values slightly
    QAuxCompare = QAux
    do m = 1, nGrids
      do k = 1, nPoints(m,3)
        do j = 1, nPoints(m,2)
          do i = 1, nPoints(m,1)
            QAuxCompare(m,i,j,k,:) = QAuxCompare(m,i,j,k,:) + 1.e-10_rk
          end do
        end do
      end do
    end do

    HDF5File = "aux_3d_almost.h5"
    call WriteHDF5File(HDF5File, nDims=nDims, nGrids=nGrids, nPoints=nPoints, QAux=QAuxCompare)

    ! Perturb the values by a large amount
    QAuxCompare = QAux
    do m = 1, nGrids
      do k = 1, nPoints(m,3)
        do j = 1, nPoints(m,2)
          do i = 1, nPoints(m,1)
            QAuxCompare(m,i,j,k,:) = QAuxCompare(m,i,j,k,:) + 1._rk
          end do
        end do
      end do
    end do

    HDF5File = "aux_3d_different.h5"
    call WriteHDF5File(HDF5File, nDims=nDims, nGrids=nGrids, nPoints=nPoints, QAux=QAuxCompare)

    deallocate(QAux)
    deallocate(QAuxCompare)

  end subroutine CompareHDF5Aux3D

  subroutine CompareHDF5AuxTarget2D

    integer, parameter :: nGrids = 2
    integer, parameter :: nDims = 2

    integer :: i, j, m
    integer, dimension(:,:), pointer :: nPoints
    real(rk), dimension(:,:,:,:,:), pointer :: QAux
    real(rk), dimension(:,:,:,:,:), pointer :: QAuxCompare
    character(len=80) :: HDF5File

    allocate(nPoints(nGrids,MAX_ND))

    nPoints = 1
    nPoints(1,:nDims) = [20,30]
    nPoints(2,:nDims) = [10,20]

    allocate(QAux(nGrids,maxval(nPoints(:,1)),maxval(nPoints(:,2)),maxval(nPoints(:,3)),2))
    QAux = 0._rk

    do m = 1, nGrids
      do j = 1, nPoints(m,2)
        do i = 1, nPoints(m,1)
          QAux(m,i,j,1,:) = [1._rk, 2._rk]
        end do
      end do
    end do

    HDF5File = "aut_2d.h5"
    call WriteHDF5File(HDF5File, nDims=nDims, nGrids=nGrids, nPoints=nPoints, QAuxTarget=QAux)

    allocate(QAuxCompare(nGrids,maxval(nPoints(:,1)),maxval(nPoints(:,2)),maxval(nPoints(:,3)),2))

    ! Perturb the values slightly
    QAuxCompare = QAux
    do m = 1, nGrids
      do j = 1, nPoints(m,2)
        do i = 1, nPoints(m,1)
          QAuxCompare(m,i,j,1,:) = QAuxCompare(m,i,j,1,:) + 1.e-10_rk
        end do
      end do
    end do

    HDF5File = "aut_2d_almost.h5"
    call WriteHDF5File(HDF5File, nDims=nDims, nGrids=nGrids, nPoints=nPoints, QAuxTarget=QAuxCompare)

    ! Perturb the values by a large amount
    QAuxCompare = QAux
    do m = 1, nGrids
      do j = 1, nPoints(m,2)
        do i = 1, nPoints(m,1)
          QAuxCompare(m,i,j,1,:) = QAuxCompare(m,i,j,1,:) + 1._rk
        end do
      end do
    end do

    HDF5File = "aut_2d_different.h5"
    call WriteHDF5File(HDF5File, nDims=nDims, nGrids=nGrids, nPoints=nPoints, QAuxTarget=QAuxCompare)

    deallocate(QAux)
    deallocate(QAuxCompare)

  end subroutine CompareHDF5AuxTarget2D

  subroutine CompareHDF5AuxTarget3D

    integer, parameter :: nGrids = 2
    integer, parameter :: nDims = 3

    integer :: i, j, k, m
    integer, dimension(:,:), pointer :: nPoints
    real(rk), dimension(:,:,:,:,:), pointer :: QAux
    real(rk), dimension(:,:,:,:,:), pointer :: QAuxCompare
    character(len=80) :: HDF5File

    allocate(nPoints(nGrids,MAX_ND))

    nPoints = 1
    nPoints(1,:nDims) = [20,30,40]
    nPoints(2,:nDims) = [10,20,30]

    allocate(QAux(nGrids,maxval(nPoints(:,1)),maxval(nPoints(:,2)),maxval(nPoints(:,3)),2))
    QAux = 0._rk

    do m = 1, nGrids
      do k = 1, nPoints(m,3)
        do j = 1, nPoints(m,2)
          do i = 1, nPoints(m,1)
            QAux(m,i,j,k,:) = [1._rk, 2._rk]
          end do
        end do
      end do
    end do

    HDF5File = "aut_3d.h5"
    call WriteHDF5File(HDF5File, nDims=nDims, nGrids=nGrids, nPoints=nPoints, QAuxTarget=QAux)

    allocate(QAuxCompare(nGrids,maxval(nPoints(:,1)),maxval(nPoints(:,2)),maxval(nPoints(:,3)),2))

    ! Perturb the values slightly
    QAuxCompare = QAux
    do m = 1, nGrids
      do k = 1, nPoints(m,3)
        do j = 1, nPoints(m,2)
          do i = 1, nPoints(m,1)
            QAuxCompare(m,i,j,k,:) = QAuxCompare(m,i,j,k,:) + 1.e-10_rk
          end do
        end do
      end do
    end do

    HDF5File = "aut_3d_almost.h5"
    call WriteHDF5File(HDF5File, nDims=nDims, nGrids=nGrids, nPoints=nPoints, QAuxTarget=QAuxCompare)

    ! Perturb the values by a large amount
    QAuxCompare = QAux
    do m = 1, nGrids
      do k = 1, nPoints(m,3)
        do j = 1, nPoints(m,2)
          do i = 1, nPoints(m,1)
            QAuxCompare(m,i,j,k,:) = QAuxCompare(m,i,j,k,:) + 1._rk
          end do
        end do
      end do
    end do

    HDF5File = "aut_3d_different.h5"
    call WriteHDF5File(HDF5File, nDims=nDims, nGrids=nGrids, nPoints=nPoints, QAuxTarget=QAuxCompare)

    deallocate(QAux)
    deallocate(QAuxCompare)

  end subroutine CompareHDF5AuxTarget3D

end program CompareTestHDF5Data
