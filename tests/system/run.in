#! /bin/sh
# Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
# License: MIT, http://opensource.org/licenses/MIT

# Fail test if a command results in a nonzero exit code
set -e

# Configuration stuff
abs_top_builddir="@abs_top_builddir@"
logfilename__=run.log
logfilepath__="$abs_top_builddir/tests/system/$logfilename__"

#===============================================
# Functions used by test runner and test suites
#===============================================

PrintOverallSuccess() {

  printf "%s\n" "${statuscolor__}[ PASSED ]${normalcolor__} $1"

  if [ $verbose__ -eq 0 ]; then
    echo "[ PASSED ] $1" >> "$logfilepath__"
  fi

}

PrintOverallFailure() {

  printf "%s\n" "${errorcolor__}[ FAILED ]${normalcolor__} $1" 1>&2

  if [ $verbose__ -eq 0 ]; then
    echo "[ FAILED ] $1" >> "$logfilepath__"
  fi

}

PrintSuiteStatus() {

  printf "%s\n" "${statuscolor__}[========]${normalcolor__} $1"

  if [ $verbose__ -eq 0 ]; then
    echo "[========] $1" >> "$logfilepath__"
  fi

}

PrintSuiteFailure() {

  printf "%s\n" "${errorcolor__}[========]${normalcolor__} $1" 1>&2

  if [ $verbose__ -eq 0 ]; then
    echo "[========] $1" >> "$logfilepath__"
  fi

}

PrintTestStatus() {

  printf "%s\n" "${statuscolor__}[ ------ ]${normalcolor__} $1"

  if [ $verbose__ -eq 0 ]; then
    echo "[ ------ ] $1" >> "$logfilepath__"
  fi

}

PrintTestFailure() {

  printf "%s\n" "${errorcolor__}[ ------ ]${normalcolor__} $1" 1>&2

  if [ $verbose__ -eq 0 ]; then
    echo "[ ------ ] $1" >> "$logfilepath__"
  fi

}

PrintError() {

  printf "%s\n" "${errorcolor__}[ ****** ]${normalcolor__} $1" 1>&2

  if [ $verbose__ -eq 0 ]; then
    echo "[ ****** ] $1" >> "$logfilepath__"
  fi

}

AddTest() {

  tests__="$tests__ $1"
  numtests__=$((numtests__ + 1))

}

FailTest() {

  exit 1

}

# Execute a command, respecting verbose options
Exec() {

  result__=0
  if [ $verbose__ -eq 1 ]; then
    $@ || result__=$?
  else
    $@ >>"$logfilepath__" 2>&1 || result__=$?
  fi

  if [ $result__ -ne 0 ]; then
    PrintError "Exec: Executed command produced nonzero exit code"
    FailTest
  fi

}

# Same as Exec, but doesn't fail test if command fails (instead saves exit code in 'RESULT')
TryExec() {

  RESULT=0
  if [ $verbose__ -eq 1 ]; then
    $@ || RESULT=$?
  else
    $@ >>"$logfilepath__" 2>&1 || RESULT=$?
  fi

}

SetJobTimeLimit() {

  time_components__=$(echo "$@" | tr ':hms' ' ')

  num_time_components__=0
  for time_component__ in $time_components__; do
    time_components_reversed__="$time_component__ $time_components_reversed__"
    num_time_components__=$((num_time_components__+1))
  done

  jobtimelimit_h__=0
  jobtimelimit_m__=0
  jobtimelimit_s__=0

  i=1
  for time_component__ in $time_components_reversed__; do
    if [ $i -eq 1 ]; then
      jobtimelimit_s__=$time_component__
    elif [ $i -eq 2 ]; then
      jobtimelimit_m__=$time_component__
    elif [ $i -eq 3 ]; then
      jobtimelimit_h__=$time_component__
    fi
    i=$((i+1))
  done

}

ExecJob() {

  timelimit__=$jobtimelimit_h__:$jobtimelimit_m__:$jobtimelimit_s__

  if [ "${mpiexec__#*srun}" != "${mpiexec__}" ]; then
    extra_args_job__="$extra_args -t $timelimit__"
  fi

  nprocs__=$1
  shift
  command__="$@"

  Exec "$mpiexec__" -n $nprocs__ $extra_args_job__ $command__

}

TryExecJob() {

  timelimit__=$jobtimelimit_h__:$jobtimelimit_m__:$jobtimelimit_s__

  if [ "${mpiexec__#*srun}" != "${mpiexec__}" ]; then
    extra_args_job__="$extra_args -t $timelimit__"
  fi

  nprocs__=$1
  shift
  command__="$@"

  TryExec "$mpiexec__" -n $nprocs__ $extra_args_job__ $command__

}

# Load additional functions
. "$abs_top_builddir/tests/system/testdefs"

#=============
# Test runner
#=============

verbose__=0
strict__=0
statuscolor__=
errorcolor__=
normalcolor__=

OPTIND=1
while getopts ":csv" opt__; do
  case $opt__ in
    c)
      statuscolor__=$(tput setaf 2)
      errorcolor__=$(tput setaf 1)
      normalcolor__=$(tput sgr0)
      ;;
    s)
      strict__=1;;
    v)
      verbose__=1;;
    *)
      echo "Invalid option -$OPTARG" 1>&2
      exit 1
      ;;
  esac
done
shift $((OPTIND-1))

rm -f "$logfilepath__"
if [ $verbose__ -eq 0 ]; then
  touch "$logfilepath__"
fi

suitefiles__=
for arg__ in "$@"; do
  suitefiles__="$suitefiles__ $arg__"
done
overallfailed__=0
numcompletedtests__=0

for suitefile__ in $suitefiles__; do

  suite__="${suitefile__##*/}"
  suitefailed__=0

  tests__=
  numtests__=0

  . "$suitefile__"

  if [ $numtests__ -eq 1 ]; then
    PrintSuiteStatus "Suite $suite__ (1 test)"
  else
    PrintSuiteStatus "Suite $suite__ ($numtests__ tests)"
  fi

  for test__ in $tests__; do

    PrintTestStatus "Running test $suite__.$test__..."

    rundir__="$abs_top_builddir/tests/system/.run_temp"
    rm -rf "$rundir__" && mkdir "$rundir__"

    SUITE=$suite__
    TEST=$test__

    jobtimelimit_h__=0
    jobtimelimit_m__=10
    jobtimelimit_s__=0
    testfailed__=0
    (cd "$rundir__" && $test__) || testfailed__=1

    if [ $testfailed__ -eq 0 ]; then
      rm -rf "$rundir__"
    fi

    if [ $testfailed__ -eq 0 ]; then
      PrintTestStatus "$suite__.$test__ passed"
    else
      PrintTestFailure "$suite__.$test__ failed"
      suitefailed__=1
      if [ $strict__ -eq 1 ]; then
        break
      fi
    fi

    numcompletedtests__=$((numcompletedtests__ + 1))

  done

  if [ $suitefailed__ -eq 0 ]; then
    PrintSuiteStatus "Suite $suite__ passed"
  else
    PrintSuiteFailure "Suite $suite__ failed"
    overallfailed__=1
    if [ $strict__ -eq 1 ]; then
      break
    fi
  fi

done

if [ $overallfailed__ -eq 0 ]; then
  if [ $numcompletedtests__ -eq 1 ]; then
    PrintOverallSuccess "1 test passed"
  else
    PrintOverallSuccess "$numcompletedtests__ tests passed"
  fi
else
  if [ $verbose__ -eq 1 ]; then
    PrintOverallFailure "One or more tests failed"
  else
    PrintOverallFailure "One or more tests failed (see $logfilename__ for details)"
  fi
  exit 1
fi
