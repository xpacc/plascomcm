-pc_type hypre
-pc_hypre_type boomeramg

#-on_error_attach_debugger
#-start_in_debugger
#-debugger_nodes 2
#-ksp_view
#-ksp_converged_reason
#-ksp_monitor_true_residual
-ksp_atol 1e-8
-ksp_rtol 1e-50
-ksp_divtol 1e+40
