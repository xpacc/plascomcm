# Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
# License: MIT, http://opensource.org/licenses/MIT
unit_tests_pf_src = \
  ExampleTest.pf \
  SplineTest.pf
