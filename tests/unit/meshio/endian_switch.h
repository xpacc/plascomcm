#include <stdlib.h>
#include <assert.h>
#include "mpi.h"


/* Switch the endian-ness of some of the elements in an N-dimensional grid.
   full_mesh describes the total size of the grid.
   sub_mesh describes the part of the grid that is to be modified.

   buf - address of the first element in the full grid
   element_size - number of bytes in each element
     If 1, the function will do nothing.
     If less than 1 or (greater than one and odd), behavior is undefined.
   data_is_big_endian - nonzero iff the data is in big-endian form.
     If the current architecture matches the endian-ness of the data,
     the function will do nothing.
   ndims - number of array dimensions
   full_mesh_sizes - number of elements in each dimension of the full mesh
     in which the sub-mesh is stored.
   sub_mesh_sizes - number of elements in each dimension of the mesh
     that is to have the endian-ness corrected.
   sub_mesh_starts - staring coordinates of the sub-mesh in each dimension
   array_order - storage order of the mesh in memory.
     MPI_ORDER_C for row-major, MPI_ORDER_FORTRAN for column-major.
*/
void endianSwitch
(void *buf,
 size_t element_size, 
 int data_is_big_endian, 
 int ndims,
 const int *full_mesh_sizes,
 const int *sub_mesh_sizes, 
 const int *sub_mesh_starts, 
 int array_order);
