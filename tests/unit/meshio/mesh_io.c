#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "mesh_io.h"
#include "reverse_bytes.h"
#include "endian_switch.h"


/* Returns nonzero iff t is a type supported by Mesh_IO_read() */
static int isSupportedType(MPI_Datatype t);

/* Read an n-dimensional mesh from a file.

   fh - Handle of the file from which data will be read.
   offset - Offset (in bytes) from the beginning of the file where the first
     byte of the full mesh can be found.
   etype - datatype of each element in the mesh. Only basic datatypes
     are supported.
   file_is_big_endian - boolean flag, nonzero iff the data in the file
     is in big-endian byte order. If this does not match the CPU format,
     the data will be byte-swapped.
   ndims - number of array dimensions (positive integer)
   buf - location of the mesh in memory
   mesh_sizes - number of elements in each dimension of the mesh being read
     (array of positive integers). These elements will be a subset of the
   file_mesh_sizes - number of elements in each dimension of the mesh
     as it is stored in the file.
   file_mesh_starts - number of elements in each dimension by which the
     submesh being read is inset from the origin of the file mesh.
   file_array_order - storage order of the mesh on disk.
     MPI_ORDER_C for row-major, MPI_ORDER_FORTRAN for column-major.
   memory_mesh_sizes - number of elements in each dimension of the full
     in-memory mesh
   memory_mesh_starts - number of elements in each dimension by which the
     submesh being written to memory is inset from the origin of the
     memory mesh.
   memory_array_order - storage order of the mesh in memory.
     MPI_ORDER_C for row-major, MPI_ORDER_FORTRAN for column-major.

  Returns:
    MPI_SUCCESS on success
    MPI_ERR_TYPE if etype is not a supported datatype
    MPI_ERR_BUFFER if buf is NULL
    MPI_ERR_DIMS if ndims is nonpositive
    MPI_ERR_ARG if any of the array bounds are invalid or the datatype
      has a nonpositive size or odd size (other than 1).
    MPI_ERR_TRUNCATE if no data is read from the file
    If any MPI call fails, this returns the error code from that call.
*/
int Mesh_IO_read
(MPI_File fh,
 MPI_Offset offset,
 MPI_Datatype etype,
 int file_is_big_endian,
 void *buf,
 int ndims,
 const int *mesh_sizes,
 const int *file_mesh_sizes,
 const int *file_mesh_starts,
 int file_array_order,
 const int *memory_mesh_sizes,
 const int *memory_mesh_starts,
 int memory_array_order) {

  int i, err = MPI_SUCCESS, rank;
  MPI_Datatype file_type, memory_type;
  size_t element_size;
  MPI_Status status;

  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  
  /* check for argument errors */
  if (ndims < 1) return MPI_ERR_DIMS;

  /* check for a basic datatype */
  if (!isSupportedType(etype)) return MPI_ERR_TYPE;

  /* check for mesh boundary errors */
  for (i=0; i < ndims; i++) {
    if (file_mesh_starts[i] < 0
        || file_mesh_starts[i] + mesh_sizes[i] > file_mesh_sizes[i]
        || memory_mesh_starts[i] < 0
        || memory_mesh_starts[i] + mesh_sizes[i] > memory_mesh_sizes[i]
        || !(file_array_order == MPI_ORDER_FORTRAN ||
             file_array_order == MPI_ORDER_C)
        || !(memory_array_order == MPI_ORDER_FORTRAN ||
             memory_array_order == MPI_ORDER_C)
        )
      return MPI_ERR_ARG;
  }

  /* get the size of each element */
  MPI_Type_size(etype, &i);
  element_size = i;
  /* complain if it's zero, negative, or odd (other than 1) */
  if (element_size < 1 || (element_size > 1 && (element_size & 1)))
    return MPI_ERR_ARG;
  
  /* define the shape of the mesh on disk (with no offset) */
  /* printf("[%02d] on disk subarray {%d,%d,%d} {%d,%d,%d} {%d,%d,%d}\n", rank,
         file_mesh_sizes[0], file_mesh_sizes[1], file_mesh_sizes[2],
         mesh_sizes[0], mesh_sizes[1], mesh_sizes[2],
         file_mesh_starts[0], file_mesh_starts[1], file_mesh_starts[2]); */
  err = MPI_Type_create_subarray
    (ndims, file_mesh_sizes, mesh_sizes, file_mesh_starts, file_array_order,
     etype, &file_type);
  if (err != MPI_SUCCESS) goto fail0;

  err = MPI_Type_commit(&file_type);
  if (err != MPI_SUCCESS) goto fail1;

  /* set the file view */
  err = MPI_File_set_view
    (fh, offset, file_type, file_type, "native", MPI_INFO_NULL);
  if (err != MPI_SUCCESS) goto fail1;

  /* define the shape of the mesh in memory */
  /* printf("[%02d] in memory subarray {%d,%d,%d} {%d,%d,%d} {%d,%d,%d}\n", rank,
         memory_mesh_sizes[0], memory_mesh_sizes[1], memory_mesh_sizes[2],
         mesh_sizes[0], mesh_sizes[1], mesh_sizes[2],
         memory_mesh_starts[0], memory_mesh_starts[1], memory_mesh_starts[2]); */
  err = MPI_Type_create_subarray
    (ndims, memory_mesh_sizes, mesh_sizes, memory_mesh_starts,
     memory_array_order, etype, &memory_type);
  if (err != MPI_SUCCESS) goto fail2;

  err = MPI_Type_commit(&memory_type);
  if (err != MPI_SUCCESS) goto fail2;

  /* read the mesh */
  err = MPI_File_read_at_all(fh, 0, buf, 1, memory_type, &status);
  if (err != MPI_SUCCESS) goto fail2;

  /* no data was read */
  MPI_Get_count(&status, memory_type, &i);
  if (i != 1) {
    err = MPI_ERR_TRUNCATE;
    goto fail2;
  }

  /* Fix the endianness of the data (if necessary) */
  endianSwitch(buf, element_size, file_is_big_endian, ndims,
               memory_mesh_sizes, mesh_sizes,
               memory_mesh_starts, memory_array_order);
  
  /* free my datatypes */
 fail2:
  MPI_Type_free(&memory_type);
 fail1:
  MPI_Type_free(&file_type);

 fail0:
  return err;
}


/* Returns nonzero iff t is a type supported by Mesh_IO_read() */
static int isSupportedType(MPI_Datatype t) {
  return (t == MPI_CHAR
          || t == MPI_SHORT
          || t == MPI_INT
          || t == MPI_LONG
          || t == MPI_LONG_LONG_INT
          || t == MPI_LONG_LONG
          || t == MPI_SIGNED_CHAR
          || t == MPI_UNSIGNED_CHAR
          || t == MPI_UNSIGNED_SHORT
          || t == MPI_UNSIGNED
          || t == MPI_UNSIGNED_LONG
          || t == MPI_UNSIGNED_LONG_LONG
          || t == MPI_FLOAT
          || t == MPI_DOUBLE
          || t == MPI_LONG_DOUBLE
          || t == MPI_WCHAR
          || t == MPI_C_BOOL
          || t == MPI_INT8_T
          || t == MPI_INT16_T
          || t == MPI_INT32_T
          || t == MPI_INT64_T
          || t == MPI_UINT8_T
          || t == MPI_UINT16_T
          || t == MPI_UINT32_T
          || t == MPI_UINT64_T
          || t == MPI_AINT
          || t == MPI_COUNT
          || t == MPI_OFFSET);
}
