#ifndef __REVERSE_BYTES_H__
#define __REVERSE_BYTES_H__

#include <stdlib.h>

void reverseBytes(void *v, int element_size, size_t count);

#endif
