configure_file(coveragereport.in coveragereport @ONLY)
configure_file(coverageTest.in coverageTest @ONLY)

add_custom_target(coverage ${CMAKE_BINARY_DIR}/tests/CoverageCMake/coverageTest ${CMAKE_BINARY_DIR})
