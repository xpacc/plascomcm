! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
!===================================================================================================
!
! ModCompare: Utilities for comparing datasets

! Notes about AlmostEqual:
!   * If Tolerance is present, uses the following comparison for real-valued data:
!       |X - Y| <= Tolerance * max(max(|X|, |Y|), 1)
!     (where | | is abs() if X and Y are scalars and the specified norm if they are arrays)
!   * If Tolerance is not present, it will bound the error to within a fixed number of ULPs
!
!===================================================================================================

module ModCompare

  use ModIEEEArithmetic
  implicit none

  private

  public :: InfNorm
  public :: PNorm
  public :: Compare
  public :: AlmostEqual
  public :: COMPARE_NORM_INF
  public :: COMPARE_NORM_P

  interface InfNorm
    module procedure InfNorm_Rank2
    module procedure InfNorm_Rank3
  end interface InfNorm

  interface PNorm
    module procedure PNorm_Rank2
    module procedure PNorm_Rank3
  end interface PNorm

  interface AlmostEqual
    module procedure AlmostEqual_Scalar
    module procedure AlmostEqual_Rank2
    module procedure AlmostEqual_Rank3
  end interface AlmostEqual

  integer, parameter :: rk = selected_real_kind(15, 307)

  integer, parameter :: COMPARE_NORM_INF = 1
  integer, parameter :: COMPARE_NORM_P = 2

contains

  function InfNorm_Rank2(Values) result(InfNorm)

    real(rk), dimension(:,:), intent(in) :: Values
    real(rk) :: InfNorm

    integer :: i, j

    InfNorm = 0._rk

    do j = 1, size(Values,2)
      do i = 1, size(Values,1)
        if (.not. ieee_is_nan(Values(i,j))) then
          InfNorm = max(abs(Values(i,j)), InfNorm)
        else
          InfNorm = Values(i,j)
          return
        end if
      end do
    end do

  end function InfNorm_Rank2

  function InfNorm_Rank3(Values) result(InfNorm)

    real(rk), dimension(:,:,:), intent(in) :: Values
    real(rk) :: InfNorm

    integer :: i, j, k

    InfNorm = 0._rk

    do k = 1, size(Values,3)
      do j = 1, size(Values,2)
        do i = 1, size(Values,1)
          if (.not. ieee_is_nan(Values(i,j,k))) then
            InfNorm = max(abs(Values(i,j,k)), InfNorm)
          else
            InfNorm = Values(i,j,k)
            return
          end if
        end do
      end do
    end do

  end function InfNorm_Rank3

  function PNorm_Rank2(Values, P) result(PNorm)

    real(rk), dimension(:,:), intent(in) :: Values
    integer, intent(in) :: P
    real(rk) :: PNorm

    integer :: i, j

    PNorm = 0._rk

    do j = 1, size(Values,2)
      do i = 1, size(Values,1)
        if (.not. ieee_is_nan(Values(i,j))) then
          PNorm = PNorm + abs(Values(i,j))**P
        else
          PNorm = Values(i,j)
          return
        end if
      end do
    end do

    PNorm = PNorm**(1._rk/real(P, kind=rk))

  end function PNorm_Rank2

  function PNorm_Rank3(Values, P) result(PNorm)

    real(rk), dimension(:,:,:), intent(in) :: Values
    integer, intent(in) :: P
    real(rk) :: PNorm

    integer :: i, j, k

    PNorm = 0._rk

    do k = 1, size(Values,3)
      do j = 1, size(Values,2)
        do i = 1, size(Values,1)
          if (.not. ieee_is_nan(Values(i,j,k))) then
            PNorm = PNorm + abs(Values(i,j,k))**P
          else
            PNorm = Values(i,j,k)
            return
          end if
        end do
      end do
    end do

    PNorm = PNorm**(1._rk/real(P, kind=rk))

  end function PNorm_Rank3

  function Compare(NormXMinusY, NormX, NormY, Tolerance, Difference)

    real(rk), intent(in) :: NormXMinusY, NormX, NormY
    real(rk), intent(in), optional :: Tolerance
    real(rk), intent(out), optional :: Difference
    logical :: Compare

    real(rk), parameter :: ULPs = 4._rk

    if (.not. (ieee_is_nan(NormXMinusY) .or. ieee_is_nan(NormX) .or. ieee_is_nan(NormY))) then

      if (present(Tolerance)) then
        Compare = NormXMinusY <= Tolerance * max(max(NormX, NormY), 1._rk)
      else
        Compare = NormXMinusY <= ULPs * spacing(max(NormX, NormY))
      end if

      if (present(Difference)) then
        if (present(Tolerance)) then
          Difference = NormXMinusY/max(max(NormX, NormY), 1._rk)
        else
          Difference = NormXMinusY
        end if
      end if

    else

      Compare = .false.

      ! Set Difference to NaN
      if (present(Difference)) then
        if (ieee_is_nan(NormXMinusY)) then
        else if (ieee_is_nan(NormX)) then
          Difference = NormX
        else if (ieee_is_nan(NormY)) then
          Difference = NormY
        end if
      end if

    end if

  end function Compare

  function AlmostEqual_Scalar(X, Y, Tolerance, Difference) result(AlmostEqual)

    real(rk), intent(in) :: X, Y
    real(rk), intent(in), optional :: Tolerance
    real(rk), intent(out), optional :: Difference
    logical :: AlmostEqual

    real(rk) :: NormX, NormY, NormXMinusY

    NormX = abs(X)
    NormY = abs(Y)
    NormXMinusY = abs(X-Y)

    AlmostEqual = Compare(NormXMinusY, NormX, NormY, Tolerance=Tolerance, Difference=Difference)

  end function AlmostEqual_Scalar

  function AlmostEqual_Rank2(X, Y, CompareNorm, P, Tolerance, Difference) result(AlmostEqual)

    real(rk), dimension(:,:), intent(in) :: X, Y
    integer, intent(in), optional :: CompareNorm
    integer, intent(in), optional :: P
    real(rk), intent(in), optional :: Tolerance
    real(rk), intent(out), optional :: Difference
    logical :: AlmostEqual

    integer :: CompareNorm_
    integer :: P_
    real(rk), dimension(:,:), allocatable :: XMinusY
    real(rk) :: NormX, NormY, NormXMinusY

    if (present(CompareNorm)) then
      CompareNorm_ = CompareNorm
    else
      CompareNorm_ = COMPARE_NORM_INF
    end if

    if (CompareNorm_ == COMPARE_NORM_P) then
      if (present(P)) then
        P_ = P
      else
        P_ = 2
      end if
    end if

    allocate(XMinusY(size(X,1),size(X,2)))
    XMinusY = X - Y

    select case (CompareNorm_)
    case (COMPARE_NORM_INF)
      NormX = InfNorm(X)
      NormY = InfNorm(Y)
      NormXMinusY = InfNorm(XMinusY)
    case (COMPARE_NORM_P)
      NormX = PNorm(X, P_)
      NormY = PNorm(Y, P_)
      NormXMinusY = PNorm(XMinusY, P_)
    end select

    AlmostEqual = Compare(NormXMinusY, NormX, NormY, Tolerance=Tolerance, Difference=Difference)

  end function AlmostEqual_Rank2

  function AlmostEqual_Rank3(X, Y, CompareNorm, P, Tolerance, Difference) result(AlmostEqual)

    real(rk), dimension(:,:,:), intent(in) :: X, Y
    integer, intent(in), optional :: CompareNorm
    integer, intent(in), optional :: P
    real(rk), intent(in), optional :: Tolerance
    real(rk), intent(out), optional :: Difference
    logical :: AlmostEqual

    integer :: CompareNorm_
    integer :: P_
    real(rk), dimension(:,:,:), allocatable :: XMinusY
    real(rk) :: NormX, NormY, NormXMinusY

    if (present(CompareNorm)) then
      CompareNorm_ = CompareNorm
    else
      CompareNorm_ = COMPARE_NORM_INF
    end if

    if (CompareNorm_ == COMPARE_NORM_P) then
      if (present(P)) then
        P_ = P
      else
        P_ = 2
      end if
    end if

    allocate(XMinusY(size(X,1),size(X,2),size(X,3)))
    XMinusY = X - Y

    select case (CompareNorm_)
    case (COMPARE_NORM_INF)
      NormX = InfNorm(X)
      NormY = InfNorm(Y)
      NormXMinusY = InfNorm(XMinusY)
    case (COMPARE_NORM_P)
      NormX = PNorm(X, P_)
      NormY = PNorm(Y, P_)
      NormXMinusY = PNorm(XMinusY, P_)
    end select

    AlmostEqual = Compare(NormXMinusY, NormX, NormY, Tolerance=Tolerance, Difference=Difference)

  end function AlmostEqual_Rank3

end module ModCompare
