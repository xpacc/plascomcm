! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
!===================================================================================================
!
! ModStdIO: Defines I/O units corresponding to stdin, stdout and stderr
!
!===================================================================================================

module ModStdIO

#ifdef f2003
  use, intrinsic iso_fortran_env, only : INPUT_UNIT, OUTPUT_UNIT, ERROR_UNIT
#else
  integer, parameter :: INPUT_UNIT = 5
  integer, parameter :: OUTPUT_UNIT = 6
  integer, parameter :: ERROR_UNIT = 0
#endif

end module ModStdIO
