! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
Program Boundary_Layer
  ! ============================================================= !
  !                                                               !
  ! BOUNDARY LAYER ON FLAT PLATE: FLOW INITIALIZATION ROUTINE     !
  ! Generates a grid and initial/boundary conditions in HDF5      !
  ! format for simulating a boundary layer on a flat plate        !
  !                                                               !
  ! ============================================================= !
  !                                                               !
  ! Written by Jesse Capecelatro (jcaps@illinois.edu)             !
  ! 26 August 2014                                                !
  ! Updated 5 November 2014                                       !
  !                                                               !
  ! ============================================================= !
  USE ModHDF5_IO, filename=>filename_HDF5, vars_to_write=>vars_to_write_HDF5, NGRID=>NGRID_HDF5, &
    NDIM=>NDIM_HDF5, nAuxVars=>nAuxVars_HDF5, ND=>ND_HDF5, X=>X_HDF5, IBLANK=>IBLANK_HDF5, &
    Q=>Q_HDF5, Qaux=>Qaux_HDF5, CONFIG=>CONFIG_HDF5
  USE ModParser
  Implicit None

  ! I/O variables
  Character(len=80) :: input_name

  !  Global mesh variables
  Integer :: I2, I3, J2
  Real(KIND=8) :: Lx, Ly, Lz
  logical :: Use_Iblank
  
  ! Call the routines
  Call Boundary_Layer_Init
  Call Boundary_Layer_Grid
  Call Boundary_Layer_Data
  Call Boundary_Layer_BC
  Call Write_HDF5
  Call Finalize

Contains


  ! ============== !
  ! Initialization !
  ! ============== !
  Subroutine Boundary_Layer_Init
    Implicit None

    ! Parse the input file
    input_name="plascomcm.inp"
    call parser_init
    call parser_parsefile(trim(input_name))

    ! HDF5 I/O details
    filename = "TBL_IC.h5"

    Allocate(vars_to_write(4))
    vars_to_write(1:4) = (/ 'xyz', 'cv ','cvt', 'rst' /)

    Return
  End Subroutine Boundary_Layer_Init


  ! ==================== !
  ! Create the grid/mesh !
  ! ==================== !
  Subroutine Boundary_Layer_Grid
    Implicit None

    Integer :: I, J, K, N, NGRIT
    Real(KIND=8) :: dx, dy, dz, ytilde, r, delta0, delta, maxY,meanY,cntY
    Real(KIND=8) :: trip_loc, trip_width, trip_height
    REAL(KIND=8) :: grit_size, rnd, GAUSS, AMP, SIG, x0, z0, z12, alpha
    Logical :: use_grit, use_stretching, done

    ! Initialize the grids
    ngrid = 1
    call parser_read('ND',NDIM,3)
    Allocate(ND(ngrid,3)); ND(:,3) = 1

    ! Number of grid points
    call parser_read('GRID1 NX',ND(1,1))
    call parser_read('GRID1 NY',ND(1,2))
    call parser_read('GRID1 NZ',ND(1,3))
    print *, 'Grid: ',ND(1,1),'x',ND(1,2),'x',ND(1,3)

    ! Allocate the arrays and assign values
    ALLOCATE(X(ngrid,MAXVAL(ND(:,1)),MAXVAL(ND(:,2)),MAXVAL(ND(:,3)),3))
    ALLOCATE(IBLANK(ngrid,MAXVAL(ND(:,1)),MAXVAL(ND(:,2)),MAXVAL(ND(:,3))))

    ! Grid size
    call parser_read('GRID1 LX',Lx)
    call parser_read('GRID1 LY',Ly)
    call parser_read('GRID1 LZ',Lz)
    dx = Lx / DBLE(ND(1,1))
    dy = Ly / DBLE(ND(1,2))
    dz = Lz / DBLE(ND(1,3))

    ! Grid Stretching
    Call Parser_Read('USE STRETCHING',Use_Stretching,.FALSE.)

    ! Stretching parameters
    If (Use_Stretching) r=2.0D0

    ! Generate the grid
    Do K = 1, ND(1,3)
       DO J = 1, ND(1,2)
          Do I = 1, ND(1,1)
             If (ND(1,1) .GT. 1) Then
                ! Create X
                X(1,I,J,K,1) = DBLE(I-1)/DBLE(ND(1,1)-1)*Lx
             Else
                X(1,I,J,K,1) = 0D0
             End If
             If (ND(1,2) .GT. 1) Then
                ! Create Y
                If (Use_Stretching) Then
                   ytilde = DBLE(ND(1,2)-J)/DBLE(ND(1,2)-1)
                   X(1,I,J,K,2) = Ly * (1.0D0-tanh(r*ytilde)/tanh(r))
                Else
                   X(1,I,J,K,2) = DBLE(J-1)/DBLE(ND(1,2)-1)*Ly
                End If
             Else
                X(1,I,J,K,2) = 0D0
             End If
             If (ND(1,3) .GT. 1) Then
                ! Create Z. Periodic direction, correct for length
                X(1,I,J,K,3) = DBLE(K-1)/DBLE(ND(1,3)-1)*(Lz-dz)
             Else
                X(1,I,J,K,3) = 0D0
             End If
          End Do
       End Do
    End Do

    ! Read trip strip parameters
    IBLANK = 1
    Call Parser_Read('USE IBLANK',Use_Iblank,.False.)
    If (Use_Iblank) Then
       Call Parser_Read('Trip location',trip_loc)
       Call Parser_Read('Trip width',trip_width)
       Call Parser_Read('Trip height',trip_height)
       Call Parser_Read('USE GRIT',use_grit,.FALSE.)
       Call Parser_Read('GRIT DIAMETER',grit_size,0.0D0)
       Call Parser_Read('NUMBER OF PARTICLES',NGRIT,0)

       ! Account for grit size
       If (use_grit) trip_height=trip_height-grit_size+1.887432E-04_WP

       ! Blank out trip-strip location
       DO I = 1, ND(1,1)-1
          If (X(1,I,1,1,1).LT.trip_loc) I2=I+1
       End Do
       DO I = ND(1,1),2,-1
          If (X(1,I,1,1,1).GT.trip_loc+trip_width) I3=I-1
       End Do
       DO J = 1, ND(1,2)
          If (X(1,1,J,1,2).LE.trip_height) J2=J
       End Do
       IBLANK(1,I2:I3,1:J2,:)=0
       print *, 'Trip I2:',I2
       print *, 'Trip I3:',I3
       print *, 'Trip J2:',J2
  
       ! Add grit
       If (use_grit) then

          ! Standard deviation
          sig = grit_size

          ! Loop through number of particles
          done=.false.
          N = 0
          !Do while (.not.done)
          Do N=1,Ngrit

             ! Compute amplitude
             AMP =  0.5_WP*grit_size

             ! Get a random location
             Call random_number(rnd)
             x0 = trip_loc + 3.5_WP*sig + (trip_width - 7.0D0*sig) * rnd
             Call random_number(rnd)
             z0 = (Lz - dz) * rnd
             If (NDIM.eq.2) z0 = 0.0D0

             ! Modify the grid
             Do K = 1, ND(1,3)
                Do I = I2, I3

                   ! Check if this location was already modified
                   !If (X(1,I,J2+1,K,2)-X(1,1,J2+1,1,2) .LT. 0.01D0*grit_size) Then
                      
                      ! Represent grit as Gaussian
                      GAUSS = AMP * Exp(-((X(1,I,J2+1,K,1)-x0)**2/(2.0D0*sig**2)+(X(1,I,J2+1,K,3)-z0)**2/(2.0D0*sig**2)))

                      ! Account for periodicity in Z
                      z12 = Lz - abs(X(1,I,J2+1,K,3) - z0)
                      If (NDIM.eq.3) GAUSS = GAUSS + &
                           AMP * Exp(-((X(1,I,J2+1,K,1)-x0)**2/(2.0D0*sig**2)+z12**2/(2.0D0*sig**2)))

                      X(1,I,J2+1,K,2) = X(1,I,J2+1,K,2) + GAUSS

                      ! Get stretching parameter
                      !delta0 = X(1,1,J2+2,1,2) - X(1,1,J2+1,1,2)
                      !Call GeometricStretchFactor(X(1,I,J2+1,K,2),Ly,ND(1,2)-J2-1,delta0,1.05_8,5D-1,alpha)

                      ! Shift cells above (smoothly)
                      Do J=J2+2,ND(1,2)

                         ! Initial delta
                         delta0 = X(1,1,J,1,2) - X(1,1,J-1,1,2)

                         ! Delta after displacement
                         delta = X(1,I,J-1,K,2) + delta0 - X(1,1,J,1,2)

                         ! Adjust grid
                         X(1,I,J,K,2) = X(1,I,J-1,K,2) + delta0 - 0.02D0*delta
                         !X(1,I,J,K,2) = X(1,I,J2+1,K,2) + delta0 + (alpha**(J-J2-2)-1.0_WP)/(alpha-1.0_WP)*delta0

                     End Do ! J
                      
                   !End If 

                End Do ! I
             End Do ! K
             !N = N+1
             !if (N.eq.Ngrit) done=.true.
          End Do
          ! Make sure first and last 2 rows of cells on trip strip are not raised (for stability)
          !X(1,I2:I2+1,J2+1,:,2) = X(1,1:2,J2+1,:,2)
          !X(1,I3-1:I3,J2+1,:,2) = X(1,1:2,J2+1,:,2)
       End If

    End If

    ! Get max/average surface height
    maxY = -huge(1.0_WP)
    meanY = 0.0_WP
    cntY=0.0_WP
    Do K=1,ND(1,3)
       Do I=I2+6,I3-6
          maxY = max(maxY,X(1,I,J2+1,K,2))
          meanY = meanY + X(1,I,J2+1,K,2)
          cntY=cntY+1.0_WP
       End Do
    End Do
    meanY = meanY/cntY
    print *, 'meax surface:',real(maxY,4)
    print *, 'mean surface:',real(meanY,4)

    print *, 'min/max delta_y:',real(X(1,1,2,1,2)-X(1,1,1,1,2),4),real(X(1,1,ND(1,2),1,2)-X(1,1,ND(1,2)-1,1,2),4)

    Return
  End Subroutine Boundary_Layer_Grid


  ! ================================ !
  ! Two-point boundary value problem !
  ! for optimal grid stretching      !
  ! ================================ !
  Subroutine GeometricStretchFactor(x1,xn,n,dx,guess,safety_fac,alpha)
    Implicit None

      ! ... input data
      Real(KIND=8) :: x1,xn,dx
      Integer :: n
      Real(KIND=8) :: guess, safety_fac

      ! ... output data
      Real(KIND=8) :: alpha

      ! ... local variables
      Integer :: i
      Real(KIND=8) :: alpha_old, alpha_new, f_old, f_new, err, slope
      Real(KIND=8), Parameter :: err_tol = 1D-12

      ! ... use secant method to solve the equation dx = (alpha - 1)/(alpha^{n-1}-1)
      alpha_old = guess
      alpha_new = 1.01_8 * guess

      f_old = (xn-x1) - (alpha_old**(n-1) - 1.0_8) / (alpha_old-1.0_8) * dx
      f_new = (xn-x1) - (alpha_new**(n-1) - 1.0_8) / (alpha_new-1.0_8) * dx
      err = dabs(f_new)

      ! ... error check that we got it right by guess
      If (err < err_tol) Then
         alpha = alpha_new
         Return
      End If

      ! ... iterate
      Do While (err > err_tol)

         ! ... secant line slope
         If (dabs(alpha_new - alpha_old) < err_tol) Then
            alpha = alpha_new
            Return
         End If
         slope = (f_new-f_old)/(alpha_new-alpha_old)

         ! ... save
         alpha_old = alpha_new
         f_old = f_new

         ! ... estimate new
         If (dabs(slope) >= err_tol) Then
            alpha_new = alpha_new - safety_fac * f_new / slope
         Else
            alpha = alpha_new
            Return
         End If

         ! ... evaluate new
         f_new = (xn-x1) - (alpha_new**(n-1) - 1.0_8) / (alpha_new-1.0_8) * dx
         err = dabs(f_new)

      End Do

      alpha = alpha_new
      Return
      
    End Subroutine GeometricStretchFactor


  ! =================== !
  ! Boundary conditions !
  ! =================== !
  Subroutine Boundary_Layer_BC
    Implicit None

    Integer :: i,j,nsx,nsy,iunit
    Real(KIND=8) :: xSponge

    ! Get sponge zone thickness
    Call parser_read('XSPONGE',xSponge)
    Do I=1,ND(1,1)
       If (X(1,I,1,1,1).LE.xSponge) nsx=I
    End Do
    Do J=ND(1,2),1,-1
       If (X(1,1,ND(1,2),1,2)-X(1,1,J,1,2).LE.xSponge) nsy=ND(1,2)-J+1
    End Do

    ! Open the file
    iunit=11
    open(iunit,file="bc.dat")

    print *, 'Writing boundary conditions'
    Write (*,'(A)') ''

    ! Write the header
    write(iunit,'(1a1 )') "#"
    write(iunit,'(1a45)') "# Basic boundary condition file for PlasComCM"
    write(iunit,'(1a1 )') "#"
    write(iunit,'(1a24)') "# FD = finite difference"
    write(iunit,'(1a20)') "# FV = finite volume"
    write(iunit,'(1a1 )') "#"
    write(iunit,'(1a8 )') "# ibType"
    write(iunit,'(1a8 )') "# ======"
    write(iunit,'(1a32)') "#     00     OUTER_BOUNDARY (FD)"
    write(iunit,'(1a41)') "#     21     SAT_SLIP_ADIABATIC_WALL (FD)"
    write(iunit,'(1a44)') "#     22     SAT_NOSLIP_ISOTHERMAL_WALL (FD)"
    write(iunit,'(1a33)') "#     23     SAT_NOSLIP_ADIABATIC_WALL (FD)"
    write(iunit,'(1a31)') "#     24     SAT_FAR_FIELD (FD)"
    write(iunit,'(1a37)') "#     25     SAT_BLOCK_INTERFACE (FD)"
    write(iunit,'(1a51)') "#     31     NSCBC_INFLOW_VELOCITY_TEMPERATURE (FD)"
    write(iunit,'(1a63)') "#     32     NSCBC_INFLOW_VELOCITY_TEMPERATURE_FROM_SPONGE (FD)"
    write(iunit,'(1a57)') "#     41     NSCBC_OUTFLOW_PERFECT_NONREFLECTION (FD, FV)"
    write(iunit,'(1a54)') "#     42     NSCBC_OUTFLOW_PERFECT_REFLECTION (FD, FV)"
    write(iunit,'(1a47)') "#     51     NSCBC_WALL_ADIABATIC_SLIP (FD, FV)"
    write(iunit,'(1a50)') "#     52     NSCBC_WALL_ISOTHERMAL_NOSLIP (FD, FV)"
    write(iunit,'(1a39)') "#     61     NSCBC_SUBSONIC_INFLOW (FD)"
    write(iunit,'(1a45)') "#     62     NSCBC_SUPERSONIC_INFLOW (FD, FV)"
    write(iunit,'(1a29)') "#     73     PERIODICITY (FV)"
    write(iunit,'(1a30)') "#     91     FV_DIRICHLET (FV)"
    write(iunit,'(1a28)') "#     92     FV_NEUMANN (FV)"
    write(iunit,'(1a29)') "#     93     FV_SYMMETRY (FV)"
    write(iunit,'(1a36)') "#     94     FV_BLOCK_INTERFACE (FV)"
    write(iunit,'(1a45)') "#     95     FV_BLOCK_INTERFACE_PERIODIC (FV)"
    write(iunit,'(1a28)') "#     99     SPONGE (FD, FV)"
    write(iunit,'(1a1 )') "#"
    write(iunit,'(1a56)') "# Grid ibType ibDir   is    ie    js    je     ks    ke "
    write(iunit,'(1a56)') "# ==== ====== =====  ====  ====  ====  ====   ====  ===="

    ! Input the boundary conditions
    ! Inflow / outflow
    write(iunit,'(9I6)')      1,   24,     1,    1,     1,    1,   -1,    1,    -1
    write(iunit,'(9I6)')      1,   24,    -1,   -1,    -1,    1,   -1,    1,    -1
    write(iunit,'(9I6)')      1,   24,    -2,    1,    -1,   -1,   -1,    1,    -1
    ! No-slip wall
    If (Use_IBlank) Then
       write(iunit,'(9I6)')      1,   22,     2,    1,  I2-1,    1,    1,    1,    -1
       write(iunit,'(9I6)')      1,   22,     2, I3+1,    -1,    1,    1,    1,    -1
       write(iunit,'(9I6)')      1,   22,     2, I2-1,  I3+1, J2+1, J2+1,    1,    -1
       write(iunit,'(9I6)')      1,   22,    -1, I2-1,  I2-1,    1, J2+1,    1,    -1
       write(iunit,'(9I6)')      1,   22,     1, I3+1,  I3+1,    1, J2+1,    1,    -1
    Else
       write(iunit,'(9I6)')      1,   22,     2,    1,    -1,    1,    1,    1,    -1
    End If
   ! Add spounge layers
    write(iunit,'(9I6)')      1,   99,     1,1         ,nsx,    1,    -1,    1,    -1
    write(iunit,'(9I6)')      1,   99,    -1,ND(1,1)-nsx,-1,    1,    -1,    1,    -1
    write(iunit,'(9I6)')      1,   99,    -2,    1,    -1,ND(1,2)-nsy,-1,    1,    -1

    ! Close the file
    close(iunit)

    Return
  End Subroutine Boundary_Layer_BC


  ! ========================= !
  ! Initial (target) solution !
  ! ========================= !
  Subroutine Boundary_Layer_Data
    Implicit None

    integer :: I, J, K, EQ_STATE
    Real(KIND=8), Dimension(:,:,:,:), Pointer :: rho,rhoU,rhoV,rhoW,rhoE
    Real(KIND=8) :: U0, rho0, P0, mu0, L0, gamma, C, Re, Pr, delta99

    ! Solution to Blasius boundary layer
    Real(KIND=8) :: blasius0,blasius1, delta, eta, X0, xx, yy
    Real(KIND=8) :: f2l,f2r,f0l,f0r
    Real(KIND=8), dimension(0:9) :: by0 = (/ &
         0.0_8, 0.165571818583440_8, 0.650024518764203_8, 1.39680822972500_8, &
         2.30574664618049_8, 3.28327391871370_8, 4.27962110517696_8, &
         5.27923901129384_8, 6.27921363832835_8, 7.27921257797747_8 /)
    Real(KIND=8), dimension(0:9) :: by1 = (/ &
         0.0_8, 0.329780063306651_8, 0.629765721178679_8, 0.84604458266019_8, &
         0.95551827831671_8, 0.99154183259084_8, 0.99897290050990_8, &
         0.9999216098795_8, 0.99999627301467_8, 0.99999989265063_8 /)
    Real(KIND=8), dimension(0:9) :: by2 = (/ &
         0.332057384255589_8, 0.323007152241930_8, 0.266751564401387_8, 0.161360240845588_8, &
         0.06423404047594_8, 0.01590689966410_8, 0.00240199722109_8, &
         0.00022016340923_8, 0.00001224984692_8, 0.00000041090325_8 /)

    ! Allocate solution array
    Allocate(Q(ngrid,MAXVAL(ND(:,1)),MAXVAL(ND(:,2)),MAXVAL(ND(:,3)),5))

    ! Link the pointers
    rho   =>  Q(:,:,:,:,1)
    rhoU  =>  Q(:,:,:,:,2)
    rhoV  =>  Q(:,:,:,:,3)
    rhoW  =>  Q(:,:,:,:,4)
    rhoE  =>  Q(:,:,:,:,5)

    ! Read from input
    Call parser_read('REYNOLDS_NUMBER',Re,0D0)
    Call parser_read('PRANDTL_NUMBER',Pr,0.72D0)
    Call parser_read('DENSITY_REFERENCE',rho0,1.0D0)
    Call parser_read('PRESSURE_REFERENCE',P0)
    Call parser_read('GAMMA_REFERENCE',gamma,1.4D0)
    Call parser_read('SNDSPD_REFERENCE',C,347.0D0)
    Call parser_read('GAS_EQUATION_OF_STATE',EQ_STATE,0)
    Call parser_read('INFLOW VELOCITY',U0)
    Call parser_read('LENGTH_REFERENCE',L0)
    Call parser_read('BL_START',X0)

    ! Assign values to the config array
    Allocate(CONFIG(1:NGRID))
    CONFIG(1)%t_iter = 0
    CONFIG(1)%NDIM   = NDIM
    CONFIG(1)%ND(1)  = ND(1,1)
    CONFIG(1)%ND(2)  = ND(1,2)
    CONFIG(1)%ND(3)  = ND(1,3)
    CONFIG(1)%Use_IB = Merge(1,0,Use_IBlank)
    CONFIG(1)%time   = 0D0
    CONFIG(1)%Re     = Re
    CONFIG(1)%Pr     = Pr
    CONFIG(1)%Sc     = 1.0D0

    ! Number of auxilary variables
    nauxvars = 0

    ! Initialize the variable array
    rho  = 1.0D0
    rhoU = 0.0D0
    rhoV = 0.0D0
    rhoW = 0.0D0

    ! Compute energy
    if (EQ_STATE == 0) then
       rhoE = P0/(gamma-1.0D0)/rho0/C**2+0.5D0*(U0/C)**2
    else
       Write (*,'(A)') 'Boundary_layer only implemented using ideal gas law'
       Stop
    end if

    ! Compute viscosity
    mu0 = rho0*C*L0/Re

    ! Initialize the velocity field (Blasius solution for laminar flat plate)
    Do J=2,ND(1,2)
       Do I=1,ND(1,1)
          ! Get the corrdinates (start slighty downstream)
          xx=X(1,I,J,1,1) + X0
          yy=X(1,I,J,1,2)

          ! Return the first derivative of the Blasius function
          delta=sqrt(mu0*xx/U0/rho0)
          eta=yy/delta
          if (eta.le.0.0D0) then
             blasius0 = 0.0D0
             blasius1 = 0.0D0
          else if (eta.ge.9.0D0) then
             blasius0 = by0(9) + (eta-9.0D0)
             blasius1 = 1.0D0
          else
             k = int(eta)

             f0l = by0(k)
             f0r = by0(k+1)
             f2l = by2(k)
             f2r = by2(k+1)
             blasius0 = &
                  1.0D0/6.0D0*f2l*(real(k+1,8)-eta)**3 + &
                  1.0D0/6.0D0*f2r*(eta-real(k,8))**3 + &
                  (f0l-1.0D0/6.0D0*f2l)*(real(k+1,8)-eta) + &
                  (f0r-1.0D0/6.0D0*f2r)*(eta-real(k,8))

             f0l = by1(k)
             f0r = by1(k+1)
             f2l = -0.5D0*by0(k)*by2(k)
             f2r = -0.5D0*by0(k+1)*by2(k+1)
             blasius1 = &
                  1.0D0/6.0D0*f2l*(real(k+1,8)-eta)**3 + &
                  1.0D0/6.0D0*f2r*(eta-real(k,8))**3 + &
                  (f0l-1.0D0/6.0D0*f2l)*(real(k+1,8)-eta) + &
                  (f0r-1.0D0/6.0D0*f2r)*(eta-real(k,8))
          end if
    
          ! Update the velocity
          rhoU(1,I,J,:) = U0*blasius1/C
          rhoV(1,I,J,:) = 0.5D0*sqrt(mu0*U0/rho0/xx)*(eta*blasius1-blasius0)/C

       End Do
    End Do

    ! Reinforce zero-slip
    rhoU(1,:,1,:) = 0.0D0
    rhoV(1,:,1,:) = 0.0D0
    rhoW(1,:,1,:) = 0.0D0

    ! Zero variables in iblank region
    If (Use_IBlank) Then
       rhoU(1,I2-1:I3+1,1:J2+2,:)=0.0D0
       rhoV(1,I2-1:I3+1,1:J2+2,:)=0.0D0
       rhoW(1,I2-1:I3+1,1:J2+2,:)=0.0D0
    End If

    ! ------------------------------------------------
    ! Compute delta99 at downstream distance

    ! Find measurement location
    Do J=1,ND(1,1)
       If (X(1,J,1,1,1).LE.0.365D0) I=J
    End Do

    ! Get delta99
    Do J=1,ND(1,2)
       If (rhoU(1,I2,J,1)*C.LE.0.99D0*U0) delta99 = X(1,I2,J,1,2)
    End Do
    print *, 'delta99=',delta99

    Return
  End Subroutine Boundary_Layer_Data


  ! ================== !
  ! Clean up and leave !
  ! ================== !
  Subroutine Finalize
    Implicit None
  
    Deallocate(ND,IBLANK,X,Q)

    Return
  End Subroutine Finalize

End Program Boundary_Layer
