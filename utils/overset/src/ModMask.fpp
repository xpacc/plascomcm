! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
module ModMask

  use ModGlobal
  use ModGridUtils
  implicit none

  private

  public :: t_mask
  public :: t_mask_
  public :: MakeMask
  public :: DestroyMask
  public :: GenerateOuterEdgeMask
  public :: GrowMask
  public :: CountMask
  public :: MaskToIBlank
  public :: PrintMask

  type t_mask
    integer(ik) :: nd
    integer(ik), dimension(MAX_ND) :: is, ie
    integer(ik), dimension(MAX_ND) :: npoints
    integer(ik), dimension(MAX_ND) :: periodic
    integer(ik) :: periodic_storage
    logical, dimension(:,:,:), allocatable :: values
  end type t_mask

  ! Trailing _ added for compatibility with compilers that don't support F2003 constructors
  interface t_mask_
    module procedure t_mask_Default
  end interface t_mask_

contains

  function t_mask_Default(nDims) result(Mask)

    integer(ik), intent(in) :: nDims
    type(t_mask) :: Mask

    Mask%nd = nDims
    Mask%is = 1
    Mask%ie(:nDims) = 0
    Mask%ie(nDims+1:) = 1
    Mask%npoints = 0
    Mask%periodic = FALSE
    Mask%periodic_storage = NO_OVERLAP_PERIODIC

  end function t_mask_Default

  subroutine MakeMask(Mask, nDims, iStart, iEnd, Periodic, PeriodicStorage)

    type(t_mask), intent(out) :: Mask
    integer(ik), intent(in) :: nDims
    integer(ik), dimension(nDims), intent(in) :: iStart, iEnd
    integer(ik), dimension(nDims), intent(in), optional :: Periodic
    integer(ik), intent(in), optional :: PeriodicStorage

    Mask%nd = nDims
    Mask%is(:nDims) = iStart
    Mask%is(nDims+1:) = 1
    Mask%ie(:nDims) = iEnd
    Mask%ie(nDims+1:) = 1
    Mask%npoints(:nDims) = iEnd - iStart + 1
    Mask%npoints(nDims+1:) = 1

    if (present(Periodic)) then
      Mask%periodic(:nDims) = Periodic
      Mask%periodic(nDims+1:) = FALSE
    else
      Mask%periodic = FALSE
    end if

    if (present(PeriodicStorage)) then
      Mask%periodic_storage = PeriodicStorage
    else
      Mask%periodic_storage = NO_OVERLAP_PERIODIC
    end if

    allocate(Mask%values(Mask%is(1):Mask%ie(1),Mask%is(2):Mask%ie(2),Mask%is(3):Mask%ie(3)))

  end subroutine MakeMask

  subroutine DestroyMask(Mask)

    type(t_mask), intent(inout) :: Mask

    deallocate(Mask%values)

  end subroutine DestroyMask

  subroutine GenerateOuterEdgeMask(Mask, OuterEdgeMask)

    type(t_mask), intent(in) :: Mask
    type(t_mask), intent(out) :: OuterEdgeMask

    integer(ik) :: i, j, k, m, n, o
    integer(ik), dimension(MAX_ND) :: iStart, iEnd
    integer(ik), dimension(MAX_ND) :: NeighborStart, NeighborEnd
    integer(ik), dimension(MAX_ND) :: Point, Neighbor

    call MakeMask(OuterEdgeMask, nDims=Mask%nd, iStart=Mask%is(:Mask%nd)-1, &
      iEnd=Mask%ie(:Mask%nd)+1)
    OuterEdgeMask%values = .false.

    iStart = Mask%is
    iEnd = merge(Mask%ie-1, Mask%ie, Mask%periodic /= FALSE .and. Mask%periodic_storage == &
      OVERLAP_PERIODIC)

    NeighborStart = merge(-1, 0, [(i<=Mask%nd,i=1,MAX_ND)])
    NeighborEnd = merge(1, 0, [(i<=Mask%nd,i=1,MAX_ND)])
    
    do k = iStart(3), iEnd(3)
      do j = iStart(2), iEnd(2)
        do i = iStart(1), iEnd(1)
          if (Mask%values(i,j,k)) then
            Point = [i,j,k]
            do o = NeighborStart(3), NeighborEnd(3)
              do n = NeighborStart(2), NeighborEnd(2)
                do m = NeighborStart(1), NeighborEnd(1)
                  Neighbor = Point + [m,n,o]
                  Neighbor = PeriodicAdjust(MAX_ND, Mask%periodic, Mask%periodic_storage, Mask%is, &
                    Mask%ie, Neighbor)
                  if (all(Neighbor >= iStart) .and. all(Neighbor <= iEnd)) then
                    OuterEdgeMask%values(Neighbor(1),Neighbor(2),Neighbor(3)) = &
                      .not. Mask%values(Neighbor(1),Neighbor(2),Neighbor(3))
                  else
                    OuterEdgeMask%values(Neighbor(1),Neighbor(2),Neighbor(3)) = .true.
                  end if
                end do
              end do
            end do
          end if
        end do
      end do
    end do

    if (Mask%periodic(1) /= FALSE) then
      if (Mask%periodic_storage == OVERLAP_PERIODIC) then
        OuterEdgeMask%values(Mask%ie(1),:,:) = OuterEdgeMask%values(Mask%is(1),:,:)
      end if
      OuterEdgeMask%values(Mask%ie(1)+1,:,:) = OuterEdgeMask%values(Mask%is(1)+1,:,:)
      OuterEdgeMask%values(Mask%is(1)-1,:,:) = OuterEdgeMask%values(Mask%ie(1)-1,:,:)
    end if

    if (Mask%periodic(2) /= FALSE) then
      if (Mask%periodic_storage == OVERLAP_PERIODIC) then
        OuterEdgeMask%values(:,Mask%ie(2),:) = OuterEdgeMask%values(:,Mask%is(2),:)
      end if
      OuterEdgeMask%values(:,Mask%ie(2)+1,:) = OuterEdgeMask%values(:,Mask%is(2)+1,:)
      OuterEdgeMask%values(:,Mask%is(2)-1,:) = OuterEdgeMask%values(:,Mask%ie(2)-1,:)
    end if

    if (Mask%periodic(3) /= FALSE) then
      if (Mask%periodic_storage == OVERLAP_PERIODIC) then
        OuterEdgeMask%values(:,:,Mask%ie(3)) = OuterEdgeMask%values(:,:,Mask%is(3))
      end if
      OuterEdgeMask%values(:,:,Mask%ie(3)+1) = OuterEdgeMask%values(:,:,Mask%is(3)+1)
      OuterEdgeMask%values(:,:,Mask%is(3)-1) = OuterEdgeMask%values(:,:,Mask%ie(3)-1)
    end if

  end subroutine GenerateOuterEdgeMask

  subroutine GrowMask(Mask, Amount)

    type(t_mask), intent(inout) :: Mask
    integer(ik), intent(in) :: Amount

    integer(ik) :: i, j, k, m, n, o
    integer(ik), dimension(MAX_ND) :: iStart, iEnd
    integer(ik), dimension(MAX_ND) :: NeighborStart, NeighborEnd
    integer(ik), dimension(MAX_ND) :: Point, Neighbor, FillPoint
    type(t_mask) :: PrevMask
    logical :: AllNeighborsEqual

    if (Amount == 0) return
    
    PrevMask = Mask

    iStart = Mask%is
    iEnd = merge(Mask%ie-1, Mask%ie, Mask%periodic /= FALSE .and. Mask%periodic_storage == &
      OVERLAP_PERIODIC)

    NeighborStart = merge(-1, 0, [(i<=Mask%nd,i=1,MAX_ND)])
    NeighborEnd = merge(1, 0, [(i<=Mask%nd,i=1,MAX_ND)])

    do k = iStart(3), iEnd(3)
      do j = iStart(2), iEnd(2)
        do i = iStart(1), iEnd(1)
          if ((Amount > 0 .and. PrevMask%values(i,j,k)) .or. &
            (Amount < 0 .and. .not. PrevMask%values(i,j,k))) then
            Point = [i,j,k]
            AllNeighborsEqual = .true.
            do o = NeighborStart(3), NeighborEnd(3)
              do n = NeighborStart(2), NeighborEnd(2)
                do m = NeighborStart(1), NeighborEnd(1)
                  Neighbor = Point + [m,n,o]
                  Neighbor = PeriodicAdjust(MAX_ND, Mask%periodic, Mask%periodic_storage, Mask%is, &
                    Mask%ie, Neighbor)
                  if (all(Neighbor >= iStart) .and. all(Neighbor <= iEnd)) then
                    AllNeighborsEqual = AllNeighborsEqual .and. &
                      (PrevMask%values(Neighbor(1),Neighbor(2),Neighbor(3)) .eqv. &
                      PrevMask%values(i,j,k))
                  end if
                end do
              end do
            end do
            if (.not. AllNeighborsEqual) then
              do o = -abs(Amount), abs(Amount)
                do n = -abs(Amount), abs(Amount)
                  do m = -abs(Amount), abs(Amount)
                    FillPoint = Point + [m,n,o]
                    FillPoint = PeriodicAdjust(MAX_ND, Mask%periodic, Mask%periodic_storage, &
                      Mask%is, Mask%ie, FillPoint)
                    if (all(FillPoint >= iStart) .and. all(FillPoint <= iEnd)) then
                      Mask%values(FillPoint(1),FillPoint(2),FillPoint(3)) = Amount > 0
                    end if
                  end do
                end do
              end do
            end if
          end if
        end do
      end do
    end do

    if (Mask%periodic(1) /= FALSE .and. Mask%periodic_storage == OVERLAP_PERIODIC) then
      Mask%values(Mask%ie(1),:,:) = Mask%values(Mask%is(1),:,:)
    end if

    if (Mask%periodic(2) /= FALSE .and. Mask%periodic_storage == OVERLAP_PERIODIC) then
      Mask%values(:,Mask%ie(2),:) = Mask%values(:,Mask%is(2),:)
    end if

    if (Mask%periodic(3) /= FALSE .and. Mask%periodic_storage == OVERLAP_PERIODIC) then
      Mask%values(:,:,Mask%ie(3)) = Mask%values(:,:,Mask%is(3))
    end if

  end subroutine GrowMask

  function CountMask(Mask) result(nTrue)

    type(t_mask), intent(in) :: Mask
    integer(ik) :: nTrue

    integer(ik) :: i, j, k
    integer(ik), dimension(MAX_ND) :: iStart, iEnd

    iStart = Mask%is
    iEnd = merge(Mask%ie-1, Mask%ie, Mask%periodic /= FALSE .and. Mask%periodic_storage == &
      OVERLAP_PERIODIC)

    nTrue = 0
    do k = iStart(3), iEnd(3)
      do j = iStart(2), iEnd(2)
        do i = iStart(1), iEnd(1)
          if (Mask%values(i,j,k)) then
            nTrue = nTrue + 1
          end if
        end do
      end do
    end do

  end function CountMask

  subroutine MaskToIBlank(Mask, IBlank)

    type(t_mask), intent(in) :: Mask
    integer(ik), dimension(Mask%is(1):Mask%ie(1),Mask%is(2):Mask%ie(2),Mask%is(3):Mask%ie(3)), &
      intent(out) :: IBlank

    integer(ik) :: i, j, k

    do k = Mask%is(3), Mask%ie(3)
      do j = Mask%is(2), Mask%ie(2)
        do i = Mask%is(1), Mask%ie(1)
          IBlank(i,j,k) = merge(1, 0, Mask%values(i,j,k))
        end do
      end do
    end do

  end subroutine MaskToIBlank

  subroutine PrintMask(Mask, iStart, iEnd)

    type(t_mask), intent(in) :: Mask
    integer(ik), dimension(Mask%nd), intent(in), optional :: iStart, iEnd

    integer(ik) :: i, j
    integer(ik), dimension(MAX_ND) :: iS, iE
    integer(ik) :: NormalDir
    integer(ik) :: iDir, jDir
    integer(ik), dimension(2) :: iSSlice, iESlice
    integer(ik) :: Stride
    integer(ik) :: Slice
    integer(ik), dimension(MAX_ND) :: Point
    integer(ik), dimension(MAX_ND) :: AdjustedPoint

    if (present(iStart)) then
      iS(:Mask%nd) = iStart
      iS(Mask%nd+1:) = 1
    else
      iS = Mask%is
    end if

    if (present(iEnd)) then
      iE(:Mask%nd) = iEnd
      iE(Mask%nd+1:) = 1
    else
      iE = Mask%ie
      iE(MAX_ND) = Mask%is(MAX_ND)
    end if

    do i = 1, MAX_ND
      if (iE(i) == iS(i)) then
        exit
      end if
    end do
    NormalDir = i

    iDir = modulo(NormalDir,3) + 1
    jDir = modulo(NormalDir+1,3) + 1
    iSSlice = [iS(iDir),iS(jDir)]
    iESlice = [iE(iDir),iE(jDir)]
    if (Mask%nd == 3) then
      Slice = iS(NormalDir)
    else
      Slice = 1
    end if

    write (*, '(a)', advance='no') "   "
    do i = iSSlice(1), iESlice(1)
      write (*, '(a)', advance='no') " - "
    end do
    write (*, '(a)') "   "

    do j = iESlice(2), iSSlice(2), -1

      write (*, '(a)', advance='no') " | "

      do i = iSSlice(1), iESlice(1)
        select case (NormalDir)
        case (1)
          Point = [Slice,i,j]
        case (2)
          Point = [j,Slice,i]
        case (3)
          Point = [i,j,Slice]
        end select
        AdjustedPoint = PeriodicAdjust(MAX_ND, Mask%periodic, Mask%periodic_storage, &
          Mask%is, Mask%ie, Point)
        write(*, '(a)', advance='no') merge(" X ", "   ", Mask%values(AdjustedPoint(1), &
          AdjustedPoint(2), AdjustedPoint(3)))
      end do

      write (*, '(a)') " | "

    end do

    write (*, '(a)', advance='no') "   "
    do i = iSSlice(1), iESlice(1)
      write (*, '(a)', advance='no') " - "
    end do
    write (*, '(a)') "   "
    write (*, '(a)') ""

  end subroutine PrintMask

end module ModMask
