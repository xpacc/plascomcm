! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
@MPITest(npes=[1])
subroutine ModBoundingBoxTest_default(this)

  use pFUnit_mod
  use ModBoundingBox
  use ModGlobal
  implicit none

  class(MPITestCase), intent(inout) :: this

  type(t_bbox) :: BBox

  BBox = t_bbox_(2)

  @AssertEqual(2, BBox%nd)
  @AssertEqual([0._rk, 0._rk, 0._rk], BBox%b)
  @AssertEqual([-1._rk, -1._rk, 0._rk], BBox%e)

  BBox = t_bbox_(3)

  @AssertEqual(3, BBox%nd)
  @AssertEqual([0._rk, 0._rk, 0._rk], BBox%b)
  @AssertEqual([-1._rk, -1._rk, -1._rk], BBox%e)

end subroutine ModBoundingBoxTest_default

@MPITest(npes=[1])
subroutine ModBoundingBoxTest_assigned(this)

  use pFUnit_mod
  use ModBoundingBox
  use ModGlobal
  implicit none

  class(MPITestCase), intent(inout) :: this

  type(t_bbox) :: BBox

  BBox = t_bbox_(2, [2._rk, 4._rk], [3._rk, 5._rk])

  @AssertEqual(2, BBox%nd)
  @AssertEqual([2._rk, 4._rk, 0._rk], BBox%b)
  @AssertEqual([3._rk, 5._rk, 0._rk], BBox%e)

  BBox = t_bbox_(3, [2._rk, 4._rk, 6._rk], [3._rk, 5._rk, 7._rk])

  @AssertEqual(3, BBox%nd)
  @AssertEqual([2._rk, 4._rk, 6._rk], BBox%b)
  @AssertEqual([3._rk, 5._rk, 7._rk], BBox%e)

end subroutine ModBoundingBoxTest_assigned

@MPITest(npes=[1])
subroutine ModBoundingBoxTest_overlaps_2d(this)

  use pFUnit_mod
  use ModBoundingBox
  use ModGlobal
  implicit none

  class(MPITestCase), intent(inout) :: this

  type(t_bbox) :: BBox1, BBox2, BBox3

  BBox1 = t_bbox_(2, [2._rk, 4._rk], [3._rk, 5._rk])

  ! Self
  BBox2 = BBox1
  @AssertTrue(BBOverlaps(BBox1, BBox2))

  ! Smaller
  BBox2 = t_bbox_(2, [2.25_rk, 4.25_rk], [2.75_rk, 4.75_rk])
  @AssertTrue(BBOverlaps(BBox1, BBox2))

  ! Shifted left in x
  BBox2 = t_bbox_(2, [1.5_rk, 4._rk], [2.5_rk, 5._rk])
  @AssertTrue(BBOverlaps(BBox1, BBox2))

  ! Shifted right in x
  BBox2 = t_bbox_(2, [2.5_rk, 4._rk], [3.5_rk, 5._rk])
  @AssertTrue(BBOverlaps(BBox1, BBox2))

  ! Shifted left in y
  BBox2 = t_bbox_(2, [2._rk, 3.5_rk], [3._rk, 4.5_rk])
  @AssertTrue(BBOverlaps(BBox1, BBox2))

  ! Shifted right in y
  BBox2 = t_bbox_(2, [2._rk, 4.5_rk], [3._rk, 5.5_rk])
  @AssertTrue(BBOverlaps(BBox1, BBox2))

  ! Larger
  BBox2 = t_bbox_(2, [1.5_rk, 3.5_rk], [3.5_rk, 5.5_rk])
  @AssertTrue(BBOverlaps(BBox1, BBox2))

  ! Non-overlapping
  BBox2 = t_bbox_(2, [4._rk, 6._rk], [5._rk, 7._rk])
  @AssertFalse(BBOverlaps(BBox1, BBox2))

  ! Left empty
  BBox2 = t_bbox_(2, [3._rk, 5._rk], [2._rk, 4._rk])
  BBox3 = t_bbox_(2, [1._rk, 3._rk], [4._rk, 6._rk])
  @AssertFalse(BBOverlaps(BBox2, BBox3))

  ! Right empty
  BBox2 = t_bbox_(2, [1._rk, 3._rk], [4._rk, 6._rk])
  BBox3 = t_bbox_(2, [3._rk, 5._rk], [2._rk, 4._rk])
  @AssertFalse(BBOverlaps(BBox2, BBox3))

end subroutine ModBoundingBoxTest_overlaps_2d

@MPITest(npes=[1])
subroutine ModBoundingBoxTest_overlaps_3d(this)

  use pFUnit_mod
  use ModBoundingBox
  use ModGlobal
  implicit none

  class(MPITestCase), intent(inout) :: this

  type(t_bbox) :: BBox1, BBox2, BBox3

  BBox1 = t_bbox_(3, [2._rk, 4._rk, 6._rk], [3._rk, 5._rk, 7._rk])

  ! Self
  BBox2 = BBox1
  @AssertTrue(BBOverlaps(BBox1, BBox2))

  ! Smaller
  BBox2 = t_bbox_(3, [2.25_rk, 4.25_rk, 6.25_rk], [2.75_rk, 4.75_rk, 6.75_rk])
  @AssertTrue(BBOverlaps(BBox1, BBox2))

  ! Shifted left in x
  BBox2 = t_bbox_(3, [1.5_rk, 4._rk, 6._rk], [2.5_rk, 5._rk, 7._rk])
  @AssertTrue(BBOverlaps(BBox1, BBox2))

  ! Shifted right in x
  BBox2 = t_bbox_(3, [2.5_rk, 4._rk, 6._rk], [3.5_rk, 5._rk, 7._rk])
  @AssertTrue(BBOverlaps(BBox1, BBox2))

  ! Shifted left in y
  BBox2 = t_bbox_(3, [2._rk, 3.5_rk, 6._rk], [3._rk, 4.5_rk, 7._rk])
  @AssertTrue(BBOverlaps(BBox1, BBox2))

  ! Shifted right in y
  BBox2 = t_bbox_(3, [2._rk, 4.5_rk, 6._rk], [3._rk, 5.5_rk, 7._rk])
  @AssertTrue(BBOverlaps(BBox1, BBox2))

  ! Shifted left in z
  BBox2 = t_bbox_(3, [2._rk, 4._rk, 5.5_rk], [3._rk, 5._rk, 6.5_rk])
  @AssertTrue(BBOverlaps(BBox1, BBox2))

  ! Shifted right in z
  BBox2 = t_bbox_(3, [2._rk, 4._rk, 6.5_rk], [3._rk, 5._rk, 7.5_rk])
  @AssertTrue(BBOverlaps(BBox1, BBox2))

  ! Larger
  BBox2 = t_bbox_(3, [1.5_rk, 3.5_rk, 5.5_rk], [3.5_rk, 5.5_rk, 7.5_rk])
  @AssertTrue(BBOverlaps(BBox1, BBox2))

  ! Non-overlapping
  BBox2 = t_bbox_(3, [4._rk, 6._rk, 8._rk], [5._rk, 7._rk, 9._rk])
  @AssertFalse(BBOverlaps(BBox1, BBox2))

  ! Left empty
  BBox2 = t_bbox_(3, [3._rk, 5._rk, 7._rk], [2._rk, 4._rk, 6._rk])
  BBox3 = t_bbox_(3, [1._rk, 3._rk, 5._rk], [4._rk, 6._rk, 8._rk])
  @AssertFalse(BBOverlaps(BBox2, BBox3))

  ! Right empty
  BBox2 = t_bbox_(3, [1._rk, 3._rk, 5._rk], [4._rk, 6._rk, 8._rk])
  BBox3 = t_bbox_(3, [3._rk, 5._rk, 7._rk], [2._rk, 4._rk, 6._rk])
  @AssertFalse(BBOverlaps(BBox2, BBox3))

end subroutine ModBoundingBoxTest_overlaps_3d

@MPITest(npes=[1])
subroutine ModBoundingBoxTest_contains_2d(this)

  use pFUnit_mod
  use ModBoundingBox
  use ModGlobal
  implicit none

  class(MPITestCase), intent(inout) :: this

  type(t_bbox) :: BBox1, BBox2, BBox3

  BBox1 = t_bbox_(2, [2._rk, 4._rk], [3._rk, 5._rk])

  ! Self
  BBox2 = BBox1
  @AssertTrue(BBContains(BBox1, BBox2))

  ! Smaller
  BBox2 = t_bbox_(2, [2.25_rk, 4.25_rk], [2.75_rk, 4.75_rk])
  @AssertTrue(BBContains(BBox1, BBox2))

  ! Shifted left in x
  BBox2 = t_bbox_(2, [1.5_rk, 4._rk], [2.5_rk, 5._rk])
  @AssertFalse(BBContains(BBox1, BBox2))

  ! Shifted right in x
  BBox2 = t_bbox_(2, [2.5_rk, 4._rk], [3.5_rk, 5._rk])
  @AssertFalse(BBContains(BBox1, BBox2))

  ! Shifted left in y
  BBox2 = t_bbox_(2, [2._rk, 3.5_rk], [3._rk, 4.5_rk])
  @AssertFalse(BBContains(BBox1, BBox2))

  ! Shifted right in y
  BBox2 = t_bbox_(2, [2._rk, 4.5_rk], [3._rk, 5.5_rk])
  @AssertFalse(BBContains(BBox1, BBox2))

  ! Larger
  BBox2 = t_bbox_(2, [1.5_rk, 3.5_rk], [3.5_rk, 5.5_rk])
  @AssertFalse(BBContains(BBox1, BBox2))

  ! Left empty
  BBox2 = t_bbox_(2, [3._rk, 5._rk], [2._rk, 4._rk])
  BBox3 = t_bbox_(2, [4._rk, 6._rk], [1._rk, 3._rk])
  @AssertFalse(BBContains(BBox2, BBox3))

  ! Right empty
  BBox2 = t_bbox_(2, [2._rk, 4._rk], [3._rk, 5._rk])
  BBox3 = t_bbox_(2, [4._rk, 6._rk], [1._rk, 3._rk])
  @AssertFalse(BBContains(BBox2, BBox3))

end subroutine ModBoundingBoxTest_contains_2d

@MPITest(npes=[1])
subroutine ModBoundingBoxTest_contains_3d(this)

  use pFUnit_mod
  use ModBoundingBox
  use ModGlobal
  implicit none

  class(MPITestCase), intent(inout) :: this

  type(t_bbox) :: BBox1, BBox2, BBox3

  BBox1 = t_bbox_(3, [2._rk, 4._rk, 6._rk], [3._rk, 5._rk, 7._rk])

  ! Self
  BBox2 = BBox1
  @AssertTrue(BBContains(BBox1, BBox2))

  ! Smaller
  BBox2 = t_bbox_(3, [2.25_rk, 4.25_rk, 6.25_rk], [2.75_rk, 4.75_rk, 6.75_rk])
  @AssertTrue(BBContains(BBox1, BBox2))

  ! Shifted left in x
  BBox2 = t_bbox_(3, [1.5_rk, 4._rk, 6._rk], [2.5_rk, 5._rk, 7._rk])
  @AssertFalse(BBContains(BBox1, BBox2))

  ! Shifted right in x
  BBox2 = t_bbox_(3, [2.5_rk, 4._rk, 6._rk], [3.5_rk, 5._rk, 7._rk])
  @AssertFalse(BBContains(BBox1, BBox2))

  ! Shifted left in y
  BBox2 = t_bbox_(3, [2._rk, 3.5_rk, 6._rk], [3._rk, 4.5_rk, 7._rk])
  @AssertFalse(BBContains(BBox1, BBox2))

  ! Shifted right in y
  BBox2 = t_bbox_(3, [2._rk, 4.5_rk, 6._rk], [3._rk, 5.5_rk, 7._rk])
  @AssertFalse(BBContains(BBox1, BBox2))

  ! Shifted left in z
  BBox2 = t_bbox_(3, [2._rk, 4._rk, 5.5_rk], [3._rk, 5._rk, 6.5_rk])
  @AssertFalse(BBContains(BBox1, BBox2))

  ! Shifted right in z
  BBox2 = t_bbox_(3, [2._rk, 4._rk, 6.5_rk], [3._rk, 5._rk, 7.5_rk])
  @AssertFalse(BBContains(BBox1, BBox2))

  ! Larger
  BBox2 = t_bbox_(3, [1.5_rk, 3.5_rk, 5.5_rk], [3.5_rk, 5.5_rk, 7.5_rk])
  @AssertFalse(BBContains(BBox1, BBox2))

  ! Left empty
  BBox2 = t_bbox_(3, [3._rk, 5._rk, 7._rk], [2._rk, 4._rk, 6._rk])
  BBox3 = t_bbox_(3, [4._rk, 6._rk, 8._rk], [1._rk, 3._rk, 5._rk])
  @AssertFalse(BBContains(BBox2, BBox3))

  ! Right empty
  BBox2 = t_bbox_(3, [2._rk, 4._rk, 6._rk], [3._rk, 5._rk, 7._rk])
  BBox3 = t_bbox_(3, [4._rk, 6._rk, 8._rk], [1._rk, 3._rk, 5._rk])
  @AssertFalse(BBContains(BBox2, BBox3))

end subroutine ModBoundingBoxTest_contains_3d

@MPITest(npes=[1])
subroutine ModBoundingBoxTest_contains_point_2d(this)

  use pFUnit_mod
  use ModBoundingBox
  use ModGlobal
  implicit none

  class(MPITestCase), intent(inout) :: this

  type(t_bbox) :: BBox

  BBox = t_bbox_(2, [2._rk, 4._rk], [3._rk, 5._rk])

  ! Inside
  @AssertTrue(BBContainsPoint(BBox, [2.5_rk, 4.5_rk]))

  ! On edge
  @AssertTrue(BBContainsPoint(BBox, [2._rk, 4._rk]))
  @AssertTrue(BBContainsPoint(BBox, [3._rk, 4._rk]))
  @AssertTrue(BBContainsPoint(BBox, [2._rk, 5._rk]))
  @AssertTrue(BBContainsPoint(BBox, [3._rk, 5._rk]))

  ! Outside
  @AssertFalse(BBContainsPoint(BBox, [1.5_rk, 4.5_rk]))
  @AssertFalse(BBContainsPoint(BBox, [3.5_rk, 4.5_rk]))
  @AssertFalse(BBContainsPoint(BBox, [2.5_rk, 3.5_rk]))
  @AssertFalse(BBContainsPoint(BBox, [2.5_rk, 5.5_rk]))

  ! Empty interval
  BBox = t_bbox_(2, [3._rk, 5._rk], [2._rk, 4._rk])
  @AssertFalse(BBContainsPoint(BBox, [2.5_rk, 4.5_rk]))

end subroutine ModBoundingBoxTest_contains_point_2d

@MPITest(npes=[1])
subroutine ModBoundingBoxTest_contains_point_3d(this)

  use pFUnit_mod
  use ModBoundingBox
  use ModGlobal
  implicit none

  class(MPITestCase), intent(inout) :: this

  type(t_bbox) :: BBox

  BBox = t_bbox_(3, [2._rk, 4._rk, 6._rk], [3._rk, 5._rk, 7._rk])

  ! Inside
  @AssertTrue(BBContainsPoint(BBox, [2.5_rk, 4.5_rk, 6.5_rk]))

  ! On edge
  @AssertTrue(BBContainsPoint(BBox, [2._rk, 4._rk, 6._rk]))
  @AssertTrue(BBContainsPoint(BBox, [3._rk, 4._rk, 6._rk]))
  @AssertTrue(BBContainsPoint(BBox, [2._rk, 5._rk, 6._rk]))
  @AssertTrue(BBContainsPoint(BBox, [3._rk, 5._rk, 6._rk]))
  @AssertTrue(BBContainsPoint(BBox, [2._rk, 4._rk, 7._rk]))
  @AssertTrue(BBContainsPoint(BBox, [3._rk, 4._rk, 7._rk]))
  @AssertTrue(BBContainsPoint(BBox, [2._rk, 5._rk, 7._rk]))
  @AssertTrue(BBContainsPoint(BBox, [3._rk, 5._rk, 7._rk]))

  ! Outside
  @AssertFalse(BBContainsPoint(BBox, [1.5_rk, 4.5_rk, 6.5_rk]))
  @AssertFalse(BBContainsPoint(BBox, [3.5_rk, 4.5_rk, 6.5_rk]))
  @AssertFalse(BBContainsPoint(BBox, [2.5_rk, 3.5_rk, 6.5_rk]))
  @AssertFalse(BBContainsPoint(BBox, [2.5_rk, 5.5_rk, 6.5_rk]))
  @AssertFalse(BBContainsPoint(BBox, [2.5_rk, 4.5_rk, 5.5_rk]))
  @AssertFalse(BBContainsPoint(BBox, [2.5_rk, 4.5_rk, 7.5_rk]))

  ! Empty interval
  BBox = t_bbox_(3, [3._rk, 5._rk, 7._rk], [2._rk, 4._rk, 6._rk])
  @AssertFalse(BBContainsPoint(BBox, [2.5_rk, 4.5_rk, 6.5_rk]))

end subroutine ModBoundingBoxTest_contains_point_3d

@MPITest(npes=[1])
subroutine ModBoundingBoxTest_empty_2d(this)

  use pFUnit_mod
  use ModBoundingBox
  use ModGlobal
  implicit none

  class(MPITestCase), intent(inout) :: this

  type(t_bbox) :: BBox

  ! Interval
  BBox = t_bbox_(2, [2._rk, 4._rk], [3._rk, 5._rk])
  @AssertFalse(BBIsEmpty(BBox))

  ! Edge x
  BBox = t_bbox_(2, [2._rk, 4._rk], [3._rk, 4._rk])
  @AssertFalse(BBIsEmpty(BBox))

  ! Edge y
  BBox = t_bbox_(2, [2._rk, 4._rk], [2._rk, 5._rk])
  @AssertFalse(BBIsEmpty(BBox))

  ! Point
  BBox = t_bbox_(2, [2._rk, 4._rk], [2._rk, 4._rk])
  @AssertFalse(BBIsEmpty(BBox))

  ! Empty
  BBox = t_bbox_(2, [2._rk, 4._rk], [1._rk, 3._rk])
  @AssertTrue(BBIsEmpty(BBox))

end subroutine ModBoundingBoxTest_empty_2d

@MPITest(npes=[1])
subroutine ModBoundingBoxTest_empty_3d(this)

  use pFUnit_mod
  use ModBoundingBox
  use ModGlobal
  implicit none

  class(MPITestCase), intent(inout) :: this

  type(t_bbox) :: BBox

  ! Interval
  BBox = t_bbox_(3, [2._rk, 4._rk, 6._rk], [3._rk, 5._rk, 7._rk])
  @AssertFalse(BBIsEmpty(BBox))

  ! Face x
  BBox = t_bbox_(3, [2._rk, 4._rk, 6._rk], [2._rk, 5._rk, 7._rk])
  @AssertFalse(BBIsEmpty(BBox))

  ! Face y
  BBox = t_bbox_(3, [2._rk, 4._rk, 6._rk], [3._rk, 4._rk, 7._rk])
  @AssertFalse(BBIsEmpty(BBox))

  ! Face z
  BBox = t_bbox_(3, [2._rk, 4._rk, 6._rk], [3._rk, 5._rk, 6._rk])
  @AssertFalse(BBIsEmpty(BBox))

  ! Edge x
  BBox = t_bbox_(3, [2._rk, 4._rk, 6._rk], [3._rk, 4._rk, 6._rk])
  @AssertFalse(BBIsEmpty(BBox))

  ! Edge y
  BBox = t_bbox_(3, [2._rk, 4._rk, 6._rk], [2._rk, 5._rk, 6._rk])
  @AssertFalse(BBIsEmpty(BBox))

  ! Edge z
  BBox = t_bbox_(3, [2._rk, 4._rk, 6._rk], [2._rk, 4._rk, 7._rk])
  @AssertFalse(BBIsEmpty(BBox))

  ! Point
  BBox = t_bbox_(3, [2._rk, 4._rk, 6._rk], [2._rk, 4._rk, 6._rk])
  @AssertFalse(BBIsEmpty(BBox))

  ! Empty
  BBox = t_bbox_(3, [2._rk, 4._rk, 6._rk], [1._rk, 3._rk, 5._rk])
  @AssertTrue(BBIsEmpty(BBox))

end subroutine ModBoundingBoxTest_empty_3d

@MPITest(npes=[1])
subroutine ModBoundingBoxTest_size_2d(this)

  use pFUnit_mod
  use ModBoundingBox
  use ModGlobal
  implicit none

  class(MPITestCase), intent(inout) :: this

  type(t_bbox) :: BBox

  ! Interval
  BBox = t_bbox_(2, [2._rk, 4._rk], [3._rk, 5._rk])
  @AssertEqual([1._rk, 1._rk], BBSize(BBox))

  ! Edge x
  BBox = t_bbox_(2, [2._rk, 4._rk], [3._rk, 4._rk])
  @AssertEqual([1._rk, 0._rk], BBSize(BBox))

  ! Edge y
  BBox = t_bbox_(2, [2._rk, 4._rk], [2._rk, 5._rk])
  @AssertEqual([0._rk, 1._rk], BBSize(BBox))

  ! Point
  BBox = t_bbox_(2, [2._rk, 4._rk], [2._rk, 4._rk])
  @AssertEqual([0._rk, 0._rk], BBSize(BBox))

  ! Empty
  BBox = t_bbox_(2, [2._rk, 4._rk], [1._rk, 3._rk])
  @AssertEqual([0._rk, 0._rk], BBSize(BBox))

end subroutine ModBoundingBoxTest_size_2d

@MPITest(npes=[1])
subroutine ModBoundingBoxTest_size_3d(this)

  use pFUnit_mod
  use ModBoundingBox
  use ModGlobal
  implicit none

  class(MPITestCase), intent(inout) :: this

  type(t_bbox) :: BBox

  ! Interval
  BBox = t_bbox_(3, [2._rk, 4._rk, 6._rk], [3._rk, 5._rk, 7._rk])
  @AssertEqual([1._rk, 1._rk, 1._rk], BBSize(BBox))

  ! Face x
  BBox = t_bbox_(3, [2._rk, 4._rk, 6._rk], [2._rk, 5._rk, 7._rk])
  @AssertEqual([0._rk, 1._rk, 1._rk], BBSize(BBox))

  ! Face y
  BBox = t_bbox_(3, [2._rk, 4._rk, 6._rk], [3._rk, 4._rk, 7._rk])
  @AssertEqual([1._rk, 0._rk, 1._rk], BBSize(BBox))

  ! Face z
  BBox = t_bbox_(3, [2._rk, 4._rk, 6._rk], [3._rk, 5._rk, 6._rk])
  @AssertEqual([1._rk, 1._rk, 0._rk], BBSize(BBox))

  ! Edge x
  BBox = t_bbox_(3, [2._rk, 4._rk, 6._rk], [3._rk, 4._rk, 6._rk])
  @AssertEqual([1._rk, 0._rk, 0._rk], BBSize(BBox))

  ! Edge y
  BBox = t_bbox_(3, [2._rk, 4._rk, 6._rk], [2._rk, 5._rk, 6._rk])
  @AssertEqual([0._rk, 1._rk, 0._rk], BBSize(BBox))

  ! Edge z
  BBox = t_bbox_(3, [2._rk, 4._rk, 6._rk], [2._rk, 4._rk, 7._rk])
  @AssertEqual([0._rk, 0._rk, 1._rk], BBSize(BBox))

  ! Point
  BBox = t_bbox_(3, [2._rk, 4._rk, 6._rk], [2._rk, 4._rk, 6._rk])
  @AssertEqual([0._rk, 0._rk, 0._rk], BBSize(BBox))

  ! Empty
  BBox = t_bbox_(3, [2._rk, 4._rk, 6._rk], [1._rk, 3._rk, 5._rk])
  @AssertEqual([0._rk, 0._rk, 0._rk], BBSize(BBox))

end subroutine ModBoundingBoxTest_size_3d

@MPITest(npes=[1])
subroutine ModBoundingBoxTest_move_2d(this)

  use pFUnit_mod
  use ModBoundingBox
  use ModGlobal
  implicit none

  class(MPITestCase), intent(inout) :: this

  type(t_bbox) :: BBox1, BBox2

  BBox1 = t_bbox_(2, [2._rk, 4._rk], [3._rk, 5._rk])

  ! Zero
  BBox2 = BBMove(BBox1, [0._rk, 0._rk])
  @AssertEqual(2, BBox2%nd)
  @AssertEqual([2._rk, 4._rk, 0._rk], BBox2%b)
  @AssertEqual([3._rk, 5._rk, 0._rk], BBox2%e)

  ! Positive x
  BBox2 = BBMove(BBox1, [0.5_rk, 0._rk])
  @AssertEqual(2, BBox2%nd)
  @AssertEqual([2.5_rk, 4._rk, 0._rk], BBox2%b)
  @AssertEqual([3.5_rk, 5._rk, 0._rk], BBox2%e)

  ! Negative x
  BBox2 = BBMove(BBox1, [-0.5_rk, 0._rk])
  @AssertEqual(2, BBox2%nd)
  @AssertEqual([1.5_rk, 4._rk, 0._rk], BBox2%b)
  @AssertEqual([2.5_rk, 5._rk, 0._rk], BBox2%e)

  ! Positive y
  BBox2 = BBMove(BBox1, [0._rk, 0.5_rk])
  @AssertEqual(2, BBox2%nd)
  @AssertEqual([2._rk, 4.5_rk, 0._rk], BBox2%b)
  @AssertEqual([3._rk, 5.5_rk, 0._rk], BBox2%e)

  ! Negative y
  BBox2 = BBMove(BBox1, [0._rk, -0.5_rk])
  @AssertEqual(2, BBox2%nd)
  @AssertEqual([2._rk, 3.5_rk, 0._rk], BBox2%b)
  @AssertEqual([3._rk, 4.5_rk, 0._rk], BBox2%e)

  ! All
  BBox2 = BBMove(BBox1, [0.5_rk, -0.5_rk])
  @AssertEqual(2, BBox2%nd)
  @AssertEqual([2.5_rk, 3.5_rk, 0._rk], BBox2%b)
  @AssertEqual([3.5_rk, 4.5_rk, 0._rk], BBox2%e)

end subroutine ModBoundingBoxTest_move_2d

@MPITest(npes=[1])
subroutine ModBoundingBoxTest_move_3d(this)

  use pFUnit_mod
  use ModBoundingBox
  use ModGlobal
  implicit none

  class(MPITestCase), intent(inout) :: this

  type(t_bbox) :: BBox1, BBox2

  BBox1 = t_bbox_(3, [2._rk, 4._rk, 6._rk], [3._rk, 5._rk, 7._rk])

  ! Zero
  BBox2 = BBMove(BBox1, [0._rk, 0._rk, 0._rk])
  @AssertEqual(3, BBox2%nd)
  @AssertEqual([2._rk, 4._rk, 6._rk], BBox2%b)
  @AssertEqual([3._rk, 5._rk, 7._rk], BBox2%e)

  ! Positive x
  BBox2 = BBMove(BBox1, [0.5_rk, 0._rk, 0._rk])
  @AssertEqual(3, BBox2%nd)
  @AssertEqual([2.5_rk, 4._rk, 6._rk], BBox2%b)
  @AssertEqual([3.5_rk, 5._rk, 7._rk], BBox2%e)

  ! Negative x
  BBox2 = BBMove(BBox1, [-0.5_rk, 0._rk, 0._rk])
  @AssertEqual(3, BBox2%nd)
  @AssertEqual([1.5_rk, 4._rk, 6._rk], BBox2%b)
  @AssertEqual([2.5_rk, 5._rk, 7._rk], BBox2%e)

  ! Positive y
  BBox2 = BBMove(BBox1, [0._rk, 0.5_rk, 0._rk])
  @AssertEqual(3, BBox2%nd)
  @AssertEqual([2._rk, 4.5_rk, 6._rk], BBox2%b)
  @AssertEqual([3._rk, 5.5_rk, 7._rk], BBox2%e)

  ! Negative y
  BBox2 = BBMove(BBox1, [0._rk, -0.5_rk, 0._rk])
  @AssertEqual(3, BBox2%nd)
  @AssertEqual([2._rk, 3.5_rk, 6._rk], BBox2%b)
  @AssertEqual([3._rk, 4.5_rk, 7._rk], BBox2%e)

  ! Positive z
  BBox2 = BBMove(BBox1, [0._rk, 0._rk, 0.5_rk])
  @AssertEqual(3, BBox2%nd)
  @AssertEqual([2._rk, 4._rk, 6.5_rk], BBox2%b)
  @AssertEqual([3._rk, 5._rk, 7.5_rk], BBox2%e)

  ! Negative z
  BBox2 = BBMove(BBox1, [0._rk, 0._rk, -0.5_rk])
  @AssertEqual(3, BBox2%nd)
  @AssertEqual([2._rk, 4._rk, 5.5_rk], BBox2%b)
  @AssertEqual([3._rk, 5._rk, 6.5_rk], BBox2%e)

  ! All
  BBox2 = BBMove(BBox1, [0.5_rk, -0.5_rk, 0.5_rk])
  @AssertEqual(3, BBox2%nd)
  @AssertEqual([2.5_rk, 3.5_rk, 6.5_rk], BBox2%b)
  @AssertEqual([3.5_rk, 4.5_rk, 7.5_rk], BBox2%e)

end subroutine ModBoundingBoxTest_move_3d

@MPITest(npes=[1])
subroutine ModBoundingBoxTest_grow_2d(this)

  use pFUnit_mod
  use ModBoundingBox
  use ModGlobal
  implicit none

  class(MPITestCase), intent(inout) :: this

  type(t_bbox) :: BBox1, BBox2

  BBox1 = t_bbox_(2, [2._rk, 4._rk], [3._rk, 5._rk])

  ! Zero
  BBox2 = BBGrow(BBox1, 0._rk)
  @AssertEqual(2, BBox2%nd)
  @AssertEqual([2._rk, 4._rk, 0._rk], BBox2%b)
  @AssertEqual([3._rk, 5._rk, 0._rk], BBox2%e)

  ! Positive
  BBox2 = BBGrow(BBox1, 0.5_rk)
  @AssertEqual(2, BBox2%nd)
  @AssertEqual([1.5_rk, 3.5_rk, 0._rk], BBox2%b)
  @AssertEqual([3.5_rk, 5.5_rk, 0._rk], BBox2%e)

  ! Negative
  BBox2 = BBGrow(BBox1, -0.5_rk)
  @AssertEqual(2, BBox2%nd)
  @AssertEqual([2.5_rk, 4.5_rk, 0._rk], BBox2%b)
  @AssertEqual([2.5_rk, 4.5_rk, 0._rk], BBox2%e)

end subroutine ModBoundingBoxTest_grow_2d

@MPITest(npes=[1])
subroutine ModBoundingBoxTest_grow_3d(this)

  use pFUnit_mod
  use ModBoundingBox
  use ModGlobal
  implicit none

  class(MPITestCase), intent(inout) :: this

  type(t_bbox) :: BBox1, BBox2

  BBox1 = t_bbox_(3, [2._rk, 4._rk, 6._rk], [3._rk, 5._rk, 7._rk])

  ! Zero
  BBox2 = BBGrow(BBox1, 0._rk)
  @AssertEqual(3, BBox2%nd)
  @AssertEqual([2._rk, 4._rk, 6._rk], BBox2%b)
  @AssertEqual([3._rk, 5._rk, 7._rk], BBox2%e)

  ! Positive
  BBox2 = BBGrow(BBox1, 0.5_rk)
  @AssertEqual(3, BBox2%nd)
  @AssertEqual([1.5_rk, 3.5_rk, 5.5_rk], BBox2%b)
  @AssertEqual([3.5_rk, 5.5_rk, 7.5_rk], BBox2%e)

  ! Negative
  BBox2 = BBGrow(BBox1, -0.5_rk)
  @AssertEqual(3, BBox2%nd)
  @AssertEqual([2.5_rk, 4.5_rk, 6.5_rk], BBox2%b)
  @AssertEqual([2.5_rk, 4.5_rk, 6.5_rk], BBox2%e)

end subroutine ModBoundingBoxTest_grow_3d

@MPITest(npes=[1])
subroutine ModBoundingBoxTest_scale_2d(this)

  use pFUnit_mod
  use ModBoundingBox
  use ModGlobal
  implicit none

  class(MPITestCase), intent(inout) :: this

  type(t_bbox) :: BBox1, BBox2

  BBox1 = t_bbox_(2, [2._rk, 4._rk], [3._rk, 5._rk])

  ! One
  BBox2 = BBScale(BBox1, 1._rk)
  @AssertEqual(2, BBox2%nd)
  @AssertEqual([2._rk, 4._rk, 0._rk], BBox2%b)
  @AssertEqual([3._rk, 5._rk, 0._rk], BBox2%e)

  ! Zero
  BBox2 = BBScale(BBox1, 0._rk)
  @AssertEqual(2, BBox2%nd)
  @AssertEqual([2.5_rk, 4.5_rk, 0._rk], BBox2%b)
  @AssertEqual([2.5_rk, 4.5_rk, 0._rk], BBox2%e)

  ! Greater than one
  BBox2 = BBScale(BBox1, 2._rk)
  @AssertEqual(2, BBox2%nd)
  @AssertEqual([1.5_rk, 3.5_rk, 0._rk], BBox2%b)
  @AssertEqual([3.5_rk, 5.5_rk, 0._rk], BBox2%e)

  ! Less than one
  BBox2 = BBScale(BBox1, 0.5_rk)
  @AssertEqual(2, BBox2%nd)
  @AssertEqual([2.25_rk, 4.25_rk, 0._rk], BBox2%b)
  @AssertEqual([2.75_rk, 4.75_rk, 0._rk], BBox2%e)

end subroutine ModBoundingBoxTest_scale_2d

@MPITest(npes=[1])
subroutine ModBoundingBoxTest_scale_3d(this)

  use pFUnit_mod
  use ModBoundingBox
  use ModGlobal
  implicit none

  class(MPITestCase), intent(inout) :: this

  type(t_bbox) :: BBox1, BBox2

  BBox1 = t_bbox_(3, [2._rk, 4._rk, 6._rk], [3._rk, 5._rk, 7._rk])

  ! One
  BBox2 = BBScale(BBox1, 1._rk)
  @AssertEqual(3, BBox2%nd)
  @AssertEqual([2._rk, 4._rk, 6._rk], BBox2%b)
  @AssertEqual([3._rk, 5._rk, 7._rk], BBox2%e)

  ! Zero
  BBox2 = BBScale(BBox1, 0._rk)
  @AssertEqual(3, BBox2%nd)
  @AssertEqual([2.5_rk, 4.5_rk, 6.5_rk], BBox2%b)
  @AssertEqual([2.5_rk, 4.5_rk, 6.5_rk], BBox2%e)

  ! Greater than one
  BBox2 = BBScale(BBox1, 2._rk)
  @AssertEqual(3, BBox2%nd)
  @AssertEqual([1.5_rk, 3.5_rk, 5.5_rk], BBox2%b)
  @AssertEqual([3.5_rk, 5.5_rk, 7.5_rk], BBox2%e)

  ! Less than one
  BBox2 = BBScale(BBox1, 0.5_rk)
  @AssertEqual(3, BBox2%nd)
  @AssertEqual([2.25_rk, 4.25_rk, 6.25_rk], BBox2%b)
  @AssertEqual([2.75_rk, 4.75_rk, 6.75_rk], BBox2%e)

end subroutine ModBoundingBoxTest_scale_3d

@MPITest(npes=[1])
subroutine ModBoundingBoxTest_extend_2d(this)

  use pFUnit_mod
  use ModBoundingBox
  use ModGlobal
  implicit none

  class(MPITestCase), intent(inout) :: this

  type(t_bbox) :: BBox1, BBox2

  BBox1 = t_bbox_(2, [2._rk, 4._rk], [3._rk, 5._rk])

  ! Contained
  BBox2 = BBExtend(BBox1, [2.5_rk, 4.5_rk])
  @AssertEqual(2, BBox2%nd)
  @AssertEqual([2._rk, 4._rk, 0._rk], BBox2%b)
  @AssertEqual([3._rk, 5._rk, 0._rk], BBox2%e)

  ! Outside lower
  BBox2 = BBExtend(BBox1, [1.5_rk, 3.5_rk])
  @AssertEqual(2, BBox2%nd)
  @AssertEqual([1.5_rk, 3.5_rk, 0._rk], BBox2%b)
  @AssertEqual([3._rk, 5._rk, 0._rk], BBox2%e)

  ! Outside upper
  BBox2 = BBExtend(BBox1, [3.5_rk, 5.5_rk])
  @AssertEqual(2, BBox2%nd)
  @AssertEqual([2._rk, 4._rk, 0._rk], BBox2%b)
  @AssertEqual([3.5_rk, 5.5_rk, 0._rk], BBox2%e)

end subroutine ModBoundingBoxTest_extend_2d

@MPITest(npes=[1])
subroutine ModBoundingBoxTest_extend_3d(this)

  use pFUnit_mod
  use ModBoundingBox
  use ModGlobal
  implicit none

  class(MPITestCase), intent(inout) :: this

  type(t_bbox) :: BBox1, BBox2

  BBox1 = t_bbox_(3, [2._rk, 4._rk, 6._rk], [3._rk, 5._rk, 7._rk])

  ! Contained
  BBox2 = BBExtend(BBox1, [2.5_rk, 4.5_rk, 6.5_rk])
  @AssertEqual(3, BBox2%nd)
  @AssertEqual([2._rk, 4._rk, 6._rk], BBox2%b)
  @AssertEqual([3._rk, 5._rk, 7._rk], BBox2%e)

  ! Outside lower
  BBox2 = BBExtend(BBox1, [1.5_rk, 3.5_rk, 5.5_rk])
  @AssertEqual(3, BBox2%nd)
  @AssertEqual([1.5_rk, 3.5_rk, 5.5_rk], BBox2%b)
  @AssertEqual([3._rk, 5._rk, 7._rk], BBox2%e)

  ! Outside upper
  BBox2 = BBExtend(BBox1, [3.5_rk, 5.5_rk, 7.5_rk])
  @AssertEqual(3, BBox2%nd)
  @AssertEqual([2._rk, 4._rk, 6._rk], BBox2%b)
  @AssertEqual([3.5_rk, 5.5_rk, 7.5_rk], BBox2%e)

end subroutine ModBoundingBoxTest_extend_3d

@MPITest(npes=[1])
subroutine ModBoundingBoxTest_union_2d(this)

  use pFUnit_mod
  use ModBoundingBox
  use ModGlobal
  implicit none

  class(MPITestCase), intent(inout) :: this

  type(t_bbox) :: BBox1, BBox2

  BBox1 = t_bbox_(2, [2._rk, 4._rk], [3._rk, 5._rk])

  ! Self
  BBox2 = BBUnion(BBox1, BBox1)
  @AssertEqual(2, BBox2%nd)
  @AssertEqual([2._rk, 4._rk, 0._rk], BBox2%b)
  @AssertEqual([3._rk, 5._rk, 0._rk], BBox2%e)

  ! Smaller
  BBox2 = BBUnion(BBox1, t_bbox_(2, [2.25_rk, 4.25_rk], [2.75_rk, 4.75_rk]))
  @AssertEqual(2, BBox2%nd)
  @AssertEqual([2._rk, 4._rk, 0._rk], BBox2%b)
  @AssertEqual([3._rk, 5._rk, 0._rk], BBox2%e)

  ! Shifted left in x
  BBox2 = BBUnion(BBox1, t_bbox_(2, [1.5_rk, 4._rk], [2.5_rk, 5._rk]))
  @AssertEqual(2, BBox2%nd)
  @AssertEqual([1.5_rk, 4._rk, 0._rk], BBox2%b)
  @AssertEqual([3._rk, 5._rk, 0._rk], BBox2%e)

  ! Shifted right in x
  BBox2 = BBUnion(BBox1, t_bbox_(2, [2.5_rk, 4._rk], [3.5_rk, 5._rk]))
  @AssertEqual(2, BBox2%nd)
  @AssertEqual([2._rk, 4._rk, 0._rk], BBox2%b)
  @AssertEqual([3.5_rk, 5._rk, 0._rk], BBox2%e)

  ! Shifted left in y
  BBox2 = BBUnion(BBox1, t_bbox_(2, [2._rk, 3.5_rk], [3._rk, 4.5_rk]))
  @AssertEqual(2, BBox2%nd)
  @AssertEqual([2._rk, 3.5_rk, 0._rk], BBox2%b)
  @AssertEqual([3._rk, 5._rk, 0._rk], BBox2%e)

  ! Shifted right in y
  BBox2 = BBUnion(BBox1, t_bbox_(2, [2._rk, 4.5_rk], [3._rk, 5.5_rk]))
  @AssertEqual(2, BBox2%nd)
  @AssertEqual([2._rk, 4._rk, 0._rk], BBox2%b)
  @AssertEqual([3._rk, 5.5_rk, 0._rk], BBox2%e)

  ! Larger
  BBox2 = BBUnion(BBox1, t_bbox_(2, [1.5_rk, 3.5_rk], [3.5_rk, 5.5_rk]))
  @AssertEqual(2, BBox2%nd)
  @AssertEqual([1.5_rk, 3.5_rk, 0._rk], BBox2%b)
  @AssertEqual([3.5_rk, 5.5_rk, 0._rk], BBox2%e)

end subroutine ModBoundingBoxTest_union_2d

@MPITest(npes=[1])
subroutine ModBoundingBoxTest_union_3d(this)

  use pFUnit_mod
  use ModBoundingBox
  use ModGlobal
  implicit none

  class(MPITestCase), intent(inout) :: this

  type(t_bbox) :: BBox1, BBox2

  BBox1 = t_bbox_(3, [2._rk, 4._rk, 6._rk], [3._rk, 5._rk, 7._rk])

  ! Self
  BBox2 = BBUnion(BBox1, BBox1)
  @AssertEqual(3, BBox2%nd)
  @AssertEqual([2._rk, 4._rk, 6._rk], BBox2%b)
  @AssertEqual([3._rk, 5._rk, 7._rk], BBox2%e)

  ! Smaller
  BBox2 = BBUnion(BBox1, t_bbox_(3, [2.25_rk, 4.25_rk, 6.25_rk], [2.75_rk, 4.75_rk, 6.75_rk]))
  @AssertEqual(3, BBox2%nd)
  @AssertEqual([2._rk, 4._rk, 6._rk], BBox2%b)
  @AssertEqual([3._rk, 5._rk, 7._rk], BBox2%e)

  ! Shifted left in x
  BBox2 = BBUnion(BBox1, t_bbox_(3, [1.5_rk, 4._rk, 6._rk], [2.5_rk, 5._rk, 7._rk]))
  @AssertEqual(3, BBox2%nd)
  @AssertEqual([1.5_rk, 4._rk, 6._rk], BBox2%b)
  @AssertEqual([3._rk, 5._rk, 7._rk], BBox2%e)

  ! Shifted right in x
  BBox2 = BBUnion(BBox1, t_bbox_(3, [2.5_rk, 4._rk, 6._rk], [3.5_rk, 5._rk, 7._rk]))
  @AssertEqual(3, BBox2%nd)
  @AssertEqual([2._rk, 4._rk, 6._rk], BBox2%b)
  @AssertEqual([3.5_rk, 5._rk, 7._rk], BBox2%e)

  ! Shifted left in y
  BBox2 = BBUnion(BBox1, t_bbox_(3, [2._rk, 3.5_rk, 6._rk], [3._rk, 4.5_rk, 7._rk]))
  @AssertEqual(3, BBox2%nd)
  @AssertEqual([2._rk, 3.5_rk, 6._rk], BBox2%b)
  @AssertEqual([3._rk, 5._rk, 7._rk], BBox2%e)

  ! Shifted right in y
  BBox2 = BBUnion(BBox1, t_bbox_(3, [2._rk, 4.5_rk, 6._rk], [3._rk, 5.5_rk, 7._rk]))
  @AssertEqual(3, BBox2%nd)
  @AssertEqual([2._rk, 4._rk, 6._rk], BBox2%b)
  @AssertEqual([3._rk, 5.5_rk, 7._rk], BBox2%e)

  ! Shifted left in z
  BBox2 = BBUnion(BBox1, t_bbox_(3, [2._rk, 4._rk, 5.5_rk], [3._rk, 5._rk, 6.5_rk]))
  @AssertEqual(3, BBox2%nd)
  @AssertEqual([2._rk, 4._rk, 5.5_rk], BBox2%b)
  @AssertEqual([3._rk, 5._rk, 7._rk], BBox2%e)

  ! Shifted right in z
  BBox2 = BBUnion(BBox1, t_bbox_(3, [2._rk, 4._rk, 6._rk], [3._rk, 5._rk, 7.5_rk]))
  @AssertEqual(3, BBox2%nd)
  @AssertEqual([2._rk, 4._rk, 6._rk], BBox2%b)
  @AssertEqual([3._rk, 5._rk, 7.5_rk], BBox2%e)

  ! Larger
  BBox2 = BBUnion(BBox1, t_bbox_(3, [1.5_rk, 3.5_rk, 5.5_rk], [3.5_rk, 5.5_rk, 7.5_rk]))
  @AssertEqual(3, BBox2%nd)
  @AssertEqual([1.5_rk, 3.5_rk, 5.5_rk], BBox2%b)
  @AssertEqual([3.5_rk, 5.5_rk, 7.5_rk], BBox2%e)

end subroutine ModBoundingBoxTest_union_3d

@MPITest(npes=[1])
subroutine ModBoundingBoxTest_intersect_2d(this)

  use pFUnit_mod
  use ModBoundingBox
  use ModGlobal
  implicit none

  class(MPITestCase), intent(inout) :: this

  type(t_bbox) :: BBox1, BBox2

  BBox1 = t_bbox_(2, [2._rk, 4._rk], [3._rk, 5._rk])

  ! Self
  BBox2 = BBIntersect(BBox1, BBox1)
  @AssertEqual(2, BBox2%nd)
  @AssertEqual([2._rk, 4._rk, 0._rk], BBox2%b)
  @AssertEqual([3._rk, 5._rk, 0._rk], BBox2%e)

  ! Smaller
  BBox2 = BBIntersect(BBox1, t_bbox_(2, [2.25_rk, 4.25_rk], [2.75_rk, 4.75_rk]))
  @AssertEqual(2, BBox2%nd)
  @AssertEqual([2.25_rk, 4.25_rk, 0._rk], BBox2%b)
  @AssertEqual([2.75_rk, 4.75_rk, 0._rk], BBox2%e)

  ! Shifted left in x
  BBox2 = BBIntersect(BBox1, t_bbox_(2, [1.5_rk, 4._rk], [2.5_rk, 5._rk]))
  @AssertEqual(2, BBox2%nd)
  @AssertEqual([2._rk, 4._rk, 0._rk], BBox2%b)
  @AssertEqual([2.5_rk, 5._rk, 0._rk], BBox2%e)

  ! Shifted right in x
  BBox2 = BBIntersect(BBox1, t_bbox_(2, [2.5_rk, 4._rk], [3.5_rk, 5._rk]))
  @AssertEqual(2, BBox2%nd)
  @AssertEqual([2.5_rk, 4._rk, 0._rk], BBox2%b)
  @AssertEqual([3._rk, 5._rk, 0._rk], BBox2%e)

  ! Shifted left in y
  BBox2 = BBIntersect(BBox1, t_bbox_(2, [2._rk, 3.5_rk], [3._rk, 4.5_rk]))
  @AssertEqual(2, BBox2%nd)
  @AssertEqual([2._rk, 4._rk, 0._rk], BBox2%b)
  @AssertEqual([3._rk, 4.5_rk, 0._rk], BBox2%e)

  ! Shifted right in y
  BBox2 = BBIntersect(BBox1, t_bbox_(2, [2._rk, 4.5_rk], [3._rk, 5.5_rk]))
  @AssertEqual(2, BBox2%nd)
  @AssertEqual([2._rk, 4.5_rk, 0._rk], BBox2%b)
  @AssertEqual([3._rk, 5._rk, 0._rk], BBox2%e)

  ! Larger
  BBox2 = BBIntersect(BBox1, t_bbox_(2, [1.5_rk, 3.5_rk], [3.5_rk, 5.5_rk]))
  @AssertEqual(2, BBox2%nd)
  @AssertEqual([2._rk, 4._rk, 0._rk], BBox2%b)
  @AssertEqual([3._rk, 5._rk, 0._rk], BBox2%e)

  ! Non-overlapping positive
  BBox2 = BBIntersect(BBox1, t_bbox_(2, [4._rk, 6._rk], [5._rk, 7._rk]))
  @AssertEqual(2, BBox2%nd)
  @AssertTrue(BBIsEmpty(BBox2))

  ! Non-overlapping negative
  BBox2 = BBIntersect(BBox1, t_bbox_(2, [0._rk, 2._rk], [1._rk, 3._rk]))
  @AssertEqual(2, BBox2%nd)
  @AssertTrue(BBIsEmpty(BBox2))

end subroutine ModBoundingBoxTest_intersect_2d

@MPITest(npes=[1])
subroutine ModBoundingBoxTest_intersect_3d(this)

  use pFUnit_mod
  use ModBoundingBox
  use ModGlobal
  implicit none

  class(MPITestCase), intent(inout) :: this

  type(t_bbox) :: BBox1, BBox2

  BBox1 = t_bbox_(3, [2._rk, 4._rk, 6._rk], [3._rk, 5._rk, 7._rk])

  ! Self
  BBox2 = BBIntersect(BBox1, BBox1)
  @AssertEqual(3, BBox2%nd)
  @AssertEqual([2._rk, 4._rk, 6._rk], BBox2%b)
  @AssertEqual([3._rk, 5._rk, 7._rk], BBox2%e)

  ! Smaller
  BBox2 = BBIntersect(BBox1, t_bbox_(3, [2.25_rk, 4.25_rk, 6.25_rk], [2.75_rk, 4.75_rk, 6.75_rk]))
  @AssertEqual(3, BBox2%nd)
  @AssertEqual([2.25_rk, 4.25_rk, 6.25_rk], BBox2%b)
  @AssertEqual([2.75_rk, 4.75_rk, 6.75_rk], BBox2%e)

  ! Shifted left in x
  BBox2 = BBIntersect(BBox1, t_bbox_(3, [1.5_rk, 4._rk, 6._rk], [2.5_rk, 5._rk, 7._rk]))
  @AssertEqual(3, BBox2%nd)
  @AssertEqual([2._rk, 4._rk, 6._rk], BBox2%b)
  @AssertEqual([2.5_rk, 5._rk, 7._rk], BBox2%e)

  ! Shifted right in x
  BBox2 = BBIntersect(BBox1, t_bbox_(3, [2.5_rk, 4._rk, 6._rk], [3.5_rk, 5._rk, 7._rk]))
  @AssertEqual(3, BBox2%nd)
  @AssertEqual([2.5_rk, 4._rk, 6._rk], BBox2%b)
  @AssertEqual([3._rk, 5._rk, 7._rk], BBox2%e)

  ! Shifted left in y
  BBox2 = BBIntersect(BBox1, t_bbox_(3, [2._rk, 3.5_rk, 6._rk], [3._rk, 4.5_rk, 7._rk]))
  @AssertEqual(3, BBox2%nd)
  @AssertEqual([2._rk, 4._rk, 6._rk], BBox2%b)
  @AssertEqual([3._rk, 4.5_rk, 7._rk], BBox2%e)

  ! Shifted right in y
  BBox2 = BBIntersect(BBox1, t_bbox_(3, [2._rk, 4.5_rk, 6._rk], [3._rk, 5.5_rk, 7._rk]))
  @AssertEqual(3, BBox2%nd)
  @AssertEqual([2._rk, 4.5_rk, 6._rk], BBox2%b)
  @AssertEqual([3._rk, 5._rk, 7._rk], BBox2%e)

  ! Shifted left in z
  BBox2 = BBIntersect(BBox1, t_bbox_(3, [2._rk, 4._rk, 5.5_rk], [3._rk, 5._rk, 6.5_rk]))
  @AssertEqual(3, BBox2%nd)
  @AssertEqual([2._rk, 4._rk, 6._rk], BBox2%b)
  @AssertEqual([3._rk, 5._rk, 6.5_rk], BBox2%e)

  ! Shifted right in z
  BBox2 = BBIntersect(BBox1, t_bbox_(3, [2._rk, 4._rk, 6.5_rk], [3._rk, 5._rk, 7.5_rk]))
  @AssertEqual(3, BBox2%nd)
  @AssertEqual([2._rk, 4._rk, 6.5_rk], BBox2%b)
  @AssertEqual([3._rk, 5._rk, 7._rk], BBox2%e)

  ! Larger
  BBox2 = BBIntersect(BBox1, t_bbox_(3, [1.5_rk, 3.5_rk, 5.5_rk], [3.5_rk, 5.5_rk, 7.5_rk]))
  @AssertEqual(3, BBox2%nd)
  @AssertEqual([2._rk, 4._rk, 6._rk], BBox2%b)
  @AssertEqual([3._rk, 5._rk, 7._rk], BBox2%e)

  ! Non-overlapping positive
  BBox2 = BBIntersect(BBox1, t_bbox_(3, [4._rk, 6._rk, 8._rk], [5._rk, 7._rk, 9._rk]))
  @AssertEqual(3, BBox2%nd)
  @AssertTrue(BBIsEmpty(BBox2))

  ! Non-overlapping negative
  BBox2 = BBIntersect(BBox1, t_bbox_(3, [0._rk, 2._rk, 4._rk], [1._rk, 3._rk, 5._rk]))
  @AssertEqual(3, BBox2%nd)
  @AssertTrue(BBIsEmpty(BBox2))

end subroutine ModBoundingBoxTest_intersect_3d

subroutine ModBoundingBoxTest_from_points_2d(this)

  use pFUnit_mod
  use ModBoundingBox
  use ModGlobal
  implicit none

  class(MPITestCase), intent(inout) :: this

  type(t_bbox) :: BBox
  real(rk), dimension(2,0) :: Empty

  ! No points
  BBox = BBFromPoints(Empty)
  @AssertEqual(2, BBox%nd)
  @AssertTrue(BBIsEmpty(BBox))

  ! One point
  BBox = BBFromPoints(reshape([2._rk, 3._rk], [2,1]))
  @AssertEqual(2, BBox%nd)
  @AssertEqual([2._rk, 3._rk, 0._rk], BBox%b)
  @AssertEqual([2._rk, 3._rk, 0._rk], BBox%e)

  ! Multiple points
  BBox = BBFromPoints(reshape([2._rk, -4._rk, -3._rk, 5._rk], [2,2]))
  @AssertEqual(2, BBox%nd)
  @AssertEqual([-3._rk, -4._rk, 0._rk], BBox%b)
  @AssertEqual([2._rk, 5._rk, 0._rk], BBox%e)

  ! Alternate point layout
  BBox = BBFromPoints(reshape([2._rk, -3._rk, -4._rk, 5._rk], [2,2]), PointsLayout=COMPONENTS_LAST)
  @AssertEqual(2, BBox%nd)
  @AssertEqual([-3._rk, -4._rk, 0._rk], BBox%b)
  @AssertEqual([2._rk, 5._rk, 0._rk], BBox%e)

end subroutine ModBoundingBoxTest_from_points_2d

subroutine ModBoundingBoxTest_from_points_3d(this)

  use pFUnit_mod
  use ModBoundingBox
  use ModGlobal
  implicit none

  class(MPITestCase), intent(inout) :: this

  type(t_bbox) :: BBox
  real(rk), dimension(3,0) :: Empty

  ! No points
  BBox = BBFromPoints(Empty)
  @AssertEqual(3, BBox%nd)
  @AssertTrue(BBIsEmpty(BBox))

  ! One point
  BBox = BBFromPoints(reshape([2._rk, 3._rk, 4._rk], [3,1]))
  @AssertEqual(3, BBox%nd)
  @AssertEqual([2._rk, 3._rk, 4._rk], BBox%b)
  @AssertEqual([2._rk, 3._rk, 4._rk], BBox%e)

  ! Multiple points
  BBox = BBFromPoints(reshape([2._rk, 4._rk, 6._rk, -3._rk, -5._rk, -7._rk], [3,2]))
  @AssertEqual(3, BBox%nd)
  @AssertEqual([-3._rk, -5._rk, -7._rk], BBox%b)
  @AssertEqual([2._rk, 4._rk, 6._rk], BBox%e)

  ! Alternate point layout
  BBox = BBFromPoints(reshape([2._rk, -3._rk, 4._rk, -5._rk, 6._rk, -7._rk], [2,3]), &
    PointsLayout=COMPONENTS_LAST)
  @AssertEqual(3, BBox%nd)
  @AssertEqual([-3._rk, -5._rk, -7._rk], BBox%b)
  @AssertEqual([2._rk, 4._rk, 6._rk], BBox%e)

end subroutine ModBoundingBoxTest_from_points_3d
