! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
@MPITest(npes=[1])
subroutine ModMaskTest_default(this)

  use pFUnit_mod
  use ModGlobal
  use ModMask
  implicit none

  class(MPITestCase), intent(inout) :: this

  type(t_mask) :: Mask

  Mask = t_mask_(2)

  @AssertEqual(2, Mask%nd)
  @AssertEqual([1,1,1], Mask%is)
  @AssertEqual([0,0,1], Mask%ie)
  @AssertEqual(0, Mask%npoints)
  @AssertEqual(FALSE, Mask%periodic)
  @AssertEqual(NO_OVERLAP_PERIODIC, Mask%periodic_storage)
  @AssertFalse(allocated(Mask%values))

  Mask = t_mask_(3)

  @AssertEqual(3, Mask%nd)
  @AssertEqual([1,1,1], Mask%is)
  @AssertEqual([0,0,0], Mask%ie)
  @AssertEqual(0, Mask%npoints)
  @AssertEqual(FALSE, Mask%periodic)
  @AssertEqual(NO_OVERLAP_PERIODIC, Mask%periodic_storage)
  @AssertFalse(allocated(Mask%values))

end subroutine ModMaskTest_default

@MPITest(npes=[1])
subroutine ModMaskTest_make(this)

  use pFUnit_mod
  use ModGlobal
  use ModMask
  implicit none

  class(MPITestCase), intent(inout) :: this

  type(t_mask) :: Mask

  call MakeMask(Mask, nDims=2, iStart=[1,1], iEnd=[3,1], Periodic=[TRUE,FALSE], &
    PeriodicStorage=OVERLAP_PERIODIC)

  @AssertEqual(2, Mask%nd)
  @AssertEqual([1,1,1], Mask%is)
  @AssertEqual([3,1,1], Mask%ie)
  @AssertEqual([3,1,1], Mask%npoints)
  @AssertEqual([TRUE,FALSE,FALSE], Mask%periodic)
  @AssertEqual(OVERLAP_PERIODIC, Mask%periodic_storage)
  @AssertTrue(allocated(Mask%values))
  @AssertEqual([3,1,1], shape(Mask%values))

  call MakeMask(Mask, nDims=3, iStart=[1,1,1], iEnd=[3,1,1], Periodic=[TRUE,FALSE,FALSE], &
    PeriodicStorage=OVERLAP_PERIODIC)

  @AssertEqual(3, Mask%nd)
  @AssertEqual([1,1,1], Mask%is)
  @AssertEqual([3,1,1], Mask%ie)
  @AssertEqual([3,1,1], Mask%npoints)
  @AssertEqual([TRUE,FALSE,FALSE], Mask%periodic)
  @AssertEqual(OVERLAP_PERIODIC, Mask%periodic_storage)
  @AssertTrue(allocated(Mask%values))
  @AssertEqual([3,1,1], shape(Mask%values))

end subroutine ModMaskTest_make

@MPITest(npes=[1])
subroutine ModMaskTest_destroy(this)

  use pFUnit_mod
  use ModGlobal
  use ModMask
  implicit none

  class(MPITestCase), intent(inout) :: this

  type(t_mask) :: Mask

  call MakeMask(Mask, nDims=2, iStart=[1,1], iEnd=[3,1], Periodic=[TRUE,FALSE], &
    PeriodicStorage=OVERLAP_PERIODIC)

  call DestroyMask(Mask)

  @AssertFalse(allocated(Mask%values))

end subroutine ModMaskTest_destroy

@MPITest(npes=[1])
subroutine ModMaskTest_outer_edge_2d(this)

  use pFUnit_mod
  use ModGlobal
  use ModMask
  implicit none

  class(MPITestCase), intent(inout) :: this

  type(t_mask) :: Mask
  type(t_mask) :: OuterEdgeMask

  integer(ik) :: i, j
  logical, dimension(0:4,0:4,1) :: ExpectedValues

  ! Non-periodic
  call MakeMask(Mask, nDims=2, iStart=[1,1], iEnd=[3,3])
  Mask%values = .true.
  Mask%values(2,2,1) = .false.

  call GenerateOuterEdgeMask(Mask, OuterEdgeMask)

  @AssertEqual(2, OuterEdgeMask%nd)
  @AssertEqual([0,0,1], OuterEdgeMask%is)
  @AssertEqual([4,4,1], OuterEdgeMask%ie)
  @AssertEqual(FALSE, OuterEdgeMask%periodic)
  @AssertEqual(NO_OVERLAP_PERIODIC, OuterEdgeMask%periodic_storage)

  ExpectedValues(0,:,1) = .true.
  ExpectedValues(4,:,1) = .true.
  ExpectedValues(:,0,1) = .true.
  ExpectedValues(:,4,1) = .true.
  ExpectedValues(1:3,1:3,1) = .false.
  ExpectedValues(2,2,1) = .true.

  @AssertTrue(all(ExpectedValues .eqv. OuterEdgeMask%values))

  ! Periodic in x
  call MakeMask(Mask, nDims=2, iStart=[1,1], iEnd=[3,3], Periodic=[TRUE,FALSE], &
    PeriodicStorage=OVERLAP_PERIODIC)
  Mask%values = .true.
  Mask%values(2,2,1) = .false.

  call GenerateOuterEdgeMask(Mask, OuterEdgeMask)

  @AssertEqual(2, OuterEdgeMask%nd)
  @AssertEqual([0,0,1], OuterEdgeMask%is)
  @AssertEqual([4,4,1], OuterEdgeMask%ie)
  @AssertEqual(FALSE, OuterEdgeMask%periodic)
  @AssertEqual(NO_OVERLAP_PERIODIC, OuterEdgeMask%periodic_storage)

  ExpectedValues(:,0,1) = .true.
  ExpectedValues(:,4,1) = .true.
  ExpectedValues(:,1:3,1) = .false.
  ExpectedValues(:,2,1) = [(modulo(i,2)==0,i=0,4)]

  @AssertTrue(all(ExpectedValues .eqv. OuterEdgeMask%values))

  ! Periodic in y
  call MakeMask(Mask, nDims=2, iStart=[1,1], iEnd=[3,3], Periodic=[FALSE,TRUE], &
    PeriodicStorage=OVERLAP_PERIODIC)
  Mask%values = .true.
  Mask%values(2,2,1) = .false.

  call GenerateOuterEdgeMask(Mask, OuterEdgeMask)

  @AssertEqual(2, OuterEdgeMask%nd)
  @AssertEqual([0,0,1], OuterEdgeMask%is)
  @AssertEqual([4,4,1], OuterEdgeMask%ie)
  @AssertEqual(FALSE, OuterEdgeMask%periodic)
  @AssertEqual(NO_OVERLAP_PERIODIC, OuterEdgeMask%periodic_storage)

  ExpectedValues(0,:,1) = .true.
  ExpectedValues(4,:,1) = .true.
  ExpectedValues(1:3,:,1) = .false.
  ExpectedValues(2,:,1) = [(modulo(i,2)==0,i=0,4)]

  @AssertTrue(all(ExpectedValues .eqv. OuterEdgeMask%values))

  ! Periodic in x and y
  call MakeMask(Mask, nDims=2, iStart=[1,1], iEnd=[3,3], Periodic=[TRUE,TRUE], &
    PeriodicStorage=OVERLAP_PERIODIC)
  Mask%values = .true.
  Mask%values(2,2,1) = .false.

  call GenerateOuterEdgeMask(Mask, OuterEdgeMask)

  @AssertEqual(2, OuterEdgeMask%nd)
  @AssertEqual([0,0,1], OuterEdgeMask%is)
  @AssertEqual([4,4,1], OuterEdgeMask%ie)
  @AssertEqual(FALSE, OuterEdgeMask%periodic)
  @AssertEqual(NO_OVERLAP_PERIODIC, OuterEdgeMask%periodic_storage)

  do j = 0, 4
    do i = 0, 4
      ExpectedValues(i,j,1) = modulo(i,2) == 0 .and. modulo(j,2) == 0
    end do
  end do

  @AssertTrue(all(ExpectedValues .eqv. OuterEdgeMask%values))

end subroutine ModMaskTest_outer_edge_2d

@MPITest(npes=[1])
subroutine ModMaskTest_outer_edge_3d(this)

  use pFUnit_mod
  use ModGlobal
  use ModMask
  implicit none

  class(MPITestCase), intent(inout) :: this

  type(t_mask) :: Mask
  type(t_mask) :: OuterEdgeMask

  integer(ik) :: i, j, k
  logical, dimension(0:4,0:4,0:4) :: ExpectedValues

  ! Non-periodic
  call MakeMask(Mask, nDims=3, iStart=[1,1,1], iEnd=[3,3,3])
  Mask%values = .true.
  Mask%values(2,2,2) = .false.

  call GenerateOuterEdgeMask(Mask, OuterEdgeMask)

  @AssertEqual(3, OuterEdgeMask%nd)
  @AssertEqual([0,0,0], OuterEdgeMask%is)
  @AssertEqual([4,4,4], OuterEdgeMask%ie)
  @AssertEqual(FALSE, OuterEdgeMask%periodic)
  @AssertEqual(NO_OVERLAP_PERIODIC, OuterEdgeMask%periodic_storage)

  ExpectedValues(0,:,:) = .true.
  ExpectedValues(4,:,:) = .true.
  ExpectedValues(:,0,:) = .true.
  ExpectedValues(:,4,:) = .true.
  ExpectedValues(:,:,0) = .true.
  ExpectedValues(:,:,4) = .true.
  ExpectedValues(1:3,1:3,1:3) = .false.
  ExpectedValues(2,2,2) = .true.

  @AssertTrue(all(ExpectedValues .eqv. OuterEdgeMask%values))

  ! Periodic in x
  call MakeMask(Mask, nDims=3, iStart=[1,1,1], iEnd=[3,3,3], Periodic=[TRUE,FALSE,FALSE], &
    PeriodicStorage=OVERLAP_PERIODIC)
  Mask%values = .true.
  Mask%values(2,2,2) = .false.

  call GenerateOuterEdgeMask(Mask, OuterEdgeMask)

  @AssertEqual(3, OuterEdgeMask%nd)
  @AssertEqual([0,0,0], OuterEdgeMask%is)
  @AssertEqual([4,4,4], OuterEdgeMask%ie)
  @AssertEqual(FALSE, OuterEdgeMask%periodic)
  @AssertEqual(NO_OVERLAP_PERIODIC, OuterEdgeMask%periodic_storage)

  ExpectedValues(:,0,:) = .true.
  ExpectedValues(:,4,:) = .true.
  ExpectedValues(:,:,0) = .true.
  ExpectedValues(:,:,4) = .true.
  ExpectedValues(:,1:3,1:3) = .false.
  ExpectedValues(:,2,2) = [(modulo(i,2)==0,i=0,4)]

  @AssertTrue(all(ExpectedValues .eqv. OuterEdgeMask%values))

  ! Periodic in y
  call MakeMask(Mask, nDims=3, iStart=[1,1,1], iEnd=[3,3,3], Periodic=[FALSE,TRUE,FALSE], &
    PeriodicStorage=OVERLAP_PERIODIC)
  Mask%values = .true.
  Mask%values(2,2,2) = .false.

  call GenerateOuterEdgeMask(Mask, OuterEdgeMask)

  @AssertEqual(3, OuterEdgeMask%nd)
  @AssertEqual([0,0,0], OuterEdgeMask%is)
  @AssertEqual([4,4,4], OuterEdgeMask%ie)
  @AssertEqual(FALSE, OuterEdgeMask%periodic)
  @AssertEqual(NO_OVERLAP_PERIODIC, OuterEdgeMask%periodic_storage)

  ExpectedValues(0,:,:) = .true.
  ExpectedValues(4,:,:) = .true.
  ExpectedValues(:,:,0) = .true.
  ExpectedValues(:,:,4) = .true.
  ExpectedValues(1:3,:,1:3) = .false.
  ExpectedValues(2,:,2) = [(modulo(i,2)==0,i=0,4)]

  @AssertTrue(all(ExpectedValues .eqv. OuterEdgeMask%values))

  ! Periodic in z
  call MakeMask(Mask, nDims=3, iStart=[1,1,1], iEnd=[3,3,3], Periodic=[FALSE,FALSE,TRUE], &
    PeriodicStorage=OVERLAP_PERIODIC)
  Mask%values = .true.
  Mask%values(2,2,2) = .false.

  call GenerateOuterEdgeMask(Mask, OuterEdgeMask)

  @AssertEqual(3, OuterEdgeMask%nd)
  @AssertEqual([0,0,0], OuterEdgeMask%is)
  @AssertEqual([4,4,4], OuterEdgeMask%ie)
  @AssertEqual(FALSE, OuterEdgeMask%periodic)
  @AssertEqual(NO_OVERLAP_PERIODIC, OuterEdgeMask%periodic_storage)

  ExpectedValues(0,:,:) = .true.
  ExpectedValues(4,:,:) = .true.
  ExpectedValues(:,0,:) = .true.
  ExpectedValues(:,4,:) = .true.
  ExpectedValues(1:3,1:3,:) = .false.
  ExpectedValues(2,2,:) = [(modulo(i,2)==0,i=0,4)]

  @AssertTrue(all(ExpectedValues .eqv. OuterEdgeMask%values))

  ! Periodic in x, y, and z
  call MakeMask(Mask, nDims=3, iStart=[1,1,1], iEnd=[3,3,3], Periodic=[TRUE,TRUE,TRUE], &
    PeriodicStorage=OVERLAP_PERIODIC)
  Mask%values = .true.
  Mask%values(2,2,2) = .false.

  call GenerateOuterEdgeMask(Mask, OuterEdgeMask)

  @AssertEqual(3, OuterEdgeMask%nd)
  @AssertEqual([0,0,0], OuterEdgeMask%is)
  @AssertEqual([4,4,4], OuterEdgeMask%ie)
  @AssertEqual(FALSE, OuterEdgeMask%periodic)
  @AssertEqual(NO_OVERLAP_PERIODIC, OuterEdgeMask%periodic_storage)

  do k = 0, 4
    do j = 0, 4
      do i = 0, 4
        ExpectedValues(i,j,k) = modulo(i,2) == 0 .and. modulo(j,2) == 0 .and. modulo(k,2) == 0
      end do
    end do
  end do

  @AssertTrue(all(ExpectedValues .eqv. OuterEdgeMask%values))

end subroutine ModMaskTest_outer_edge_3d

@MPITest(npes=[1])
subroutine ModMaskTest_grow_2d(this)

  use pFUnit_mod
  use ModGlobal
  use ModMask
  implicit none

  class(MPITestCase), intent(inout) :: this

  type(t_mask) :: Mask
  logical, dimension(8,8,1) :: ExpectedValues

  ExpectedValues = .true.
  ExpectedValues(4:5,4:5,1) = .false.

  ! Non-periodic
  call MakeMask(Mask, nDims=2, iStart=[1,1], iEnd=[8,8])
  Mask%values = .true.
  Mask%values(2:7,2:7,1) = .false.

  call GrowMask(Mask, 2)

  @AssertTrue(all(ExpectedValues .eqv. Mask%values))

  ! Periodic in x
  call MakeMask(Mask, nDims=2, iStart=[1,1], iEnd=[8,8], Periodic=[TRUE,FALSE], &
    PeriodicStorage=OVERLAP_PERIODIC)
  Mask%values = .true.
  Mask%values(2:,2:7,1) = .false.

  call GrowMask(Mask, 2)

  @AssertTrue(all(ExpectedValues .eqv. Mask%values))

  ! Periodic in y
  call MakeMask(Mask, nDims=2, iStart=[1,1], iEnd=[8,8], Periodic=[FALSE,TRUE], &
    PeriodicStorage=OVERLAP_PERIODIC)
  Mask%values = .true.
  Mask%values(2:7,2:,1) = .false.

  call GrowMask(Mask, 2)

  @AssertTrue(all(ExpectedValues .eqv. Mask%values))

  ! Periodic in x and y
  call MakeMask(Mask, nDims=2, iStart=[1,1], iEnd=[8,8], Periodic=[TRUE,TRUE], &
    PeriodicStorage=OVERLAP_PERIODIC)
  Mask%values = .true.
  Mask%values(2:,2:,1) = .false.

  call GrowMask(Mask, 2)

  @AssertTrue(all(ExpectedValues .eqv. Mask%values))

end subroutine ModMaskTest_grow_2d

@MPITest(npes=[1])
subroutine ModMaskTest_grow_3d(this)

  use pFUnit_mod
  use ModGlobal
  use ModMask
  implicit none

  class(MPITestCase), intent(inout) :: this

  type(t_mask) :: Mask
  logical, dimension(8,8,8) :: ExpectedValues

  ExpectedValues = .true.
  ExpectedValues(4:5,4:5,4:5) = .false.

  ! Non-periodic
  call MakeMask(Mask, nDims=3, iStart=[1,1,1], iEnd=[8,8,8])
  Mask%values = .true.
  Mask%values(2:7,2:7,2:7) = .false.

  call GrowMask(Mask, 2)

  @AssertTrue(all(ExpectedValues .eqv. Mask%values))

  ! Periodic in x
  call MakeMask(Mask, nDims=3, iStart=[1,1,1], iEnd=[8,8,8], Periodic=[TRUE,FALSE,FALSE], &
    PeriodicStorage=OVERLAP_PERIODIC)
  Mask%values = .true.
  Mask%values(2:,2:7,2:7) = .false.

  call GrowMask(Mask, 2)

  @AssertTrue(all(ExpectedValues .eqv. Mask%values))

  ! Periodic in y
  call MakeMask(Mask, nDims=3, iStart=[1,1,1], iEnd=[8,8,8], Periodic=[FALSE,TRUE,FALSE], &
    PeriodicStorage=OVERLAP_PERIODIC)
  Mask%values = .true.
  Mask%values(2:7,2:,2:7) = .false.

  call GrowMask(Mask, 2)

  @AssertTrue(all(ExpectedValues .eqv. Mask%values))

  ! Periodic in z
  call MakeMask(Mask, nDims=3, iStart=[1,1,1], iEnd=[8,8,8], Periodic=[FALSE,FALSE,TRUE], &
    PeriodicStorage=OVERLAP_PERIODIC)
  Mask%values = .true.
  Mask%values(2:7,2:7,2:) = .false.

  call GrowMask(Mask, 2)

  @AssertTrue(all(ExpectedValues .eqv. Mask%values))

  ! Periodic in x, y, and z
  call MakeMask(Mask, nDims=3, iStart=[1,1,1], iEnd=[8,8,8], Periodic=[TRUE,TRUE,TRUE], &
    PeriodicStorage=OVERLAP_PERIODIC)
  Mask%values = .true.
  Mask%values(2:,2:,2:) = .false.

  call GrowMask(Mask, 2)

  @AssertTrue(all(ExpectedValues .eqv. Mask%values))

end subroutine ModMaskTest_grow_3d
