# Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
# License: MIT, http://opensource.org/licenses/MIT
# Make sure automake knows where to look for the local macros for configure
ACLOCAL_AMFLAGS = -I m4
# Build the object files in the source directory instead of the top
# directory (subdir-objects) and don't act like a GNU tool (foreign)
# If you change the subdir-objects, change the FC_DEPDIR option to match
AUTOMAKE_OPTIONS = subdir-objects foreign

# Initialize variables
BUILT_SOURCES =
EXTRA_DIST =
lib_LIBRARIES =
check_PROGRAMS =
noinst_PROGRAMS =
TESTS =
dist_noinst_SCRIPTS =
noinst_HEADERS =
CLEAN_LOCAL_TARGETS =
DISTCLEAN_LOCAL_TARGETS =
deps_fpp_src =
deps_c_src =
FPPFLAGS =

# For suffix rules
SUFFIXES = .fpp .$(FC_MODEXT) .pf

#==============
# Main library
#==============

# Include source file list
include src/Makefile.mk
overset_fc_src = $(overset_fpp_src:.fpp=.f90)

# Distribute the unprocessed source files
EXTRA_DIST += $(overset_fpp_src)

.PHONY: overset-clean

if BUILD_OVERSET

# Library target
lib_LIBRARIES += liboverset.a
nodist_liboverset_a_SOURCES = $(overset_fc_src)

# Enable status output
FPPFLAGS += -DOVERSET_VERBOSE

# Generate dependencies
deps_fpp_src += $(overset_fpp_src)

# Cleanup rules
CLEAN_LOCAL_TARGETS += overset-clean
overset-clean:
	rm -f $(overset_fc_src)

endif

#============
# Unit tests
#============

# Include test file list
include tests/Makefile.mk
tests_fpp_src = $(tests_pf_src:.pf=.fpp)
tests_fc_src = $(tests_fpp_src:.fpp=.f90)

# Distribute .pf source files
EXTRA_DIST += $(tests_pf_src)

# Distribute test data
EXTRA_DIST += tests/data

# Distribute test suite include file
noinst_HEADERS += tests/testSuites.inc

# Distribute the test runner script
dist_noinst_SCRIPTS += tests/runall.in

.PHONY: tests-clean

if BUILD_OVERSET
if BUILD_TESTS
if HAVE_PFUNIT

# Test runner
TESTS += tests/runall

# Unit test driver
noinst_PROGRAMS += tests/unittests
check_PROGRAMS += tests/unittests
nodist_tests_unittests_SOURCES = $(tests_fc_src) tests/driver.f90 $(overset_fc_src)

# Generate dependencies
deps_fpp_src += $(tests_fpp_src) tests/driver.fpp

# Copy the test driver source file over to a local directory before compiling
tests/driver.fpp: @PFUNIT_DRIVER@
	cp @PFUNIT_DRIVER@ tests/driver.fpp

# Make sure driver gets reprocessed if testSuites.inc changes
tests/driver.f90: tests/testSuites.inc

# Preprocess the .pf test files into Fortran .fpp files
.pf.fpp:
	@PFPARSE@ $^ $@

# Create directories and copy files for VPATH builds
BUILT_SOURCES += $(abs_builddir)/tests/.datastamp
$(abs_builddir)/tests/.datastamp:
	if [ "$(builddir)" != "$(srcdir)" ]; then \
    test -d tests || @MKDIR_P@ tests; \
    test -L tests/data || @LN_S@ $(abs_srcdir)/tests/data tests/data; \
    test -L tests/testSuites.inc || @LN_S@ $(abs_srcdir)/tests/testSuites.inc tests/testSuites.inc; \
  fi; \
  touch tests/.datastamp

# Cleanup rules
CLEAN_LOCAL_TARGETS += tests-clean
tests-clean:
	rm -f $(tests_fpp_src)
	rm -f $(tests_fc_src)
	rm -f tests/driver.f90 tests/driver.fpp
	rm -f tests/.datastamp
	if [ "$(builddir)" != "$(srcdir)" ]; then \
    rm -f tests/data; \
    rm -f tests/testSuites.inc; \
  fi

endif
endif
endif

#======
# Misc
#======

# Fortran module characteristics
FC_MODEXT   = @FC_MODEXT@
FC_MODINC   = @FC_MODINC@
FC_MODOUT   = @FC_MODOUT@
FC_MODCASE  = @FC_MODCASE@
AM_FCFLAGS  = $(FC_MODOUT)$(MODDIR) $(FC_MODINC)$(MODDIR)

# Tool-specific flags
OVERSET_FCFLAGS = @OVERSET_FCFLAGS@
AM_FCFLAGS += $(OVERSET_FCFLAGS)

# aggressive optimization flags
# The strange form here protects these conditionals from automake,
# and ensures that the are executed by make, not automake.
@MK@ifeq "$(USE_AGGRESSIVE_OPTS)" "1"
@MK@ifeq "$(COMPILER_FAMILY)" "intel"
@MK@  AM_CFLAGS   += -O2
@MK@  AM_CXXFLAGS += -O2
@MK@  AM_FCFLAGS  += -O3 -ip -ipo -xHost
@MK@endif
@MK@ifeq "$(COMPILER_FAMILY)" "gnu"
@MK@  AM_CFLAGS   += -O2
@MK@  AM_FCFLAGS  += -fomit-frame-pointer -O3 -ftree-vectorize -ffast-math -funroll-loops
@MK@  AM_CXXFLAGS += -O2
@MK@endif
@MK@ifeq "$(COMPILER_FAMILY)" "cray"
@MK@  AM_CFLAGS   += -O
@MK@endif
@MK@endif

# Fortran preprocessing
FPPFLAGS += @FPPFLAGS@

# Rule for processing .fpp files to .f90
# Note that Make has trouble combining implicit with explicit rules.
# Thus, a change to an .fpp file will not force a recompilation of all of the
# modules that make use of the module(s) created by that .fpp file unless
# we include that as an explicit dependency
.fpp.f90:
	$(FPP) $(FPPFLAGS) $< >$@
	-@rm -f $*.s
	$(FC_DEPCOMP) -J$(MODDIR) --modext=$(FC_MODEXT) --modcase=$(FC_MODCASE) --depdir=$(DEPDIR) --subdirobj --modulelist=_fcmods --origsrc=$*.fpp $*.f90

# Cleanup rules
clean-local: $(CLEAN_LOCAL_TARGETS)
	rm -rf $(MODDIR)
	find . -name *.dSYM -print0 | xargs -0 rm -rf

distclean-local: $(DISTCLEAN_LOCAL_TARGETS)

# Dependency generation script
FC_DEPCOMP = @FC_DEPCOMP@

# Dependency directory name
DEPDIR = @DEPDIR@

# Create module directory
BUILT_SOURCES += $(abs_builddir)/$(MODDIR)
$(abs_builddir)/$(MODDIR):
	test -d $(MODDIR) || @MKDIR_P@ $(MODDIR)

# Create dependency directory
BUILT_SOURCES += $(abs_builddir)/$(DEPDIR)
$(abs_builddir)/$(DEPDIR):
	test -d $(DEPDIR) || @MKDIR_P@ $(DEPDIR)

# Create src and tests directories if necessary
BUILT_SOURCES += $(abs_builddir)/src/.dirstamp $(abs_builddir)/tests/.dirstamp
$(abs_builddir)/src/.dirstamp:
	test -d src || @MKDIR_P@ src
	touch src/.dirstamp
$(abs_builddir)/tests/.dirstamp:
	test -d tests || @MKDIR_P@ tests
	touch tests/.dirstamp

# Directory for Fortran module files
MODDIR = mod

# FC dependencies
# Since automake won't generate one for each Fortran source file,
# we create one. (automake will add this for us if wel use C or C++)
# Do dependency generation first
BUILT_SOURCES += $(abs_builddir)/$(DEPDIR)/stamp_fclist
# If the module is not found, it is expected in MODDIR
# Include source files on the dependencies, but then always rescan
# everyfile on a change to any of them.
# Do need to add a rescan when a file is changed
# Note the two pass algorithm.  First find all of the module names defined
# by the package, then use those to create the dependency files.
# Updates to individual files will update the module list (_fcmods)
$(abs_builddir)/$(DEPDIR)/stamp_fclist: $(deps_fpp_src) $(deps_c_src)
	rm -f $(DEPDIR)/_fclist $(DEPDIR)/_fcmods
	touch $(DEPDIR)/_fclist
	for file in $(deps_fpp_src); do \
	    bfile=`echo $$file | sed 's/\.fpp/.f90/'`; \
	    $(MAKE) $$bfile; \
	    $(FC_DEPCOMP) -J$(MODDIR) --modext=$(FC_MODEXT) --modcase=$(FC_MODCASE) --depdir=$(DEPDIR) \
        --subdirobj --modulelist=_fcmods --origsrc=$$file $$bfile; \
	done
	for file in $(deps_fpp_src:.fpp=.f90); do \
	    bfile=`basename $$file .f90`; \
	    sfile=`echo $$file | sed 's/\.f90/.fpp/'` ; \
	    $(FC_DEPCOMP) -J$(MODDIR) --modext=$(FC_MODEXT) --modcase=$(FC_MODCASE) --depdir=$(DEPDIR) \
        --subdirobj --modulelist=_fcmods --origsrc=$$sfile $$file; \
	    echo "include $(DEPDIR)/$$bfile.Po" >> $(DEPDIR)/_fclist;\
	done
	echo "Stamp for FC dependencies" > $(DEPDIR)/stamp_fclist

.PHONY: deps-clean deps-distclean

# _fclist and C .Po files are created by configure, so 'clean' target shouldn't delete them
# (just empty them); Fortran .Po files are created by make, so they should be deleted
CLEAN_LOCAL_TARGETS += deps-clean
deps-clean:
	rm -f $(DEPDIR)/_fcmods
	rm -f $(DEPDIR)/stamp_fclist
	echo "# dummy" > $(DEPDIR)/_fclist
	(cd $(DEPDIR) && for file in $(deps_c_src:.c=.Po); do \
	  echo "# dummy" > "$$file"; \
	done)
	(cd $(DEPDIR) && for file in $(deps_fpp_src:.fpp=.Po); do \
	  rm -f "$$file"; \
	done)

DISTCLEAN_LOCAL_TARGETS += deps-distclean
deps-distclean:
	rm -rf $(DEPDIR)

# Include dependency files
@AMDEP_TRUE@@am__include@ @am__quote@$(DEPDIR)/_fclist@am__quote@
