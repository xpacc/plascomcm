! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                                                               !
! Compute the difference between two PLOT3D solutions           !
!                                                               !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                                                               !
! Written by Daniel J. Bodony (bodony@illinois.edu)             !
! 12 February 2009                                              !
!                                                               !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

Program diffSoln

  USE ModPLOT3D_IO
  Implicit None

  !  Arrays
  Real(KIND=8), Dimension(:,:,:,:,:), Pointer :: X1, X2
  Real(KIND=8) :: XMIN, XMAX, YMIN, YMAX, ZMIN, ZMAX, TAU(4)

  !  Integers
  Integer :: ngrid(3), I, J, K, dir, NX, NY, NZ, NDIM(3)
  Integer, Dimension(:,:), Pointer :: ND1, ND2
  Integer, Dimension(:,:,:,:), Pointer :: IBLANK1, IBLANK2

  !  Logical
  Logical :: gf_exists(3), sf_exists(3)

  !  Characters
  Character(LEN=2)  :: prec(3), gf(3), vf(3), ib(3)
  Character(LEN=80) :: grid_file(3), cmd

  !  Step zero, read in the command line
  Integer :: iargc, nargc, ng, isperiodic, ftype

  nargc = iargc()
  If (nargc /= 3) Then
    Write (*,'(A)') 'USAGE: diffSoln {Soln file 1} {Soln file 2} {diff Soln file}'
    Stop
  Endif

  Call Getarg(1,grid_file(1));
  Call Getarg(2,grid_file(2));
  Call Getarg(3,grid_file(3));

  Call p3d_detect(LEN(Trim(grid_file(1))),Trim(grid_file(1)),prec,NDIM,gf,vf,ib,ftype)

  !  Step one, read in the mesh1 grid file
  Write (*,'(A)') ''
  Call Read_Soln(NDIM(1),ngrid(1),ND1,X1,TAU, &
       prec(1),gf(1),vf(1),grid_file(1), 1 )
  Call Read_Soln(NDIM(2),ngrid(2),ND2,X2,TAU, &
       prec(2),gf(2),vf(2),grid_file(2), 1 )
  Write (*,'(A)') ''

  Do ng = 1, ngrid(1)
    Do I = 1, ND1(ng,1)
      Do J = 1, ND1(ng,2)
        Do K = 1, ND1(ng,3)
          X2(ng,I,J,K,1:NDIM(1)+2) = X2(ng,I,J,K,1:NDIM(1)+2) - X1(ng,I,J,K,1:NDIM(1)+2)
        End Do
      End Do
    End Do
  End Do

  Call Write_Soln(NDIM(2),ngrid(2),ND2,X2,tau,&
       prec(1),gf(1),vf(1),grid_file(3))

  Write (*,'(A)') ''

  ! Step three, clean up & leave
  Deallocate(ND1, X1, ND2, X2)


  Stop
End Program diffSoln
