! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
Program extrudeMesh
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  !                                                               !
  ! Extrude a PLOT3D mesh in one direction                        !
  !                                                               !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  !                                                               !
  ! Written by Daniel J. Bodony (bodony@stanford.edu)             !
  ! 26 April 2006                                                 !
  ! 24 January 2007
  !                                                               !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  USE ModPLOT3D_IO
  Implicit None

  !  Arrays
  Real(KIND=8), Dimension(:,:,:,:,:), Pointer :: X1, X2
  Real(KIND=8) :: XMIN, XMAX, YMIN, YMAX, ZMIN, ZMAX

  !  Integers
  Integer :: ngrid(3), I, J, K, dir, NX, NY, NZ, NDIM(3)
  Integer, Dimension(:,:), Pointer :: ND1, ND2
  Integer, Dimension(:,:,:,:), Pointer :: IBLANK1, IBLANK2

  !  Logical
  Logical :: gf_exists(3), sf_exists(3)

  !  Characters
  Character(LEN=2)  :: prec(3), gf(3), vf(3), ib(3)
  Character(LEN=80) :: grid_file(3), cmd

  !  Step zero, read in the command line
  Integer :: iargc, nargc, ng, isperiodic, ftype
  Integer, Parameter :: TRUE = 1

  nargc = iargc()
  If (nargc /= 2) Then
    Write (*,'(A)') 'USAGE: extrudeMesh {grid file 1} {extruded grid file}'
    Stop
  Endif

  Call Getarg(1,grid_file(1));
  Call Getarg(2,grid_file(2));

  Call p3d_detect(LEN(Trim(grid_file(1))),Trim(grid_file(1)),prec,NDIM,gf,vf,ib,ftype)

  !  Step one, read in the mesh1 grid file
  Write (*,'(A)') ''
  Call Read_Grid(NDIM(1),ngrid(1),ND1,X1,IBLANK1, &
       prec(1),gf(1),vf(1),ib(1),grid_file(1),TRUE)
  Write (*,'(A)') ''

  !  Step two, find extrusion dir
  Write (*,'(A)', ADVANCE='NO') 'Enter extrusion dir: ' 
  Read (*,*) dir
  Do ng = 1, ngrid(1)
    If (ND1(ng,dir) .ne. 1) Then
      Write (*,'(A,I2)') &
           'Extrusion dir does not have one grid point on grid ', ng
      Stop
    End If
  End Do

  !  Step two, get parameters from user
  ngrid(2) = ngrid(1)
  NDIM(2) = NDIM(1)
  Allocate(ND2(ngrid(2),3))
  ND2 = ND1
  If (dir .eq. 1) Then
    Write (*,'(A)',ADVANCE='NO') 'Input NX: '
    Read (*,*) ND2(1,1)
    ND2(:,1) = ND2(1,1)    
    Write (*,'(A)',ADVANCE='NO') 'Input XMIN, XMAX: '
    Read (*,*) XMIN, XMAX
  Else If (dir .eq. 2) Then
    Write (*,'(A)',ADVANCE='NO') 'Input NY: '
    Read (*,*) ND2(1,2)
    ND2(:,2) = ND2(1,2)
    Write (*,'(A)',ADVANCE='NO') 'Input YMIN, YMAX: '
    Read (*,*) YMIN, YMAX
  Else
    Write (*,'(A)',ADVANCE='NO') 'Input NZ: '
    Read (*,*) ND2(1,3)
    ND2(:,3) = ND2(1,3)
    Write (*,'(A)',ADVANCE='NO') 'Input ZMIN, ZMAX: '
    Read (*,*) ZMIN, ZMAX
  End If
  Write (*,'(A)',ADVANCE='NO') 'Is periodic? (0=no, 1=yes): '
  Read (*,*) isperiodic

  NX = MAXVAL(ND2(:,1))
  NY = MAXVAL(ND2(:,2))
  NZ = MAXVAL(ND2(:,3))
  ALLOCATE(X2(ngrid(2),NX,NY,NZ,3),IBLANK2(ngrid(2),NX,NY,NZ))

  Do ng = 1, ngrid(1)
    Do I = 1, ND1(ng,1)
      Do J = 1, ND1(ng,2)
        Do K = 1, ND1(ng,3)
          X2(ng,I,J,K,:) = X1(ng,I,J,K,:)
          IBLANK2(ng,I,J,K) = IBLANK1(ng,I,J,K)
        End Do
      End Do
    End Do
  End Do

  If (dir .eq. 1) Then
    Do ng = 1, ngrid(2)
      Do I = 1, ND2(ng,1)
        Do J = 1, ND2(ng,2)
          Do K = 1, ND2(ng,3)
            X2(ng,I,J,K,1) = XMIN + DBLE(I-1)/DBLE(ND2(ng,1)-(1-isperiodic)) &
                 * (XMAX-XMIN)
            X2(ng,I,J,K,2) = X2(ng,1,J,K,2)
            X2(ng,I,J,K,3) = X2(ng,1,J,K,3)
            IBLANK2(ng,I,J,K) = IBLANK2(ng,1,J,K)
          End Do
        End Do
      End Do
    End Do
  Else If (dir .eq. 2) Then
    Do ng = 1, ngrid(2)
      Do I = 1, ND2(ng,1)
        Do J = 1, ND2(ng,2)
          Do K = 1, ND2(ng,3)
            X2(ng,I,J,K,2) = YMIN + DBLE(J-1)/DBLE(ND2(ng,2)-(1-isperiodic)) &
                 * (YMAX-YMIN)
            X2(ng,I,J,K,1) = X2(ng,I,1,K,1)
            X2(ng,I,J,K,3) = X2(ng,I,1,K,3)
            IBLANK2(ng,I,J,K) = IBLANK2(ng,I,1,K)
          End Do
        End Do
      End Do
    End Do
  Else
    Do ng = 1, ngrid(2)
      Do I = 1, ND2(ng,1)
        Do J = 1, ND2(ng,2)
          Do K = 1, ND2(ng,3)
            X2(ng,I,J,K,3) = ZMIN + DBLE(K-1)/DBLE(ND2(ng,3)-(1-isperiodic)) &
                 * (ZMAX-ZMIN)
            X2(ng,I,J,K,1) = X2(ng,I,J,1,1)
            X2(ng,I,J,K,2) = X2(ng,I,J,1,2)
            IBLANK2(ng,I,J,K) = IBLANK2(ng,I,J,1)
          End Do
        End Do
      End Do
    End Do
  End If

  Call Write_Grid(NDIM(2),ngrid(2),ND2,X2,IBLANK2, &
       prec(1),gf(1),vf(1),ib(1),grid_file(2))
  Write (*,'(A)') ''

  ! Step three, clean up & leave
  Deallocate(ND1, X1, IBLANK1, ND2, X2, IBLANK2)


  Stop
End Program extrudeMesh
