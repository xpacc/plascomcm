! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
Program moveNodes
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                                                               !
! Move the (x,y,z) locations of a set of nodes                  !
!                                                               !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                                                               !
! Written by Daniel J. Bodony (bodony@uiuc.edu)                 !
! 1 April 2008                                                  !
!                                                               !
! $Header: /cvsroot/genx/Codes/RocfloCM/plot3d_utils/moveNodes.f90,v 1.5 2010/08/10 18:17:56 mtcampbe Exp $
!                                                               !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  USE ModPLOT3D_IO
  Implicit None

  !  Arrays
  Real(KIND=8), Dimension(:,:,:,:,:), Pointer :: X1, X2
  Real(KIND=8) :: XMIN, XMAX, YMIN, YMAX, ZMIN, ZMAX

  !  Integers
  Integer :: ngrid(3), I, J, K, dir, NX, NY, NZ, NDIM(3)
  Integer, Dimension(:,:), Pointer :: ND1, ND2
  Integer, Dimension(:,:,:,:), Pointer :: IBLANK1, IBLANK2
  Integer, Parameter :: TRUE = 1, FALSE = 0


  !  Logical
  Logical :: gf_exists(3), sf_exists(3)

  !  Characters
  Character(LEN=2)  :: prec, gf, vf, ib
  Character(LEN=80) :: grid_file, cmd, file

  !  Step zero, read in the command line
  Integer :: iargc, nargc, ng, isperiodic, ftype
  Integer :: IS, IE, JS, JE, KS, KE, ibval, alter
  Real(KIND=8) :: xval

  nargc = iargc()
  If (nargc /= 2) Then
    Write (*,'(A)') 'USAGE: moveNodes {grid file 1} {altered grid file}'
    Stop
  Endif

  Call Getarg(1,file);
  Call Getarg(2,grid_file);

  Call p3d_detect(LEN(Trim(file)),Trim(file),prec,NDIM,gf,vf,ib,ftype)

  !  Step one, read in the mesh1 grid file
  Write (*,'(A)') ''
  Call Read_Grid(NDIM(1),ngrid(1),ND1,X1,IBLANK1, &
       prec,gf,vf,ib,file,TRUE)
  Write (*,'(A)') ''

  !  Step two, get parameters from user
  Do ng = 1, ngrid(1)

    Write (*,'(A,I2,A,3(I3,1X))') 'Block ', ng, ' has size ', &
         (ND1(ng,i),i=1,3)

    Write (*,'(A)',advance='no') 'Alter this block? (0=no, 1=yes): '
    Read (*,*) alter

    If (alter == 1) Then
      IS = 1; IE = 1;
      If (ND1(ng,1) > 1) Then
        Write (*,'(A)',ADVANCE='NO') 'Input IS, IE: '
        Read (*,*) IS, IE
        IF (IS < 0) IS = (IS+1) + ND1(ng,1)
        IF (IE < 0) IE = (IE+1) + ND1(ng,1)
      End If

      JS = 1; JE = 1;
      If (ND1(ng,2) > 1) Then
        Write (*,'(A)',ADVANCE='NO') 'Input JS, JE: '
        Read (*,*) JS, JE
        IF (JS < 0) JS = (JS+1) + ND1(ng,2)
        IF (JE < 0) JE = (JE+1) + ND1(ng,2)
      End If

      KS = 1; KE = 1;
      If (ND1(ng,3) > 1) Then
        Write (*,'(A)',ADVANCE='NO') 'Input KS, KE: '
        Read (*,*) KS, KE
        IF (KS < 0) KS = (KS+1) + ND1(ng,3)
        IF (KE < 0) KE = (KE+1) + ND1(ng,3)
      End If

      Do dir = 1, 3
        If (ND1(ng,dir) > 1) Then
          Write (*,'(A,I1,A)',advance='no') 'Alter the ', dir, '-dir value? (0=no, 1=yes): '
          Read (*,*) alter
          If (alter == 1) Then
            Write (*,'(A,I1,A)',ADVANCE='NO') 'Input new ', dir, '-dir value: '
            Read (*,*) xval
            do i = is, ie
              do j = js, je
                do k = ks, ke
                  X1(ng,i,j,k,dir) = xval
                end do
              end do
            end do
          End If
        End If
      End Do

    End If

  End Do

  Call Write_Grid(NDIM(1),ngrid(1),ND1,X1,IBLANK1, &
       prec,gf,vf,ib,grid_file)
  Write (*,'(A)') ''

  ! Step three, clean up & leave
  Deallocate(ND1, X1, IBLANK1)


  Stop
End Program moveNodes
