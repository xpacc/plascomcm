! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
Program extractProbe
  
  Implicit None

  Interface
    Subroutine ReadProbeData(funit, probeID, gridID, ijk, l0, &
                             sampleRate, numSample, sampleDT, &
                             iter_min, dt, time_min, &
                             xyz, &
                             numVar, &
                             var)

      Integer :: funit, probeID, gridID, ijk(3), l0, sampleRate, numSample, iter_min, numVar
      Real(KIND=8) :: sampleDT, dt, time_min, xyz(3)
      Real(KIND=8), Pointer :: var(:,:)
    End Subroutine ReadProbeData

    Subroutine WriteProbeData(base_fname, probeID, gridID, ijk, l0, &
                             sampleRate, numSample, sampleDT, &
                             iter_min, dt, time_min, &
                             xyz, &
                             numVar, &
                             var)

      Integer :: probeID, gridID, ijk(3), l0, sampleRate, numSample, iter_min, numVar
      Real(KIND=8) :: sampleDT, dt, time_min, xyz(3)
      Real(KIND=8), Pointer :: var(:,:)
      Character(LEN=80) :: base_fname
    End Subroutine WriteProbeData
  End Interface

  ! ... variables
  Integer :: nargc, iargc
  Integer :: numProbeGlobal, probeID, gridID, ijk(3), l0, iter_min
  Integer :: sampleRate, numSample, numvar, probeIDRead, nprobe
  Integer, Parameter :: funit = 10
  Real(KIND=8) :: dt, time_min, sampleDT, xyz(3), dtmp, itmp
  Real(KIND=8), Dimension(:,:), Pointer :: var
  Character(LEN=80) :: fname_in, fname_out

  ! ... check command line
  nargc = iargc()
  If (nargc /= 1) then
    Write (*,'(A)') 'extractProbe: ERROR'
    Write (*,'(A)') 'extractProbe <probe_fname>'
    Stop
  End If

  ! ... read probe fname
  Call Getarg(1,fname_in)

  ! .. Read the probe header and report how many are available
  Open(funit,file=trim(fname_in),status='old',form='unformatted')
  Read(funit) numProbeGlobal
  Write (*,'(A,I7.7,A)') 'File '//trim(fname_in)//' has ', numProbeGlobal, ' probe(s).'

!!$  ! ... query user on which probe is wanted
!!$  probeIDRead = 0
!!$  Do While (probeIDRead <= 0 .or. probeIDRead > numProbeGlobal)
!!$    Write (*,'(A)',ADVANCE='NO') 'Which probe ID do you want: '
!!$    Read (*,*) probeIDRead
!!$  End Do

  ! ... read unwanted data
  nullify(var)
  Do nprobe = 1, numProbeGlobal
    Call ReadProbeData(funit, probeID, gridID, ijk, l0, &
                       sampleRate, numSample, sampleDT, &
                       iter_min, dt, time_min, &
                       xyz, &
                       numVar, &
                       var)

    Call WriteProbeData(fname_in, probeID, gridID, ijk, l0, &
                       sampleRate, numSample, sampleDT, &
                       iter_min, dt, time_min, &
                       xyz, &
                       numVar, &
                       var)
  End Do

  Close(funit)
  Stop

End Program extractProbe

Subroutine ReadProbeData(funit, probeID, gridID, ijk, l0, &
                         sampleRate, numSample, sampleDT, &
                         iter_min, dt, time_min, &
                         xyz, &
                         numVar, &
                         var)

  Implicit None

  ! ... Incoming variables
  Integer :: funit, probeID, gridID, ijk(3), l0, sampleRate, numSample, iter_min, numVar
  Real(KIND=8) :: sampleDT, dt, time_min, xyz(3)
  Real(KIND=8), Pointer :: var(:,:)

  ! ... Internal variables
  Integer :: j, k

  ! ... read the data
  Read (funit) probeID, gridID, ijk, l0
  Read (funit) sampleRate, numSample, sampleDT
  Read (funit) iter_min, dt, time_min
  Read (funit) xyz
  Read (funit) numVar

  ! ... check for var
  if (.not.associated(var)) then
    allocate(var(numSample,numVar))
  end if

  ! ... last read
  Read (funit) ((var(j,k),j=1,numSample),k=1,numVar)

  Return

End Subroutine ReadProbeData

Subroutine WriteProbeData(base_fname, probeID, gridID, ijk, l0, &
                         sampleRate, numSample, sampleDT, &
                         iter_min, dt, time_min, &
                         xyz, &
                         numVar, &
                         var)

  Implicit None

  ! ... Incoming variables
  Integer :: probeID, gridID, ijk(3), l0, sampleRate, numSample, iter_min, numVar
  Real(KIND=8) :: sampleDT, dt, time_min, xyz(3)
  Real(KIND=8), Pointer :: var(:,:)
  Character(LEN=80) :: base_fname

  ! ... Internal variables
  Integer :: j, k, funit
  Character(LEN=80) :: fname, fmt
  Real(KIND=8), Parameter :: gamma = 1.4_8

  ! ... form output filename
  Write (fname,'(A,I8.8)') trim(base_fname)//'.probe_number.', probeID
  Open (funit,file=trim(fname),status='unknown',form='formatted')

  ! ... Write header (USE FOR TECPLOT)
!!$  Write (funit,'(3(A,E9.2),A)') 'TITLE="Probe @ (x,y,z) = (', xyz(1), ',', xyz(2), ',', xyz(3), ')"'
!!$  If (numVar == 4) Then
!!$    Write (funit,'(A)') 'VARIABLES = "T" "RHO" "U" "V" "P"'
!!$  Else If (numVar == 5) Then
!!$    Write (funit,'(A)') 'VARIABLES = "T" "RHO" "U" "V" "W" "P"'
!!$  End If
!!$  Write (funit,'(A,I7,A)') 'ZONE I = ', numSample, ' DATAPACKING=POINT'

  ! ... Write data (t,[rho u v w p])
  Write (fmt,'(A,I1,A)') '(,', numVar+1, '(E20.12,1X))'
  Do j = 1, numSample
    Write (funit,trim(fmt)) time_min + dble(j-1)*sampleDT, (var(j,k),k=1,numVar)
  End Do

  ! ... all done
  Close (funit)

  Return

End Subroutine WriteProbeData
