! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
Program Acoustic_Source
  ! ============================================================= !
  !                                                               !
  ! ACOUSTIC SOURCE: FLOW INITIALIZATION ROUTINE                  !
  ! Generates a grid and initial/boundary conditions for          !
  ! simulating a Gaussian-shaped, harmonic acoustic source        !
  !                                                               !
  ! ============================================================= !
  !                                                               !
  ! Written by Jesse Capecelatro (jcaps@illinois.edu)             !
  ! 23 September 2014                                             !
  !                                                               !
  ! ============================================================= !
  USE ModPLOT3D_IO
  USE ModParser
  Implicit None

  ! I/O variables
  Character(LEN=2)  :: prec(3), grid_format(3), volume_format(3), ib(3)
  Character(LEN=80) :: grid_file(3), sol_file(1)
  character(len=64) :: input_name

  !  Mesh variables
  Integer :: ngrid, ndim, I2, I3, J2
  Integer, Dimension(:,:), Pointer :: ND
  Integer, Dimension(:,:,:,:), Pointer :: IBLANK1
  Real(KIND=8), Dimension(:,:,:,:,:), Pointer :: X, Q
  Real(KIND=8) :: XMIN,XMAX,YMIN,YMAX,ZMIN,ZMAX

  Call Acoustic_Source_Init
  Call Acoustic_Source_Grid
  Call Acoustic_Source_BC
  Call Acoustic_Source_Data
  Call Finalize

Contains


  ! ============== !
  ! Initialization !
  ! ============== !
  Subroutine Acoustic_Source_Init
    Implicit None

    ! Parse the input file
    input_name="plascomcm.inp"
    call parser_init
    call parser_parsefile(trim(input_name))

    Return
  End Subroutine Acoustic_Source_Init


  ! ==================== !
  ! Create the grid/mesh !
  ! ==================== !
  Subroutine Acoustic_Source_Grid
    Implicit None

    Integer :: I, J, K

    ! Initialize the grids
    ngrid = 1
    call parser_read('ND',NDIM,3)
    Allocate(ND(ngrid,3)); ND(:,3) = 1

    ! Set grid parameters
    prec(1) = 'd' ! Precision=double
    grid_format(1) = 'm' ! Grid format = single
    volume_format(1) = 'w' ! Volume format = whole
    ib(1) = 'n' ! Iblank

    ! Number of grid points
    call parser_read('GRID1 NX',ND(1,1))
    call parser_read('GRID1 NY',ND(1,2))
    call parser_read('GRID1 NZ',ND(1,3))
    print *, 'Grid: ',ND(1,1),'x',ND(1,2),'x',ND(1,3)

    ! Allocate the arrays and assign values
    ALLOCATE(X(ngrid,MAXVAL(ND(:,1)),MAXVAL(ND(:,2)),MAXVAL(ND(:,3)),3))
    ALLOCATE(IBLANK1(ngrid,MAXVAL(ND(:,1)),MAXVAL(ND(:,2)),MAXVAL(ND(:,3))))

    ! Grid size
    call parser_read('GRID1 XMIN',XMIN)
    call parser_read('GRID1 XMAX',XMAX)
    call parser_read('GRID1 YMIN',YMIN)
    call parser_read('GRID1 YMAX',YMAX)
    call parser_read('GRID1 ZMIN',ZMIN)
    call parser_read('GRID1 ZMAX',ZMAX)

    ! Generate the grid
    Do K = 1, ND(1,3)
       DO J = 1, ND(1,2)
          Do I = 1, ND(1,1)
             If (ND(1,1) .GT. 1) Then
                ! Create X
                X(1,I,J,K,1) = DBLE(I-1)/DBLE(ND(1,1)-1)*(XMAX-XMIN) + XMIN
             Else
                X(1,I,J,K,1) = 0D0
             End If
             If (ND(1,2) .GT. 1) Then
                ! Create Y
                X(1,I,J,K,2) = DBLE(J-1)/DBLE(ND(1,2)-1)*(YMAX-YMIN) + YMIN
             Else
                X(1,I,J,K,2) = 0D0
             End If
             If (ND(1,3) .GT. 1) Then
                ! Create Z. Periodic direction, correct for length
                X(1,I,J,K,3) = DBLE(K-1)/DBLE(ND(1,3)-1)*(ZMAX-ZMIN) + ZMIN
             Else
                X(1,I,J,K,3) = 0D0
             End If
          End Do
       End Do
    End Do

    ! Write the grid file
    grid_file(1)="grid.xyz"
    Write (*,'(A)') ''
    Call Write_Grid(3,ngrid,ND,X,IBLANK1, &
         prec(1),grid_format(1),volume_format(1),ib(1),grid_file(1))
    Write (*,'(A)') ''

    return

  End Subroutine Acoustic_Source_Grid


  ! =================== !
  ! Boundary conditions !
  ! =================== !
  Subroutine Acoustic_Source_BC
    Implicit None

    Integer :: iunit

    ! Open the file
    iunit=11
    open(iunit,file="bc.dat")

    print *, 'Writing boundary conditions'
    Write (*,'(A)') ''

    ! Write the header
    write(iunit,'(1a1 )') "#"
    write(iunit,'(1a45)') "# Basic boundary condition file for PlasComCM"
    write(iunit,'(1a1 )') "#"
    write(iunit,'(1a24)') "# FD = finite difference"
    write(iunit,'(1a20)') "# FV = finite volume"
    write(iunit,'(1a1 )') "#"
    write(iunit,'(1a8 )') "# ibType"
    write(iunit,'(1a8 )') "# ======"
    write(iunit,'(1a32)') "#     00     OUTER_BOUNDARY (FD)"
    write(iunit,'(1a41)') "#     21     SAT_SLIP_ADIABATIC_WALL (FD)"
    write(iunit,'(1a44)') "#     22     SAT_NOSLIP_ISOTHERMAL_WALL (FD)"
    write(iunit,'(1a33)') "#     23     SAT_NOSLIP_ADIABATIC_WALL (FD)"
    write(iunit,'(1a31)') "#     24     SAT_FAR_FIELD (FD)"
    write(iunit,'(1a37)') "#     25     SAT_BLOCK_INTERFACE (FD)"
    write(iunit,'(1a51)') "#     31     NSCBC_INFLOW_VELOCITY_TEMPERATURE (FD)"
    write(iunit,'(1a63)') "#     32     NSCBC_INFLOW_VELOCITY_TEMPERATURE_FROM_SPONGE (FD)"
    write(iunit,'(1a57)') "#     41     NSCBC_OUTFLOW_PERFECT_NONREFLECTION (FD, FV)"
    write(iunit,'(1a54)') "#     42     NSCBC_OUTFLOW_PERFECT_REFLECTION (FD, FV)"
    write(iunit,'(1a47)') "#     51     NSCBC_WALL_ADIABATIC_SLIP (FD, FV)"
    write(iunit,'(1a50)') "#     52     NSCBC_WALL_ISOTHERMAL_NOSLIP (FD, FV)"
    write(iunit,'(1a39)') "#     61     NSCBC_SUBSONIC_INFLOW (FD)"
    write(iunit,'(1a45)') "#     62     NSCBC_SUPERSONIC_INFLOW (FD, FV)"
    write(iunit,'(1a29)') "#     73     PERIODICITY (FV)"
    write(iunit,'(1a30)') "#     91     FV_DIRICHLET (FV)"
    write(iunit,'(1a28)') "#     92     FV_NEUMANN (FV)"
    write(iunit,'(1a29)') "#     93     FV_SYMMETRY (FV)"
    write(iunit,'(1a36)') "#     94     FV_BLOCK_INTERFACE (FV)"
    write(iunit,'(1a45)') "#     95     FV_BLOCK_INTERFACE_PERIODIC (FV)"
    write(iunit,'(1a28)') "#     99     SPONGE (FD, FV)"
    write(iunit,'(1a1 )') "#"
    write(iunit,'(1a56)') "# Grid ibType ibDir   is    ie    js    je     ks    ke "
    write(iunit,'(1a56)') "# ==== ====== =====  ====  ====  ====  ====   ====  ===="

    ! Input the boundary conditions
    write(iunit,'(9I6)')      1,   24,     1,    1,     1,    1,   -1,    1,    -1
    write(iunit,'(9I6)')      1,   24,    -1,   -1,    -1,    1,   -1,    1,    -1
    write(iunit,'(9I6)')      1,   24,    -2,    1,    -1,   -1,   -1,    1,    -1
    write(iunit,'(9I6)')      1,   24,     2,    1,    -1,    1,    1,    1,    -1

    ! Close the file
    close(iunit)

    return

  End Subroutine Acoustic_Source_BC


  ! ========================= !
  ! Initial (target) solution !
  ! ========================= !
  Subroutine Acoustic_Source_Data
    Implicit None

    integer :: I, J, K, EQ_STATE
    Real(KIND=8), Dimension(:,:,:,:), Pointer :: rho,rhoU,rhoV,rhoW,rhoE
    Real(KIND=8) :: rho0, P0, mu0, L0, gamma, C, Re, Pr, TAU(4)

    ! Allocate solution array
    Allocate(Q(ngrid,MAXVAL(ND(:,1)),MAXVAL(ND(:,2)),MAXVAL(ND(:,3)),5))

    ! Link the pointers
    rho   =>  Q(:,:,:,:,1)
    rhoU  =>  Q(:,:,:,:,2)
    rhoV  =>  Q(:,:,:,:,3)
    rhoW  =>  Q(:,:,:,:,4)
    rhoE  =>  Q(:,:,:,:,5)

    ! Read from input
    call parser_read('REYNOLDS_NUMBER',Re,0D0)
    call parser_read('PRANDTL_NUMBER',Pr,0.72D0)
    call parser_read('DENSITY_REFERENCE',rho0,1.0D0)
    call parser_read('PRESSURE_REFERENCE',P0)
    call parser_read('GAMMA_REFERENCE',gamma,1.4D0)
    call parser_read('SNDSPD_REFERENCE',C,347.0D0)
    call parser_read('GAS_EQUATION_OF_STATE',EQ_STATE,0)
    call parser_read('LENGTH_REFERENCE',L0)

    ! Initialize the timing
    TAU(1) = 0D0
    TAU(2) = Pr
    TAU(3) = Re
    TAU(4) = 0D0

    ! Initialize the variable array
    rho  = 1.0D0
    rhoU = 0.0D0
    rhoV = 0.0D0
    rhoW = 0.0D0

    ! Compute energy
    if (EQ_STATE == 0) then
       rhoE = P0/(gamma-1.0D0)/rho0/C**2
    else
       Write (*,'(A)') 'Acoustic_source only implemented using ideal gas law'
       Stop
    end if

    ! Compute viscosity
    mu0 = rho0*C*L0/Re

    ! Write the solution file
    sol_file(1)="RocFlo-CM.00000000.target.q"
    Call Write_Soln(3,ngrid,ND,Q,TAU,prec(1),grid_format(1),volume_format(1),sol_file(1))

    Write (*,'(A)') ''

    return

  End Subroutine Acoustic_Source_Data


  ! ================== !
  ! Clean up and leave !
  ! ================== !
  Subroutine Finalize
    Implicit None
  
    Deallocate(ND,IBLANK1,X,Q)

    return
    
  End Subroutine Finalize

End Program Acoustic_Source
