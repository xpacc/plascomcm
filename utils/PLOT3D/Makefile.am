# Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
# License: MIT, http://opensource.org/licenses/MIT
# Initialize variables
EXTRA_DIST =

# Useful binaries for operating on PLOT3D files
bin_PROGRAMS =  detectformat cart3d appendgrids diffsoln readsolntime 
bin_PROGRAMS += extrudemesh reversedir ogrid subsetmesh genogrid subsetsoln
bin_PROGRAMS += setiblank extractvalue mergegrids hrefine computexzaverage
bin_PROGRAMS += cart2d movenodes searchandreplaceiblank assemblesoln
bin_PROGRAMS += assemblegrid processpiv computemean extrudesoln extractprobe 
bin_PROGRAMS += computefunc gencircularsectorgrid data2ensight swapendian
bin_PROGRAMS += modifyiter modifytime modifytimefornextoutput
bin_PROGRAMS += compare-p3d boundarylayer jetcrossflow flame1D

if BUILD_OVERSET
bin_PROGRAMS += overset-sandbox
endif

noinst_LIBRARIES = libplot3d.a
libplot3d_a_SOURCES = ModPLOT3D_IO.f90 ModParser.f90 plot3d_format.c
LDADD = libplot3d.a

detectformat_SOURCES = detectFormat.f90
cart3d_SOURCES = cart3d.f90
boundarylayer_SOURCES = boundary-layer.f90
flame1D_SOURCES = flame1D.f90
jetcrossflow_SOURCES = jet-crossflow.f90
appendgrids_SOURCES = appendGrids.f90
diffsoln_SOURCES = diffSoln.f90
readsolntime_SOURCES = readSolnTime.f90
extrudemesh_SOURCES = extrudeMesh.f90
reversedir_SOURCES = reverseDir.f90 
ogrid_SOURCES = Ogrid.f90
subsetmesh_SOURCES = subsetMesh.f90
genogrid_SOURCES = genOgrid.f90
subsetsoln_SOURCES = subsetSoln.f90
setiblank_SOURCES = setIBLANK.f90
extractvalue_SOURCES = extractValue.f90
mergegrids_SOURCES = mergeGrids.f90
hrefine_SOURCES = hRefine.f90
computexzaverage_SOURCES = computeXZAverage.f90
cart2d_SOURCES = cart2d.f90  
movenodes_SOURCES = moveNodes.f90
searchandreplaceiblank_SOURCES = searchAndReplaceIBLANK.f90
assemblesoln_SOURCES = assembleSoln.f90
assemblegrid_SOURCES = assembleGrid.f90 
processpiv_SOURCES = processPIV.f90
computemean_SOURCES = computeMean.f90
extrudesoln_SOURCES = extrudeSoln.f90
extractprobe_SOURCES = extractProbe.f90
computefunc_SOURCES = computeFunc.f90
gencircularsectorgrid_SOURCES = genCircularSectorGrid.f90
data2ensight_SOURCES = data2ensight.f90
swapendian_SOURCES = swapEndian.c
modifyiter_SOURCES = modifyIter.c
modifytime_SOURCES = modifyTime.c
modifytimefornextoutput_SOURCES = modifyTimeForNextOutput.c
compare_p3d_SOURCES = compare-p3d.f90
compare_p3d_LDADD = $(LDADD) -L.. -lutils
compare_p3d_DEPENDENCIES = ../libutils.a
overset_sandbox_SOURCES = overset-sandbox.f90
overset_sandbox_LDADD = $(LDADD) -L.. -lutils -L../overset -loverset
overset_sandbox_DEPENDENCIES = ../libutils.a ../overset/liboverset.a
# checkrate_SOURCES = checkRate.f90
# checkrate_LDADD = $(LDADD) -L.. -lutils
# checkrate_DEPENDENCIES = ../libutils.a
# premixed_SOURCES = premixed.f90
# premixed_LDADD = $(LDADD) -L.. -lutils
# premixed_DEPENDENCIES = ../libutils.a

EXTRA_DIST += plascomcm_overset-sandbox.inp

UTILSMODDIR = ../mod

if BUILD_OVERSET
OVERSETMODDIR = ../overset/mod
endif

FC_MODEXT = @FC_MODEXT@
FC_MODINC   = @FC_MODINC@
FC_MODOUT   = @FC_MODOUT@
FC_MODCASE  = @FC_MODCASE@
AM_FCFLAGS = $(FC_MODOUT). $(FC_MODINC). $(FC_MODINC)$(UTILSMODDIR)

if BUILD_OVERSET
AM_FCFLAGS += $(FC_MODINC)$(OVERSETMODDIR)
endif

clean-local:
	rm -f *.$(FC_MODEXT)
	rm -f obj/*
	rm -rf *.dSYM
