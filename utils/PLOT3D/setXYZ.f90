! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
Program setXYZ
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                                                               !
! adjust the coordinates of a PLOT3D mesh                       !
!                                                               !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                                                               !
! Modified by DJB (bodony@illinois.edu)                         !
! 15 April 2009                                                 !
!                                                               !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  USE ModPLOT3D_IO
  Implicit None

  !  Arrays
  Real(KIND=8), Dimension(:,:,:,:,:), Pointer :: X1, X2
  Real(KIND=8) :: XMIN, XMAX, YMIN, YMAX, ZMIN, ZMAX

  !  Integers
  Integer :: ngrid(3), I, J, K, dir, NX, NY, NZ, NDIM(3)
  Integer, Dimension(:,:), Pointer :: ND1, ND2
  Integer, Dimension(:,:,:,:), Pointer :: IBLANK1, IBLANK2

  !  Logical
  Logical :: gf_exists(3), sf_exists(3)

  !  Characters
  Character(LEN=2)  :: prec, gf, vf, ib
  Character(LEN=80) :: grid_file, cmd, file

  !  Step zero, read in the command line
  Integer :: iargc, nargc, ng, isperiodic, ftype
  Integer :: IS, IE, JS, JE, KS, KE, ibval, alter, oldIB, newIB
  Character(LEN=1) :: charinput
  Real :: newXYZ

  nargc = iargc()
  If (nargc /= 2) Then
    Write (*,'(A)') 'USAGE: setXYZ {grid file 1} {altered grid file}'
    Stop
  Endif

  Call Getarg(1,file);
  Call Getarg(2,grid_file);

  Call p3d_detect(LEN(Trim(file)),Trim(file),prec,NDIM,gf,vf,ib,ftype)

  !  Step one, read in the mesh1 grid file
  Write (*,'(A)') ''
  Call Read_Grid(NDIM(1),ngrid(1),ND1,X1,IBLANK1, &
       prec,gf,vf,ib,file,.TRUE.)
  Write (*,'(A)') ''

  !  Warn user
  Write (*,'(A)') 'WARNING: This code modifies the ENTIRE file.'
  Write (*,'(A)', ADVANCE='NO') 'Press enter to continue: '
  Read (*,'(A)') charinput

  ! Collect information
  Write (*,'(A)', ADVANCE='NO') 'ENTER coordinate (1,2,3) to be changed: '
  Read (*,*) oldIB

  Write (*,'(A)', ADVANCE='NO') 'ENTER new coordinate value: '
  Read (*,*) newXYZ

  ! Alter
  X1(:,:,:,:,oldIB) = newXYZ

  ib = 'y'
  Call Write_Grid(NDIM(1),ngrid(1),ND1,X1,IBLANK1, &
       prec,gf,vf,ib,grid_file)
  Write (*,'(A)') ''

  ! Step three, clean up & leave
  Deallocate(ND1, X1, IBLANK1)


  Stop
End Program setXYZ
