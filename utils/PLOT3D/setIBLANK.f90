! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
Program setIBLANK
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                                                               !
! adjust the IBLANK value of a PLOT3D mesh                      !
!                                                               !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                                                               !
! Written by Daniel J. Bodony (bodony@stanford.edu)             !
! 31 January 2007                                               !
!                                                               !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  USE ModPLOT3D_IO
  Implicit None

  !  Arrays
  Real(KIND=8), Dimension(:,:,:,:,:), Pointer :: X1, X2
  Real(KIND=8) :: XMIN, XMAX, YMIN, YMAX, ZMIN, ZMAX

  !  Integers
  Integer :: ngrid(3), I, J, K, dir, NX, NY, NZ, NDIM(3)
  Integer, Dimension(:,:), Pointer :: ND1, ND2
  Integer, Dimension(:,:,:,:), Pointer :: IBLANK1, IBLANK2

  !  Logical
  Logical :: gf_exists(3), sf_exists(3)

  !  Characters
  Character(LEN=2)  :: prec, gf, vf, ib
  Character(LEN=80) :: grid_file, cmd, file

  !  Step zero, read in the command line
  Integer :: iargc, nargc, ng, isperiodic, ftype
  Integer :: IS, IE, JS, JE, KS, KE, ibval, alter
  Integer, Parameter :: TRUE = 1

  nargc = iargc()
  If (nargc /= 2) Then
    Write (*,'(A)') 'USAGE: setIBLANK {grid file 1} {altered grid file}'
    Stop
  Endif

  Call Getarg(1,file);
  Call Getarg(2,grid_file);

  Call p3d_detect(LEN(Trim(file)),Trim(file),prec,NDIM,gf,vf,ib,ftype)

  !  Step one, read in the mesh1 grid file
  Write (*,'(A)') ''
  Call Read_Grid(NDIM(1),ngrid(1),ND1,X1,IBLANK1, &
       prec,gf,vf,ib,file,TRUE)
  Write (*,'(A)') ''

  !  Step two, get parameters from user
  Do ng = 1, ngrid(1)

    Write (*,'(A,I2,A,3(I5,1X))') 'Block ', ng, ' has size ', &
         (ND1(ng,i),i=1,3)

    Write (*,'(A)',advance='no') 'Alter this block? (0=no, 1=yes): '
    Read (*,*) alter

    If (alter == 1) Then
      IS = 1; IE = 1;
      If (ND1(ng,1) > 1) Then
        Write (*,'(A)',ADVANCE='NO') 'Input IS, IE: '
        Read (*,*) IS, IE
        IF (IE < 0) IE = ND1(ng,1) + IE + 1
        IF (IS < 0) IS = ND1(ng,1) + IS + 1
      End If

      JS = 1; JE = 1;
      If (ND1(ng,2) > 1) Then
        Write (*,'(A)',ADVANCE='NO') 'Input JS, JE: '
        Read (*,*) JS, JE
        IF (JE < 0) JE = ND1(ng,2) + JE + 1
        IF (JS < 0) JS = ND1(ng,2) + JS + 1
      End If

      KS = 1; KE = 1;
      If (ND1(ng,3) > 1) Then
        Write (*,'(A)',ADVANCE='NO') 'Input KS, KE: '
        Read (*,*) KS, KE
        IF (KE < 0) KE = ND1(ng,3) + KE + 1
        IF (KS < 0) KS = ND1(ng,3) + KS + 1
      End If

      Write (*,'(A)',ADVANCE='NO') 'Input IBLANK value: '
      Read (*,*) ibval

      IBLANK1(ng,IS:IE,JS:JE,KS:KE) = ibval
    End If

  End Do

  ib = 'y'
  Call Write_Grid(NDIM(1),ngrid(1),ND1,X1,IBLANK1, &
       prec,gf,vf,ib,grid_file)
  Write (*,'(A)') ''

  ! Step three, clean up & leave
  Deallocate(ND1, X1, IBLANK1)


  Stop
End Program setIBLANK
