Instructions for setting up and running the code in axisymmetric mode on the XPACC cluster
machines (see INSTALL.md for general instructions).

Dependencies
============

Cantera
-------

(Optional) Install Intel compilers:

On xpacc-cluster-0, can use:
```
module load intel-x86_64
module load mvapich2-x86_64-intel
```
On xpacc-serv-[01-03], can use:
```
module load xpacc-mvapich-intel-x86_64/2-2.0rc2
```
Install Python, Cython, and NumPy:

On xpacc-cluster-0:
```
module load opt-python
```
On xpacc-serv-[01-03]:
```
module load python/2.7.3-x86_64
pip install --user cython
pip install --user numpy
```
Install SCons:
```
wget http://sourceforge.net/projects/scons/files/scons/2.3.0/scons-2.3.0.tar.gz
tar -xzvf scons-2.3.0.tar.gz
cd scons-2.3.0
python setup.py install --prefix=$HOME/scons
```
Set up PATH to find SCons, e.g.:
```
export PATH="$HOME/scons/bin:$PATH"
```
Install Cantera:
```
git clone https://github.com/Cantera/cantera.git cantera-src
cd cantera-src
git checkout 2.1 (or newer version)
scons build prefix=$HOME/cantera CXX=<CXX> CC=<CC> F90=<F90> F77=<F77> python_package=full
  use_sundials=n debug=false
```
(where <CXX>, <CC>, <F90>, and <F77> are replaced with the compilers you want to build with.)
```
scons install
```

Building/running the code
=========================

When building PlasComCM, use the instructions from INSTALL.md, except run configure as follows:
```
./configure FC=<FC> CC=<CC> --with-cantera=$HOME/cantera --exec-prefix=$PWD
  --enable-axisymmetric --enable-opts
```
(where <FC> and <CC> are replaced with the compilers used when building Cantera. MPI must also
be set up to use these compilers.)

Build the rest as normal. To run, start from the root PlasComCM directory and do the following:
```
cd inputs/flame
tar -xvf flame.bz2
```
Find the location of the plascomcm executable, and then run it from the flame directory using the
provided restart file, e.g.:
```
../../src/plascomcm RocFlo-CM.16300000.q
```
Note: If using Intel compilers, you may need to set the F_UFMTENDIAN environment variable:
```
export F_UFMTENDIAN=big
```
