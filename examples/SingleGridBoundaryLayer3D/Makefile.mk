if BUILD_EXAMPLES

noinst_PROGRAMS += SingleGridBoundaryLayer3D/SingleGridBoundaryLayer3D

endif

SingleGridBoundaryLayer3D_SingleGridBoundaryLayer3D_SOURCES = \
  SingleGridBoundaryLayer3D/SingleGridBoundaryLayer3D.f90
SingleGridBoundaryLayer3D_SingleGridBoundaryLayer3D_DEPENDENCIES = \
  libexamples-common.a

example_extras += \
  SingleGridBoundaryLayer3D/README \
  SingleGridBoundaryLayer3D/plascomcm_plot3d.inp \
  SingleGridBoundaryLayer3D/plascomcm_hdf5.inp
