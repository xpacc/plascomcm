if BUILD_EXAMPLES

noinst_PROGRAMS += Quiescent3D/Quiescent3D

endif

Quiescent3D_Quiescent3D_SOURCES = Quiescent3D/Quiescent3D.f90
Quiescent3D_Quiescent3D_DEPENDENCIES = libexamples-common.a

example_extras += \
  Quiescent3D/README \
  Quiescent3D/plascomcm_plot3d.inp \
  Quiescent3D/plascomcm_hdf5.inp
