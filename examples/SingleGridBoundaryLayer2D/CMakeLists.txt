#
# Build SingleGridBoundaryLayer2D example
#
add_executable(SingleGridBoundaryLayer2D SingleGridBoundaryLayer2D.f90)
set_target_properties(SingleGridBoundaryLayer2D PROPERTIES COMPILE_FLAGS "${F90-Flags}")
if(AutodetectMPI)
  set_target_properties(SingleGridBoundaryLayer2D PROPERTIES COMPILE_FLAGS "${MPI_FORTRAN_COMPILE_FLAGS}"
                                               LINK_FLAGS "${MPI_Fortran_LINK_FLAGS}")
endif()
if(BuildStatic)
  set_target_properties(SingleGridBoundaryLayer2D PROPERTIES LINK_FLAGS "-Wl,-Bdynamic")
endif()
target_link_libraries(SingleGridBoundaryLayer2D ${exampleExtraLibs})
install(TARGETS SingleGridBoundaryLayer2D RUNTIME DESTINATION bin/examples LIBRARY DESTINATION lib ARCHIVE DESTINATION lib)
install(FILES README plascomcm_hdf5.inp plascomcm_plot3d.inp DESTINATION examples/SingleGridBoundaryLayer2D)
