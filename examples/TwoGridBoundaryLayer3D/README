TwoGridBoundaryLayer3D - Boundary layer flow in 3D, plane periodic in x and y, two grids

Usage: ./TwoGridBoundaryLayer3D <options>

Options:
  --format (-f)  Grid output format ('plot3d' or 'hdf5')
  --size-bg (-N) Number of grid points along x direction on coarse background grid (other directions
                 are proportional)
  --size-fg (-n) Number of grid points along x direction on fine foreground grid (other directions
                 are proportional)
  --interp (-i)  Interpolation mode ('linear' or 'cubic')

Input files:
  PLOT3D: plascomcm_plot3d.inp, bc.dat, grid.xyz (grid), initial.q (target/restart),
    XINTOUT.HO.2D, XINTOUT.X.2D
  HDF5: plascomcm_hdf5.inp, bc.dat, initial.h5 (grid/target/restart), XINTOUT.HO.2D, XINTOUT.X.2D

Notes for running in PlasComCM:
  * If using the generated input files with a different version of PlasComCM, will need to copy
    over the hard-coded periodicity information from ModifyInput in src/ModRegion.fpp (look for the
    corresponding CASE_ID from the .inp file)
