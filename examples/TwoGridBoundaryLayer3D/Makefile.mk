if BUILD_EXAMPLES
if BUILD_OVERSET

noinst_PROGRAMS += TwoGridBoundaryLayer3D/TwoGridBoundaryLayer3D

endif
endif

TwoGridBoundaryLayer3D_TwoGridBoundaryLayer3D_SOURCES = \
  TwoGridBoundaryLayer3D/TwoGridBoundaryLayer3D.f90
TwoGridBoundaryLayer3D_TwoGridBoundaryLayer3D_DEPENDENCIES = \
  libexamples-common.a $(abs_top_builddir)/utils/overset/liboverset.a

example_extras += \
  TwoGridBoundaryLayer3D/README \
  TwoGridBoundaryLayer3D/plascomcm_plot3d.inp \
  TwoGridBoundaryLayer3D/plascomcm_hdf5.inp
