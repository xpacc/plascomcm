program TwoGridBoundaryLayer3D

  use ModExamplesCommon
  use ModBC
  use ModParseArguments
  use ModStdIO
  use ModOverset, overset_ik=>ik, overset_rk=>rk, overset_MAX_ND=>MAX_ND, &
    overset_OUTPUT_UNIT=>OUTPUT_UNIT, overset_INPUT_UNIT=>INPUT_UNIT, overset_ERROR_UNIT=>ERROR_UNIT
  use ModPegasus
  implicit none

  integer(ik) :: i, j, k, m
  character(len=CMD_ARG_LENGTH), dimension(:), allocatable :: RawArguments
  type(t_cmd_opt), dimension(4) :: Options
  integer(ik) :: OutputFormat
  character(len=PATH_LENGTH) :: OutputFile
  integer(ik) :: Nbg, Nfg
  integer(ik) :: InterpScheme
  real(rk) :: U, V, W
  integer(ik), dimension(3,2) :: nPoints
  real(rk), dimension(3,2) :: Lengths
  real(rk), dimension(:,:,:,:,:), allocatable :: X
  integer(ik), dimension(3,2) :: Periodic
  integer(ik), dimension(2) :: PeriodicStorage
  real(rk), dimension(3,2) :: PeriodicLength
  integer(ik), dimension(:,:,:,:), allocatable :: IBlank
  integer(ik), dimension(2) :: GridType
  type(t_grid), dimension(2) :: Grids
  type(t_interp), dimension(2) :: InterpData
  type(t_pegasus) :: PegasusData
  real(rk), dimension(:,:,:,:,:), allocatable :: Q
  type(t_bc), dimension(3) :: BCs
  real(rk) :: Gamma
  real(rk) :: Temperature
  real(rk) :: Density
  real(rk), dimension(3) :: Velocity
  integer(ik), dimension(3,2) :: nSponge

  allocate(RawArguments(command_argument_count()))
  do i = 1, size(RawArguments)
    call get_command_argument(i, RawArguments(i))
  end do

  Options(1) = t_cmd_opt_("format", "f", OPT_VALUE_TYPE_STRING)
  Options(2) = t_cmd_opt_("size-bg", "N", OPT_VALUE_TYPE_INTEGER)
  Options(3) = t_cmd_opt_("size-fg", "n", OPT_VALUE_TYPE_INTEGER)
  Options(4) = t_cmd_opt_("interp", "i", OPT_VALUE_TYPE_STRING)

  call ParseArguments(RawArguments, Options=Options)

  if (Options(1)%is_present) then
    if (Options(1)%value == "plot3d") then
      OutputFormat = OUTPUT_FORMAT_PLOT3D
    else if (Options(1)%value == "hdf5") then
      OutputFormat = OUTPUT_FORMAT_HDF5
    else
      write (ERROR_UNIT, '(3a)') "ERROR: Unrecognized grid format '", trim(Options(1)%value), "'."
      stop 1
    end if
  else
    OutputFormat = DefaultOutputFormat
  end if

  if (Options(2)%is_present) then
    read (Options(2)%value, *) Nbg
  else
    Nbg = 41
  end if

  if (Options(3)%is_present) then
    read (Options(3)%value, *) Nfg
  else
    Nfg = 41
  end if

  if (Options(4)%is_present) then
    if (Options(4)%value == "linear") then
      InterpScheme = INTERP_LINEAR
    else if (Options(4)%value == "cubic") then
      InterpScheme = INTERP_CUBIC
    else
      write (ERROR_UNIT, '(3a)') "ERROR: Unrecognized interpolation scheme '", &
        trim(Options(4)%value), "'."
      stop 1
    end if
  else
    InterpScheme = INTERP_LINEAR
  end if

  nPoints(:,1) = [Nbg, Nbg/2, Nbg/2]
  nPoints(:,2) = [Nfg, Nfg/2, Nfg/2]

  Lengths(:,1) = [1._rk, 0.5_rk, 0.5_rk]
  Lengths(:,2) = [0.85_rk, 0.7_rk, 0.7_rk] * Lengths(:,1)

  allocate(X(3,maxval(nPoints(1,:)),maxval(nPoints(2,:)),maxval(nPoints(3,:)),2))

  do m = 1, 2
    do k = 1, nPoints(3,m)
      do j = 1, nPoints(2,m)
        do i = 1, nPoints(1,m)
          U = real(i-1, kind=rk)/real(nPoints(1,m)-1, kind=rk)
          V = real(j-1, kind=rk)/real(nPoints(2,m)-1, kind=rk)
          W = real(k-1, kind=rk)/real(nPoints(3,m)-1, kind=rk)
          X(:,i,j,k,m) = Lengths(:,m) * ([U,V,W] - 0.5_rk)
        end do
      end do
    end do
  end do

  Periodic = FALSE
  PeriodicStorage = NO_OVERLAP_PERIODIC
  PeriodicLength = 0._rk

  Periodic(:2,1) = PLANE_PERIODIC
  PeriodicStorage(1) = OVERLAP_PERIODIC
  PeriodicLength(:2,1) = Lengths(:2,1)

  allocate(IBlank(maxval(nPoints(1,:)),maxval(nPoints(2,:)),maxval(nPoints(3,:)),2))

  IBlank = 1

  GridType = GRID_TYPE_CARTESIAN

  do m = 1, 2
    call MakeGrid(Grids(m), nDims=3, iStart=[1,1,1], iEnd=nPoints(:,m), &
      Coords=X(:,:nPoints(1,m),:nPoints(2,m),:nPoints(3,m),m), Periodic=Periodic(:,m), &
      PeriodicStorage=PeriodicStorage(m), PeriodicLength=PeriodicLength(:,m), &
      IBlank=IBlank(:nPoints(1,m),:nPoints(2,m),:nPoints(3,m),m), ID=m, GridType=GridType(m))
  end do

  call AssembleOverset(Grids, InterpData, FringeSize=2, InterpScheme=InterpScheme)
  call MakePegasusData(Grids, InterpData, PegasusData)

  do m = 1, 2
    IBlank(:nPoints(1,m),:nPoints(2,m),:nPoints(3,m),m) = reshape(Grids(m)%iblank, &
      [nPoints(1,m),nPoints(2,m),nPoints(3,m)])
  end do

  allocate(Q(5,maxval(nPoints(1,:)),maxval(nPoints(2,:)),maxval(nPoints(3,:)),2))

  Gamma = 1.4_rk
  Temperature = 1._rk/(Gamma - 1._rk)
  Density = 1._rk
  Velocity = [0.2_rk, 0._rk, 0._rk]

  Q(1,:,:,:,:) = Density
  Q(2,:,:,:,:) = Density * Velocity(1)
  Q(3,:,:,:,:) = Density * Velocity(2)
  Q(4,:,:,:,:) = Density * Velocity(3)
  Q(5,:,:,:,:) = Density * (Temperature/Gamma + 0.5_rk * sum(Velocity**2))

  nSponge = nPoints/6

  BCs(1) = t_bc_(1, BC_TYPE_SAT_NOSLIP_ISOTHERMAL_WALL, 3, [1,-1], [1,-1], [1,1])
  BCs(2) = t_bc_(1, BC_TYPE_SAT_FAR_FIELD, -3, [1,-1], [1,-1], [-1,-1])
  BCs(3) = t_bc_(1, BC_TYPE_SPONGE, -3, [1,-1], [1,-1], [-nSponge(3,1),-1])

  select case (OutputFormat)
  case (OUTPUT_FORMAT_PLOT3D)
    call WritePLOT3DGrid("grid.xyz", nDims=3, nGrids=2, nPoints=nPoints, X=X, IBlank=IBlank)
    call WritePLOT3DSolution("initial.q", nDims=3, nGrids=2, nPoints=nPoints, Q=Q)
  case (OUTPUT_FORMAT_HDF5)
    call WriteHDF5("initial.h5", nDims=3, nGrids=2, nPoints=nPoints, X=X, IBlank=IBlank, Q=Q, &
      WriteTarget=.true.)
  end select

  call WriteBCs(BCs, "bc.dat")
  call WritePegasusData(PegasusData, "XINTOUT.HO.2D", "XINTOUT.X.2D")

end program TwoGridBoundaryLayer3D
