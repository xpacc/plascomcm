#
# CMake configuration file 
# Adopt standards based on KDE style:
#  https://community.kde.org/Policies/CMake_Coding_Style
#  This will help seperate native CMake variables and our local variables
#  some highlights:
#  * lower case for CMake commands
#  * Camel Case for Variables, starting with lower case
#  * Camel Case for functions and options, starting with upper case
#  ** All Caps for Acronyms, followed by a hyphen for readability
#
cmake_minimum_required (VERSION 3.0)
project(PlasComCM Fortran C CXX)
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/CMake/Modules")

if(BuildStatic)
  set(CMAKE_SKIP_BUILD_RPATH TRUE)
else()
  set(CMAKE_INSTALL_RPATH "${CMAKE_INSTALL_PREFIX}/lib")
  set(CMAKE_INSTALL_RPATH_USE_LINK_PATH TRUE)
endif()
#
# setup various options to configure the code
#
option(AutodetectMPI    "Automatically detect MPI" ON)
option(BuildStatic      "Build static libraries" OFF)
option(PreProcTool      "Use a third-party tool to preprocess source files" OFF)
set(PreProcToolFlags    "" CACHE STRING "Additonal flags to give to the preprocessor")
set(PreProcToolBinary   "" CACHE STRING "Location of the preprocessor")
option(Debug            "Turn on debugging" OFF)
option(Opts             "Turn on aggressive optimizations" OFF)
option(OptBC            "Use optimized BC routines (EXPERIMENTAL)" OFF)
option(OptSIMD          "Enable developer placed SIMD directives" OFF)
option(VecReport        "Enable compiler vectorization report" OFF)
option(KeepAssemply     "Request the compiler to leave the assembly files" OFF)
option(Combustion       "Compile with combustion support enabled" OFF)
option(Axisymmetric     "Compile in axisymmetric mode" OFF)
option(TM-Solver        "Compile with thermo-mechanical solver enabled" OFF)
option(EF-Solver        "Compile with electric field solver enabled" OFF)
option(Coverage         "Turn on coverage analysis using gcc and gcov" OFF)
option(Tests            "Enable building of tests" OFF)
option(OversetTool      "Enable building of overset tool" OFF)
option(OversetVerbose   "Enable high verbosity in the overset tool" OFF)
option(Examples         "Enable building of examples" OFF)
option(PerfPlot         "Enable generation of performance plots" OFF)
option(Profiling        "Enable profiling with Tau" OFF)
option(BuildDocumentation "Create and install the HTML based API documentation (requires Doxygen)" OFF)
set(BatchTestSubmission "" CACHE STRING "How to submit test jobs to the batch system")
set(F77-FlagsOverride "" CACHE STRING "Additional Compiler flags for the F77 compiler")
set(F90-FlagsOverride "" CACHE STRING "Additional Compiler flags for the F90 compiler")
set(C-FlagsOverride   "" CACHE STRING "Additional Compiler flags for the C compiler")
set(CXX-FlagsOverride "" CACHE STRING "Additional Compiler flags for the CXX compiler")

include(FortranCInterface)
FortranCInterface_HEADER(${PROJECT_BINARY_DIR}/include/FC.h MACRO_NAMESPACE "FC_")
FortranCInterface_VERIFY(CXX)
add_definitions(-DCMAKE_BUILD)
include_directories(${PROJECT_BINARY_DIR}/include)
#
# guard against in-source builds
#
if(${CMAKE_SOURCE_DIR} STREQUAL ${CMAKE_BINARY_DIR})
  message(FATAL_ERROR "In-source builds not allowed. Please make a new directory (called a build directory) and run CMake from there. You may need to remove CMakeCache.txt. ")
endif()

# configure MPI
if(AutodetectMPI)
  find_package(MPI REQUIRED)
  #add_definitions( -DMPICH_IGNORE_CXX_SEEK )
  message("Autodetecting MPI. Check the following to see what we found.")
  if(MPI_CXX_FOUND)
    message("MPI_CXX_FOUND ${MPI_CXX_FOUND}")
    message("\t MPI_CXX_COMPILER ${MPI_CXX_COMPILER}")
    message("\t MPI_CXX_COMPILER_FLAGS ${MPI_CXX_COMPILER_FLAGS}")
    message("\t MPI_CXX_INCLUDE_PATH ${MPI_CXX_INCLUDE_PATH}")
    message("\t MPI_CXX_LINK_FLAGS ${MPI_CXX_LINK_FLAGS}")
    message("\t MPI_CXX_LIBRARIES ${MPI_CXX_LIBRARIES}")
    include_directories(${MPI_CXX_INCLUDE_PATH})
  endif()
  if(MPI_C_FOUND)
    message("MPI_C_FOUND ${MPI_C_FOUND}")
    message("\t MPI_C_COMPILER ${MPI_C_COMPILER}")
    message("\t MPI_C_COMPILER_FLAGS ${MPI_C_COMPILER_FLAGS}")
    message("\t MPI_C_INCLUDE_PATH ${MPI_C_INCLUDE_PATH}")
    message("\t MPI_C_LINK_FLAGS ${MPI_C_LINK_FLAGS}")
    message("\t MPI_C_LIBRARIES ${MPI_C_LIBRARIES}")
    include_directories(${MPI_C_INCLUDE_PATH})
  endif()
  if(MPI_Fortran_FOUND)
    message("MPI_Fortran_FOUND ${MPI_Fortran_FOUND}")
    message("\t MPI_Fortran_COMPILER ${MPI_Fortran_COMPILER}")
    message("\t MPI_Fortran_COMPILER_FLAGS ${MPI_Fortran_COMPILER_FLAGS}")
    message("\t MPI_Fortran_INCLUDE_PATH ${MPI_Fortran_INCLUDE_PATH}")
    message("\t MPI_Fortran_LINK_FLAGS ${MPI_Fortran_LINK_FLAGS}")
    message("\t MPI_Fortran_LIBRARIES ${MPI_Fortran_LIBRARIES}")
    include_directories(${MPI_Fortran_INCLUDE_PATH})
  endif()
else()
  message(WARNING "AutodetectMPI is not set. The user is responsible for "
                  "specifying the correct MPI wrapper as the compiler or "
                  "the appropriate link flags and includes!")
endif()

if(Coverage)
  message("Converage on")
  set(BUILD_COVERAGE 1)
  set(F77-FlagsOverride "${F77-FlagsOverride} -O0 -g -fprofile-arcs -ftest-coverage")
  set(F90-FlagsOverride "${F90-FlagsOverride} -O0 -g -fprofile-arcs -ftest-coverage")
  set(C-FlagsOverride "${C-FlagsOverride} -O0 -g -fprofile-arcs -ftest-coverage")
  set(CXX-FlagsOverride "${CXX-FlagsOverride} -O0 -g -fprofile-arcs -ftest-coverage")
  set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -fprofile-arcs -ftest-coverage")
else()
  set(BUILD_COVERAGE 0)
endif()

# set things up for static builds
if(BuildStatic)
  message("Building static executables")
  set(BUILD_SHARED_LIBS OFF)
endif()

#
# Enable debugging, two way to do this
#  1. Set CMAKE_BUILD_TYPE, defaults to Release if not set
#  2. Set the option Debug, this overrides setting CMAKE_BUILD_TYPE
#
if(NOT CMAKE_BUILD_TYPE)
  message(STATUS "No build type selected, default to Release")
  set(CMAKE_BUILD_TYPE "Release")
endif()

if(Debug)
  set(CMAKE_BUILD_TYPE "Debug")
endif()

#
# Check machine endianness
# 
include(TestBigEndian)
TEST_BIG_ENDIAN(isBigEndian)
if(isBigEndian)
  message(STATUS "Machine is BIG endian")
else()
  message(STATUS "Machine is little endian")
  add_definitions(-DLE)
endif()

#
# Setup compilation options
#
# Use internal variables instead of CMAKE_Fortran_FLAGS etc.
# F77-Flags, F90-Flags, C-Flags CXX-Flags
# Allow for override flags for each compiler that get specified last in the list
# F77-FlagsOverride, F90-FlagsOverride, C-FlagsOverride, CXX-FlagsOverride
#
# todo:
# need to see if autotools sets any other options that are machine dependent
#
# Can check that all these options actually work with check_fortran_compiler_flag
# Generate a list of desired options and omit the ones that don't work?
#
if(${CMAKE_VERSION} VERSION_GREATER 3.4.1)
  include(CheckFortranCompilerFlag)
endif()

if(CMAKE_Fortran_COMPILER_ID MATCHES "GNU")
  # GNU compilers
  if(BuildStatic)
    set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -static")
  endif()
  set(F77-Flags "-std=gnu")
  set(F90-Flags "-ffree-form -pipe -std=gnu -ffree-line-length-0 -fconvert=big-endian")
  set(C-Flags "")
  set(CXX-Flags "")

  if(${CMAKE_BUILD_TYPE} STREQUAL Release)
    set(F77-Flags "${F77-Flags} -O3")
    set(F90-Flags "${F90-Flags} -O3")
    set(C-Flags ${C-Flags} "-O2")
    set(CXX-Flags ${CXX-Flags} "-O2")
  elseif(${CMAKE_BUILD_TYPE} STREQUAL Debug)
    set(F77-Flags "${F77-Flags} -O0 -g")
    set(F90-Flags "${F90-Flags} -O0 -g")
    set(C-Flags "${C-Flags} -O0 -g")
    set(CXX-Flags ${CXX-Flags} "-O0 -g")

    if(${CMAKE_VERSION} VERSION_GREATER 3.4.1)
      message("Version check success")
      unset(fcheckSupport CACHE)
      CHECK_Fortran_COMPILER_FLAG(-fcheck=all fcheckSupport)
      if(fcheckSupport)
        set(F77-Flags "${F77-Flags} -fcheck=all")
        set(F90-Flags "${F90-Flags} -fcheck=all")
      else()
        set(F77-Flags "${F77-Flags} -fbounds-check -fcheck-array-temporaries")
        set(F90-Flags "${F90-Flags} -fbounds-check -fcheck-array-temporaries")
      endif()
    else()
      message("Version check fail")
      set(F77-Flags "${F77-Flags} -fbounds-check -fcheck-array-temporaries")
      set(F90-Flags "${F90-Flags} -fbounds-check -fcheck-array-temporaries")
    endif()
    

    set(F77-Flags "${F77-Flags} -ffpe-trap=invalid,zero,overflow,underflow")
    set(F90-Flags "${F90-Flags} -ffpe-trap=invalid,zero,overflow,underflow")
  endif()

  # Aggressive optimization 
  if(Opts)
    set(F77-Flags "${F77-Flags} -O3 -fomit-frame-pointer -ftree-vectorize -ffast-math -funroll-loops")
    set(F90-Flags "${F90-Flags} -O3 -fomit-frame-pointer -ftree-vectorize -ffast-math -funroll-loops")
  Endif()

  # Enable vector reports
  if(VecReport)
    set(F90-Flags "${F90-Flags} -ftree-vectorizer-verbose=6")
  endif()

  # Retain assembly code
  if(KeepAssembly)
    set(F90-Flags "${F90-Flags} -save-temps -fverbose-asm")
  endif()
elseif (CMAKE_Fortran_COMPILER_ID MATCHES "Intel")
  # Intel compilers (untested)
  set(F77-Flags "-convert big_endian")
  set(F90-Flags "-convert big_endian")
  set(C-Flags "")
  set(CXX-Flags "")

  if(${CMAKE_BUILD_TYPE} STREQUAL Release)
    set(F77-Flags "${F77-Flags} -O3")
    set(F90-Flags "${F90-Flags} -O3")
    set(C-Flags ${C-Flags} "-O2")
    set(CXX-Flags ${CXX-Flags} "-O2")
  elseif(${CMAKE_BUILD_TYPE} STREQUAL Debug)
    set(F77-Flags "${F77-Flags} -O0 -g -ftrapuv -fp-stack-check -fpe0 -traceback -C")
    set(F90-Flags "${F90-Flags} -O0 -g -ftrapuv -fp-stack-check -fpe0 -traceback -C")
    set(C-Flags "${C-Flags} -O0 -g -traceback")
    set(CXX-Flags "${CXX-Flags} -O0 -g -traceback")
  endif()

  # Agressive optimization
  if(Opts)
    set(F77-Flags "${F77-Flags} -O3 -ip -ipo -xHost")
    set(F90-Flags "${F90-Flags} -O3 -ip -ipo -xHost")
  endif()

  # Enable vector reports
  if(VecReport)
    set(F90-Flags "${F90-Flags} -vec-report=6")
  endif()

  # Retain assembly code
  if(KeepAssembly)
    message(WARNING 
"     ---------------------------------------------------------
      | Intel compiler does not allow you save assembly files |
      | and link your program.  Quietly ignoring your         |
      | request to build PlasComCM executable.                |
      |                                                       |
      | Also, the Intel compiler will output the assembly     |
      | to $FILE.o, rather than $FILE.asm or $FILE.s, because |
      | we use the -o $FILE.o option with -S.                 |
      ---------------------------------------------------------")
    set(F90-Flags "${F90-Flags} -S -fsource-asm -Fa")
  endif()
elseif(CMAKE_Fortran_COMPILER_ID MATCHES "IBM")
  # GNU compilers
  if(BuildStatic)
    set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -Bstatic")
  endif()
  #set(F77-Flags "-std=gnu")
  #set(F90-Flags "-ffree-form -pipe -std=gnu -ffree-line-length-0 -fconvert=big-endian")
  set(F77-Flags "")
  set(F90-Flags "")
  set(C-Flags "")
  set(CXX-Flags "")

  if(${CMAKE_BUILD_TYPE} STREQUAL Release)
    set(F77-Flags "${F77-Flags} -O3")
    set(F90-Flags "${F90-Flags} -O3")
    set(C-Flags ${C-Flags} "-O2")
    set(CXX-Flags ${CXX-Flags} "-O2")
  elseif(${CMAKE_BUILD_TYPE} STREQUAL Debug)
    set(F77-Flags "${F77-Flags} -O0 -g")
    set(F90-Flags "${F90-Flags} -O0 -g")
    set(C-Flags "${C-Flags} -O0 -g")
    set(CXX-Flags ${CXX-Flags} "-O0 -g")

    #if(${CMAKE_VERSION} VERSION_GREATER 3.4.1)
      #message("Version check success")
      #unset(fcheckSupport CACHE)
      #CHECK_Fortran_COMPILER_FLAG(-fcheck=all fcheckSupport)
      #if(fcheckSupport)
        #set(F77-Flags "${F77-Flags} -fcheck=all")
        #set(F90-Flags "${F90-Flags} -fcheck=all")
      #else()
        #set(F77-Flags "${F77-Flags} -fbounds-check -fcheck-array-temporaries")
        #set(F90-Flags "${F90-Flags} -fbounds-check -fcheck-array-temporaries")
      #endif()
    #else()
      #message("Version check fail")
      #set(F77-Flags "${F77-Flags} -fbounds-check -fcheck-array-temporaries")
      #set(F90-Flags "${F90-Flags} -fbounds-check -fcheck-array-temporaries")
    #endif()
    #

    set(F77-Flags "${F77-Flags} -ffpe-trap=invalid,zero,overflow,underflow")
    set(F90-Flags "${F90-Flags} -ffpe-trap=invalid,zero,overflow,underflow")
  endif()

  # Aggressive optimization 
  if(Opts)
    set(F77-Flags "${F77-Flags} -O3 -fomit-frame-pointer -ftree-vectorize -ffast-math -funroll-loops")
    set(F90-Flags "${F90-Flags} -O3 -fomit-frame-pointer -ftree-vectorize -ffast-math -funroll-loops")
  Endif()

  # Enable vector reports
  if(VecReport)
    set(F90-Flags "${F90-Flags} -ftree-vectorizer-verbose=6")
  endif()

  # Retain assembly code
  if(KeepAssembly)
    set(F90-Flags "${F90-Flags} -save-temps -fverbose-asm")
  endif()
else ()
  message ("CMAKE_Fortran_COMPILER full path: " ${CMAKE_Fortran_COMPILER})
  message ("Fortran compiler: " ${Fortran_COMPILER_NAME})
  message ("No optimized Fortran compiler flags are known, just try -O2...")
  if(${CMAKE_BUILD_TYPE} STREQUAL Release)
    set(F77-Flags "${F77-Flags} -O2")
    set(F90-Flags "${F90-Flags} -O2")
    set(C-Flags ${C-Flags} "-O2")
    set(CXX-Flags ${C-Flags} "-O2")
  elseif(${CMAKE_BUILD_TYPE} STREQUAL Debug)
    set(F77-Flags "${F77-Flags} -O0 -g")
    set(F90-Flags "${F90-Flags} -O0 -g")
    set(C-Flags ${C-Flags} "-O0 -g")
    set(CXX-Flags ${C-Flags} "-O0 -g")
  endif()

   # I don't know what to do for the other options? Warn the user.
  if (VecReport)
    message(WARNING "Unsure how to generate vector reports for this compiler")
  endif()
  if (KeepAssembly)
    message(WARNING "Unsure how to retain assembly code for this compiler")
  endif()
  if(Opts)
    message(WARNING "Unsure how to aggresively optimize code for this compiler")
  endif()
endif()

# include any flags set by the user
# we post-pend here to enforce any changes to the optimization level

if(NOT ${F77-FlagsOverride} STREQUAL "")
  set(F77-Flags "${F77-Flags} ${F77-FlagsOverride}")
endif()
if(NOT ${F90-FlagsOverride} STREQUAL "")
  set(F90-Flags "${F90-Flags} ${F90-FlagsOverride}")
endif()
if(NOT ${C-FlagsOverride} STREQUAL "")
  set(C-Flags "${C-Flags} ${C-FlagsOverride}")
endif()
if(NOT ${CXX-FlagsOverride} STREQUAL "")
  set(CXX-Flags "${CXX-Flags} ${CXX-FlagsOverride}")
endif()

message(STATUS "Compile flags for F77 sources will be \n ${F77-Flags}")
message(STATUS "Compile flags for F90 sources will be \n ${F90-Flags}")
message(STATUS "Compile flags for C sources will be \n ${C-Flags}")
message(STATUS "Compile flags for CXX sources will be \n ${CXX-Flags}")

#
# Enable experimental optimized boundary condition routines
#
if(OptBC)
  add_definitions(-DUSE_OPT_BC)
  set(FPP-Flags ${FPP-Flags} -DUSE_OPT_BC)
endif() 

#
# Enable developer placed SIMD directives
#
if(OptSIMD)
  add_definitions(-DENABLE_SIMD)
  set(FPP-Flags ${FPP-Flags} -DENABLE_SIMD)
endif() 

#
# Add support for axisymmetric mode
#
if(Axisymmetric)
  add_definitions(-DAXISYMMETRIC)
  set(FPP-Flags ${FPP-Flags} -DAXISYMMETRIC)
endif()

#
# Add support for BoxMG
#
if(BoxMG)
  find_package(BoxMG)
endif()

#
# Add support for testing
#
if(Tests)
  enable_testing()
  add_definitions(-DBUILD_TESTS)
  find_package(pFUnit)
  if(PFUNIT_FOUND)
    message(STATUS "Found pFUnit. Enabling build support for unit testing")
    message(STATUS "PFUNIT_LIBRARY ${PFUNIT_LIBRARY}")
    message(STATUS "PFUNIT_INCLUDE_DIR ${PFUNIT_INCLUDE_DIR}")
    message(STATUS "PFUNIT_DRIVER ${PFUNIT_DRIVER}")
    message(STATUS "PFUNIT_MODULE_DIR ${PFUNIT_MODULE_DIR}")
    message(STATUS "PFUNIT_PARSER ${PFUNIT_PARSER}")

    set(FPP-Flags ${FPP-Flags} -DBUILD_TESTS)
    #set(plascomIncludes ${plascomIncludes} ${PFUNIT_INCLUDE_DIR} ${PFUNIT_MODULE_DIR})
    include_directories(${PFUNIT_INCLUDE_DIR} ${PFUNIT_MODULE_DIR})
  else()
    message(WARNING "Could not find pFUnit. Disabling unit testing support.")
  endif()
endif()

#
# Find option packages for building PlasComCM
#
set(plascomExtraLibs "")
set(plascomIncludes "")

if(Coverage)
  set(plascomExtraLibs gcov)
endif()

#
# Look for HDF5 and add support is approprate libraries are found
#
message("Looking for parallel HDF5 support.")
if(BuildStatic OR APPLE)
  set(HDF5_USE_STATIC_LIBRARIES TRUE)
endif()
find_package(HDF5 COMPONENTS C CXX Fortran)                                                          
if(HDF5_FOUND)  
  message(STATUS "HDF5_IS_PARALLEL       = ${HDF5_IS_PARALLEL}")
  message(STATUS "HDF5_LIBRARY_DIRS      = ${HDF5_LIBRARY_DIRS}")
  message(STATUS "HDF5_INCLUDE_DIRS      = ${HDF5_INCLUDE_DIRS}")
  message(STATUS "HDF5_INCLUDE_DIR       = ${HDF5_INCLUDE_DIR}")
  message(STATUS "HDF5_DEFINITIONS       = ${HDF5_DEFINITIONS}")
  message(STATUS "HDF5_LIBRARIES         = ${HDF5_LIBRARIES}")                              
  message(STATUS "HDF5_C_LIBRARIES       = ${HDF5_C_LIBRARIES}")                         
  message(STATUS "HDF5_CXX_LIBRARIES     = ${HDF5_CXX_LIBRARIES}")                            
  message(STATUS "HDF5_Fortran_LIBRARIES = ${HDF5_Fortran_LIBRARIES}")   
  message(STATUS "HDF5_C_COMPILER_EXECUTABLE = ${HDF5_C_COMPILER_EXECUTABLE}")   
  message(STATUS "HDF5_CXX_COMPILER_EXECUTABLE = ${HDF5_CXX_COMPILER_EXECUTABLE}")   
  message(STATUS "HDF5_Fortran_COMPILER_EXECUTABLE = ${HDF5_Fortran_COMPILER_EXECUTABLE}")
  if(HDF5_IS_PARALLEL) 
    message(STATUS "Enabling HDF5.")
    set(useHDF TRUE)
  else()
    message(STATUS "Disabling HDF5 (not parallel).")
    set(useHDF FALSE)
  endif(HDF5_IS_PARALLEL)
endif() 

if(useHDF)
  message(STATUS "Setting up build for HDF5.")
  #set(plascomIncludes ${plascomIncludes} ${HDF5_INCLUDE_DIRS})
  include_directories(${HDF5_INCLUDE_DIRS})                                                    
  set(plascomExtraLibs ${plascomExtraLibs} ${HDF5_Fortran_LIBRARIES})
  add_definitions(-DHAVE_HDF5)
  set(FPP-Flags ${FPP-Flags} -DHAVE_HDF5)
else()
  message(WARNING "Not using HDF5. Your I/O may be slow and problem size may be limited.")
endif()   
#
# Add support for PETSc for the EF-Solver and TM-Solver
# 2 options:
#   1. Put the path to PETSc in CMAKE_PREFIX_PATH
#   2. Define PETSC_DIR and PETSC_ARCH in the environment
#
if((TM-Solver OR EF-Solver))
  message("Looking for PETSc.")
  find_package(PETSc)
  if(PETSC_FOUND)
    message(STATUS "Found PETSC ${PETSC_VERSION}")
    message(STATUS "PETSC_INCLUDE_DIR ${PETSC_INCLUDE_DIR}")
    message(STATUS "PETSC_LIBRARIES ${PETSC_LIBRARIES}")
    if (NOT ${PETSC_VERSION} MATCHES "3.6.4")
      message(WARNING "Version 3.6.4 is required, disabling PETSc support")
      unset(PETSC_FOUND)
    endif()
  endif()

  if(PETSC_FOUND)
    add_definitions(-DHAVE_PETSC -DPETSC_NAMESPACED_HEADERS)
    #
    # this is a change for petsc 3.6 and newer, includes moved from finclude to petsc/finclude
    # really don't need this since we require 3.6.4, but whatever
    #
    set(FPP-Flags ${FPP-Flags} -DHAVE_PETSC -DPETSC_NAMESPACED_HEADERS)
    message(STATUS "Setting up build to use PETSc.")
    include_directories(${PETSC_INCLUDE_DIR})
    #set(plascomIncludes ${plascomIncludes} ${PETSC_INCLUDE_DIRS})
    set(plascomExtraLibs ${plascomExtraLibs} ${PETSC_LIBRARIES})
  else()
    message(STATUS "PETSc not found.")
    if(EF-Solver)
      message(FATAL_ERROR "PETSc required to build PlasComCM with the EF-Solver")
    elseif(TM-Solver)
      message(FATAL_ERROR "PETSc required to build PlasComCM with the TM-Solver")
    endif()
  endif()
endif()

#
# Add suport for Cantera for combustion
# 2 options:
#   1. Put the path to Cantera in CMAKE_PREFIX_PATH
#   2. Define CANTERA_DIR in the environment
#
if(Combustion)
  message("Looking for Cantera")
  find_package(Cantera)
  if(CANTERA_FOUND)
    message(STATUS "Found Cantera Version ${CANTERA_VERSION}")
    message(STATUS "CANTERA_INCLUDE_DIR ${CANTERA_INCLUDE_DIRS}")
    message(STATUS "CANTERA_CORE_LIBRARY ${CANTERA_CORE_LIBRARY}")
    message(STATUS "CANTERA_EXTRA_LIBRARIES ${CANTERA_EXTRA_LIBRARIES}")
    message(STATUS "CANTERA_Fortran_LIBRARY ${CANTERA_Fortran_LIBRARY}")

    add_definitions(-DHAVE_CANTERA)
    set(FPP-Flags ${FPP-Flags} -DHAVE_CANTERA)
    message(STATUS "Setting up build to use Cantera.")
    include_directories(${CANTERA_INCLUDE_DIRS})
    set(plascomExtraLibs ${plascomExtraLibs}
        ${CANTERA_Fortran_LIBRARY}
        ${CANTERA_CORE_LIBRARY} 
        ${CANTERA_EXTRA_LIBRARIES}
        )
  else()
    message(STATUS "Cantera not found.")
    if(Combustion)
      message(FATAL_ERROR "Cantera required for builds with combustion enabled.")
    endif() 
  endif()
endif()

#
# setup a place for the module files to reside
#
set(moduleDirectory ${CMAKE_CURRENT_BINARY_DIR}/src/modules)
file(MAKE_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/src)
file(MAKE_DIRECTORY ${moduleDirectory})

# 
# Make lists of sources to be compiled for PlasComCM
# Files that need to be processed by a custom preprocessor are added to plascomPreProc
# Currently we process FPP files to F90.
#
# todo: Add support for other languages
#
# PlasComCM common files
#
set(plascomCommonFPP ModMain.fpp ModGlobal.fpp ModDataStruct.fpp ModMPI.fpp ModString.fpp ModParam.fpp 
    ModMatrixVectorOps.fpp ModSpline.fpp ModSBI.fpp ModPSAAP2.fpp 
    ModPLOT3D_IO.fpp ModHDF5_IO.fpp ModIO.fpp ModIOUtil.fpp
    ModPSAAP2_cp.fpp ModInitialCondition.fpp ModGeometry.fpp ModEOS.fpp ModDataUtils.fpp ModPenta.fpp 
    ModDeriv.fpp ModDerivBuildOps.fpp ModMetrics.fpp ModFVSetup.fpp ModFiniteVolume.fpp 
    ModLighthill.fpp ModRiemannSolver.fpp ModNASARotor.fpp ModEulerFV.fpp ModNavierStokesBC.fpp 
    ModNavierStokesRHS.fpp ModCombustion.fpp ModOneStep.fpp ModModel0.fpp ModModel1.fpp
    ModNavierStokesImplicitSATArtDiss.fpp ModNavierStokesImplicit.fpp 
    ModLinNavierStokesRHS.fpp ModLinNavierStokesBC.fpp ModAdjointNS.fpp ModCyl1D.fpp ModQ1D.fpp 
    ModIBLANK.fpp ModInterp.fpp ModTimemarch.fpp ModOptimization.fpp ModRungeKutta.fpp 
    ModActuator.fpp ModInput.fpp ModRegion.fpp ModPostProcess.fpp ModStat.fpp 
    ModPlasComCM.fpp )
set(plascomPreProc ${plascomCommonFPP})
set(plascomF77 "")
set(plascomF90 "")
set(plascomINC "")
include_directories(${CMAKE_SOURCE_DIR}/src)

#
# Sourcesfor Thermo-Mechanical solver
#
if(TM-Solver)
  message("Adding TM-Solver capability")
  set(plascomTM-FPP ModTMIO.fpp ModTMInt.fpp ModElasticity.fpp ModTMEOM.fpp ModFTMCoupling.fpp 
      ModTMEOMBC.fpp ModPETSc.fpp ModTMSingleStep.fpp ModInitTM.fpp)
  set(plascomPreProc ${plascomPreProc} ${plascomTM-FPP})
  message(FATAL_ERROR "TM-Solver is currently broken due to out-of-date PETSc code. Fix it.")
endif()

# Sources for Combustion
if(Combustion)
  message("Adding Combustion capability")
  add_definitions(-DBUILD_COMBUSTION)
  set(FPP-Flags ${FPP-Flags} -DBUILD_COMBUSTION)
  set(plascomCombFPP ModXMLCantera.fpp ModLaserIgnition.fpp)
  set(plascomCombINC src/read_from_buffer.inc src/read_xml_array.inc src/read_xml_scalar.inc) 
  set(plascomPreProc ${plascomPreProc} ${plascomCombFPP})
  set(plascomF90 ${plascomF90} ${plascomCombINC})
endif()

# Sources for E-Field Solver
if(EF-Solver)
  message("Adding E-Field solver capability") 
  add_definitions(-DBUILD_ELECTRIC)
  set(FPP-Flags ${FPP-Flags} -DBUILD_ELECTRIC)
  file(GLOB plascomEF-H src/ef*.h)
  file(GLOB plascomEF-C src/ef*.c)
  set(plascomEF-FPP ModElectric.fpp ModElectricBC.fpp ModElectricState.fpp 
      ModElectricInterface.fpp ModElectricSchwarz.fpp)
  set(plascomPreProc ${plascomPreProc} ${plascomEF-FPP})
  set(plascomC ${plascomC} ${plascomEF-H} ${plascomEF-C})
endif()
#
#  Setup preprocessing
#
include(ExternalPreprocessor)
get_directory_property(includeDirectories INCLUDE_DIRECTORIES)
set(includeDirectories ${includeDirectories} ${plascomIncludes})
#message("includeDirectories ${includeDirectories}")
set(includeFlags)
foreach(inc ${includeDirectories})
  set(includeFlags ${includeFlags} -I${inc})
endforeach()
#message("includeFlags ${includeFlags}")

# can't do this...there is a flag -DLE that gets set for little endian for plot3d
# when we preprocess with that flag, all the .LE. get replaced...oops!
# instead store the preprocess defines in FPP flags
#get_directory_property(defines COMPILE_DEFINITIONS)
#set(defineFlags)
#foreach(def ${defines})
  #set(defineFlags ${defineFlags} -D${def})
#endforeach()
set(defineFlags ${FPP-Flags})

if(PreProcTool)
  set(FPP-PreProc ${PreProcTool})
  set(FPP-PreProcFlags ${PreProcToolFlags} ${includeFlags} ${defineFlags})
else()
  if (CMAKE_Fortran_COMPILER_ID MATCHES "Intel")
    set(FPP-PreProc ${CMAKE_Fortran_COMPILER})
    set(FPP-PreProcFlags -EP -FR ${includeFlags} ${defineFlags})
  else ()
    set(FPP-PreProc ${CMAKE_C_COMPILER})
    set(FPP-PreProcFlags -E -P ${includeFlags} ${defineFlags})
  endif()
endif()
#SetupPreProcessor()
#
# Add subdirectories for additional libraries
#
add_subdirectory(amos)
add_subdirectory(utils)


if(Examples)
  if( NOT OversetTool)
    message(FATAL_ERROR "The Overset Tools are required for building the examples."
                        "\n Run CMake with -DOversetTool=ON to enable.")
  endif()
  add_subdirectory(examples)
endif()
if(Tests)
  add_subdirectory(tests)
endif()

#
# Preprocess the PlasCom FPP sources
#
foreach(fileIn ${plascomPreProc})
  PreProcess(${CMAKE_CURRENT_SOURCE_DIR}/src/${fileIn} 
             ${CMAKE_CURRENT_BINARY_DIR}/src fileOut)
  list(APPEND plascomF90 ${fileOut})
endforeach()

set_source_files_properties(${plascomF90} PROPERTIES COMPILE_FLAGS
  "${F90-Flags} ${MPI_Fortran_COMPILE_FLAGS}")

set(plascomC ${plascomC} src/plot3d_format.c src/mesh_io.c src/readbuffer.c)
if(useHDF)
  set(plascomC ${plascomC} src/hdf5_format.c)
endif()

set_source_files_properties(${plascomC} PROPERTIES COMPILE_FLAGS 
  "${C-Flags} ${MPI_C_COMPILE_FLAGS}")

#
# this gathers all the executables and libraries into a centralized location
# in the build directory. But this does not work with the current testing 
# scripts. Comment out for now and revisit later
#
# Note that I also set RUNTIME_OUTPUT_DIRECTORY for plascomcm so it gets stuck
# in the src directory in the build tree. This mimics autotools behavior
#
#set(EXECUTABLE_OUTPUT_PATH ${PROJECT_BINARY_DIR}/bin 
#     CACHE PATH "Single directory for all executables.")
#set(LIBRARY_OUTPUT_PATH ${PROJECT_BINARY_DIR}/lib 
#     CACHE PATH "Single directory for all libraries and archives.")
#mark_as_advanced(LIBRARY_OUTPUT_PATH EXECUTABLE_OUTPUT_PATH)


add_executable(plascomcm ${plascomF90} ${plascomC})
if(BuildStatic)
  set_target_properties(plascomcm PROPERTIES LINK_FLAGS "-Wl,-Bdynamic")
endif()

if(AutodetectMPI)
  set_target_properties(plascomcm PROPERTIES LINK_FLAGS "${MPI_Fortran_LINK_FLAGS}"
                              Fortran_MODULE_DIRECTORY ${moduleDirectory}
                              RUNTIME_OUTPUT_DIRECTORY src
                              LINKER_LANGUAGE Fortran) 
  target_link_libraries(plascomcm amos plot3d ${plascomExtraLibs} ${MPI_Fortran_LIBRARIES})
else()
  set_target_properties(plascomcm PROPERTIES Fortran_MODULE_DIRECTORY ${moduleDirectory}
                              RUNTIME_OUTPUT_DIRECTORY src
                              LINKER_LANGUAGE Fortran
                       ) 
  target_link_libraries(plascomcm amos plot3d ${plascomExtraLibs}) 
endif()


target_include_directories(plascomcm PUBLIC ${moduleDirectory} ${plascomIncludes})
#message("plascomExtraLibs ${plascomExtraLibs}")
#message("MPI_Fortran_LIBRARIES ${MPI_Fortran_LIBRARIES}")
#message("MPI_LIBRARIES ${MPI_LIBRARIES}")

#if(APPLE)
  #set (CMAKE_SHARED_LINKER_FLAGS "{CMAKE_SHARE_LINKER_FLAGS} -undefined dyanmic-lookup")
#endif(APPLE)

install(TARGETS plascomcm RUNTIME DESTINATION bin LIBRARY DESTINATION lib ARCHIVE DESTINATION lib)
# 
# Add testing support from IRAD
#
include(CTest)
if(BUILD_TESTING)
  set(BUILDNAME "${BUILDNAME}" CACHE STRING "Name of build on the dashboard")
  mark_as_advanced(BUILDNAME)
endif()
#
#  add custom targets for bulding Doxygen documentation
#
if(BuildDocumentation)
  add_subdirectory(Docs)
endif()

include(CPack)

