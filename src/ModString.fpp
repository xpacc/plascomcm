! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
module ModString

contains

  logical function string_compare(str1, str2)

    implicit none

    character (len=*) :: str1
    character (len=*) :: str2

    !write(*,*) 'string_compare("',trim(str1),'","',trim(str2),'").'

    string_compare = ( index(trim(str1), trim(str2)) == 1 .AND. &
         index(trim(str2), trim(str1)) == 1 )

  end function string_compare

  logical function string_starts_with(str1, str2)

    implicit none

    character (len=*) :: str1
    character (len=*) :: str2

    string_starts_with = ( index(trim(str1), trim(str2)) == 1 )

  end function string_starts_with

  logical function compare_tokens_to_string(tokens, str)

    implicit none

    character (len=*) :: tokens
    character (len=*) :: str

    integer :: len_trim_tokens,istart,iend

    len_trim_tokens = len_trim(tokens)

    !write(*,*) 'compare_tokens_to_string("',trim(tokens),'","',trim(str),'").'

    iend = 1
    do

      do istart = iend,len_trim_tokens
        if (index(':',tokens(istart:istart)) == 0) &
             exit
      end do

      do iend = istart,len_trim_tokens
        if (index(':',tokens(iend:iend)) /= 0) &
             exit
      end do

      if (istart < iend) then
        if (string_compare(tokens(istart:iend-1),str)) then
          compare_tokens_to_string = .TRUE.
          return
        end if
      else
        exit
      end if

    end do

    compare_tokens_to_string = .FALSE.

  end function compare_tokens_to_string

  logical function compare_tokens_to_tokens_old(tokens1, tokens2, delimiters)

    implicit none

    character (len=*) :: tokens1
    character (len=*) :: tokens2
    character (len=*) :: delimiters

    integer :: len_trim_tokens1,istart1,iend1
    integer :: len_trim_tokens2,istart2,iend2

    len_trim_tokens1 = len_trim(tokens1)
    len_trim_tokens2 = len_trim(tokens2)

    !write(*,*) 'compare_tokens_to_tokens("',trim(tokens1),'","',trim(tokens2),'").'

    iend1 = 1
    do

      do istart1 = iend1,len_trim_tokens1
        if (index(trim(delimiters),tokens1(istart1:istart1)) == 0) &
             exit
      end do

      do iend1 = istart1,len_trim_tokens1
        if (index(trim(delimiters),tokens1(iend1:iend1)) /= 0) &
             exit
      end do

      if (istart1 < iend1) then

        iend2 = 1
        do

          do istart2 = iend2,len_trim_tokens2
            if (index(trim(delimiters),tokens2(istart2:istart2)) == 0) &
                 exit
          end do

          do iend2 = istart2,len_trim_tokens2
            if (index(trim(delimiters),tokens2(iend2:iend2)) /= 0) &
                 exit
          end do

          if (istart2 < iend2) then		

            if (string_compare(tokens1(istart1:iend1-1),tokens2(istart2:iend2-1))) then
              compare_tokens_to_tokens_old = .TRUE.
              return
            end if

          else

            exit

          end if

        end do

      else

        exit

      end if

    end do

    compare_tokens_to_tokens_old = .FALSE.

  end function compare_tokens_to_tokens_old

  logical function compare_tokens_to_tokens(tokens1, tokens2)

    implicit none

    character (len=*) :: tokens1
    character (len=*) :: tokens2

    integer :: len_trim_tokens1,istart1,iend1
    integer :: len_trim_tokens2,istart2,iend2

    len_trim_tokens1 = len_trim(tokens1)
    len_trim_tokens2 = len_trim(tokens2)

    !write(*,*) 'compare_tokens_to_tokens("',trim(tokens1),'","',trim(tokens2),'").'

    iend1 = 1
    do

      do istart1 = iend1,len_trim_tokens1
        if (index(':',tokens1(istart1:istart1)) == 0) &
             exit
      end do

      do iend1 = istart1,len_trim_tokens1
        if (index(':',tokens1(iend1:iend1)) /= 0) &
             exit
      end do

      if (istart1 < iend1) then

        iend2 = 1
        do

          do istart2 = iend2,len_trim_tokens2
            if (index(':',tokens2(istart2:istart2)) == 0) &
                 exit
          end do

          do iend2 = istart2,len_trim_tokens2
            if (index(':',tokens2(iend2:iend2)) /= 0) &
                 exit
          end do

          if (istart2 < iend2) then		

            if (string_compare(tokens1(istart1:iend1-1),tokens2(istart2:iend2-1))) then
              compare_tokens_to_tokens = .TRUE.
              return
            end if

          else

            exit

          end if

        end do

      else

        exit

      end if

    end do

    compare_tokens_to_tokens = .FALSE.

  end function compare_tokens_to_tokens

  subroutine convert_to_uppercase(str)

    implicit none

    character (len=*) :: str

    integer :: i,iachar_a,iachar_z,delta,this_iachar

    iachar_a = IACHAR('a')
    iachar_z = IACHAR('z')
    delta = IACHAR('A') - iachar_a

    do i = 1,len_trim(str)
      this_iachar = IACHAR(str(i:i))
      if ((iachar_a <= this_iachar).AND.(this_iachar <= iachar_z)) &
           str(i:i) = ACHAR(this_iachar + delta)
    end do

  end subroutine convert_to_uppercase

  subroutine bisect_string(string, token_count, before_string, after_string)

    implicit none

    character(len=*), intent(in) :: string
    integer, intent(in) :: token_count
    character(len=*), intent(out) :: before_string, after_string

    integer :: i, j
    integer :: N

    N = len_trim(string)

    i = 1

    do while (string(i:i) == ' ' .and. i < N)
      i = i + 1
    end do

    j = 0
    do while (j < token_count .and. i < N)
      if (string(i:i) == ' ') then
        j = j + 1
        do while (string(i:i) == ' ' .and. i < N)
          i = i + 1
        end do
      else
        i = i + 1
      end if
    end do

    before_string = string(:i-1)
    after_string = string(i:)

  end subroutine bisect_string

end module ModString
