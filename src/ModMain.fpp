! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
!-----------------------------------------------------------------------
!
! ModMain.f90
!
! - overall flow of the application
!
! Revision history
! - 20 Dec 2006 : DJB : initial code
! - 13 Jan 2007 : DJB : evolution (added region)
! - 01 May 2007 : DJB : evolution (added restart)
!
!-----------------------------------------------------------------------
#ifdef USE_AMPI
Subroutine MPI_Main
#else
Program PlasComCM
#endif

  USE ModDataStruct
  USE ModMPI
  USE ModPlasComCM

  implicit none

! ... local variables
  TYPE(t_region), pointer :: region
  integer :: ierr
  Character(LEN=PATH_LENGTH) :: input_fname, restart_fname, restart_aux_fname
  REAL(RFREAL) :: time0,time1
  call MPI_Init(ierr)

  ! Global variable 'debug_on' is now final, must not be modified
  debug_on = .true.

  ! ... the region holds everything
  allocate(region)

  call PlasComCM_Init_MPI(region, MPI_COMM_WORLD)

  time0 = MPI_Wtime()
  call PlasComCM_Init_Timings(region)
  call PlasComCM_Read_Cmd_Line(region, input_fname, restart_fname, restart_aux_fname)
  call PlasComCM_Init_Input(region, input_fname, restart_fname, restart_aux_fname)

  call MPI_Barrier(mycomm,ierr)
  region%init_grids_time = MPI_Wtime()
  call PlasComCM_Init_Grids(region)
  call MPI_Barrier(mycomm,ierr)
  region%init_state_time = MPI_Wtime()
  region%init_grids_time = region%init_state_time - region%init_grids_time
  call PlasComCM_Init_State(region)
  call MPI_Barrier(mycomm,ierr)
  region%init_state_time = MPI_Wtime() - region%init_state_time
  call PlasComCM_Init_Adjoint(region)
  call PlasComCM_Init_Probe(region)
  call PlasComCM_Init_LST(region)
  call PlasComCM_Init_TM(region)
  call PlasComCM_Init_NASA_Rotor_Grid(region)
  call PlasComCM_Init_LES(region)
  call PlasComCM_Init_Stat(region)
  call PlasComCM_Output_Decomp_Map(region)
  time1 = MPI_Wtime()
  region%init_time = time1 - time0
  call PlasComCM_Run(region)
  region%run_time = MPI_Wtime() - time1
  call PlasComCM_Finalize_TM(region)
  call PlasComCM_Finalize_Timings(region)

  if(region%myrank == 0) then
    write (*,'(A)') ''
    write (*,'(A)') 'PlasComCM: Thank you for using PlasComCM.  Have a nice day.'
    write (*,'(A)') ''
  end if

  call MPI_Finalize(ierr)

#ifdef USE_AMPI
End Subroutine MPI_Main
#else
End Program PlasComCM
#endif
