! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
!-----------------------------------------------------------------------
! ModDeriv.f90
!  
! - basic code for derivatives
!
! Revision history
! - 18 Dec 2006 : DJB : initial code
! - 29 Dec 2006 : DJB : evolution
! - 10 Jan 2007 : DJB : more evolution---Chuck's Pentadiagonal solvers
! - 29 Oct 2008 : DJB : conversion to operators
! - 20 May 2009 : DJB : low-memory version
!
! $Header: /cvsroot/genx/Codes/RocfloCM/Source/ModDeriv.f90,v 1.43 2011/09/26 21:12:13 bodony Exp $
!
!-----------------------------------------------------------------------

module ModDeriv

contains

  !-----------------------------------------------------------------------

  subroutine FIRST_DERIV(region, gridID, alpha, F, dF, add, grid_diff)

    use ModGlobal
    use ModDataStruct

    implicit none

    type(t_grid), pointer :: grid
    type(t_region), pointer :: region
    integer :: dir, gridID, order, alpha
    real(rfreal), pointer :: F(:), dF(:)
    logical :: grid_diff
    real(rfreal) :: add

    ! ... local variables
    integer :: i, j
    real(rfreal), pointer :: dF_aux(:)

    grid => region%grid(gridID)

    Do i = 1, grid%nCells
      dF(i) = 0.0_rfreal
    End Do ! i

    nullify(dF_aux)
    allocate(dF_aux(size(F)))

    if (grid%metric_type /= CARTESIAN) then
      do j = 1, grid%ND

        ! ... compute dF/d(xi_j)
        call APPLY_OPERATOR(region, gridID, 1, j, F, dF_aux, grid_diff)
        Do i = 1, grid%nCells
          dF(i) = dF(i) + add * grid%MT1(i,region%global%t2Map(j,alpha)) * dF_aux(i)
        End Do

      end do

    else

      call APPLY_OPERATOR(region, gridID, 1, alpha, F, dF_aux, grid_diff)
      Do i = 1, grid%nCells
        dF(i) = dF(i) + add * grid%MT1(i,region%global%t2Map(alpha,alpha)) * dF_aux(i)
      End Do

    end if

    Do i = 1, grid%nCells
      dF(i) = dF(i) * grid%JAC(i)
    End Do

    deallocate(dF_aux);

  end subroutine FIRST_DERIV

  subroutine SECOND_DERIV(region, gridID, alpha, beta, F, dF, add, grid_diff)

    use ModGlobal
    use ModDataStruct
    use ModMPI

    implicit none

    type(t_grid), pointer :: grid
    type(t_region), pointer :: region
    integer :: dir, gridID, order, alpha, beta
    real(rfreal), pointer :: F(:), dF(:)
    logical :: grid_diff
    real(rfreal) :: add

    ! ... local variables
    integer :: i, j
    real(rfreal), pointer :: dF_aux(:), dF_aux2(:)

    grid => region%grid(gridID)

    Do i = 1, grid%nCells
      dF(i) = 0.0_rfreal
    End Do ! i

    nullify(dF_aux); allocate(dF_aux(size(F)))

    if (grid%metric_type /= CARTESIAN .OR. (alpha /= beta)) then

      nullify(dF_aux2); allocate(dF_aux2(size(F)))

      ! ... initialize
      Do i = 1, grid%nCells
        dF_aux(i)  = 0.0_rfreal
        dF_aux2(i) = 0.0_rfreal
      End Do

      ! ... differentiate in direction alpha
      call FIRST_DERIV(region, gridID, alpha, F, dF_aux, add, grid_diff)

      ! ... differentiate in direction beta
      call FIRST_DERIV(region, gridID, beta, dF_aux, dF_aux2, add, grid_diff)

      Do i = 1, grid%nCells
        dF(i) = dF(i) + add * dF_aux2(i)
      End Do

      deallocate(dF_aux2)

    else

      if (grid%metric_type /= CARTESIAN) &
        call graceful_exit(region%myrank, 'ERROR: NONORTHONGAL grid with alpha == beta made it to SECOND_DERIV()')

      ! ... d2f/dx_i/dx_i = 1/(d2x_i/dxi_i/dxi_i) df/dxi_i + (dxi_i/dx_i)**2 d2f/(dxi_i dxi_i)
      call APPLY_OPERATOR(region, gridID, 1, alpha, F, dF_aux, grid_diff)
      Do i = 1, grid%nCells
        dF(i) = dF(i) + add * grid%cartMetric(i,alpha) * dF_aux(i)
        dF_aux(i) = 0.0_rfreal;
      End Do

      call APPLY_OPERATOR(region, gridID, 2, alpha, F, dF_aux, grid_diff)
      Do i = 1, grid%nCells
        dF(i) = dF(i) + add * dF_aux(i) * (grid%MT1(i,region%global%t2Map(alpha,alpha)) * grid%JAC(i))**2
      End Do

    end if

    deallocate(dF_aux);

  end subroutine SECOND_DERIV

  !-----------------------------------------------------------------------

  subroutine FIRST_AND_SECOND_DERIV(region, gridID, alpha, beta, F, dF, d2F, add, grid_diff)

    use ModGlobal
    use ModDataStruct
    use ModMPI

    implicit none

    type(t_grid), pointer :: grid
    type(t_region), pointer :: region
    integer :: dir, gridID, order, alpha, beta
    real(rfreal), pointer :: F(:), dF(:), d2F(:)
    logical :: grid_diff
    real(rfreal) :: add
    integer :: Nc

    ! ... local variables
    integer :: i, j
    real(rfreal), pointer :: dF_aux(:), dF_aux2(:)
    real(rfreal) :: dfac

    If (alpha /= beta) Then
      Call Graceful_Exit(region%myrank, 'ERROR: PlasComCM: Do not use FIRST_AND_SECOND_DERIV with alpha /= beta.')
    End If

    grid => region%grid(gridID)
    Nc = grid%nCells

    Do i = 1, Nc
       dF(i) = 0.0_rfreal
      d2F(i) = 0.0_rfreal
    End Do

    nullify(dF_aux); allocate(dF_aux(Nc))

    ! ... d2f/dx2 = (d2xi_i/dx_i2) df/dxi_i + (dxi_i/dx_i)**2 * d2f/dxi_i2
    call APPLY_OPERATOR(region, gridID, 1, alpha, F, dF_aux, grid_diff)
    Do i = 1, grid%nCells
      d2F(i) = d2F(i) + add * grid%cartMetric(i,alpha) * dF_aux(i)
       dF(i) =  dF(i) + add * dF_aux(i) * grid%JAC(i)
    End Do

    call APPLY_OPERATOR(region, gridID, 2, alpha, F, dF_aux, grid_diff)
    Do i = 1, grid%nCells
      dfac = grid%MT1(i,region%global%t2Map(alpha,alpha)) * grid%JAC(i)
      d2F(i) = d2F(i) + add * dF_aux(i) * dfac * dfac
    End Do

    deallocate(dF_aux);

  end subroutine FIRST_AND_SECOND_DERIV

  !-----------------------------------------------------------------------

  Subroutine Plane_Periodic_Metric_Hack(dir, grid, F)

    USE ModGlobal
    USE ModDataStruct
    USE ModMatrixVectorOps
    USE ModMPI

    implicit none

    integer :: dir
    type(t_grid), pointer :: grid
    real(rfreal), pointer :: F(:)

    ! ... local variables
    integer :: i, j, N, l0
    real(rfreal) :: f1, fN, L
    type(t_matrix_sparse), pointer :: B
    logical :: adjust(2)

    ! ... size of direction dir
    N = grid%B(1,dir)%n

    ! ... periodic length of domain in direction dir
    L = grid%periodicL(dir)

    adjust = .TRUE.
    If (grid%cartDims(dir) > 1) Then
      adjust = .FALSE.
      If (grid%cartCoords(dir) == 0)                    adjust(1) = .TRUE.
      If (grid%cartCoords(dir) == grid%cartDims(dir)-1) adjust(2) = .TRUE.
    End If

    ! ... augment F with its periodic version
    if (adjust(2) .eqv. .true.) Then
      do j = 1, grid%vdsGhost(dir)
        do i = 0, grid%nGhostRHS(dir,2)-1
          l0 = grid%vec_indGhost(j,N-i,dir)
          F(l0) = F(l0) + L
        end do
      end do
    end if

    if (adjust(1) .eqv. .true.) Then
      do j = 1, grid%vdsGhost(dir)
        do i = 1, grid%nGhostRHS(dir,1), 1
          l0 = grid%vec_indGhost(j,i,dir)
          F(l0) = F(l0) - L
        end do
      end do
    end if

    Return

  End Subroutine Plane_Periodic_Metric_Hack

  !-----------------------------------------------------------------------

  Subroutine Save_Patch_Deriv(region, ng, varID, deriv_dir, dflux, deriv_adj)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    implicit none

    type (t_region), pointer :: region
    integer :: ng, deriv_dir, varID
    real(rfreal), pointer :: dflux(:)
    logical, optional :: deriv_adj

    ! ... local variables
    type (t_grid), pointer :: grid
    type (t_patch), pointer :: patch
    integer :: patchID, Np(MAX_ND), N(MAX_ND), ip, jp, kp, lp, i, l0, j, k
    real(rfreal), pointer :: ptr_to_data(:,:,:)

    do patchID = 1, region%nPatches

      patch => region%patch(patchID)
      grid  => region%grid(patch%gridID)

      if (patch%gridID == ng .and. patch%bcType /= SPONGE .and. patch%bcType /= SPONGE_WITH_LINEARIZED_DISTURBANCE) Then

        nullify(ptr_to_data); ptr_to_data => patch%Deriv
        if (PRESENT(deriv_adj)) then
          if (deriv_adj) then
            nullify(ptr_to_data); ptr_to_data => patch%DerivAdj
          end if ! deriv_adj
        end if ! PRESENT(deriv_adj)

        Np(:) = 1; N(:) = 1
        do i = 1, grid%ND
          Np(i) = patch%ie(i) - patch%is(i) + 1
          N(i)  =  grid%ie(i) -  grid%is(i) + 1
        end do

        do kp = patch%is(3), patch%ie(3)
          do jp = patch%is(2), patch%ie(2)
            do ip = patch%is(1), patch%ie(1)

              ! ... point on the boundary in volume coordinates
              l0 = (kp- grid%is(3))* N(1)* N(2) + (jp- grid%is(2))*N(1)  + (ip- grid%is(1)+1)

              ! ... point on the boundary in boundary coordinates
              lp = (kp-patch%is(3))*Np(1)*Np(2) + (jp-patch%is(2))*Np(1) + (ip-patch%is(1)+1)

              ! ... save the value
              ptr_to_data(varID,lp,deriv_dir) = dflux(l0)

            end do
          end do
        end do

      end if

    end do

  End Subroutine Save_Patch_Deriv

  Subroutine Save_Patch_Viscous_Flux(region, ng, varID, deriv_dir, dflux)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    implicit none

    type (t_region), pointer :: region
    integer :: ng, deriv_dir, varID
    real(rfreal), pointer :: dflux(:)

    ! ... local variables
    type (t_grid), pointer :: grid
    type (t_patch), pointer :: patch
    integer :: patchID, Np(MAX_ND), N(MAX_ND), ip, jp, kp, lp, i, l0, j, k, varIDAux

    do patchID = 1, region%nPatches

      patch => region%patch(patchID)
      grid  => region%grid(patch%gridID)

      if (patch%gridID == ng .and. patch%bcType /= SPONGE .and. patch%bcType /= SPONGE_WITH_LINEARIZED_DISTURBANCE) Then

        Np(:) = 1; N(:) = 1
        do i = 1, grid%ND
          Np(i) = patch%ie(i) - patch%is(i) + 1
          N(i)  =  grid%ie(i) -  grid%is(i) + 1
        end do

        if (varID == 0 .AND. deriv_dir == 0) then

          Do k = 1, size(patch%ViscousFlux,3)
            Do j = 1, size(patch%ViscousFlux,2)
              Do i = 1, size(patch%ViscousFlux,1)
                patch%ViscousFlux(i,j,k) = 0.0_rfreal
              End Do
            End Do
          End Do

          Do k = 1, size(patch%ViscousFluxAux,3)
            Do j = 1, size(patch%ViscousFluxAux,2)
              Do i = 1, size(patch%ViscousFluxAux,1)
                patch%ViscousFluxAux(i,j,k) = 0.0_rfreal
              End Do
            End Do
          End Do

        else

          do kp = patch%is(3), patch%ie(3)
            do jp = patch%is(2), patch%ie(2)
              do ip = patch%is(1), patch%ie(1)

                ! ... point on the boundary in volume coordinates
                l0 = (kp- grid%is(3))* N(1)* N(2) + (jp- grid%is(2))*N(1)  + (ip- grid%is(1)+1)

                ! ... point on the boundary in boundary coordinates
                lp = (kp-patch%is(3))*Np(1)*Np(2) + (jp-patch%is(2))*Np(1) + (ip-patch%is(1)+1)

                ! ... save the value
                if(varID <= size(patch%ViscousFlux,1)) &
                   patch%ViscousFlux(varID,lp,deriv_dir) = patch%ViscousFlux(varID,lp,deriv_dir) + dflux(l0)

                if(varID > size(patch%ViscousFlux,1)) then
                   varIDAux = varID - size(patch%ViscousFlux,1)
                   if(varIDAux <= size(patch%ViscousFluxAux,1)) &
                        patch%ViscousFluxAux(varIDAux,lp,deriv_dir) = patch%ViscousFluxAux(varIDAux,lp,deriv_dir) + dflux(l0)
                endif

              end do
            end do
          end do

        end if

      end if

    end do

  End Subroutine Save_Patch_Viscous_Flux

 Subroutine Save_Patch_Viscous_RHS(region, ng, varID, dflux)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    implicit none

    type (t_region), pointer :: region
    integer :: ng, varID
    real(rfreal), pointer :: dflux(:)

! ... local variables
    type (t_grid), pointer :: grid
    type (t_patch), pointer :: patch
    integer :: patchID, Np(MAX_ND), N(MAX_ND), ip, jp, kp, lp, i, l0, j, k

    do patchID = 1, region%nPatches

       patch => region%patch(patchID)
       grid  => region%grid(patch%gridID)

       if (patch%gridID == ng .and. patch%bcType /= SPONGE .and. patch%bcType /= SPONGE_WITH_LINEARIZED_DISTURBANCE) Then

          Np(:) = 1; N(:) = 1
          do i = 1, grid%ND
             Np(i) = patch%ie(i) - patch%is(i) + 1
             N(i)  =  grid%ie(i) -  grid%is(i) + 1
          end do


          if (varID == 0 ) then

             Do j = 1, size(patch%ViscousRHSAux,2)
                Do i = 1, size(patch%ViscousRHSAux,1)
                   patch%ViscousRHSAux(i,j) = 0.0_rfreal
                End Do
             End Do

             cycle

          endif


          do kp = patch%is(3), patch%ie(3)
             do jp = patch%is(2), patch%ie(2)
                do ip = patch%is(1), patch%ie(1)

! ... point on the boundary in volume coordinates
                   l0 = (kp- grid%is(3))* N(1)* N(2) + (jp- grid%is(2))*N(1)  + (ip- grid%is(1)+1)

! ... point on the boundary in boundary coordinates
                   lp = (kp-patch%is(3))*Np(1)*Np(2) + (jp-patch%is(2))*Np(1) + (ip-patch%is(1)+1)

! ... save the value
                   patch%ViscousRHSAux(varID,lp) = patch%ViscousRHSAux(varID,lp) + dflux(l0)

                end do
             end do
          end do

       end if

    end do

  End Subroutine Save_Patch_Viscous_RHS

  Subroutine Save_Patch_TempGrad(region, ng, deriv_dir, dflux)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    implicit none

    type (t_region), pointer :: region
    integer :: ng, deriv_dir
    real(rfreal), pointer :: dflux(:)

    ! ... local variables
    type (t_grid), pointer :: grid
    type (t_patch), pointer :: patch
    integer :: patchID, Np(MAX_ND), N(MAX_ND), ip, jp, kp, lp, i, l0, j, k

    do patchID = 1, region%nPatches

      patch => region%patch(patchID)
      grid  => region%grid(patch%gridID)

      if (patch%gridID == ng .and. patch%bcType == NSCBC_WALL_NOSLIP_THERMALLY_COUPLED .or. &
           patch%bcType == SAT_WALL_NOSLIP_THERMALLY_COUPLED.OR. &
          patch%bcType == NSCBC_WALL_ISOTHERMAL_NOSLIP      .OR. &
          patch%bcType == SAT_NOSLIP_ISOTHERMAL_WALL        .OR. &
          patch%bcType == STRUCTURAL_INTERACTING           ) then


        Np(:) = 1; N(:) = 1
        do i = 1, grid%ND
          Np(i) = patch%ie(i) - patch%is(i) + 1
          N(i)  =  grid%ie(i) -  grid%is(i) + 1
        end do

        if (deriv_dir == 0) then

           Do k = 1, size(patch%TempGrad,2)
              Do j = 1, size(patch%TempGrad,1)
                 patch%TempGrad(j,k) = 0.0_rfreal
              End Do
           End Do

        else

          do kp = patch%is(3), patch%ie(3)
            do jp = patch%is(2), patch%ie(2)
              do ip = patch%is(1), patch%ie(1)

                ! ... point on the boundary in volume coordinates
                l0 = (kp- grid%is(3))* N(1)* N(2) + (jp- grid%is(2))*N(1)  + (ip- grid%is(1)+1)

                ! ... point on the boundary in boundary coordinates
                lp = (kp-patch%is(3))*Np(1)*Np(2) + (jp-patch%is(2))*Np(1) + (ip-patch%is(1)+1)

                ! ... save the value
                patch%TempGrad(lp,deriv_dir) = patch%TempGrad(lp,deriv_dir) + dflux(l0)

             end do
          end do
       end do

    end if

 end if

    end do

  End Subroutine Save_Patch_TempGrad


  subroutine INTERNAL_FILTER_box(region, gridID, order, dir, F, dF, grid_diff, exchange_flag)

    use ModGlobal
    use ModDataStruct
    USE ModMatrixVectorOps
    USE ModMPI
    USE ModPenta
    !USE ModSPACK

    implicit none

    type(t_grid), pointer :: grid
    type(t_region), pointer :: region
    type(t_mixt_input), pointer :: input
    type(t_mpi_timings), pointer :: timings
    integer :: dir, gridID, order, overlapPeriodic
    real(rfreal), pointer :: F(:), dF(:)
    logical :: grid_diff
    integer :: exchange_flag

    ! ... local variables
    integer :: N, mpiDer, Ncg, Ng, gOffset(2), iterations, Np, req, ierr
    real(rfreal), pointer :: dF_aux2(:,:), sbuf(:), rbuf(:)
    real(rfreal), pointer :: aloc(:,:), bloc(:,:), cloc(:,:), dloc(:,:), eloc(:,:)
    real(rfreal) :: fac, fac1, fac2, timer, timer2
    integer :: i, j, k, ii, l0, l1, iis, iie, N1, N2, N3, G1, G2, G3, N1g, N2g, N3g
    integer :: is, ie, js, je, ks, ke, is_in, ie_in, js_in, je_in, ks_in, ke_in
    integer :: ilc, jlc, klc
    integer :: Nc
    integer, pointer :: grid_periodic(:), deriv_type(:)
    real(rfreal) :: vals(-MAX_INTERIOR_STENCIL_WIDTH:MAX_INTERIOR_STENCIL_WIDTH)
    real(rfreal), pointer :: ptr_to_data(:)

    ! - edge information of box shape data struct ... WZhang, 10/2014
    INTEGER, POINTER      :: l_ctr_lc_b(:,:), r_ctr_lc_b(:,:)
    ! ... global index of locally detected edges
    INTEGER, POINTER      :: l_bndry_lc_b(:,:,:), r_bndry_lc_b(:,:,:)
    ! ... whether boundary points extended from neighboring processor
    INTEGER, POINTER      :: l_extended_b(:,:), r_extended_b(:,:)
    ! ... number of boundary points extended from neighboring processor
    INTEGER, POINTER      :: l_extended_size_b(:,:), r_extended_size_b(:,:)

    ! ... for boundary stencils
    integer :: bndry_width, bndry_depth_local, bndry_depth_max, bndry_interior
    integer :: bndry_affected, dist, bndry_location, bndry_diff
    integer :: ctr, ba, offset
    real(RFREAL) :: rhs_block1(12,20), rhs_block2(12,20)

    ! ... serial optimizations, WZhang 02/2015
    integer :: slice_k, offset_c, offset_ck, offset_ckj, offset_ckjii, offset_ckjb
    integer :: offset_cki, offset_ckib
    integer :: offset_cji, offset_cjib
    integer :: offset_dist
    integer :: bndry_extent
    real(rfreal) :: value
    integer, pointer :: nGhostRHS(:,:)
    integer :: grid_is(MAX_ND), grid_ie(MAX_ND), grid_is_unique(MAX_ND), grid_ie_unique(MAX_ND)
    integer :: rhs_block_depth, l0_dist

    ! ... start timing
    timer = MPI_WTime()

    ! ... pointer dereference
    grid         => region%grid(gridID)
    input        => grid%input
    timings      => region%mpi_timings(gridID)
    grid_periodic => grid%periodic
    deriv_type   => grid%deriv_type
    
    l_ctr_lc_b   => grid%l_ctr_lc_b(:,:,dir)
    r_ctr_lc_b   => grid%r_ctr_lc_b(:,:,dir)
    l_bndry_lc_b => grid%l_bndry_lc_b(:,:,:,dir)
    r_bndry_lc_b => grid%r_bndry_lc_b(:,:,:,dir)
    l_extended_b => grid%l_extended_b(:,:,dir)
    r_extended_b => grid%r_extended_b(:,:,dir)
    l_extended_size_b => grid%l_extended_size_b(:,:,dir)
    r_extended_size_b => grid%r_extended_size_b(:,:,dir)

    nGhostRHS => grid%nGhostRHS
    grid_is(:) = grid%is(:)
    grid_ie(:) = grid%ie(:)
    grid_is_unique(:) = grid%is_unique(:)
    grid_ie_unique(:) = grid%ie_unique(:)

    N1         = grid_ie_unique(1) - grid_is_unique(1) + 1
    N2         = grid_ie_unique(2) - grid_is_unique(2) + 1
    N3         = grid_ie_unique(3) - grid_is_unique(3) + 1
    G1         = nGhostRHS(1,1) + nGhostRHS(1,2)
    G2         = nGhostRHS(2,1) + nGhostRHS(2,2)
    G3         = nGhostRHS(3,1) + nGhostRHS(3,2)
    N1g        = N1 + G1
    N2g        = N2 + G2
    N3g        = N3 + G3

    Nc         = grid%nCells
    slice_k    = N1g*N2g

   ! ... size of interior stencil
    iis        = input%rhs_interior_stencil_start(order) 
    iie        = input%rhs_interior_stencil_end(order) 

    ! ... store values of interior stencil
    do ii = iis, iie
      vals(ii) = input%rhs_interior(order,ii)
    end do

    ! ... store values of the boundary stencil
    do j = 1, 20
      do i = 1, 12
        rhs_block1(i,j) = input%rhs_block1(order,j,i)
        rhs_block2(i,j) = input%rhs_block2(order,j,i)
      end do
    end do

    ! ... for boundary stencils
    bndry_width = input%operator_bndry_width(order)
    bndry_depth_local = input%operator_bndry_depth(order)
    bndry_depth_max =   maxval(input%operator_bndry_depth(:))
    bndry_interior = bndry_depth_max - bndry_depth_local
    bndry_diff = bndry_width - bndry_depth_local

    Select Case (dir)
      Case (1)
        dist = 1
      Case (2)
        dist = N1g
      Case (3)
        dist = N1g * N2g
    End Select

! ... this block will replace CID and Sparse_Simple, WZhang 10/2014
! ------------------------------------------------------------------------------------
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
    Do i = 1, Nc
      dF(i) = 0.0_8
    End Do

    Select Case (dir)
      Case (1)
        ks = grid_is_unique(3) - nGhostRHS(3,1)
        ke = grid_ie_unique(3) + nGhostRHS(3,2)
        js = grid_is_unique(2) - nGhostRHS(2,1)
        je = grid_ie_unique(2) + nGhostRHS(2,2)
        is = grid_is_unique(1)
        ie = grid_ie_unique(1)

        ! ... interior part
        if (nGhostRHS(1,1) == 0) then
          is_in = is - iis
        else
          is_in = is
        end if
        if (nGhostRHS(1,2) == 0) then
          ie_in = ie - iie
        else
          ie_in = ie
        end if
        offset_c = (-ks)*slice_k + (-js)*N1g + (-is+nGhostRHS(1,1)) + 1
        do k = ks, ke
          offset_ck = offset_c + k*slice_k
          do j = js, je
            offset_ckj = offset_ck + j*N1g
            do ii = iis, iie
              offset_ckjii = offset_ckj + ii
              value = vals(ii)
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
              do i = is_in, ie_in
                dF(i+offset_ckj) = dF(i+offset_ckj) + value * F(i+offset_ckjii)
              end do
            end do
          end do
        end do

        ! ... boundary part
        do k = ks, ke
          do j = js, je

            ! ... local indices on this face
            jlc = j - js + 1
            klc = k - ks + 1

            ! ... "regular" left edge
            if (l_ctr_lc_b(jlc,klc) > 0) then
              offset_ckj = (k-ks)*slice_k + (j-js)*N1g + (-is+nGhostRHS(1,1))
              do ctr = 1, l_ctr_lc_b(jlc,klc)
                bndry_location = l_bndry_lc_b(ctr,jlc,klc)
                offset_ckjb = offset_ckj + bndry_location
                bndry_extent = ie - bndry_location + 1
                if (bndry_extent < bndry_depth_max) then
                  bndry_affected = bndry_extent
                else
                  bndry_affected = bndry_depth_max
                end if
                do ba = 1, bndry_affected
                  l0 = ba + offset_ckjb
                  dF(l0) = 0.0_8
                  if (ba + bndry_interior > bndry_depth_max) then
                    do ii = iis, iie
                      dF(l0) = dF(l0) + vals(ii) * F(l0+ii*dist)
                    end do  ! ... i
                  else
                    rhs_block_depth = ba
                    offset_dist = (-rhs_block_depth)*dist
                    l0_dist = l0 + offset_dist
                    do ii = 1, bndry_width
                      dF(l0) = dF(l0) + rhs_block1(ii,rhs_block_depth) * F(l0_dist+ii*dist)
                    end do  ! ... i
                  end if
                end do  ! ... ba
              end do  ! ... ctr  
            end if ! ... "regular" left edge

            ! ... "extended" left edge
            if (l_extended_b(jlc,klc) > 0) then
              bndry_location = grid_is_unique(1)
              bndry_affected = l_extended_size_b(jlc,klc)
              offset = bndry_depth_max - bndry_affected
              offset_ckjb = (k-ks)*slice_k + (j-js)*N1g + (bndry_location-is+nGhostRHS(1,1))

              do ba = 1, bndry_affected
                l0 = ba + offset_ckjb
                dF(l0) = 0.0_8
                if (ba + offset + bndry_interior > bndry_depth_max) then
                  do ii = iis, iie
                    dF(l0) = dF(l0) + vals(ii) * F(l0+ii*dist)
                  end do  ! ... i
                else
                  rhs_block_depth = ba + offset
                  offset_dist = (-rhs_block_depth)*dist
                  l0_dist = l0 + offset_dist
                  do ii = 1, bndry_width
                    dF(l0) = dF(l0) + rhs_block1(ii,rhs_block_depth) * F(l0_dist+ii*dist)
                  end do ! ... i
                end if
              end do ! ... ba
            end if ! ... "extended" left edge

            ! ... "regular" right edge
            if (r_ctr_lc_b(jlc,klc) > 0) then
              offset_ckj = (k-ks)*slice_k + (j-js)*N1g + (-is+nGhostRHS(1,1)) + 1
              do ctr = 1, r_ctr_lc_b(jlc,klc)
                bndry_location = r_bndry_lc_b(ctr,jlc,klc)
                bndry_extent = bndry_location - is + 1
                if (bndry_extent < bndry_depth_max) then
                  bndry_affected = bndry_extent
                else
                  bndry_affected = bndry_depth_max
                end if
                offset = bndry_depth_max - bndry_affected
                offset_ckjb = offset_ckj + bndry_location - bndry_affected
                do ba = 1, bndry_affected
                  l0 = ba + offset_ckjb
                  dF(l0) = 0.0_8
                  if ((ba+offset) <= bndry_interior ) then
                    do ii = iis, iie
                      dF(l0) = dF(l0) + vals(ii) * F(l0+ii*dist)
                    end do  ! ... i
                  else
                    rhs_block_depth = ba - bndry_interior + offset
                    offset_dist = (-rhs_block_depth-bndry_diff)*dist
                    l0_dist = l0 + offset_dist
                    do ii = bndry_width, 1, -1
                      dF(l0) = dF(l0) + rhs_block2(ii,rhs_block_depth) * F(l0_dist+ii*dist)
                    end do ! ... i
                  end if
                end do  ! ... ba
              end do  ! ... ctr
            end if ! ... "regular" right edge

            ! ... "extended" right edge
            if (r_extended_b(jlc,klc) > 0) then
              bndry_location = grid_ie_unique(1)
              bndry_affected = r_extended_size_b(jlc,klc)
              offset_ckjb = (k-ks)*slice_k + (j-js)*N1g + (bndry_location-bndry_affected-is+nGhostRHS(1,1)) + 1
              do ba = 1, bndry_affected
                l0 = ba + offset_ckjb
                dF(l0) = 0.0_8
                if (ba <= bndry_interior) then
                  do ii = iis, iie
                    dF(l0) = dF(l0) + vals(ii) * F(l0+ii*dist)
                  end do  ! ... i
                else
                  rhs_block_depth = ba - bndry_interior
                  offset_dist = (-rhs_block_depth-bndry_diff)*dist
                  l0_dist = l0 + offset_dist
                  do ii = bndry_width, 1, -1
                    dF(l0) = dF(l0) + rhs_block2(ii,rhs_block_depth) * F(l0_dist+ii*dist)
                  end do ! ... i
                end if
              end do ! ... ba
            end if ! ... "extended" right edge

          end do ! ... j
        end do ! ... k

      Case (2)
        ks = grid_is_unique(3) - nGhostRHS(3,1)
        ke = grid_ie_unique(3) + nGhostRHS(3,2)
        is = grid_is_unique(1) - nGhostRHS(1,1)
        ie = grid_ie_unique(1) + nGhostRHS(1,2)
        js = grid_is_unique(2)
        je = grid_ie_unique(2)

        ! ... interior part
        if (nGhostRHS(2,1) == 0) then
          js_in = js - iis
        else
          js_in = js
        end if
        if (nGhostRHS(2,2) == 0) then
          je_in = je - iie
        else
          je_in = je
        end if
        offset_c = (-ks)*slice_k + (-js+nGhostRHS(2,1))*N1g + (-is) + 1
        do k = ks, ke
          offset_ck = offset_c + k*slice_k
          do j = js_in, je_in
            offset_ckj = offset_ck + j*N1g
            do ii = iis, iie
              offset_ckjii = offset_ckj + ii*N1g
              value = vals(ii)
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
              do i = is, ie
                dF(i+offset_ckj) = dF(i+offset_ckj) + value * F(i+offset_ckjii)
              end do
            end do
          end do
        end do

        ! ... boundary part
        do k = ks, ke
          do i = is, ie

            ! ... local indices on this face
            ilc = i - is + 1
            klc = k - ks + 1

            ! ... "regular" left edge
            if (l_ctr_lc_b(ilc,klc) > 0) then
              offset_cki = (k-ks)*slice_k + (-1-js+nGhostRHS(2,1))*N1g + (i-is) + 1
              do ctr = 1, l_ctr_lc_b(ilc,klc)
                bndry_location = l_bndry_lc_b(ctr,ilc,klc)
                offset_ckib = offset_cki + bndry_location*N1g
                bndry_extent = je - bndry_location + 1
                if (bndry_extent < bndry_depth_max) then
                  bndry_affected = bndry_extent
                else
                  bndry_affected = bndry_depth_max
                end if
                do ba = 1, bndry_affected
                  l0 = ba*N1g + offset_ckib
                  dF(l0) = 0.0_8
                  if (ba + bndry_interior > bndry_depth_max) then
                    do ii = iis, iie
                      dF(l0) = dF(l0) + vals(ii) * F(l0+ii*dist)
                    end do  ! ... i
                  else
                    rhs_block_depth = ba
                    offset_dist = (-rhs_block_depth)*dist
                    l0_dist = l0 + offset_dist
                    do ii = 1, bndry_width
                      dF(l0) = dF(l0) + rhs_block1(ii,rhs_block_depth) * F(l0_dist+ii*dist)
                    end do  ! ... i
                  end if
                end do  ! ... ba
              end do  ! ... ctr  
            end if ! ... "regular" left edge

            ! ... "extended" left edge
            if (l_extended_b(ilc,klc) > 0) then
              bndry_location = grid_is_unique(2)
              bndry_affected = l_extended_size_b(ilc,klc)
              offset = bndry_depth_max - bndry_affected
              offset_ckib = (k-ks)*slice_k + (bndry_location-1-js+nGhostRHS(2,1))*N1g + (i-is) + 1

              do ba = 1, bndry_affected
                l0 = ba*N1g + offset_ckib
                dF(l0) = 0.0_8
                if (ba + offset + bndry_interior > bndry_depth_max) then
                  do ii = iis, iie
                    dF(l0) = dF(l0) + vals(ii) * F(l0+ii*dist)
                  end do  ! ... i
                else
                  rhs_block_depth = ba + offset
                  offset_dist = (-rhs_block_depth)*dist
                  l0_dist = l0 + offset_dist
                  do ii = 1, bndry_width
                    dF(l0) = dF(l0) + rhs_block1(ii,rhs_block_depth) * F(l0_dist+ii*dist)
                  end do ! ... i
                end if
              end do ! ... ba
            end if ! ... "extended" left edge

            ! ... "regular" right edge
            if (r_ctr_lc_b(ilc,klc) > 0) then
              offset_cki = (k-ks)*slice_k + (-js+nGhostRHS(2,1))*N1g + (i-is) + 1
              do ctr = 1, r_ctr_lc_b(ilc,klc)
                bndry_location = r_bndry_lc_b(ctr,ilc,klc)
                bndry_extent = bndry_location - js + 1
                if (bndry_extent < bndry_depth_max) then
                  bndry_affected = bndry_extent
                else
                  bndry_affected = bndry_depth_max
                end if
                offset = bndry_depth_max - bndry_affected
                offset_ckib = offset_cki + (bndry_location-bndry_affected)*N1g
                do ba = 1, bndry_affected
                  l0 = ba*N1g + offset_ckib
                  dF(l0) = 0.0_8
                  if ((ba+offset) <= bndry_interior ) then
                    do ii = iis, iie
                      dF(l0) = dF(l0) + vals(ii) * F(l0+ii*dist)
                    end do  ! ... i
                  else
                    rhs_block_depth = ba - bndry_interior + offset
                    offset_dist = (-rhs_block_depth-bndry_diff)*dist
                    l0_dist = l0 + offset_dist
                    do ii = bndry_width, 1, -1
                      dF(l0) = dF(l0) + rhs_block2(ii,rhs_block_depth) * F(l0_dist+ii*dist)
                    end do ! ... i
                  end if
                end do  ! ... ba
              end do  ! ... ctr
            end if ! ... "regular" right edge

            ! ... "extended" right edge
            if (r_extended_b(ilc,klc) > 0) then
              bndry_location = grid_ie_unique(2)
              bndry_affected = r_extended_size_b(ilc,klc)
              offset_ckib = (k-ks)*slice_k + (bndry_location-bndry_affected-js+nGhostRHS(2,1))*N1g + (i-is) + 1
              do ba = 1, bndry_affected
                l0 = ba*N1g + offset_ckib
                dF(l0) = 0.0_8
                if (ba <= bndry_interior) then
                  do ii = iis, iie
                    dF(l0) = dF(l0) + vals(ii) * F(l0+ii*dist)
                  end do  ! ... i
                else
                  rhs_block_depth = ba - bndry_interior
                  offset_dist = (-rhs_block_depth-bndry_diff)*dist
                  l0_dist = l0 + offset_dist
                  do ii = bndry_width, 1, -1
                    dF(l0) = dF(l0) + rhs_block2(ii,rhs_block_depth) * F(l0_dist+ii*dist)
                  end do ! ... i
                end if
              end do ! ... ba
            end if ! ... "extended" right edge

          end do ! ... i
        end do ! ... k

      Case (3)
        ks = grid_is_unique(3)
        ke = grid_ie_unique(3)
        js = grid_is_unique(2) - nGhostRHS(2,1)
        je = grid_ie_unique(2) + nGhostRHS(2,2)
        is = grid_is_unique(1) - nGhostRHS(1,1)
        ie = grid_ie_unique(1) + nGhostRHS(1,2)

        ! ... interior part
        if (nGhostRHS(3,1) == 0) then
          ks_in = ks - iis
        else
          ks_in = ks
        end if
        if (nGhostRHS(3,2) == 0) then
          ke_in = ke - iie
        else
          ke_in = ke
        end if
        offset_c = (-ks+nGhostRHS(3,1))*slice_k + (-js)*N1g + (-is) + 1
        do k = ks_in, ke_in
          offset_ck = offset_c + k*slice_k
          do j = js, je
          offset_ckj = offset_ck + j*N1g
            do ii = iis, iie
              offset_ckjii = offset_ckj + ii*slice_k
              value = vals(ii)
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
              do i = is, ie
                dF(i+offset_ckj) = dF(i+offset_ckj) + value * F(i+offset_ckjii)
              end do
            end do
          end do
        end do

        ! ... boundary part
        do j = js, je
          do i = is, ie

            ! ... local indices on this face
            ilc = i - is + 1
            jlc = j - js + 1

            ! ... "regular" left edge
            if (l_ctr_lc_b(ilc,jlc) > 0) then
              offset_cji = (-1-ks+nGhostRHS(3,1))*slice_k + (j-js)*N1g + (i-is) + 1
              do ctr = 1, l_ctr_lc_b(ilc,jlc)
                bndry_location = l_bndry_lc_b(ctr,ilc,jlc)
                offset_cjib = offset_cji + bndry_location*slice_k
                bndry_extent = ke - bndry_location + 1
                if (bndry_extent < bndry_depth_max) then
                  bndry_affected = bndry_extent
                else
                  bndry_affected = bndry_depth_max
                end if
                do ba = 1, bndry_affected
                  l0 = ba*slice_k + offset_cjib
                  dF(l0) = 0.0_8
                  if (ba + bndry_interior > bndry_depth_max) then
                    do ii = iis, iie
                      dF(l0) = dF(l0) + vals(ii) * F(l0+ii*dist)
                    end do  ! ... i
                  else
                    rhs_block_depth = ba
                    offset_dist = (-rhs_block_depth)*dist
                    l0_dist = l0 + offset_dist
                    do ii = 1, bndry_width
                      dF(l0) = dF(l0) + rhs_block1(ii,rhs_block_depth) * F(l0_dist+ii*dist)
                    end do  ! ... i
                  end if
                end do  ! ... ba
              end do  ! ... ctr  
            end if ! ... "regular" left edge

            ! ... "extended" left edge
            if (l_extended_b(ilc,jlc) > 0) then
              bndry_location = grid_is_unique(3)
              bndry_affected = l_extended_size_b(ilc,jlc)
              offset = bndry_depth_max - bndry_affected
              offset_cjib = (bndry_location-1-ks+nGhostRHS(3,1))*slice_k + (j-js)*N1g + (i-is) + 1

              do ba = 1, bndry_affected
                l0 = ba*slice_k + offset_cjib
                dF(l0) = 0.0_8
                if (ba + offset + bndry_interior > bndry_depth_max) then
                  do ii = iis, iie
                    dF(l0) = dF(l0) + vals(ii) * F(l0+ii*dist)
                  end do  ! ... i
                else
                  rhs_block_depth = ba + offset
                  offset_dist = (-rhs_block_depth)*dist
                  l0_dist = l0 + offset_dist
                  do ii = 1, bndry_width
                    dF(l0) = dF(l0) + rhs_block1(ii,rhs_block_depth) * F(l0_dist+ii*dist)
                  end do ! ... i
                end if
              end do ! ... ba
            end if ! ... "extended" left edge

            ! ... "regular" right edge
            if (r_ctr_lc_b(ilc,jlc) > 0) then
              offset_cji = (-ks+nGhostRHS(3,1))*slice_k + (j-js)*N1g + (i-is) + 1
              do ctr = 1, r_ctr_lc_b(ilc,jlc)
                bndry_location = r_bndry_lc_b(ctr,ilc,jlc)
                bndry_extent = bndry_location - ks + 1
                if (bndry_extent < bndry_depth_max) then
                  bndry_affected = bndry_extent
                else
                  bndry_affected = bndry_depth_max
                end if
                offset = bndry_depth_max - bndry_affected
                offset_cjib = offset_cji + (bndry_location-bndry_affected)*slice_k
                do ba = 1, bndry_affected
                  l0 = ba*slice_k + offset_cjib
                  dF(l0) = 0.0_8
                  if ((ba+offset) <= bndry_interior ) then
                    do ii = iis, iie
                      dF(l0) = dF(l0) + vals(ii) * F(l0+ii*dist)
                    end do  ! ... i
                  else
                    rhs_block_depth = ba - bndry_interior + offset
                    offset_dist = (-rhs_block_depth-bndry_diff)*dist
                    l0_dist = l0 + offset_dist
                    do ii = bndry_width, 1, -1
                      dF(l0) = dF(l0) + rhs_block2(ii,rhs_block_depth) * F(l0_dist+ii*dist)
                    end do ! ... i
                  end if
                end do  ! ... ba
              end do  ! ... ctr
            end if ! ... "regular" right edge

            ! ... "extended" right edge
            if (r_extended_b(ilc,jlc) > 0) then
              bndry_location = grid_ie_unique(3)
              bndry_affected = r_extended_size_b(ilc,jlc)
              offset_cjib = (bndry_location-bndry_affected-ks+nGhostRHS(3,1))*slice_k + (j-js)*N1g + (i-is) + 1
              do ba = 1, bndry_affected
                l0 = ba*slice_k + offset_cjib
                dF(l0) = 0.0_8
                if (ba <= bndry_interior) then
                  do ii = iis, iie
                    dF(l0) = dF(l0) + vals(ii) * F(l0+ii*dist)
                  end do  ! ... i
                else
                  rhs_block_depth = ba - bndry_interior
                  offset_dist = (-rhs_block_depth-bndry_diff)*dist
                  l0_dist = l0 + offset_dist
                  do ii = bndry_width, 1, -1
                    dF(l0) = dF(l0) + rhs_block2(ii,rhs_block_depth) * F(l0_dist+ii*dist)
                  end do ! ... i
                end if
              end do ! ... ba
            end if ! ... "extended" right edge

          end do ! ... i
        end do ! ... j
    End Select

    ! ... exchange the ghost cell data right now, if we need
    if (exchange_flag == 1) then
      ptr_to_data => dF
      Call Ghost_Cell_Exchange_Box(region, gridID, ptr_to_data)
    end if

! -------------------------------------------------------------------------------------
    if (grid%operator_type(order) == EXPLICIT) then
      timings%operator(order,dir) = timings%operator(order,dir) + (MPI_WTIME() - timer)
      return
    end if

! ... let's first comment out the part that has not been adjusted to box halo, WZhang 04/2015
!#ifdef USE_HYPRE
!    if (grid%operator_implicit_solver(order) == GMRES) then
!
!      ! ... update HYPRE right-hand-side vector
!      call fill_hypre_pade_implicit_rhs_vector(region%nGridsGlobal, grid%iGridGlobal, gridID, region%nGrids, &
!           grid%myrank_inComm, grid%numproc_inComm, grid%comm, &
!           grid%ND, dir, order, grid%is, grid%ie, dF)
!
!      ! ... initialize the HYPRE solution vector to be the unfiltered field
!      call fill_hypre_pade_implicit_soln_vector(region%nGridsGlobal, grid%iGridGlobal, gridID, region%nGrids, &
!           grid%myrank_inComm, grid%numproc_inComm, grid%comm, &
!           grid%ND, dir, order, grid%is, grid%ie, F)
!
!      ! ... solve the HYPRE system using ILU preconditioning
!      timer2 = MPI_WTime()
!      call hypre_solve_pade_implicit(region%nGridsGlobal, grid%iGridGlobal, gridID, region%nGrids, &
!           grid%myrank_inComm, grid%numproc_inComm, grid%comm, &
!           grid%ND, dir, order, grid%is, grid%ie, iterations)
!      region%mpi_timings(gridID)%operator_solve(order,dir) = region%mpi_timings(gridID)%operator_solve(order,dir) &
!           + (MPI_WTIME() - timer2)
!
!      if (grid%myrank_inComm == 0) then
!        if (iterations > 50) then
!          write (*,'(A,I3,A)') 'PlasComCM: Warning: HYPRE-based filter solve needed ', iterations, ' iterations.'
!          write (*,'(A,I3,A)') 'PlasComCM: Consider changing ILU preconditioner settings.'
!          write (*,'(A,I3)')   'PlasComCM: opID = ', order
!        end if
!      end if
!
!      ! ... retreive the HYPRE solution
!      call hypre_update_pade_implicit_soln_vector(region%nGridsGlobal, grid%iGridGlobal, gridID, region%nGrids, &
!           grid%myrank_inComm, grid%numproc_inComm, grid%comm, &
!           grid%ND, dir, order, grid%is, grid%ie, dF)
!
!      ! ... update 
!      if (order == input%iFilter) then
!        fac1 = 1.0_rfreal - input%filter_fac
!        fac2 = input%filter_fac
!      else
!        fac1 = 1.0_rfreal - input%LHS_Filter_Fac
!        fac2 = input%LHS_Filter_Fac
!      end if
!
!      do i = 1, Nc
!        dF(i) = fac1 * F(i) + fac2 * dF(i)
!      end do
!
!      timings%operator(order,dir) = timings%operator(order,dir) + (MPI_WTIME() - timer)
!
!      return
!
!    end if
!#endif
!
    ! ... otherwise we are Pade and need to solve pentadiagonal system  
    N  = grid%ie_unique(dir)-grid%is_unique(dir)+1
    allocate(dF_aux2(grid%vds(dir),N))

    Select Case (dir)
      Case(1)
        do k = 1, N3
          do j = 1, N2
            do i = 1, N1
              l0 = (k-1+nGhostRHS(3,1))*N2g*N1g + (j-1+nGhostRHS(2,1))*N1g + nGhostRHS(1,1) + i
              l1 = (k-1)*N2 + j
              dF_aux2(l1,i) = dF(l0)
            end do
          end do
        end do
      Case(2)
        do k = 1, N3
          do i = 1, N1
            do j = 1, N2
              l0 = (k-1+nGhostRHS(3,1))*N2g*N1g + (j-1+nGhostRHS(2,1))*N1g + nGhostRHS(1,1) + i
              l1 = (k-1)*N1 + i
              dF_aux2(l1,j) = dF(l0)
            end do
          end do
        end do
      Case(3)
        do j = 1, N2
          do i = 1, N1
            do k = 1, N3
              l0 = (k-1+nGhostRHS(3,1))*N2g*N1g + (j-1+nGhostRHS(2,1))*N1g + nGhostRHS(1,1) + i
              l1 = (j-1)*N1 + i
              dF_aux2(l1,k) = dF(l0)
            end do
          end do
        end do
    End Select

    aloc => grid%penta(order,dir)%a
    bloc => grid%penta(order,dir)%b
    cloc => grid%penta(order,dir)%c
    dloc => grid%penta(order,dir)%d
    eloc => grid%penta(order,dir)%e

    mpiDer = FALSE
    If ( grid%cartDims(dir) > 1 ) mpiDer = TRUE

    overlapPeriodic = FALSE
    Np = N
    if ( grid%periodic(dir) >= TRUE .and. grid%periodicStorage == OVERLAP_PERIODIC) then
      overlapPeriodic = TRUE
      if (grid%myrank_inPencilComm(dir) == (grid%numproc_inPencilComm(dir)-1)) Np = N - 1
    end if

    timer2 = MPI_WTime()
    If (grid%periodic(dir) >= TRUE) Then
      If (mpiDer == FALSE) Then
        call pentaPeriodicM(aloc, bloc, cloc, dloc, eloc, &
             dF_aux2, Np, grid%vds(dir))
      Else
        call MPI_pentaPeriodicM(aloc, bloc, cloc, dloc, eloc, &
             dF_aux2, Np, grid%vds(dir), grid%pencilComm(dir))
      End If
    Else
      If (mpiDer == FALSE) Then
        call pentaDiagonalM(aloc, bloc, cloc, dloc, eloc, & 
             dF_aux2, N, grid%vds(dir))
      Else
        call MPI_pentaDiagonalM(aloc, bloc, cloc, dloc, eloc, &
             dF_aux2, N, grid%vds(dir), grid%pencilComm(dir))
      End If
    End If
        
    region%mpi_timings(gridID)%operator_solve(order,dir) = region%mpi_timings(gridID)%operator_solve(order,dir) &
         + (MPI_WTIME() - timer2)

    if (overlapPeriodic == TRUE) then
      if (grid%numproc_inPencilComm(dir) == 1) then
        do j = 1, grid%vds(dir)
          dF_aux2(j,N) = dF_aux2(j,1)
        end do
      else
        if ((grid%myrank_inPencilComm(dir) == (grid%numproc_inPencilComm(dir)-1)) .or. (grid%myrank_inPencilComm(dir) == 0)) then
          ! ... lots of heap memory being made ...
          allocate(sbuf(grid%vds(dir)))
          allocate(rbuf(grid%vds(dir)))
          if (grid%myrank_inPencilComm(dir) == 0) then 
            do j = 1, grid%vds(dir)
              sbuf(j) = dF_aux2(j,1)
            end do
            Call MPI_ISend(sbuf, grid%vds(dir), MPI_DOUBLE_PRECISION, grid%numproc_inPencilComm(dir)-1, 0, &
                           grid%pencilComm(dir), req, ierr)
          end if
          if (grid%myrank_inPencilComm(dir) == (grid%numproc_inPencilComm(dir)-1)) then 
            Call MPI_Recv(rbuf, grid%vds(dir), MPI_DOUBLE_PRECISION, 0, 0, grid%pencilComm(dir), MPI_STATUS_IGNORE, ierr)
            do j = 1, grid%vds(dir)
              dF_aux2(j,N) = rbuf(j)
            end do
          end if
          if (grid%myrank_inPencilComm(dir) == 0) then 
            Call MPI_Wait(req, MPI_STATUS_IGNORE, ierr)
          end if
        end if
        !call mpi_barrier(grid%pencilComm(dir), ierr)
        if ((grid%myrank_inPencilComm(dir) == (grid%numproc_inPencilComm(dir)-1)) .or. (grid%myrank_inPencilComm(dir) == 0)) then
          deallocate(sbuf,rbuf)
        end if
      end if 
    end if

    ! ... update 
    if (order == input%iFilter) then
      fac1 = 1.0_rfreal - input%filter_fac
      fac2 = input%filter_fac
    else
      fac1 = 1.0_rfreal - input%LHS_Filter_Fac
      fac2 = input%LHS_Filter_Fac
    end if

    ! ... populate the output vector
    Select Case (dir)
      Case(1)
        do k = 1, N3
          do j = 1, N2
            do i = 1, N1
              l0 = (k-1+nGhostRHS(3,1))*N2g*N1g + (j-1+nGhostRHS(2,1))*N1g + nGhostRHS(1,1) + i
              l1 = (k-1)*N2 + j
              dF(l0) = fac1 * F(l0) + fac2 * dF_aux2(l1,i)
            end do
          end do
        end do
      Case(2)
        do k = 1, N3
          do i = 1, N1
            do j = 1, N2
              l0 = (k-1+nGhostRHS(3,1))*N2g*N1g + (j-1+nGhostRHS(2,1))*N1g + nGhostRHS(1,1) + i
              l1 = (k-1)*N1 + i
              dF(l0) = fac1 * F(l0) + fac2 * dF_aux2(l1,j)
            end do
          end do
        end do
      Case(3)
        do j = 1, N2
          do i = 1, N1
            do k = 1, N3
              l0 = (k-1+nGhostRHS(3,1))*N2g*N1g + (j-1+nGhostRHS(2,1))*N1g + nGhostRHS(1,1) + i
              l1 = (j-1)*N1 + i
              dF(l0) = fac1 * F(l0) + fac2 * dF_aux2(l1,k)
            end do
          end do
        end do
    End Select

    timings%operator(order,dir) = timings%operator(order,dir) + (MPI_WTIME() - timer)

    deallocate(dF_aux2)

    ! ... exchange the halo data
    ptr_to_data => dF
    Call Ghost_Cell_Exchange_Box(region, gridID, ptr_to_data)

    return

  end subroutine INTERNAL_FILTER_box


  Subroutine Filter(region, ng, flag, opID_in)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    implicit none

    Type (t_region), pointer :: region
    integer :: flag, ng
    integer, optional :: opID_in

    ! ... local variables
    Integer :: i, j, k, opID
    Type (t_grid), pointer :: grid
    Type (t_mixt), pointer :: state
    Type (t_mixt_input), pointer :: input
    Real(rfreal), pointer :: flux(:), dflux(:)
    Real(rfreal) :: ibfac
    Integer :: order(2*MAX_ND,MAX_ND), Nc
    real(rfreal), pointer :: ptr_to_data(:)

    If (present(opID_in) .eqv. .true.) then
      opID = opID_in
    else
      opID = region%input%iFilter
    End if

    If ((region%global%filtercount(opID) == 2) .and. (region%input%ND == 1)) region%global%filtercount(opID) = 1
    If ((region%global%filtercount(opID) == 3) .and. (region%input%ND == 2)) region%global%filtercount(opID) = 1
    If ((region%global%filtercount(opID) == 7) .and. (region%input%ND == 3)) region%global%filtercount(opID) = 1

    ! ... filtering order
    order(1,:) = (/ 1, 2, 3 /)
    order(2,:) = (/ 2, 1, 3 /)
    order(3,:) = (/ 3, 2, 1 /)
    order(4,:) = (/ 1, 3, 2 /)
    order(5,:) = (/ 2, 3, 1 /)
    order(6,:) = (/ 3, 1, 2 /)

    grid  => region%grid(ng)
    state => region%state(ng)
    input => grid%input
    Nc    = grid%nCells

    ! ... allocate local memory (state%flux and state%dflux may not be available)
    allocate(flux(Nc),dflux(Nc))

    Do j = 1, input%nCv

      ! ... filter the values
      if (flag == FALSE) then
        if (region%input%AdjNS) then
          ptr_to_data => state%av(:,j)
          Call Ghost_Cell_Exchange_Box(region, ng, ptr_to_data)
          do i = 1, Nc
            flux(i) = state%av(i,j)
          end do
        else
          ptr_to_data => state%cv(:,j)
          Call Ghost_Cell_Exchange_Box(region, ng, ptr_to_data)
          do i = 1, Nc
            flux(i) = state%cv(i,j)
          end do
        end if ! input%AdjNS
      end if

      ! ... filter the rhs
      if (flag == TRUE ) then
        ptr_to_data => state%rhs(:,j)
        Call Ghost_Cell_Exchange_Box(region, ng, ptr_to_data)
        do i = 1, Nc
          flux(i) = state%rhs(i,j)
        end do
      end if

      ! ... main filter loop
      Do i = 1, grid%ND
        call internal_filter_box(region, ng, opID, order(region%global%filtercount(opID),i), flux, dflux, .FALSE., 1)
        do k = 1, Nc
          flux(k) = dflux(k)
        end do
      End Do

      ! ... filter the values
      if (flag == FALSE) then
        if (region%input%AdjNS) then
          do i = 1, Nc
            state%av(i,j) = dflux(i)
          end do
        else
          do i = 1, Nc
            state%cv(i,j) = dflux(i)
          end do
        end if ! input%AdjNS
      end if

      ! ... filter the rhs
      if (flag == TRUE) then
        do i = 1, Nc
          state%rhs(i,j) = dflux(i) * dble(MIN(abs(grid%iblank(i)),1))
        end do
      end if

    End Do

    Do j = 1, input%nAuxVars

      ! ... filter the values
      if (flag == FALSE) then
        ptr_to_data => state%AuxVars(:,j)
        Call Ghost_Cell_Exchange_Box(region, ng, ptr_to_data)
        do i = 1, Nc
          flux(i) = state%AuxVars(i,j)
        end do
      end if

      ! ... filter the rhs
      if (flag == TRUE ) then
        ptr_to_data => state%rhs_AuxVars(:,j)
        Call Ghost_Cell_Exchange_Box(region, ng, ptr_to_data)
        do i = 1, Nc
          flux(i) = state%rhs_AuxVars(i,j)
        end do
      end if

      ! ... main filter loop
      Do i = 1, grid%ND
        call internal_filter_box(region, ng, opID, order(region%global%filtercount(opID),i), flux, dflux, .FALSE., 1)
        do k = 1, Nc
          flux(k) = dflux(k)
        end do
      End Do

      ! ... filter the values
      if (flag == FALSE) then
        do i = 1, Nc
          state%AuxVars(i,j) = dflux(i)
        end do
      end if

      ! ... filter the rhs
      if (flag == TRUE) then
        do i = 1, Nc
          state%rhs_AuxVars(i,j) = dflux(i) * dble(MIN(abs(grid%iblank(i)),1))
        end do
      end if

    End Do

    ! ... clean up
    region%global%filtercount(opID) = region%global%filtercount(opID) + 1

    deallocate(flux,dflux)

  End subroutine filter

  Subroutine TestFilter(region, ng, ToBeFiltered, Filtered)

    USE ModGlobal
    USE ModDataStruct

    implicit none

    ! ... subroutine arguments
    Type (t_region), pointer :: region
    integer :: ng
    real(rfreal), pointer :: ToBeFiltered(:), Filtered(:)

    ! ... local variables
    Integer :: i, j
    Type (t_grid), pointer :: grid
    Type (t_mixt), pointer :: state
    Type (t_mixt_input), pointer :: input
    Real(rfreal), pointer :: flux(:), dflux(:)
    Integer :: order(2*MAX_ND,MAX_ND), Nc, opID

    ! ... filtering order
    order(1,:) = (/ 1, 2, 3 /)
    order(2,:) = (/ 2, 1, 3 /)
    order(3,:) = (/ 3, 2, 1 /)
    order(4,:) = (/ 1, 3, 2 /)
    order(5,:) = (/ 2, 3, 1 /)
    order(6,:) = (/ 3, 1, 2 /)

    grid  => region%grid(ng)
    state => region%state(ng)
    input => grid%input
    Nc = grid%nCells

    opID = input%iFilterTest

    If ((region%global%filtercount(opID) == 3) .and. (region%input%ND == 2)) region%global%filtercount(opID) = 1
    If ((region%global%filtercount(opID) == 7) .and. (region%input%ND == 3)) region%global%filtercount(opID) = 1

    Do i = 1, grid%nCells
      state%flux(i) = ToBeFiltered(i)
    End Do

    Do i = 1, grid%ND

      call internal_filter_box(region, ng, opID, order(region%global%filtercount(opID),i), state%Flux, state%dFlux, .FALSE., 1)
      Do j = 1, grid%nCells
        state%flux(j) = state%dflux(j)
      End Do

    End Do ! i

    Do i = 1, grid%nCells
      Filtered(i) = state%flux(i)
    End Do

    region%global%filtercount(opID) = region%global%filtercount(opID) + 1

  End subroutine TestFilter

  Subroutine TruncatedGaussianFilter(region, ng, ToBeFiltered, Filtered)

    USE ModGlobal
    USE ModDataStruct

    implicit none

    ! ... subroutine arguments
    Type (t_region), pointer :: region
    integer :: ng
    real(rfreal), pointer :: ToBeFiltered(:), Filtered(:)

    ! ... local variables
    Integer :: i, j
    Type (t_grid), pointer :: grid
    Type (t_mixt), pointer :: state
    Type (t_mixt_input), pointer :: input
    Real(rfreal), pointer :: flux(:), dflux(:)
    Integer :: order(2*MAX_ND,MAX_ND), Nc, opID

    ! ... filtering order
    order(1,:) = (/ 1, 2, 3 /)
    order(2,:) = (/ 2, 1, 3 /)
    order(3,:) = (/ 3, 2, 1 /)
    order(4,:) = (/ 1, 3, 2 /)
    order(5,:) = (/ 2, 3, 1 /)
    order(6,:) = (/ 3, 1, 2 /)

    grid => region%grid(ng)
    state => region%state(ng)
    input => grid%input
    Nc = grid%nCells

    opID = input%iFilterGaussianHyperviscosity

    If ((region%global%filtercount(opID) == 3) .and. (region%input%ND == 2)) region%global%filtercount(opID) = 1
    If ((region%global%filtercount(opID) == 7) .and. (region%input%ND == 3)) region%global%filtercount(opID) = 1

    Do i = 1, grid%nCells
      state%flux(i) = ToBeFiltered(i)
    End Do

    Do i = 1, grid%ND

      call internal_filter_box(region, ng, opID, order(region%global%filtercount(opID),i), state%Flux, state%dFlux, .FALSE., 1)
      Do j = 1, grid%nCells
        state%flux(j) = state%dflux(j)
      End Do

    End Do ! i

    Do i = 1, grid%nCells
      Filtered(i) = state%flux(i)
    End Do

    region%global%filtercount(opID) = region%global%filtercount(opID) + 1

  End subroutine TruncatedGaussianFilter

  subroutine APPLY_OPERATOR(region, gridID, order, dir, F, dF, grid_diff, coordDir_in)

!   optimized by John L. Larson 7/17/2014

    use ModGlobal
    use ModDataStruct
    USE ModMatrixVectorOps
    USE ModMPI

    implicit none

    type(t_grid), pointer :: grid
    type(t_region), pointer :: region
    type(t_mixt_input), pointer :: input
    type(t_mpi_timings), pointer :: timings
    integer :: dir, gridID, order
    real(rfreal), pointer :: F(:), dF(:)
    logical :: grid_diff
    integer, optional :: coordDir_in

    ! ... local variables
    integer :: i, j, k, ii, l0, l1, Nc, N, mpiDer, Ncg, Ng, gOffset(MAX_ND), iterations, iis, iie, ND
    integer :: is, ie, js, je, ks, ke, N1, N2, N3, NGhost, coordDir, Nlocal(MAX_ND), NwithHalo(MAX_ND)
    real(rfreal), pointer :: F_aux(:), dF_aux(:), dF_aux2(:,:)
    real(rfreal), pointer :: aloc(:,:), bloc(:,:), cloc(:,:), dloc(:,:), eloc(:,:)
    real(rfreal) :: fac, timer, timer2

    ! ... declarations for pointer dereference, WZhang 05/2014
    integer, pointer :: grid_is(:), grid_ie(:), cell2ghost(:,:), grid_periodic(:), deriv_type(:), nGhostRHS(:,:)
    integer :: nGrids, nGridsGlobal, iGridGlobal, myrank_inComm, numproc_inComm, comm
    type (t_matrix_sparse), pointer :: B(:,:)

    integer :: N2N1, NH1NH2
    integer :: offset_k, offset_jk, hoffset_k, hoffset_jk

    ! ... timings
    timer = MPI_WTIME()

    ! ... simplicity
    grid         => region%grid(gridID)
    input        => grid%input
    timings      => region%mpi_timings(gridID)
    ND           = grid%ND
    Nc           = grid%nCells
    N            = grid%ie(dir)-grid%is(dir)+1
    NGhost       = grid%nGhostRHS(dir,1) + grid%nGhostRHS(dir,2)
    Ncg          = Nc + NGhost*grid%vdsGhost(dir)
    Ng           = N + NGhost
    Nlocal(:)    = 1
    NwithHalo(:) = 1
    gOffset(:)   = 0
    gOffset(dir) = grid%nGhostRHS(dir,1)

    ! ... pointer dereference, WZhang 05/2014
    grid_is       => grid%is
    grid_ie       => grid%ie
    cell2ghost    => grid%cell2ghost
    nGhostRHS     => grid%nGhostRHS
    grid_periodic => grid%periodic
    B             => grid%B
    deriv_type    => grid%deriv_type
    nGrids         = region%nGrids
    nGridsGlobal   = region%nGridsGlobal
    iGridGlobal    = grid%iGridGlobal
    myrank_inComm  = grid%myrank_inComm
    numproc_inComm = grid%numproc_inComm
    comm           = grid%comm

    ! ... local sizes
    Nlocal(1:ND)    = grid_ie(1:ND) - grid_is(1:ND) + 1
    NwithHalo(1:ND) = Nlocal(1:ND)

    ! ... extend in the halo direction
    NwithHalo(dir) = Nlocal(dir) + NGhost

    ! ... check for coordDir argument
    if (present(coordDir_in) .eqv. .true.) then
      coordDir = coordDir_in
    else
      coordDir = 0
    end if

    ! ... temporary storage
    allocate(dF_aux(Ncg), F_aux(Ncg))

#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
    Do i = 1, Ncg
      dF_aux(i) = 0.0_8
    End Do

    ! ... fill in this grid's data
#ifdef USE_EXPERIMENTAL_ROUTINES
    do k = 1, Nlocal(3)
      do j = 1, Nlocal(2)
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
        do i = 1, Nlocal(1)

          ! ... local counting
          l0 = (k-1)*Nlocal(2)*Nlocal(1) + (j-1)*Nlocal(1) + i

          ! ... counting in halo coordinates 
          l1 = (k-1+gOffset(3))*NwithHalo(1)*NwithHalo(2) + (j-1+gOffset(2))*NwithHalo(1) + i+gOffset(1)

          ! ... fill the buffer
          F_aux(l1) = F(l0)

        end do
      end do
    end do
#else
!    Do i = 1, nCells
!      F_aux(cell2ghost(i,dir)) = F(i)
!    End Do

!   Assuming the above formulas in #ifdef are correct for #else

    N2N1 = Nlocal(2)*Nlocal(1)
    NH1NH2 = NwithHalo(1)*NwithHalo(2)
    do k = 1, Nlocal(3)
      offset_k = (k-1)*N2N1
      hoffset_k = (k-1+gOffset(3))*NH1NH2
      do j = 1, Nlocal(2)
        offset_jk = offset_k + (j-1)*Nlocal(1)
        hoffset_jk = hoffset_k + (j-1+gOffset(2))*NwithHalo(1) + gOffset(1)
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
        do i = 1, Nlocal(1)
          F_aux( i + hoffset_jk ) = F( i + offset_jk )
        end do
      end do
    end do

#endif

    ! ... collect Ghost cell data
    timer2 = MPI_WTIME()
    IF (ANY(nGhostRHS(dir,:) .GT. 0)) Call Ghost_Cell_Exchange(region, gridID, dir, F_aux)
    timings%operator_ghostExchange(order,dir) = timings%operator_ghostExchange(order,dir) + (MPI_WTIME() - timer2)

    ! ... hack to permit plane periodicity for metric derivative
    If (grid_diff .and. (grid_periodic(dir) == PLANE_PERIODIC)) Then
      Call Plane_Periodic_Metric_Hack(dir, grid, F_aux)
    End If

    ! ... compute interior derivatives
    timer2 = MPI_WTIME()
    Call Compute_Interior_Derivatives(grid, dir, order, F_aux, dF_aux)
    timings%operator_interior(order,dir) = timings%operator_interior(order,dir) + (MPI_WTIME() - timer2)

    ! ... matrix-vector multiply the rhs...only for boundary terms now
    timer2 = MPI_WTIME()
!    Call MV_Mult_Sparse_Simple(grid, dir, order, B(order,dir), F_aux, dF_aux)
    Call MV_Mult_Sparse_Simple_with_holes(grid, dir, order, B(order,dir), F_aux, dF_aux)
    timings%operator_boundary(order,dir) = timings%operator_boundary(order,dir) + (MPI_WTIME() - timer2)

    ! ... copy the non-ghost cell data
    ! ... fill in this grid's data
#ifdef USE_EXPERIMENTAL_ROUTINES
    do k = 1, Nlocal(3)
      do j = 1, Nlocal(2)
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
        do i = 1, Nlocal(1)

          ! ... local counting
          l0 = (k-1)*Nlocal(2)*Nlocal(1) + (j-1)*Nlocal(1) + i

          ! ... counting in halo coordinates 
          l1 = (k-1+gOffset(3))*NwithHalo(1)*NwithHalo(2) + (j-1+gOffset(2))*NwithHalo(1) + i+gOffset(1)

          ! ... fill the buffer
          dF(l0) = dF_aux(l1)

        end do
      end do
    end do
#else
!    Do i = 1, nCells
!      dF(i) = dF_aux(cell2ghost(i,dir))
!    End Do

!   Assuming the above formulas in #ifdef are correct for #else

    N2N1 = Nlocal(2)*Nlocal(1)
    NH1NH2 = NwithHalo(1)*NwithHalo(2)
    do k = 1, Nlocal(3)
      offset_k = (k-1)*N2N1
      hoffset_k = (k-1+gOffset(3))*NH1NH2
      do j = 1, Nlocal(2)
        offset_jk = offset_k + (j-1)*Nlocal(1)
        hoffset_jk = hoffset_k + (j-1+gOffset(2))*NwithHalo(1) + gOffset(1)
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
        do i = 1, Nlocal(1)
          dF( i + offset_jk ) = dF_aux( i + hoffset_jk )
        end do
      end do
    end do

#endif

    ! ... clean up
    deallocate(F_aux)
    deallocate(dF_aux)

    ! ... if explicit then return
    if (deriv_type(dir) == EXPLICIT) then
      timings%operator(order,dir) = timings%operator(order,dir) + (MPI_WTIME() - timer)
      return
    end if

#ifdef USE_HYPRE
    ! ... update HYPRE right-hand-side vector
    call fill_hypre_pade_implicit_rhs_vector(nGridsGlobal, iGridGlobal, gridID, nGrids, &
                                             myrank_inComm, numproc_inComm, comm, &
                                             ND, dir, order, grid_is, grid_ie, dF)

    ! ... initialize the HYPRE solution vector to be the RHS dF
    call fill_hypre_pade_implicit_soln_vector(nGridsGlobal, iGridGlobal, gridID, nGrids, &
                                              myrank_inComm, numproc_inComm, comm, &
                                              ND, dir, order, grid_is, grid_ie, dF)


    ! ... solve the HYPRE system using ILU preconditioning
    timer2 = MPI_WTime()
    call hypre_solve_pade_implicit(nGridsGlobal, iGridGlobal, gridID, nGrids, &
                                   myrank_inComm, numproc_inComm, comm, &
                                   ND, dir, order, grid_is, grid_ie, iterations)
    region%mpi_timings(gridID)%operator_solve(order,dir) = region%mpi_timings(gridID)%operator_solve(order,dir) &
                                                         + (MPI_WTIME() - timer2)

    if (myrank_inComm == 0) then
      if (iterations > 5) then
        write (*,'(A,I3,A)') 'PlasComCM: Warning: HYPRE-based derivative solve needed ', iterations, ' iterations.'
        write (*,'(A,I3,A)') 'PlasComCM: Consider changing ILU preconditioner settings.'
        write (*,'(A,I3)')   'PlasComCM: opID = ', order
      end if
    end if

    ! ... retreive the HYPRE solution
    call hypre_update_pade_implicit_soln_vector(nGridsGlobal, iGridGlobal, gridID, nGrids, &
                                                myrank_inComm, numproc_inComm, comm, &
                                                ND, dir, order, grid_is, grid_ie, dF)

    timings%operator(order,dir) = timings%operator(order,dir) + (MPI_WTIME() - timer)
#endif

    return

  end subroutine APPLY_OPERATOR

  subroutine APPLY_OPERATOR_WO_BC(region, gridID, order, dir, F, dF, grid_diff, coordDir_in)

    use ModGlobal
    use ModDataStruct
    USE ModMatrixVectorOps
    USE ModMPI

    implicit none

    type(t_grid), pointer :: grid
    type(t_region), pointer :: region
    type(t_mixt_input), pointer :: input
    type(t_mpi_timings), pointer :: timings
    integer :: dir, gridID, order
    real(rfreal), pointer :: F(:), dF(:)
    logical :: grid_diff
    integer, optional :: coordDir_in

    ! ... local variables
    integer :: i, j, k, ii, l0, l1, Nc, N, mpiDer, Ncg, Ng, gOffset(MAX_ND), iterations, iis, iie, ND
    integer :: is, ie, js, je, ks, ke, N1, N2, N3, NGhost, coordDir, Nlocal(MAX_ND), NwithHalo(MAX_ND)
    real(rfreal), pointer :: F_aux(:), dF_aux(:), dF_aux2(:,:)
    real(rfreal), pointer :: aloc(:,:), bloc(:,:), cloc(:,:), dloc(:,:), eloc(:,:)
    real(rfreal) :: fac, timer, timer2

    ! ... timings
    timer = MPI_WTIME()

    ! ... simplicity
    grid         => region%grid(gridID)
    input        => grid%input
    timings      => region%mpi_timings(gridID)
    ND           = grid%ND
    Nc           = grid%nCells
    N            = grid%ie(dir)-grid%is(dir)+1
    NGhost       = grid%nGhostRHS(dir,1) + grid%nGhostRHS(dir,2)
    Ncg          = Nc + NGhost*grid%vdsGhost(dir)
    Ng           = N + NGhost
    Nlocal(:)    = 1
    NwithHalo(:) = 1
    gOffset(:)   = 0
    gOffset(dir) = grid%nGhostRHS(dir,1)

    ! ... local sizes
    Nlocal(1:ND)    = grid%ie(1:ND) - grid%is(1:ND) + 1
    NwithHalo(1:ND) = Nlocal(1:ND)

    ! ... extend in the halo direction
    NwithHalo(dir) = Nlocal(dir) + NGhost

    ! ... check for coordDir argument
    if (present(coordDir_in) .eqv. .true.) then
      coordDir = coordDir_in
    else
      coordDir = 0
    end if

    ! ... temporary storage
    allocate(dF_aux(Ncg), F_aux(Ncg))
    Do i = 1, Ncg
      dF_aux(i) = 0.0_8
    End Do

    ! ... fill in this grid's data
#ifdef USE_EXPERIMENTAL_ROUTINES
    do k = 1, Nlocal(3)
      do j = 1, Nlocal(2)
        do i = 1, Nlocal(1)

          ! ... local counting
          l0 = (k-1)*Nlocal(2)*Nlocal(1) + (j-1)*Nlocal(1) + i

          ! ... counting in halo coordinates 
          l1 = (k-1+gOffset(3))*NwithHalo(1)*NwithHalo(2) + (j-1+gOffset(2))*NwithHalo(1) + i+gOffset(1)

          ! ... fill the buffer
          F_aux(l1) = F(l0)

        end do
      end do
    end do
#else
    Do i = 1, Nc
      F_aux(grid%cell2ghost(i,dir)) = F(i)
    End Do
#endif

    ! ... collect Ghost cell data
    timer2 = MPI_WTIME()
    IF (ANY(grid%nGhostRHS(dir,:) .GT. 0)) Call Ghost_Cell_Exchange(region, gridID, dir, F_aux)
    timings%operator_ghostExchange(order,dir) = timings%operator_ghostExchange(order,dir) + (MPI_WTIME() - timer2)

    ! ... hack to permit plane periodicity for metric derivative
    If (grid_diff .and. (grid%periodic(dir) == PLANE_PERIODIC)) Then
      Call Plane_Periodic_Metric_Hack(dir, grid, F_aux)
    End If

    ! ... compute interior derivatives
    timer2 = MPI_WTIME()
    Call Compute_Interior_Derivatives(grid, dir, order, F_aux, dF_aux)
    timings%operator_interior(order,dir) = timings%operator_interior(order,dir) + (MPI_WTIME() - timer2)

    ! ... matrix-vector multiply the rhs...only for boundary terms now
    timer2 = MPI_WTIME()
    Call MV_Mult_Sparse_ZERO(grid, dir, grid%B(order,dir), F_aux, dF_aux)
    timings%operator_boundary(order,dir) = timings%operator_boundary(order,dir) + (MPI_WTIME() - timer2)

    ! ... copy the non-ghost cell data
    ! ... fill in this grid's data
#ifdef USE_EXPERIMENTAL_ROUTINES
    do k = 1, Nlocal(3)
      do j = 1, Nlocal(2)
        do i = 1, Nlocal(1)

          ! ... local counting
          l0 = (k-1)*Nlocal(2)*Nlocal(1) + (j-1)*Nlocal(1) + i

          ! ... counting in halo coordinates 
          l1 = (k-1+gOffset(3))*NwithHalo(1)*NwithHalo(2) + (j-1+gOffset(2))*NwithHalo(1) + i+gOffset(1)

          ! ... fill the buffer
          dF(l0) = dF_aux(l1)

        end do
      end do
    end do
#else
    Do i = 1, Nc
      dF(i) = dF_aux(grid%cell2ghost(i,dir))
    End Do
#endif

    ! ... clean up
    deallocate(F_aux)
    deallocate(dF_aux)

    ! ... if explicit then return
    if (grid%deriv_type(dir) == EXPLICIT) then
      timings%operator(order,dir) = timings%operator(order,dir) + (MPI_WTIME() - timer)
      return
    end if

#ifdef USE_HYPRE
    ! ... update HYPRE right-hand-side vector
    call fill_hypre_pade_implicit_rhs_vector(region%nGridsGlobal, grid%iGridGlobal, gridID, region%nGrids, &
                                             grid%myrank_inComm, grid%numproc_inComm, grid%comm, &
                                             grid%ND, dir, order, grid%is, grid%ie, dF)

    ! ... initialize the HYPRE solution vector to be the RHS dF
    call fill_hypre_pade_implicit_soln_vector(region%nGridsGlobal, grid%iGridGlobal, gridID, region%nGrids, &
                                              grid%myrank_inComm, grid%numproc_inComm, grid%comm, &
                                              grid%ND, dir, order, grid%is, grid%ie, dF)


    ! ... solve the HYPRE system using ILU preconditioning
    timer2 = MPI_WTime()
    call hypre_solve_pade_implicit(region%nGridsGlobal, grid%iGridGlobal, gridID, region%nGrids, &
                                   grid%myrank_inComm, grid%numproc_inComm, grid%comm, &
                                   grid%ND, dir, order, grid%is, grid%ie, iterations)
    region%mpi_timings(gridID)%operator_solve(order,dir) = region%mpi_timings(gridID)%operator_solve(order,dir) &
                                                         + (MPI_WTIME() - timer2)

    if (grid%myrank_inComm == 0) then
      if (iterations > 5) then
        write (*,'(A,I3,A)') 'PlasComCM: Warning: HYPRE-based derivative solve needed ', iterations, ' iterations.'
        write (*,'(A,I3,A)') 'PlasComCM: Consider changing ILU preconditioner settings.'
        write (*,'(A,I3)')   'PlasComCM: opID = ', order
      end if
    end if

    ! ... retreive the HYPRE solution
    call hypre_update_pade_implicit_soln_vector(region%nGridsGlobal, grid%iGridGlobal, gridID, region%nGrids, &
                                                grid%myrank_inComm, grid%numproc_inComm, grid%comm, &
                                                grid%ND, dir, order, grid%is, grid%ie, dF)

    timings%operator(order,dir) = timings%operator(order,dir) + (MPI_WTIME() - timer)
#endif

    return

  end subroutine APPLY_OPERATOR_WO_BC

  subroutine KERNEL(region, gridID, l, p, A, u, dflux_test)

    use ModGlobal
    use ModDataStruct
    USE ModMatrixVectorOps
    USE ModMPI

    implicit none

    type(t_grid), pointer :: grid
    type(t_region), pointer :: region
    REAL(rfreal) :: A(:), u(:)
    REAL(rfreal), POINTER :: dflux_test(:), A_aux(:), u_aux(:)
    type(t_mixt_input), pointer :: input
    type(t_mpi_timings), pointer :: timings
    integer :: dir, gridID, order
    real(rfreal), pointer :: F(:), dF(:)
    logical :: grid_diff
  !  integer, optional :: coordDir_in

    ! ... local variables
    integer :: i, j, k, ii, l0, l1, Nc, N, mpiDer, Ncg, Ng, gOffset(MAX_ND), iterations, iis, iie, ND
    integer :: is, ie, js, je, ks, ke, N1, N2, N3, NGhost, coordDir, Nlocal(MAX_ND), NwithHalo(MAX_ND)
    real(rfreal), pointer :: F_aux(:), dF_aux(:), dF_aux2(:,:)
    real(rfreal), pointer :: aloc(:,:), bloc(:,:), cloc(:,:), dloc(:,:), eloc(:,:)
    real(rfreal) :: fac, timer, timer2
    integer :: l, p
    ! ... timings
    timer = MPI_WTIME()


    IF(l == p) THEN
    ! ... simplicity
    grid         => region%grid(gridID)
    input        => grid%input
    timings      => region%mpi_timings(gridID)
    ND           = grid%ND
    Nc           = grid%nCells
    N            = grid%ie(p)-grid%is(p)+1
    NGhost       = grid%nGhostRHS(p,1) + grid%nGhostRHS(p,2)
    Ncg          = Nc + NGhost*grid%vdsGhost(p)
    Ng           = N + NGhost
    Nlocal(:)    = 1
    NwithHalo(:) = 1
    gOffset(:)   = 0
    gOffset(p) = grid%nGhostRHS(p,1)

    ! ... local sizes
    Nlocal(1:ND)    = grid%ie(1:ND) - grid%is(1:ND) + 1
    NwithHalo(1:ND) = Nlocal(1:ND)

    ! ... extend in the halo direction
    NwithHalo(p) = Nlocal(p) + NGhost

    ! ... check for coordDir argument
!    if (present(coordDir_in) .eqv. .true.) then
!      coordDir = coordDir_in
!    else
!      coordDir = 0
!    end if
 

allocate(dF_aux(Ncg), A_aux(Ncg), u_aux(Ncg)) !... Extended array

       Do i = 1, Ncg
          dF_aux(i) = 0.0d0
          A_aux(i) = 0.0d0
          u_aux(i) = 0.0d0
       End Do

    ! ... fill in this grid's data
#ifdef USE_EXPERIMENTAL_ROUTINES
    do k = 1, Nlocal(3)
      do j = 1, Nlocal(2)
        do i = 1, Nlocal(1)

          ! ... local counting
          l0 = (k-1)*Nlocal(2)*Nlocal(1) + (j-1)*Nlocal(1) + i

          ! ... counting in halo coordinates 
          l1 = (k-1+gOffset(3))*NwithHalo(1)*NwithHalo(2) + (j-1+gOffset(2))*NwithHalo(1) + i+gOffset(1)

          ! ... fill the buffer
          A_aux(l1) = A(l0)
	  u_aux(l1) = u(l0)

        end do
      end do
    end do
#else
    Do i = 1, Nc
      A_aux(grid%cell2ghost(i,p)) = A(i)
      u_aux(grid%cell2ghost(i,p)) = u(i)
    End Do
#endif

    ! ... collect Ghost cell data
    timer2 = MPI_WTIME()
    IF (ANY(grid%nGhostRHS(p,:) .GT. 0)) THEN
      Call Ghost_Cell_Exchange(region, gridID, p, A_aux)
      Call Ghost_Cell_Exchange(region, gridID, p, u_aux) ! ... for velocity profile
    END IF
!    ! ... hack to permit plane periodicity for metric derivative
!    If (grid_diff .and. (grid%periodic(dir) == PLANE_PERIODIC)) Then
!      Call Plane_Periodic_Metric_Hack(dir, grid, F_aux)
!    End If

    ! ... compute interior derivatives
    timer2 = MPI_WTIME()
    Call Compute_Interior_Derivatives_Bilinear(grid, p, A_aux, u_aux, dF_aux)

    ! ... matrix-vector multiply the rhs...only for boundary terms now
    timer2 = MPI_WTIME()
    Call MV_Mult_Sparse_ZERO(grid, p, grid%B(1,p), F_aux, dF_aux)
  

    ! ... copy the non-ghost cell data
    ! ... fill in this grid's data
#ifdef USE_EXPERIMENTAL_ROUTINES
    do k = 1, Nlocal(3)
      do j = 1, Nlocal(2)
        do i = 1, Nlocal(1)

          ! ... local counting
          l0 = (k-1)*Nlocal(2)*Nlocal(1) + (j-1)*Nlocal(1) + i

          ! ... counting in halo coordinates 
          l1 = (k-1+gOffset(3))*NwithHalo(1)*NwithHalo(2) + (j-1+gOffset(2))*NwithHalo(1) + i+gOffset(1)

          ! ... fill the buffer
          dflux_test(l0) = dF_aux(l1)

        end do
      end do
    end do
#else
    Do i = 1, Nc
      dflux_test(i) = dF_aux(grid%cell2ghost(i,p))
    End Do
#endif

    ! ... clean up

    deallocate(dF_aux, A_aux, u_aux)

    ELSE
    ! ... cross terms
    call graceful_exit(region%myrank, 'cross is not supported')
        
    END IF
    return
  END SUBROUTINE kernel

  Subroutine Compute_Interior_Derivatives_Bilinear(grid, dir, F, F2, dF)
    use ModGlobal
    use ModDataStruct
    USE ModMPI

    implicit none

    type(t_grid), pointer :: grid
    type(t_mixt_input), pointer :: input
    integer :: dir, order
    real(rfreal), pointer :: F(:), dF(:), F2(:), dF2(:)

    ! ... local variables
    integer :: i, j, k, ii, l0, l1, l2, Nc, N, mpiDer, Ncg, Ng, gOffset(2), iterations, iis, iie
    integer :: is, ie, js, je, ks, ke, N1, N2, N3, NGhost, N1g, N2g, N3g
    real(rfreal) :: fac, timer, M_val(-2:2)

    ! ... timings
    timer = MPI_WTIME()

    ! ... simplicity
    input      => grid%input
    Nc         = grid%nCells
    N          = grid%ie(dir)-grid%is(dir)+1
    goffset(1) = grid%nGhostRHS(dir,1)
    goffset(2) = grid%nGhostRHS(dir,2)
    NGhost     = gOffset(1) + gOffset(2)
    Ncg        = Nc + NGhost*grid%vdsGhost(dir)
    Ng         =  N + NGhost
    N1         = grid%ie(1) - grid%is(1) + 1
    N2         = grid%ie(2) - grid%is(2) + 1
    N3         = grid%ie(3) - grid%is(3) + 1
    N1g        = N1 + Nghost
    N2g        = N2 + Nghost
    N3g        = N3 + Nghost

    ! ... size of interior stencil
    iis        = -2 
    iie        = 2

  
    ! ... determine starting/stopping indices 
    is = 1; ie = N1
    js = 1; je = N2
    ks = 1; ke = N3

    If (gOffset(1) == 0) Then
      If (dir == 1) Then
        is =  1 - iis
      Else If (dir == 2) Then
        js =  1 - iis
      Else If (dir == 3) Then
        ks =  1 - iis
      End If
    End If

    If (gOffset(2) == 0) Then
      If (dir == 1) Then
        ie = N1 - iie
      Else If (dir == 2) Then
        je = N2 - iie
      Else If (dir == 3) Then
        ke = N3 - iie
      End If
    End If
    ! ... compute derivatives
    Select Case (dir)
    Case (1)
       is = is + gOffset(1)
       ie = ie + gOffset(1)
       do k = ks, ke
          do j = js, je
             do i = is, ie
                l0 = ( (k-1)*N2 + (j-1) ) * N1g + i
		M_val(-2) =  - 1.0d0 /  8.0d0 * F(l0-2) &
			     + 1.0d0 /  6.0d0 * F(l0-1) &
			     - 1.0d0 /  8.0d0 * F(l0);
		M_val(-1) =  + 1.0d0 /  6.0d0 * F(l0-2) &
			     + 1.0d0 /  2.0d0 * F(l0-1) &
			     + 1.0d0 /  2.0d0 * F(l0)   &
			     + 1.0d0 /  6.0d0 * F(l0+1);
		M_val( 0) =  - 1.0d0 / 24.0d0 * F(l0-2) &
		             - 5.0d0 /  6.0d0 * F(l0-1) &
			     - 3.0d0 /  4.0d0 * F(l0)   &
			     - 5.0d0 /  6.0d0 * F(l0+1) &
			     - 1.0d0 / 24.0d0 * F(l0+2);
		M_val( 1) =  + 1.0d0 /  6.0d0 * F(l0-1) &
		       	     + 1.0d0 /  2.0d0 * F(l0)   &
			     + 1.0d0 /  2.0d0 * F(l0+1) &
			     + 1.0d0 /  6.0d0 * F(l0+2) ;
               M_val( 2) =   - 1.0d0 /  8.0d0 * F(l0)   &
	       	      	     + 1.0d0 /  6.0d0 * F(l0+1) &
			     - 1.0d0 /  8.0d0 * F(l0+2);
                do ii = iis, iie
                   l1 = l0 + ii
                      dF(l0) = dF(l0) + M_val(ii) * F2(l1)
                end do
             end do
          end do
       end do

    Case (2)

       js = js + gOffset(1)
       je = je + gOffset(1)
       do k = ks, ke
          do i = is, ie
             do j = js, je
                l0 = ( (k-1)*N2g + (j-1) ) * N1 + i
		M_val(-2) = - 1.0d0 /  8.0d0 * F(l0-2 * N1) &
			    + 1.0d0 /  6.0d0 * F(l0-1 * N1) &
			    - 1.0d0 /  8.0d0 * F(l0)   	    ;
		M_val(-1) = + 1.0d0 /  6.0d0 * F(l0-2 * N1) &
			    + 1.0d0 /  2.0d0 * F(l0-1 * N1) &
			    + 1.0d0 /  2.0d0 * F(l0)  	    &
			    + 1.0d0 /  6.0d0 * F(l0+1 * N1) ;
		M_val( 0) = - 1.0d0 / 24.0d0 * F(l0-2 * N1) &
		       	    - 5.0d0 /  6.0d0 * F(l0-1 * N1) &
			    - 3.0d0 /  4.0d0 * F(l0)  	    &
			    - 5.0d0 /  6.0d0 * F(l0+1 * N1) &
			    - 1.0d0 / 24.0d0 * F(l0+2 * N1) ;
		M_val( 1) = + 1.0d0 /  6.0d0 * F(l0-1 * N1) &
		       	    + 1.0d0 /  2.0d0 * F(l0)  	    &
			    + 1.0d0 /  2.0d0 * F(l0+1 * N1) &
			    + 1.0d0 /  6.0d0 * F(l0+2 * N1) ; 
		M_val( 2) = - 1.0d0 /  8.0d0 * F(l0)  	    &
		       	    + 1.0d0 /  6.0d0 * F(l0+1 * N1) &
			    - 1.0d0 /  8.0d0 * F(l0+2 * N1)


                do ii = iis, iie
                   l1 = l0 + ii * N1
                      dF(l0) = dF(l0) + M_val(ii) * F2(l1) 
                end do
             end do
          end do
       end do
    Case (3)
       ks = ks + gOffset(1)
       ke = ke + gOffset(1)
       do j = js, je
          do i = is, ie
             do k = ks, ke
                l0 = ( (k-1)*N2 + (j-1) ) * N1 + i
		M_val(-2) = - 1.0d0 /  8.0d0 * F(l0-2 * N1 * N2) &
			    + 1.0d0 /  6.0d0 * F(l0-1 * N1 * N2) &
			    - 1.0d0 /  8.0d0 * F(l0)  	     	 ;
		M_val(-1) = + 1.0d0 /  6.0d0 * F(l0-2 * N1 * N2) &
			    + 1.0d0 /  2.0d0 * F(l0-1 * N1 * N2) &
			    + 1.0d0 /  2.0d0 * F(l0)  	     	 &
			    + 1.0d0 /  6.0d0 * F(l0+1 * N1 * N2) ;
		M_val( 0) = - 1.0d0 / 24.0d0 * F(l0-2 * N1 * N2) &
		       	    - 5.0d0 /  6.0d0 * F(l0-1 * N1 * N2) &
			    - 3.0d0 /  4.0d0 * F(l0)  	     	 &
			    - 5.0d0 /  6.0d0 * F(l0+1 * N1 * N2) &
			    - 1.0d0 / 24.0d0 * F(l0+2 * N1 * N2) ;
		M_val( 1) = + 1.0d0 /  6.0d0 * F(l0-1 * N1 * N2) &
		       	    + 1.0d0 /  2.0d0 * F(l0)  	     	 &
			    + 1.0d0 /  2.0d0 * F(l0+1 * N1 * N2) &
			    + 1.0d0 /  6.0d0 * F(l0+2 * N1 * N2) ; 
		M_val( 2) = - 1.0d0 /  8.0d0 * F(l0)  	     	 &
		       	    + 1.0d0 /  6.0d0 * F(l0+1 * N1 * N2) &
			    - 1.0d0 /  8.0d0 * F(l0+2 * N1 * N2)

                do ii = iis, iie
                   l1 = l0 + ii * N2 * N1
                      dF(l0) = dF(l0) + M_val(ii) * F2(l1)
                end do
             end do
          end do
       end do
    End Select

  End Subroutine Compute_Interior_Derivatives_Bilinear

  Subroutine Compute_Interior_Derivatives(grid, dir, order, F, dF)

    use ModGlobal
    use ModDataStruct
    USE ModMPI

    implicit none

    type(t_grid), pointer :: grid
    type(t_mixt_input), pointer :: input
    integer :: dir, order
    real(rfreal), pointer :: F(:), dF(:), F2(:), dF2(:)

    ! ... local variables
    integer :: i, j, k, ii, l0, l1, l2, Nc, N, mpiDer, Ncg, Ng, gOffset(2), iterations, iis, iie
    integer :: is, ie, js, je, ks, ke, N1, N2, N3, NGhost, N1g, N2g, N3g
    real(rfreal) :: fac, timer, vals(-MAX_INTERIOR_STENCIL_WIDTH:MAX_INTERIOR_STENCIL_WIDTH)

    ! ... declarations for stride-1 computations, WZhang 06/2014
    integer :: slice_k, offset_k, offset_kj, offset_kjii
    real(rfreal) :: value

    ! ... declarations for pointer dereference, WZhang 05/2014
    integer, pointer :: rhs_interior_stencil_start(:), rhs_interior_stencil_end(:)
    real(rfreal), pointer :: rhs_interior(:,:)

    ! ... timings
    timer = MPI_WTIME()

    ! ... simplicity
    input      => grid%input
    Nc         = grid%nCells
    N          = grid%ie(dir)-grid%is(dir)+1
    goffset(1) = grid%nGhostRHS(dir,1)
    goffset(2) = grid%nGhostRHS(dir,2)
    NGhost     = gOffset(1) + gOffset(2)
    Ncg        = Nc + NGhost*grid%vdsGhost(dir)
    Ng         =  N + NGhost
    N1         = grid%ie(1) - grid%is(1) + 1
    N2         = grid%ie(2) - grid%is(2) + 1
    N3         = grid%ie(3) - grid%is(3) + 1
    N1g        = N1 + Nghost
    N2g        = N2 + Nghost
    N3g        = N3 + Nghost

    ! ... pointer dereference, WZhang 05/2014
    rhs_interior_stencil_start => input%rhs_interior_stencil_start
    rhs_interior_stencil_end   => input%rhs_interior_stencil_end
    rhs_interior               => input%rhs_interior

    ! ... size of interior stencil
    iis        = rhs_interior_stencil_start(order) 
    iie        = rhs_interior_stencil_end(order) 

    ! ... store values of interior stencil
    do ii = iis, iie
      vals(ii) = rhs_interior(order,ii)
    end do

    ! ... determine starting/stopping indices 
    is = 1; ie = N1
    js = 1; je = N2
    ks = 1; ke = N3

    If (gOffset(1) == 0) Then
      If (dir == 1) Then
        is =  1 - iis
      Else If (dir == 2) Then
        js =  1 - iis
      Else If (dir == 3) Then
        ks =  1 - iis
      End If
    End If

    If (gOffset(2) == 0) Then
      If (dir == 1) Then
        ie = N1 - iie
      Else If (dir == 2) Then
        je = N2 - iie
      Else If (dir == 3) Then
        ke = N3 - iie
      End If
    End If

!!$    ! ... swap order of dF for speed
!!$    If (dir == 2) Then
!!$      allocate(dF2(N1*N2g*N3))
!!$      allocate(F2(N1*N2g*N3))
!!$      Do k = 1, N3
!!$        Do j = 1, N2g
!!$          Do i = 1, N1
!!$            l0  = ( (k-1)*N2g + (j-1) ) * N1 + i
!!$            l2  = ( (k-1)*N1 + (i-1) ) * N2g + j
!!$            dF2(l2) = 0.0_8
!!$             F2(l2) = F(l0)
!!$          End Do
!!$        End Do
!!$      End Do
!!$    End If
!!$
!!$    ! ... compute derivatives
!!$    Select Case (dir)
!!$      Case (1)
!!$        do k = ks, ke
!!$          do j = js, je
!!$            do i = is, ie
!!$              l0 = ( (k-1)*N2+(j-1) )*(N1+NGhost) + i + gOffset(1)
!!$              do ii = iis, iie
!!$                l1 = ( (k-1)*N2 + (j-1) ) * (N1+NGhost) + i + gOffset(1) + ii
!!$                dF(l0) = dF(l0) + vals(ii) * F(l1)
!!$              end do
!!$            end do
!!$          end do
!!$        end do
!!$      Case (2)
!!$        do k = ks, ke
!!$          do i = is, ie
!!$            do j = js, je
!!$              l0 = ( (k-1)*N1 + (i-1) ) * N2g + (j + gOffset(1))
!!$              do ii = iis, iie
!!$                l1 = ( (k-1)*N1 + (i-1) ) * N2g + (j + gOffset(1) + ii)
!!$                dF2(l0) = dF2(l0) + vals(ii) * F2(l1)
!!$              end do
!!$            end do
!!$          end do
!!$        end do
!!$      Case (3)
!!$        do j = js, je
!!$          do i = is, ie
!!$            do k = ks, ke
!!$              l0 = ( (k+gOffset(1)-1)*N2+(j-1) )*N1 + i
!!$              do ii = iis, iie
!!$                l1 = ( (k+gOffset(1)+ii-1)*N2 + (j-1) ) * N1 + i
!!$                dF(l0) = dF(l0) + vals(ii) * F(l1)
!!$              end do
!!$            end do
!!$          end do
!!$        end do
!!$    End Select  
!!$
!!$    ! ... swap back order of dF for speed
!!$    If (dir == 2) Then
!!$      Do k = 1, N3
!!$        Do j = 1, N2g
!!$          Do i = 1, N1
!!$            l0  = ( (k-1)*N2g + (j-1) ) * N1 + i
!!$            l2  = ( (k-1)*N1 + (i-1) ) * N2g + j
!!$            dF(l0) = dF2(l2)
!!$          End Do
!!$        End Do
!!$      End Do
!!$      deallocate(dF2,F2)
!!$    End If  

    ! ... compute derivatives (stride-1 version), WZhang 06/2014
    Select Case (dir)
      Case (1)
        is = is + gOffset(1)
        ie = ie + gOffset(1)
        slice_k = N2 * N1g
        do k = ks, ke
          offset_k = (k-1) * slice_k
          do j = js, je
            offset_kj = offset_k + (j-1) * N1g
            do ii = iis, iie
              value = vals(ii)
              offset_kjii = offset_kj + ii
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
              do i = is, ie
                dF(i+offset_kj) = dF(i+offset_kj) + value * F(i+offset_kjii)
              end do
            end do
          end do
        end do
      Case (2)
        js = js + gOffset(1)
        je = je + gOffset(1)
        slice_k = N2g * N1
        do k = ks, ke
          offset_k = (k-1) * slice_k
          do j = js, je
            offset_kj = offset_k + (j-1) * N1
            do ii = iis, iie
              value = vals(ii)
              offset_kjii = offset_kj + ii * N1
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
              do i = is, ie
                dF(i+offset_kj) = dF(i+offset_kj) + value * F(i+offset_kjii)
              end do
            end do
          end do
        end do
      Case (3)
        ks = ks + gOffset(1)
        ke = ke + gOffset(1)
        slice_k = N2 * N1
        do k = ks, ke
          offset_k = (k-1) * slice_k
          do j = js, je
            offset_kj = offset_k + (j-1) * N1
            do ii = iis, iie
              value = vals(ii)
              offset_kjii = offset_kj + ii * slice_k
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
              do i = is, ie
                dF(i+offset_kj) = dF(i+offset_kj) + value * F(i+offset_kjii)
              end do
            end do
          end do
        end do
    End Select    

  End Subroutine Compute_Interior_Derivatives


  Subroutine Plane_Periodic_Metric_Hack_NASA(dir, grid, F, coordDir)

    USE ModGlobal
    USE ModDataStruct
    USE ModMatrixVectorOps
    USE ModMPI

    implicit none

    integer :: dir, coordDir
    type(t_grid), pointer :: grid
    real(rfreal), pointer :: F(:)

    ! ... local variables
    integer :: i, j, N, l0, lg(2)
    real(rfreal) :: f1, fN, L, radius(2), theta_e(2), dtheta, xx(2), yy(2)
    type(t_matrix_sparse), pointer :: B
    logical :: adjust(2)

    ! ... size of direction dir
    N = grid%B(1,dir)%n

    ! ... periodic length of domain in direction dir
    L = grid%periodicL(dir)

    adjust = .TRUE.
    If (grid%cartDims(dir) > 1) Then
      adjust = .FALSE.
      If (grid%cartCoords(dir) == 0)                    adjust(1) = .TRUE.
      If (grid%cartCoords(dir) == grid%cartDims(dir)-1) adjust(2) = .TRUE.
    End If

    ! ... augment F with its periodic version
    if (adjust(2) .eqv. .true.) Then
      do j = 1, grid%vdsGhost(dir)
        lg(1) = grid%vec_ind(j,N-grid%nGhostRHS(dir,2)-grid%nGhostRHS(dir,1)-0,dir)
        lg(2) = grid%vec_ind(j,N-grid%nGhostRHS(dir,2)-grid%nGhostRHS(dir,1)-1,dir)
        xx(:) = grid%XYZ(lg(:),1)
        yy(:) = grid%XYZ(lg(:),2)
        radius(:) = dsqrt(xx(:)*xx(:)+yy(:)*yy(:))
        theta_e(:) = datan2(yy(:),xx(:))
        dtheta = dabs(theta_e(2) - theta_e(1))
        do i = 0, grid%nGhostRHS(dir,2)-1
          l0 = grid%vec_indGhost(j,N-i,dir)
          if (coordDir == 1) then
            L = radius(1) * dcos(theta_e(1) - dble(grid%nGhostRHS(dir,2)-i+1) * dtheta)
          else if (coordDir == 2) then
            L = radius(1) * dsin(theta_e(1) - dble(grid%nGhostRHS(dir,2)-i+1) * dtheta)
          end if
          F(l0) = L
        end do
      end do
    end if

    if (adjust(1) .eqv. .true.) Then
      do j = 1, grid%vdsGhost(dir)
        lg(1) = grid%vec_ind(j,1,dir)
        lg(2) = grid%vec_ind(j,2,dir)
        xx(:) = grid%XYZ(lg(:),1)
        yy(:) = grid%XYZ(lg(:),2)
        radius(:) = dsqrt(xx(:)*xx(:)+yy(:)*yy(:))
        theta_e(:) = datan2(yy(:),xx(:))
        dtheta = dabs(theta_e(2) - theta_e(1)) 
        do i = 1, grid%nGhostRHS(dir,1), 1
          l0 = grid%vec_indGhost(j,i,dir)
          if (coordDir == 1) then
            L = radius(1) * dcos(theta_e(1) + dble(grid%nGhostRHS(dir,1)-i+1) * dtheta)
          else if (coordDir == 2) then
            L = radius(1) * dsin(theta_e(1) + dble(grid%nGhostRHS(dir,1)-i+1) * dtheta)
          end if
          F(l0) = L
        end do
      end do
    end if

    Return

  End Subroutine Plane_Periodic_Metric_Hack_NASA
 
  subroutine APPLY_OPERATOR_box(region, gridID, order, dir, F, dF, grid_diff, exchange_flag, coordDir_in)

    use ModGlobal
    use ModDataStruct
    USE ModMatrixVectorOps
    USE ModMPI

    implicit none

    type(t_grid), pointer :: grid
    type(t_region), pointer :: region
    type(t_mixt_input), pointer :: input
    type(t_mpi_timings), pointer :: timings
    integer :: dir, gridID, order
    real(rfreal), pointer :: F(:), dF(:)
    logical :: grid_diff
    integer :: exchange_flag
    integer, optional :: coordDir_in

    ! ... local variables
    integer :: i, j, k, ii, l0, l1, iis, iie, N1, N2, N3, G1, G2, G3, N1g, N2g, N3g
    integer :: is, ie, js, je, ks, ke, is_in, ie_in, js_in, je_in, ks_in, ke_in
    integer :: ilc, jlc, klc
    integer :: Nc
    integer, pointer :: grid_periodic(:), deriv_type(:)
    real(rfreal) :: timer
    real(rfreal) :: vals(-MAX_INTERIOR_STENCIL_WIDTH:MAX_INTERIOR_STENCIL_WIDTH)
    real(rfreal), pointer :: ptr_to_data(:)

    ! - edge information of box shape data struct ... WZhang, 10/2014
    INTEGER, POINTER      :: l_ctr_lc_b(:,:), r_ctr_lc_b(:,:)
    ! ... global index of locally detected edges
    INTEGER, POINTER      :: l_bndry_lc_b(:,:,:), r_bndry_lc_b(:,:,:)
    ! ... whether boundary points extended from neighboring processor
    INTEGER, POINTER      :: l_extended_b(:,:), r_extended_b(:,:)
    ! ... number of boundary points extended from neighboring processor
    INTEGER, POINTER      :: l_extended_size_b(:,:), r_extended_size_b(:,:)

    ! ... for boundary stencils
    integer :: bndry_width, bndry_depth_local, bndry_depth_max, bndry_interior
    integer :: bndry_affected, dist, bndry_location, bndry_diff
    integer :: ctr, ba, offset
    real(RFREAL) :: rhs_block1(12,20), rhs_block2(12,20)

    ! ... serial optimizations, WZhang 02/2015
    integer :: slice_k, offset_c, offset_ck, offset_ckj, offset_ckjii, offset_ckjb
    integer :: offset_cki, offset_ckib
    integer :: offset_cji, offset_cjib
    integer :: offset_dist
    integer :: bndry_extent
    real(rfreal) :: value
    integer, pointer :: nGhostRHS(:,:)
    integer :: grid_is(MAX_ND), grid_ie(MAX_ND), grid_is_unique(MAX_ND), grid_ie_unique(MAX_ND)
    integer :: rhs_block_depth, l0_dist

    ! ... timings
    timer = MPI_WTIME()

    ! ... pointer dereference
    grid         => region%grid(gridID)
    input        => grid%input
    timings      => region%mpi_timings(gridID)
    grid_periodic => grid%periodic
    deriv_type   => grid%deriv_type
    
    l_ctr_lc_b   => grid%l_ctr_lc_b(:,:,dir)
    r_ctr_lc_b   => grid%r_ctr_lc_b(:,:,dir)
    l_bndry_lc_b => grid%l_bndry_lc_b(:,:,:,dir)
    r_bndry_lc_b => grid%r_bndry_lc_b(:,:,:,dir)
    l_extended_b => grid%l_extended_b(:,:,dir)
    r_extended_b => grid%r_extended_b(:,:,dir)
    l_extended_size_b => grid%l_extended_size_b(:,:,dir)
    r_extended_size_b => grid%r_extended_size_b(:,:,dir)

    nGhostRHS => grid%nGhostRHS
    grid_is(:) = grid%is(:)
    grid_ie(:) = grid%ie(:)
    grid_is_unique(:) = grid%is_unique(:)
    grid_ie_unique(:) = grid%ie_unique(:)

    N1         = grid_ie_unique(1) - grid_is_unique(1) + 1
    N2         = grid_ie_unique(2) - grid_is_unique(2) + 1
    N3         = grid_ie_unique(3) - grid_is_unique(3) + 1
    G1         = nGhostRHS(1,1) + nGhostRHS(1,2)
    G2         = nGhostRHS(2,1) + nGhostRHS(2,2)
    G3         = nGhostRHS(3,1) + nGhostRHS(3,2)
    N1g        = N1 + G1
    N2g        = N2 + G2
    N3g        = N3 + G3

    Nc         = grid%nCells
    slice_k    = N1g*N2g

   ! ... size of interior stencil
    iis        = input%rhs_interior_stencil_start(order) 
    iie        = input%rhs_interior_stencil_end(order) 

    ! ... store values of interior stencil
    do ii = iis, iie
      vals(ii) = input%rhs_interior(order,ii)
    end do

    ! ... store values of the boundary stencil
    do j = 1, 20
      do i = 1, 12
        rhs_block1(i,j) = input%rhs_block1(order,j,i)
        rhs_block2(i,j) = input%rhs_block2(order,j,i)
      end do
    end do

    ! ... for boundary stencils
    bndry_width = input%operator_bndry_width(order)
    bndry_depth_local = input%operator_bndry_depth(order)
    bndry_depth_max =   maxval(input%operator_bndry_depth(:))
    bndry_interior = bndry_depth_max - bndry_depth_local
    bndry_diff = bndry_width - bndry_depth_local

    Select Case (dir)
      Case (1)
        dist = 1
      Case (2)
        dist = N1g
      Case (3)
        dist = N1g * N2g
    End Select

!    ! ... check for coordDir argument
!    if (present(coordDir_in) .eqv. .true.) then
!      coordDir = coordDir_in
!    else
!      coordDir = 0
!    end if

    ! ... hack to permit plane periodicity for metric derivative
    If (grid_diff .and. (grid_periodic(dir) == PLANE_PERIODIC)) Then
      Call Plane_Periodic_Metric_Hack_Box(dir, grid, F)
    End If

! ... this block will replace CID and Sparse_Simple, WZhang 10/2014
! ------------------------------------------------------------------------------------
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
    Do i = 1, Nc
      dF(i) = 0.0_8
    End Do

    Select Case (dir)
      Case (1)
        ks = grid_is_unique(3) - nGhostRHS(3,1)
        ke = grid_ie_unique(3) + nGhostRHS(3,2)
        js = grid_is_unique(2) - nGhostRHS(2,1)
        je = grid_ie_unique(2) + nGhostRHS(2,2)
        is = grid_is_unique(1)
        ie = grid_ie_unique(1)

        ! ... interior part
        if (nGhostRHS(1,1) == 0) then
          is_in = is - iis
        else
          is_in = is
        end if
        if (nGhostRHS(1,2) == 0) then
          ie_in = ie - iie
        else
          ie_in = ie
        end if
        offset_c = (-ks)*slice_k + (-js)*N1g + (-is+nGhostRHS(1,1)) + 1
        do k = ks, ke
          offset_ck = offset_c + k*slice_k
          do j = js, je
            offset_ckj = offset_ck + j*N1g
            do ii = iis, iie
              offset_ckjii = offset_ckj + ii
              value = vals(ii)
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
              do i = is_in, ie_in
                dF(i+offset_ckj) = dF(i+offset_ckj) + value * F(i+offset_ckjii)
              end do
            end do
          end do
        end do

        ! ... boundary part
        do k = ks, ke
          do j = js, je

            ! ... local indices on this face
            jlc = j - js + 1
            klc = k - ks + 1

            ! ... "regular" left edge
            if (l_ctr_lc_b(jlc,klc) > 0) then
              offset_ckj = (k-ks)*slice_k + (j-js)*N1g + (-is+nGhostRHS(1,1))
              do ctr = 1, l_ctr_lc_b(jlc,klc)
                bndry_location = l_bndry_lc_b(ctr,jlc,klc)
                offset_ckjb = offset_ckj + bndry_location
                bndry_extent = ie - bndry_location + 1
                if (bndry_extent < bndry_depth_max) then
                  bndry_affected = bndry_extent
                else
                  bndry_affected = bndry_depth_max
                end if
                do ba = 1, bndry_affected
                  l0 = ba + offset_ckjb
                  dF(l0) = 0.0_8
                  if (ba + bndry_interior > bndry_depth_max) then
                    do ii = iis, iie
                      dF(l0) = dF(l0) + vals(ii) * F(l0+ii*dist)
                    end do  ! ... i
                  else
                    rhs_block_depth = ba
                    offset_dist = (-rhs_block_depth)*dist
                    l0_dist = l0 + offset_dist
                    do ii = 1, bndry_width
                      dF(l0) = dF(l0) + rhs_block1(ii,rhs_block_depth) * F(l0_dist+ii*dist)
                    end do  ! ... i
                  end if
                end do  ! ... ba
              end do  ! ... ctr  
            end if ! ... "regular" left edge

            ! ... "extended" left edge
            if (l_extended_b(jlc,klc) > 0) then
              bndry_location = grid_is_unique(1)
              bndry_affected = l_extended_size_b(jlc,klc)
              offset = bndry_depth_max - bndry_affected
              offset_ckjb = (k-ks)*slice_k + (j-js)*N1g + (bndry_location-is+nGhostRHS(1,1))

              do ba = 1, bndry_affected
                l0 = ba + offset_ckjb
                dF(l0) = 0.0_8
                if (ba + offset + bndry_interior > bndry_depth_max) then
                  do ii = iis, iie
                    dF(l0) = dF(l0) + vals(ii) * F(l0+ii*dist)
                  end do  ! ... i
                else
                  rhs_block_depth = ba + offset
                  offset_dist = (-rhs_block_depth)*dist
                  l0_dist = l0 + offset_dist
                  do ii = 1, bndry_width
                    dF(l0) = dF(l0) + rhs_block1(ii,rhs_block_depth) * F(l0_dist+ii*dist)
                  end do ! ... i
                end if
              end do ! ... ba
            end if ! ... "extended" left edge

            ! ... "regular" right edge
            if (r_ctr_lc_b(jlc,klc) > 0) then
              offset_ckj = (k-ks)*slice_k + (j-js)*N1g + (-is+nGhostRHS(1,1)) + 1
              do ctr = 1, r_ctr_lc_b(jlc,klc)
                bndry_location = r_bndry_lc_b(ctr,jlc,klc)
                bndry_extent = bndry_location - is + 1
                if (bndry_extent < bndry_depth_max) then
                  bndry_affected = bndry_extent
                else
                  bndry_affected = bndry_depth_max
                end if
                offset = bndry_depth_max - bndry_affected
                offset_ckjb = offset_ckj + bndry_location - bndry_affected
                do ba = 1, bndry_affected
                  l0 = ba + offset_ckjb
                  dF(l0) = 0.0_8
                  if ((ba+offset) <= bndry_interior ) then
                    do ii = iis, iie
                      dF(l0) = dF(l0) + vals(ii) * F(l0+ii*dist)
                    end do  ! ... i
                  else
                    rhs_block_depth = ba - bndry_interior + offset
                    offset_dist = (-rhs_block_depth-bndry_diff)*dist
                    l0_dist = l0 + offset_dist
                    do ii = bndry_width, 1, -1
                      dF(l0) = dF(l0) + rhs_block2(ii,rhs_block_depth) * F(l0_dist+ii*dist)
                    end do ! ... i
                  end if
                end do  ! ... ba
              end do  ! ... ctr
            end if ! ... "regular" right edge

            ! ... "extended" right edge
            if (r_extended_b(jlc,klc) > 0) then
              bndry_location = grid_ie_unique(1)
              bndry_affected = r_extended_size_b(jlc,klc)
              offset_ckjb = (k-ks)*slice_k + (j-js)*N1g + (bndry_location-bndry_affected-is+nGhostRHS(1,1)) + 1
              do ba = 1, bndry_affected
                l0 = ba + offset_ckjb
                dF(l0) = 0.0_8
                if (ba <= bndry_interior) then
                  do ii = iis, iie
                    dF(l0) = dF(l0) + vals(ii) * F(l0+ii*dist)
                  end do  ! ... i
                else
                  rhs_block_depth = ba - bndry_interior
                  offset_dist = (-rhs_block_depth-bndry_diff)*dist
                  l0_dist = l0 + offset_dist
                  do ii = bndry_width, 1, -1
                    dF(l0) = dF(l0) + rhs_block2(ii,rhs_block_depth) * F(l0_dist+ii*dist)
                  end do ! ... i
                end if
              end do ! ... ba
            end if ! ... "extended" right edge

          end do ! ... j
        end do ! ... k

      Case (2)
        ks = grid_is_unique(3) - nGhostRHS(3,1)
        ke = grid_ie_unique(3) + nGhostRHS(3,2)
        is = grid_is_unique(1) - nGhostRHS(1,1)
        ie = grid_ie_unique(1) + nGhostRHS(1,2)
        js = grid_is_unique(2)
        je = grid_ie_unique(2)

        ! ... interior part
        if (nGhostRHS(2,1) == 0) then
          js_in = js - iis
        else
          js_in = js
        end if
        if (nGhostRHS(2,2) == 0) then
          je_in = je - iie
        else
          je_in = je
        end if
        offset_c = (-ks)*slice_k + (-js+nGhostRHS(2,1))*N1g + (-is) + 1
        do k = ks, ke
          offset_ck = offset_c + k*slice_k
          do j = js_in, je_in
            offset_ckj = offset_ck + j*N1g
            do ii = iis, iie
              offset_ckjii = offset_ckj + ii*N1g
              value = vals(ii)
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
              do i = is, ie
                dF(i+offset_ckj) = dF(i+offset_ckj) + value * F(i+offset_ckjii)
              end do
            end do
          end do
        end do

        ! ... boundary part
        do k = ks, ke
          do i = is, ie

            ! ... local indices on this face
            ilc = i - is + 1
            klc = k - ks + 1

            ! ... "regular" left edge
            if (l_ctr_lc_b(ilc,klc) > 0) then
              offset_cki = (k-ks)*slice_k + (-1-js+nGhostRHS(2,1))*N1g + (i-is) + 1
              do ctr = 1, l_ctr_lc_b(ilc,klc)
                bndry_location = l_bndry_lc_b(ctr,ilc,klc)
                offset_ckib = offset_cki + bndry_location*N1g
                bndry_extent = je - bndry_location + 1
                if (bndry_extent < bndry_depth_max) then
                  bndry_affected = bndry_extent
                else
                  bndry_affected = bndry_depth_max
                end if
                do ba = 1, bndry_affected
                  l0 = ba*N1g + offset_ckib
                  dF(l0) = 0.0_8
                  if (ba + bndry_interior > bndry_depth_max) then
                    do ii = iis, iie
                      dF(l0) = dF(l0) + vals(ii) * F(l0+ii*dist)
                    end do  ! ... i
                  else
                    rhs_block_depth = ba
                    offset_dist = (-rhs_block_depth)*dist
                    l0_dist = l0 + offset_dist
                    do ii = 1, bndry_width
                      dF(l0) = dF(l0) + rhs_block1(ii,rhs_block_depth) * F(l0_dist+ii*dist)
                    end do  ! ... i
                  end if
                end do  ! ... ba
              end do  ! ... ctr  
            end if ! ... "regular" left edge

            ! ... "extended" left edge
            if (l_extended_b(ilc,klc) > 0) then
              bndry_location = grid_is_unique(2)
              bndry_affected = l_extended_size_b(ilc,klc)
              offset = bndry_depth_max - bndry_affected
              offset_ckib = (k-ks)*slice_k + (bndry_location-1-js+nGhostRHS(2,1))*N1g + (i-is) + 1

              do ba = 1, bndry_affected
                l0 = ba*N1g + offset_ckib
                dF(l0) = 0.0_8
                if (ba + offset + bndry_interior > bndry_depth_max) then
                  do ii = iis, iie
                    dF(l0) = dF(l0) + vals(ii) * F(l0+ii*dist)
                  end do  ! ... i
                else
                  rhs_block_depth = ba + offset
                  offset_dist = (-rhs_block_depth)*dist
                  l0_dist = l0 + offset_dist
                  do ii = 1, bndry_width
                    dF(l0) = dF(l0) + rhs_block1(ii,rhs_block_depth) * F(l0_dist+ii*dist)
                  end do ! ... i
                end if
              end do ! ... ba
            end if ! ... "extended" left edge

            ! ... "regular" right edge
            if (r_ctr_lc_b(ilc,klc) > 0) then
              offset_cki = (k-ks)*slice_k + (-js+nGhostRHS(2,1))*N1g + (i-is) + 1
              do ctr = 1, r_ctr_lc_b(ilc,klc)
                bndry_location = r_bndry_lc_b(ctr,ilc,klc)
                bndry_extent = bndry_location - js + 1
                if (bndry_extent < bndry_depth_max) then
                  bndry_affected = bndry_extent
                else
                  bndry_affected = bndry_depth_max
                end if
                offset = bndry_depth_max - bndry_affected
                offset_ckib = offset_cki + (bndry_location-bndry_affected)*N1g
                do ba = 1, bndry_affected
                  l0 = ba*N1g + offset_ckib
                  dF(l0) = 0.0_8
                  if ((ba+offset) <= bndry_interior ) then
                    do ii = iis, iie
                      dF(l0) = dF(l0) + vals(ii) * F(l0+ii*dist)
                    end do  ! ... i
                  else
                    rhs_block_depth = ba - bndry_interior + offset
                    offset_dist = (-rhs_block_depth-bndry_diff)*dist
                    l0_dist = l0 + offset_dist
                    do ii = bndry_width, 1, -1
                      dF(l0) = dF(l0) + rhs_block2(ii,rhs_block_depth) * F(l0_dist+ii*dist)
                    end do ! ... i
                  end if
                end do  ! ... ba
              end do  ! ... ctr
            end if ! ... "regular" right edge

            ! ... "extended" right edge
            if (r_extended_b(ilc,klc) > 0) then
              bndry_location = grid_ie_unique(2)
              bndry_affected = r_extended_size_b(ilc,klc)
              offset_ckib = (k-ks)*slice_k + (bndry_location-bndry_affected-js+nGhostRHS(2,1))*N1g + (i-is) + 1
              do ba = 1, bndry_affected
                l0 = ba*N1g + offset_ckib
                dF(l0) = 0.0_8
                if (ba <= bndry_interior) then
                  do ii = iis, iie
                    dF(l0) = dF(l0) + vals(ii) * F(l0+ii*dist)
                  end do  ! ... i
                else
                  rhs_block_depth = ba - bndry_interior
                  offset_dist = (-rhs_block_depth-bndry_diff)*dist
                  l0_dist = l0 + offset_dist
                  do ii = bndry_width, 1, -1
                    dF(l0) = dF(l0) + rhs_block2(ii,rhs_block_depth) * F(l0_dist+ii*dist)
                  end do ! ... i
                end if
              end do ! ... ba
            end if ! ... "extended" right edge

          end do ! ... i
        end do ! ... k

      Case (3)
        ks = grid_is_unique(3)
        ke = grid_ie_unique(3)
        js = grid_is_unique(2) - nGhostRHS(2,1)
        je = grid_ie_unique(2) + nGhostRHS(2,2)
        is = grid_is_unique(1) - nGhostRHS(1,1)
        ie = grid_ie_unique(1) + nGhostRHS(1,2)

        ! ... interior part
        if (nGhostRHS(3,1) == 0) then
          ks_in = ks - iis
        else
          ks_in = ks
        end if
        if (nGhostRHS(3,2) == 0) then
          ke_in = ke - iie
        else
          ke_in = ke
        end if
        offset_c = (-ks+nGhostRHS(3,1))*slice_k + (-js)*N1g + (-is) + 1
        do k = ks_in, ke_in
          offset_ck = offset_c + k*slice_k
          do j = js, je
          offset_ckj = offset_ck + j*N1g
            do ii = iis, iie
              offset_ckjii = offset_ckj + ii*slice_k
              value = vals(ii)
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
              do i = is, ie
                dF(i+offset_ckj) = dF(i+offset_ckj) + value * F(i+offset_ckjii)
              end do
            end do
          end do
        end do

        ! ... boundary part
        do j = js, je
          do i = is, ie

            ! ... local indices on this face
            ilc = i - is + 1
            jlc = j - js + 1

            ! ... "regular" left edge
            if (l_ctr_lc_b(ilc,jlc) > 0) then
              offset_cji = (-1-ks+nGhostRHS(3,1))*slice_k + (j-js)*N1g + (i-is) + 1
              do ctr = 1, l_ctr_lc_b(ilc,jlc)
                bndry_location = l_bndry_lc_b(ctr,ilc,jlc)
                offset_cjib = offset_cji + bndry_location*slice_k
                bndry_extent = ke - bndry_location + 1
                if (bndry_extent < bndry_depth_max) then
                  bndry_affected = bndry_extent
                else
                  bndry_affected = bndry_depth_max
                end if
                do ba = 1, bndry_affected
                  l0 = ba*slice_k + offset_cjib
                  dF(l0) = 0.0_8
                  if (ba + bndry_interior > bndry_depth_max) then
                    do ii = iis, iie
                      dF(l0) = dF(l0) + vals(ii) * F(l0+ii*dist)
                    end do  ! ... i
                  else
                    rhs_block_depth = ba
                    offset_dist = (-rhs_block_depth)*dist
                    l0_dist = l0 + offset_dist
                    do ii = 1, bndry_width
                      dF(l0) = dF(l0) + rhs_block1(ii,rhs_block_depth) * F(l0_dist+ii*dist)
                    end do  ! ... i
                  end if
                end do  ! ... ba
              end do  ! ... ctr  
            end if ! ... "regular" left edge

            ! ... "extended" left edge
            if (l_extended_b(ilc,jlc) > 0) then
              bndry_location = grid_is_unique(3)
              bndry_affected = l_extended_size_b(ilc,jlc)
              offset = bndry_depth_max - bndry_affected
              offset_cjib = (bndry_location-1-ks+nGhostRHS(3,1))*slice_k + (j-js)*N1g + (i-is) + 1

              do ba = 1, bndry_affected
                l0 = ba*slice_k + offset_cjib
                dF(l0) = 0.0_8
                if (ba + offset + bndry_interior > bndry_depth_max) then
                  do ii = iis, iie
                    dF(l0) = dF(l0) + vals(ii) * F(l0+ii*dist)
                  end do  ! ... i
                else
                  rhs_block_depth = ba + offset
                  offset_dist = (-rhs_block_depth)*dist
                  l0_dist = l0 + offset_dist
                  do ii = 1, bndry_width
                    dF(l0) = dF(l0) + rhs_block1(ii,rhs_block_depth) * F(l0_dist+ii*dist)
                  end do ! ... i
                end if
              end do ! ... ba
            end if ! ... "extended" left edge

            ! ... "regular" right edge
            if (r_ctr_lc_b(ilc,jlc) > 0) then
              offset_cji = (-ks+nGhostRHS(3,1))*slice_k + (j-js)*N1g + (i-is) + 1
              do ctr = 1, r_ctr_lc_b(ilc,jlc)
                bndry_location = r_bndry_lc_b(ctr,ilc,jlc)
                bndry_extent = bndry_location - ks + 1
                if (bndry_extent < bndry_depth_max) then
                  bndry_affected = bndry_extent
                else
                  bndry_affected = bndry_depth_max
                end if
                offset = bndry_depth_max - bndry_affected
                offset_cjib = offset_cji + (bndry_location-bndry_affected)*slice_k
                do ba = 1, bndry_affected
                  l0 = ba*slice_k + offset_cjib
                  dF(l0) = 0.0_8
                  if ((ba+offset) <= bndry_interior ) then
                    do ii = iis, iie
                      dF(l0) = dF(l0) + vals(ii) * F(l0+ii*dist)
                    end do  ! ... i
                  else
                    rhs_block_depth = ba - bndry_interior + offset
                    offset_dist = (-rhs_block_depth-bndry_diff)*dist
                    l0_dist = l0 + offset_dist
                    do ii = bndry_width, 1, -1
                      dF(l0) = dF(l0) + rhs_block2(ii,rhs_block_depth) * F(l0_dist+ii*dist)
                    end do ! ... i
                  end if
                end do  ! ... ba
              end do  ! ... ctr
            end if ! ... "regular" right edge

            ! ... "extended" right edge
            if (r_extended_b(ilc,jlc) > 0) then
              bndry_location = grid_ie_unique(3)
              bndry_affected = r_extended_size_b(ilc,jlc)
              offset_cjib = (bndry_location-bndry_affected-ks+nGhostRHS(3,1))*slice_k + (j-js)*N1g + (i-is) + 1
              do ba = 1, bndry_affected
                l0 = ba*slice_k + offset_cjib
                dF(l0) = 0.0_8
                if (ba <= bndry_interior) then
                  do ii = iis, iie
                    dF(l0) = dF(l0) + vals(ii) * F(l0+ii*dist)
                  end do  ! ... i
                else
                  rhs_block_depth = ba - bndry_interior
                  offset_dist = (-rhs_block_depth-bndry_diff)*dist
                  l0_dist = l0 + offset_dist
                  do ii = bndry_width, 1, -1
                    dF(l0) = dF(l0) + rhs_block2(ii,rhs_block_depth) * F(l0_dist+ii*dist)
                  end do ! ... i
                end if
              end do ! ... ba
            end if ! ... "extended" right edge

          end do ! ... i
        end do ! ... j
    End Select

    ! ... exchange the ghost cell data right now, if we need
    if (exchange_flag == 1) then
      ptr_to_data => dF
      Call Ghost_Cell_Exchange_Box(region, gridID, ptr_to_data)
    end if

! -------------------------------------------------------------------------------------

    ! ... if explicit then return
    if (deriv_type(dir) == EXPLICIT) then
      timings%operator(order,dir) = timings%operator(order,dir) + (MPI_WTIME() - timer)
      return
    end if

!#ifdef USE_HYPRE
!    ! ... update HYPRE right-hand-side vector
!    call fill_hypre_pade_implicit_rhs_vector(nGridsGlobal, iGridGlobal, gridID, nGrids, &
!                                             myrank_inComm, numproc_inComm, comm, &
!                                             ND, dir, order, grid_is, grid_ie, dF)
!
!    ! ... initialize the HYPRE solution vector to be the RHS dF
!    call fill_hypre_pade_implicit_soln_vector(nGridsGlobal, iGridGlobal, gridID, nGrids, &
!                                              myrank_inComm, numproc_inComm, comm, &
!                                              ND, dir, order, grid_is, grid_ie, dF)
!
!
!    ! ... solve the HYPRE system using ILU preconditioning
!    timer2 = MPI_WTime()
!    call hypre_solve_pade_implicit(nGridsGlobal, iGridGlobal, gridID, nGrids, &
!                                   myrank_inComm, numproc_inComm, comm, &
!                                   ND, dir, order, grid_is, grid_ie, iterations)
!    region%mpi_timings(gridID)%operator_solve(order,dir) = region%mpi_timings(gridID)%operator_solve(order,dir) &
!                                                         + (MPI_WTIME() - timer2)
!
!    if (myrank_inComm == 0) then
!      if (iterations > 5) then
!        write (*,'(A,I3,A)') 'PlasComCM: Warning: HYPRE-based derivative solve needed ', iterations, ' iterations.'
!        write (*,'(A,I3,A)') 'PlasComCM: Consider changing ILU preconditioner settings.'
!        write (*,'(A,I3)')   'PlasComCM: opID = ', order
!      end if
!    end if
!
!    ! ... retreive the HYPRE solution
!    call hypre_update_pade_implicit_soln_vector(nGridsGlobal, iGridGlobal, gridID, nGrids, &
!                                                myrank_inComm, numproc_inComm, comm, &
!                                                ND, dir, order, grid_is, grid_ie, dF)
!
!    timings%operator(order,dir) = timings%operator(order,dir) + (MPI_WTIME() - timer)
!#endif
!
    return

  end subroutine APPLY_OPERATOR_box

  subroutine Ghost_Cell_Update_Box(region, gridID, flags, var_to_exchange)

    use ModGlobal
    use ModDataStruct
    USE ModMatrixVectorOps
    USE ModMPI

    implicit none

    type(t_region), pointer :: region
    integer :: gridID
    integer :: flags
    character(len=3) :: var_to_exchange

    ! ... local variables
    type(t_grid), pointer :: grid
    type(t_mixt_input), pointer :: input
    type (t_mixt), pointer :: state
    real(rfreal), pointer :: ptr_to_data(:)
    integer :: Noutput
    integer :: N1, N2, N3, G1, G2, G3, N1g, N2g, N3g, Nghost
    integer :: l0, lbuf, k, j, i, req(52)
    integer :: bufsz_W, bufsz_S, bufsz_SW, bufsz_D, bufsz_D_W, bufsz_D_S, bufsz_D_SW
    integer :: nCv, nAuxvars

    grid  => region%grid(gridID)
    state => region%state(gridID)
    input => grid%input

    N1         = grid%ie_unique(1) - grid%is_unique(1) + 1
    N2         = grid%ie_unique(2) - grid%is_unique(2) + 1
    N3         = grid%ie_unique(3) - grid%is_unique(3) + 1
    G1         = grid%nGhostRHS(1,1) + grid%nGhostRHS(1,2)
    G2         = grid%nGhostRHS(2,1) + grid%nGhostRHS(2,2)
    G3         = grid%nGhostRHS(3,1) + grid%nGhostRHS(3,2)
    N1g        = N1 + G1
    N2g        = N2 + G2
    N3g        = N3 + G3
    Nghost  = input%nOverLap
    Noutput = 2*Nghost

    nCv        = input%nCv
    nAuxvars   = input%nAuxVars

    ! ... cv
    if ( var_to_exchange(1:3) == 'cv ' ) then
      do i = 1, nCv
        ptr_to_data => state%cv(:,i)
        Call Ghost_Cell_Exchange_Box(region, gridID, ptr_to_data)
      end do
    end if

    ! ... auxvars
    if ( var_to_exchange(1:3) == 'aux' ) then
      do i = 1, nAuxvars
        ptr_to_data => state%auxVars(:,i)
        Call Ghost_Cell_Exchange_Box(region, gridID, ptr_to_data)
      end do
    end if

    ! ... velocity gradient and temperature gradient
    if ( var_to_exchange(1:3) == 'grd' ) then
      if (flags /= INVISCID_ONLY) then
        if ( state%RE > 0.0_rfreal .OR. &
           input%shock /= 0      .OR. &
           (input%LES >= 200 .AND. input%LES < 300)) then
          do i = 1, grid%ND
            do j = 1, grid%ND
              ptr_to_data => state%VelGrad1st(:,region%global%t2Map(i,j))
              Call Ghost_Cell_Exchange_Box(region, gridID, ptr_to_data)
            end do
          end do
          do i = 1, grid%ND
            ptr_to_data => state%TempGrad1st(:,i)
            Call Ghost_Cell_Exchange_Box(region, gridID, ptr_to_data)
          end do
        end if ! state%RE
      end if
    end if

    return

  end subroutine Ghost_Cell_Update_Box


  Subroutine Plane_Periodic_Metric_Hack_Box(dir, grid, F)

    USE ModGlobal
    USE ModDataStruct
    USE ModMatrixVectorOps
    USE ModMPI

    implicit none

    integer :: dir
    type(t_grid), pointer :: grid
    real(rfreal), pointer :: F(:)

    ! ... local variables
    type(t_mixt_input), pointer :: input
    integer :: i, j, k, N1, N2, N3, G1, G2, G3, N1g, N2g, N3g, Nghost, l0
    real(rfreal) :: L
    logical :: adjust(2)

    ! ... pointer dereference
    input => grid%input

    ! ... size
    N1         = grid%ie_unique(1) - grid%is_unique(1) + 1
    N2         = grid%ie_unique(2) - grid%is_unique(2) + 1
    N3         = grid%ie_unique(3) - grid%is_unique(3) + 1
    G1         = grid%nGhostRHS(1,1) + grid%nGhostRHS(1,2)
    G2         = grid%nGhostRHS(2,1) + grid%nGhostRHS(2,2)
    G3         = grid%nGhostRHS(3,1) + grid%nGhostRHS(3,2)
    N1g        = N1 + G1
    N2g        = N2 + G2
    N3g        = N3 + G3
    Nghost = input%nOverLap

    ! ... periodic length of domain in direction dir
    L = grid%periodicL(dir)

    adjust = .TRUE.
    If (grid%cartDims(dir) > 1) Then
      adjust = .FALSE.
      If (grid%cartCoords(dir) == 0)                    adjust(1) = .TRUE.
      If (grid%cartCoords(dir) == grid%cartDims(dir)-1) adjust(2) = .TRUE.
    End If

    ! ... augment F with its periodic version
    if (adjust(2) .eqv. .true.) Then
      Select Case (dir)
        Case (1)
          do k = 1, N3g
            do j = 1, N2g
              do i = 1, Nghost
                l0 = (k-1)*N1g*N2g + (j-1)*N1g + grid%nGhostRHS(1,1) + N1 + i
                F(l0) = F(l0) + L
              end do
            end do
          end do
        Case (2)
          do k = 1, N3g
            do j = 1, Nghost
              do i = 1, N1g
                l0 = (k-1)*N1g*N2g + (grid%nGhostRHS(2,1)+N2+j-1)*N1g + i
                F(l0) = F(l0) + L
              end do
            end do
          end do
        Case (3)
          do k = 1, Nghost
            do j = 1, N2g
              do i = 1, N1g
                l0 = (grid%nGhostRHS(3,1)+N3+k-1)*N1g*N2g + (j-1)*N1g + i
                F(l0) = F(l0) + L
              end do
            end do
          end do
      End Select
    end if

    if (adjust(1) .eqv. .true.) Then
      Select Case (dir)
        Case (1)
          do k = 1, N3g
            do j = 1, N2g
              do i = 1, Nghost
                l0 = (k-1)*N1g*N2g + (j-1)*N1g + i
                F(l0) = F(l0) - L
              end do
            end do
          end do
        Case (2)
          do k = 1, N3g
            do j = 1, Nghost
              do i = 1, N1g
                l0 = (k-1)*N1g*N2g + (j-1)*N1g + i
                F(l0) = F(l0) - L
              end do
            end do
          end do
        Case (3)
          do k = 1, Nghost
            do j = 1, N2g
              do i = 1, N1g
                l0 = (k-1)*N1g*N2g + (j-1)*N1g + i
                F(l0) = F(l0) - L
              end do
            end do
          end do
      End Select
    end if

    Return

  End Subroutine Plane_Periodic_Metric_Hack_Box

  
  subroutine POLE_DERIV(region, gridID, order, dir, F, dF, grid_diff)

    use ModGlobal
    use ModDataStruct
    USE ModMatrixVectorOps
    USE ModMPI

    implicit none

    type(t_grid), pointer :: grid
    type(t_region), pointer :: region
    type(t_mixt_input), pointer :: input
    type(t_mpi_timings), pointer :: timings
    integer :: dir, gridID, order
    real(rfreal), pointer :: F(:), dF(:)
    logical :: grid_diff
    integer :: exchange_flag

    ! ... local variables
    integer :: i, j, k, ii, l0, l1, iis, iie, N1, N2, N3, G1, G2, G3, N1g, N2g, N3g
    integer :: is, ie, js, je, ks, ke, is_in, ie_in, js_in, je_in, ks_in, ke_in
    integer :: ilc, jlc, klc
    integer :: Nc
    integer, pointer :: grid_periodic(:), deriv_type(:)
    real(rfreal) :: timer
    real(rfreal), pointer :: ptr_to_data(:)

                                ! - edge information of box shape data struct ... WZhang, 10/2014
    INTEGER, POINTER      :: l_ctr_lc_b(:,:), r_ctr_lc_b(:,:)
    ! ... global index of locally detected edges
    INTEGER, POINTER      :: l_bndry_lc_b(:,:,:), r_bndry_lc_b(:,:,:)
    ! ... whether boundary points extended from neighboring processor
    INTEGER, POINTER      :: l_extended_b(:,:), r_extended_b(:,:)
    ! ... number of boundary points extended from neighboring processor
    INTEGER, POINTER      :: l_extended_size_b(:,:), r_extended_size_b(:,:)

    ! ... for boundary stencils
    integer :: bndry_width, bndry_depth_local, bndry_depth_max, bndry_interior
    integer :: dist, bndry_location, bndry_diff
    integer :: ctr, ba, offset
    real(RFREAL) :: rhs_block1(12,20), rhs_block2(12,20)

    ! ... serial optimizations, WZhang 02/2015
    integer :: slice_k, offset_c, offset_ck, offset_ckj, offset_ckjii, offset_ckjb
    integer :: offset_cki, offset_ckib
    integer :: offset_cji, offset_cjib
    integer :: offset_dist
    real(rfreal) :: value
    integer, pointer :: nGhostRHS(:,:)
    integer :: grid_is(MAX_ND), grid_ie(MAX_ND), grid_is_unique(MAX_ND), grid_ie_unique(MAX_ND)
    integer :: rhs_block_depth, l0_dist

    ! ... timings
    timer = MPI_WTIME()

    ! ... pointer dereference
    grid         => region%grid(gridID)
    input        => grid%input
    timings      => region%mpi_timings(gridID)
    grid_periodic => grid%periodic
    deriv_type   => grid%deriv_type

    l_ctr_lc_b   => grid%l_ctr_lc_b(:,:,dir)
    r_ctr_lc_b   => grid%r_ctr_lc_b(:,:,dir)
    l_bndry_lc_b => grid%l_bndry_lc_b(:,:,:,dir)
    r_bndry_lc_b => grid%r_bndry_lc_b(:,:,:,dir)
    l_extended_b => grid%l_extended_b(:,:,dir)
    r_extended_b => grid%r_extended_b(:,:,dir)
    l_extended_size_b => grid%l_extended_size_b(:,:,dir)
    r_extended_size_b => grid%r_extended_size_b(:,:,dir)

    nGhostRHS => grid%nGhostRHS
    grid_is(:) = grid%is(:)
    grid_ie(:) = grid%ie(:)
    grid_is_unique(:) = grid%is_unique(:)
    grid_ie_unique(:) = grid%ie_unique(:)

    N1         = grid_ie_unique(1) - grid_is_unique(1) + 1
    N2         = grid_ie_unique(2) - grid_is_unique(2) + 1
    N3         = grid_ie_unique(3) - grid_is_unique(3) + 1
    G1         = nGhostRHS(1,1) + nGhostRHS(1,2)
    G2         = nGhostRHS(2,1) + nGhostRHS(2,2)
    G3         = nGhostRHS(3,1) + nGhostRHS(3,2)
    N1g        = N1 + G1
    N2g        = N2 + G2
    N3g        = N3 + G3

    Nc         = grid%nCells
    slice_k    = N1g*N2g

    ! ... size of interior stencil
    iis        = input%rhs_interior_stencil_start(order) 
    iie        = input%rhs_interior_stencil_end(order) 

    ! ... store values of the boundary stencil
    do j = 1, 20
      do i = 1, 12
        rhs_block1(i,j) = input%rhs_block1(order,j,i)
        rhs_block2(i,j) = input%rhs_block2(order,j,i)
      end do
    end do

    ! ... for boundary stencils
    bndry_width = input%operator_bndry_width(order)
    bndry_depth_local = input%operator_bndry_depth(order)
    bndry_depth_max =   maxval(input%operator_bndry_depth(:))
    bndry_interior = bndry_depth_max - bndry_depth_local
    bndry_diff = bndry_width - bndry_depth_local

    Select Case (dir)
    Case (1)
      dist = 1
    Case (2)
      dist = N1g
    Case (3)
      dist = N1g * N2g
    End Select


    ! ... this block will replace CID and Sparse_Simple, WZhang 10/2014
                                ! ------------------------------------------------------------------------------------

    ks = grid_is_unique(3) - nGhostRHS(3,1)
    ke = grid_ie_unique(3) + nGhostRHS(3,2)
    js = grid_is_unique(2) - nGhostRHS(2,1)
    je = grid_ie_unique(2) + nGhostRHS(2,2)
    is = grid_is_unique(1)
    ie = grid_ie_unique(1)


    ! ... boundary part
    ba=1
    rhs_block_depth = ba
    do k = ks, ke
      do j = js, je

        ! ... local indices on this face
        jlc = j - js + 1
        klc = k - ks + 1

        ! ... "regular" left edge
        if (l_ctr_lc_b(jlc,klc) > 0) then
          offset_ckj = (k-ks)*slice_k + (j-js)*N1g + (-is+nGhostRHS(1,1))
          do ctr = 1, l_ctr_lc_b(jlc,klc)
            bndry_location = l_bndry_lc_b(ctr,jlc,klc)
            offset_ckjb = offset_ckj + bndry_location
            l0 = ba + offset_ckjb
            dF(l0) = 0.0_8
            offset_dist = (-rhs_block_depth)*dist
            l0_dist = l0 + offset_dist
            do ii = 1, bndry_width
              dF(l0) = dF(l0) + rhs_block1(ii,rhs_block_depth) * F(l0_dist+ii*dist)
            end do  ! ... i
          end do  ! ... ctr
        end if ! ... "regular" left edge

      end do ! ... j
    end do ! ... k


    return

  end subroutine POLE_DERIV
end module ModDeriv
