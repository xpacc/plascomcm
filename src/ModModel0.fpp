! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
!-----------------------------------------------------------------------
!
! ModModel0.f90
! 
! - XPACC Model 0
! 
!-----------------------------------------------------------------------
MODULE ModModel0

  USE ModGlobal

  Implicit none

  integer,parameter :: m_nspecies = 4
  integer,parameter :: m_nreactions = 4

  ! ... species
  integer,parameter :: m_iF = 1
  integer,parameter :: m_iO = 2
  integer,parameter :: m_iR = 3
  integer,parameter :: m_iW = 4
  integer,parameter :: m_H2 = m_iF
  integer,parameter :: m_O2 = m_iO
  integer,parameter :: m_H =  m_iR
  integer,parameter :: m_H2O = m_nspecies
  integer,parameter :: m_N2 = m_nspecies+1

  ! ... reactions
  integer,parameter :: m_jI = min(1,m_nreactions) !initiation
  integer,parameter :: m_jR = min(2,m_nreactions) !Recombination
  integer,parameter :: m_jD = min(3,m_nreactions) !decomposition
  integer,parameter :: m_jT = min(4,m_nreactions) !termination

  logical, parameter :: m_isConstTV = .false., doSpeciesFix = .true., m_unreactive = .false.

  real(rfreal), parameter :: m_rUniversal = 8314.4621d0 !J/K/kmole
  real(rfreal), parameter :: m_cal_cms2W_m = 418.4d0
  real(rfreal), parameter :: R_calMolK = 1.9872041d0;
  real(rfreal), parameter :: m_MAXTEMP = 3.5d3, m_HUGETEMP = 1d4, m_HACKTEMP=3.5d3;
  real(rfreal), parameter :: m_wO=15.9994_rfreal, m_wH=1.00794_rfreal, m_wN=14.00674_rfreal
  
CONTAINS
  
  Subroutine Model0Init(this, input, first_param_ptr, myrank, gridComm, regionInput)
    
    USE ModGlobal
    USE ModDataStruct
    USE ModParam
    USE ModMPI
#ifdef HAVE_CANTERA
    use Cantera
    USE ModXMLCantera
#endif

    Implicit None

    Type(t_model0), pointer :: this
    Type(t_mixt_input), pointer :: input
    Type(t_mixt_input), pointer, intent(in), optional :: regionInput
    Type(t_param), pointer :: first_param_ptr
    Integer, intent(in) :: myrank, gridComm

    ! ... Model 0 requires Cantera
#ifndef HAVE_CANTERA
    Call Graceful_Exit(myrank, 'Model0Init: Model0 requires PlasComCM to be built with Cantera!')
#else

    Type(phase_t), pointer :: Cgas
    Type(t_canteraInterface), pointer :: canteraInterface
    Type (t_jetCrossFlow), pointer :: jetCrossFlow

    Real(WP), Pointer :: Mwcoeff(:)
    integer, pointer :: X2C(:) ! ... mapping
    !Locals
    integer :: i, k, ksp, numSpecies, numReactions, ierr
    integer :: gridRank
    real(rfreal) :: Rgas, cpMix, cvMix,MwRef, OldRE
    real(rfreal) :: mwFuel, gamma, rhoH2, muH2, lambdaH2,alphaH2,SH2, tempState, junk, YF, factor, M0, Mf
    real(rfreal) :: TempRef, Lref, PresRef, Rref, DensRef, SndSpdRef, PresScale, lambdaref, alpharef
    real(rfreal), pointer :: mfrac(:), diff(:), molW(:)
    real(rfreal), pointer,dimension(:)  :: scaleVector

    Call MPI_COMM_RANK(gridComm, gridRank, ierr)

    nullify(this%canteraInterface)
    nullify(this%jetCrossflow)
    nullify(this%E, this%theta, this%b, this%Da, this%Qg)
    nullify(this%order)
    nullify(this%massMatrix)
    nullify(this%thirdBodyEff)
    nullify(this%is3BR)
    nullify(this%conc, this%tBconc, this%omega, this%kr)
    nullify(this%massDens2Conc)
    nullify(this%dimDividers)

    ! ... Cantera interface
    allocate(this%canteraInterface)
    canteraInterface => this%canteraInterface
    
    ! ... Jet in a Crossflow parameters
    allocate(this%jetCrossFlow)
    jetCrossFlow => this%jetCrossFlow
    
    X2C => canteraInterface%X2C
    
    TempRef = input%TempRef
    PresRef = input%PresRef
    gamma = input%GamRef

    ! ... get the XML file name
    call set_param(myrank,input%combustion_model_fname,'COMBUSTION_MODEL_FNAME',first_param_ptr,default='')

    ! ... clip the species?
    this%clipSpecies = get_integer_param(myrank,'CLIP_SPECIES',first_param_ptr,default=FALSE) == TRUE

    if(input%nScalars <= m_nspecies) then
       numSpecies = input%nScalars
       input%nAuxVars = numSpecies
       input%nTv = 3+numSpecies
       input%massDiffusivityIndex = 4
    else
       call graceful_exit(myRank, "numSpecies > MaxSpecies")
    end if

    ! ... reset the equation of state
    if(numSpecies > 0) then
       input%gas_dv_model = DV_MODEL_IDEALGAS_MIXTURE
       input%gas_tv_model = TV_MODEL_EXTERNAL
    else
       input%gas_dv_model = DV_MODEL_IDEALGAS
       input%gas_tv_model = TV_MODEL_POWERLAW
       input%TimeRef = input%LengRef / input%sndSpdref
       RETURN
    end if

    
    Cgas  => this%canteraInterface%Cgas
    if (myrank == 0) print*, "Reading Chemistry File", trim(input%combustion_model_fname)
    Cgas = importPhase(trim(input%combustion_model_fname),'model2_mix')

    call mpi_barrier(gridComm, ierr)

!cantera indexes
    canteraInterface%CkR = speciesIndex(Cgas, 'H')
    canteraInterface%CkF = speciesIndex(Cgas, 'H2')
    canteraInterface%CkO = speciesIndex(Cgas, 'O2')
    canteraInterface%CkN = speciesIndex(Cgas, 'N2')
    canteraInterface%CkW = speciesIndex(Cgas, 'H2O')
    ksp = nSpecies(Cgas)

    if (myrank == 0) print'("Num Species",i5,"indexes R, F, O, N, W",5i5)',ksp,canteraInterface%CkR,canteraInterface%CkF,canteraInterface%CkO,canteraInterface%CkN,canteraInterface%CkW

    allocate(canteraInterface%mfrac(ksp), canteraInterface%diff(ksp), canteraInterface%X2C(numSpecies), canteraInterface%molW(m_N2));
    canteraInterface%mfrac=0d0;canteraInterface%diff=0d0
    canteraInterface%X2C(m_iF) = canteraInterface%CkF
    canteraInterface%X2C(m_iO) = canteraInterface%CkO
    if(m_iR <= numSpecies) canteraInterface%X2C(m_iR) = canteraInterface%CkR
    if(m_iW <= numSpecies) canteraInterface%X2C(m_iW) = canteraInterface%CkW
    
!store properties for the jet stream
    call setState_TPX(Cgas, TempRef, PresRef, 'H2:0.99999, O2:0.00001')
    Rgas = m_rUniversal/meanMolecularWeight(Cgas)
    jetCrossFlow%jetSndSpdRef = sqrt( gamma* Rgas * TempRef)
    jetCrossFlow%jetmuref=viscosity(Cgas)
    jetCrossFlow%jetRref=density(Cgas)

    rhoH2 = jetCrossFlow%jetRref

! properties for the cross stream
    call setState_TPX(Cgas, TempRef, PresRef, 'H2:0.00000001, O2:1, N2:3.76')
    Rgas = m_rUniversal/meanMolecularWeight(Cgas)
    cpMix = Rgas*gamma/(gamma-1d0)
    cvMix = Rgas/(gamma-1d0)
    input%EintRef = cvMix*TempRef
    input%Cpref = cpMix
    input%Cvref = cvMix
    input%gasConstantRef = Rgas
    jetCrossFlow%airSndSpdRef = sqrt( gamma* Rgas * TempRef)
    jetCrossFlow%airmuref=viscosity(Cgas)
    jetCrossFlow%airRref=density(Cgas)


    allocate(this%Mwcoeff(numSpecies))

    Mwcoeff => this%Mwcoeff
    this%YO = 1d0/(1d0+m_wN/m_wO*this%nitrogenOxygenMoleRatio)
    this%oxygenFuelMassRatio = m_wO/2d0/m_wH
    mwFuel = meanMolecularWeight(Cgas)
    canteraInterface%molW(m_H2O) = 2d0*m_wH + m_wO  !this is reset below if numSpecies >= m_H2O
    canteraInterface%molW(m_N2) = 2d0*m_wN
    if(.true.) then
      !hardwire the Mwcoeff to be consistent with the Intialization in
      !jet_cross_flow.f90
      Mwcoeff(m_H2) = 2d0*m_wH
      Mwcoeff(m_O2) = 2d0*m_wO
      if(numSpecies >= m_H) Mwcoeff(m_H) = m_wH
      if(numSpecies >= m_H2O) Mwcoeff(m_H2O) = 2d0*m_wH + m_wO
      do k =1, numSpecies
        canteraInterface%molW(k) =  Mwcoeff(k)
      enddo
      
      this%MwcoeffRho = 1d0/(2d0*m_wN)
      Mwcoeff = 1d0/Mwcoeff
!inverse of the reference Mw
      MwRef = this%YO*Mwcoeff(m_O2) + (1d0-this%YO)*this%MwcoeffRho
!divide Mw by MwRef
      Mwcoeff = Mwcoeff/MwRef
      this%MwcoeffRho = this%MwcoeffRho/MwRef
!remove the nitrogen
      Mwcoeff(1:numSpecies) = Mwcoeff(1:numSpecies) - this%MwcoeffRho
    else
!use cantera
      call getMolecularWeights(Cgas, canteraInterface%mfrac) !!mfrac is a dummy here
      this%MwcoeffRho = mwFuel/canteraInterface%mfrac(canteraInterface%CkN)
      do k =1,numSpecies
        canteraInterface%molW(k) = canteraInterface%mfrac(canteraInterface%X2C(k))
        this%Mwcoeff(k) = mwFuel/canteraInterface%molW(k)-this%MwcoeffRho
      enddo
    endif
    
    if(myRank == 0) then
       print'(1p123e12.4)',this%Mwcoeff,this%MwcoeffRho,mwFuel,canteraInterface%molW
       call getMolecularWeights(Cgas, canteraInterface%mfrac) 
       print*,'species and reference Mol W',canteraInterface%mfrac(canteraInterface%X2C(:)),canteraInterface%mfrac(canteraInterface%CkN),mwFuel
    endif

    !density at reference (pressure and temperature are read from input, the density, speed of sound etc.. are overridden)
    Rref = density(Cgas)
    input%DensRef = Rref
    input%SndSpdRef = sqrt( gamma* Rgas * TempRef)


!dimensional values to get dimensionless qtities in computeTV
    canteraInterface%muref=viscosity(Cgas)
    canteraInterface%lambdaref=thermalConductivity(Cgas)
    call getMixDiffCoeffsMass(Cgas, canteraInterface%diff)
    canteraInterface%alpharef = Rref*canteraInterface%diff(canteraInterface%CkO)

!reassign the Reynolds and other numbers
    this%isInviscid = input%RE <= 0.0d0
    OldRE = input%RE
    input%RE = input%DensRef*input%SndSpdRef*input%LengRef/canteraInterface%muref
    input%PR = input%Cpref*canteraInterface%muref/canteraInterface%lambdaref
    input%SC = canteraInterface%muref/canteraInterface%alpharef   
    input%TimeRef = input%LengRef / input%sndSpdref
    
    if (myRank == 0) print'("OldRE, Reynolds, PR, Schmidt",1p123e12.4)', OldRE, input%RE, input%PR, input%SC
    if (myRank == 0) print'("Rey per unit Velocity",1p123e12.4)', input%RE/input%SndSpdRef
    if (myRank == 0) print'("Temperature Scale",1p123e12.4)',  input%TempRef *(input%GamRef-1)
    if (myRank == 0)  print'("DensRef,SndSpdRef,LengRef,muref ",1p123e12.4)',  input%DensRef,input%SndSpdRef,input%LengRef,canteraInterface%muref


    input%Froude = input%SndSpdRef**2/9.81d0/input%LengRef
    input%gravity_angle_phi = TWOPI/4d0
    input%gravity_angle_theta = -TWOPI/4d0
    if (myRank == 0) print'("Froude, Froude_code",1p123e12.4)',jetCrossFlow%injectionVelocity**2/9.81d0/input%LengRef,input%Froude


    if (myRank == 0) write (*,'(A,1p999e12.4)') 'Combustion: ==> Resetting RE, PR, SC,U <==', input%RE, input%RE/input%SndSpdRef*jetCrossFlow%injectionVelocity, input%PR, input%SC,input%SndSpdRef,input%DensRef,input%LengRef,canteraInterface%muref


!this block increases the velocity and the viscoisty, gravity to match Reynolds Froude and so on
    if(input%machFactor > 1d0) then  !scale the reynolds and Froude numbers
       input%RE = input%RE/input%machFactor
       input%Froude = input%Froude/(input%machFactor**2)
       input%TimeRef = input%TimeRef*input%machFactor
       if (myRank == 0) write (*,'(A,1p999e12.4)') 'Combustion: ==> Modified Re, input%machFactor <==', input%RE,input%machFactor,  input%machFactor*jetCrossFlow%injectionVelocity/input%SndSpdRef,  input%machFactor*jetCrossFlow%crossVelocity/jetCrossFlow%airSndSpdRef
    else
       input%machFactor=1.0d0
    end if
    input%invFroude = 1.0_WP / input%Froude
    this%dimVelFactor = input%SndSpdRef/input%machFactor

    if(this%isInviscid) then
      input%RE = 0.0d0
      input%Froude = 0d0
      input%invFroude = 0d0
    endif
    if(myRank == 0) print'(a,1p123e12.4)',"Inverse Froude g L /c^2 ",input%invFroude

!set number of reactions in model0
    if(numSpecies > 2) then
       numReactions = 4
    else
       numReactions = 0;
    end if
!!  record indexes for exporting iformation to other modules
    this%numSpecies = numSpecies
    this%numReactions = numReactions
    this%iH2 = m_H2
    this%iO2 = m_O2
    this%iH =  m_H
    this%iH2O =m_H2O
    this%iN2 = m_N2
    if(.false.) then  !use the flow scale
       this%DBDTimeRef = input%TimeRef
    else  !use the acoustic scale
       this%DBDTimeRef = input%LengRef / input%sndSpdref
    endif
    this%momentumScaling =  input%TimeRef * input%machFactor / (input%DensRef*input%sndSpdref)
    this%radicalSourceScaling = input%DensRef*input%TimeRef /canteraInterface%molW(m_H2)
    this%energySourceScaling = input%DensRef**2*input%TimeRef /(input%DensRef*input%SndSpdRef**2)
    if(numReactions > 0 ) then
!allocate work arrays
       allocate(this%kr(numReactions), this%omega(numSpecies+1),  this%conc(numSpecies),  this%tBconc(numReactions))
    endif

    this%isChemistryInitialized = .false.

    molW => this%canteraInterface%molW
    scaleVector => this%kr

! ... reference values
    TempRef   = input%TempRef
    DensRef   = input%densref
    SndSpdRef = input%sndspdref
    gamma    = input%gamref
    this%gamma = gamma
!lucaNotice:: the input%presref below is not the pressure scale but the pressure at the reference state
    PresScale   = input%presref*gamma

!factors to transform the dimensionless mass densities in dimensional concentrations in kmol/m^3
    allocate(this%massDens2Conc(m_N2))
    this%massDens2Conc = DensRef/molW(1:m_N2)

!source dimension factor
    allocate(this%dimDividers(numSpecies+1))
    this%dimDividers = DensRef
    this%dimDividers(1) = PresScale  !for the energy equation
    this%dimDividers = this%dimDividers/input%TimeRef

!factors to get dimensional temperature, density and momentum
    this%dimTempFactor = (gamma-1.0d0)*TempRef
    this%dimDensFactor = input%densref
    this%dimMomFactor = DensRef*SndSpdRef

    !model0 set-up
    if(gridRank == 0) call read_xml_file_cantera_M0(trim(input%combustion_model_fname), this )
    call BroadcastPtr(this%theta, gridComm)
    call BroadcastPtr(this%b, gridComm)
    call BroadcastPtr(this%Da, gridComm)
    call BroadcastPtr2(this%order, gridComm)
    call BroadcastPtr2(this%thirdBodyEff, gridComm)
    call BroadcastPtrL(this%is3BR, gridComm)



!Heat Release Calculations
    if ( numReactions >0 ) then

       allocate(this%Qg(numReactions))
       M0 = (4d0*m_wH+2d0*m_wO+this%nitrogenOxygenMoleRatio*2d0*m_wN)/(3d0+this%nitrogenOxygenMoleRatio);
       Mf = (4d0*m_wH+2d0*m_wO+this%nitrogenOxygenMoleRatio*2d0*m_wN)/(2d0+this%nitrogenOxygenMoleRatio);
       this%Qg(m_jI) = this%Qg1
       this%Qg(m_jR) = -this%Qg1
       this%Qg(m_jD) = m_rUniversal*(this%Tf/Mf -this%T0/M0)*gamma/(gamma-1.0)*(1.0+m_wN/m_wH/2d0*this%nitrogenOxygenMoleRatio+this%oxygenFuelMassRatio)
       this%Qg(m_jT) = this%Qg(m_jD) - this%Qg1

!mass matrix based on atom conservation
      allocate(this%massMatrix(numSpecies+1, numReactions))
      this%massMatrix = 0d0
      
      this%massMatrix(1,1:numReactions) = this%Qg(1:numReactions)
      scaleVector = [1d0, -1d0, 0d0, -1d0]
      if(m_H <= numSpecies)&
           this%massMatrix(min(m_H,numSpecies)+1,1:numReactions) = scaleVector(1:numReactions);
      scaleVector = [-1d0, 1d0, -1d0, 0d0];
      this%massMatrix(m_H2+1,1:numReactions) = scaleVector(1:numReactions);
      scaleVector = [0d0, 0d0, -this%oxygenFuelMassRatio, -this%oxygenFuelMassRatio]
      this%massMatrix(m_O2+1,1:numReactions) = scaleVector(1:numReactions);
      if(m_H2O <= numSpecies) then
        do i = 1,numReactions
          this%massMatrix(m_H2O+1,i) = -sum(this%massMatrix(2:m_H2O,i))
        enddo
      endif

!scale Da to go from molar to mass units; [2,2,4,4] is the mass of hydrogen processed per unit reaction progress
      scaleVector = [2d0,2d0,4d0,4d0]*m_wH
      do i = 1,min(4,ubound(this%Da,1))
        this%Da(i) = this%Da(i)*scaleVector(i)
      enddo
      this%isUnreactive = m_unreactive
    else
      this%isUnreactive = .true.
    endif
    this%isChemistryInitialized = .true.


    if (myRank == 0) write (*,'(A,1p999e12.4)') 'Combustion: ==> Final RE, PR, SC, Aref <==', input%RE, input%PR, input%SC, input%SndSpdRef

    ! ... set the number of species
    !this%numSpecies = input%nScalars

    !If (this%numSpecies /= 2) Call Graceful_Exit(myrank, 'Model0Init: only implemented for 2 species')

    ! ... read from the input file
    !this%YO0 = get_real_param(myrank,'YO0',first_param_ptr,default=0.233_rfreal)

    ! ... equation of state
    if (input%gas_dv_model /= DV_MODEL_IDEALGAS_MIXTURE .or. input%gas_dv_model /= TV_MODEL_EXTERNAL) &
         Call Graceful_Exit(myrank, 'Model0Init: Model0 requires GAS_EQUATION_OF_STATE = IDEAL_GAS_MIXTURE or TV_MODEL_EXTERNAL!')

    ! ... overwrite region input file if present
    if (present(regionInput)) then
       regionInput%nAuxVars = input%nAuxVars
       regionInput%nTv = input%nTv
       regionInput%massDiffusivityIndex = input%massDiffusivityIndex
       regionInput%gas_dv_model = input%gas_dv_model
       regionInput%gas_tv_model = input%gas_tv_model
       regionInput%TimeRef = input%TimeRef
       regionInput%EintRef = input%EintRef
       regionInput%Cpref = input%Cpref
       regionInput%Cvref = input%Cvref
       regionInput%gasConstantRef = input%gasConstantRef
       regionInput%DensRef = input%DensRef
       regionInput%SndSpdRef = input%SndSpdRef
       regionInput%RE = input%RE
       regionInput%PR = input%PR
       regionInput%SC = input%SC
       regionInput%Froude = input%Froude
       regionInput%gravity_angle_phi = input%gravity_angle_phi
       regionInput%gravity_angle_theta = input%gravity_angle_theta
       regionInput%machFactor = input%machFactor
       regionInput%invFroude = input%invFroude
    end if
#endif

  End Subroutine Model0Init

  Subroutine Model0Source(this, xyz, cv, dv, aux, rhsNS, rhsSP, iblank, input, myrank)
    USE ModGlobal
    USE ModMPI
    USE ModDataStruct

    IMPLICIT none

    Type(t_model0), pointer :: this
    real(rfreal), pointer, dimension(:,:),optional,intent(IN) :: XYZ
    real(rfreal), pointer, dimension(:,:) :: cv, dv, aux
    real(rfreal), dimension(:,:), intent(INOUT) :: rhsNS, rhsSP
    INTEGER, dimension(:), intent(IN) :: iblank
    type(t_mixt_input), pointer :: input
    integer :: myrank

#ifdef HAVE_CANTERA

    real(rfreal),pointer, dimension(:) :: dimDividers, massDens2Conc, molW
    real(rfreal), pointer,dimension(:)  :: theta, b, Da
    real(rfreal), pointer, dimension(:,:) :: order !d log(k_i)/ d log(Y_j)
    real(rfreal), pointer, dimension(:,:) :: thirdBodyEff !third body efficiencies
    real(rfreal),pointer, dimension(:,:) :: massMatrix
    real(rfreal),pointer, dimension(:) :: conc, tBconc, omega, kr
    logical, pointer, dimension(:) :: is3BR

    real(rfreal) :: temp, mdotU,  prodConc,dimTempFactor, ibfac

    real(rfreal) :: maxNS, allMaxNS, sumSpec, Ys, PresRef

    integer :: Nx, ienergy,irho, numSpecies, numReactions
    integer :: i,k,ir,is
    logical :: has3BR,useActivityStandPressure=.true.
    real(rfreal) :: xfactor, Tfactor, maxT

    numSpecies = this%numSpecies
    numReactions = this%numReactions
  
    if(numSpecies == 0 .or. numReactions  == 0 .or. m_unreactive) return

    PresRef   = input%presref
    useActivityStandPressure = input%useActivityStandardPressure

    dimDividers => this%dimDividers
    massDens2Conc => this%massDens2Conc
    massMatrix  => this%massMatrix
    Da  => this%Da
    theta  => this%theta
    b  => this%b
    order  => this%order
    thirdBodyEff  => this%thirdBodyEff
    is3BR  => this%is3BR
    conc  => this%conc
    tBconc  => this%tBconc
    omega  => this%omega
    kr  => this%kr
    molW => this%canteraInterface%molW

    has3BR = any(is3BR)

    if(.not. this%isChemistryInitialized) then
       call graceful_exit(myRank, "in addCombustionSources w/o ChemistryIntialized")
    endif

    Nx = ubound(dv,1)
    ienergy = ubound(cv,2)
    irho =    lbound(cv,2)
    dimTempFactor = this%dimTempFactor

    Do i = 1, Nx

!unroll
       if(iblank(i) == 0) cycle
       temp = dv(i,2) * dimTempFactor
       if(temp > m_HUGETEMP) cycle
       if(useActivityStandPressure) then
          sumSpec = 0d0
          prodConc = 1d0
          do is = 1,numSpecies
             Ys =  aux(i,is)/cv(i,irho)
             conc(is) = Ys/molW(is)  !Y_s/M_s
             if(Ys>0d0) then 
               sumSpec = sumSpec + conc(is)
               prodConc = prodConc - Ys
             endif
          enddo
          sumSpec = sumSpec + prodConc/molW(m_N2)
          sumSpec =  PresRef/m_rUniversal/temp/sumSpec
          do is = 1,numSpecies
             conc(is) = conc(is)*sumSpec            !P/Rhat/T*X_i=[]_i (dimensional)
          enddo
       endif
       
       if(.not.useActivityStandPressure) then
         do is = 1,numSpecies
            conc(is) = aux(i,is)*massDens2Conc(is)  !dimensional pressure dependent
         enddo
       endif
       prodConc = PresRef/m_rUniversal/temp - sum(conc(1:numSpecies)) !kmole/m^3

!3rdbody reactions
       if(has3BR) then
          do ir =1,numReactions
             if(.not. is3BR(ir)) cycle
             tBconc(ir) = 0e0
             do is =1,numSpecies
                tBconc(ir)=tBconc(ir)+thirdBodyEff(is,ir)*conc(is) 
             enddo
             is = numSpecies+1 !this will include both the nitrogen and the water if numSpecies == 3
             tBconc(ir)=tBconc(ir)+thirdBodyEff(is,ir)*prodConc
          enddo
       endif


       do ir =1, numReactions
          kr(ir) = da(ir)*exp(-theta(ir)/temp)*temp**(b(ir))
          if(is3BR(ir).eqv. .true.) kr(ir) = kr(ir)*tBconc(ir)
          do is =1,numSpecies
             if(order(is,ir) /= 0d0) then
                if(conc(is) > 0d0) then
                   kr(ir) = kr(ir) * conc(is)**order(is,ir)
                 else
                   kr(ir) = 0d0
                 endif
             end if
          enddo
       enddo
       !half the activation energy if T>m_HACKTEMP
       if(m_HACKTEMP > 0d0) then
         maxT = m_HACKTEMP
         Tfactor = maxT 
         ir = m_jD
         if(temp > maxT)&
           kr(ir) = kr(ir)*exp(theta(ir)/2d0*(1d0/temp-1d0/Tfactor))
        endif

!mutliply by the mass matrix
       omega = MATMUL(massMatrix, kr)

!make omega  dimensionless
       omega = omega / dimDividers !energy,radical,fuel,oxidizer

       !!if(present(src) .and. input%useLaser == FALSE) src(i,1) = omega(1)
      !LM:Hack switch off chemical thermal addition for temp > m_MAXTEMP (module variable)
       if(temp >= m_MAXTEMP) then
         omega(1) = min(omega(1),0d0) 
       endif
       rhsNS(i,ienergy) = rhsNS(i,ienergy) + omega(1)
       do is =1,numSpecies
          rhsSP(i,is) = rhsSP(i,is) + omega(is+1)
       enddo

    enddo !i=1,Nx

#endif

  End Subroutine Model0Source

  Subroutine Model0Dv(this, grid, state, myrank)
    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    
    Implicit None
    Type(t_model0), pointer :: this
    TYPE(t_grid), pointer :: grid
    TYPE(t_mixt), pointer :: state
    Integer, intent(in) :: myrank

#ifdef HAVE_CANTERA

    ! ... local variables
    TYPE(t_mixt_input), pointer :: input
    integer :: j, i, ND, Nc
    real(rfreal) :: ibfac, gamma, gamref, tmp,tmp2, MwcoeffRho
    real(rfreal),pointer :: Mwcoeff(:)

    ! ... simplicity
    ND = grid%ND
    Nc = grid%nCells
    input => grid%input
    gamref = input%GamRef
    Mwcoeff => this%Mwcoeff ! ... Mref/Mi-Mref/M_N2, Mref is not necessarily M_N2
    MwcoeffRho = this%MwcoeffRho ! ... Mref/M_N2

    do i = 1, Nc

       ibfac = grid%ibfac(i)

       ! ... adjust original values wrt IBLANK
       state%cv(i,   1) = ibfac * state%cv(i,1) + (1.0_8 - ibfac)
       state%cv(i,ND+2) = ibfac * state%cv(i,ND+2) + (1.0_8 - ibfac) / (gamref - 1.0_8)
       do j = 1, ND
          state%cv(i,j+1) = ibfac * state%cv(i,j+1)
       end do

       ! specific volume
       state%dv(i,3) = 1.0_8 / state%cv(i,1)

       ! pressure
       state%dv(i,1) = state%cv(i,ND+2)
       do j = 1, grid%ND
          state%dv(i,1) = state%dv(i,1) - 0.5_rfreal * state%cv(i,1+j) * state%cv(i,1+j) * state%dv(i,3)
       end do
       tmp = state%dv(i,1)
       state%dv(i,1) = (gamref-1.0_rfreal) * tmp

       ! temperature
       tmp2 = sum(state%auxVars(i,:)*Mwcoeff) + MwcoeffRho*state%cv(i,1) ! (Luca's implementation)
       !tmp2 = state%auxVars(i,1)*(Cp1-Cpref) + state%cv(i,1)*Cpref ! Jesse's, function of Cp, only for 1 aux var
       state%dv(i,2) = gamref * tmp/tmp2
    end do

    do j = 1, input%nAuxVars
       do i = 1, Nc
          ibfac = grid%ibfac(i)
          state%auxVars(i,j) = state%auxVars(i,j) * ibfac
       end do
    end do

#endif

  End Subroutine Model0Dv

  Subroutine Model0Tv(this, grid, state)
    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
#ifdef HAVE_CANTERA
    use cantera ! lower case 'use' to trick makedeps
#endif
    
    Implicit None
    Type(t_model0), pointer :: this
    TYPE(t_grid), pointer :: grid
    TYPE(t_mixt), pointer :: state

#ifdef HAVE_CANTERA
    type(phase_t), pointer :: Cgas
    real(rfreal), dimension(:,:),pointer :: tv,dv,cv,aux
    real(rfreal), pointer :: mfrac(:), diff(:)
    integer, pointer :: X2C(:) !mapping
    real(rfreal) :: temp,dens,dmu,dlam,factor
    integer :: i,k,kxpacc,kcantr
    integer :: CkR,CkF,CkO,CkN, numSpecies
    real(rfreal) :: dimTempFactor, dimDensFactor, muref, lambdaref, alpharef

    if(state%combustion%iRKstage > 1) return

    tv => state%tv
    dv => state%dv
    cv => state%cv
    aux => state%auxVars
    mfrac => this%canteraInterface%mfrac
    diff => this%canteraInterface%diff
    X2C => this%canteraInterface%X2C
    CkF = this%canteraInterface%CkF
    CkO = this%canteraInterface%CkO
    CkR = this%canteraInterface%CkR
    CkN = this%canteraInterface%CkN
    Cgas  => this%canteraInterface%Cgas
    alpharef = this%canteraInterface%alpharef
    lambdaref = this%canteraInterface%lambdaref
    muref = this%canteraInterface%muref
    dimTempFactor = this%dimTempFactor
    dimDensFactor = this%dimDensFactor
    numSpecies = this%numSpecies
    
    
    if(m_isConstTV .or. this%isInviscid) then;tv=1.0d0;where(grid%iblank==0);tv(:,1)=0d0;tv(:,2)=0d0;endwhere;return;endif

    mfrac=1d-9
    do i = 1, grid%nCells
       if(grid%iblank(i) == 0) then
          tv(i,:) = 0.0d0
          cycle
       end if

       temp = min(dimTempFactor*dv(i,2),m_HUGETEMP)
       !!temp = dimTempFactor*dv(i,2)
       dens = dimDensFactor*cv(i,1)
       mfrac = 0d0
       do k = 1, numSpecies
         kcantr = X2C(k)
         mfrac(kcantr) = aux(i,k)/cv(i,1)
       enddo
       ! ...N2
       mfrac(CkN) = max(1.0d0-sum(mfrac),1d-9)

       call setState_TRY(Cgas,temp,dens,mfrac)

       dmu = viscosity(Cgas)
       dlam = thermalConductivity(Cgas)
       call getMixDiffCoeffsMass(Cgas, diff)

       tv(i,1) =dmu/muref
! ... lambda (bulk viscosity = 0.60 mu for air)
! ... lambda = (mu'/mu -2/3) mu
       tv(i,2) = (0.60_rfreal - 2.0_rfreal/3.0_rfreal) * state%tv(i,1)
       tv(i,3) = dlam/lambdaref

       factor = dens/alpharef
       do k = 1, numSpecies
          kcantr = X2C(k)
          tv(i,3+k) = factor*diff(kcantr)
       enddo

    enddo
#endif
  End Subroutine Model0Tv

 Subroutine Model0FixSpecies(this, aux, cv, dv)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    real(rfreal), dimension(:,:),intent(INOUT) :: aux, cv, dv
    Type(t_model0), pointer :: this

#ifdef HAVE_CANTERA

    ! ... local variables
    integer :: i, k, Nx,Nvars,imomM, numSpecies
    real(rfreal) :: SSUM, temp, MaxTemp,dimTempFactor

    ! ... simplify
    dimTempFactor = this%dimTempFactor
    numSpecies = this%numSpecies

    if(numSpecies == 0 .or. .not. this%clipSpecies) return

    Nx = ubound(aux,1)
    Nvars = ubound(aux,2)
    imomM = ubound(cv,2) - 1
    do i = 1, Nx
       if(cv(i,1) <= 0.0d0) cv(i,1) = 1.0d-10
       temp = dv(i,2) * dimTempFactor
       
       if(temp > m_HUGETEMP) then
         !!cv(i,imomM+1) = cv(i,imomM+1) + (temp/m_HUGETEMP-1d0)/2d0*sum(cv(i,2:imomM)**2)/cv(i,1)
         cv(i,1) = cv(i,1) * temp/m_HUGETEMP
         aux(i,1:Nvars) = aux(i,1:Nvars) * temp/m_HUGETEMP
         !!cv(i,2:imomM) = cv(i,2:imomM)* temp/m_HUGETEMP
       endif
       do k = 1, Nvars
          if (aux(i,k) <= cv(i,1) * 1d-20) aux(i,k) = cv(i,1) * 1d-20
          if (aux(i,k) >= 1d0*cv(i,1) ) aux(i,k) = 1d0*cv(i,1)
       end do
       !!if(aux(i,m_iO) >= cv(i,1) * this%YO) aux(i,m_iO) = cv(i,1) * this%YO
       !!SSUM = sum(aux(i,:))/cv(i,1)
       !!if (SSUM >= 1d0) aux(i,:) = 1d0*aux(i,:)/SSUM
    end do

#endif

  End Subroutine Model0FixSpecies

#ifdef HAVE_CANTERA

 Subroutine BroadcastPtr(ptr, gridComm)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None
    real(rfreal), pointer :: ptr(:)
    integer :: i, myrank, gridComm, ierr

    Call MPI_COMM_RANK(gridComm, myrank, ierr)
     i=0
    if(myrank == 0) i = ubound(ptr,1)
    Call MPI_BCAST(i, 1, MPI_INTEGER, 0, gridComm, ierr)
    if(myrank /= 0) allocate(ptr(i))
    Call MPI_BCAST(ptr, i, MPI_DOUBLE_PRECISION, 0, gridComm, ierr)
  end Subroutine BroadcastPtr

  Subroutine BroadcastPtr2(ptr, gridComm)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None
    real(rfreal), pointer :: ptr(:,:)
    integer :: i(2), myrank, gridComm, ierr

    Call MPI_COMM_RANK(gridComm, myrank, ierr)
     i=0
    if(myrank == 0) i = shape(ptr)
    Call MPI_BCAST(i, 2, MPI_INTEGER, 0, gridComm, ierr)
    if(myrank /= 0) allocate(ptr(i(1),i(2)))
    Call MPI_BCAST(ptr, i(1)*i(2), MPI_DOUBLE_PRECISION, 0, gridComm, ierr)
  end Subroutine BroadcastPtr2

  Subroutine BroadcastPtrL(ptr, gridComm)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None
    logical, pointer :: ptr(:)
    integer :: i, myrank, gridComm, ierr

    Call MPI_COMM_RANK(gridComm, myrank, ierr)
     i=0
    if(myrank == 0) i = ubound(ptr,1)
    Call MPI_BCAST(i, 1, MPI_INTEGER, 0, gridComm, ierr)
    if(myrank /= 0) allocate(ptr(i))
    Call MPI_BCAST(ptr, i, MPI_LOGICAL, 0, gridComm, ierr)
  end Subroutine BroadcastPtrL

#endif
  
End Module ModModel0
