! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
MODULE ModRiemannSolver

CONTAINS


  ! FOR THESE SUBROUTINES I NEED APPROXIMATE LEFT AND RIGHT STATES
  ! THESE LEFT AND RIGHT STATES COME FROM MY RECONSTRUCTION STEP (POLYNOMIAL RECON)

  !               u-a           t              u+a
  !                 \           |          '    /       
  !                  \          |         '    /           
  !                   \         |        '    /
  !                    \        |       '    /
  !    rl               \       | LS   ' RS /        rr
  !    ul                \      |     '    /         ur
  !    vl                 \     |    '    /          vr
  !    wl                  \    |   '    /           wr 
  !    Pl                   \   |  '    /            Pr
  !                          \  | '    /
  !                           \ |'    /
  !---------------------------x+1/2--------------------------------

  ! FROM THESE LEFT AND RIGHT STATES WE APPROXIMATE THE CENTER REGION 

  !...CURRENT PLAN FOR FINITE VOLUME IMPLEMENTATION INTO RocFlo-CM
  !...1. code LLF compare with Ratnesh and other 1D test problems  (06/09)
  !...   make it 3D capable from the start
  !...2. code HLLC next and compare with Ratnesh and other 1D test problems (06/09)
  !...   make it 3D capatable from the start
  !...   computing normal and tangential with respect to cell center -- assumption good if grid is very moderately stretched
  !...   The split algorithm will be used -- therefore, the conservative variables in Q must be interchanged appropriately
  !...   SEE TORO -- pg 550
  !...3. Think about WENO -- PPM -- QPM -- for higher order method -- you can get up to 3rd and 4th order with PPM and QPM 
  !...   and still capture shocks -- maybe start 08/09
     
 
  subroutine HLLC_FLUX(FLUX,MAXSPEED,Q,normalvec,tangentvec,dir,region,ng,flags)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    ! ... local variables
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    type(t_mixt_input), pointer :: input
    type(t_region), pointer :: region
    type(t_patch), pointer :: patch
    integer :: flags
    real(rfreal), pointer, volatile :: FLUX(:),Q(:,:)
    real(rfreal) :: F(2,MAX_ND+2), Fs(2,MAX_ND+2),Qs(2,Max_ND+2)
    real(rfreal), pointer :: normalvec(:),tangentvec(:,:)
    real(rfreal), volatile :: P(2),a(2)
    integer :: ND, Nc, N(MAX_ND), ng, i, j, k, Np(MAX_ND), patchID
    integer :: R_xi(MAX_ND,MAX_ND),R_eta(MAX_ND,MAX_ND),R_zeta(MAX_ND,MAX_ND),R(MAX_ND,MAX_ND)
    integer :: ip, jp, kp,dir
    integer :: lr
    real(rfreal) :: source,un(2),ut1(2),ut2(2),S(2),S_S
    real(rfreal) :: MAXSPEED

    ! ... simplicity
    grid => region%grid(ng)
    state => region%state(ng)
    input => grid%input
    ND = grid%ND
    Nc = grid%nCells
    N  = 1
    do j = 1, grid%ND
      N(j) = grid%ie(j)-grid%is(j)+1
    end do ! j
    
    !...compute pressure
    P(:)=(Q(:,ND+3)-1.0_rfreal)*Q(:,ND+2)
    do j=1,ND
    P(:)=P(:)-(Q(:,ND+3)-1.0_rfreal)*0.5_rfreal*Q(:,j+1)**2.0_rfreal/Q(:,1)
    end do
 

    !...compute normal/tangential velocities of the left and right states
    !...given ijk components of the vector normal to the i+1/2 cell face
    !...un = (u,v,w).normalvec(:) 
    !...ut1 = (u,v,w).tang1vec(:)
    !...ut2 = (u,v,w).tang2vec(:)

    do lr=1,2
    if (ND == 2) then  
     un(lr)    = dot_product((/Q(lr,2)/Q(lr,1),Q(lr,3)/Q(lr,1)/),normalvec(:))
     ut1(lr)   = dot_product((/Q(lr,2)/Q(lr,1),Q(lr,3)/Q(lr,1)/),(/-normalvec(2),normalvec(1)/))
    else if (ND == 3) then
     un(lr)    = dot_product((/Q(lr,2)/Q(lr,1),Q(lr,3)/Q(lr,1),Q(lr,ND+1)/Q(lr,1)/), normalvec(:))
     ut1(lr)   = dot_product((/Q(lr,2)/Q(lr,1),Q(lr,3)/Q(lr,1),Q(lr,ND+1)/Q(lr,1)/), tangentvec(1,:))
     ut2(lr)   = dot_product((/Q(lr,2)/Q(lr,1),Q(lr,3)/Q(lr,1),Q(lr,ND+1)/Q(lr,1)/), tangentvec(2,:))
    end if

    !...Compute left and right sound speeds
    a(lr)=dsqrt(Q(lr,ND+3)*(P(lr))/Q(lr,1))
    end do !j=1,2

    !...Compute the maximum and minimum wave speeds estimates of the Riemann problem
    !...This estimate was suggested by Davis SIAM 9:445-473, 1988
    !...Other min and max speeds are available and could be more robust
    S(1)=MINVAL((/(un(1) - a(1)),(un(2) - a(2))/))
    S(2)=MAXVAL((/(un(1) + a(1)),(un(2) + a(2))/))

    !...compute the middle speed S_s
    !...tiny to prevent division by zero
    S_S = (P(2) - P(1) + Q(1,1)*un(1)*(S(1) - un(1)) - Q(2,1)*un(2)*(S(2) - un(2))) / &
            (TINY+Q(1,1)*(S(1) - un(1)) - Q(2,1)*(S(2) - un(2)))

    !...compute the left and right normal flux on the j+1/2 face 
    F(:,1) = Q(:,1) * un(:)                           
    F(:,2) = Q(:,2) * un(:) + P(:)*normalvec(1)
    F(:,3) = Q(:,3) * un(:) + P(:)*normalvec(2) 
    if (ND==3) then
    F(:,ND+1) = Q(:,ND+1)*un(:) + P(:)*normalvec(3)  
    end if
    F(:,ND+2) = un(:) * (Q(:,ND+2) + P(:))


    !...compute the L & R intermediate states (Qs)
    !...computed with respect to cartesian velocities
    Qs(:,1)=Q(:,1)*(S(:)-un(:))/(S(:)-S_S)
    do lr=1,2
     Qs(lr,2)=Qs(lr,1)*dot_product((/S_S,ut1(lr)/),(/normalvec(1),-normalvec(2)/))
     Qs(lr,3)=Qs(lr,1)*dot_product((/S_S,ut1(lr)/),(/normalvec(2),normalvec(1)/))
      if (ND==3) then
       Qs(lr,2)=Qs(lr,1)*dot_product((/S_S,ut1(lr),ut2(lr)/),(/normalvec(1),tangentvec(1,1),tangentvec(2,1)/))
       Qs(lr,3)=Qs(lr,1)*dot_product((/S_S,ut1(lr),ut2(lr)/),(/normalvec(2),tangentvec(1,2),tangentvec(2,2)/))
       Qs(lr,4)=Qs(lr,1)*dot_product((/S_S,ut1(lr),ut2(lr)/),(/normalvec(3),tangentvec(1,3),tangentvec(2,3)/))
      end if
    end do !lr=1,2
    Qs(:,ND+2)=Qs(:,1)*(Q(:,ND+2)/Q(:,1)+(S_S - un(:))*(S_S + P(:)/(Q(:,1)*(S(:) - un(:)))))

    
    do lr=1,2
    !...compute the K --> L & R intermediate flux states (Fs)
    Fs(lr,:)=F(lr,:) + S(lr)*(Qs(lr,:) - Q(lr,:))
    end do
    !...compute the flux at the j+1/2 cell based on the following condition statements
    if (S(1) >= 0.0_rfreal) then
      FLUX(:) = F(1,:)
     else if (S(1) <= 0.0_rfreal .AND. S_S >= 0.0_rfreal) then
      FLUX(:) = Fs(1,:)
     else if (S_S  <= 0.0_rfreal .AND. S(2) >= 0.0_rfreal) then
      FLUX(:) = Fs(2,:)
     else if (S(2) <=0.0_rfreal)then
      FLUX(:) = F(2,:)
    end if


   MAXSPEED = MAXVAL((/abs(S(1)),abs(S(2))/))

  end subroutine HLLC_FLUX

  subroutine LLF_FLUX(FLUX,MAXSPEED,Q,normalvec,tangentvec,dir,region,ng,flags)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    ! ... local variables
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    type(t_mixt_input), pointer :: input
    type(t_region), pointer :: region
    type(t_patch), pointer :: patch
    integer :: flags
    real(rfreal), pointer :: FLUX(:),Q(:,:)
    real(rfreal) :: F(2,MAX_ND+2), Fs(2,MAX_ND+2),Qs(2,Max_ND+2)
    real(rfreal), pointer :: normalvec(:),tangentvec(:,:)
    real(rfreal) :: P(2),a(2)
    integer :: ND, Nc, N(MAX_ND), ng, i, j, k, Np(MAX_ND), patchID
    integer :: R_xi(MAX_ND,MAX_ND),R_eta(MAX_ND,MAX_ND),R_zeta(MAX_ND,MAX_ND),R(MAX_ND,MAX_ND)
    integer :: ip, jp, kp,dir
    integer :: lr
    real(rfreal) :: source,un(2),ut1(2),ut2(2),S(2),S_S
    real(rfreal) :: MAXSPEED

    ! ... simplicity
    grid => region%grid(ng)
    state => region%state(ng)
    input => grid%input
    ND = grid%ND
    Nc = grid%nCells
    N  = 1
    do j = 1, grid%ND
      N(j) = grid%ie(j)-grid%is(j)+1
    end do ! j
 
  end subroutine LLF_FLUX



END MODULE ModRiemannSolver

