/* Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
   License: MIT, http://opensource.org/licenses/MIT
*/

#include <stdio.h>
#include "mesh_io.h"


/* Read an n-dimensional mesh from a file.

   fh - Handle of the file from which data will be read.
   offset - Offset (in bytes) from the beginning of the file where the first
     byte of the full mesh can be found.
   etype - datatype of each element in the mesh. If file_is_big_endian is
     MESH_IO_IGNORE_ENDIAN, then any datatype is allowed. Otherwise,
     only basic datatypes are supported.
   file_is_big_endian - one of four values:
     MESH_IO_LITTLE_ENDIAN - Data file is little-endian.
                             Swap bytes if CPU is not.
     MESH_IO_BIG_ENDIAN - Data file is big-endian.
                          Swap bytes if CPU is not.
     MESH_IO_IGNORE_ENDIAN - Don't worry about endianness; do no swapping.
     MESH_IO_SWAP_ENDIAN   - Data file is opposite endian; swap bytes.
   ndims - number of array dimensions (positive integer)
   buf - location of the mesh in memory
   mesh_sizes - number of elements in each dimension of the mesh being read
     (array of positive integers). These elements will be a subset of the
   file_mesh_sizes - number of elements in each dimension of the mesh
     as it is stored in the file.
   file_mesh_starts - number of elements in each dimension by which the
     submesh being read is inset from the origin of the file mesh.
   file_array_order - storage order of the mesh on disk.
     MPI_ORDER_C for row-major, MPI_ORDER_FORTRAN for column-major.
   memory_mesh_sizes - number of elements in each dimension of the full
     in-memory mesh
   memory_mesh_starts - number of elements in each dimension by which the
     submesh being written to memory is inset from the origin of the
     memory mesh.
   memory_array_order - storage order of the mesh in memory.
     MPI_ORDER_C for row-major, MPI_ORDER_FORTRAN for column-major.

  Returns:
    MPI_SUCCESS on success
    MPI_ERR_TYPE if etype is not a supported datatype
    MPI_ERR_BUFFER if buf is NULL
    MPI_ERR_DIMS if ndims is nonpositive
    MPI_ERR_ARG if:
      any of the array bounds are invalid, or
      the datatype has a nonpositive size or odd size (other than 1), or
      if file_endian is not one of the expected values.
    MPI_ERR_TRUNCATE if no data is read from the file
    If any MPI call fails, this returns the error code from that call.
*/
int Mesh_IO_read
(MPI_File fh,                   /* grid_fname opened in caller */
 MPI_Offset offset,             /* offset */
 MPI_Datatype etype,            /* double excepting iblank which is int */
 int file_endian,               /* determined in caller */
 void *buf,                     /* data destination buffer */
 int ndims,                     /* NDIM */
 const int *mesh_sizes,         /* grid%GlobelSize */
 const int *file_mesh_sizes,    /* grid%GlobalSize  */
 const int *file_mesh_starts,   /* is, ie */
 int file_array_order,          /* FORTRAN */
 const int *memory_mesh_sizes,  /* is, ie */
 const int *memory_mesh_starts, /* 0 0 0  */
 int memory_array_order,        /* FORTRAN */
 MPI_Comm ioComm) {

  int i, err = MPI_SUCCESS, rank;
  MPI_Datatype file_type, memory_type;
  size_t element_size;
  MPI_Status status;

  MPI_Comm_rank(ioComm, &rank);
  
  /* check for argument errors and initialize datatypes */
  err = readWriteInit(fh, offset, etype, file_endian, ndims, mesh_sizes,
                      file_mesh_sizes, file_mesh_starts, file_array_order,
                      memory_mesh_sizes, memory_mesh_starts, memory_array_order,
                      &file_type, &memory_type, &element_size);
  if (err != MPI_SUCCESS) return err;

  /* read the mesh */
  err = MPI_File_read_at_all(fh, 0, buf, 1, memory_type, &status);
  if (err != MPI_SUCCESS) goto fail;

  /* no data was read */
  MPI_Get_count(&status, memory_type, &i);
  if (i != 1) {
    err = MPI_ERR_TRUNCATE;
    goto fail;
  }

  /* Fix the endianness of the data */
  if (isEndianSwapNeeded(file_endian)) {
    Mesh_IO_endian_swap_in_place
      (buf, element_size, ndims,
       memory_mesh_sizes, mesh_sizes,
       memory_mesh_starts, memory_array_order);
  }
  
  /* free my datatypes */
 fail:
  MPI_Type_free(&memory_type);
  MPI_Type_free(&file_type);

  return err;
}


/* Write an n-dimensional mesh to a file.
   The arguments have the same meanings as those of Mesh_IO_read(),
   except the data will be written to the file rather than read.

   'buf' is not const because when byte-swapping (to correct an endian
   mismatch) a large amount of data, it may need to be byte-swapped
   in place rather than by allocating a secondary buffer. The data will
   be byte-swapped again before the function returns, so the data is left
   unchanged.
*/
int Mesh_IO_write
(MPI_File fh,                   /* grid_fname opened in caller */
 MPI_Offset offset,             /* offset */
 MPI_Datatype etype,            /* double excepting iblank which is int */
 int file_endian,               /* determined in caller */
 void *buf,                     /* data source buffer */
 int ndims,                     /* NDIM */
 const int *mesh_sizes,         /* grid%GlobalSize */
 const int *file_mesh_sizes,    /* grid%GlobalSize  */
 const int *file_mesh_starts,   /* is, ie */
 int file_array_order,          /* FORTRAN */
 const int *memory_mesh_sizes,  /* is, ie */
 const int *memory_mesh_starts, /* 0 0 0  */
 int memory_array_order,
 MPI_Comm ioComm) {      /* FORTRAN */

  int i, err = MPI_SUCCESS, rank, doEndianSwap;
  MPI_Datatype file_type, memory_type;
  size_t element_size, element_count;
  MPI_Status status;
  void *temp_buffer = NULL;
  char errorString[MPI_MAX_ERROR_STRING];
  int errorSize = 0;

  MPI_Comm_rank(ioComm, &rank);
  
  /* compute the total number of elements */
  element_count = 1;
  for (i=0; i < ndims; i++) element_count *= mesh_sizes[i];

  doEndianSwap = isEndianSwapNeeded(file_endian);

  /* Check for argument errors and initialize datatypes.
     If bytes need to be swapped; don't create the memory datatype. */
  err = readWriteInit(fh, offset, etype, file_endian, ndims, mesh_sizes,
                      file_mesh_sizes, file_mesh_starts, file_array_order,
                      memory_mesh_sizes, memory_mesh_starts, memory_array_order,
                      &file_type,
                      doEndianSwap ? NULL : &memory_type,
                      &element_size);
  if (err != MPI_SUCCESS) {
    MPI_Error_string(err,errorString,&errorSize);
    errorString[errorSize] = '\0';
    printf("readWriteInit: %s\n", errorString);
    return err;
  }


  if (doEndianSwap) {
    /* copy the data into a temp buffer and swap the bytes there. */
    /* XXX if the data gets large, rather then copying the data this 
       routine could swap the data in place */
    temp_buffer = malloc(element_count * element_size);
    if (!temp_buffer) {
      err = MPI_ERR_NO_MEM;
      goto fail;
    }

    /* XXX optimization: the byte swapping could be done at the same
       time the data is copied to the buffer. */
    
    Mesh_IO_copy_to_linear_array
      (temp_buffer, buf, element_size, ndims, memory_mesh_sizes,
       mesh_sizes, memory_mesh_starts, memory_array_order);

    reverseBytes(temp_buffer, element_size, element_count);
    
    MPI_Type_contiguous(element_count, etype, &memory_type);
    MPI_Type_commit(&memory_type);

    buf = temp_buffer;
  }

  /* write the mesh */
  err = MPI_File_write_at_all(fh, 0, buf, 1, memory_type, &status);
  /*if (err != MPI_SUCCESS) goto fail; */
  if (err != MPI_SUCCESS) {
    MPI_Error_string(err,errorString,&errorSize);
    errorString[errorSize] = '\0';
    printf("write_at_all: %s\n", errorString);
    /*    printf("write_at_all error %d\n", err); */ 
    goto fail;
  }

  /* no data was read */
  MPI_Get_count(&status, memory_type, &i);
  if (i != 1) {
    err = MPI_ERR_TRUNCATE;
    goto fail;
  }

 fail:
  free(temp_buffer);
  MPI_Type_free(&memory_type);
  MPI_Type_free(&file_type);

  return err;
}


/* Common initialization routine for Mesh_IO_read() and Mesh_IO_write().
   Checks for argument errors.  Sets file_type, memory_type, and
   element_size. */
static int readWriteInit
(MPI_File fh,
 MPI_Offset offset,
 MPI_Datatype etype,
 int file_endian,
 int ndims, 
 const int *mesh_sizes,
 const int *file_mesh_sizes,
 const int *file_mesh_starts,
 int file_array_order,
 const int *memory_mesh_sizes,
 const int *memory_mesh_starts,
 int memory_array_order,
 MPI_Datatype *file_type,
 MPI_Datatype *memory_type,
 size_t *element_size) {

  int i, err = MPI_SUCCESS;
  int rank;
  /* check for argument errors */
  if (ndims < 1) return MPI_ERR_DIMS;

  if (file_endian < MESH_IO_LITTLE_ENDIAN ||
      file_endian > MESH_IO_SWAP_ENDIAN){
    printf("endian problem\n");
    return MPI_ERR_ARG;
  }

  /* if endianness is to be considered, check for a basic datatype */
  if (file_endian != MESH_IO_IGNORE_ENDIAN
      && !isSupportedType(etype)) return MPI_ERR_TYPE;

  /* check for mesh boundary errors */
  for (i=0; i < ndims; i++) {
    int dimError = 0;
    if (file_mesh_starts[i] < 0){
      dimError++;
      printf("File mesh starts %d < 0\n",file_mesh_starts[i]);
    }
    if(file_mesh_starts[i] + mesh_sizes[i] > file_mesh_sizes[i]){
      dimError++;
      printf("File start + size > file size: (%d,%d,%d)\n",file_mesh_starts[i],mesh_sizes[i],file_mesh_sizes[i]);
    }
    if(memory_mesh_starts[i] < 0){
      dimError++;
      printf("memory mesh start < 0\n");
    }
    if(memory_mesh_starts[i] + mesh_sizes[i] > memory_mesh_sizes[i]){
      dimError++;
      printf("memory mesh start + size > memory size\n");
    }
    if (!(file_array_order == MPI_ORDER_FORTRAN ||
	  file_array_order == MPI_ORDER_C)
        || !(memory_array_order == MPI_ORDER_FORTRAN ||
             memory_array_order == MPI_ORDER_C)){
      printf("array order issue\n");
      dimError++;
    }
    if(dimError > 0){
      printf("Dimension %d problem\n",i);
      return MPI_ERR_ARG;
    }
  }

  /* get the size of each element */
  MPI_Type_size(etype, &i);
  *element_size = i;
  /* complain if it's nonpositive */
  if (*element_size < 1)
    return MPI_ERR_ARG;
  /* if endian swapping is to be done, complain if the size is larger
     than 1 and odd */
  if (file_endian != MESH_IO_IGNORE_ENDIAN
      && *element_size > 1
      && (*element_size & 1)){
    printf("Element size problem.\n");
    return MPI_ERR_ARG;    
  }

  MPI_Comm_rank(MPI_COMM_WORLD,&rank);
  /* define the shape of the mesh on disk (with no offset) */
  /*  printf("[%02d] on disk subarray G{%d,%d,%d} P{%d,%d,%d} O{%d,%d,%d}\n", rank,
         file_mesh_sizes[0], file_mesh_sizes[1], file_mesh_sizes[2],
         mesh_sizes[0], mesh_sizes[1], mesh_sizes[2],
         file_mesh_starts[0], file_mesh_starts[1], file_mesh_starts[2]);
  */
  err = MPI_Type_create_subarray
    (ndims, file_mesh_sizes, mesh_sizes, file_mesh_starts, file_array_order,
     etype, file_type);
  if (err != MPI_SUCCESS) goto fail0;

  err = MPI_Type_commit(file_type);
  if (err != MPI_SUCCESS) goto fail1;

  /* set the file view */
  err = MPI_File_set_view
    (fh, offset, *file_type, *file_type, "native", MPI_INFO_NULL);
  if (err != MPI_SUCCESS) goto fail1;

  /* define the shape of the mesh in memory */
  /* printf("[%02d] in memory subarray {%d,%d,%d} {%d,%d,%d} {%d,%d,%d}\n", rank,
         memory_mesh_sizes[0], memory_mesh_sizes[1], memory_mesh_sizes[2],
         mesh_sizes[0], mesh_sizes[1], mesh_sizes[2],
         memory_mesh_starts[0], memory_mesh_starts[1], memory_mesh_starts[2]); */
  if (memory_type != NULL) {
    err = MPI_Type_create_subarray
      (ndims, memory_mesh_sizes, mesh_sizes, memory_mesh_starts,
       memory_array_order, etype, memory_type);
    if (err != MPI_SUCCESS) goto fail1;
    
    err = MPI_Type_commit(memory_type);
    if (err != MPI_SUCCESS) goto fail2;
  }

  return MPI_SUCCESS;
  
  /* free my datatypes */
 fail2:
  MPI_Type_free(memory_type);
 fail1:
  MPI_Type_free(file_type);

 fail0:
  return err;
}  


/* Returns nonzero iff t is a type supported by Mesh_IO_read() */
static int isSupportedType(MPI_Datatype t) {
  return (t == MPI_CHAR
          || t == MPI_SHORT
          || t == MPI_INT
          || t == MPI_LONG
          || t == MPI_LONG_LONG_INT
          || t == MPI_LONG_LONG
          || t == MPI_SIGNED_CHAR
          || t == MPI_UNSIGNED_CHAR
          || t == MPI_UNSIGNED_SHORT
          || t == MPI_UNSIGNED
          || t == MPI_UNSIGNED_LONG
          || t == MPI_UNSIGNED_LONG_LONG
          || t == MPI_FLOAT
          || t == MPI_DOUBLE
          || t == MPI_LONG_DOUBLE
          || t == MPI_WCHAR
          || t == MPI_C_BOOL
          || t == MPI_INT8_T
          || t == MPI_INT16_T
          || t == MPI_INT32_T
          || t == MPI_INT64_T
          || t == MPI_UINT8_T
          || t == MPI_UINT16_T
          || t == MPI_UINT32_T
          || t == MPI_UINT64_T
          || t == MPI_AINT
          || t == MPI_OFFSET);
}


/* Returns 1 iff the current architectures is big-endian. */
static int isBigEndian() {
  int tmp = 1;
  return ! *(char*) &tmp;
}


static int isEndianSwapNeeded(int file_endian) {
  switch (file_endian) {
  case MESH_IO_LITTLE_ENDIAN: return isBigEndian();
  case MESH_IO_BIG_ENDIAN: return !isBigEndian();
  case MESH_IO_IGNORE_ENDIAN: return 0;
  case MESH_IO_SWAP_ENDIAN: return 1;
  default: /* shouldn't happen */ return 0;
  }
}

/* to cut down argument length, package up the arguments for
   Mesh_IO_traverseOneDim */
typedef struct {
  int ndims;

  /* for all i > first_full_dim, sub_mesh_sizes[i] == full_mesh_sizes[i] */
  int first_full_dim;

  size_t element_size;
  const int *sub_mesh_sizes, *full_mesh_sizes, *sub_mesh_starts;

  /* {full,sub}_mesh_sizes[i] = product of {full,sub}_mesh_sizes[i+1..ndim-1] */
  const int *full_cum_sizes, *sub_cum_sizes;

  Mesh_IO_traverse_fn rowFunction;
  void *rowFunctionParam;

} MeshTraverseParams;


/* Helper function used inside Mesh_IO_traverse; calls itself recursively
   for each dimension. */
static void Mesh_IO_traverseOneDim(int dim_no, char *buf,
                                   const MeshTraverseParams *p) {
  int i, start=p->sub_mesh_starts[dim_no], count = p->sub_mesh_sizes[dim_no];
  buf += p->element_size * start * p->full_cum_sizes[dim_no];

  if (dim_no+1 >= p->first_full_dim) {

    /* call the user function on each contigous chunk */
    p->rowFunction(buf, count * p->sub_cum_sizes[dim_no], p->rowFunctionParam);

  } else {
    for (i = start; i < start + count; i++) {
      Mesh_IO_traverseOneDim(dim_no+1, buf, p);
      buf += p->element_size * p->full_cum_sizes[dim_no];
    }
  }
}
  

/* Traverse an n-dimensional array, calling the given function on
   each contiguous segment of data. */
static size_t Mesh_IO_traverse
(void *buf, size_t element_size, int ndims,
 const int *full_mesh_sizes, const int *sub_mesh_sizes,
 const int *sub_mesh_starts, int array_order,
 Mesh_IO_traverse_fn rowFunction, void *rowFunctionParam) {

  int i, first_full_dim, *full_cum_sizes, *sub_cum_sizes;

  /* do nothing if the element size is less than 1 */
  if (element_size < 1) return 0;

  /* reverse the dimension order if it's column-major */
  if (array_order != MPI_ORDER_C) {
    sub_mesh_sizes = createReversedArrayCopy(ndims, sub_mesh_sizes);
    full_mesh_sizes = createReversedArrayCopy(ndims, full_mesh_sizes);
    sub_mesh_starts = createReversedArrayCopy(ndims, sub_mesh_starts);
  }

  /* Summarize the size of each dimension as the product of the remaining
     dimensions. For example, with a C array foo[2][3][5]:
       size[0] == 15, size[1] == 5, size[2] == 1
  */
  full_cum_sizes = (int*) malloc(sizeof(int) * ndims);
  sub_cum_sizes  = (int*) malloc(sizeof(int) * ndims);
  assert(full_cum_sizes);
  assert(sub_cum_sizes);
  full_cum_sizes[ndims-1] = sub_cum_sizes[ndims-1] = 1;
  for (i=ndims-2; i >= 0; i--) {
    full_cum_sizes[i] = full_cum_sizes[i+1] * full_mesh_sizes[i+1];
    sub_cum_sizes[i]  = sub_cum_sizes[i+1]  * sub_mesh_sizes[i+1];
  }

  /* Figure out the dimension after which the storage is contiguous.
     For example, with full_mesh_sizes={5,5,5,5,5,5} and
     sub_mesh_sizes={4,3,2,1,5,5}
     the last two dimensions are contiguous.  first_full_dim is the
     index after which all dimensions are full so it will be 3.
 */
  for (i = ndims; i > 0; i--)
    if (sub_mesh_sizes[i-1] != full_mesh_sizes[i-1]) break;
  first_full_dim = i;

  /* use a recursive function to traverse the dimensions */
  {
    MeshTraverseParams params = {
      ndims, first_full_dim, element_size, sub_mesh_sizes, full_mesh_sizes,
      sub_mesh_starts, full_cum_sizes, sub_cum_sizes, rowFunction,
      rowFunctionParam
    };
    Mesh_IO_traverseOneDim(0, (char*)buf, &params);
  }

  /* free the copies I made of the shape arrays */
  if (array_order != MPI_ORDER_C) {
    /* strip off 'const' */
    free((int*)sub_mesh_sizes);
    free((int*)full_mesh_sizes);
    free((int*)sub_mesh_starts);
  }
  free(full_cum_sizes);
  free(sub_cum_sizes);

  /* return the nubmer of elements processed */
  return sub_cum_sizes[0] * sub_mesh_sizes[0];
}


static void rowFnEndianSwapInPlace(void *p, size_t count, void *param) {
  size_t element_size = *(size_t*)param;
  reverseBytes(p, element_size, count);
}


static size_t Mesh_IO_endian_swap_in_place
(void *buf, size_t element_size, int ndims,
 const int *full_mesh_sizes, const int *sub_mesh_sizes,
 const int *sub_mesh_starts, int array_order) {

  return Mesh_IO_traverse
    (buf, element_size, ndims, full_mesh_sizes, sub_mesh_sizes,
     sub_mesh_starts, array_order, rowFnEndianSwapInPlace,
     &element_size);
}


typedef struct {
  char *write_ptr;
  size_t element_size;
} CopyToLinearParam;

static void rowFnCopyToLinear(void *p, size_t count, void *param_v) {
  CopyToLinearParam *param = (CopyToLinearParam*)param_v;
  size_t len = param->element_size * count;
  memcpy(param->write_ptr, p, len);
  param->write_ptr += len;
}


static size_t Mesh_IO_copy_to_linear_array
(void *dest, const void *src, size_t element_size, int ndims,
 const int *full_mesh_sizes, const int *sub_mesh_sizes,
 const int *sub_mesh_starts, int array_order) {

  CopyToLinearParam param = {(char*)dest, element_size};
  
  return Mesh_IO_traverse
    ((void*)src, element_size, ndims, full_mesh_sizes, sub_mesh_sizes,
     sub_mesh_starts, array_order, rowFnCopyToLinear, &param);
}


/*
  Speed in GiB/sec       element size (bytes)
                          2      4      8     16
  Core i7@3.40GHz
    GCC 4.8            3700   7500  13500  14800
  Xeon E5-2660
    GCC 4.9.2         14000   6200  11400  12600
    Intel icpc 15.0   14100   6600  12000  13300
  AMD Opteron 6276 Interlagos (Blue Waters):
    craycc             2700   3700   4600   4500
    pgic v15           1200   3700   2800   1000
    pgic v16           1200   3600   2800   1000
    icpc v15           3700   3000   3800   3900
    icpc v16           4100   3200   4000   4200
    gcc 4.9.3          4600   3400   4100   4700
    gcc 5.3.0          4600   3400   4400   4700

  There are also compiler intrinsics __builtin_bswap{16,32,64} on
  GCC 4.8 and up and on ICPC, but their performance seems identical 
  to the macros below.  Same thing with htobe{16,32,64} macros in endian.h

  Ed Karrels, edk@illinois.edu
*/
#define BSWAP_16(x)                             \
  (((uint16_t)(x) << 8) |                       \
   ((uint32_t)(x) >> 8))

#define BSWAP_32(x)                                       \
  (((uint32_t)(x) << 24) |                                \
   (((uint32_t)(x) << 8) & 0xff0000) |                    \
   (((uint32_t)(x) >> 8) & 0xff00) |                      \
   ((uint32_t)(x) >> 24))

#define BSWAP_64(x)                                                   \
  (((uint64_t)(x) << 56) |                                            \
   (((uint64_t)(x) << 40) & 0xff000000000000ULL) |                    \
   (((uint64_t)(x) << 24) & 0xff0000000000ULL) |                      \
   (((uint64_t)(x) <<  8) & 0xff00000000ULL) |                        \
   (((uint64_t)(x) >>  8) & 0xff000000ULL) |                          \
   (((uint64_t)(x) >> 24) & 0xff0000ULL) |                            \
   (((uint64_t)(x) >> 40) & 0xff00ULL) |                              \
   ((uint64_t)(x) >> 56))

#define	BSWAP_32_USING_64(x)                       \
  (((uint64_t)(x) & 0xff000000ffull) << 24 |       \
   ((uint64_t)(x) & 0xff000000ff00ull) << 8 |      \
   ((uint64_t)(x) & 0xff000000ff0000ull) >> 8 |    \
   ((uint64_t)(x) & 0xff000000ff000000ull) >> 24)


#define SWAP(a,b) temp=(a); (a)=(b); (b)=temp;


void reverseBytes(void *v, int element_size, size_t count) {
  if (element_size < 2) return;

  switch (element_size) {
  case 2: {
    uint16_t *p = (uint16_t*)v, *end;
    end = p + count;
    for (; p < end; p++)
      *p = BSWAP_16(*p);
    break;
  }

  case 4: {
    uint32_t *p = (uint32_t*)v, *end = p + count;
    uint64_t *p64, *p64end;

    /* handle unaligned prefix */
    if ((uintptr_t)p & 7) {
      *p = BSWAP_32(*p);
      p++;
    }

    /* process two 4-byte words at a time */
    p64 = (uint64_t*) p;
    p64end = (uint64_t*) ((uintptr_t)end & ~7);
    for (; p64 < p64end; p64++)
      *p64 = BSWAP_32_USING_64(*p64);
    
    p = (uint32_t*) p64;

    /* handle unaligned suffix */
    if (p < end)
      *p = BSWAP_32(*p);
    break;
  }
    

  case 8: {
    uint64_t *p = (uint64_t*)v, *end;
    end = p + count;
    for (; p < end; p++)
      *p = BSWAP_64(*p);
    break;
  }

  case 16: {
    uint64_t *p = (uint64_t*)v, *end, tmp;
    end = p + count*2;
    for (; p < end; p += 2) {
      tmp = BSWAP_64(*p);
      *p = BSWAP_64(*(p+1));
      *(p+1) = tmp;
    }
    break;
  }

  default: {
    /* Written by Daniel J. Bodony (bodony@Stanford.EDU)
       Copyright (c) 2001 */
    size_t i;
    int x;
    char *a, temp;
    
    for (i = 0; i < count; i++) {
      a = (char *)v + i*element_size;
      for (x = 0; x < element_size/2; x++) {
        SWAP(a[x],a[element_size-x-1]);
      }
    }
  }
  }
}


static int *createReversedArrayCopy(int len, const int *array) {
  int i, *copy = (int*) malloc(sizeof(int) * len);
  assert(copy);

  for (i=0; i < len; i++)
    copy[len - 1 - i] = array[i];

  return copy;
}
