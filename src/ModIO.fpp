! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
!-----------------------------------------------------------------------
!
! ModIO.f90
!
! - basic code for the input/output
!
! Revision history
! - 20 Dec 2006 : DJB : initial code
! - 29 Dec 2006 : DJB : evolution
!
! $Header: /cvsroot/genx/Codes/RocfloCM/Source/ModIO.f90,v 1.53 2011/10/20 16:55:21 bodony Exp $
!
!-----------------------------------------------------------------------
Module ModIO

  USE ModDataStruct

  INTEGER, PARAMETER            :: FORMAT_P3D=0, FORMAT_HDF5=1
  TYPE(t_io_descriptor), TARGET :: ioDescriptors(4)

CONTAINS

  INTEGER FUNCTION FileFormat(fname)

    IMPLICIT NONE

    CHARACTER(LEN=*), INTENT(IN) :: fname

    INTEGER :: idot
    CHARACTER(LEN=1) :: extension

    idot = INDEX(fname,'.',BACK=.TRUE.)
    extension = fname(idot+1:idot+1)
    IF(extension == 'h') THEN
       FILEFORMAT=FORMAT_HDF5
    ELSE
       FILEFORMAT=FORMAT_P3D
    ENDIF

  END FUNCTION FILEFORMAT

  subroutine ReadRestart(region, fnames)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    USE ModPLOT3D_IO
    USE ModHDF5_IO

    Implicit None

    Type (t_region), Pointer :: region
    Character(LEN=*) :: fnames(2)
    INTEGER :: gridFormat

    gridFormat = FileFormat(fnames(1))
    IF(gridFormat == FORMAT_HDF5) THEN
#ifdef HAVE_HDF5
       CALL ReadRestart_HDF5(region, fnames(1))
#else
       STOP 'HDF5 not enabled.'
#endif
    ELSE IF(gridFormat == FORMAT_P3D) THEN
       CALL ReadRestart_P3D(region,fnames)
    ENDIF

  end subroutine ReadRestart

  subroutine InitializeState(region, restart_fname)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    USE ModDeriv
    USE ModInitialCondition
    USE ModEOS
    USE ModCombustion
    USE ModInterp
!   USE ModDeriv

    Implicit None

    type(t_region), pointer :: region
    character(len=PATH_LENGTH) :: restart_fname(2)

    ! ... local variables
    type(t_mixt_input), pointer :: input
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state, mean
    Real(rfreal), Dimension(:,:,:,:,:), Pointer :: X
    Integer, Dimension(:,:,:,:), Pointer :: IBLANK
    Character(LEN=2) :: prec, gf, vf, ib
    Integer :: ngrid, I, J, K, L0, Nx, Ny, Nz, NDIM, M, ftype, ng, alloc_stat, ierr
    Integer, Dimension(:,:), Pointer :: ND
    Real(rfreal) :: exp_fac, vortex_u, vortex_v, vortex_rho, vortex_p, M_vortex
    Real(rfreal) :: L_vortex, gamma, bg_u, bg_v, bg_rho, bg_p, x0, y0, r, u_inf
    Real(rfreal) :: L_pulse_x, L_pulse_y, A_pulse
    Real(rfreal) :: bg_temperature, hotspot_pressure, hotspot_ux, hotspot_uy, hotspot_temperature
    Real(rfreal) :: hotspot_density, hotspot_rhoE, new_pressure, new_temperature, new_ux, new_uy
    Real(rfreal) :: new_density, new_rhoE, timer
    real(rfreal), pointer :: ptr_to_data(:)
    real(rfreal), pointer :: iblank_dble(:)
    
    ! HDF5 variables
    INTEGER :: gridFormat
    
    ! ... allocate state datatype
    if (associated(region%state) .eqv. .false.) allocate(region%state(region%nGrids))
    allocate(region%mean(region%nGrids))

    ! ... setup the state
    do ng = 1, region%nGrids

      ! ... this grid
      grid  => region%grid(ng)
      state => region%state(ng)
      input => grid%input
      mean  => region%mean(ng)

      ! ... setup the combustion module first (MODEL0 & MODEL1 overwrite inputs)
      !call combustionInit(region, ng)

      ! ... global parameters
      state%RE = input%RE
      state%REinv = 0.0_rfreal
      state%SC = input%SC
      state%SCinv = 0.0_rfreal
      if (state%RE > 0.0_rfreal) state%REinv = 1.0_rfreal / state%RE
      if (state%SC > 0.0_rfreal) state%SCinv = 1.0_rfreal / state%SC
      state%PR = input%PR;
      state%PRinv = 1.0_rfreal / state%PR
      state%NVARS = grid%ND+2

      allocate(state%cv(grid%nCells,input%nCv),stat=alloc_stat)
      if (alloc_stat /= 0) call graceful_exit(region%myrank, 'PlasComCM: could not allocate state%cv');
      allocate(state%tv(grid%nCells,input%nTv),stat=alloc_stat)
      if (alloc_stat /= 0) call graceful_exit(region%myrank, 'PlasComCM: could not allocate state%tv');
      allocate(state%dv(grid%nCells,input%nDv),stat=alloc_stat)
      if (alloc_stat /= 0) call graceful_exit(region%myrank, 'PlasComCM: could not allocate state%dv');
      allocate(state%gv(grid%nCells,input%nGv),stat=alloc_stat)
      if (alloc_stat /= 0) call graceful_exit(region%myrank, 'PlasComCM: could not allocate state%gv');
      allocate(state%time(grid%nCells),stat=alloc_stat)
      if (alloc_stat /= 0) call graceful_exit(region%myrank, 'PlasComCM: could not allocate state%time');
      allocate(state%cfl(grid%nCells),stat=alloc_stat)
      if (alloc_stat /= 0) call graceful_exit(region%myrank, 'PlasComCM: could not allocate state%cfl');
      allocate(state%cvTarget(size(state%cv,1),size(state%cv,2)),stat=alloc_stat)
      if (alloc_stat /= 0) call graceful_exit(region%myrank, 'PlasComCM: could not allocate state%cvTarget');

      allocate(state%MolWeight(grid%nCells),stat=alloc_stat)
      if (alloc_stat /= 0) call graceful_exit(region%myrank, 'PlasComCM: could not allocate state%MolWeight');

      ! ... mean quantities
      if (input%fluidModel == LINCNS) then
        allocate(mean%cv(grid%nCells,input%nCv))
        allocate(mean%dv(grid%nCells,input%nDv))
        allocate(mean%gv(grid%nCells,input%nGv))
      end if

      ! ... nullify a number of pointers for better memory management
      nullify(state%cvOld)
      nullify(state%rhs)
      nullify(state%rhs_explicit)
      nullify(state%rhs_implicit)
      nullify(state%rk_rhs)
      nullify(state%timeOld)
      nullify(state%dt)
      nullify(state%cvTargetOld)
      nullify(state%flux)
      nullify(state%dflux)
      nullify(state%VelGrad1st)
      nullify(state%TempGrad1st)
      nullify(state%MagStrnRt)
      nullify(state%tvCor)
      nullify(state%muT)
      nullify(state%PrT)
      nullify(state%SGS_KE)

      ! ... Adjoint N-S or adjoint-based control optimization
      if (input%AdjNS .OR. input%AdjOptim) then
        ! ... followings not used in standalone adjoint N-S solver
        if (input%AdjNS) then
          deallocate(state%cvTarget)
          nullify(state%cvTarget)
        end if ! input%AdjNS

        allocate(state%av(grid%nCells,input%nCv),stat=alloc_stat)
        if (alloc_stat /= 0) call graceful_exit(region%myrank, 'could not allocate state%av');
!!$     allocate(state%avOld(grid%nCells,input%nCv),stat=alloc_stat)
!!$     if (alloc_stat /= 0) call graceful_exit(region%myrank, 'could not allocate state%avOld');

        allocate(state%avTarget(size(state%av,1),size(state%av,2)),stat=alloc_stat)
        if (alloc_stat /= 0) call graceful_exit(region%myrank, 'could not allocate state%avTarget');
!!$     allocate(state%avTargetOld(size(state%av,1),size(state%av,2)),stat=alloc_stat)
!!$     if (alloc_stat /= 0) call graceful_exit(region%myrank, 'could not allocate state%avTargetOld');

        allocate(state%cvOld(grid%nCells,input%nCv),stat=alloc_stat)
        if (alloc_stat /= 0) call graceful_exit(region%myrank, 'could not allocate state%cvOld');
        allocate(state%cvNew(grid%nCells,input%nCv),stat=alloc_stat)
        if (alloc_stat /= 0) call graceful_exit(region%myrank, 'could not allocate state%cvNew');

        nullify(state%avOld)
        nullify(state%avTargetOld)
      end if ! input%AdjNS

      ! ... species
      nullify(state%auxVars)
      nullify(state%auxVarsSteady)
      nullify(state%rhs_auxVars)
      nullify(state%levelSet)
      if (input%nAuxVars > 0) then

        ! ... auxillary variables for chemistry
        allocate(state%auxVars(grid%nCells,input%nAuxVars))
        allocate(state%auxVarsSteady(grid%nCells,input%nAuxVarsSteady))
        allocate(state%rhs_auxVars(grid%nCells,input%nAuxVars))
        allocate(state%auxVarsTarget(grid%nCells,input%nAuxVars))

        ! ... initial area function
        if (input%fluidModel == Q1D) then
          if (.not.(associated(grid%area) .eqv. .true.)) allocate(grid%area(grid%nCells))

          ! ... CAA Benchmark 3, Category 1, Problem 2
          if (.false.) then
            do i = 1, grid%nCells
              if (grid%XYZ(i,1) >= 0) then
                grid%area(i) = 0.536572_dp - 0.198086_dp * exp(-log(2.0_dp)*(grid%xyz(i,1)/0.6_dp)**2)
              else
                grid%area(i) = 1.0_dp - 0.661514_dp * exp(-log(2.0_dp)*(grid%xyz(i,1)/0.6_dp)**2)
              end if
            end do
          end if

          ! ... Joanna's shock tube, normalized by the radius
          if (.false.) then
            do i = 1, grid%nCells
              grid%area(i) = 0.5_dp * TWOPI
              state%auxVars(i,1) = 1.5e-5
              state%auxVars(i,2) = 1.0e-5
            end do
          end if
        end if

      end if

    end do

    ! ... read the mean
    if (region%input%fluidModel == LINCNS) then
       CALL MPI_BARRIER(MYCOMM,ierr)
       IF((debug_on .eqv. .true.) .and. (region%myrank == 0)) write(*,*) 'PlasComCM: Reading mean.'
       call ReadMean(region, region%input%mean_fname)
       CALL MPI_BARRIER(MYCOMM,ierr)
       IF((debug_on .eqv. .true.) .and. (region%myrank == 0)) write(*,*) 'PlasComCM: Done reading mean.'
      do ng = 1, region%nGrids
        grid  => region%grid(ng)
        input => grid%input
        mean  => region%mean(ng)
        do i = 1, grid%nCells
          mean%gv(i,1) = input%GamRef
!!$          mean%cv(i,1) = 1.0_8
!!$          mean%cv(i,2) = 0.0_8
!!$          mean%cv(i,3) = 0.0_8
!!$          mean%cv(i,4) = (1.0_8/input%GamRef)/(input%GamRef-1.0_8)
        end do
        call computeDv(region%myrank, grid, mean)
        if (input%bcic_eigenfunction == TRUE) call read_eigenfunction(region%myrank, grid, input)
        if (input%bcic_eigenfunction == PSE ) call read_PSEeigenfunction(grid, input)
      end do
    end if

    ! ... initialize state if no restart file
    if (region%global%USE_RESTART_FILE == FALSE) Then
      IF((debug_on .eqv. .true.) .and. (region%myrank == 0)) write(*,*) 'PlasComCM: Not a restart'
      if (region%input%fluidModel == LINCNS) then
        call LINInitialCondition(region)
      else
        IF((debug_on .eqv. .true.) .and. (region%myrank == 0)) write(*,*) 'PlasComCM: Setting IC'
        call InitialCondition(region)
        IF((debug_on .eqv. .true.) .and. (region%myrank == 0)) write(*,*) 'PlasComCM: IC done.'
      end if

      do ng = 1, region%nGrids

        grid  => region%grid(ng)
        state => region%state(ng)
        input => grid%input

        ! ... Adjoint N-S
        if (input%AdjNS .OR. input%AdjOptim) then
          ! ... save state for target
          do j = 1, size(state%av,2)
            do i = 1, size(state%av,1)
              state%avTarget(i,j) = state%av(i,j)
            end do
          end do
        end if ! input%AdjNS

        ! ... Forward N-S or adjoint-based control optimization
        if ((.NOT. input%AdjNS) .OR. input%AdjOptim) then
          ! ... save state for target
          if (trim(input%initflow_name) .ne. 'NASA_JET') then
            do j = 1, size(state%cv,2)
              do i = 1, size(state%cv,1)
                state%cvTarget(i,j) = state%cv(i,j)
              end do
            end do
          end if 
          do j = 1, input%nAuxVars
            do i = 1, grid%nCells
              state%auxVarsTarget(i,j) = state%auxVars(i,j)
            end do
          end do
        end if ! .NOT.

      end do

      ! ... read the target file
      if (input%readTargetOnStartup == TRUE) then
        if (input%readTargetOnStartup == TRUE) then
           IF(input%read_nothing == FALSE) THEN
              IF((debug_on .eqv. .true.).and.(region%myrank == 0)) write(*,*) 'PlasComCM: Reading target...'
              call ReadTarget(region,region%input%target_fname)
              IF((debug_on .eqv. .true.).and.(region%myrank == 0)) write(*,*) 'PlasComCM: Reading target done.'
           ENDIF
        endif
        if (input%AdjOptim) then ! ... if optimization, above read_target reads 
                                 ! ... state%cvTarget since input%AdjNS = .FALSE.
          do ng = 1, region%nGrids
            state => region%state(ng)

            do j = 1, size(state%av,2)
              do i = 1, size(state%av,1)
                state%avTarget(i,j) = 0.0_rfreal ! ... this is ad-hoc
              end do
            end do
          end do ! ng
        end if ! ... input%AdjOptim
      end if ! input%readTargetOnStartup
      CALL MPI_BARRIER(MYCOMM,ierr)

    else

      ! ... loop over all of the grids
      do ng = 1, region%nGrids

        grid  => region%grid(ng)
        state => region%state(ng)
        input => grid%input

        do k = 1, grid%nCells
          ! ... set the time
          state%time(k) = 0.0_rfreal

          ! ... initialize gas variables
          state%gv(k,1) = input%GamRef

          ! ... initialize CFL
          state%cfl(k)  = input%cfl
        end do

        ! ... else read the restart file
        do j = 1, input%nCv
          do i = 1, grid%nCells
            state%cv(i,j) = 0.0_rfreal
          end do
        end do
        do j = 1, input%nDv
          do i = 1, grid%nCells
            state%dv(i,j) = 0.0_rfreal
          end do
        end do
! ... special initialization for MODEL1
        if (region%input%chemistry_model == MODEL1) then
          do i = 1, grid%nCells
            state%dv(i,1) = 1.0_rfreal/input%gamref
            state%dv(i,2) = 1.0_rfreal/(input%gamref-1.0_rfreal)
            state%dv(i,3) = 1.0_rfreal
          end do
        endif
        do j = 1, input%nAuxVars
          do i = 1, grid%nCells
            state%auxVars(i,j) = 0.0_rfreal
          end do
        end do
        if (region%input%chemistry_model == MODEL1) then
          do j = 1, input%nAuxVarsSteady
            do i = 1, grid%nCells
              state%auxVarsSteady(i,j) = 0.0_rfreal
            end do
          end do
        endif

      end do
   
      IF(input%read_nothing == FALSE) THEN
         IF((debug_on.eqv..true.).and.(region%myrank == 0)) write(*,*) 'PlasComCM: Reading restart and target'
         timer = MPI_WTime()
         Call ReadRestart(region, restart_fname)
         CALL MPI_BARRIER(MYCOMM,ierr)
         Call ReadTarget(region, region%input%target_fname)
         CALL MPI_BARRIER(MYCOMM,ierr)
         region%mpi_timings(:)%io_read_restart = region%mpi_timings(:)%io_read_restart + (MPI_Wtime()-timer)
         IF((debug_on.eqv..true.).and.(region%myrank == 0)) write(*,*) 'PlasComCM (InitializeState): Restart read.'
      ENDIF

      ! ... Since each proc is reading only the unique data, we need to do a halo
      ! ... exchange here, WZhang 12/2014
      Do ng = 1, region%nGrids

        grid  => region%grid(ng)
        state => region%state(ng)
        input => grid%input

        ! ... cv
        do i = 1, size(state%cv,2)
          ptr_to_data => state%cv(:,i)
          Call Ghost_Cell_Exchange_Box(region, ng, ptr_to_data)
        end do

        ! ... cvTarget
        do i = 1, size(state%cvTarget,2)
          ptr_to_data => state%cvTarget(:,i)
          Call Ghost_Cell_Exchange_Box(region, ng, ptr_to_data)
        end do

        if (input%nAuxVars > 0) then      
          ! ... auxVars
          do i = 1, size(state%auxVars,2)
            ptr_to_data => state%auxVars(:,i)
            Call Ghost_Cell_Exchange_Box(region, ng, ptr_to_data)
          end do

          ! ... auxVarsTarget
          do i = 1, size(state%auxVarsTarget,2)
            ptr_to_data => state%auxVarsTarget(:,i)
            Call Ghost_Cell_Exchange_Box(region, ng, ptr_to_data)
          end do
        end if

      End do
      IF((debug_on.eqv..true.).and.(region%myrank == 0)) write(*,*) 'PlasComCM: Restart and target read.'
      if (input%AdjOptim) then ! ... if optimization, above read_target reads 
                               ! ... state%cvTarget since input%AdjNS = .FALSE.
        do ng = 1, region%nGrids
          state => region%state(ng)

          do j = 1, size(state%av,2)
            do i = 1, size(state%av,1)
              state%avTarget(i,j) = 0.0_rfreal ! ... this is ad-hoc
            end do
          end do
        end do ! ng
      end if ! ... input%AdjOptim

!!$      if ( input%nstepi == 4200000 ) then
!!$        if ( region%myrank == 0 ) write (*,'(A)') '----> Adding hotspot <----'
!!$        Call Add_Hotspot(region)
!!$      end if

    end if

    ! set the basic target state
    do ng = 1, region%nGrids

      state => region%state(ng)
      grid  => region%grid(ng)
      input => grid%input

      ! ... Adjoint N-S
      if (input%AdjNS .OR. input%AdjOptim) then
        if (.not.associated(state%avTargetOld) .eqv. .true.) then
          allocate(state%avTargetOld(size(state%avTarget,1), size(state%avTarget,2)))
        end if
        do j = 1, size(state%avTarget,2)
          do i = 1, size(state%avTarget,1)
            state%avTargetOld(i,j) = state%avTarget(i,j)
          end do
        end do
      end if ! input%AdjNS

      ! ... Forward N-S or adjoint-based control optimization
      if ((.NOT. input%AdjNS) .OR. input%AdjOptim) then
        if (.not.associated(state%cvTargetOld) .eqv. .true.) then
          allocate(state%cvTargetOld(size(state%cvTarget,1), size(state%cvTarget,2)))
        end if
        do j = 1, size(state%cvTarget,2)
          do i = 1, size(state%cvTarget,1)
            state%cvTargetOld(i,j) = state%cvTarget(i,j)
          end do
        end do
!!$        do j = 1, input%nAuxVars
!!$          do i = 1, grid%nCells
!!$            state%auxVarsTarget(i,j) = 0.0_8
!!$          end do
!!$        end do
      end if ! .NOT.
    end do

    ! ... reset all values based on IBLANK once
    do ng = 1, region%nGrids
      grid  => region%grid(ng)
      state => region%state(ng)
      input => grid%input
      if ((input%fluidModel == CNS .OR. input%fluidModel == Q1D) .and. (input%nAuxVars == 0)) then
        do i = 1, grid%nCells
          if (grid%iblank(i) == 0) then
            state%cv(i,1)         = 1.0_rfreal
            state%cv(i,grid%ND+2) = (1.0_rfreal / state%gv(i,1)) / (state%gv(i,1) - 1.0_rfreal)
            do j = 1, grid%ND
              state%cv(i,j+1) = 0.0_rfreal
            end do
          end if
        end do
      else if (input%fluidModel == LINCNS) then
        do i = 1, grid%nCells
          if (grid%iblank(i) == 0) then
            state%cv(i,1)         = 0.0_rfreal
            state%cv(i,grid%ND+2) = 0.0_rfreal
            do j = 1, grid%ND
              state%cv(i,j+1) = 0.0_rfreal
            end do
          end if
        end do
      end if
    end do

    if (region%input%gridType == CHIMERA .and. region%input%volumeIntersection == TRUE) then
      call Setup_Interpolated_State(region)
    end if

    ! PPP: added a call to routine for conversion between Model0 and Model1 rhoE and rho definitions
    if ( (region%input%chemistry_model == MODEL1).and.(region%input%chemModelConvert)) then
       do ng = 1, region%nGrids
          grid => region%grid(ng)
          state => region%state(ng)
          input => grid%input         
          call Model0_to_Model1(input,grid,state)
       end do
    end if ! PPP: end Model0 to Model1 conversion routine call

    CALL MPI_BARRIER(MYCOMM,ierr)
    IF((debug_on.eqv..true.).and.(region%myrank == 0)) write(*,*) 'PlasComCM: All processors done with InitializeState'
  end subroutine InitializeState

  Subroutine AssembleVarsToWrite(input, vars_to_write, main_ts_loop_index, rst_flag)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    ! ... input/output variables
    character(len=3), pointer, dimension(:) :: vars_to_write
    integer :: main_ts_loop_index
    type(t_mixt_input), pointer :: input
    integer, optional :: rst_flag

    ! ... local variables
    integer :: num_vars_to_write, counter
    integer :: restart = FALSE
    ! ... determine number of vars to write
    ! ... special case for rst
    if (present(rst_flag)) restart = rst_flag
    !        allocate(vars_to_write(1))
    !        vars_to_write(1) = 'rst'
    !        return
    !      end if
    !    end if
    
    ! ... default case
    ! ... write grid only if either (a) on main_ts_loop_index = 0 or (b) grid is moving
    if (main_ts_loop_index == 0 .or. input%moveGrid == TRUE) then
      num_vars_to_write = 2 ! xyz, cv
    else
      num_vars_to_write = 1 ! cv
    end if

    if (input%nAuxVars > 0) then
       num_vars_to_write = num_vars_to_write + 1
       if(restart == TRUE) num_vars_to_write = num_vars_to_write + 1 ! aut
    endif

#ifdef BUILD_ELECTRIC
    if (input%write_phi == TRUE) num_vars_to_write = num_vars_to_write + 1
#endif
    IF(restart == TRUE) num_vars_to_write = num_vars_to_write + 1 ! cvt i guess

    IF(restart == FALSE) THEN
       if (input%write_dv  == TRUE) num_vars_to_write = num_vars_to_write + 1
       if (input%write_cvt == TRUE .or. main_ts_loop_index == 0) num_vars_to_write = num_vars_to_write + 1
       if (input%write_met == TRUE) num_vars_to_write = num_vars_to_write + 1
       if (input%write_jac == TRUE) num_vars_to_write = num_vars_to_write + 1
       if (input%write_mid == TRUE) num_vars_to_write = num_vars_to_write + 1
    ENDIF
    ! ... allocate 
    allocate(vars_to_write(num_vars_to_write))

    ! ... now fill the array
    if (main_ts_loop_index == 0 .or. input%moveGrid == TRUE) then
      vars_to_write(1:2) = (/ 'xyz', 'cv ' /)
      counter = 2
    else
      vars_to_write(1:1) = (/ 'cv ' /)
      counter = 1
    end if

    IF(restart == TRUE .AND. main_ts_loop_index > 0) THEN
       counter = counter + 1
       vars_to_write(counter:counter) = 'cvt'
    ENDIF
#ifdef BUILD_ELECTRIC
    if (input%write_phi == TRUE) then
      counter = counter + 1
      vars_to_write(counter:counter) = 'phi'
    end if
#endif

    if (input%nAuxVars > 0) then
      counter = counter + 1
      vars_to_write(counter:counter) = 'aux'
      IF(restart == TRUE) THEN
         counter = counter + 1
         vars_to_write(counter:counter) = 'aut'
      ENDIF
    end if


    IF(restart == FALSE) THEN
       if (input%write_cvt == TRUE .or. main_ts_loop_index == 0) then
          counter = counter + 1
          vars_to_write(counter:counter) = 'cvt'
       end if
       
       if (input%write_dv == TRUE) then
          counter = counter + 1
          vars_to_write(counter:counter) = 'dv '
       end if
       
       
       if (input%write_met == TRUE) then
          counter = counter + 1
          vars_to_write(counter:counter) = 'met'
       end if
       
       if (input%write_jac == TRUE) then
          counter = counter + 1
          vars_to_write(counter:counter) = 'jac'
       end if
       
       if (input%write_mid == TRUE) then
          counter = counter + 1
          vars_to_write(counter:counter) = 'mid'
       end if
    END IF

  End Subroutine AssembleVarsToWrite


  SUBROUTINE WriteOutput(region, restart)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    USE ModPLOT3D_IO
    USE ModHDF5_IO

    IMPLICIT NONE

    TYPE(t_region), POINTER :: region
    INTEGER, OPTIONAL :: restart

    ! ... local variables
    TYPE(t_mixt_input), POINTER :: input
    CHARACTER(LEN=3), DIMENSION(:), POINTER :: vars_to_write
    INTEGER :: iter
    INTEGER :: isRestart

    input => region%input
    IF(input%write_nothing == TRUE) RETURN

    iter = region%global%main_ts_loop_index

    isRestart = FALSE
    IF (PRESENT(restart) .EQV. .TRUE.) isRestart = restart
    IF(region%myrank .EQ. 0) THEN
       IF(isRestart == TRUE) THEN
          WRITE(*,*) 'PlasComCM: Writing restart at step',iter
       ELSE
          WRITE(*,*) 'PlasComCM: Writing output at step',iter
       ENDIF
    ENDIF


    CALL AssembleVarsToWrite(input,vars_to_write,iter,isRestart)

    IF(input%useHDF5 == 1) THEN
#ifdef HAVE_HDF5
       CALL WriteHDF5File(region,vars_to_write,isRestart)
#else 
      STOP 'PlasComCM: HDF5 not enabled.'
#endif
    ELSE
       CALL WriteP3DFile(region,vars_to_write,isRestart)
    ENDIF

    IF(region%myrank .EQ. 0) THEN
       IF(isRestart == TRUE) THEN
          WRITE(*,*) 'PlasComCM: Writing restart done.'
       ELSE
          WRITE(*,*) 'PlasComCM: Writing output done.'
       ENDIF
    ENDIF

    DEALLOCATE(vars_to_write)

  END SUBROUTINE WriteOutput


  SUBROUTINE WriteData(region, var_to_write,iter_in)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    USE ModPLOT3D_IO
    USE ModHDF5_IO

    IMPLICIT NONE

    TYPE(t_region), POINTER :: region
    CHARACTER(LEN=3) :: var_to_write
    INTEGER, OPTIONAL :: iter_in

    ! ... local variables
    CHARACTER(LEN=3), DIMENSION(:), POINTER :: vars_to_write
    INTEGER :: iter

    IF(region%input%write_nothing == TRUE) RETURN

    IF (PRESENT(iter_in) .EQV. .TRUE.) THEN
      iter = iter_in
    ELSE
      iter = region%global%main_ts_loop_index
    END if

    ALLOCATE(vars_to_write(1))
    vars_to_write(1) = var_to_write

    IF(region%input%useHDF5 == 1) THEN
#ifdef HAVE_HDF5
       CALL Write_HDF_File(region,vars_to_write)
#else 
      STOP 'PlasComCM: HDF5 not enabled.'
#endif
    ELSE
       CALL WriteP3DFile(region,vars_to_write,iter)
    ENDIF

    DEALLOCATE(vars_to_write)

  END SUBROUTINE WriteData

  subroutine output_matlab_matrix(A, varname, fname)

    USE ModGlobal

    Implicit None

    real(rfreal), pointer :: A(:,:)
    Character(LEN=*) :: fname, varname

    ! ... local variables
    integer :: i, j

    open (unit=10, file=trim(fname), form='formatted')
    write (10,'(A)') trim(varname)//' = [ ...'
    do i = 1, size(A,1)
      do j = 1, size(A,2)
        write (10,'(E20.12,2X)',ADVANCE='NO') A(i,j)
      end do
      write (10,*)
    end do
    write (10,'(A)') '];'
    close (10)
    
  end subroutine output_matlab_matrix



  subroutine CompleteVectorization(region, ng_in_file)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    Type (t_region), pointer :: region

    ! ... local variables
    Real(rfreal), Dimension(:,:,:,:,:), Pointer :: X
    Integer, Dimension(:,:,:,:), Pointer :: IBLANK
    Character(LEN=2) :: prec, gf, vf, ib
    Integer :: ngrid, I, J, K, L0, Nx, Ny, Nz, NDIM, M, ftype, ng, p, dir
    Integer, Dimension(:,:), Pointer :: ND
    Type (t_mixt_input), pointer :: input
    Type (t_grid), pointer :: grid
    Integer :: N(MAX_ND), vds(MAX_ND), ng_in_file, ierr

    Do ng = 1, region%nGrids

      grid  => region%grid(ng)
      input => grid%input

      if (grid%iGridGlobal == ng_in_file) THEN

        do p = 0, grid%numproc_inComm-1

          if ( grid%myrank_inComm == p ) THEN

            ! ... set ghost-cell information
            Do dir = 1, grid%ND
              grid%nGhostRHS(dir,:) = input%nOverLap
              if ( grid%left_process(dir) == MPI_PROC_NULL) grid%nGhostRHS(dir,1) = 0
              if (grid%right_process(dir) == MPI_PROC_NULL) grid%nGhostRHS(dir,2) = 0
            End Do

            ! ... compute the vectorization tables for each direction
            Call Vectorization_Tables(region%myrank, grid)

            ! ... compute the vectorization tables for each direction if 
            ! ... grid is finite volume
!            if (input%fv_grid(ng) == FINITE_VOL) THEN
!              Call FV_Vectorization_Tables(grid)
!            end if

          end if

          if ( grid%numproc_inComm > 1 .and. mod(p,64) .eq. 0) CALL MPI_BARRIER(grid%comm, ierr)

        end do

      end if

    End Do

  End Subroutine CompleteVectorization


! ... update the vectorization table if we find more ghost points are needed, WZhang 08/2014
  subroutine UpdateVectorization(region, ng_in_file, all_max_overlap)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    Type (t_region), pointer :: region
    integer :: all_max_overlap

    ! ... local variables
    Real(rfreal), Dimension(:,:,:,:,:), Pointer :: X
    Integer, Dimension(:,:,:,:), Pointer :: IBLANK
    Character(LEN=2) :: prec, gf, vf, ib
    Integer :: ngrid, I, J, K, L0, Nx, Ny, Nz, NDIM, M, ftype, ng, p, dir
    Integer, Dimension(:,:), Pointer :: ND
    Type (t_mixt_input), pointer :: input
    Type (t_grid), pointer :: grid
    Integer :: N(MAX_ND), vds(MAX_ND), ng_in_file, ierr

    Do ng = 1, region%nGrids

      grid  => region%grid(ng)
      input => grid%input

      write(*,'(A,I4,A,I2,A,I2)') 'Updating vectorization table for processor ', region%myrank, & 
                                ', from ', input%nOverLap, ' to ', all_max_overlap

      input%nOverlap = all_max_overlap

      if (grid%iGridGlobal == ng_in_file) THEN

        do p = 0, grid%numproc_inComm-1

          if ( grid%myrank_inComm == p ) THEN

            ! ... set ghost-cell information
            Do dir = 1, grid%ND
              grid%nGhostRHS(dir,:) = input%nOverLap
              if ( grid%left_process(dir) == MPI_PROC_NULL) grid%nGhostRHS(dir,1) = 0
              if (grid%right_process(dir) == MPI_PROC_NULL) grid%nGhostRHS(dir,2) = 0
            End Do

            ! ... compute the vectorization tables for each direction
            Call Vectorization_Tables(region%myrank, grid)

            ! ... compute the vectorization tables for each direction if 
            ! ... grid is finite volume
!            if (input%fv_grid(ng) == FINITE_VOL) THEN
!              Call FV_Vectorization_Tables(grid)
!            end if
          end if

          if ( grid%numproc_inComm > 1 ) CALL MPI_BARRIER(grid%comm, ierr)

        end do

      end if
      
    End Do

  End Subroutine UpdateVectorization

  subroutine Vectorization_Tables(myrank, grid)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    integer :: myrank
    Type(t_grid), pointer :: grid

    ! ... local variables
    Integer :: i, j, k, N(MAX_ND), vds(MAX_ND), l0, l1
    Integer :: dir, gOffset(MAX_ND)

    ! ... allocate space for vds, need to re-allocate memory for updating tables, WZhang 08/2014
    if (allocated(grid%vds) .eqv. .true.) THEN
      deallocate(grid%vds)
    end if
    allocate(grid%vds(grid%ND)); grid%vds(:) = 1;

    ! ... fill in the vectorization size
    N(:) = 1; vds(:) = 1
    do i = 1, grid%ND
      vds(i) = 1
      do j = 1, grid%ND
        N(j) = grid%ie(j)-grid%is(j)+1
        If (j /= i) vds(i) = vds(i) * N(j)
      end do
    end do

    ! ... allocate space for vec_ind, need to re-allocate memory for updating tables, WZhang 08/2014
    if (allocated(grid%vec_ind) .eqv. .true.) THEN
      deallocate(grid%vec_ind)
    end if
    allocate(grid%vec_ind(MAXVAL(vds),MAXVAL(N),grid%ND)); grid%vec_ind(:,:,:) = -1
    grid%vds(1:grid%ND) = vds(1:grid%ND)

    ! ... fill in vectorization index
    do dir = 1, grid%ND

      do k = 1, grid%ie(3)-grid%is(3)+1
        do j = 1, grid%ie(2)-grid%is(2)+1
          do i = 1, grid%ie(1)-grid%is(1)+1
            l0 = (k-1)*(grid%ie(2)-grid%is(2)+1)*(grid%ie(1)-grid%is(1)+1) &
                 + (j-1)*(grid%ie(1)-grid%is(1)+1) + i
            if (dir == 1) THEN
              l1 = (k-1)*(grid%ie(2)-grid%is(2)+1) + j
              grid%vec_ind(l1,i,dir) = l0
            ELSE IF (dir == 2) THEN
              l1 = (k-1)*(grid%ie(1)-grid%is(1)+1) + i
              grid%vec_ind(l1,j,dir) = l0
            ELSE IF (dir == 3) THEN
              l1 = (j-1)*(grid%ie(1)-grid%is(1)+1) + i
              grid%vec_ind(l1,k,dir) = l0
            end if
          end do
        end do
      end do
    end do

    If (ALL(grid%nGhostRHS(:,:) .EQ. 0)) THEN

      ! ... allocate space for vdsGhost, need to re-size memory for updating tables, WZhang 08/2014
      if (allocated(grid%vdsGhost) .eqv. .true.) THEN
        deallocate(grid%vdsGhost)
      end if
      allocate(grid%vdsGhost(grid%ND))
      do i = 1, grid%ND
        grid%vdsGhost(i) = grid%vds(i)
      end do

      !grid%vdsGhost => grid%vds
      !grid%vec_indGhost => grid%vec_ind
      ! ... allocate space for vec_indGhost, need to re-size memory for updating tables, WZhang 08/2014
      if (allocated(grid%vec_indGhost) .eqv. .true.) THEN
        deallocate(grid%vec_indGhost)
      end if
      allocate(grid%vec_indGhost(MAXVAL(vds),MAXVAL(N),grid%ND))
      do i = 1, MAXVAL(vds)
        do j = 1, MAXVAL(N)
          do k = 1, grid%ND
            grid%vec_indGhost(i,j,k) = grid%vec_ind(i,j,k)
          end do
        end do
      end do

    Else

      ! ... allocate space for vdsGhost, need to re-size memory for updating tables, WZhang 08/2014
      if (allocated(grid%vdsGhost) .eqv. .true.) THEN
        deallocate(grid%vdsGhost)
      end if
      ! ... allocate space for vds
      allocate(grid%vdsGhost(grid%ND))

      ! ... fill in the vectorization size, including Ghost cells
      N(:) = 1; vds(:) = 1;
      do i = 1, grid%ND
        vds(i) = 1
        N(i) = grid%ie(i)-grid%is(i)+1
      end do
      do i = 1, grid%ND
        do j = 1, grid%ND
          if (j /= i) vds(i) = vds(i) * N(j)
        end do
      end do
      N(1:grid%ND) = N(1:grid%ND) + grid%nGhostRHS(1:grid%ND,1) + grid%nGhostRHS(1:grid%ND,2)
      ! ... allocate space for vec_indGhost, need to re-size memory for updating tables, WZhang 08/2014
      if (allocated(grid%vec_indGhost) .eqv. .true.) THEN
        deallocate(grid%vec_indGhost)
      end if
      allocate(grid%vec_indGhost(MAXVAL(vds),MAXVAL(N),grid%ND))
      grid%vdsGhost(1:grid%ND) = vds(1:grid%ND)
      N(1:grid%ND) = N(1:grid%ND) - grid%nGhostRHS(1:grid%ND,1) - grid%nGhostRHS(1:grid%ND,2)

      ! ... fill in vectorization index, including Ghost cells
      do dir = 1, grid%ND
        N(dir) = N(dir) + grid%nGhostRHS(dir,1) + grid%nGhostRHS(dir,2)
        do k = 1, N(3)
          do j = 1, N(2)
            do i = 1, N(1)
              l0 = (k-1)*N(2)*N(1) + (j-1)*N(1) + i
              if (dir == 1) THEN
                l1 = (k-1)*N(2) + j
                grid%vec_indGhost(l1,i,dir) = l0
              ELSE IF (dir == 2) THEN
                l1 = (k-1)*N(1) + i
                grid%vec_indGhost(l1,j,dir) = l0
              ELSE IF (dir == 3) THEN
                l1 = (j-1)*N(1) + i
                grid%vec_indGhost(l1,k,dir) = l0
              end if
            end do
          end do
        end do
        N(dir) = N(dir) - grid%nGhostRHS(dir,1) - grid%nGhostRHS(dir,2)
      end do
    End If

    ! ... mapping from Cell to ghost data
    ! ... allocate space for cell2ghost, need to re-size memory for updating tables, WZhang 08/2014
    if (allocated(grid%cell2ghost) .eqv. .true.) THEN
      deallocate(grid%cell2ghost)
    end if
    allocate(grid%cell2ghost(grid%nCells,grid%ND))
    do dir = 1, grid%ND
      N(dir) = N(dir) + grid%nGhostRHS(dir,1) + grid%nGhostRHS(dir,2)
      gOffset(:) = 0; gOffset(dir) = grid%nGhostRHS(dir,1)
      do k = 1, grid%ie(3)-grid%is(3)+1
        do j = 1, grid%ie(2)-grid%is(2)+1
          do i = 1, grid%ie(1)-grid%is(1)+1

            l0 = (k-1)*(grid%ie(2)-grid%is(2)+1)*(grid%ie(1)-grid%is(1)+1) &
                 + (j-1)*(grid%ie(1)-grid%is(1)+1) + i

            l1 = (k-1+gOffset(3))*N(1)*N(2) + (j-1+gOffset(2))*N(1) + i+gOffset(1)

            grid%cell2ghost(l0,dir) = l1

          end do
        end do
      end do
      N(dir) = N(dir) - grid%nGhostRHS(dir,1) - grid%nGhostRHS(dir,2)
    end do

    ! if (myrank == 0) write (*,'(A)') '=========== Vectorization Info. ==========='
!!$    do i = 0, numproc-1
!!$      if (myrank == i) THEN
!!$        write (*,'((A,I5))') 'PlasComCM: For processor : ', myrank
!!$        do j = 1, grid%ND
!!$          write (*,'(A,2(I5,1X))') 'PlasComCM: ', grid%is(j), grid%ie(j)
!!$        end do
!!$      end if
!!$    end do
    ! if (myrank == 0) write (*,'(A)') '=========== END Vectorization Info. ==========='

    if (myrank == -1) THEN
      print *, grid%nCells, l0, l1
      print *, (grid%ie(2)-grid%is(2)+1)*(grid%ie(1)-grid%is(1)+1)*(grid%ie(3)-grid%is(3)+1)
      do i = 1, grid%nCells
        print *, i, ' ==> ', grid%cell2ghost(i,1)
      end do
    end if

  end subroutine Vectorization_Tables



  SUBROUTINE ReadTarget(region, fname)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    USE ModPLOT3D_IO
    USE ModHDF5_IO

    IMPLICIT NONE

    TYPE (t_region), POINTER :: region
    CHARACTER(LEN=80) :: fname

    INTEGER :: gridFormat

    gridFormat = FileFormat(fname)
    IF(gridFormat == FORMAT_HDF5) THEN
#ifdef HAVE_HDF5
       CALL ReadRestart_HDF5(region, fname, isTarget_IN=1)
#else
       STOP 'HDF5 not enabled.'
#endif
    ELSE IF(gridFormat == FORMAT_P3D) THEN
       CALL ReadTarget_P3D(region,fname)
    ENDIF
  END SUBROUTINE ReadTarget


  SUBROUTINE ReadMean(region, fname)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    USE ModPLOT3D_IO
    USE ModHDF5_IO

    IMPLICIT NONE

    TYPE (t_region), POINTER :: region
    CHARACTER(LEN=80) :: fname

    INTEGER :: gridFormat

    gridFormat = FileFormat(fname)
    IF(gridFormat == FORMAT_HDF5) THEN
#ifdef HAVE_HDF5
!       Call ReadMean_HDF5(region,fname)
#else
       STOP 'HDF5 not enabled.'
#endif
    ELSE IF(gridFormat == FORMAT_P3D) THEN
       CALL ReadMean_P3D(region,fname)
    ENDIF
  END SUBROUTINE ReadMean





  Subroutine Output_BC_As_IBLANK(region)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    IMPLICIT NONE

    ! ... function arguments
    type(t_region), pointer :: region

    ! ... local variables
    integer :: i, j, k, l0, N(MAX_ND), sgn, npatch, ierr
    type(t_patch), pointer :: patch
    type(t_grid), pointer :: grid

    do npatch = 1, region%nPatches

      patch => region%patch(npatch)
      grid => region%grid(patch%gridID)
      sgn = abs(patch%normDir) / patch%normDir

      N(:) = 1
      do j = 1, grid%ND
        N(j) = grid%ie(j) - grid%is(j) + 1
      end do;

      if ( patch%bcType /= SPONGE .and. patch%bcType /= SPONGE_WITH_LINEARIZED_DISTURBANCE ) then

        Do k = patch%is(3), patch%ie(3)
          Do j = patch%is(2), patch%ie(2)
            Do i = patch%is(1), patch%ie(1)

              l0 = (k-grid%is(3))*N(1)*N(2) + (j-grid%is(2))*N(1) + i-grid%is(1)+1

              if (grid%iblank(l0) > 0) grid%iblank(l0) = sgn * patch%bcType

            End Do
          End Do
        End Do
      End If
    End Do

    Call MPI_BARRIER(mycomm, ierr)
    CALL WriteData(region, 'xyz')
    Call MPI_BARRIER(mycomm, ierr)

    call graceful_exit(region%myrank, 'PlasComCM: WARNING: Stopping at Output_BC_As_IBLANK.')

  End Subroutine Output_BC_As_IBLANK

  Subroutine Output_Rank_As_IBLANK(region)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    IMPLICIT NONE

    ! ... function arguments
    type(t_region), pointer :: region

    ! ... local variables
    integer :: i, ng, ierr
    type(t_patch), pointer :: patch
    type(t_grid), pointer :: grid

    Do ng = 1, region%nGrids

      grid => region%grid(ng)

      Do i = 1, grid%nCells
        if (grid%iblank(i) /= 0) then
          grid%iblank(i) = region%myrank
        else 
          grid%iblank(i) = -1
        end if
      End Do

    End Do

    Call MPI_BARRIER(mycomm, ierr)
    call WriteData(region, 'xyz')
    Call MPI_BARRIER(mycomm, ierr)

    call graceful_exit(region%myrank, 'PlasComCM: WARNING: Stopping at Output_Rank_As_IBLANK.')

  End Subroutine Output_Rank_As_IBLANK


!!$
!!$



  

  SUBROUTINE ReadSingleGrid(region, localGridID)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    USE ModPLOT3D_IO
    USE ModDeriv
    USE ModGeometry
#ifdef HAVE_HDF5
    USE ModHDF5_IO
#endif
    Implicit None



    Type (t_region), pointer :: region
    Integer :: localGridID

    ! ... local variables
    Integer :: I, J, K, L0, ng, gridFormat,globalGridID
    Type (t_mixt_input), pointer :: input
    Type (t_grid), pointer :: grid
    logical :: grid_already_read

    grid_already_read = .false.
    
    grid  => region%grid(localGridID)
    input => grid%input
    globalGridID = grid%iGridGlobal

    IF (region%input%generateGrid == TRUE) THEN
       CALL GenerateGrid(region,localGridID)
       CALL TransformGrid(region,localGridID)
       CALL SetSpongeInfo(region,localGridID)
       grid_already_read = .true.
    ENDIF
    IF(input%read_nothing == TRUE) RETURN
    IF(.NOT. grid_already_read) THEN
       gridFormat = FileFormat(region%grid(1)%input%grid_fname)
       IF(gridFormat == FORMAT_HDF5) THEN
#ifdef HAVE_HDF5
          Call ReadSingleGrid_HDF5(region, globalGridID)
#else
          STOP 'HDF5 not enabled.'
#endif
       ELSE IF(gridFormat == FORMAT_P3D) THEN
          CALL ReadLocalGrid_P3D(region, localGridID)
       ENDIF
       
    END IF

  END SUBROUTINE ReadSingleGrid
  
  Subroutine SetSpongeInfo(region, localGridID)


    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    
    Implicit None
    
   ! ... input data
    TYPE(t_region), pointer :: region
    Integer :: localGridID
    
    ! ... local data
    TYPE(t_grid), pointer :: grid
    REAL(rfreal), pointer :: X(:,:,:,:)
    Integer, Dimension(:,:,:), Pointer :: IBLANK
    Integer :: ijkInd(MAX_ND)
   TYPE(t_mixt_input), pointer :: input
    TYPE(t_patch), pointer :: patch
    Integer :: npatch, N(MAX_ND), Np(MAX_ND), Nc, sgn, normDir, i, j, k, l1, l2, ii, jj, kk, lp
    Integer :: is(MAX_ND), ie(MAX_ND), offset, Ns(MAX_ND),globalGridID
    
    globalGridID = region%grid(localGridID)%iGridGlobal
    
    ! ... loop through all patches
    do npatch = 1, region%nPatches
      
       patch => region%patch(npatch)
       grid => region%grid(patch%gridID)
       
       ! ... only save information if this grid corresponds to ng_in_file
       if ( grid%iGridGlobal == globalGridID ) then
          
          ! ... check to see if this is a sponge boundary
          if ( patch%bcType == SPONGE .or. patch%bcType == SPONGE_WITH_LINEARIZED_DISTURBANCE ) then
            
             N(:) = 1; Np(:) = 1
             do j = 1, grid%ND
                N(j)  =  grid%ie(j) -  grid%is(j) + 1
                Np(j) = patch%ie(j) - patch%is(j) + 1
             end do
             
             normDir = abs(patch%normDir)
             sgn = normDir / patch%normDir
            
             ! ... extent of this ENTIRE sponge zone (in C-counting)
             is(:) = 0; ie(:) = 0; Ns(:) = 1; 
             do j = 1, grid%ND
                is(j) = patch%bcData(4+2*(j-1)  )
                ie(j) = patch%bcData(4+2*(j-1)+1)
                Ns(j) = ie(j) - is(j) + 1
             end do
             
            ! ... allocate memory for sponge boundary coordinates
             allocate(patch%sponge_xs(patch%prodN,grid%ND))
             allocate(patch%sponge_xe(patch%prodN,grid%ND))
             
             ! ... loop over all of the points and save the number of indeces
             Do k = patch%is(3), patch%ie(3)
                Do j = patch%is(2), patch%ie(2)
                   Do i = patch%is(1), patch%ie(1)
                      
                     lp = (k-patch%is(3))*Np(1)*Np(2) + (j-patch%is(2))*Np(1) + (i-patch%is(1)+1)
                      
                      Select Case (patch%normDir)
                         
                      Case (+1)
                         ijkInd(:) = (/ie(1), j, k/)
                         patch%sponge_xs(lp,1:grid%ND) = DBLE(ijkInd(1:grid%ND))
                         ijkInd(:) = (/is(1), j, k/)
                         patch%sponge_xe(lp,1:grid%ND) = DBLE(ijkInd(1:grid%ND))
                     Case (-1)
                         ijkInd(:) = (/is(1), j, k/)
                         patch%sponge_xs(lp,1:grid%ND) = DBLE(ijkInd(1:grid%ND))
                         ijkInd(:) = (/ie(1), j, k/)
                         patch%sponge_xe(lp,1:grid%ND) = DBLE(ijkInd(1:grid%ND))
                      Case (+2)
                         ijkInd(:) = (/i, ie(2), k/)
                         patch%sponge_xs(lp,1:grid%ND) = DBLE(ijkInd(1:grid%ND))
                         ijkInd(:) = (/i, is(1), k/)
                        patch%sponge_xe(lp,1:grid%ND) = DBLE(ijkInd(1:grid%ND))
                      Case (-2)
                         ijkInd(:) = (/i, is(2), k/)
                         patch%sponge_xs(lp,1:grid%ND) = DBLE(ijkInd(1:grid%ND))
                         ijkInd(:) = (/i, ie(1), k/)
                         patch%sponge_xe(lp,1:grid%ND) = DBLE(ijkInd(1:grid%ND))
                      Case (+3)
                         ijkInd(:) = (/i, j, ie(3)/)
                         patch%sponge_xs(lp,1:grid%ND) = DBLE(ijkInd(1:grid%ND))
                        ijkInd(:) = (/i, j, is(3)/)
                         patch%sponge_xe(lp,1:grid%ND) = DBLE(ijkInd(1:grid%ND))
                      Case (-3)
                         ijkInd(:) = (/i, j, is(3)/)
                         patch%sponge_xs(lp,1:grid%ND) = DBLE(ijkInd(1:grid%ND))
                         ijkInd(:) = (/i, j, ie(3)/)
                         patch%sponge_xe(lp,1:grid%ND) = DBLE(ijkInd(1:grid%ND))
                      End Select
                      
                      if (abs(sqrt(dot_product( patch%sponge_xe(lp,1:grid%ND) - patch%sponge_xs(lp,1:grid%ND), patch%sponge_xe(lp,1:grid%ND) - patch%sponge_xs(lp,1:grid%ND)))) <= 1e-10) then
                         print *, 'PlasComCM: sponge length too small ', i, j, k
                      end if
                   End Do
                End Do
             End do
             
          end if
          
       end if
 
    end do
    
  End Subroutine SetSpongeInfo

  subroutine AdjInterpolate_cv(region, ng)
    
    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    ! ... subroutine arguments
    Type(t_region), Pointer :: region
    Integer :: ng

    ! ... local variables and arrays
    Type(t_grid), Pointer :: grid
    Type(t_mixt), Pointer :: state
    Type(t_mixt_input), Pointer :: input
    Integer :: ND, Nc, NVARS, N(MAX_ND)
    Integer :: i, j, k, l, m
    Real(rfreal) :: fac1, fac2, denom

    ! ... simplicity
    grid  => region%grid(ng)
    state => region%state(ng)
    input => grid%input
    ND = grid%ND
    Nc = grid%nCells
    NVARS = state%NVARS
    N = 1
    do j = 1, grid%ND
      N(j) = grid%ie(j)-grid%is(j)+1
    end do ! j

    ! ... METHOD 1: read two consecutive solution files to interpolate state%cv
    ! ...           need to ensure if state%time is lying between the two files;
    ! ...           otherwise read repeatedly until it's so
    if (input%AdjNS_ReadNSSoln) then

      ! ... compare the current adjoint time with readily stored forward solutions'
      ! ... if the currently stored data can interpolate state%cv, go ahead;
      ! ... otherwise, read the next data and interpolate
      i = state%numCur_cvFile
      if (state%time(1) .LT. state%time_cvFiles(i)-0.1_rfreal*input%dt) then ! ... due to backward integration

        if (state%numCur_cvFile > 1) then
          ! ... slide cvOld & cvNew pair backward in time
          state%numCur_cvFile = state%numCur_cvFile - 1

         ! ... defensive programming: cannot jump over more than one interval!
         i = state%numCur_cvFile
         if (state%time(1) .LT. state%time_cvFiles(i)) &
           call graceful_exit(region%myrank, "... ERROR: adjoint DT too large compared to saved forward solutions")

          ! ... cvOld becomes cvNew due to backward integration; former cvNew thrown away
          do k = 1,SIZE(state%cvNew,2)
            do j = 1,SIZE(state%cvNew,1)
              state%cvNew(j,k) = state%cvOld(j,k)
            end do ! j
          end do ! k

          ! ... fill in state%cvOld with "fresh" old data
          call ReadLocalForwardSoln(region, ng, state%numCur_cvFile, state%cvOld)

        ELSE IF (state%numCur_cvFile == 1) then
          if (ABS(state%time(1) - state%time_cvFiles(i)) > 1E3*TINY) then
            call graceful_exit(region%myrank, "... ERROR: forward solutions cannot support adjoint N-S time range")
          else
            state%time(1) = state%time_cvFiles(i)
          end if ! ABS(state%time(1) - state%time_cvFiles(i))

        end if ! state%numCur_cvFile

      ELSE IF (state%time(1) .LT. state%time_cvFiles(i)) then ! ... due to backward integration
        state%time(1) = state%time_cvFiles(i)

      end if ! state%time(1)

      ! ... now actual interpolation
      ! ... Case 1: linear interpolation in time
      i = state%numCur_cvFile; j = state%numCur_cvFile + 1
      denom = 1.0_rfreal / DABS(state%time_cvFiles(j) - state%time_cvFiles(i))
      fac1  = (state%time_cvFiles(j) - state%time(1)) * denom
      fac2  = (state%time(1) - state%time_cvFiles(i)) * denom
      do k = 1,SIZE(state%cv,2)
        do j = 1,SIZE(state%cv,1)
          state%cv(j,k) = fac1*state%cvOld(j,k) + fac2*state%cvNew(j,k)
        end do ! j
      end do ! k

      ! ... Higher time-interpolation should be implemented here

    ! ... METHOD 2: hard-coded update of state%cv at state%time (NO interpolation!)
    ! ...           should be used only for testing and validating purposes
    else

      ! ... By default, ambient forward solution
      state%cv(:,1) = 1.0_rfreal
      do i = 1,ND
        state%cv(:,i+1) = state%cv(:,1) * 0.0_rfreal
      end do ! i
      state%cv(:,NVARS) = 1.0_rfreal/1.4_rfreal/0.4_rfreal
      do i = 1,ND
        state%cv(:,NVARS) = state%cv(:,NVARS) + 0.5_rfreal * state%cv(:,i+1)**2 / state%cv(:,1)
      end do ! i

      ! ... if needed, specify non-trivial unsteady forward solutions here

    end if ! input%AdjNS_ReadNSSoln

  end subroutine AdjInterpolate_cv

  subroutine Initialize_AdjInterpolate_cv(region, readFiles, numCur_cvFile)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    ! ... subroutine arguments
    Type(t_region), Pointer :: region
    Integer, Optional :: numCur_cvFile ! ... we may explicitly let the code know 
                                       ! ... where our forward solution is located
    LOGICAL :: readFiles

    ! ... local variables and arrays
    Type(t_mixt_input), Pointer :: input
    Type(t_grid), Pointer :: grid
    Type(t_mixt), Pointer :: state
    logical :: lf_exists
    character(len=80) :: dummy
    integer :: ng, i, ierr
    integer :: num_cvFiles
    real(rfreal) :: initialTime, finalTime

    ! ... simplicity
    input => region%input

    ! ... if not reading solution files, no more business here
    if (.NOT. input%AdjNS_ReadNSSoln) return

    ! ... Step 1: check if the list can really be found in the current directory
    inquire(file=trim(input%AdjNS_SolnList), exist=lf_exists)
    if (lf_exists) then
      ! ... Step 2: read the list here in each grid (we can keep only in region though)
      do ng = 1, region%nGrids
        state => region%state(ng)
  
        open(36, file=trim(input%AdjNS_SolnList))
        read(36,*) dummy, num_cvFiles
        state%num_cvFiles = num_cvFiles
  
        nullify(state%iter_cvFiles); allocate(state%iter_cvFiles(num_cvFiles))
        nullify(state%time_cvFiles); allocate(state%time_cvFiles(num_cvFiles))
  
        do i = 1,num_cvFiles
          read(36,*) dummy, state%iter_cvFiles(i), state%time_cvFiles(i)
        end do ! i
        close(36)
      end do ! ng

    else
      ! ... Step 3: list does not exist but can be constructed in ascending order
      do ng = 1, region%nGrids
        state => region%state(ng)

        num_cvFiles = region%numFwdSolnFiles
        state%num_cvFiles = num_cvFiles

!!$     if (ASSOCIATED(state%iter_cvFiles)) then
!!$       deallocate(state%iter_cvFiles)
!!$       deallocate(state%time_cvFiles)
!!$     end if ! ASSOCIATED(state%iter_cvFiles)

        nullify(state%iter_cvFiles); allocate(state%iter_cvFiles(num_cvFiles))
        nullify(state%time_cvFiles); allocate(state%time_cvFiles(num_cvFiles))

        do i = 1,num_cvFiles
          state%iter_cvFiles(i) = region%ctrlStartIter + (i-1)*input%noutput
          state%time_cvFiles(i) = region%ctrlStartTime + REAL((i-1)*input%noutput,rfreal)*input%dt
        end do ! i
      end do ! ng
    end if ! lf_exists

    do ng = 1, region%nGrids
      state => region%state(ng)

      ! ... Step 4: check if forward solutions have appropriate time range
      ! ... time is initialized in InitialCondition if starting from scratch
      ! ...                     in read_restart if re-starting
      if (lf_exists) then ! ... standalone adjoint N-S solver
        initialTime = state%time(1)
        finalTime = initialTime - REAL(input%nstepmax, rfreal)*input%dt ! ... CONSTANT_DT

        ! ... initial data can be reconstructed?
        ! ... initialTime = MAX(state%time_cvFiles(:)) is allowed
        if ((initialTime .LE. MINVAL(state%time_cvFiles(:))) .OR. &
            (initialTime .GT. MAXVAL(state%time_cvFiles(:)))) &
          call graceful_exit(region%myrank, "... state%time outside of forward solution time zone")

        ! ... final data can be reconstructed? (CONSTANT_DT assumed)
        ! ... finalTime = MIN(state%time_cvFiles(:) okay
        if (finalTime .LT. MINVAL(state%time_cvFiles(:))) &
          call graceful_exit(region%myrank, "... final estimated state%time not supported by forward solution")

      else ! ... adjoint-based control optimization

        ! ... state%time_cvFiles is always in correct range so we don't need to check

      end if ! input%AdjNS
    end do ! ng

    ! ... Step 5: in which interval we are?
    do ng = 1, region%nGrids
      state => region%state(ng)

      if (lf_exists) then
        state%numCur_cvFile = FALSE
        Loop_i: do i = 1,num_cvFiles-1
          if (state%time(1) .GE. state%time_cvFiles(i) .AND. &
              state%time(1) .LE. state%time_cvFiles(i+1)) then
              state%numCur_cvFile = i
              exit Loop_i
          end if ! state%time(1)
        end do Loop_i
        if (state%numCur_cvFile == FALSE) & ! ... defensive programming
            call graceful_exit(region%myrank, "... ERROR: something wrong in locating forward solution time")

      else
        state%numCur_cvFile = state%num_cvFiles - 1
        if (present(numCur_cvFile)) state%numCur_cvFile = numCur_cvFile

      end if ! lf_exists
    end do ! ng

    ! ... Step 5: initialize state%cvOld and state%cvNew
    ! ...         state%cvOld defined to have smaller state%iter_cvFiles
    ! ...         state%cvNew defined to have bigger  state%iter_cvFiles
    ! ...         if interpolation other than linear one is considered, we gotta 
    ! ...         store more state%cv
    do ng = 1, region%nGrids
      state => region%state(ng)

      if (readFiles) then
        call ReadLocalForwardSoln(region, ng, state%numCur_cvFile,   state%cvOld)
        call ReadLocalForwardSoln(region, ng, state%numCur_cvFile+1, state%cvNew)
      else
        state%cvOld(:,:) = 0.0_rfreal
        state%cvNew(:,:) = 0.0_rfreal
      end if ! readFiles
    end do ! ng

    call mpi_barrier(mycomm, ierr)

  end subroutine Initialize_AdjInterpolate_cv


! Needs to be made generic (i.e. format independent)
  subroutine ReadLocalForwardSoln(region, ng, file_index, ptr_to_data)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    USE ModPLOT3D_IO
    USE ModHDF5_IO

    ! ... subroutine arguments
    Type (t_region), Pointer :: region
    Integer :: ng, file_index
    Real(rfreal), Dimension(:,:), Pointer :: ptr_to_data ! ... Adjoint N-S

    Type(t_mixt_input), Pointer :: input

    input => region%input

    IF(input%useHDF5 == 1) THEN
#ifdef HAVE_HDF5
!       CALL ReadLocalForwardSoln_HDF5(region,vars_to_write)
      STOP 'ReadLocalForwardSoln_HDF5 not implemented'
#else 
      STOP 'PlasComCM: HDF5 not enabled.'
#endif
    ELSE
       CALL ReadLocalForwardSoln_P3D(region, ng, file_index, ptr_to_data)
    ENDIF

  end subroutine ReadLocalForwardSoln

  Subroutine initialize_probe_data(region)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    ! ... subroutine arguments
    Type (t_region), Pointer :: region

    ! ... local variables and arrays
    Integer :: i, iProbe, funit, ng, idum
    Integer :: sampleRate, numSample, numProbeLocal, numProbeGlobal, numVar
    Real(rfreal) :: sampleDT
    Character(LEN=80) :: probe_fname, junk
    Integer, Pointer :: probeID(:), gridID(:), ijk(:,:)
    Type (t_grid), Pointer :: grid
    Type(t_mixt), Pointer :: state
    Type(t_mixt_input), Pointer :: input
    Type (t_probe), Pointer :: probe
    integer :: ierr

    ! ... simplicity
    input => region%input
    funit = 57

    if (input%GetTrace .eqv. .false.) return ! ... defensive programming

    If (input%cfl_mode /= CONSTANT_DT) Then ! ... defensive programming
      call graceful_exit(region%myrank, "... ERROR: cannot use CONSTANT_CFL to get pointwise time trace.")
    End If ! input%cfl_mode

    Call MPI_BARRIER(mycomm, ierr)

    ! ... initialize local variables
    numProbeGlobal = input%numProbeGlobal
    sampleRate = input%sampleRate
    numSample = input%nstepmax / sampleRate ! ... IC data is not written
    sampleDT = sampleRate * input%dt
    numVar = input%ND+2
    probe_fname = TRIM(input%probe_fname)

    ! ... allocate temporary arrays
    Nullify(probeID); Allocate(probeID(numProbeGlobal)); probeID = 1
    Nullify(gridID); Allocate(gridID(numProbeGlobal)); gridID = 1
    Nullify(ijk); Allocate(ijk(numProbeGlobal,MAX_ND)); ijk = 1

    ! ... every processor reads a file containing probing location
    Open(unit=funit, file=trim(probe_fname), status='old')
    Read(funit,'(A)') junk
    Read(funit,'(A)') junk
    Read(funit,'(A)') junk
    Read(funit,'(A)') junk
    Read(funit,'(A)') junk
    Do iProbe = 1,numProbeGlobal
      Read(funit,*) probeID(iProbe), gridID(iProbe), ijk(iProbe,1:input%ND), junk
    End Do ! iProbe
    Close(unit=funit)

    ! ... check how many probes this processor owns
    numProbeLocal = 0 ! ... by default, there is no probe in this processor
    ngProbe1: Do iProbe = 1,numProbeGlobal
      ngLoop1: Do ng = 1,region%nGrids
        grid => region%grid(ng)

        If (grid%iGridGlobal == gridID(iProbe)) then ! ... at least, global grid ID is matching
                                                     ! ... see if ijk-index lies within this grid
          Do i = 1,grid%ND
            idum = (grid%ie(i) - ijk(iProbe,i)) * (ijk(iProbe,i) - grid%is(i))
            If (idum < 0) CYCLE ngLoop1
          End Do ! i

          numProbeLocal = numProbeLocal + 1
          CYCLE ngProbe1
        End If ! grid%iGridGlobal
      End Do ngLoop1
    End Do ngProbe1

    ! ... allocate region%probe if needed
    If (numProbeLocal > 0) Then
      Nullify(region%probe); Allocate(region%probe(numProbeLocal))
    End If ! numProbeLocal

    ! ... save all probe information to region%probe(:)
    region%numProbeGlobal = numProbeGlobal
    region%numProbeLocal  = numProbeLocal
    numProbeLocal = 0
    iProbe2: Do iProbe = 1,numProbeGlobal
      ngLoop2: Do ng = 1,region%nGrids
        grid => region%grid(ng)
        state => region%state(ng)

        If (grid%iGridGlobal == gridID(iProbe)) then
          Do i = 1,grid%ND
            idum = (grid%ie(i) - ijk(iProbe,i)) * (ijk(iProbe,i) - grid%is(i))
            If (idum < 0) CYCLE ngLoop2
          End Do ! i

          numProbeLocal = numProbeLocal + 1
          probe => region%probe(numProbeLocal)

          probe%probeID = iProbe
          probe%gridID  = gridID(iProbe)
          probe%ng      = ng
          probe%ijk(1:MAX_ND) = ijk(iProbe,1:MAX_ND)
          probe%l0 = (ijk(iProbe,3)-grid%is(3))*(grid%ie(1)-grid%is(1)+1)*(grid%ie(2)-grid%is(2)+1) &
                   + (ijk(iProbe,2)-grid%is(2))*(grid%ie(1)-grid%is(1)+1) &
                   +  ijk(iProbe,1)-grid%is(1) + 1
          probe%sampleRate = sampleRate
          probe%numSample = 0 ! ... initially number of data is zero
          probe%sampleDT = sampleDT

          probe%xyz(:) = 0.0_rfreal
          probe%xyz(1:grid%ND) = grid%xyz(probe%l0,1:grid%ND)

          probe%numVar = numVar
          Nullify(probe%var); Allocate(probe%var(numSample,numVar)); probe%var = 0.0_rfreal

          probe%iter_min = input%nstepi+input%nstepmax ! ... this is an initial value
          probe%dt = input%dt
          probe%time_min = state%time(1) + input%dt*Real(input%nstepmax, rfreal) ! ... this is an initial value

          CYCLE iProbe2
        End If ! grid%iGridGlobal
      End Do ngLoop2
    End Do iProbe2

    ! ... wipe out any local storage
    Deallocate(probeID, gridID, ijk)
    Call MPI_BARRIER(mycomm, ierr)

  End Subroutine initialize_probe_data

  Subroutine store_probe_data(region)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    ! ... subroutine arguments
    Type (t_region), Pointer :: region

    ! ... local variables and arrays
    Integer :: iProbe, ng, l0, numVar, i, j
    Real(rfreal) :: imsi
    Type (t_grid), Pointer :: grid
    Type(t_mixt), Pointer :: state
    Type(t_mixt_input), Pointer :: input
    Type (t_probe), Pointer :: probe
    integer :: ierr

    ! ... simplicity
    input => region%input

    if (.NOT. input%GetTrace) return ! ... defensive programming

    Call MPI_BARRIER(mycomm, ierr)
    If (region%numProbeLocal > 0) Then ! ... if this processor has any probe
      Do iProbe = 1,region%numProbeLocal
        probe => region%probe(iProbe)

        If (MOD(region%global%main_ts_loop_index,probe%sampleRate) == 0) THEN
          probe%numSample = probe%numSample + 1
          j = probe%numSample

          ! ... get local grid and point information from a probe
          ng = probe%ng
          l0 = probe%l0

          ! ... simplicity
          grid => region%grid(ng)
          state => region%state(ng)

          probe%iter_min = MIN(probe%iter_min, region%global%main_ts_loop_index)
          probe%time_min = MIN(probe%time_min, state%time(1))

          ! ... as of now, primitive variables {rho, u_i, p}^T are stored
          probe%var(j,1) = state%cv(l0,1)
          probe%var(j,grid%ND+2) = state%dv(l0,1)
          do i = 1,grid%ND
            probe%var(j,i+1) = state%cv(l0,i+1) * state%dv(l0,3)
          end do ! i
        End If ! MOD(region%global%main_ts_loop_index,probe%sampleRate)

      End Do ! iProbe
    End If ! region%numProbeLocal
    Call MPI_BARRIER(mycomm, ierr)

  End Subroutine store_probe_data

  Subroutine write_probe_data(region)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    ! ... subroutine arguments
    Type (t_region), Pointer :: region

    ! ... local variables and arrays
    Integer :: iProbe, funit, i, ng, l0, j, k, sampleRate, numSample, numVar
    Character(LEN=80) :: fname
    Type (t_grid), Pointer :: grid
    Type(t_mixt), Pointer :: state
    Type(t_mixt_input), Pointer :: input
    Type (t_probe), Pointer :: probe
    integer :: ierr

    ! ... simplicity
    input => region%input
    funit = 57

    if (.NOT. input%GetTrace) return ! ... defensive programming

    fname(1:6) = 'probe.'
    Write(fname(7:18),'(I8.8,A4)') region%global%main_ts_loop_index, '.dat'

    ! ... write file header
    If (region%myrank == 0) Then
      Open(unit=funit, file=trim(fname), form='UNFORMATTED')
      Write(funit) region%numProbeGlobal
      Close(unit=funit)
    End If ! myrank

    Call MPI_BARRIER(mycomm, ierr)
    Do iProbe = 1,region%numProbeGlobal ! ... need to write probed data in their 
                                        ! ... ascending order
      If (region%numProbeLocal > 0) Then ! ... if this processor has any probe
        Do i = 1,region%numProbeLocal
          probe => region%probe(i)

          If (probe%probeID == iProbe) Then
            ! ... simplicity
            ng = probe%ng
            grid => region%grid(ng)
            state => region%state(ng)

            Open(unit=funit, file=trim(fname), form='UNFORMATTED', position='APPEND')

            ! ... probe%l0 depends on number of processors so it is useless in post-processing
            ! ... it should be dropped in the later update
            Write(funit) probe%probeID, probe%gridID, probe%ijk(1:MAX_ND), probe%l0
            Write(funit) probe%sampleRate, probe%numSample, probe%sampleDT
            Write(funit) probe%iter_min, probe%dt, probe%time_min
            Write(funit) probe%xyz(1:MAX_ND)
            Write(funit) probe%numVar
            Write(funit) ((probe%var(j,k),j=1,probe%numSample),k=1,probe%numVar)

            Close(unit=funit)

            ! ... now that we've written everything, initialize counters
            sampleRate = probe%sampleRate
            numSample = input%noutput / sampleRate ! ... IC data is not written
            numVar = probe%numVar
            Deallocate(probe%var)
            Nullify(probe%var); Allocate(probe%var(numSample,numVar)); probe%var = 0.0_rfreal

            probe%numSample = 0
            probe%iter_min = input%nstepi+input%nstepmax
            probe%time_min = state%time(1) + input%dt*Real(input%nstepmax, rfreal)
          End If ! probe%probeID
        End Do ! i
      End If ! region%numProbeLocal

      Call MPI_BARRIER(mycomm, ierr)

    End Do ! iProbe
    Call MPI_BARRIER(mycomm, ierr)

  End Subroutine write_probe_data

  Subroutine Read_And_Interpolate_LST_Eigenmodes(region)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    USE ModSpline

    ! ... subroutine arguments
    Type (t_region), Pointer :: region

    ! ... local variables and arrays
    Type(t_grid), Pointer :: grid
    Type(t_mixt), Pointer :: state
    Type(t_mixt_input), Pointer :: input
    Type(t_patch), Pointer :: patch
    Integer :: i, j, k, l
    Integer :: npatch, ND, N(MAX_ND), Np(MAX_ND)

    Logical :: pf_exists
    Integer :: M_mode, N_mode, Nr, p, q, r, s, l0, lp, iModeTemp, iModeAzi
    Real(rfreal) :: radius, angle, junkRe, junkIm
    Real(rfreal), Pointer :: rLST(:), beta_pert(:,:), amp_pert(:,:)
    Real(rfreal), Pointer :: qHatRe(:,:), qHatIm(:,:)
    type(t_nat_spline), pointer :: qHatReSpline(:), qHatImSpline(:)
    Complex(rfreal) :: eye = (0.0_rfreal, 1.0_rfreal)
    Complex(rfreal) :: junkc(MAX_ND)
    Complex(rfreal), Pointer :: alpha_pert(:,:), freq_pert(:,:), Qp_hat(:,:,:,:)

    Do npatch = 1, region%npatches

      patch => region%patch(npatch)
      grid  => region%grid(patch%gridID)
      state => region%state(patch%gridID)
      input => grid%input

      N  = 1; Np = 1;
      do j = 1, grid%ND
        N(j)  = grid%ie(j)-grid%is(j)+1
        Np(j) = patch%ie(j) - patch%is(j) + 1
      end do;

      If (patch%bcType == SPONGE_WITH_LINEARIZED_DISTURBANCE) Then
        ! ... defensive programming
        if (grid%ND /= 3) call graceful_exit(region%myrank, "... ERROR: inflow forcing by LST supports only 3-D problem.")

        ! ... initialize patch data
        patch%LST_num_mode_temporal = input%LST_num_mode_temporal
        patch%LST_num_mode_azimuthal = input%LST_num_mode_azimuthal
        patch%LST_amp = input%LST_amp / REAL(patch%LST_num_mode_temporal*(patch%LST_num_mode_azimuthal-1), rfreal) ! ... 0-th azimuthal mode neglected
        patch%LST_x0 = input%LST_x0

        Nullify(patch%LST_freq_angular)
        Allocate(patch%LST_freq_angular(patch%LST_num_mode_temporal))
        patch%LST_freq_angular = 0.0_rfreal

        Nullify(patch%LST_wavenum_azimuthal)
        Allocate(patch%LST_wavenum_azimuthal(patch%LST_num_mode_azimuthal))
        patch%LST_wavenum_azimuthal = REAL(0,rfreal)

        Nullify(patch%LST_phase)
        Allocate(patch%LST_phase(patch%LST_num_mode_temporal,patch%LST_num_mode_azimuthal))
        patch%LST_phase = 0.0_rfreal

        Nullify(patch%LST_phase_dir)
        Allocate(patch%LST_phase_dir(patch%LST_num_mode_temporal,patch%LST_num_mode_azimuthal))
        patch%LST_phase_dir = 0.0_rfreal

        Nullify(patch%LST_wavenum_streamwise)
        Allocate(patch%LST_wavenum_streamwise(patch%LST_num_mode_temporal,patch%LST_num_mode_azimuthal))
        patch%LST_wavenum_streamwise = (0.0_rfreal, 0.0_rfreal)

        Nullify(patch%LST_eigenf)
        Allocate(patch%LST_eigenf(PRODUCT(N), patch%LST_num_mode_temporal, patch%LST_num_mode_azimuthal, grid%ND+2))
        patch%LST_eigenf = (0.0_rfreal, 0.0_rfreal)

        ! ... read readily computed eigenmode data from the upstream LST code
        Inquire(file=trim(input%LST_fname), exist=pf_exists)
        If (.NOT. pf_exists) call graceful_exit(region%myrank, "... ERROR: eigenfunction file for perturbing inflow not found.")
        Open(unit=52, file=trim(input%LST_fname), form='unformatted', status='old')
        Read(52) M_mode, N_mode, Nr

        ! ... defensive programming
        If (M_mode /= patch%LST_num_mode_temporal .OR. &
            N_mode /= patch%LST_num_mode_azimuthal) &
          call graceful_exit(region%myrank, "... ERROR: number of instability modes different than upstream LST code.")

        Nullify(rLST, alpha_pert, freq_pert, beta_pert, amp_pert, Qp_hat)
        Allocate(rLST(Nr))
        Allocate(alpha_pert(patch%LST_num_mode_temporal,patch%LST_num_mode_azimuthal))
        Allocate(freq_pert(patch%LST_num_mode_temporal,patch%LST_num_mode_azimuthal))
        Allocate(beta_pert(patch%LST_num_mode_temporal,patch%LST_num_mode_azimuthal))
        Allocate(amp_pert(patch%LST_num_mode_temporal,patch%LST_num_mode_azimuthal))
        Allocate(Qp_hat(Nr,6,patch%LST_num_mode_temporal,patch%LST_num_mode_azimuthal))
        rLST = 0.0_rfreal
        alpha_pert = (0.0_rfreal, 0.0_rfreal)
        freq_pert = (0.0_rfreal, 0.0_rfreal)
        beta_pert = REAL(0,rfreal)
        amp_pert = 0.0_rfreal
        Qp_hat = (0.0_rfreal, 0.0_rfreal)

        ! ... following Lui's code format
        Read(52) rLST
        Read(52) alpha_pert
        Read(52) freq_pert
        Read(52) beta_pert
        Read(52) amp_pert
        Read(52) Qp_hat
        Close(unit=52)

        ! ... now store them in appropriate format (amp_pert not used)
        Do q = 1,patch%LST_num_mode_azimuthal
          Do p = 1,patch%LST_num_mode_temporal
            patch%LST_wavenum_streamwise(p,q) = alpha_pert(p,q)
          End Do ! p
        End Do ! q
        patch%LST_freq_angular(1:patch%LST_num_mode_temporal) = DREAL(freq_pert(1:M_mode,1))
        patch%LST_wavenum_azimuthal(1:patch%LST_num_mode_azimuthal) = beta_pert(1,1:N_mode)

        ! ... interpolate eigenfunctions Qp_hat using the cubic spline
        ! ... computed eigenfunctions from the upstream LST code
        Nullify(qHatRe); Allocate(qHatRe(Nr,grid%ND+2)); qHatRe = 0.0_rfreal
        Nullify(qHatIm); Allocate(qHatIm(Nr,grid%ND+2)); qHatIm = 0.0_rfreal
        ! ... second derivatives of computed eigenfunctions
        Nullify(qHatReSpline); Allocate(qHatReSpline(grid%ND+2))
        Nullify(qHatImSpline); Allocate(qHatImSpline(grid%ND+2))

        Do iModeAzi = 1,patch%LST_num_mode_azimuthal ! ... although not used, 0-th azimuthal mode is also interpolated
          Do iModeTemp = 1,patch%LST_num_mode_temporal

            ! ... index of Qp_hat following Lui's code
            ! ... real part
            qHatRe(:,1) = Real(Qp_hat(:,4,iModeTemp,iModeAzi)) ! ... rho hat
            qHatRe(:,2) = Real(Qp_hat(:,2,iModeTemp,iModeAzi)) ! ... rho-ur hat
            qHatRe(:,3) = Real(Qp_hat(:,3,iModeTemp,iModeAzi)) ! ... rho-utheta hat
            qHatRe(:,4) = Real(Qp_hat(:,1,iModeTemp,iModeAzi)) ! ... rho-ux hat
            qHatRe(:,5) = Real(Qp_hat(:,5,iModeTemp,iModeAzi)) ! ... rho-E hat
            ! ... imaginary part
            qHatIm(:,1) = IMAG(Qp_hat(:,4,iModeTemp,iModeAzi)) ! ... rho hat
            qHatIm(:,2) = IMAG(Qp_hat(:,2,iModeTemp,iModeAzi)) ! ... rho-ur hat
            qHatIm(:,3) = IMAG(Qp_hat(:,3,iModeTemp,iModeAzi)) ! ... rho-utheta hat
            qHatIm(:,4) = IMAG(Qp_hat(:,1,iModeTemp,iModeAzi)) ! ... rho-ux hat
            qHatIm(:,5) = IMAG(Qp_hat(:,5,iModeTemp,iModeAzi)) ! ... rho-E hat

            ! ... construct natural spline interpolants
            Do s = 1,grid%ND+2
              call GenerateSpline(rLST, qHatRe(:,s), qHatReSpline(s))
              call GenerateSpline(rLST, qHatIm(:,s), qHatImSpline(s))
            End Do ! s

            Do r = patch%is(3), patch%ie(3)
              Do q = patch%is(2), patch%ie(2)
                Do p = patch%is(1), patch%ie(1)

                  l0 = (r-grid%is(3))*(grid%ie(1)-grid%is(1)+1)*(grid%ie(2)-grid%is(2)+1) &
                     + (q-grid%is(2))*(grid%ie(1)-grid%is(1)+1) &
                     +  p-grid%is(1)+1
                  lp = (r-patch%is(3))*(patch%ie(1)-patch%is(1)+1)*(patch%ie(2)-patch%is(2)+1) &
                     + (q-patch%is(2))*(patch%ie(1)-patch%is(1)+1) &
                     +  p-patch%is(1)+1

                  ! ... radius and azimuthal angle
                  radius = SQRT((grid%xyz(l0,2))**2 + (grid%xyz(l0,3))**2)
                  angle = ATAN2(grid%xyz(l0,3), grid%xyz(l0,2)) ! ... angle measured from +y axis
                  If (angle < 0.0_rfreal) angle = angle + TWOPI

                  ! ... spline interpolation
                  Do s = 1,grid%ND+2
                    junkRe = SplineInterp(qHatReSpline(s), radius)
                    junkIm = SplineInterp(qHatImSpline(s), radius)
                    patch%LST_eigenf(lp,iModeTemp,iModeAzi,s) = junkRe + junkIm * eye
                  End Do ! s

                  ! ... coordinate transformation from (r, theta, x) to (x, y, z)
                  junkc(1:3) = patch%LST_eigenf(lp,iModeTemp,iModeAzi,2:4) ! ... {rho-ur hat, rho-utheta hat, rho-ux hat}
                  patch%LST_eigenf(lp,iModeTemp,iModeAzi,2) = junkc(3) ! ... rho-ux hat
                  patch%LST_eigenf(lp,iModeTemp,iModeAzi,3) = junkc(1)*COS(angle) - junkc(2)*SIN(angle) ! ... rho-uy hat
                  patch%LST_eigenf(lp,iModeTemp,iModeAzi,4) = junkc(1)*SIN(angle) + junkc(2)*COS(angle) ! ... rho-uz hat

                  ! ... multiply by spatial portions of instability wave
                  patch%LST_eigenf(lp,iModeTemp,iModeAzi,:) = patch%LST_eigenf(lp,iModeTemp,iModeAzi,:) &
                                 * EXP(eye * (patch%LST_wavenum_streamwise(iModeTemp,iModeAzi)*(grid%xyz(l0,1) - patch%LST_x0) &
                                            + patch%LST_wavenum_azimuthal(iModeAzi)*angle))

                  ! ... patch%LST_eigenf contains 
                  ! ... qHat * exp[i (alpha * x + n * theta)]
                  ! ... time-dependent term, exp[i (-omega t + phase)] is multiplied while time-marching

                End Do ! p
              End Do ! q
            End Do ! r

          End Do ! iModeTemp
        End Do ! iModeAzi

        ! ... effective sponge edge is moved to patch%LST_x0
        Do k = 1,SIZE(patch%sponge_xe,1)
          patch%sponge_xe(k,1) = patch%LST_x0
        End Do ! k

        Deallocate(rLST, alpha_pert, freq_pert, beta_pert, amp_pert, Qp_hat)
        Deallocate(qHatRe, qHatIm)
        deallocate(qHatReSpline, qHatImSpline)
      End If ! patch%bcType

    End Do ! npatch

  End Subroutine Read_And_Interpolate_LST_Eigenmodes


  Subroutine Create_Global_Mask(region)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    USE ModPLOT3D_IO

    implicit none

    ! ... function call
    type (t_mixt_input), pointer :: input
    type (t_grid), pointer :: grid
    type (t_region), pointer :: region

    ! ... local variables
    integer :: NDIM, ngrid, i, j, k, l0, ng, ierr
    integer, pointer :: ND(:,:), IBLANK(:,:,:)
    character(len=2) ::  prec, gf, vf, ib
    real(rfreal) :: timer

    ierr = 0

    ! ... collect all data onto core rank
    Call Agglomerate_IBLANK_To_Core_Rank(region, ierr)

    ! ... now broadcast data
    Do i = 1, region%nGridsGlobal
      Do ng = 1, region%nGrids
        grid => region%grid(ng)
        If (grid%iGridGlobal == i) Then
          timer = MPI_WTime()
          If (.not.associated(grid%global_iblank) .eqv. .true.) &
               allocate(grid%global_iblank(grid%globalSize(1),grid%globalSize(2),grid%globalSize(3)),stat=ierr)
          If (ierr /= 0) CALL graceful_exit(region%myrank, 'Cannot allocate global_iblank.')
          Call MPI_Bcast(grid%global_iblank,PRODUCT(grid%globalSize(:)),MPI_INTEGER,0,grid%comm,ierr)
          region%mpi_timings(ng)%operator_setup(:,:) = region%mpi_timings(ng)%operator_setup(:,:) + (MPI_WTime()-timer)
        End If
      End Do
    End Do

    Return

  End Subroutine Create_Global_Mask

  Subroutine Request_Global_Mask(region, ng, req_proc)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    USE ModPLOT3D_IO

    implicit none

    ! ... function call
    type (t_mixt_input), pointer :: input
    type (t_grid), pointer :: grid
    type (t_region), pointer :: region
    integer :: ng, req_proc

    ! ... local variables
    integer :: NDIM, ngrid, i, j, k, l0, ierr
    integer, pointer :: ND(:,:), IBLANK(:,:,:)
    integer :: status(MPI_STATUS_SIZE)
    character(len=2) ::  prec, gf, vf, ib
    real(rfreal) :: timer

    ierr = 0

    ! ... collect all data onto core rank
    Call Agglomerate_IBLANK_To_Core_Rank(region, ierr)

    ! ... start timer
    timer = MPI_WTime()

    ! ... Simplicity
    grid => region%grid(ng)

    ! ... return if we are core
    if (req_proc == 0) return

    ! ... else, send data
    If (req_proc == grid%myrank_inComm) then
      allocate(grid%global_iblank(grid%globalSize(1),grid%globalSize(2),grid%globalSize(3)),stat=ierr)
      if (ierr /= 0) CALL graceful_exit(region%myrank, 'Cannot allocate global_iblank.')
      Call MPI_Recv(grid%global_iblank,PRODUCT(grid%globalSize(:)),MPI_INTEGER,0,0,grid%comm,status,ierr)
    End If
    If (grid%myrank_inComm == 0) Then
      Call MPI_Send(grid%global_iblank,PRODUCT(grid%globalSize(:)),MPI_INTEGER,req_proc,0,grid%comm,ierr)
      Deallocate(grid%global_iblank)
    End If

    Call MPI_Barrier(grid%comm,ierr)

    ! ... stop timer
    region%mpi_timings(ng)%operator_setup(:,:) = region%mpi_timings(ng)%operator_setup(:,:) + (MPI_WTime()-timer)

    Return

  End Subroutine Request_Global_Mask


  subroutine write_approximate_matrix(region, cmd, i0, j0, k0, var)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    USE ModPLOT3D_IO

    Implicit None

    type(t_region), pointer :: region
    character(len=80) :: cmd
    integer :: i0, j0, k0, var

    ! ... local variables
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    type(t_mixt_input), pointer :: input
    real(rfreal) :: tau(4)
    integer :: i, ng, numGridsInFile, NDIM, iter, ii, jj, kk, mm, row, col
    character(len=80) :: fname
    integer, parameter :: funit = 50
    integer :: fnamelen, all_done, ierr

    ! ... length of filename
    fnamelen = len_trim(cmd)

    if (region%myrank == 0) Write (*,'(A)') 'PlasComCM: Writing "'//cmd(1:fnamelen)//'".'

    ! ... row of matrix for this (i0,j0,k0,var)
    grid => region%grid(1)
    input => grid%input
    col = ((k0-1)*grid%GlobalSize(1)*grid%GlobalSize(2)+(j0-1)*grid%GlobalSize(1)+(i0-1))*(input%ND+2) + var

    ! ... step 2
    ! ... agglomerate decomposed data to core Rank for that grid
    Call Agglomerate_Data_To_Core_Rank(region, 'cv ')
    Call MPI_BARRIER(mycomm, ierr)

    ! ... step 3
    ! ... write agglomerated data
    Loop1: Do i = 1, region%grid(1)%numGridsInFile

      Loop2: Do ng = 1, region%nGrids

        grid => region%grid(ng)
        state => region%state(ng)
        input => grid%input

        all_done = FALSE
        if ( (i0 == grid%GlobalSize(1)) .and. (j0 == grid%GlobalSize(2)) .and. &
             (k0 == grid%GlobalSize(3)) .and. (var == (input%ND+2)) ) all_done = TRUE

        if ( (grid%iGridGlobal == i) .and. (region%myrank == grid%coreRank_inComm) ) then
          if (ANY(dabs(state%DBUF_IO(:,:,:,:)) >= 1d-10)) open (unit=10,file=cmd(1:fnamelen),status='unknown',form='unformatted')
          do mm = 1, input%ND+2
            do kk = 1, grid%GlobalSize(3)
              do jj = 1, grid%GlobalSize(2)
                do ii = 1, grid%GlobalSize(1)
                  if (dabs(state%DBUF_IO(ii,jj,kk,mm)) >= 1d-10) then
                    row = ((kk-1)*grid%GlobalSize(1)*grid%GlobalSize(2)+(jj-1)*grid%GlobalSize(1)+(ii-1))*(input%ND+2) + mm
                    write (10) row,col,state%DBUF_IO(ii,jj,kk,mm)
                    region%modIOnnz = region%modIOnnz + 1
                  end if
                end do
              end do
            end do
          end do
          if (ANY(dabs(state%DBUF_IO(:,:,:,:)) >= 1d-10)) close (10)
          deallocate(state%DBUF_IO)
        end if

      End Do Loop2

      Call MPI_BARRIER(mycomm, ierr)

    End Do Loop1

    Call MPI_BARRIER(mycomm, ierr)

    if (region%myrank == 0) Write (*,'(A)') 'PlasComCM: Done writing "'//cmd(1:fnamelen)//'".'
    if (region%myrank == 0 .and. all_done == TRUE) Write(*,'(A,I8)') 'PlasComCM: nnz = ', region%modIOnnz

  end subroutine write_approximate_matrix

  Subroutine SetUpTransfiniteInterp(region, ng_in_file)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    USE ModPLOT3D_IO

    Implicit None

    Type (t_region), pointer :: region
    Integer :: ng_in_file

    ! ... local variables
    Type (t_mixt_input), pointer :: input
    Type (t_grid), pointer :: grid
    Real(rfreal), Dimension(:), Pointer :: Smax, Smin, SmaxAux, sndbuf(:), rcvbuf(:), ds1(:)
    Real(rfreal) :: ds
    Integer :: ng, i, j, k, l0, lm1, l1, dir, dir2, p
    Integer :: ND, Nc, ierr
    Integer :: N(MAX_ND)
    integer :: status(MPI_STATUS_SIZE)

    Do ng = 1, region%nGrids

      grid  => region%grid(ng)
      input => grid%input
      ND    =  grid%ND
      Nc    =  grid%nCells

      if (grid%iGridGlobal == ng_in_file) then

         ! ... array to hold normalized arc-length coordinates of each point in each direction
         allocate(grid%S(Nc,ND))

         ! ... grid size
         N(:) = 1
         do dir = 1, ND
            N(dir) = grid%ie(dir) - grid%is(dir) + 1
         end do

         ! ... loop through each direction
         do dir = 1, ND
            
            ! ... need to find global arc-lengths and arc-length coordinate of 1st point on this partition in this direction
            allocate(Smax(grid%vds(dir)), SmaxAux(grid%vds(dir)), Smin(grid%vds(dir)), dS1(grid%vds(dir)))

            ! ... initialize
            do i = 1, grid%vds(dir)
               Smax(i) = 0.0_rfreal
               SmaxAux(i) = 0.0_rfreal
               Smin(i) = 0.0_rfreal
               dS1(i) = 0.0_rfreal
            end do

            allocate(sndbuf(grid%vds(dir)*ND),rcvbuf(grid%vds(dir)*ND))
            
            ! ... get coordinates of left processes last gridpoints in this direction
            do l1 = 1, grid%vds(dir)
               l0 = grid%vec_ind(l1,N(dir),dir)
               do dir2 = 1, ND
                  lm1 = (l1-1)*ND + dir2
                  sndbuf(lm1) = grid%XYZ(l0,dir2)
               end do
            end do

            if(grid%myrank_inPencilComm(dir) /= grid%numproc_inPencilComm(dir)-1) then
               !print*,'rank sending',region%myrank
               Call MPI_SEND(sndbuf(1),grid%vds(dir)*ND,MPI_DOUBLE_PRECISION,grid%myrank_inPencilComm(dir)+1,0,grid%PencilComm(dir),ierr)
               
            end if
            if(grid%myrank_inPencilComm(dir) /= 0) then
               !print*,'erer',region%myrank
               Call MPI_RECV(rcvbuf(1),grid%vds(dir)*ND,MPI_DOUBLE_PRECISION,grid%myrank_inPencilComm(dir)-1,0,grid%PencilComm(dir),status,ierr)
               !print*,'rank receiving',region%myrank
            end if

            if(grid%myrank_inPencilComm(dir) /= 0) then
               ! ... get local arc-lengths
               do l1 = 1, grid%vds(dir)

                  l0 = grid%vec_ind(l1,1,dir)
                  ! ... distance between partitions
                  ds = 0.0_rfreal
                  do dir2 = 1, ND
                     lm1 = (l1-1)*ND + dir2
                     ds = ds + (grid%XYZ(l0,dir2)-rcvbuf(lm1))*(grid%XYZ(l0,dir2)-rcvbuf(lm1))
                  end do
                  
                  ! ... record it
                  dS1(l1) = sqrt(ds)
                  ! ... add to arc-length
                  !SmaxAux(l1) = sqrt(ds)

               end do ! ... l1
            end if

            deallocate(sndbuf,rcvbuf)

            ! ... get local arc-lengths
            do l1 = 1, grid%vds(dir)

               do i = 2, N(dir)
                  
                  l0 = grid%vec_ind(l1,i,dir)
                  lm1 = grid%vec_ind(l1,i-1,dir)
                  ! ... distance between i and i-1
                  ds = 0.0_rfreal
                  do dir2 = 1, ND
                     ds = ds + (grid%XYZ(l0,dir2)-grid%XYZ(lm1,dir2))*(grid%XYZ(l0,dir2)-grid%XYZ(lm1,dir2))
                  end do
                  
                  ! ... add it to the arc-length
                  SmaxAux(l1) = SmaxAux(l1) + sqrt(ds)
                  
               end do ! ... i
            end do ! ... l1

            if(grid%numproc_inPencilComm(dir) > 1) then
               do p = 1, grid%numproc_inPencilComm(dir)-1
                  
!                  if( p == grid%myrank_inPencilComm(dir) - 1 .or. p == grid%myrank_inPencilComm(dir)) then
                     ! ... now comunicate these values to the right
                     if(grid%myrank_inPencilComm(dir) == p - 1) then
                        !print*,'rank sending',region%myrank
                        Call MPI_SEND(SmaxAux,grid%vds(dir),MPI_DOUBLE_PRECISION,grid%myrank_inPencilComm(dir)+1,0,grid%PencilComm(dir),ierr)
                        
                     end if
                     if(grid%myrank_inPencilComm(dir) == p) then
                        !print*,'erer',region%myrank
                        Call MPI_RECV(Smin,grid%vds(dir),MPI_DOUBLE_PRECISION,grid%myrank_inPencilComm(dir)-1,0,grid%PencilComm(dir),status,ierr)
                        !print*,'rank receiving',region%myrank
                        do l1 = 1, grid%vds(dir)
                           Smin(l1) = Smin(l1) + dS1(l1)
                           SmaxAux(l1) = SmaxAux(l1) + Smin(l1)
                        end do
                        
                     end if
                     
                  !end if

                  call MPI_BARRIER(grid%PencilComm(dir),ierr)
               end do
            else
               do l1 = 1, grid%vds(dir)
                  Smin(l1) = 0.0_rfreal
               end do
            end if
                     
            ! ... each process now knows the arc-length coordinate of the first point in this direction
            ! ... the last processor knows the full arc-length
            
            Call MPI_AllReduce(SmaxAux,Smax,grid%vds(dir),MPI_DOUBLE_PRECISION,MPI_MAX,grid%PencilComm(dir),ierr)
            
          !   ! ... now all processes know the full arc-length
!             do i = 1, numproc
!                if(region%myrank == i-1) then
!                   print*,'dir',region%myrank, dir
!                   do l1 = 1, grid%vds(dir)
!                      print*,'SmaxAux(l1),Smax(l1),Smin(l1)',SmaxAux(l1),Smax(l1),Smin(l1)
!                   end do
!                end if
!                call MPI_BARRIER(mycomm,ierr)
!            end do
            ! ... calculate the normalized arc-length coordingate
            do l1 = 1, grid%vds(dir)
               
               grid%S(grid%vec_ind(l1,1,dir),dir) = Smin(l1)/Smax(l1)
               
               do i = 2, N(dir)
                  
                  l0 = grid%vec_ind(l1,i,dir)
                  lm1 = grid%vec_ind(l1,i-1,dir)

                  ! ... distance between i and i-1
                  ds = 0.0_rfreal
                  do dir2 = 1, ND
                     ds = ds + (grid%XYZ(l0,dir2)-grid%XYZ(lm1,dir2))*(grid%XYZ(l0,dir2)-grid%XYZ(lm1,dir2))
                  end do
                  
                  ! ... add it to the arc-length
                  grid%S(l0,dir) = grid%S(lm1,dir) + sqrt(ds)/Smax(l1)
                  
               end do ! ... i
            end do ! ... l1
            
            deallocate(Smax,SmaxAux,Smin,dS1)

         end do ! ... dir

      end if ! ... iGridGlobal = ng_in_file

   end Do ! ... ng
   
 End Subroutine SetUpTransfiniteInterp




  SUBROUTINE ReadGridSize(NDIM, ngrid, ND, file, output_flag, ib)

    USE ModPLOT3D_IO
    USE ModHDF5_IO

    IMPLICIT NONE

    INTEGER :: NDIM, ngrid
    INTEGER, POINTER :: ND(:,:)
    CHARACTER(LEN=*) :: file
    INTEGER :: output_flag
    CHARACTER(LEN=2), INTENT(out), OPTIONAL :: ib

    INTEGER :: gridFormat
    LOGICAL :: gf_exists

    INQUIRE(FILE=TRIM(file),EXIST=gf_exists)
    IF (.NOT.(gf_exists.EQV..TRUE.)) THEN
      WRITE (*,'(A,A,A)') 'File ', file(1:LEN_TRIM(file)), ' is not readable'
      STOP
    END IF

    gridFormat = FileFormat(file)

    IF(gridFormat == FORMAT_P3D) THEN
       CALL ReadGridSize_P3D(NDIM, ngrid,ND,file,output_flag,ib)
    ELSE IF(gridFormat == FORMAT_HDF5) THEN
#ifdef HAVE_HDF5
       CALL ReadGridSize_HDF5(NDIM,ngrid,ND,file,output_flag,ib)
#else 
       STOP 'HDF5 not enabled.'
#endif
    ENDIF

  END SUBROUTINE ReadGridSize


  SUBROUTINE SetupIO(region)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    IMPLICIT NONE

    ! ... input/output variables
    Type(t_region),        POINTER :: region

    ! ... local variables
    TYPE(t_mixt_input),    POINTER :: input
    TYPE(t_io_descriptor), POINTER :: ioDescriptor 

    INTEGER          :: numVars
    CHARACTER(LEN=3) :: varNames(20)
       
    input => region%input

    ! Assemble the time 0 io descriptor
    ioDescriptor => ioDescriptors(1)
    varNames(1) = 'xyz'
    IF(input%AdjNS .eqv. .true.) THEN ! ... Adjoint N-S
       varNames(2:3) = (/'av ','avt'/)
    ELSE
       varNames(2:3) = (/'cv ','cvt'/)
    END IF
    numVars = 3
    IF(input%nAuxVars > 0) THEN
       varNames(4:5) = (/'aux','aut'/)
       numVars = 5
    ENDIF
#ifdef BUILD_ELECTRIC
    IF(input%write_phi == TRUE) THEN
       numVars = numVars + 1
       varNames(numVars) = 'phi'
    ENDIF
#endif
    IF(input%write_dv == TRUE)  THEN
       numVars = numVars + 1
       varNames(numVars) = 'dv '
    ENDIF
    IF(input%write_met == TRUE) THEN
       numVars = numVars + 1
       varNames(numVars) = 'met'
    ENDIF
    IF(input%write_jac == TRUE) THEN
       numVars = numVars + 1
       varNames(numVars) = 'jac'
    ENDIF
    IF(input%write_mid == TRUE) THEN
       numVars = numVars + 1
       varNames(numVars) = 'mid'
    ENDIF
!    IF(input%write_rank == TRUE) THEN
!       numVars = numVars + 1
!       varNames(numVars) = 'rank'
!    ENDIF
!    CALL AssembleIODescriptor(ioDescriptor,varNames,numVars)
    
    ! Assemble the (time > 0) io descriptor
    ioDescriptor => ioDescriptors(2)
    numVars = 0
    IF(input%moveGrid == TRUE) THEN
       numVars = numVars + 1
       varNames(numVars) = 'xyz'
    END IF
    numVars = numVars + 1
    IF(input%AdjNS .eqv. .true.) THEN
       varNames(numVars) = 'av '
    ELSE
       varNames(numVars) = 'cv '
    ENDIF
#ifdef BUILD_ELECTRIC
    IF(input%write_phi == TRUE) THEN
       numVars = numVars + 1
       varNames(numVars) = 'phi'
    ENDIF
#endif
    IF(input%nAuxVars > 0) THEN
       numVars = numVars + 1
       varNames(numVars) = 'aux'
    ENDIF
    IF(input%write_dv == TRUE)  THEN
       numVars = numVars + 1
       varNames(numVars) = 'dv '
    ENDIF
    IF(input%write_met == TRUE) THEN
       numVars = numVars + 1
       varNames(numVars) = 'met'
    ENDIF
    IF(input%write_jac == TRUE) THEN
       numVars = numVars + 1
       varNames(numVars) = 'jac'
    ENDIF
    IF(input%write_mid == TRUE) THEN
       numVars = numVars + 1
       varNames(numVars) = 'mid'
    ENDIF
    IF(input%write_rhs == TRUE) THEN
       numVars = numVars + 1
       varNames(numVars) = 'rhs'
    ENDIF
    IF(input%write_cvt == TRUE) THEN
       numVars = numVars + 1
       IF(input%AdjNS .EQV. .TRUE.) THEN
          varNames(numVars) = 'avt'
       ELSE
          varNames(numVars) = 'cvt'
       ENDIF
       IF(input%nAuxVars > 0) THEN
          numVars = numVars + 1
          varNames(numVars) = 'aut'
       ENDIF
    ENDIF
!    CALL AssembleIODescriptor(ioDescriptor,varNames,numVars)
    
    ! Assemble the restart io descriptor
    ioDescriptor => ioDescriptors(3)
    numVars = 0
    IF(input%moveGrid == TRUE) THEN
       numVars = numVars + 1
       varNames(numVars) = 'xyz'
    END IF
    numVars = numVars + 1
    IF(input%AdjNS .eqv. .true.) THEN
       varNames(numVars) = 'av '
       varNames(numVars+1) = 'avt'
    ELSE
       varNames(numVars)  = 'cv '
       varNames(numVars+1)= 'cvt'
    ENDIF
    numVars = numVars + 1
#ifdef BUILD_ELECTRIC
    IF(input%write_phi == TRUE) THEN
       numVars = numVars + 1
       varNames(numVars) = 'phi'
    ENDIF
#endif
    IF(input%nAuxVars > 0) THEN
       numVars = numVars + 1
       varNames(numVars)   = 'aux'
       varNames(numVars+1) = 'aut'
    ENDIF
!    CALL AssembleIODescriptor(ioDescriptor,varNames,numVars)

  End Subroutine SetupIO

End Module ModIO
