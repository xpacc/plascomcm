! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
!-----------------------------------------------------------------------
!
! ModRungeKutta.f90
!
! - Various implementations of Runge-Kutta split from ModTimemarch due
!   to internal IBM compiler errors having to do with limits on the
!   size of modules.
!
! Revision history
! - 26 Aug 2010 : MTC : initial code
!
!-----------------------------------------------------------------------
MODULE ModRungeKutta

CONTAINS

  !---------------------------------------------------------------
  !
  ! BDF_RK5_SemiImplicit :: USE the 3-4-1 BDF with RK5 subiterations
  !
  !---------------------------------------------------------------
  subroutine BDF_RK5_SemiImplicit(region, conv_iter)

    USE ModGlobal
    USE ModDataStruct
    USE ModIO
    USE ModDerivBuildOps
    USE ModDeriv
    USE ModMetrics
    USE ModEOS
    USE ModNavierStokesBC
    USE ModNavierStokesRHS
    USE ModMPI
    USE ModInterp

    Implicit None

    ! ... global variables
    type(t_region), pointer :: region

    ! ... local variables
    integer :: rkLevel, rkStep, i, j, k, ng
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    type(t_mixt_input), pointer :: input
    type(t_mpi_timings), pointer :: mpi_timings
    real(rfreal) :: ibfac_local
    real(rfreal) :: timer, timer_total
    real(rfreal), parameter :: rkalpha(5) = (/ 1.0_rfreal/4.0_rfreal, &
         1.0_rfreal/6.0_rfreal, &
         3.0_rfreal/8.0_rfreal, &
         1.0_rfreal/2.0_rfreal, &
         1.0_rfreal /)
    type(t_rk), pointer :: rk
    integer, allocatable :: err(:)
    integer :: stop_criteria, conv_iter, ierr
    real(rfreal) :: dtau_local, dtau, solution_diff_local, solution_diff

    allocate(err(region%nGrids)); err(:) = 0
    timer_total = MPI_WTIME()

    if(region%global%rk_alloc .eqv. .true.) then
      region%global%rk_alloc = .false.
      if (region%myrank == 0) write (*,'(A)') 'PlasComCM: ==> Using BDF_RK5 time integration <=='
    endif
    allocate(rk)
    allocate(rk%flags(1)); rk%flags(:) = COMBINED

    ! ... simplicity
    input => region%input

    ! ... first save push data down
    Do ng = 1, region%nGrids
      state => region%state(ng)
      grid  => region%grid(ng)
      Do i = 1, grid%nCells
        Do j = 1, size(state%cv,2)
          state%cvOld2(i,j) = state%cvOld1(i,j)
          state%cvOld1(i,j) = state%cv(i,j)
        End Do
      End Do
    End Do

    ! ... update the time
    dtau_local = 0.0_rfreal
    Do ng = 1, region%nGrids
      state => region%state(ng)
      Do i = 1, size(state%time)
        state%time(i) = state%time(i) + state%dt(i)
      End do
      dtau_local = MAX(dtau_local,MAXVAL(state%dt))
    End do
    Call MPI_Allreduce(dtau_local, dtau, 1, MPI_DOUBLE_PRECISION, MPI_MAX, mycomm, ierr)
    dtau = 0.3_dp

    ! ... updates needed for moving grids (only when time changes)
    if (input%moveGrid == TRUE) then

      ! ... move only the rotor grid
      ! call Move_Rotor_Grid(region)

      ! ... move only the cylinder grid
      !call Move_Cylinder_Grid(region)

      ! ... Chimera-specific updates
      if (input%gridType == CHIMERA) then

        ! ... set up the new grid interp
        call Setup_Interpolation(region)

        ! ... update operators since they depend on IBLANK
        Call create_global_mask(region)
        do ng = 1, region%nGrids
          grid => region%grid(ng)
          Call Operator_Setup(region, grid%input, grid, err(ng), ng)
          Call HYPRE_Pade_Operator_Setup(region, ng)
        end do
        if (sum(err(:)) /= 0) Call Output_Rank_As_IBLANK(region)

      end if

      ! ... update metrics
      do ng = 1, region%nGrids
        timer = MPI_Wtime()
        call metrics(region, ng)
        region%mpi_timings(ng)%metrics = region%mpi_timings(ng)%metrics + (MPI_WTIME() - timer)
      end do

    end if

    ! ... initialize
    stop_criteria = FALSE
    conv_iter = 0

    ! ... loop until we converge
    Do While (stop_criteria == FALSE)

      ! ... step k
      do rkStep = 1, 5

        do ng = 1, region%nGrids

          state => region%state(ng)
          grid => region%grid(ng)
          mpi_timings => region%mpi_timings(ng)

          ! ... viscous terms are computed at the first one-and-a-half RK stages, JKim 05/2009
          rk%flags(1) = COMBINED
          if (rkStep > 2) rk%flags(1) = INVISCID_ONLY

          ! ... compute the dependent variables
          call computeDv(region%myrank, grid, state)

          ! ... allocate memory, if needed
          call NS_Allocate_Memory(region, ng, rk%flags(1), rkStep)

          ! ... compute velocity gradient tensor, if needed
          call NS_VelGradTensor(region, ng, rk%flags(1))

          ! ... compute the LES-modified transport variables, if needed
          ! ... this modifies the pressure (state%dv(1)) and temperature (state%dv(2))
          call NS_LES_Model(region, ng, rk%flags(1), rkStep)

          ! ... compute the transport variables
          call computeTv(grid, state)

          ! ... compute temperature gradient, if needed
          call NS_Temperature_Gradient(region, ng, rk%flags(1), rkStep)

          ! ... compute the shock-capturing-modified transport variables, if needed
          call NS_Shock_Capture(region, ng, rk%flags(1), rkStep)

          ! ... add tvCor to tv
          if (rk%flags(1) /= INVISCID_ONLY) then
            if ( grid%input%tvCor_in_dt == TRUE) then
              do j = 1, size(state%tv,2)
                do i = 1, size(state%tv,1)
                  state%tv(i,j) = state%tv(i,j) + state%tvCor(i,j)
                end do ! i
              end do ! j
            end if ! input%tvCor_in_dt
          end if ! rk%flags(1)

        end do ! ng

        if (rkStep == 1) then

          ! ... compute the CFL for reporting
          call NS_Timestep(region)

          do ng = 1, region%nGrids
            state => region%state(ng)
            grid  => region%grid(ng)
            allocate(state%cvOld(size(state%cv,1),size(state%cv,2)))
            allocate(state%rk_rhs(size(state%cv,1),size(state%cv,2),1))
            do k = 1, size(state%cv,2)
              do i = 1, size(state%cv,1)
                state%cvOld(i,k) = state%cv(i,k)
                state%rhs(i,k) = 0.0_rfreal
              end do ! i
            end do ! k
          end do ! ng
        end if

        if (rkStep == 1) then

           do ng = 1, region%nGrids

              state => region%state(ng)
              grid => region%grid(ng)
              mpi_timings => region%mpi_timings(ng)

              ! ... compute the full rhs
              rk%flags(1) = COMBINED

              ! ... update the target
              timer = MPI_WTIME()
              call NS_Update_Target(region, ng)
              mpi_timings%rhs_bc = mpi_timings%rhs_bc + (MPI_WTIME() - timer)

              ! ... compute the RHS for each grid
              call NS_RHS(region, ng, rk%flags(1))

           end do

           do ng = 1, region%nGrids

              state => region%state(ng)
              grid => region%grid(ng)
              mpi_timings => region%mpi_timings(ng)

              ! ... call the boundary conditions
              timer = MPI_WTIME()
              call NS_BC(region, ng, rk%flags(1))
              mpi_timings%rhs_bc = mpi_timings%rhs_bc + (MPI_WTIME() - timer)

              timer = MPI_WTIME()
              call NS_BC_Fix_Value(region)
              region%mpi_timings(:)%rhs_bc = region%mpi_timings(:)%rhs_bc + (MPI_WTIME() - timer)

              ! ... use iblank to zero out rhs for hole and interpolated points
              do i = 1, size(state%rhs,1)
                 ibfac_local = grid%ibfac(i)
                 do k = 1, size(state%rhs,2)
                    state%rhs(i,k) = (state%rhs(i,k) - (1.5_dp * state%cv(i,k) - 2.0_dp * state%cvOld1(i,k) + 0.5_dp * state%cvOld2(i,k))/state%dt(i)) * ibfac_local
                 end do
              end do

              ! ... filter the residual
              if (input%filter .eqv. .true.) call filter(region,ng,TRUE)

              ! ... update
              do k = 1, size(state%cv,2)
                 do i = 1, size(state%cv,1)
                    state%cv(i,k) = state%cvOld(i,k) + rkalpha(rkStep) * dtau * state%rhs(i,k)
                 end do
              end do

           end do

           ! ... explicitly set BC when needed
           call NS_BC_Fix_Value(region)

           ! ... interpolate
           If (input%gridType == CHIMERA .AND. input%volumeIntersection == TRUE) Then
              timer = MPI_WTIME()
              Call Exchange_Interpolated_State(region)
              region%mpi_timings(:)%interp = region%mpi_timings(:)%interp + (MPI_WTIME() - timer)
           End If

!!$        Call write_plot3d_file(region,'cv ');
!!$        Call graceful_exit(region%myrank, 'Stopping after output.')

        else if (rkStep == 2) then

           do ng = 1, region%nGrids

            state => region%state(ng)
            grid => region%grid(ng)
            mpi_timings => region%mpi_timings(ng)

            ! ... zero out the rhs vector
            do j = 1, size(state%rhs,2)
              do i = 1, size(state%rhs,1)
                state%rhs(i,j) = 0.0_rfreal
              end do
            end do

            ! ... compute the viscous terms only first
            rk%flags(1) = VISCOUS_ONLY

            ! ... update the target
            timer = MPI_WTIME()
            call NS_Update_Target(region, ng)
            mpi_timings%rhs_bc = mpi_timings%rhs_bc + (MPI_WTIME() - timer)

            ! ... compute the RHS for each grid
            call NS_RHS(region, ng, rk%flags(1))

            ! ... filter the residual
            if (input%filter .eqv. .true.) call filter(region,ng,TRUE)

            ! ... save viscous terms
            do i = 1, size(state%cv,1)
              ibfac_local = grid%ibfac(i)
              do k = 1, size(state%cv,2)
                state%rk_rhs(i,k,1) = state%rhs(i,k) * ibfac_local
              end do
            end do

            ! ... compute the inviscid terms next
            rk%flags(1) = INVISCID_ONLY

            ! ... compute the RHS for each grid
            ! ... zero out the rhs vector
            do j = 1, size(state%rhs,2)
              do i = 1, size(state%rhs,1)
                state%rhs(i,j) = 0.0_rfreal
              end do
            end do
            call NS_RHS(region, ng, rk%flags(1))

          end do

          do ng = 1, region%nGrids

            state => region%state(ng)
            grid => region%grid(ng)

            ! ... call the boundary conditions
            timer = MPI_WTIME()
            call NS_BC(region, ng, rk%flags(1))
            mpi_timings%rhs_bc = mpi_timings%rhs_bc + (MPI_WTIME() - timer)

            timer = MPI_WTIME()
            call NS_BC_Fix_Value(region)
            region%mpi_timings(:)%rhs_bc = region%mpi_timings(:)%rhs_bc + (MPI_WTIME() - timer)

            ! ... filter the residual
            if (input%filter .eqv. .true.) call filter(region,ng,TRUE)

            ! ... use iblank to zero out rhs for hole and interpolated points
            do i = 1, size(state%rhs,1)
              ibfac_local = grid%ibfac(i)
              do k = 1, size(state%rhs,2)
                state%rhs(i,k) = (state%rhs(i,k) - (1.5_dp * state%cv(i,k) - 2.0_dp * state%cvOld1(i,k) + 0.5_dp * state%cvOld2(i,k))/state%dt(i)) * ibfac_local
              end do
            end do

            ! ... update
            do k = 1, size(state%cv,2)
              do i = 1, size(state%cv,1)
                state%cv(i,k) = state%cvOld(i,k) + rkalpha(rkStep) * dtau * (state%rhs(i,k) + state%rk_rhs(i,k,1))
              end do
            end do

          end do

          ! ... interpolate
          If (input%gridType == CHIMERA .AND. input%volumeIntersection == TRUE) Then
            timer = MPI_WTIME()
            Call Exchange_Interpolated_State(region)
            region%mpi_timings(:)%interp = region%mpi_timings(:)%interp + (MPI_WTIME() - timer)
          End If

        else

          do ng = 1, region%nGrids

            state => region%state(ng)
            grid => region%grid(ng)
            mpi_timings => region%mpi_timings(ng)

            ! ... zero out the rhs vector
            do j = 1, size(state%rhs,2)
              do i = 1, size(state%rhs,1)
                state%rhs(i,j) = 0.0_rfreal
              end do
            end do

            ! ... compute the inviscid terms only
            rk%flags(1) = INVISCID_ONLY

            ! ... update the target
            timer = MPI_WTIME()
            call NS_Update_Target(region, ng)
            mpi_timings%rhs_bc = mpi_timings%rhs_bc + (MPI_WTIME() - timer)

            ! ... compute the RHS for each grid
            call NS_RHS(region, ng, rk%flags(1))

          end do

          do ng = 1, region%nGrids

            state => region%state(ng)
            grid => region%grid(ng)
            mpi_timings => region%mpi_timings(ng)

            ! ... call the boundary conditions
            timer = MPI_WTIME()
            call NS_BC(region, ng, rk%flags(1))
            mpi_timings%rhs_bc = mpi_timings%rhs_bc + (MPI_WTIME() - timer)

            timer = MPI_WTIME()
            call NS_BC_Fix_Value(region)
            region%mpi_timings(:)%rhs_bc = region%mpi_timings(:)%rhs_bc + (MPI_WTIME() - timer)

            ! ... use iblank to zero out rhs for hole and interpolated points
            do i = 1, size(state%rhs,1)
              ibfac_local = grid%ibfac(i)
              do k = 1, size(state%rhs,2)
                state%rhs(i,k) = (state%rhs(i,k) - (1.5_dp * state%cv(i,k) - 2.0_dp * state%cvOld1(i,k) + 0.5_dp * state%cvOld2(i,k))/state%dt(i)) * ibfac_local
              end do
            end do

            ! ... filter the residual
            if (input%filter .eqv. .true.) call filter(region,ng,TRUE)

            ! ... update
            do k = 1, size(state%cv,2)
              do i = 1, size(state%cv,1)
                state%cv(i,k) = state%cvOld(i,k) + rkalpha(rkStep) * dtau * (state%rhs(i,k) + state%rk_rhs(i,k,1))
              end do
            end do

          end do

          ! ... explicitly set BC when needed
          call NS_BC_Fix_Value(region)

          ! ... interpolate
          If (input%gridType == CHIMERA .AND. input%volumeIntersection == TRUE .AND. rkStep < 5) Then
            timer = MPI_WTIME()
            Call Exchange_Interpolated_State(region)
            region%mpi_timings(:)%interp = region%mpi_timings(:)%interp + (MPI_WTIME() - timer)
          End If

       end if

        ! ... deallocate memory, if needed
        if (rkStep == 2) rk%flags(1) = COMBINED ! ... since rk%flags(1) is changing in the 2nd stage
        do ng = 1, region%nGrids
          call NS_Deallocate_Memory(region, ng, rk%flags(1), rkStep)
        end do

      end do ! rkStep

      ! ... check convergence
      solution_diff_local = 0.0_rfreal
      Do ng = 1, region%nGrids
        state => region%state(ng)
        grid  => region%grid(ng)
        Do i = 1, grid%nCells
          Do j = 1, size(state%cv,2)
            solution_diff_local = MAX(solution_diff_local,abs(state%cv(i,j) - state%cvOld(i,j)))
          End Do
        End Do
      End Do
      Call MPI_Allreduce(solution_diff_local, solution_diff, 1, MPI_DOUBLE_PRECISION, MPI_MAX, mycomm, ierr)
      if (solution_diff <= 1e-6) stop_criteria = TRUE
      conv_iter = conv_iter + 1
      if (conv_iter > input%maxsubits) &
        call graceful_exit(region%myrank, 'PlasComCM: Exceeded maximum sub-iterations in BDF_RK5.')

    end do ! ..

    ! ... call the boundary conditions
    timer = MPI_WTIME()
    call NS_BC_Fix_Value(region)
    region%mpi_timings(:)%rhs_bc = region%mpi_timings(:)%rhs_bc + (MPI_WTIME() - timer)

    ! ... interpolation here
!!$    If (input%gridType == CHIMERA .AND. input%volumeIntersection == TRUE) Then
!!$      timer = MPI_WTIME()
!!$      region%mpi_timings(:)%interp = region%mpi_timings(:)%interp + (MPI_WTIME() - timer)
!!$      Call Exchange_Interpolated_State(region)
!!$    End If

    ! ... filter the conservative variables
    if (input%filter .eqv. .true.) then
      do ng = 1, region%nGrids
        call filter(region,ng,FALSE)
      end do
    end if

    deallocate(rk%flags)
    deallocate(rk)

    !call mpi_barrier(mycomm, ierr)

  end subroutine BDF_RK5_SemiImplicit


  ! taken from Magnus et al.
  subroutine additive_RK_coeff(myrank, input, grid, ImplicitFlag)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    ! ... input variables
    Integer :: myrank
    Type(t_mixt_input), Pointer :: input
    Type(t_grid), Pointer :: grid 
    Integer :: ImplicitFlag

    ! ... local variables
    integer :: i, j


    if (input%timeScheme == IMEX_RK4) then

      ! ... 4th order six stage Implicit-Explicit Runge-Kutta scheme
      grid%ARK2_nStages = 6
      ImplicitFlag = TRUE

      ! ... allocate space for implicit and explicit matrices of coefficients
      allocate(grid%ARK2_a_mat_exp(grid%ARK2_nStages,grid%ARK2_nStages))
      allocate(grid%ARK2_a_mat_imp(grid%ARK2_nStages,grid%ARK2_nStages))
      allocate(grid%ARK2_b_vec_exp(grid%ARK2_nStages))
      allocate(grid%ARK2_b_vec_imp(grid%ARK2_nStages))
      allocate(grid%ARK2_c_vec(grid%ARK2_nStages))

      ! ... fill the coefficient arrays
      do i=1,grid%ARK2_nStages
       do j=1,grid%ARK2_nStages
          grid%ARK2_a_mat_exp(i,j) = 0.0_dp
          grid%ARK2_a_mat_imp(i,j) = 0.0_dp
       enddo
       grid%ARK2_b_vec_exp(i) = 0.0_dp
       grid%ARK2_b_vec_imp(i) = 0.0_dp
      enddo

      grid%ARK2_b_vec_exp(1) = 82889.0_dp/524892.0_dp
      grid%ARK2_b_vec_exp(2) = 0.0_dp
      grid%ARK2_b_vec_exp(3) = 15625.0_dp/83664.0_dp
      grid%ARK2_b_vec_exp(4) = 69875.0_dp/102672.0_dp
      grid%ARK2_b_vec_exp(5) =-2260.0_dp/8211.0_dp
      grid%ARK2_b_vec_exp(6) = 1.0_dp/4.0_dp

      do j = 1, 6
        grid%ARK2_b_vec_imp(j) = grid%ARK2_b_vec_exp(j)
      end do 

!!$      !      B5e = 4.09963 B5i = 5.47914 C5e = 1.35811
!!$      !      (Error measures for this scheme)
!!$      !      C5i = 1.38062 E5e = 2.90161 E5i = 4.12639
!!$      b_vec_err(1) = +4586570599.0_dp/29645900160.0_dp
!!$      b_vec_err(2) = 0.0_dp
!!$      b_vec_err(3) = +178811875.0_dp/945068544.0_dp
!!$      b_vec_err(4) = +814220225.0_dp/1159782912.0_dp
!!$      b_vec_err(5) = -3700637.0_dp/11593932.0_dp
!!$      b_vec_err(6) = +61727.0_dp/225920.0_dp
!!$      b_vec_err(:) = b_vec_err(:) - b_vec(:)

      grid%ARK2_a_mat_exp(2,1) = 1.0_dp/2.0_dp
      grid%ARK2_a_mat_exp(3,1) = 13861.0_dp/62500.0_dp
      grid%ARK2_a_mat_exp(3,2) = 6889.0_dp/62500.0_dp
      grid%ARK2_a_mat_exp(4,1) =-116923316275.0_dp/2393684061468.0_dp
      grid%ARK2_a_mat_exp(4,2) =-2731218467317.0_dp/15368042101831.0_dp
      grid%ARK2_a_mat_exp(4,3) = 9408046702089.0_dp/11113171139209.0_dp
      grid%ARK2_a_mat_exp(5,1) =-451086348788.0_dp/2902428689909.0_dp
      grid%ARK2_a_mat_exp(5,2) =-2682348792572.0_dp/7519795681897.0_dp
      grid%ARK2_a_mat_exp(5,3) = 12662868775082.0_dp/11960479115383.0_dp
      grid%ARK2_a_mat_exp(5,4) = 3355817975965.0_dp/11060851509271.0_dp
      grid%ARK2_a_mat_exp(6,1) = 647845179188.0_dp/3216320057751.0_dp
      grid%ARK2_a_mat_exp(6,2) = 73281519250.0_dp/8382639484533.0_dp
      grid%ARK2_a_mat_exp(6,3) = 552539513391.0_dp/3454668386233.0_dp
      grid%ARK2_a_mat_exp(6,4) = 3354512671639.0_dp/8306763924573.0_dp
      grid%ARK2_a_mat_exp(6,5) = 4040.0_dp/17871.0_dp

      grid%ARK2_a_mat_imp(2,1) = 1.0_dp/4.0_dp
      grid%ARK2_a_mat_imp(2,2) = 1.0_dp/4.0_dp
      grid%ARK2_a_mat_imp(3,1) = 8611.0_dp/62500.0_dp
      grid%ARK2_a_mat_imp(3,2) =-1743.0_dp/31250.0_dp
      grid%ARK2_a_mat_imp(3,3) = 1.0_dp/4.0_dp
      grid%ARK2_a_mat_imp(4,1) = 5012029.0_dp/34652500.0_dp
      grid%ARK2_a_mat_imp(4,2) =-654441.0_dp/2922500.0_dp
      grid%ARK2_a_mat_imp(4,3) = 174375.0_dp/388108.0_dp
      grid%ARK2_a_mat_imp(4,4) = 1.0_dp/4.0_dp
      grid%ARK2_a_mat_imp(5,1) = 15267082809.0_dp/155376265600.0_dp
      grid%ARK2_a_mat_imp(5,2) =-71443401.0_dp/120774400.0_dp
      grid%ARK2_a_mat_imp(5,3) = 730878875.0_dp/902184768.0_dp
      grid%ARK2_a_mat_imp(5,4) = 2285395.0_dp/8070912.0_dp
      grid%ARK2_a_mat_imp(5,5) = 1.0_dp/4.0_dp
      grid%ARK2_a_mat_imp(6,1) = 82889.0_dp/524892.0_dp
      grid%ARK2_a_mat_imp(6,2) = 0.0_dp
      grid%ARK2_a_mat_imp(6,3) = 15625.0_dp/83664.0_dp
      grid%ARK2_a_mat_imp(6,4) = 69875.0_dp/102672.0_dp
      grid%ARK2_a_mat_imp(6,5) =-2260.0_dp/8211.0_dp
      grid%ARK2_a_mat_imp(6,6) = 1.0_dp/4.0_dp

      do i = 1, grid%ARK2_nStages
         grid%ARK2_c_vec(i)     = 0.0_dp
         do j = 1, grid%ARK2_nStages
            grid%ARK2_c_vec(i) = grid%ARK2_c_vec(i) + grid%ARK2_a_mat_imp(i,j)
         enddo
      enddo

    else if ( input%timeScheme == EXPLICIT_RK4 ) then

      ! ... 4th order four stage Explicit Runge-Kutta scheme
      grid%ARK2_nStages = 4
      ImplicitFlag = FALSE

      ! ... allocate space for implicit and explicit matrices of coefficients
      allocate(grid%ARK2_a_mat_exp(grid%ARK2_nStages,grid%ARK2_nStages))
      allocate(grid%ARK2_a_mat_imp(grid%ARK2_nStages,grid%ARK2_nStages))
      allocate(grid%ARK2_b_vec_exp(grid%ARK2_nStages))
      allocate(grid%ARK2_b_vec_imp(grid%ARK2_nStages))
      allocate(grid%ARK2_c_vec(grid%ARK2_nStages))

      ! ... fill the coefficient arrays
      do i=1,grid%ARK2_nStages
       do j=1,grid%ARK2_nStages
          grid%ARK2_a_mat_exp(i,j) = 0.0_dp
          grid%ARK2_a_mat_imp(i,j) = 0.0_dp
       enddo
       grid%ARK2_b_vec_exp(i) = 0.0_dp
       grid%ARK2_b_vec_imp(i) = 0.0_dp
      enddo

      ! ... time increments
      grid%ARK2_c_vec(1) = 0.0_dp
      grid%ARK2_c_vec(2) = 0.5_dp
      grid%ARK2_c_vec(3) = 0.5_dp
      grid%ARK2_c_vec(4) = 1.0_dp

      ! ... a_mat_exp
      grid%ARK2_a_mat_exp(2,1) = 0.5_dp
      grid%ARK2_a_mat_exp(3,2) = 0.5_dp
      grid%ARK2_a_mat_exp(4,3) = 1.0_dp

      ! ... weights
      grid%ARK2_b_vec_exp(1) = 1.0_dp / 6.0_dp
      grid%ARK2_b_vec_exp(2) = 1.0_dp / 3.0_dp
      grid%ARK2_b_vec_exp(3) = 1.0_dp / 3.0_dp
      grid%ARK2_b_vec_exp(4) = 1.0_dp / 6.0_dp 

    else

      call graceful_exit(myrank, 'PlasComCM: Unknown Additive RK Scheme.')

    end if

  end subroutine additive_RK_coeff

  Subroutine save_sat_implicit_data(region, rkStep)

    USE ModGlobal
    USE ModDataStruct
    USE ModIO
    USE ModMPI
    USE ModDerivBuildOps
    USE ModDeriv
    USE ModMetrics
    USE ModInterp

    Implicit None

    ! ... incoming variables
    type(t_region), pointer :: region
    integer :: rkStep

    ! ... local variables
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    type(t_mixt_input), pointer :: input
    type(t_patch), pointer :: patch
    integer :: npatch, Nc, Np(MAX_ND), N(MAX_ND), i, j, k, l0, lp

    ! ... loop over all patches.  If the patch is a SAT_NOSLIP_ISOTHERMAL_WALL, then
    ! ... we need to save the patch%sat_fac times the old flow variable.
    do npatch = 1, region%nPatches

      patch => region%patch(npatch)
      grid => region%grid(patch%gridID)
      state => region%state(patch%gridID)


      Nc = grid%nCells
      N  = 1; Np = 1;
      do j = 1, grid%ND
        N(j)  = grid%ie(j)-grid%is(j)+1
        Np(j) = patch%ie(j) - patch%is(j) + 1
      end do;


      if (patch%bcType == SAT_NOSLIP_ISOTHERMAL_WALL) then

        Do k = patch%is(3), patch%ie(3)
          Do j = patch%is(2), patch%ie(2)
            Do i = patch%is(1), patch%ie(1)

              l0 = (k-grid%is(3))*N(1)*N(2) + (j-grid%is(2))*N(1) + i-grid%is(1)+1
              lp = (k-patch%is(3))*Np(1)*Np(2) &
                   + (j-patch%is(2))*Np(1) + (i-patch%is(1)+1)

              patch%sat_fac_rk(lp,:,rkStep) = patch%sat_fac(lp) * state%cv(l0,:)

            End Do
          End Do
        End Do

      end if

    end do

  End Subroutine save_sat_implicit_data

  Subroutine update_implicit_part(region,rkStep,a_mat_imp,nstage_imp)

    USE ModGlobal
    USE ModDataStruct
    USE ModIO
    USE ModMPI
    USE ModDerivBuildOps
    USE ModDeriv
    USE ModMetrics
    USE ModInterp

    Implicit None

    ! ... incoming variables
    type(t_region), pointer :: region
    integer :: rkStep
    real(rfreal) :: a_mat_imp(6,6)
    integer :: nstage_imp(6)

    ! ... local variables
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    type(t_mixt_input), pointer :: input
    type(t_patch), pointer :: patch
    integer :: npatch, Nc, Np(MAX_ND), N(MAX_ND), i, j, k, l0, lp, ii

    ! ... loop over all patches.  If the patch is a SAT_NOSLIP_ISOTHERMAL_WALL, then
    ! ... first update cv with the old implicit rhs
    do npatch = 1, region%nPatches

      patch => region%patch(npatch)
      grid => region%grid(patch%gridID)
      state => region%state(patch%gridID)

      Nc = grid%nCells
      N  = 1; Np = 1;
      do j = 1, grid%ND
        N(j)  = grid%ie(j)-grid%is(j)+1
        Np(j) = patch%ie(j) - patch%is(j) + 1
      end do;

      if (patch%bcType == SAT_NOSLIP_ISOTHERMAL_WALL) then

        Do k = patch%is(3), patch%ie(3)
          Do j = patch%is(2), patch%ie(2)
            Do i = patch%is(1), patch%ie(1)

              l0 = (k-grid%is(3))*N(1)*N(2) + (j-grid%is(2))*N(1) + i-grid%is(1)+1
              lp = (k-patch%is(3))*Np(1)*Np(2) &
                   + (j-patch%is(2))*Np(1) + (i-patch%is(1)+1)

              ! ... first update the old implicit terms
              do ii = 1, nstage_imp(rkStep)-1
                state%cv(l0,:) = state%cv(l0,:) + a_mat_imp(rkStep,ii) * patch%sat_fac_rk(lp,:,ii) * state%dt(l0)
              end do

              ! ... now invert the system for the current solution
              state%cv(l0,:) = state%cv(l0,:) / (1.0_rfreal - patch%sat_fac(lp) * a_mat_imp(rkStep,nstage_imp(rkStep)) * state%dt(l0) )

            End Do
          End Do
        End Do

      end if

    end do

  End Subroutine update_implicit_part

  Subroutine finalize_implicit_part(region, b_vec)

    USE ModGlobal
    USE ModDataStruct
    USE ModIO
    USE ModMPI
    USE ModDerivBuildOps
    USE ModDeriv
    USE ModMetrics
    USE ModInterp

    Implicit None

    ! ... incoming variables
    type(t_region), pointer :: region
    real(rfreal) :: b_vec(6)

    ! ... local variables
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    type(t_mixt_input), pointer :: input
    type(t_patch), pointer :: patch
    integer :: npatch, Nc, Np(MAX_ND), N(MAX_ND), i, j, k, l0, lp, ii

    ! ... loop over all patches.  If the patch is a SAT_NOSLIP_ISOTHERMAL_WALL, then
    ! ... first update cv with the old implicit rhs
    do npatch = 1, region%nPatches

      patch => region%patch(npatch)
      grid => region%grid(patch%gridID)
      state => region%state(patch%gridID)

      Nc = grid%nCells
      N  = 1; Np = 1;
      do j = 1, grid%ND
        N(j)  = grid%ie(j)-grid%is(j)+1
        Np(j) = patch%ie(j) - patch%is(j) + 1
      end do;

      if (patch%bcType == SAT_NOSLIP_ISOTHERMAL_WALL) then

        Do k = patch%is(3), patch%ie(3)
          Do j = patch%is(2), patch%ie(2)
            Do i = patch%is(1), patch%ie(1)

              l0 = (k-grid%is(3))*N(1)*N(2) + (j-grid%is(2))*N(1) + i-grid%is(1)+1
              lp = (k-patch%is(3))*Np(1)*Np(2) &
                   + (j-patch%is(2))*Np(1) + (i-patch%is(1)+1)

              ! ... update the old implicit terms
              do ii = 1, size(b_vec)
                state%cv(l0,:) = state%cv(l0,:) + b_vec(ii) * patch%sat_fac_rk(lp,:,ii) * state%dt(l0)
              end do

            End Do
          End Do
        End Do

      end if

    end do

  End Subroutine finalize_implicit_part

  !---------------------------------------------------------------
  !
  ! RK4 :: Runge-Kutta (4th order) for non-linear systems of
  !        differential equations
  !
  !---------------------------------------------------------------
  subroutine RK4_Q1D(region)

    USE ModGlobal
    USE ModDataStruct
    USE ModIO
    USE ModDerivBuildOps
    USE ModDeriv
    USE ModMetrics
    USE ModEOS
    USE ModQ1D
    USE ModMPI
    USE ModInterp
    USE ModNavierStokesRHS

    Implicit None

    type(t_region), pointer :: region

    ! ... local variables
    integer :: rkLevel, rkStep, i, j, k, ng
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    type(t_mixt_input), pointer :: input
    type(t_mpi_timings), pointer :: mpi_timings
    type(t_interp), pointer :: interp
    real(rfreal), parameter :: rkTimeFac(4) = (/ 0.0_rfreal, 0.5_rfreal, 0.5_rfreal, 1.0_rfreal /)
    real(rfreal), parameter :: rkRHSFac(4) = (/ 0.5_rfreal, 0.5_rfreal, 1.0_rfreal, 1.0_rfreal /)
    type(t_rk), pointer :: rk
    integer, allocatable :: err(:)
    
    real(rfreal) :: timer, timer_total

    timer_total = MPI_WTIME()
    allocate(err(region%nGrids)); err(:) = 0
   
    if(.not. region%rkInit) then
      allocate(region%rk)
      region%rkInit = .true.
    end if

    rk => region%rk

    if(region%global%rk_alloc .eqv. .true.) then
      region%global%rk_alloc = .false.
      if (region%myrank == 0) write (*,'(A)') 'PlasComCM: ==> Using RK4_Q1D time integration <=='
    endif
    allocate(rk%flags(1)); rk%flags(:) = COMBINED

    ! ... simplicity
    input => region%input

    ! ... step k
    do rkStep = 1, 4

      do ng = 1, region%nGrids

        state => region%state(ng)
        grid  => region%grid(ng)
        mpi_timings => region%mpi_timings(ng)

        ! ... compute the dependent variables
        call computeDv(region%myrank, grid, state)

        ! ... allocate memory, if needed
        call NS_Allocate_Memory(region, ng, rk%flags(1), rkStep)

        ! ... compute velocity gradient tensor, if needed
        call NS_VelGradTensor(region, ng, rk%flags(1))

        ! ... compute the LES-modified transport variables, if needed
        ! ... this modifies the pressure (state%dv(1)) and temperature (state%dv(2))
        call NS_LES_Model(region, ng, rk%flags(1), rkStep)

        ! ... compute the transport variables
        call computeTv(grid, state)

        ! ... compute temperature gradient, if needed
        call NS_Temperature_Gradient(region, ng, rk%flags(1), rkStep)

        ! ... compute the shock-capturing-modified transport variables, if needed
        if (grid%input%shock /= 3) call NS_Shock_Capture(region, ng, rk%flags(1), rkStep)

        ! ... add tvCor to tv
        if ( grid%input%tvCor_in_dt == TRUE) then
          do j = 1, size(state%tv,2)
            do i = 1, grid%nCells
              state%tv(i,j) = state%tv(i,j) + state%tvCor(i,j)
            end do
          end do
        end if

      end do

      if (rkStep == 1) then

        ! ... allocate space for RHS and old values of conserved variables
        do ng = 1, region%nGrids
          state => region%state(ng)
          grid  => region%grid(ng)

          if (.not.associated(state%rhs) .eqv. .true.) &
            allocate(state%rhs(size(state%cv,1),size(state%cv,2)))
          if (.not.associated(state%rk_rhs) .eqv. .true.) &
            allocate(state%rk_rhs(size(state%cv,1),size(state%cv,2),3))
          if (.not.associated(state%timeOld) .eqv. .true.) &
            allocate(state%timeOld(size(state%time)))
          if (.not.associated(state%cfl) .eqv. .true.) &
            allocate(state%cfl(size(state%time)))
          if (.not.associated(state%cvOld) .eqv. .true.) &
            allocate(state%cvOld(size(state%cv,1),size(state%cv,2)))
          if (.not.associated(state%dt) .eqv. .true.) &
            allocate(state%dt(size(state%cv,1)))

          ! ... save time
          do i = 1, size(state%time)
            state%timeOld(i) = state%time(i)
          end do

          ! save solution
          do k = 1, size(state%cv,2)
            do i = 1, size(state%cv,1)
              state%cvOld(i,k) = state%cv(i,k)
            end do
          end do

          ! ... allocate space for RHS and old values of auxillary variables
          if (associated(state%auxVars) .eqv. .true.) then
            allocate(state%rk_rhs_AuxVars(size(state%rhs_AuxVars,1),size(state%rhs_AuxVars,2),3))
            allocate(state%auxVarsOld(size(state%auxVars,1),size(state%auxVars,2)))
            do k = 1, size(state%auxVars,2)
              do i = 1, size(state%auxVars,1)
                state%auxVarsOld(i,k) = state%auxVars(i,k)
              end do
            end do
          end if

          ! ... level set
          if (associated(state%levelSet) .eqv. .true.) then
            allocate(state%rk_rhs_levelSet(grid%nCells,3))
            allocate(state%levelSetOld(grid%nCells))
            do k = 1, grid%nCells
              state%levelSetOld(k) = state%levelSet(k)
            end do
          end if

        end do
      end if

      if (rkStep == 1) call NS_Timestep(region)

      do ng = 1, region%nGrids

        state => region%state(ng)
        grid => region%grid(ng)

        ! ... time vector
        do i = 1, size(state%time)
          state%time(i) = state%timeOld(i) + rkTimeFac(rkStep) * state%dt(i)
          do j = 1, grid%ND+2
            state%rhs(i,j) = 0.0_rfreal
          end do
        end do

        ! ... zero out aux var RHS
        if (associated(state%auxVars) .eqv. .true.) then
          do j = 1, size(state%AuxVars,2)
            do i = 1, grid%nCells
              state%rhs_AuxVars(i,j) = 0.0_rfreal
            end do
          end do
        end if

        ! ... zero out levelset RHS
        if (associated(state%levelSet) .eqv. .true.) then
          do i = 1, grid%nCells
            state%rhs_levelSet(i) = 0.0_rfreal
          end do
        end if

        ! ... update the target
        timer = MPI_WTIME()
        call Q1D_Update_Target(region, ng)
        mpi_timings%rhs_bc = mpi_timings%rhs_bc + (MPI_WTIME() - timer)

        ! ... compute the RHS for each grid
        call Q1D_RHS(region, ng, rk%flags(1))

      end do

      do ng = 1, region%nGrids

        mpi_timings => region%mpi_timings(ng)

        ! ... call the boundary conditions
        timer = MPI_WTIME()
        call Q1D_BC(region, ng, rk%flags(1))
        mpi_timings%rhs_bc = mpi_timings%rhs_bc + (MPI_WTIME() - timer)

      end do

      do ng = 1, region%nGrids

        state => region%state(ng)
        grid => region%grid(ng)

        ! ... use iblank to zero out rhs for hole and interpolated points
        do k = 1, size(state%rhs,2)
          do i = 1, size(state%rhs,1)
            state%rhs(i,k) = state%rhs(i,k) * dble(min(abs(grid%iblank(i)),1))
          end do
        end do

        ! ... use iblank to zero out rhs for hole and interpolated points
        if (associated(state%auxVars) .eqv. .true.) then
          do k = 1, size(state%rhs_AuxVars,2)
            do i = 1, size(state%rhs_AuxVars,1)
              state%rhs_AuxVars(i,k) = state%rhs_AuxVars(i,k) * dble(min(abs(grid%iblank(i)),1))
            end do
          end do
        end if

        ! ... use iblank to zero out rhs for hole and interpolated points
        if (associated(state%levelSet) .eqv. .true.) then
          do i = 1, size(state%rhs_AuxVars,1)
            state%rhs_levelSet(i) = state%rhs_levelSet(i) * dble(min(abs(grid%iblank(i)),1))
          end do
        end if


        ! ... update
        if (rkStep <= 3) then

          ! ... conservative variables
          do k = 1, size(state%cv,2)
            do i = 1, size(state%cv,1)
              state%cv(i,k) = state%cvOld(i,k) &
                   + rkRHSFac(rkStep) * state%rhs(i,k) * state%dt(i)
            end do
          end do

          ! ... auxilliary variables
          if (associated(state%auxVars) .eqv. .true.) then
            do k = 1, size(state%auxVars,2)
              do i = 1, size(state%auxVars,1)
                state%auxVars(i,k) = state%auxVarsOld(i,k) &
                     + rkRHSFac(rkStep) * state%rhs_auxVars(i,k) * state%dt(i)
              end do
            end do
          end if

          ! ... levelSet variables
          if (associated(state%levelSet) .eqv. .true.) then
            do i = 1, size(state%levelSet,1)
              state%levelSet(i) = state%levelSetOld(i) &
                   + rkRHSFac(rkStep) * state%rhs_levelSet(i) * state%dt(i)
            end do
          end if


          ! ... save this rhs
          do k = 1, size(state%rhs,2)
            do i = 1, size(state%rhs,1)
              state%rk_rhs(i,k,rkStep) = state%rhs(i,k)
            end do
          end do

          ! ... auxilliary variables
          if (associated(state%auxVars) .eqv. .true.) then
            do k = 1, size(state%rhs_auxVars,2)
              do i = 1, size(state%rhs_AuxVars,1)
                state%rk_rhs_AuxVars(i,k,rkStep) = state%rhs_AuxVars(i,k)
              end do
            end do
          end if

          ! ... levelSet variables
          if (associated(state%levelSet) .eqv. .true.) then
            do i = 1, size(state%levelSet,1)
              state%rk_rhs_levelSet(i,rkStep) = state%rhs_levelSet(i)
            end do
          end if

        end if

      end do ! ng

      ! ... interpolation here
      If (input%gridType == CHIMERA .AND. input%volumeIntersection == TRUE) Then
        timer = MPI_WTIME()
        if (rkStep <= 3) then
          Call Exchange_Interpolated_State(region)
        end if
        region%mpi_timings(:)%interp = region%mpi_timings(:)%interp + (MPI_WTIME() - timer)
      End if

    end do ! rkStep

    ! ... add-em up
    do ng = 1, region%nGrids

      grid => region%grid(ng)
      state => region%state(ng)

      do i = 1, size(state%time)
        state%time(i) = state%timeOld(i) + state%dt(i)
      end do

      ! ... conservative variables
      do k = 1, size(state%cv,2)
        do i = 1, size(state%cv,1)
          state%cv(i,k) = state%cvOld(i,k) + &
               (state%rk_rhs(i,k,1) + 2.0_rfreal * state%rk_rhs(i,k,2) + &
               2.0_rfreal * state%rk_rhs(i,k,3) + state%rhs(i,k)) * state%dt(i) / 6.0_rfreal
        end do
      end do

      ! ... auxillary variables
      if (associated(state%auxVars) .eqv. .true.) then
        do k = 1, size(state%auxVars,2)
          do i = 1, size(state%auxVars,1)
            state%AuxVars(i,k) = state%AuxVarsOld(i,k) + &
                 (state%rk_rhs_AuxVars(i,k,1) + 2.0_rfreal * state%rk_rhs_AuxVars(i,k,2) + &
                 2.0_rfreal * state%rk_rhs_AuxVars(i,k,3) + state%rhs_AuxVars(i,k)) * state%dt(i) / 6.0_rfreal
          end do
        end do
      end if

      ! ... levelSet variables
      if (associated(state%levelSet) .eqv. .true.) then
        do i = 1, size(state%auxVars,1)
          state%levelSet(i) = state%levelSetOld(i) + &
               (state%rk_rhs_levelSet(i,1) + 2.0_rfreal * state%rk_rhs_levelSet(i,2) + &
               2.0_rfreal * state%rk_rhs_levelSet(i,3) + state%rhs_levelSet(i)) * state%dt(i) / 6.0_rfreal
        end do
      end if


      deallocate(state%rk_rhs, state%timeOld, state%cvOld)
      if (associated(state%auxVars) .eqv. .true.) deallocate(state%rk_rhs_AuxVars, state%AuxVarsOld)
      if (associated(state%levelSet) .eqv. .true.) deallocate(state%rk_rhs_levelSet, state%levelSetOld)

      state%AuxVars(:,:) = 1.0_rfreal

    end do

    ! ... call the boundary conditions
!!$    timer = MPI_WTIME()
!!$    call Q1D_BC_Fix_Value(region)
!!$    region%mpi_timings(:)%rhs_bc = region%mpi_timings(:)%rhs_bc + (MPI_WTIME() - timer)

    ! ... interpolation here
    If (input%gridType == CHIMERA .AND. input%volumeIntersection == TRUE) Then
      timer = MPI_WTIME()
      Call Exchange_Interpolated_State(region)
      region%mpi_timings(:)%interp = region%mpi_timings(:)%interp + (MPI_WTIME() - timer)
    End If

    ! ... filter the conservative variables
    if (input%filter .eqv. .true.) then
      do ng = 1, region%nGrids
        call filter(region,ng,FALSE)
      end do
    end if

    ! ... shock filtering, if needed
    if (input%shock == 3) then
      do ng = 1, region%nGrids
        call Q1D_Shock_Filter(region,ng)
      end do
    end if

    deallocate(rk%flags)
    deallocate(rk)
    !call mpi_barrier(mycomm, ierr)

  end subroutine RK4_Q1D
  !---------------------------------------------------------------
  !
  ! LINRK4 :: Runge-Kutta (4th order) for linear systems of
  !           differential equations
  !
  !---------------------------------------------------------------
  subroutine LINRK4(region)

    USE ModGlobal
    USE ModDataStruct
    USE ModIO
    USE ModDerivBuildOps
    USE ModDeriv
    USE ModMetrics
    USE ModEOS
    USE ModLinNavierStokesBC
    USE ModLinNavierStokesRHS
    USE ModMPI
    USE ModInterp

    Implicit None

    type(t_region), pointer :: region

    ! ... local variables
    integer :: rkLevel, rkStep, i, j, k, ng
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state, mean
    type(t_mixt_input), pointer :: input
    type(t_mpi_timings), pointer :: mpi_timings
    type(t_interp), pointer :: interp
    real(rfreal), parameter :: rkTimeFac(4) = (/ 0.0_rfreal, 0.5_rfreal, 0.5_rfreal, 1.0_rfreal /)
    real(rfreal), parameter :: rkRHSFac(4) = (/ 0.5_rfreal, 0.5_rfreal, 1.0_rfreal, 1.0_rfreal /)
    type(t_rk), pointer :: rk
    integer, allocatable :: err(:)
    real(rfreal) :: timer, timer_total, ibfac_local

    timer_total = MPI_WTIME()
    allocate(err(region%nGrids)); err(:) = 0
  
    if(.not. region%rkInit) then
      allocate(region%rk)
      region%rkInit = .true.
    end if

    rk => region%rk

    if(region%global%rk_alloc .eqv. .true.) then
      region%global%rk_alloc = .false.
      if (region%myrank == 0) write (*,'(A)') 'PlasComCM: ==> Using LinRK4 time integration <=='
    endif

    ! ... allocate rk data
    allocate(rk%flags(1)); rk%flags(1) = COMBINED

    ! ... simplicity
    input => region%input

    ! ... step k
    do rkStep = 1, 4

      ! ... updates needed for moving grids (only when time changes)
      if (input%moveGrid == TRUE) then

        ! ... move only the rotor grid
        !call Move_Rotor_Grid(region)

        ! ... move only the cylinder grid
        !call Move_Cylinder_Grid(region)

        ! ... Chimera-specific updates
        if (input%gridType == CHIMERA .and. ((rkStep == 2) .OR. (rkStep == 4))) then

          ! ... set up the new grid interp
          call Setup_Interpolation(region)

          ! ... update operators since they depend on IBLANK
          Call create_global_mask(region)
          do ng = 1, region%nGrids
            grid => region%grid(ng)
            Call Operator_Setup(region, grid%input, grid, err(ng), ng)
            Call HYPRE_Pade_Operator_Setup(region, ng)
          end do
          if (sum(err(:)) /= 0) Call Output_Rank_As_IBLANK(region)

        end if

        ! ... update metrics
        do ng = 1, region%nGrids
          call metrics(region, ng)
        end do

      end if

      do ng = 1, region%nGrids

        state => region%state(ng)
        mean  => region%mean(ng)
        grid  => region%grid(ng)
        mpi_timings => region%mpi_timings(ng)

        ! ... compute the dependent variables
        call computeDv_LinIdealGas(grid, state, mean)

        ! ... allocate memory, if needed
        call LinNS_Allocate_Memory(region, ng, rk%flags(1), rkStep)

      end do

      if (rkStep == 1) then
        do ng = 1, region%nGrids
          state => region%state(ng)
          grid  => region%grid(ng)
          if (.not.associated(state%rhs)     .eqv. .true.) allocate(state%rhs(size(state%cv,1),size(state%cv,2)))
          if (.not.associated(state%rk_rhs)  .eqv. .true.) allocate(state%rk_rhs(size(state%cv,1),size(state%cv,2),3))
          if (.not.associated(state%timeOld) .eqv. .true.) allocate(state%timeOld(size(state%time)))
          if (.not.associated(state%cfl)     .eqv. .true.) allocate(state%cfl(size(state%time)))
          if (.not.associated(state%cvOld)   .eqv. .true.) allocate(state%cvOld(size(state%cv,1),size(state%cv,2)))
          if (.not.associated(state%dt)      .eqv. .true.) allocate(state%dt(size(state%cv,1)))
          do i = 1, grid%nCells
            state%timeOld(i) = state%time(i)
          end do
          do k = 1, size(state%cv,2)
            do i = 1, grid%nCells
              state%cvOld(i,k) = state%cv(i,k)
            end do
          end do
        end do
      end if

      if (rkStep == 1) then
  
        call LinNS_Timestep(region) 
        call SAMPLE_CHECK(region)

      endif

      do ng = 1, region%nGrids

        state => region%state(ng)
        grid => region%grid(ng)

        ! ... time vector
        do i = 1, grid%nCells
          state%time(i) = state%timeOld(i) + rkTimeFac(rkStep) * state%dt(i)
        end do

        ! ... zero out the rhs vector
        Do j = 1, size(state%rhs,2)
          Do i = 1, grid%nCells
            state%rhs(i,j) = 0.0_rfreal
          End Do
        End Do

        ! ... update the target
        timer = MPI_WTIME()
        if (input%fv_grid(ng) == FINITE_DIF) call LinNS_Update_Target(region, ng)
        mpi_timings%rhs_bc = mpi_timings%rhs_bc + (MPI_WTIME() - timer)

        ! ... compute the RHS for each grid
        call LinNS_RHS(region, ng, rk%flags(1))

      end do

      do ng = 1, region%nGrids

        mpi_timings => region%mpi_timings(ng)

        ! ... add ``Lighthill force''
        ! call Lighthill_Force(region, ng)

        ! ... call the boundary conditions
        timer = MPI_WTIME()
        if (input%fv_grid(ng) == FINITE_DIF) call LinNS_BC(region, ng, rk%flags(1))
        mpi_timings%rhs_bc = mpi_timings%rhs_bc + (MPI_WTIME() - timer)

      end do

      do ng = 1, region%nGrids

        state => region%state(ng)
        grid => region%grid(ng)

        ! ... use iblank to zero out rhs for hole points
        do k = 1, size(state%rhs,2)
          do i = 1, grid%nCells
            ibfac_local = grid%ibfac(i)
            state%rhs(i,k) = state%rhs(i,k) * ibfac_local
          end do
        end do

        ! ... update
        if (rkStep <= 3) then
          do k = 1, size(state%cv,2)
            do i = 1, grid%nCells
              state%cv(i,k) = state%cvOld(i,k) + rkRHSFac(rkStep) * state%rhs(i,k) * state%dt(i)
            end do
          end do

          ! ... save this rhs
          do k = 1, size(state%rhs,2)
            do i = 1, grid%nCells
              state%rk_rhs(i,k,rkStep) = state%rhs(i,k)
            end do
          end do
        end if

        ! ... call the boundary conditions
        !call computeDv(region%myrank, grid, state)
        !call NS_BC_Fix_Value(region)

      end do ! ng

      ! ... interpolation here
      If (input%gridType == CHIMERA .AND. input%volumeIntersection == TRUE) Then
        timer = MPI_WTIME()
        if (rkStep <= 3) then
          Call Exchange_Interpolated_State(region)
        end if
        region%mpi_timings(:)%interp = region%mpi_timings(:)%interp + (MPI_WTIME() - timer)
      End if

      ! ... deallocate memory, if needed
      do ng = 1, region%nGrids
        call LinNS_Deallocate_Memory(region, ng, rk%flags(1), rkStep)
      end do

    end do ! rkStep

    ! ... add-em up
    do ng = 1, region%nGrids

      grid  => region%grid(ng)
      state => region%state(ng)

      do i = 1, grid%nCells
        state%time(i) = state%timeOld(i) + state%dt(i)
      end do

      ! ... update state%cv
      do k = 1, size(state%cv,2)
        do i = 1, grid%nCells
          ibfac_local = grid%ibfac(i)
          state%cv(i,k) = state%cvOld(i,k) + &
               (state%rk_rhs(i,k,1) + 2.0_rfreal * state%rk_rhs(i,k,2) + &
               2.0_rfreal * state%rk_rhs(i,k,3) + state%rhs(i,k)) * state%dt(i) / 6.0_rfreal * ibfac_local
        end do
      end do

      deallocate(state%rhs, state%rk_rhs, state%timeOld, state%cvOld)
      nullify(state%rhs, state%rk_rhs, state%timeOld, state%cvOld)

    end do

    ! ... interpolation here
    If (input%gridType == CHIMERA .AND. input%volumeIntersection == TRUE) Then
      timer = MPI_WTIME()
      Call Exchange_Interpolated_State(region)
      region%mpi_timings(:)%interp = region%mpi_timings(:)%interp + (MPI_WTIME() - timer)
    End If

    ! ... filter the conservative variables
    if (input%filter .eqv. .true.) then
      if (mod(region%global%main_ts_loop_index,1) == 0) then
        do ng = 1, region%nGrids
          call filter(region,ng,FALSE)
        end do
      end if
    end if

    deallocate(rk%flags)
    deallocate(rk)
    !call mpi_barrier(mycomm, ierr)

  end subroutine LINRK4

  !---------------------------------------------------------------
  !
  ! RK4 :: Runge-Kutta (4th order) for adjoint N-S equations
  !
  !---------------------------------------------------------------
  subroutine RK4_AdjNS(region)

    USE ModGlobal
    USE ModDataStruct
    USE ModIO
    USE ModDeriv
    USE ModMetrics
    USE ModEOS
    USE ModNavierStokesBC
    USE ModNavierStokesRHS
    USE ModMPI
    USE ModInterp
    USE ModActuator
    !USE ModLighthill
    USE ModAdjointNS

    Implicit None

    type(t_region), pointer :: region

    ! ... local variables
    integer :: rkLevel, rkStep, i, j, k, ng
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    type(t_mixt_input), pointer :: input
    type(t_mpi_timings), pointer :: mpi_timings
    type(t_interp), pointer :: interp
    real(rfreal), parameter :: rkTimeFac(4) = (/ 0.0_rfreal, 0.5_rfreal, 0.5_rfreal, 1.0_rfreal /)
    real(rfreal), parameter :: rkRHSFac(4) = (/ 0.5_rfreal, 0.5_rfreal, 1.0_rfreal, 1.0_rfreal /)
    type(t_rk), pointer :: rk
    integer, allocatable :: err(:)
    real(rfreal) :: timer, timer_total, ibfac_local

    timer_total = MPI_WTIME()
    allocate(err(region%nGrids)); err(:) = 0

    if(.not. region%rkInit) then
      allocate(region%rk)
      region%rkInit = .true.
    end if

    rk => region%rk

    if(region%global%rk_alloc .eqv. .true.) then
      region%global%rk_alloc = .false.
      if (region%myrank == 0) write (*,'(A)') 'PlasComCM: ==> Using RK4 backward time integration <=='
    end if ! rk_alloc

    ! ... allocate rk data
    allocate(rk%flags(1)); rk%flags(1) = COMBINED
    if (region%input%RE == 0.0_rfreal .and. region%input%tvCor_in_dt == FALSE) &
                                                     rk%flags(1) = INVISCID_ONLY

    ! ... simplicity
    input => region%input

    ! ... step k
    do rkStep = 1, 4

      ! ... updates needed for moving grids (only when time changes)
      if (input%moveGrid == TRUE) then
        ! ... do nothing for moving grids; not supported in optimization
      end if

      ! ... state%rhs has to be allocated prior to computing LES, JKim 04/2010
      if (rkStep == 1) then
        do ng = 1, region%nGrids
          state => region%state(ng)
          grid  => region%grid(ng)
          if (.not.associated(state%rhs)     .eqv. .true.) allocate(state%rhs(size(state%cv,1),size(state%cv,2)))
        end do
      end if
      do ng = 1, region%nGrids
        state => region%state(ng)
        grid  => region%grid(ng)
        ! ... zero out the rhs vector
        Do j = 1, size(state%rhs,2)
          Do i = 1, grid%nCells
            state%rhs(i,j) = 0.0_rfreal
          End Do
        End Do
      end do

      if (MOD(rkStep,2) == 0) then ! ... interpolate cv with respect to time
                                   ! ... check rkTimeFac why we're doing in this way
        do ng = 1, region%nGrids
          state => region%state(ng)
          grid  => region%grid(ng)

          do i = 1, grid%nCells
            state%time(i) = state%timeOld(i) - rkTimeFac(rkStep) * state%dt(i) ! ... Adjoint N-S: backward in time
          end do ! i
         !if (state%time(1) < 0.0_rfreal) call graceful_exit(region%myrank, "... ERROR: time became negative")

          call AdjInterpolate_cv(region, ng)
        end do ! ng
      end if ! MOD(rkStep,2)

      do ng = 1, region%nGrids

        state => region%state(ng)
        grid  => region%grid(ng)
        mpi_timings => region%mpi_timings(ng)

        ! ... compute the dependent variables
        call computeDv(region%myrank, grid, state)

        ! ... allocate memory, if needed
        call AdjNS_Allocate_Memory(region, ng, rk%flags(1), rkStep)

!!$     ! ... compute velocity gradient tensor, if needed
!!$     call NS_VelGradTensor(region, ng, rk%flags(1))

!!$     ! ... compute the LES-modified transport variables, if needed
!!$     ! ... this modifies the pressure (state%dv(1)) and temperature (state%dv(2))
!!$     call NS_LES_Model(region, ng, rk%flags(1), rkStep)

        ! ... compute the transport variables
        call computeTv(grid, state)

!!$     ! ... compute temperature gradient, if needed
!!$     call NS_Temperature_Gradient(region, ng, rk%flags(1), rkStep)

!!$     ! ... compute the shock-capturing-modified transport variables, if needed
!!$     call NS_Shock_Capture(region, ng, rk%flags(1), rkStep)

!!$     ! ... add tvCor to tv
!!$     if ( grid%input%tvCor_in_dt == TRUE) then
!!$       do j = 1, size(state%tv,2)
!!$         do i = 1, grid%nCells
!!$           state%tv(i,j) = state%tv(i,j) + state%tvCor(i,j)
!!$         end do
!!$       end do
!!$     end if

      end do

      if (rkStep == 1) then
        do ng = 1, region%nGrids
          state => region%state(ng)
          grid  => region%grid(ng)
!!$       if (.not.associated(state%rhs)     .eqv. .true.) allocate(state%rhs(size(state%cv,1),size(state%cv,2)))
          if (.not.associated(state%rk_rhs)  .eqv. .true.) allocate(state%rk_rhs(size(state%cv,1),size(state%cv,2),3))
          if (.not.associated(state%timeOld) .eqv. .true.) allocate(state%timeOld(size(state%time)))
          if (.not.associated(state%cfl)     .eqv. .true.) allocate(state%cfl(size(state%time)))
          if (.not.associated(state%avOld)   .eqv. .true.) allocate(state%avOld(size(state%av,1),size(state%av,2)))
          if (.not.associated(state%dt)      .eqv. .true.) allocate(state%dt(size(state%cv,1)))
          do i = 1, grid%nCells
            state%timeOld(i) = state%time(i)
          end do
          do k = 1, size(state%av,2)
            do i = 1, grid%nCells
              state%avOld(i,k) = state%av(i,k)
            end do
          end do
        end do
      end if

      if (rkStep == 1) call NS_Timestep(region)

      do ng = 1, region%nGrids

        state => region%state(ng)
        grid => region%grid(ng)

        ! ... time vector
        do i = 1, grid%nCells
          state%time(i) = state%timeOld(i) - rkTimeFac(rkStep) * state%dt(i) ! ... Adjoint N-S: backward in time
        end do

        ! ... if the grid is moving, (re)compute metrics
!!$        if (grid%moving == TRUE) then
!!$          call Read_and_Distribute_Moving_Single_Grid(region, ng)
!!$          call metrics(region, ng)
!!$        end if

        ! ... zero out the rhs vector
!!$     Do j = 1, size(state%rhs,2)
!!$       Do i = 1, grid%nCells
!!$         state%rhs(i,j) = 0.0_rfreal
!!$       End Do
!!$     End Do

        ! ... update the target
        timer = MPI_WTIME()
        call AdjNS_Update_Target(region, ng)
        mpi_timings%rhs_bc = mpi_timings%rhs_bc + (MPI_WTIME() - timer)

        ! ... compute the RHS for each grid
        call AdjNS_RHS(region, ng, rk%flags(1))

      end do

      do ng = 1, region%nGrids

        mpi_timings => region%mpi_timings(ng)

        ! ... add ``Lighthill force''
        ! call Lighthill_Force(region, ng)

        ! ... call the boundary conditions
        timer = MPI_WTIME()
        call AdjNS_BC(region, ng, rk%flags(1))
        mpi_timings%rhs_bc = mpi_timings%rhs_bc + (MPI_WTIME() - timer)

        ! ... add ``Plasma Actuators''
        if (input%use_plasma_actuator == TRUE) call Plasma_Actuator(region, ng)

        ! ... add adjoint source for control optimization solving N-S equations
        if (input%AdjOptim .AND. region%global%USE_RESTART_FILE == TRUE) &
                                                   call AdjNS_Source(region, ng)

      end do

      ! ... call the boundary conditions
      timer = MPI_WTIME()
      call AdjNS_BC_Fix_Value(region)
      region%mpi_timings(:)%rhs_bc = region%mpi_timings(:)%rhs_bc + (MPI_WTIME() - timer)

      do ng = 1, region%nGrids

        state => region%state(ng)
        grid  => region%grid(ng)

        ! ... use iblank to zero out rhs for hole points
        do k = 1, size(state%rhs,2)
          do i = 1, grid%nCells
            ibfac_local = grid%ibfac(i)
            state%rhs(i,k) = state%rhs(i,k) * ibfac_local
          end do
        end do

        ! ... update
        if (rkStep <= 3) then
          do k = 1, size(state%av,2)
            do i = 1, grid%nCells
              state%av(i,k) = state%avOld(i,k) - rkRHSFac(rkStep) * state%rhs(i,k) * state%dt(i) ! ... Adjoint N-S: backward in time
            end do
          end do

          ! ... save this rhs
          do k = 1, size(state%rhs,2)
            do i = 1, grid%nCells
              state%rk_rhs(i,k,rkStep) = state%rhs(i,k)
            end do
          end do
        end if

        ! ... call the boundary conditions
        !call computeDv(region%myrank, grid, state)
        !call NS_BC_Fix_Value(region)

      end do ! ng

      ! ... interpolation here
      If (input%gridType == CHIMERA .AND. input%volumeIntersection == TRUE) Then
        timer = MPI_WTIME()
        if (rkStep <= 3) then
          Call Exchange_Interpolated_State(region)
        end if
        region%mpi_timings(:)%interp = region%mpi_timings(:)%interp + (MPI_WTIME() - timer)
      End if

      ! ... deallocate memory, if needed
      do ng = 1, region%nGrids
        call AdjNS_Deallocate_Memory(region, ng, rk%flags(1), rkStep)
      end do

    end do ! rkStep

    ! ... add-em up
    do ng = 1, region%nGrids

      grid  => region%grid(ng)
      state => region%state(ng)

      do i = 1, grid%nCells
        state%time(i) = state%timeOld(i) - state%dt(i) ! ... Adjoint N-S: backward in time
      end do

      ! ... update state%av
      do k = 1, size(state%av,2)
        do i = 1, grid%nCells
          ibfac_local = grid%ibfac(i)
          state%av(i,k) = state%avOld(i,k) - & ! ... Adjoint N-S: backward in time
               (state%rk_rhs(i,k,1) + 2.0_rfreal * state%rk_rhs(i,k,2) + &
               2.0_rfreal * state%rk_rhs(i,k,3) + state%rhs(i,k)) * state%dt(i) / 6.0_rfreal * ibfac_local
        end do
      end do

      deallocate(state%rhs, state%rk_rhs, state%timeOld, state%avOld)
      nullify(state%rhs, state%rk_rhs, state%timeOld, state%avOld)

    end do

    ! ... call the boundary conditions
    timer = MPI_WTIME()
    call AdjNS_BC_Fix_Value(region)
    region%mpi_timings(:)%rhs_bc = region%mpi_timings(:)%rhs_bc + (MPI_WTIME() - timer)

    ! ... interpolation here
    If (input%gridType == CHIMERA .AND. input%volumeIntersection == TRUE) Then
      timer = MPI_WTIME()
      Call Exchange_Interpolated_State(region)
      region%mpi_timings(:)%interp = region%mpi_timings(:)%interp + (MPI_WTIME() - timer)
    End If

    ! ... filter the conservative variables
    if (input%filter .eqv. .true.) then
      do ng = 1, region%nGrids
        call filter(region,ng,FALSE)
      end do
    end if

    deallocate(rk%flags)
    deallocate(rk)
    deallocate(err)
    !call mpi_barrier(mycomm, ierr)

  end subroutine RK4_AdjNS


  !---------------------------------------------------------------
  !
  ! RK4 :: Runge-Kutta (4th order) for non-linear systems of
  !        differential equations
  !
  !---------------------------------------------------------------
  subroutine RK4_Cyl1D(region)

    USE ModGlobal
    USE ModDataStruct
    USE ModIO
    USE ModDerivBuildOps
    USE ModDeriv
    USE ModMetrics
    USE ModEOS
    USE ModCyl1D
    USE ModNavierStokesRHS
    USE ModMPI
    USE ModActuator

    Implicit None

    type(t_region), pointer :: region

    ! ... local variables
    integer :: rkLevel, rkStep, i, j, k, ng
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    type(t_mixt_input), pointer :: input
    type(t_mpi_timings), pointer :: mpi_timings
    type(t_interp), pointer :: interp
    real(rfreal), parameter :: rkTimeFac(4) = (/ 0.0_rfreal, 0.5_rfreal, 0.5_rfreal, 1.0_rfreal /)
    real(rfreal), parameter :: rkRHSFac(4) = (/ 0.5_rfreal, 0.5_rfreal, 1.0_rfreal, 1.0_rfreal /)
    type(t_rk), pointer :: rk
    integer, allocatable :: err(:)
    real(rfreal) :: timer, timer_total

    timer_total = MPI_WTIME()
    allocate(err(region%nGrids)); err(:) = 0
    
    if(.not. region%rkInit) then
      allocate(region%rk)
      region%rkInit = .true.
    end if
    
    rk => region%rk

    if(region%global%rk_alloc .eqv. .true.) then
      region%global%rk_alloc = .false.
      if (region%myrank == 0) write (*,'(A)') 'PlasComCM: ==> Using RK4_Cyl1D time integration <=='
    endif
    allocate(rk%flags(1)); rk%flags(:) = COMBINED

    ! ... simplicity
    input => region%input

    ! ... step k
    do rkStep = 1, 4

      do ng = 1, region%nGrids

        state => region%state(ng)
        grid  => region%grid(ng)
        mpi_timings => region%mpi_timings(ng)

        ! ... compute the dependent variables
        call computeDv(region%myrank, grid, state)

        ! ... compute the transport variables
        call computeTv(grid, state)

      end do

      if (rkStep == 1) then

        ! ... allocate space for RHS and old values of conserved variables
        do ng = 1, region%nGrids
          state => region%state(ng)
          grid  => region%grid(ng)

          if (.not.associated(state%rhs) .eqv. .true.) &
            allocate(state%rhs(size(state%cv,1),size(state%cv,2)))
          if (.not.associated(state%rk_rhs) .eqv. .true.) &
            allocate(state%rk_rhs(size(state%cv,1),size(state%cv,2),3))
          if (.not.associated(state%timeOld) .eqv. .true.) &
            allocate(state%timeOld(size(state%time)))
          if (.not.associated(state%cfl) .eqv. .true.) &
            allocate(state%cfl(size(state%time)))
          if (.not.associated(state%cvOld) .eqv. .true.) &
            allocate(state%cvOld(size(state%cv,1),size(state%cv,2)))
          if (.not.associated(state%dt) .eqv. .true.) &
            allocate(state%dt(size(state%cv,1)))

          ! ... save time
          do i = 1, size(state%time)
            state%timeOld(i) = state%time(i)
          end do

          ! save solution
          do k = 1, size(state%cv,2)
            do i = 1, size(state%cv,1)
              state%cvOld(i,k) = state%cv(i,k)
            end do
          end do

        end do
      end if

      if (rkStep == 1) call NS_Timestep(region)

      do ng = 1, region%nGrids

        state => region%state(ng)
        grid => region%grid(ng)

        ! ... time vector
        do i = 1, size(state%time)
          state%time(i) = state%timeOld(i) + rkTimeFac(rkStep) * state%dt(i)
          do j = 1, grid%ND+2
            state%rhs(i,j) = 0.0_rfreal
          end do
        end do

        ! ... compute the RHS for each grid
        call Cyl1D_RHS(region, ng, rk%flags(1))

        ! ... add the actuator
        if (input%use_plasma_actuator == TRUE) call Plasma_Actuator(region, ng)

      end do

      do ng = 1, region%nGrids

        mpi_timings => region%mpi_timings(ng)

        ! ... call the boundary conditions
        timer = MPI_WTIME()
        call Cyl1D_BC(region, ng, rk%flags(1))
        mpi_timings%rhs_bc = mpi_timings%rhs_bc + (MPI_WTIME() - timer)

      end do

      do ng = 1, region%nGrids

        state => region%state(ng)
        grid => region%grid(ng)

        ! ... use iblank to zero out rhs for hole and interpolated points
        do k = 1, size(state%rhs,2)
          do i = 1, size(state%rhs,1)
            state%rhs(i,k) = state%rhs(i,k) * dble(min(abs(grid%iblank(i)),1))
          end do
        end do

        ! ... update
        if (rkStep <= 3) then

          ! ... conservative variables
          do k = 1, size(state%cv,2)
            do i = 1, size(state%cv,1)
              state%cv(i,k) = state%cvOld(i,k) &
                   + rkRHSFac(rkStep) * state%rhs(i,k) * state%dt(i)
            end do
          end do

          ! ... save this rhs
          do k = 1, size(state%rhs,2)
            do i = 1, size(state%rhs,1)
              state%rk_rhs(i,k,rkStep) = state%rhs(i,k)
            end do
          end do

        end if

      end do ! ng

    end do ! rkStep

    ! ... add-em up
    do ng = 1, region%nGrids

      grid => region%grid(ng)
      state => region%state(ng)

      do i = 1, size(state%time)
        state%time(i) = state%timeOld(i) + state%dt(i)
      end do

      ! ... conservative variables
      do k = 1, size(state%cv,2)
        do i = 1, size(state%cv,1)
          state%cv(i,k) = state%cvOld(i,k) + &
               (state%rk_rhs(i,k,1) + 2.0_rfreal * state%rk_rhs(i,k,2) + &
               2.0_rfreal * state%rk_rhs(i,k,3) + state%rhs(i,k)) * state%dt(i) / 6.0_rfreal
        end do
      end do

      deallocate(state%rk_rhs, state%timeOld, state%cvOld)

    end do

    ! ... filter the conservative variables
    if (input%filter .eqv. .true.) then
      do ng = 1, region%nGrids
        call filter(region,ng,FALSE)
      end do
    end if

    deallocate(rk%flags)
    deallocate(rk)
    !call mpi_barrier(mycomm, ierr)

  end subroutine RK4_Cyl1D

  !---------------------------------------------------------------
  !
  ! ARK2 :: Additive Runge-Kutta for non-linear systems of
  !         differential equations
  !
  ! dU
  ! -- = F^[explicit](U,t) + F^[implicit](U,t)
  ! dt 
  !
  !---------------------------------------------------------------
  subroutine ARK2(region, implicit_flag, MaxSurfRHS)

    USE ModGlobal
    USE ModDataStruct
    USE ModIO
    USE ModDerivBuildOps
    USE ModDeriv
    USE ModMetrics
    USE ModEOS
    USE ModNavierStokesBC
    USE ModNavierStokesRHS
    USE ModMPI
    USE ModInterp
    USE ModSBI
    USE ModActuator
    USE ModCombustion
#ifdef HAVE_CANTERA
    USE ModLaserIgnition
#endif
    USE ModNASARotor

    Implicit None

    ! ... Incoming variables
    type(t_region), pointer :: region
    Integer, optional :: implicit_flag
    real(rfreal), optional :: MaxSurfRHS

    ! ... local variables
    integer :: rkStep, i, j, k, ng, ARK2_nStages, ImplicitFlag
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
#ifdef BUILD_ELECTRIC
    type(t_electric), pointer :: electric
#endif
    type(t_mixt_input), pointer :: input
    type(t_mpi_timings), pointer :: mpi_timings
    type(t_interp), pointer :: interp
    integer, allocatable :: err(:)
    real(rfreal) :: timer, timer_total, ibfac_local, source_dim_factor, max_source_term_local, max_source_term, max_temp_local, max_temp, max_rho
    real(rfreal), allocatable :: Temp(:), Density(:), MassFrac(:,:), WOm(:,:), WOm_max(:), MassFrac_max(:)
    real(rfreal), pointer :: a_mat_exp(:,:), a_mat_imp(:,:), b_vec_imp(:), b_vec_exp(:), c_vec(:)
    integer :: newTime, ierr
    real(rfreal) :: cvi1inv

    ! ... declarations for pointer dereference, WZhang 05/2014
    integer :: nGrids, Nc, nCv, nAuxvars
    integer :: moveGrid, gridType, motionType, tvCor_in_dt, SelectiveFrequencyDamping
    integer :: boundaryIntersection, useNativeSBI, use_plasma_actuator, acoustic_source, volumeIntersection, filt_freq
    integer, pointer :: iblank(:)
    character(len=80) :: caseID
    real(rfreal), pointer :: rhs(:,:), rhs_auxVars(:,:), tv(:,:), tvCor(:,:), timeOld(:), time(:), cvOld(:,:), cv(:,:)
    real(rfreal), pointer :: cvTarget(:,:), cvTargetOld(:,:), auxVarsOld(:,:), auxVars(:,:), dt(:), dv(:,:)
    real(rfreal) :: gamref, tempref, densref, lengref, sndspdref, SFD_D, SFD_X
    real(rfreal), pointer :: rhs_explicit(:,:,:), rhs_auxVars_explicit(:,:,:), rhs_implicit(:,:,:), rhs_auxVars_implicit(:,:,:), rhs3(:,:,:), rhs_auxVars3(:,:,:)
    logical :: AdjOptim, input_filter
  

    ! ... pointer dereference, WZhang 05/2014
    nGrids     = region%nGrids
    moveGrid   = region%input%moveGrid
    gridType   = region%input%gridType
    motiontype = region%input%motionType
    caseID     = region%input%caseID
    boundaryIntersection      = region%input%boundaryIntersection
    useNativeSBI              = region%input%useNativeSBI
    volumeIntersection        = region%input%volumeIntersection
    input_filter              = region%input%filter
    filt_freq                 = region%input%filt_freq

    timer_total = MPI_WTIME()
    allocate(err(nGrids)); err(:) = 0
    ImplicitFlag = FALSE

    if (region%global%rk_alloc .eqv. .true.) then
      do ng = 1, region%nGrids
        grid  => region%grid(ng)
        input => grid%input 
        call additive_RK_coeff(region%myrank, input, grid, ImplicitFlag)
      end do
      region%global%rk_alloc = .false.
      if (region%myrank == 0) &
        write (*,'(A)') 'PlasComCM: ==> Using ARK2 time integration <=='
    end if

    do ng = 1, nGrids
      grid => region%grid(ng)
      if (ng == 1) then
        ARK2_nStages = grid%ARK2_nStages
        a_mat_exp => grid%ARK2_a_mat_exp
        a_mat_imp => grid%ARK2_a_mat_imp
        b_vec_exp => grid%ARK2_b_vec_exp
        b_vec_imp => grid%ARK2_b_vec_imp
        c_vec     => grid%ARK2_c_vec
      end if
      if (ARK2_nStages /= grid%ARK2_nStages) err(ng) = 1
    end do 
    if (sum(err(:)) /= 0) &
      Call graceful_exit(region%myrank, &
      'PlasComCM: ERROR: ARK2 time integration must be the same on all grids.')

    ! ... check for memory allocations
    do ng = 1, nGrids
      state => region%state(ng)
      grid  => region%grid(ng)
      input => grid%input

      ! ... pointer dereference, WZhang 05/2014
      Nc            = grid%nCells
      nCv           = input%nCv
      nAuxvars      = input%nAuxVars

      if (.not.associated(state%rhs) .eqv. .true.) then
        allocate(state%rhs(Nc, nCv))
      end if
      if (.not.associated(state%rhs_explicit) .eqv. .true.) then
        allocate(state%rhs_explicit(Nc, nCv, ARK2_nStages))
      end if
      if (ImplicitFlag == TRUE) then
        if (.not.associated(state%rhs_implicit) .eqv. .true.) allocate(state%rhs_implicit(Nc, nCv, ARK2_nStages))
      end if
      if (nAuxVars > 0) then
        if (.not.associated(state%rhs_auxVars) .eqv. .true.) allocate(state%rhs_auxVars(Nc, nAuxVars))
        if (.not.associated(state%rhs_auxVars_explicit) .eqv. .true.) allocate(state%rhs_auxVars_explicit(Nc, nAuxVars, ARK2_nStages))
        if (ImplicitFlag == TRUE) then
          if (.not.associated(state%rhs_auxVars_implicit) .eqv. .true.) allocate(state%rhs_auxVars_implicit(Nc, nAuxVars, ARK2_nStages))
        end if
      end if
      if (.not.associated(state%timeOld) .eqv. .true.) allocate(state%timeOld(Nc))
      if (.not.associated(state%cfl)     .eqv. .true.) allocate(state%cfl(Nc))
      if (.not.associated(state%cvOld)   .eqv. .true.) allocate(state%cvOld(Nc,nCv))
      if (.not.associated(state%dt)      .eqv. .true.) allocate(state%dt(Nc))
      if (nAuxVars > 0) then
        if (.not.associated(state%auxVarsOld) .eqv. .true.) allocate(state%auxVarsOld(Nc,nAuxVars))
      end if
    end do

    ! ... step k
    do rkStep = 1, ARK2_nStages

      ! ... update the ghost cell data for cv and auxvars
      do ng = 1, nGrids
        grid  => region%grid(ng)
        input => grid%input
        nAuxvars = input%nAuxVars
        ! ... exchange cv
        call Ghost_Cell_Update_Box(region, ng, COMBINED, 'cv ')
        ! ... exchange auxvars, if needed
        if (nAuxvars > 0) then
          call Ghost_Cell_Update_Box(region, ng, COMBINED, 'aux')
        end if
#ifdef HAVE_CANTERA
        region%state(ng)%combustion%iRKstage = rkStep
#endif
     end do

      ! ... determine if the current time is different than the previous time
      newTime = FALSE
      if (rkStep > 1) then
        if (dabs(c_vec(rkStep) - c_vec(rkStep-1)) > TINY) newTime = TRUE
      end if

      ! ... updates needed for moving grids (only when time changes)
      if (moveGrid == TRUE .and. newTime == TRUE) then

        ! ... move only the rotor grid
        If (caseID == 'NASA_ROTOR_STATOR') call Move_Rotor_Grid_NASA(region)

        ! ... move only the cylinder grid
        !call Move_Cylinder_Grid(region)

        ! ... Chimera-specific updates
        if (gridType == CHIMERA .and. c_vec(rkStep) > 0.0_rfreal .and. motionType == DEFORMING) then

          ! ... set up the new grid interp
          call Setup_Interpolation(region)

          ! ... update operators since they depend on IBLANK
          Call create_global_mask(region)

          do ng = 1, nGrids
            grid  => region%grid(ng)
            input => grid%input
            Call Operator_Setup(region, input, grid, err(ng), ng)
            Call HYPRE_Pade_Operator_Setup(region, ng)
          end do
          if (sum(err(:)) /= 0) Call Output_Rank_As_IBLANK(region)

        end if

        ! ... update metrics
        do ng = 1, nGrids
          call metrics(region, ng)
        end do

      end if

      ! ... zero out the rhs vector
      do ng = 1, nGrids
        state => region%state(ng)
        grid  => region%grid(ng)
        input => grid%input

        ! ... pointer dereference, WZhang 05/2014
        Nc           = grid%nCells
        nCv          = input%nCv
        nAuxvars     = input%nAuxVars
        rhs         => state%rhs
        rhs_auxVars => state%rhs_auxVars
        rhs_implicit => state%rhs_implicit
        rhs_explicit => state%rhs_explicit
        rhs_auxVars_implicit => state%rhs_auxVars_implicit
        rhs_auxVars_explicit => state%rhs_auxVars_explicit

        Do j = 1, nCv
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
          Do i = 1, Nc
            rhs(i,j) = 0.0_rfreal
            rhs_explicit(i,j,rkStep) = 0_8
          End Do
        End Do
        Do j = 1, nAuxVars
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
          Do i = 1, Nc
            rhs_auxVars(i,j) = 0.0_rfreal
            rhs_auxVars_explicit(i,j,rkStep) = 0_8
          End Do
        End Do
        
        if (ImplicitFlag == TRUE) then
          Do j = 1, nCv
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
            Do i = 1, Nc
              rhs_implicit(i,j,rkStep) = 0_8
            End Do
          End Do
          Do j = 1, nAuxVars
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
            Do i = 1, Nc
              rhs_auxVars_implicit(i,j,rkStep) = 0_8
            End Do
          End Do
        end if
      end do

      do ng = 1, nGrids

        state => region%state(ng)
        grid  => region%grid(ng)
        input => grid%input

        ! ... pointer dereference, WZhang 05/2014
        tvCor_in_dt  = input%tvCor_in_dt
        Nc           = grid%nCells
        nCv          = input%nCv
        nAuxvars     = input%nAuxVars
        tv          => state%tv

        ! ... compute the dependent variables
        call computeDv(region%myrank, grid, state)

        ! ... allocate memory, if needed
        call NS_Allocate_Memory(region, ng, COMBINED, rkStep)

        ! ... compute velocity gradient tensor, if needed
        call NS_VelGradTensor(region, ng, COMBINED)

        ! ... compute the LES-modified transport variables, if needed
        ! ... this modifies the pressure (state%dv(1)) and temperature (state%dv(2))
        call NS_LES_Model(region, ng, COMBINED, rkStep)

        ! ... compute the transport variables
        call computeTv(grid, state)

        ! ... compute temperature gradient, if needed
        call NS_Temperature_Gradient(region, ng, COMBINED, rkStep)

        ! ... update the ghost cell data for velocity gradient and temperature gradient
        call Ghost_Cell_Update_Box(region, ng, COMBINED, 'grd')

        ! ... compute the shock-capturing-modified transport variables, if needed
        call NS_Shock_Capture(region, ng, COMBINED, rkStep)

        ! ... add tvCor to tv
        if ( tvCor_in_dt == TRUE) then

          tvCor => state%tvCor

          do j = 1, size(tv,2)
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
            do i = 1, Nc
              tv(i,j) = tv(i,j) + tvCor(i,j)
            end do
          end do

        end if

      end do

      if (rkStep == 1) then
        do ng = 1, nGrids
          state => region%state(ng)
          grid  => region%grid(ng)
          input => grid%input

          ! ... pointer dereference, WZhang 05/2014
          Nc           = grid%nCells
          nCv          = input%nCv
          nAuxvars     = input%nAuxVars
          timeOld     => state%timeOld
          time        => state%time
          cv          => state%cv
          cvOld       => state%cvOld
          cvTargetOld => state%cvTargetOld
          cvTarget    => state%cvTarget
          auxVarsOld  => state%auxVarsOld
          auxVars     => state%auxVars
          SelectiveFrequencyDamping = input%SelectiveFrequencyDamping

#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
          do i = 1, Nc
            timeOld(i) = time(i)
          end do
          do k = 1, nCv
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
            do i = 1, Nc
              cvOld(i,k) = cv(i,k)
            end do
          end do
          if (SelectiveFrequencyDamping == TRUE) then
            do k = 1, nCv
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
              do i = 1, Nc
                cvTargetOld(i,k) = cvTarget(i,k)
              end do
            end do
          end if
          do k = 1, nAuxVars
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
            do i = 1, Nc
              auxVarsOld(i,k) = auxVars(i,k)
            end do
          end do
        end do
      end if

      if (rkStep == 1) then 

        call NS_Timestep(region)
        call SAMPLE_CHECK(region)

      endif

      ! ... interpolate boundary data (cv and fluxes), if any
      If (boundaryIntersection == TRUE) Then
        timer = MPI_WTIME()
        CALL Native_Block_Interface_Exchange(region)
        region%mpi_timings(:)%interp = region%mpi_timings(:)%interp + (MPI_WTIME() - timer)
      End If

      do ng = 1, nGrids

        state => region%state(ng)
        grid  => region%grid(ng)
        input => grid%input

        ! ... pointer dereference, WZhang 05/2014
        Nc           = grid%nCells
        nCv          = input%nCv
        nAuxvars     = input%nAuxVars
        timeOld     => state%timeOld
        time        => state%time
        dt          => state%dt
        dv          => state%dv
        gamref       = input%gamref
        tempref      = input%tempref
        densref      = input%densref
        sndspdref    = input%sndspdref
        lengref      = input%lengref
        cv          => state%cv
        auxVars     => state%auxVars
        iblank      => grid%iblank
        
        if(ImplicitFlag == TRUE)then
          rhs3 => state%rhs_implicit
          rhs_auxVars3 => state%rhs_auxVars_implicit
        else
          rhs3 => state%rhs_explicit
          rhs_auxVars3 => state%rhs_auxVars_explicit
        endif

        ! ... time vector
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
        do i = 1, Nc
          time(i) = timeOld(i) + c_vec(rkStep) * dt(i)
        end do

        ! ... update the target
        timer = MPI_WTIME()
        call NS_Update_Target(region, ng)
        region%mpi_timings(ng)%rhs_bc = region%mpi_timings(ng)%rhs_bc + (MPI_WTIME() - timer)

        ! ... compute the RHS for each grid
        call NS_RHS(region, ng, COMBINED)

        ! ... add the chemical source terms
        call CombustionSource(region, ng)

      end do  !ng = 1, region%nGrids

      do ng = 1, nGrids

        state => region%state(ng)
        grid  => region%grid(ng)
        input => grid%input
        mpi_timings => region%mpi_timings(ng)

        ! ... pointer dereference, WZhang 05/2014
        use_plasma_actuator = input%use_plasma_actuator    
        acoustic_source     = input%acoustic_source    
        AdjOptim            = input%AdjOptim

        ! ... call the boundary conditions
        timer = MPI_WTIME()
#ifdef USE_OPT_BC
        Call NS_BC_Optimized(region, ng, COMBINED)
#else
        Call NS_BC(region, ng, COMBINED)
#endif
        mpi_timings%rhs_bc = mpi_timings%rhs_bc + (MPI_WTIME() - timer)

        ! ... add ``Plasma Actuators''
        if (use_plasma_actuator == TRUE) call Plasma_Actuator(region, ng)

        ! ... add acoustic source
        if (acoustic_source == TRUE) call AcousticSource(region, ng)

        ! ... add control for control optimization solving N-S equations
        if (AdjOptim .AND. region%global%USE_RESTART_FILE == TRUE) call OperateActuator4Optimization(region, ng)
        if (input%use_momentum_actuator == TRUE)call Plasma_MomentumTransfer_Actuator(region, ng)
        if (input%use_radical_actuator == TRUE) call Plasma_Radical_Actuator(region, ng)
#ifdef HAVE_CANTERA
        ! Initialize laser ignition source term for jet in crossflow
        if (input%caseID == 'JETXFLOW') then
          if (input%useLaser == TRUE) call LaserSource(region, ng)
        end if
#endif
      end do

      ! ... output the rhs if desired
      If (rkStep == 1 .and. region%input%write_rhs == TRUE .and. &
          mod(region%global%main_ts_loop_index-1,region%input%noutput) == 0) &
        Call WriteData(region, 'rhs')

      ! ... output the maximum residual if desired
      If (rkStep == 1 .AND. region%input%RESIDUAL_INF_NORM_CALCULATE == TRUE) &
        Call Calculate_Inf_Norm(region)

      ! ... check if we have any preconditioners in use
      If (region%input%USE_LOCAL_LOW_MACH_PRECONDITIONER == TRUE) &
        Call NS_Local_Low_Mach_Preconditioner(region)

      do ng = 1, nGrids
        state => region%state(ng)
        grid  => region%grid(ng)
        input => grid%input
        SelectiveFrequencyDamping = input%SelectiveFrequencyDamping

        if (SelectiveFrequencyDamping == TRUE) then
          ! ... pointer dereference, WZhang 05/2014
          Nc           = grid%nCells
          nCv          = input%nCv
          nAuxvars     = input%nAuxVars
          cvTarget    => state%cvTarget
          cvTargetOld => state%cvTargetOld
          cvOld       => state%cvOld
          SFD_D        = input%SFD_D
          dt          => state%dt
          SFD_X        = input%SFD_X
          rhs         => state%rhs
          cv          => state%cv

          ! ... update the target state
          if (newTime == TRUE) then
            do k = 1, nCv
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
              do i = 1, Nc
                cvTarget(i,k) = cvTargetOld(i,k) + (-cvTargetOld(i,k) + cvOld(i,k)) / SFD_D * c_vec(rkStep) * dt(i)
              end do
            end do
          end if

          ! ... update the flow field
          do k = 1, nCv
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
            do i = 1, Nc
              rhs(i,k) = rhs(i,k) - SFD_X * (cv(i,k) - cvTarget(i,k))
            end do
          end do

        end if
      end do

      do ng = 1, nGrids

        state => region%state(ng)
        grid  => region%grid(ng)
        input => grid%input

        ! ... pointer dereference, WZhang 05/2014
        Nc            = grid%nCells
        nCv           = input%nCv
        nAuxvars      = input%nAuxVars
        iblank       => grid%iblank
        rhs_explicit => state%rhs_explicit
        rhs          => state%rhs
        rhs_auxVars_explicit => state%rhs_auxVars_explicit
        rhs_auxVars          => state%rhs_auxVars
        cv           => state%cv
        cvOld        => state%cvOld
        auxVars      => state%auxVars
        auxVarsOld   => state%auxVarsOld
        dt           => state%dt
        rhs_implicit => state%rhs_implicit
        rhs_auxVars_implicit => state%rhs_auxVars_implicit

        ! ... use iblank to zero out rhs for hole points
        do k = 1, nCv
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
          do i = 1, Nc
            ibfac_local = grid%ibfac(i)
            rhs_explicit(i,k,rkStep) = rhs_explicit(i,k,rkStep) + rhs(i,k) * ibfac_local
          end do
        end do
        do k = 1, nAuxVars
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
          do i = 1, Nc
            ibfac_local = grid%ibfac(i)
            rhs_auxVars_explicit(i,k,rkStep) = rhs_auxVars_explicit(i,k,rkStep) + rhs_auxVars(i,k) * ibfac_local
          end do
        end do

        ! ... save base values
        do k = 1, nCv
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
          do i = 1, Nc
            cv(i,k) = cvOld(i,k)
          end do
        end do
        do k = 1, nAuxVars
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
          do i = 1, Nc
            auxVars(i,k) = auxVarsOld(i,k)
          end do
        end do

        ! ... update the predicted value \tilde{U^{(i)}}
        if (rkStep <= ARK2_nStages - 1) then
          do j = 1, rkStep
            if (dabs(a_mat_exp(rkStep+1,j)) > 10.0_rfreal * TINY) then
              do k = 1, nCv
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
                do i = 1, Nc
                  cv(i,k) = cvOld(i,k) + dt(i) * a_mat_exp(rkStep+1,j) * rhs_explicit(i,k,j)
                end do
              end do
              do k = 1, nAuxVars
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
                do i = 1, Nc
                  auxVars(i,k) = auxVarsOld(i,k) + dt(i) * a_mat_exp(rkStep+1,j) * rhs_auxVars_explicit(i,k,j)
                end do
              end do
            end if
            if (dabs(a_mat_imp(rkStep+1,j)) > 10.0_rfreal * TINY) then
              do k = 1, nCv
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
                do i = 1, Nc
                  cv(i,k) = cv(i,k) + dt(i) * a_mat_imp(rkStep+1,j) * rhs_implicit(i,k,j)
                end do
              end do
              do k = 1, nAuxVars
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
                do i = 1, Nc
                  auxVars(i,k) = auxVars(i,k) + dt(i) * a_mat_imp(rkStep+1,j) * rhs_auxVars_implicit(i,k,j)
                  if (auxVars(i,k) <= 0.0_8) auxVars(i,k) = cv(i,1) * 1d-10
                end do
              end do
            end if
          end do !j = 1, rkStep

          ! ... solve the implicit system, if necessary
          if (ImplicitFlag == TRUE) then
            if (dabs(a_mat_imp(rkStep,rkStep)) > 10.0_rfreal * TINY) then
            end if
          end if
        end if

      end do ! ng

      ! ... call the boundary conditions
      timer = MPI_WTIME()
      call NS_BC_Fix_Value(region)
      region%mpi_timings(:)%rhs_bc_fix_value = region%mpi_timings(:)%rhs_bc_fix_value + (MPI_WTIME() - timer)

      ! ... interpolation here
      If (gridType == CHIMERA .AND. volumeIntersection == TRUE) then
        Call Exchange_Interpolated_State(region)
      end if

      ! ... deallocate memory, if needed
      do ng = 1, nGrids
        call NS_Deallocate_Memory(region, ng, COMBINED, rkStep)
      end do

    end do ! rkStep

    ! ... add-em up
    do ng = 1, nGrids

      grid  => region%grid(ng)
      state => region%state(ng)
      input => grid%input

      ! ... pointer dereference, WZhang 05/2014
      Nc            = grid%nCells
      nCv           = input%nCv
      nAuxvars      = input%nAuxVars
      time         => state%time
      timeOld      => state%timeOld
      dt           => state%dt
      cv           => state%cv
      cvOld        => state%cvOld
      auxVars      => state%auxVars
      auxVarsOld   => state%auxVarsOld
      cvTarget     => state%cvTarget
      cvTargetOld  => state%cvTargetOld
      SFD_D         = input%SFD_D
      rhs_explicit => state%rhs_explicit
      rhs_AuxVars_explicit => state%rhs_AuxVars_explicit
      rhs_implicit => state%rhs_implicit
      rhs_AuxVars_implicit => state%rhs_AuxVars_implicit
      SelectiveFrequencyDamping = input%SelectiveFrequencyDamping

#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
      do i = 1, Nc
        time(i) = timeOld(i) + dt(i)
      end do

      ! ... save base values
      do k = 1, nCv
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
        do i = 1, Nc
          cv(i,k) = cvOld(i,k)
        end do
      end do
      do k = 1, nAuxVars
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
        do i = 1, Nc
          auxVars(i,k) = auxVarsOld(i,k)
        end do
      end do

      ! ... selective frequency damping
      if (SelectiveFrequencyDamping == TRUE) then
        do k = 1, nCv
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
          do i = 1, Nc
            cvTarget(i,k) = cvTargetOld(i,k) + (-cvTargetOld(i,k) + cvOld(i,k)) * dt(i) / SFD_D
          end do
        end do
      end if

      ! ... update state%cv with the explict RHS only
      do j = 1, ARK2_nStages
        do k = 1, nCv
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
          do i = 1, Nc
            cv(i,k) = cv(i,k) + dt(i) * b_vec_exp(j) * rhs_explicit(i,k,j)
          end do
        end do

        ! ... update state%auxVars with the explict RHS only
        do k = 1, nAuxVars
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
          do i = 1, Nc
            auxVars(i,k) = auxVars(i,k) + dt(i) * b_vec_exp(j) * rhs_AuxVars_explicit(i,k,j)
          end do
        end do
      end do

      ! ... add implicit parts
      if (ImplicitFlag == TRUE) then
        ! ... update state%cv with the explict RHS only
        do j = 1, ARK2_nStages
          do k = 1, nCv
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
            do i = 1, Nc
              cv(i,k) = cv(i,k) + dt(i) * b_vec_imp(j) * rhs_implicit(i,k,j)
            end do
          end do

          ! ... update state%auxVars with the explict RHS only
          do k = 1, nAuxVars
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
            do i = 1, Nc
              auxVars(i,k) = auxVars(i,k) + dt(i) * b_vec_imp(j) * rhs_AuxVars_implicit(i,k,j)
            end do
          end do
        end do
      end if

    end do !ng = 1, ngrids

    ! ... call the boundary conditions
    timer = MPI_WTIME()
    call NS_BC_Fix_Value(region)
    region%mpi_timings(:)%rhs_bc = region%mpi_timings(:)%rhs_bc + (MPI_WTIME() - timer)

    ! ... interpolation here
    If (gridType == CHIMERA .AND. volumeIntersection == TRUE) Then
      Call Exchange_Interpolated_State(region)
    End If

    ! ... filter the conservative variables
    do ng = 1, nGrids
      if (input_filter .eqv. .true.) then
        if ( mod(region%global%main_ts_loop_index, filt_freq) == 0 ) then
          call filter(region,ng,FALSE)
        endif
      endif
      state => region%state(ng)
      grid  => region%grid(ng)
      input => grid%input
      cv           => state%cv
      dv           => state%dv
      auxVars      => state%auxVars
      select case(grid%input%chemistry_model)
      case (MODEL0)
         call computeDv(region%myRank, grid, state)
         call model0FixSpecies(state%combustion%model0, auxVars, cv, dv)
      case (MODEL1)
         call computeDv(region%myRank, grid, state)
         call model1FixSpecies(state%combustion%model1, auxVars, cv, dv,input, state%auxVarsSteady)
      end select

    end do
    
    deallocate(err)

  end subroutine ARK2

  !---------------------------------------------------------------
  !
  ! SAMPLE_CHECK :: Subroutine for ensuring that the sampling time
  !                 is enforced
  !
  !---------------------------------------------------------------

  subroutine SAMPLE_CHECK(region)
  
    USE ModGlobal
    USE ModDataStruct

    Implicit None

    ! ... global variables
    type(t_region), pointer :: region

    ! ... local variables
    integer :: ng, i
    type(t_mixt_input), pointer :: input
    type(t_mixt), pointer :: state
    type(t_grid), pointer :: grid

    if (region%input%output_mode == SAMPLE_TIME) then
      
      if(region%global%main_ts_loop_index==1) then 
        region%state(1)%t_output_next = region%input%t_output
        region%state(1)%tcnt = 1
      endif

      if ((region%state(1)%time(1)+region%state(1)%dt(1)) < region%state(1)%t_output_next) then
        region%state(1)%dt(1) = region%state(1)%dt(1)
        region%state(1)%tcnt = 1
      elseif ((region%state(1)%time(1)+region%state(1)%dt(1)) == region%state(1)%t_output_next) then
        region%state(1)%dt(1) = region%state(1)%dt(1)
        region%state(1)%t_output_next = region%state(1)%t_output_next + region%input%t_output
        region%state(1)%tcnt = 0
      else
        do i = 1, region%grid(1)%nCells
          region%state(1)%dt(i) = region%state(1)%t_output_next - region%state(1)%time(i)
        end do
        region%state(1)%t_output_next = region%state(1)%t_output_next + region%input%t_output
        region%state(1)%tcnt = 0
      end if

    end if

  end subroutine SAMPLE_CHECK

  subroutine APPROXIMATE_LINEAR_CNS_OPERATOR(region)

    USE ModGlobal
    USE ModDataStruct
    USE ModIO
    USE ModDerivBuildOps
    USE ModDeriv
    USE ModMetrics
    USE ModEOS
    USE ModNavierStokesBC
    USE ModNavierStokesRHS
    USE ModMPI
    USE ModInterp
    USE ModActuator
    USE ModFVSetup
    USE ModPSAAP2

    Implicit None

    type(t_region), pointer :: region

    ! ... local variables
    integer :: i, j, k, ng, i0, j0, k0, l0, var, Nx, Ny, ii, jj, kk, ll, col
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    type(t_mixt_input), pointer :: input
    real(rfreal) :: ibfac_local
    character(len=80) :: cmd

    if (region%global%rk_alloc .eqv. .true.) then
      region%global%rk_alloc = .false.
      if (region%myrank == 0) write (*,'(A)') 'PlasComCM: ==> Approximating linearized RHS operator <=='
      if (region%input%nAuxVars /= 0) call graceful_exit(region%myrank, 'PlasComCM: ERROR: No auxVars allowed in APPROXIMATE_LINEAR_CNS_OPERATOR.')
      if (region%input%gridType == CHIMERA) call graceful_exit(region%myrank, 'PlasComCM: ERROR: No CHIMERA problems in APPROXIMATE_LINEAR_CNS_OPERATOR.')
      if (region%input%moveGrid == TRUE) call graceful_exit(region%myrank, 'PlasComCM: ERROR: No moving grid problems in APPROXIMATE_LINEAR_CNS_OPERATOR.')
    end if
    
    ! ... check for memory allocations
    do ng = 1, region%nGrids
      state => region%state(ng)
      grid  => region%grid(ng)
      input => grid%input
      if (.not.associated(state%rhs) .eqv. .true.) then
        allocate(state%rhs(grid%nCells, input%nCv))
      end if
      if (.not.associated(state%cvOld) .eqv. .true.) then
        allocate(state%cvOld(grid%nCells, input%nCv))
      end if
      if (.not.associated(state%rhs_explicit) .eqv. .true.) then
        allocate(state%rhs_explicit(grid%nCells, input%nCv, 1))
      end if
    end do

    ! ... compute the base state rhs
    do ng = 1, region%nGrids
      state => region%state(ng)
      grid  => region%grid(ng)
      input => grid%input

      ! ... zero out the rhs vector
      Do j = 1, input%nCv
        Do i = 1, grid%nCells
          state%rhs(i,j) = 0.0_rfreal
        End Do
      End Do

      ! ... compute the dependent variables
      call computeDv(region%myrank, grid, state)

      ! ... allocate memory, if needed
      call NS_Allocate_Memory(region, ng, COMBINED, 1)

      ! ... compute velocity gradient tensor, if needed
      call NS_VelGradTensor(region, ng, COMBINED)

      ! ... compute the LES-modified transport variables, if needed
      ! ... this modifies the pressure (state%dv(1)) and temperature (state%dv(2))
      call NS_LES_Model(region, ng, COMBINED, 1)

      ! ... compute the transport variables
      call computeTv(grid, state)

      ! ... compute temperature gradient, if needed
      call NS_Temperature_Gradient(region, ng, COMBINED, 1)

      ! ... compute the shock-capturing-modified transport variables, if needed
      call NS_Shock_Capture(region, ng, COMBINED, 1)

      ! ... add tvCor to tv
      if ( input%tvCor_in_dt == TRUE) then
        do j = 1, size(state%tv,2)
          do i = 1, grid%nCells
            state%tv(i,j) = state%tv(i,j) + state%tvCor(i,j)
          end do
        end do
      end if

      ! ... compute the RHS for each grid
      call NS_RHS(region, ng, COMBINED)
      call NS_BC(region, ng, COMBINED)

      if (region%input%filter .eqv. .true.) then
        call filter(region,ng,FALSE)
      end if

      ! ... use iblank to zero out rhs for hole points
      do k = 1, input%nCv
        do i = 1, grid%nCells
          ibfac_local = grid%ibfac(i)
          state%rhs_explicit(i,k,1) = state%rhs(i,k) * ibfac_local
          state%cvOld(i,k) = state%cv(i,k)
        end do
      end do
    end do

    ! ... now compute the column for only one unknown
    !if (region%myrank == 0) then
    !  write (*,'(A)',ADVANCE='NO') 'PlasComCM: APPROXIMATE_LINEAR_OPERATOR: Input (i,j,k) global indices: '
    !  read (*,*) i0, j0, k0
    !  write (*,'(A)',ADVANCE='NO') 'PlasComCM: APPROXIMATE_LINEAR_OPERATOR: Input variable: '
    !  read (*,*) var
    !  write (*,'(A,E20.12)') 'PlasComCM: Using an epsilon of ', region%input%approximate_linear_operator_epsilon
    !end if
    !call mpi_barrier(mycomm, ierr)

    ! ... perturb the state
    ! ... compute the rhs with perturbed state
    ! ... subtract base rhs
    ! ... output
    ! ... compute the base state rhs
    do ng = 1, region%nGrids
      state => region%state(ng)
      grid  => region%grid(ng)
      input => grid%input

      do var = 1, input%ND+2
        do i0 = 1, grid%GlobalSize(1)
          do j0 = 1, grid%GlobalSize(2)
            do k0 = 1, grid%GlobalSize(3)

              ! ... zero out the rhs vector
              Do j = 1, input%nCv
                Do i = 1, grid%nCells
                  state%rhs(i,j) = 0.0_rfreal
                  state%cv(i,j) = state%cvOld(i,j)
                End Do
              End Do

              ! ... perturb
              Nx = grid%ie(1) - grid%is(1) + 1
              Ny = grid%ie(2) - grid%is(2) + 1
              if ( (i0 >= grid%is(1) .and. i0 <= grid%ie(1)) .and. &
                   (j0 >= grid%is(2) .and. j0 <= grid%ie(2)) .and. &
                   (k0 >= grid%is(3) .and. k0 <= grid%ie(3)) ) then
                l0 = (k0-grid%is(3))*Nx*Ny + (j0-grid%is(2))*Nx + i0-grid%is(1)+1;
                state%cv(l0,var) = state%cv(l0,var) + input%approximate_linear_operator_epsilon
              end if

              ! col = ((k0-1)*grid%GlobalSize(1)*grid%GlobalSize(2)+(j0-1)*grid%GlobalSize(1)+(i0-1))*(input%ND+2) + var
              ! if (col == 237) call write_plot3d_file(region, 'cv ')

              ! ... compute the dependent variables
              call computeDv(region%myrank, grid, state)
  
              ! ... allocate memory, if needed
              call NS_Allocate_Memory(region, ng, COMBINED, 1)
  
              ! ... compute the LES-modified transport variables, if needed
              ! ... this modifies the pressure (state%dv(1)) and temperature (state%dv(2))
              call NS_LES_Model(region, ng, COMBINED, 1)
  
              ! ... compute the transport variables
              call computeTv(grid, state)

              ! ... compute temperature gradient, if needed
              call NS_Temperature_Gradient(region, ng, COMBINED, 1)

              ! ... compute the shock-capturing-modified transport variables, if needed
              call NS_Shock_Capture(region, ng, COMBINED, 1)

              ! ... add tvCor to tv
              if ( input%tvCor_in_dt == TRUE) then
                do j = 1, size(state%tv,2)
                  do i = 1, grid%nCells
                    state%tv(i,j) = state%tv(i,j) + state%tvCor(i,j)
                  end do
                end do
              end if

              ! ... compute the RHS for each grid
              call NS_RHS(region, ng, COMBINED)
              call NS_BC(region, ng, COMBINED)

              if (region%input%filter .eqv. .true.) then
                call filter(region,ng,FALSE)
              end if

              ! ... update CV to be difference in the RHSs
              do k = 1, input%nCv
                do i = 1, grid%nCells
                  ibfac_local = grid%ibfac(i)
                  state%cv(i,k) = (state%rhs(i,k) * ibfac_local - state%rhs_explicit(i,k,1)) / input%approximate_linear_operator_epsilon
                end do
              end do

              write (cmd,'(4(A,I3.3),A)') 'PlasComCM_',i0,'_',j0,'_',k0,'_',var,'.dat'
              call write_approximate_matrix(region, cmd, i0, j0, k0, var)

              if (region%myrank == 0) write (*,'(4(A,I3),A)') 'PlasComCM: Done with (i0,j0,k0,var) = (',i0,',',j0,',',k0,',',var,')'

            end do
          end do
        end do
      end do
      call NS_BC_Fix_Value(region)
    end do

    ! ... write
    call graceful_exit(region%myrank, 'PlasComCM: APPROXIMATE_LINEAR_CNS_OPERATOR completed.')
   
  end subroutine APPROXIMATE_LINEAR_CNS_OPERATOR


  subroutine APPROXIMATE_LINEAR_CNS_OPERATOR_POWER_METHOD(region)

    USE ModGlobal
    USE ModDataStruct
    USE ModIO
    USE ModDerivBuildOps
    USE ModDeriv
    USE ModMetrics
    USE ModEOS
    USE ModNavierStokesBC
    USE ModNavierStokesRHS
    USE ModMPI
    USE ModInterp
    USE ModActuator
    USE ModFVSetup
    USE ModPSAAP2

    Implicit None

    type(t_region), pointer :: region

    ! ... local variables
    integer :: i, j, k, ng, i0, j0, k0, l0, var, Nx, Ny, ii, jj, kk, ll, rkStep, timestep, ierr
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    type(t_mixt_input), pointer :: input
    real(rfreal) :: ibfac_local, fac, harvest, max_cv, max_cv_local
    character(len=80) :: cmd

    if (region%global%rk_alloc .eqv. .true.) then
      region%global%rk_alloc = .false.
      if (region%myrank == 0) write (*,'(A)') 'PlasComCM: ==> Approximating linearized RHS operator <=='
      if (region%input%nAuxVars /= 0) call graceful_exit(region%myrank, 'PlasComCM: ERROR: No auxVars allowed in APPROXIMATE_LINEAR_CNS_OPERATOR.')
      if (region%input%gridType == CHIMERA) call graceful_exit(region%myrank, 'PlasComCM: ERROR: No CHIMERA problems in APPROXIMATE_LINEAR_CNS_OPERATOR.')
      if (region%input%moveGrid == TRUE) call graceful_exit(region%myrank, 'PlasComCM: ERROR: No moving grid problems in APPROXIMATE_LINEAR_CNS_OPERATOR.')
    end if
    
    ! ... check for memory allocations
    do ng = 1, region%nGrids
      state => region%state(ng)
      grid  => region%grid(ng)
      input => grid%input
      if (.not.associated(state%rhs) .eqv. .true.) then
        allocate(state%rhs(grid%nCells, input%nCv))
      end if
      if (.not.associated(state%dt) .eqv. .true.) then
        allocate(state%dt(grid%nCells))
      end if
      if (.not.associated(state%cvOld) .eqv. .true.) then
        allocate(state%cvOld(grid%nCells, input%nCv))
      end if
      if (.not.associated(state%rhs_explicit) .eqv. .true.) then
        allocate(state%rhs_explicit(grid%nCells, input%nCv, 1))
      end if
    end do

    ! ... compute the base state rhs
    do ng = 1, region%nGrids
      state => region%state(ng)
      grid  => region%grid(ng)
      input => grid%input

      ! ... zero out the rhs vector
      Do j = 1, input%nCv
        Do i = 1, grid%nCells
          state%rhs(i,j) = 0.0_rfreal
        End Do
      End Do

      ! ... compute the dependent variables
      call computeDv(region%myrank, grid, state)

      ! ... allocate memory, if needed
      call NS_Allocate_Memory(region, ng, COMBINED, 1)

      ! ... compute velocity gradient tensor, if needed
      call NS_VelGradTensor(region, ng, COMBINED)

      ! ... compute the LES-modified transport variables, if needed
      ! ... this modifies the pressure (state%dv(1)) and temperature (state%dv(2))
      call NS_LES_Model(region, ng, COMBINED, 1)

      ! ... compute the transport variables
      call computeTv(grid, state)

      ! ... compute temperature gradient, if needed
      call NS_Temperature_Gradient(region, ng, COMBINED, 1)

      ! ... compute the shock-capturing-modified transport variables, if needed
      call NS_Shock_Capture(region, ng, COMBINED, 1)

      ! ... add tvCor to tv
      if ( input%tvCor_in_dt == TRUE) then
        do j = 1, size(state%tv,2)
          do i = 1, grid%nCells
            state%tv(i,j) = state%tv(i,j) + state%tvCor(i,j)
          end do
        end do
      end if

      ! ... compute the RHS for each grid
      call NS_RHS(region, ng, COMBINED)
      call NS_BC(region, ng, COMBINED)

      ! ... use iblank to zero out rhs for hole points
      do k = 1, input%nCv
        do i = 1, grid%nCells
          ibfac_local = grid%ibfac(i)
          state%rhs_explicit(i,k,1) = state%rhs(i,k) * ibfac_local
          state%cvOld(i,k) = state%cv(i,k)
        end do
      end do
    end do

    ! ... now compute the column for only one unknown
    !if (region%myrank == 0) then
    !  write (*,'(A)',ADVANCE='NO') 'PlasComCM: APPROXIMATE_LINEAR_OPERATOR: Input (i,j,k) global indices: '
    !  read (*,*) i0, j0, k0
    !  write (*,'(A)',ADVANCE='NO') 'PlasComCM: APPROXIMATE_LINEAR_OPERATOR: Input variable: '
    !  read (*,*) var
    !  write (*,'(A,E20.12)') 'PlasComCM: Using an epsilon of ', region%input%approximate_linear_operator_epsilon
    !end if
    !call mpi_barrier(mycomm, ierr)

    ! ... perturb the state
    ! ... compute the rhs with perturbed state
    ! ... subtract base rhs
    ! ... output
    ! ... compute the base state rhs
    Call RANDOM_SEED()
    Do ng = 1, region%nGrids
      state => region%state(ng)
      grid  => region%grid(ng)
      input => grid%input
      ! ... zero out the rhs vector
      Do j = 1, input%nCv
        Do i = 1, grid%nCells
          Call Random_Number(harvest) 
          state%cv(i,j) = state%cvOld(i,j) + 1d-5 * (harvest - 0.5_8)
        End Do
      End Do
    End Do

    Do timestep = 1, region%input%approximate_linear_operator_power_method_steps
      Do rkStep = 1, 2

        if (rkStep == 1) call NS_Timestep(region)

        Do ng = 1, region%nGrids
          state => region%state(ng)
          grid  => region%grid(ng)
          input => grid%input

          ! ... zero out the rhs vector
          Do j = 1, input%nCv
            Do i = 1, grid%nCells
              state%rhs(i,j) = 0.0_8
            End Do
          End Do

          ! ... compute the dependent variables
          call computeDv(region%myrank, grid, state)
  
          ! ... allocate memory, if needed
          call NS_Allocate_Memory(region, ng, COMBINED, 1)
  
          ! ... compute the LES-modified transport variables, if needed
          ! ... this modifies the pressure (state%dv(1)) and temperature (state%dv(2))
          call NS_LES_Model(region, ng, COMBINED, 1)
  
          ! ... compute the transport variables
          call computeTv(grid, state)

          ! ... compute temperature gradient, if needed
          call NS_Temperature_Gradient(region, ng, COMBINED, 1)

          ! ... compute the shock-capturing-modified transport variables, if needed
          call NS_Shock_Capture(region, ng, COMBINED, 1)

          ! ... add tvCor to tv
          if ( input%tvCor_in_dt == TRUE) then
            do j = 1, size(state%tv,2)
              do i = 1, grid%nCells
                state%tv(i,j) = state%tv(i,j) + state%tvCor(i,j)
              end do
            end do
          end if

          ! ... compute the RHS for each grid
          call NS_RHS(region, ng, COMBINED)
          call NS_BC(region, ng, COMBINED)

          ! ... update CV 
          if (rkStep == 1) fac = 0.5_8
          if (rkStep == 2) fac = 1.0_8
          do k = 1, input%nCv
            do i = 1, grid%nCells
              state%cv(i,k) = state%cvOld(i,k) + fac * (state%rhs(i,k)-state%rhs_explicit(i,k,1)) * state%dt(i) 
            end do
          end do
        end do
      end do

      ! ... normalize q'
      Do ng = 1, region%nGrids
        state => region%state(ng)
        grid  => region%grid(ng)
        input => grid%input
        max_cv_local = 0.0d0
        do k = 1, input%nCv
          do i = 1, grid%nCells
            state%cv(i,k) = state%cv(i,k) - state%cvOld(i,k)
            max_cv_local = MAX(max_cv_local,dabs(state%cv(i,k)))
          end do
        end do
      End Do
      call mpi_allreduce(max_cv_local, max_cv, 1, mpi_real8, mpi_max, mycomm, ierr)
      Do ng = 1, region%nGrids
        state => region%state(ng)
        grid  => region%grid(ng)
        input => grid%input
        max_cv_local = 1.0d0
        do k = 1, input%nCv
          do i = 1, grid%nCells
            state%cv(i,k) = state%cv(i,k) / max_cv * 1d-5 + state%cvOld(i,k)
          end do
        end do
      End Do

      if (region%input%filter .eqv. .true.) then
        do ng = 1, region%nGrids
          call filter(region,ng,FALSE)
        end do
      end if

      if (region%myrank == 0) write(*,'(A,I8.8)') 'PlasComCM: Done with step ', timestep
    end do

    Do ng = 1, region%nGrids
      state => region%state(ng)
      grid  => region%grid(ng)
      input => grid%input
      max_cv_local = 1.0d0
      do k = 1, input%nCv
        do i = 1, grid%nCells
          state%cv(i,k) = state%cv(i,k) - state%cvOld(i,k)
        end do
      end do
    End Do

    ! ... write
    call WriteData(region, 'cv ')
    call graceful_exit(region%myrank, 'PlasComCM: APPROXIMATE_LINEAR_CNS_OPERATOR_POWER_METHOD completed.')
   
  end subroutine APPROXIMATE_LINEAR_CNS_OPERATOR_POWER_METHOD

  Subroutine Calculate_Inf_Norm(region)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    ! ... Incoming variables
    type(t_region), pointer :: region

    ! ... local variables
    Type(t_mixt), pointer :: state
    Type(t_grid), pointer :: grid
    Real(RFREAL), Pointer :: time(:), rhs(:,:)
    Real(RFREAL), Allocatable :: inf_norm_local(:,:), inf_norm_global(:,:)
    Integer :: ng, k, size_rhs, ierr, nGrids, ii, jj, kk, l0
    Character(LEN=80) :: output_str

    nGrids = region%nGrids

    Do ng = 1, nGrids

      state => region%state(ng)
      grid  => region%grid(ng)
      rhs   => state%rhs
      time  => state%time
      size_rhs = size(rhs,2)

      If (ng == 1) Then
        Allocate(inf_norm_local(nGrids,size_rhs))
        Allocate(inf_norm_global(nGrids,size_rhs))
      End If

      ! ... find the maximum rhs over all local points
      Do k = 1, size_rhs
        inf_norm_local(ng,k) = 0.0_rfreal
        Do kk = grid%is_unique(3), grid%ie_unique(3) 
          Do jj = grid%is_unique(2), grid%ie_unique(2)
            Do ii = grid%is_unique(1), grid%ie_unique(1)
              l0 = (kk - grid%is_unique(3)+grid%nGhostRHS(3,1)) &
                   * (grid%ie(1)-grid%is(1)+1) * (grid%ie(2)-grid%is(2)+1) &
                   + (jj-grid%is_unique(2)+grid%nGhostRHS(2,1)) &
                   * (grid%ie(1)-grid%is(1)+1) + grid%nGhostRHS(1,1) &
                   + ii - grid%is_unique(1)+1
              if (inf_norm_local(ng,k) < dabs(rhs(l0,k))) &
                inf_norm_local(ng,k) = dabs(rhs(l0,k))
            End Do
          End Do
        End Do
      End Do

      ! ... find the maximum rhs over all points
      Do k = 1, size_rhs
        Call MPI_AllReduce(inf_norm_local(ng,k), inf_norm_global(ng,k), &
                           1, MPI_REAL8, MPI_MAX, mycomm, ierr)
      End Do
    End Do

    ! ... create the output format string
    Write (output_str,'(A,I1,A)') &
      "(A,2X,I4.4,2X,I8.8,(", size_rhs + 1, "E18.10))"

    If (region%input%RESIDUAL_INF_NORM_OUTPUT_FILE == TRUE) Then
      If (region%myrank == 0) Then
        Open (unit=10, &
              file=trim(region%input%RESIDUAL_INF_NORM_OUTPUT_FILENAME), &
              POSITION='append', ACTION='write', FORM='formatted', &
              IOSTAT=ierr)
        Do ng = 1, nGrids
          Write (10,output_str) '', ng, region%global%main_ts_loop_index-1, &
            region%state(1)%time(1), inf_norm_global(ng,:)
        End Do
        Close (10)
      End If
    Else
      If (region%myrank == 0) Then
        Do ng = 1, nGrids
          Write (*,output_str) 'inf-norm:', ng, &
            region%global%main_ts_loop_index-1, region%state(1)%time(1), &
            inf_norm_global(ng,:)
        End Do
      End If
    End If

    Deallocate(inf_norm_local, inf_norm_global)

    If (region%input%RESIDUAL_INF_NORM_STOP == TRUE) &
      Call Graceful_Exit(region%myrank, 'Stopping after computing inf-norm')

  End Subroutine Calculate_Inf_Norm

END MODULE ModRungeKutta
