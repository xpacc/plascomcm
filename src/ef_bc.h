#ifndef EF_BC_H
#define EF_BC_H

#include <petscdmda.h>
#include <petscmat.h>
#include <petscvec.h>

#include "ef_level.h"
#include "ef_dmap.h"
#include "ef_state.h"
#include "ef_grid.h"

/**
 * Base class for boundary conditions
 *
 * This module is used for defining common functions among boundary conditions.
 */
typedef enum {
	EF_DIRICHLET_G = 600,
	EF_DIRICHLET_P = 601,
	EF_NEUMANN = 602,
	EF_SCHWARZ = 698
} ef_bctype;


/**
 * Contains indexing information for corners of boundary condition
 */
typedef struct {
	int is[3], ie[3]; /**< index of bc's starting and ending grid point in each dimension */
} ef_corner;


/**
 * Holds boundary condition information from PlasComCM (dupcliates patch Fortran ds)
 */
typedef struct {
	ef_corner corners;
	ef_bctype bc_type;
	int norm_dir; /**< Direction of the normal */
	double *dirichlet; /**< Dirichlet BC specification for each grid point contained in BC */
	int offset[3]; /**< offset of fortran arrays (see ef_interface.h) */
	int stride[3];  /**< stride of fortran arrays (see ef_interface.h) */
} ef_patch;


/**
 * Base class for boundary condition objects
 */
typedef struct ef_bc_ {
	PetscErrorCode (*apply)(struct ef_bc_ *bc, Mat A, DM da);
	PetscErrorCode (*apply_rhs)(struct ef_bc_ *bc, DM da, Vec rhs);
	PetscErrorCode (*symmetric)(struct ef_bc_ *bc, Mat A, DM da);
	PetscErrorCode (*destroy)(struct ef_bc_ *bc);
	ef_level *level;
	int axisymmetric;
	ef_dmap *slv_dmap;
	ef_patch *patch;
	ef_state *state;
	ef_fd *fd;
	ef_bctype btype;
	void *sub; /** pointer for child specific data */
} ef_bc;


/**
 * Creates a boundary condition object
 *
 * @param[out] efbc boundary condition object that is created
 * @param btype flag to specify type of boundary condition (e.g. Dirichlet or Neumann)
 * @param norm_dir direction of the normal for the boundary condition
 * @param is global indicies of local grid starts by dimension
 * @param ie global indicies of local grid ends by dimension
 * @param dirichlet grid function of dirichlet values (not used if btype is not dirichlet)
 * @param level ef_level object
 * @param dmap data mapping object for solver
 * @param state state variable object
 * @param fd object that contains finite difference coefficients
 */
PetscErrorCode ef_bc_create(ef_bc **efbc, ef_bctype btype, int norm_dir, int is[], int ie[],
                            double *dirichlet, ef_level *level, ef_dmap *dmap,
                            ef_state *state, ef_fd *fd);


/**
 * Applies boundary condition to matrix
 *
 * @param bc boundary condition object
 * @param A matrix to apply bc to
 * @param da PETSc DM object
 */
PetscErrorCode ef_bc_apply(ef_bc *bc, Mat A, DM da);


/**
 * Applies boundary condition to the rhs
 *
 * @param bc boundary condition object
 * @param rhs right hand side for solve
 */
PetscErrorCode ef_bc_apply_rhs(ef_bc *bc, DM da, Vec rhs);


/**
 * Callback used to make the operator symmetric in the presence of this bc
 *
 * @param bc boundary condition object
 * @param A PETSc matrix
 * @param da PETSC DM object
 */
PetscErrorCode ef_bc_symmetric(ef_bc *bc, Mat A, DM da);


/**
 * Destroys data structures owned by the bc
 *
 * @param bc boundary condition object
 */
PetscErrorCode ef_bc_destroy(ef_bc *bc);


#endif
