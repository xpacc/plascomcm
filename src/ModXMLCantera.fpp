! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
! xmlparse.f90 - Simple, limited XML parser in Fortran
module xmlparse

  implicit none

  integer, parameter :: XML_BUFFER_LENGTH = 1000
!
! Define the data type that holds the parser information
!
  type XML_PARSE
     integer          :: lun                ! LU-number of the XML-file
     integer          :: level              ! Indentation level (output)
     integer          :: lineno             ! Line in file
     logical          :: ignore_whitespace  ! Ignore leading blanks etc.
     logical          :: no_data_truncation ! Do not allow data truncation
     logical          :: too_many_attribs   ! More attributes than could be stored?
     logical          :: too_many_data      ! More lines of data than could be stored?
     logical          :: eof                ! End of file?
     logical          :: error              ! Invalid XML file or other error?
     character(len=XML_BUFFER_LENGTH) :: line  ! Buffer
  end type XML_PARSE

!
! Global options
!
  integer, parameter    :: XML_STDOUT       = -1
  integer, private      :: report_lun_      = XML_STDOUT
  logical, private      :: report_errors_   = .false.
  logical, private      :: report_details_  = .false.

!
! Global data (the ampersand must come first)
!
  character(len=10), dimension(2,3), save, private :: entities = &
       reshape( (/ '&    ', '&amp;', &
       '>    ', '&gt; ',  &
       '<    ', '&lt; ' /), (/2,3/) )

!
! Auxiliary routines - private

  private               :: xml_compress_
  private               :: xml_put_open_tag_
  private               :: xml_put_element_
  private               :: xml_put_close_tag_
  private               :: xml_replace_entities_
!
! Interfaces to reporting routines
!
  private               :: xml_report_details_int_
  private               :: xml_report_details_string_
  private               :: xml_report_errors_int_
  private               :: xml_report_errors_string_

  interface xml_report_details
     module procedure xml_report_details_int_
     module procedure xml_report_details_string_
  end interface xml_report_details
  interface xml_report_errors
     module procedure xml_report_errors_int_
     module procedure xml_report_errors_string_
     module procedure xml_report_errors_extern_
  end interface xml_report_errors

contains

! xml_report_details_int_ --
!    Routine to write a text with an integer value
! Arguments:
!    text        Text to be written
!    int         Integer value to be added
!
  subroutine xml_report_details_int_( text, int )
    character(len=*), intent(in)     :: text
    integer,          intent(in)     :: int

    if ( report_details_ ) then
       if ( report_lun_ .eq. XML_STDOUT ) then
          write(*,*) trim(text), int
       else
          write(report_lun_,*) trim(text), int
       endif
    endif
  end subroutine xml_report_details_int_

! xml_report_details_string_ --
!    Routine to write a text with a string value
! Arguments:
!    text        Text to be written
!    string      String to be added
!
  subroutine xml_report_details_string_( text, string )
    character(len=*), intent(in)     :: text
    character(len=*), intent(in)     :: string

    if ( report_details_ ) then
       if ( report_lun_ .eq. XML_STDOUT ) then
          write(*,*) trim(text), ' ', trim(string)
       else
          write(report_lun_,*) trim(text), ' ', trim(string)
       endif
    endif
  end subroutine xml_report_details_string_


! xml_report_errors_string_ --
!    Routine to write an error message text with an integer value
! Arguments:
!    text        Text to be written
!    int         Integer value to be added
!    lineno      Line number in the file
!
  subroutine xml_report_errors_int_( text, int, lineno )
    character(len=*),  intent(in)     :: text
    integer,           intent(in)     :: int
    integer, optional, intent(in)     :: lineno

    if ( report_errors_ .or. report_details_ ) then
       if ( report_lun_ .eq. XML_STDOUT ) then
          write(*,*) trim(text), int
          if ( present(lineno) ) then
             write(*,*) '   At or near line', lineno
          endif
       else
          write(report_lun_,*) trim(text), int
          if ( present(lineno) ) then
             write(report_lun_,*) '   At or near line', lineno
          endif
       endif
    endif
  end subroutine xml_report_errors_int_

! xml_report_errors_string_ --
!    Routine to write an error message text with a string value
! Arguments:
!    text        Text to be written
!    string      String to be added
!    lineno      Line number in the file
!
  subroutine xml_report_errors_string_( text, string, lineno )
    character(len=*),  intent(in)     :: text
    character(len=*),  intent(in)     :: string
    integer, optional, intent(in)     :: lineno

    if ( report_errors_ .or. report_details_ ) then
       if ( report_lun_ .eq. XML_STDOUT ) then
          write(*,*) trim(text), ' ', trim(string)
          if ( present(lineno) ) then
             write(*,*) '   At or near line', lineno
          endif
       else
          write(report_lun_,*) trim(text), ' ', trim(string)
          if ( present(lineno) ) then
             write(report_lun_,*) '   At or near line', lineno
          endif
       endif
    endif
  end subroutine xml_report_errors_string_

! xml_report_errors_extern_ --
!    Routine to write an error message text with a string value
! Arguments:
!    info        Structure holding information on the XML-file
!    text        Text to be written
! Note:
!    This routine is meant for use by routines outside
!    this module
!
  subroutine xml_report_errors_extern_( info, text )
    type(XML_PARSE),   intent(in)     :: info
    character(len=*),  intent(in)     :: text

    if ( report_lun_ .eq. XML_STDOUT ) then
       write(*,*) trim(text), ' - at or near line', info%lineno
    else
       write(report_lun_,*) trim(text), ' - at or near line', info%lineno
    endif
  end subroutine xml_report_errors_extern_

! xml_open --
!    Routine to open an XML file for reading or writing
! Arguments:
!    info        Structure holding information on the XML-file
!    fname       Name of the file
!    mustread    The file will be read (.true.) or written (.false.)
!
  subroutine xml_open( info, fname, mustread )
    character(len=*), intent(in)     :: fname
    logical,          intent(in)     :: mustread
    type(XML_PARSE),  intent(out)    :: info

    integer                          :: i
    integer                          :: k
    integer                          :: kend
    integer                          :: ierr
    logical                          :: opend
    logical                          :: exists

    info%lun = 10
    info%ignore_whitespace  = .false.
    info%no_data_truncation = .false.
    info%too_many_attribs   = .false.
    info%too_many_data      = .false.
    info%eof                = .false.
    info%error              = .false.
    info%level              = -1
    info%lineno             =  0

    do i = 10,99
       inquire( unit = i, opened = opend )
       if ( .not. opend ) then
          info%lun = i
          inquire( file = fname, exist = exists )
          if ( .not. exists .and. mustread ) then
             call xml_report_errors( 'XML_OPEN: file does not exist:', trim(fname))
             info%lun   = -1
             info%error = .true.
          else
             open( unit = info%lun, file = fname )
             call xml_report_details( 'XML_OPEN: opened file ', trim(fname) )
             call xml_report_details( 'at LU-number: ', info%lun )
          endif
          exit
       endif
    enddo
    if ( .not. info%error .and. mustread ) then
       k = 1
       do while ( k .ge. 1 )
          read( info%lun, '(a)', iostat = ierr ) info%line
          if ( ierr .eq. 0 ) then
             info%line = adjustl(  info%line )
             k         = index( info%line, '<?' )
!
! Assume (for now at least) that <?xml ... ?> appears on a single line!
!
             if ( k .ge. 1 ) then
                kend = index( info%line, '?>' )
                if ( kend .le. 0 ) then
                   call xml_report_errors( 'XML_OPEN: error reading file with LU-number: ', info%lun )
                   call xml_report_errors( 'Line starting with "<?xml" should end with "?>"', ' ' )
                   info%error = .true.
                   exit
                endif
             endif
          else
             call xml_report_errors( 'XML_OPEN: error reading file with LU-number: ', info%lun )
             call xml_report_errors( 'Possibly no line starting with "<?xml"', ' ' )
             call xml_close( info )
             info%error = .true.
             exit
          endif
       enddo
    endif
    if ( .not. info%error .and. .not. mustread ) then
       write( info%lun, '(a)' ) '<?xml version="1.0"?>'
    endif
  end subroutine xml_open

! xml_close --
!    Routine to close an XML file
! Arguments:
!    info        Structure holding information on the XML-file
!
  subroutine xml_close( info )
    type(XML_PARSE),  intent(inout)    :: info

    close( info%lun )

!
! Only clean up the LU-number, so that the calling program
! can examine the last condition
!
    call xml_report_details( 'XML_CLOSE: Closing file with LU-number ', info%lun )
    info%lun              = -1
  end subroutine xml_close

! xml_get --
!    Routine to get the next bit of information from an XML file
! Arguments:
!    info        Structure holding information on the XML-file
!    tag         Tag that was encountered
!    endtag      Whether the end of the element was encountered
!    attribs     List of attribute-value pairs
!    no_attribs  Number of pairs in the list
!    data        Lines of character data found
!    no_data     Number of lines of character data
!
  subroutine xml_get( info, tag, endtag, attribs, no_attribs, &
       data, no_data )
    type(XML_PARSE),  intent(inout)               :: info
    character(len=*), intent(out)                 :: tag
    logical,          intent(out)                 :: endtag
    character(len=*), intent(out), dimension(:,:) :: attribs
    integer,          intent(out)                 :: no_attribs
    character(len=*), intent(out), dimension(:)   :: data
    integer,          intent(out)                 :: no_data

    integer         :: kspace
    integer         :: kend
    integer         :: keq
    integer         :: kfirst
    integer         :: ksecond
    integer         :: idxat
    integer         :: idxdat
    integer         :: ierr
    logical         :: close_bracket
    logical         :: comment_tag
    character(len=XML_BUFFER_LENGTH) :: nextline

!
! Initialise the output
!
    endtag     = .false.
    no_attribs = 0
    no_data    = 0

    info%too_many_attribs = .false.
    info%too_many_data    = .false.

    if ( info%lun .lt. 0 ) then
       call xml_report_details( 'XML_GET on closed file ', ' ' )
       return
    endif

!
! From the previous call or the call to xmlopen we have
! the line that we need to parse already in memory:
! <tag attrib1="..." attrib2="..." />
!
    comment_tag   = .false.
    close_bracket = .false.
    kspace        = index( info%line, ' ' )
    kend          = index( info%line, '>' )
    do while ( kend .le. 0 )
       read( info%lun, '(a)', iostat = ierr ) nextline
       info%lineno = info%lineno + 1

       if ( ierr .eq. 0 ) then
          info%line = trim(info%line) // ' ' // adjustl(nextline)
       else
          info%error = .true.
          call xml_report_errors( 'XML_GET - end of tag not found ', &
               '(buffer too small?)', info%lineno )
          call xml_close( info )
          return
       endif
       kend = index( info%line, '>' )
    enddo
    if ( kend .gt. kspace ) then
       kend = kspace
    else
       close_bracket = .true.
    endif

!
! Check for the end of an ordianry tag and of
! a comment tag
!
    if ( info%line(1:3) .eq. '-->' ) then
       endtag = .true.
       tag    = info%line(4:kend-1)
    else if ( info%line(1:2) .eq. '</' ) then
       endtag = .true.
       tag    = info%line(3:kend-1)
    else
       if ( info%line(1:1) .eq. '<' ) then
          tag    = info%line(2:kend-1)
          call xml_report_details( 'XML_GET - tag found: ', trim(tag) )
       else
          kend   = 0 ! Beginning of data!
       endif
    endif

    info%line = adjustl( info%line(kend+1:) )

    idxat     = 0
    idxdat    = 0

    if ( tag(1:3) .eq. '!--' ) comment_tag = .true.

    do while ( info%line .ne. ' ' .and. .not. close_bracket .and. .not. comment_tag )

       keq  = index( info%line, '=' )
       kend = index( info%line, '>' )
       if ( keq .gt. kend ) keq = 0 ! Guard against multiple tags
! with attributes on one line

!
! No attributes any more?
!
       if ( keq .lt. 1 ) then
          kend = index( info%line, '/>' )
          if ( kend .ge. 1 ) then
             kend   = kend + 1 ! To go beyond the ">" character
             endtag = .true.
          else
             kend = index( info%line, '>' )
             if ( kend .lt. 1 ) then
                call xml_report_errors( 'XML_GET - wrong ending of tag ', &
                     trim(info%line), info%lineno  )
                info%error = .true. ! Wrong ending of line!
                call xml_close( info )
                return
             else
                close_bracket = .true.
             endif
          endif
          if ( kend .ge. 1 ) then
             info%line = adjustl( info%line(kend+1:) )
          endif
          exit
       endif

       idxat = idxat + 1
       if ( idxat .le. size(attribs,2) ) then
          no_attribs = idxat
          attribs(1,idxat) = adjustl(info%line(1:keq-1)) ! Use adjustl() to avoid
! multiple spaces, etc
          info%line = adjustl( info%line(keq+1:) )

!
! We have almost found the start of the attribute's value
!
          kfirst  = index( info%line, '"' )
          if ( kfirst .lt. 1 ) then
             call xml_report_errors( 'XML_GET - malformed attribute-value pair: ', &
                  trim(info%line), info%lineno  )
             info%error = .true. ! Wrong form of attribute-value pair
             call xml_close( info )
             return
          endif

          ksecond = index( info%line(kfirst+1:), '"' ) + kfirst
          if ( ksecond .lt. 1 ) then
             call xml_report_errors( 'XML_GET - malformed attribute-value pair: ', &
                  trim(info%line), info%lineno  )
             info%error = .true. ! Wrong form of attribute-value pair
             call xml_close( info )
             return
          endif

          attribs(2,idxat) = info%line(kfirst+1:ksecond-1)
          info%line = adjustl( info%line(ksecond+1:) )
       endif

       if ( idxat .gt. size(attribs,2) ) then
          call xml_report_errors( 'XML_GET - more attributes than could be stored: ', &
               trim(info%line), info%lineno  )
          info%too_many_attribs = .true.
          info%line             = ' '
          exit
       endif
    enddo

!
! Now read the data associated with the current tag
! - all the way to the next "<" character
!
! To do: reduce the number of data lines - empty ones
! at the end should not count.
!
    do
       if ( comment_tag ) then
          kend   = index( info%line, '-->' )
       else
          kend   = index( info%line, '<' )
       endif
       idxdat = idxdat + 1
       if ( idxdat .le. size(data) ) then
          no_data = idxdat
          if ( kend .ge. 1 ) then
             data(idxdat) = info%line(1:kend-1)
             info%line    = info%line(kend:)
          else
             data(idxdat) = info%line
          endif
       else
          call xml_report_errors( 'XML_GET - more data lines than could be stored: ', &
               trim(info%line), info%lineno  )
          info%too_many_data = .true.
          exit
       endif

!
! No more data? Otherwise, read on
!
       if ( kend .ge. 1 ) then
          exit
       else
          read( info%lun, '(a)', iostat = ierr ) info%line
          info%lineno = info%lineno + 1

          if ( ierr .lt. 0 ) then
             call xml_report_details( 'XML_GET - end of file found - LU-number: ', &
                  info%lun )
             info%eof = .true.
          elseif ( ierr .gt. 0 ) then
             call xml_report_errors( 'XML_GET - error reading file with LU-number ', &
                  info%lun, info%lineno  )
             info%error = .true.
          endif
          if ( ierr .ne. 0 ) then
             exit
          endif
       endif
    enddo

!
! Compress the data?
!
    if ( info%ignore_whitespace ) then
       call xml_compress_( data, no_data )
    endif

!
! Replace the entities, if any
!
    call xml_replace_entities_( data, no_data )

    call xml_report_details( 'XML_GET - number of attributes: ', no_attribs )
    call xml_report_details( 'XML_GET - number of data lines: ', no_data    )

  end subroutine xml_get

! xml_put --
!    Routine to write a tag with the associated data to an XML file
! Arguments:
!    info        Structure holding information on the XML-file
!    tag         Tag that was encountered
!    endtag      Whether the end of the element was encountered
!    attribs     List of attribute-value pairs
!    no_attribs  Number of pairs in the list
!    data        Lines of character data found
!    no_data     Number of lines of character data
!    type        Type of action:
!                open - just the opening tag with attributes
!                elem - complete element
!                close - just the closing tag
!
  subroutine xml_put(info, tag, attribs, no_attribs, &
       data, no_data, type)

    type(XML_PARSE),  intent(inout)               :: info
    character(len=*), intent(in)                  :: tag
    character(len=*), intent(in), dimension(:,:)  :: attribs
    integer,          intent(in)                  :: no_attribs
    character(len=*), intent(in), dimension(:)    :: data
    integer,          intent(in)                  :: no_data
    character(len=*)                              :: type

    integer         :: i

    character(len=300), parameter :: indent = ' '

    select case(type)
    case('open')
       call xml_put_open_tag_(info, tag, attribs, no_attribs, &
            data, no_data)
    case('elem')
       call xml_put_element_(info, tag, attribs, no_attribs, &
            data, no_data)
    case('close')
       call xml_put_close_tag_(info, tag, attribs, no_attribs, &
            data, no_data)
    end select

  end subroutine xml_put

! xml_put_open_tag_ --
!    Routine to write the opening tag with the attributes
! Arguments:
!    info        Structure holding information on the XML-file
!    tag         Tag that was encountered
!    endtag      Whether the end of the element was encountered
!    attribs     List of attribute-value pairs
!    no_attribs  Number of pairs in the list
!    data        Lines of character data found
!    no_data     Number of lines of character data
!
  subroutine xml_put_open_tag_(info, tag, attribs, no_attribs, &
       data, no_data)

    type(XML_PARSE),  intent(inout)               :: info
    character(len=*), intent(in)                  :: tag
    character(len=*), intent(in), dimension(:,:)  :: attribs
    integer,          intent(in)                  :: no_attribs
    character(len=*), intent(in), dimension(:)    :: data
    integer,          intent(in)                  :: no_data

    character(len=1)                              :: aa
    integer         :: i

    character(len=300), parameter :: indent = ' '

    write( info%lun, '(3a)', advance = 'no' ) &
         indent(1:3*info%level), '<', adjustl(tag)
    do i=1,no_attribs
       if (attribs(2,i).ne.'') then
          write( info%lun, '(5a)', advance = 'no' ) &
               ' ',trim(attribs(1,i)),'="', trim(attribs(2,i)),'"'
       endif
    enddo
    write( info%lun, '(a)' ) '>'
    info%level = info%level + 1

  end subroutine xml_put_open_tag_

! xml_put_element_ --
!    Routine to write the complete element
! Arguments:
!    info        Structure holding information on the XML-file
!    tag         Tag that was encountered
!    endtag      Whether the end of the element was encountered
!    attribs     List of attribute-value pairs
!    no_attribs  Number of pairs in the list
!    data        Lines of character data found
!    no_data     Number of lines of character data
!
  subroutine xml_put_element_(info, tag, attribs, no_attribs, &
       data, no_data)

    type(XML_PARSE),  intent(inout)               :: info
    character(len=*), intent(in)                  :: tag
    character(len=*), intent(in), dimension(:,:)  :: attribs
    integer,          intent(in)                  :: no_attribs
    character(len=*), intent(in), dimension(:)    :: data
    integer,          intent(in)                  :: no_data

    logical          :: logic
    character(len=1) :: aa
    integer          :: i, ii

    character(len=300), parameter :: indent = ' '

    if ( (no_attribs.eq.0 .and. no_data.eq.0) ) then
       return
    else
       logic = .true.
       do ii = 1,no_attribs
          logic = logic .and. (attribs(2,ii).eq.'')
       enddo
       do ii = 1,no_data
          logic = logic .and. (data(ii).eq.'')
       enddo
       if ( logic ) then
          return
       else
          write( info%lun, '(3a)', advance = 'no' ) &
               indent(1:3*info%level), '<', adjustl(tag)
          do i = 1,no_attribs
             if (attribs(2,i).ne.'') then
                write( info%lun, '(5a)', advance = 'no' ) &
                     ' ',trim(attribs(1,i)),'="', trim(attribs(2,i)),'"'
             endif
          enddo
          if ( no_attribs.gt.0 .and. no_data.eq.0 ) then
             aa='a'
          elseif ( (no_attribs.gt.0 .and. no_data.gt.0) .or. &
               (no_attribs.eq.0 .and. no_data.gt.0) ) then
             aa='b'
          else
             write(*,*) no_attribs, no_data
          endif
       endif
    endif

    select case(aa)
    case('a')
       write( info%lun, '(a)' ) '/>'
    case('b')
       write( info%lun, '(a)',advance='no' ) '>'
       write( info%lun, '(2a)', advance='no') &
            ( ' ', trim(data(i)), i=1,no_data )
       write( info%lun, '(4a)' ) ' ','</', tag, '>'
    end select

  end subroutine xml_put_element_

! xml_put_close_tag_ --
!    Routine to write the closing tag
! Arguments:
!    info        Structure holding information on the XML-file
!    tag         Tag that was encountered
!    endtag      Whether the end of the element was encountered
!    attribs     List of attribute-value pairs
!    no_attribs  Number of pairs in the list
!    data        Lines of character data found
!    no_data     Number of lines of character data
!
  subroutine xml_put_close_tag_(info, tag, attribs, no_attribs, &
       data, no_data)

    type(XML_PARSE),  intent(inout)               :: info
    character(len=*), intent(in)                  :: tag
    character(len=*), intent(in), dimension(:,:)  :: attribs
    integer,          intent(in)                  :: no_attribs
    character(len=*), intent(in), dimension(:)    :: data
    integer,          intent(in)                  :: no_data

    integer         :: i

    character(len=300), parameter :: indent = ' '

    info%level=info%level-1
    write( info%lun, '(4a)' ) &
         indent(1:3*info%level), '</', adjustl(tag), '>'

  end subroutine xml_put_close_tag_

! xml_compress_ --
!    Routine to remove empty lines from the character data
! Arguments:
!    data        Lines of character data found
!    no_data     (Nett) number of lines of character data
!
  subroutine xml_compress_( data, no_data )
    character(len=*), intent(inout), dimension(:)    :: data
    integer,          intent(inout)                  :: no_data

    integer :: i
    integer :: j
    logical :: empty

    j     = 0
    empty = .true.
    do i = 1,no_data
       if ( len_trim(data(i)) .ne. 0 .or. .not. empty ) then
          j       = j + 1
          data(j) = adjustl(data(i))
          empty = .false.
       endif
    enddo

    no_data = j

    do i = no_data,1,-1
       if ( len_trim(data(i)) .ne. 0 ) then
          exit
       else
          no_data = no_data - 1
       endif
    enddo

  end subroutine xml_compress_

! xml_replace_entities_ --
!    Routine to replace entities such as &gt; by their
!    proper character representation
! Arguments:
!    data        Lines of character data found
!    no_data     (Nett) number of lines of character data
!
  subroutine xml_replace_entities_( data, no_data )
    character(len=*), intent(inout), dimension(:)    :: data
    integer,          intent(inout)                  :: no_data

    integer :: i
    integer :: j
    integer :: j2
    integer :: k
    integer :: pos
    logical :: found

    do i = 1,no_data
       j = 1
       do
          do k = 1,size(entities,2)
             found = .false.
             pos   = index( data(i)(j:), trim(entities(2,k)) )
             if ( pos .gt. 0 ) then
                found = .true.
                j     = j + pos - 1
                j2    = j + len_trim(entities(2,k))
                data(i)(j:) = trim(entities(1,k)) // data(i)(j2:)
                j     = j2
             endif
          enddo
          if ( .not. found ) exit
       enddo
    enddo

  end subroutine xml_replace_entities_

! xml_options --
!    Routine to handle the parser options
! Arguments:
!    info                Structure holding information on the XML-file
!    ignore_whitespace   Ignore whitespace (leading blanks, empty lines) or not
!    no_data_truncation  Consider truncation of strings an error or not
!    report_lun          LU-number for reporting information
!    report_errors       Write messages about errors or not
!    report_details      Write messages about all kinds of actions or not
!
  subroutine xml_options( info, ignore_whitespace, no_data_truncation, &
       report_lun, report_errors, &
       report_details )
    type(XML_PARSE),  intent(inout)               :: info
    logical, intent(in), optional                 :: ignore_whitespace
    logical, intent(in), optional                 :: no_data_truncation

    integer, intent(in), optional                 :: report_lun
    logical, intent(in), optional                 :: report_errors
    logical, intent(in), optional                 :: report_details

    if ( present(ignore_whitespace) ) then
       info%ignore_whitespace = ignore_whitespace
    endif
    if ( present(no_data_truncation) ) then
       info%no_data_truncation = no_data_truncation
    endif
    if ( present(report_lun) ) then
       report_lun_ = report_lun
    endif
    if ( present(report_errors) ) then
       report_errors_ = report_errors
    endif
    if ( present(report_details) ) then
       report_details_ = report_details
    endif
  end subroutine xml_options

! xml_ok --
!    Function that returns whether all was okay or not
! Arguments:
!    info                Structure holding information on the XML-file
! Returns:
!    .true. if there was no error, .false. otherwise
!
  logical function xml_ok( info )
    type(XML_PARSE),  intent(in)               :: info

    xml_ok = info%eof .or. info%error .or. &
         ( info%no_data_truncation .and.    &
         ( info%too_many_attribs .or. info%too_many_data ) )
    xml_ok = .not. xml_ok
  end function xml_ok

! xml_error --
!    Function that returns whether there was an error
! Arguments:
!    info                Structure holding information on the XML-file
! Returns:
!    .true. if there was an error, .false. if there was none
!
  logical function xml_error( info )
    type(XML_PARSE),  intent(in)               :: info

    xml_error = info%error .or. &
         ( info%no_data_truncation .and.    &
         ( info%too_many_attribs .or. info%too_many_data ) )
  end function xml_error

! xml_data_trunc --
!    Function that returns whether data were truncated or not
! Arguments:
!    info                Structure holding information on the XML-file
! Returns:
!    .true. if data were truncated, .false. otherwise
!
  logical function xml_data_trunc( info )
    type(XML_PARSE),  intent(in)               :: info

    xml_data_trunc = info%too_many_attribs .or. info%too_many_data
  end function xml_data_trunc

  integer function xml_find_attrib( attribs, no_attribs, name, value )
    character(len=*), dimension(:,:)  :: attribs
    integer                           :: no_attribs
    character(len=*)                  :: name
    character(len=*)                  :: value

    integer :: i

    xml_find_attrib = -1
    do i = 1,no_attribs
       if ( name .eq. attribs(1,i) ) then
          value           = attribs(2,i)
          xml_find_attrib = i
          exit
       endif
    enddo

  end function xml_find_attrib

! xml_process --
!    Routine to read the XML file as a whole and distribute processing
!    the contents over three user-defined subroutines
! Arguments:
!    filename            Name of the file to process
!    attribs             Array for holding the attributes
!    data                Array for holding the character data
!    startfunc           Subroutine to handle the start of elements
!    datafunc            Subroutine to handle the character data
!    endfunc             Subroutine to handle the end of elements
!    error               Indicates if there was an error or not
! Note:
!    The routine is declared recursive to allow inclusion of XML files
!    (common with XSD schemas). This extends to the auxiliary routines.
!
  recursive &
       subroutine xml_process( filename, attribs, data, startfunc, datafunc, endfunc, lunrep, error )
  character(len=*)                  :: filename
  character(len=*), dimension(:,:)  :: attribs
  character(len=*), dimension(:)    :: data
  integer                           :: lunrep
  logical                           :: error

  interface
     recursive subroutine startfunc( tag, attribs, error )
       character(len=*)                  :: tag
       character(len=*), dimension(:,:)  :: attribs
       logical                           :: error
     end subroutine startfunc
  end interface

  interface
     recursive subroutine datafunc( tag, data, error )
       character(len=*)                  :: tag
       character(len=*), dimension(:)    :: data
       logical                           :: error
     end subroutine datafunc
  end interface

  interface
     recursive subroutine endfunc( tag, error )
       character(len=*)                  :: tag
       logical                           :: error
     end subroutine endfunc
  end interface

  type(XML_PARSE)                               :: info
  character(len=80)                             :: tag
  logical                                       :: endtag
  integer                                       :: noattribs
  integer                                       :: nodata

  call xml_options( info, report_lun = lunrep, report_details = .false. )
  call xml_open( info, filename, .true. )

  error = .false.
  do
     call xml_get( info, tag, endtag, attribs, noattribs, data, nodata )
     if ( .not. xml_ok(info) ) then
        exit
     endif

     if ( xml_error(info) ) then
        write(lunrep,*) 'Error reading XML file!'
        error = .true.
        exit
     endif

     if ( .not. endtag .or. noattribs .ne. 0 ) then
        call startfunc( tag, attribs(:,1:noattribs), error )
        if ( error ) exit

        call datafunc( tag, data(1:nodata), error )
        if ( error ) exit
     endif

     if ( endtag ) then
        call endfunc( tag, error )
        if ( error ) exit
     endif
  enddo
  call xml_close( info )
end subroutine xml_process

end module xmlparse
! read_xml_prims.f90 - Read routines for primitive data
!
! $Id: read_xml_prims.f90,v 1.7 2007/12/07 10:38:41 arjenmarkus Exp $
!
! Arjen Markus
!
! General information:
! This module is part of the XML-Fortran library. Its
! purpose is to help read individual items from an XML
! file into the variables that have been connected to
! the various tags. It is used by the code generated
! by the make_xml_reader program.
!
! Because the routines differ mostly by the type of the
! output variable, the body is included, to prevent
! too much repeated blocks of code with all the maintenance
! issues that causes.
!
module read_xml_primitives
use xmlparse
implicit none

private :: read_from_buffer
private :: read_from_buffer_integers
private :: read_from_buffer_reals
private :: read_from_buffer_doubles
private :: read_from_buffer_logicals
private :: read_from_buffer_words

interface read_from_buffer
   module procedure read_from_buffer_integers
   module procedure read_from_buffer_reals
   module procedure read_from_buffer_doubles
   module procedure read_from_buffer_logicals
   module procedure read_from_buffer_words
end interface read_from_buffer

contains

! skip_until_endtag --
!    Routine to read the XML file until the end tag is encountered
!
! Arguments:
!    info        The XML file data structure
!    tag         The tag in question
!    attribs     Array of attributes and their values
!    data        Array of strings, representing the data
!    error       Has an error occurred?
!
subroutine skip_until_endtag( info, tag, attribs, data, error )
  type(XML_PARSE), intent(inout)                  :: info
  character(len=*), intent(in)                    :: tag
  character(len=*), dimension(:,:), intent(inout) :: attribs
  character(len=*), dimension(:), intent(inout)   :: data
  logical, intent(out)                            :: error

  integer                                         :: noattribs
  integer                                         :: nodata
  integer                                         :: ierr
  logical                                         :: endtag
  character(len=len(tag))                         :: newtag

  error = .true.
  do
     call xml_get( info, newtag, endtag, attribs, noattribs, &
          data, nodata )
     if ( xml_error(info) ) then
        error = .true.
        exit
     endif
     if ( endtag .and. newtag .eq. tag ) then
        exit
     endif
  enddo
end subroutine skip_until_endtag

! read_xml_integer --
!    Routine to read a single integer from the parsed data
!
! Arguments:
!    info        XML parser structure
!    tag         The tag in question (error message only)
!    endtag      End tag found? (Dummy argument, actually)
!    attribs     Array of attributes and their values
!    noattribs   Number of attributes found
!    data        Array of strings, representing the data
!    nodata      Number of data strings
!    var         Variable to be filled
!    has_var     Has the variable been set?
!
subroutine read_xml_integer( info, tag, endtag, attribs, noattribs, data, nodata, &
     var, has_var )
  integer, intent(inout)                       :: var

  include 'read_xml_scalar.inc'

end subroutine read_xml_integer

! read_xml_line --
!    Routine to read a single line of text from the parsed data
!
! Arguments:
!    info        XML parser structure
!    tag         The tag in question (error message only)
!    endtag      End tag found? (Dummy argument, actually)
!    attribs     Array of attributes and their values
!    noattribs   Number of attributes found
!    data        Array of strings, representing the data
!    nodata      Number of data strings
!    var         Variable to be filled
!    has_var     Has the variable been set?
!
subroutine read_xml_line( info, tag, endtag, attribs, noattribs, data, nodata, &
     var, has_var )
  type(XML_PARSE), intent(inout)               :: info
  character(len=*), intent(in)                 :: tag
  logical, intent(inout)                       :: endtag
  character(len=*), dimension(:,:), intent(in) :: attribs
  integer, intent(in)                          :: noattribs
  character(len=*), dimension(:), intent(in)   :: data
  integer, intent(in)                          :: nodata
  character(len=*), intent(inout)              :: var
  logical, intent(inout)                       :: has_var

  character(len=len(attribs(1,1)))             :: buffer
  integer                                      :: idx
  integer                                      :: ierr

!
! The value can be stored in an attribute value="..." or in
! the data
!
  has_var = .false.
  idx = xml_find_attrib( attribs, noattribs, 'value', buffer )
  if ( idx .gt. 0 ) then
     var     = buffer
     has_var = .true.
  else
     do idx = 1,nodata
        if ( data(idx) .ne. ' ' ) then
           var = data(idx)
           has_var = .true.
           exit
        endif
     enddo
  endif
end subroutine read_xml_line

! read_xml_real, ... --
!    See read_xml_integer for an explanation
!
subroutine read_xml_real( info, tag, endtag, attribs, noattribs, data, nodata, &
     var, has_var )
  real, intent(inout)                          :: var

  include 'read_xml_scalar.inc'

end subroutine read_xml_real

subroutine read_xml_double( info, tag, endtag, attribs, noattribs, data, nodata, &
     var, has_var )
  real(kind=kind(1.0d00)), intent(inout)       :: var

  include 'read_xml_scalar.inc'

end subroutine read_xml_double

subroutine read_xml_logical( info, tag, endtag, attribs, noattribs, data, nodata, &
     var, has_var )
  logical, intent(inout)       :: var

  include 'read_xml_scalar.inc'

end subroutine read_xml_logical

subroutine read_xml_word( info, tag, endtag, attribs, noattribs, data, nodata, &
     var, has_var )
  character(len=*), intent(inout)       :: var

  include 'read_xml_scalar.inc'

end subroutine read_xml_word

! read_xml_integer_array --
!    Routine to read a one-dimensional integer array from the parsed
!    ata
!
! Arguments:
!    info        XML parser structure
!    tag         The tag in question (error message only)
!    endtag      End tag found? (Dummy argument, actually)
!    attribs     Array of attributes and their values
!    noattribs   Number of attributes found
!    data        Array of strings, representing the data
!    nodata      Number of data strings
!    var         Variable to be filled
!    has_var     Has the variable been set?
!
subroutine read_xml_integer_array( info, tag, endtag, attribs, noattribs, data, &
     nodata, var, has_var )
  integer, dimension(:), pointer                :: var

  include 'read_xml_array.inc'

end subroutine read_xml_integer_array

! read_xml_line_array --
!    Routine to read an array of lines of text from the parsed data
!
! Arguments:
!    info        XML parser structure
!    tag         The tag in question (error message only)
!    attribs     Array of attributes and their values
!    noattribs   Number of attributes found
!    data        Array of strings, representing the data
!    nodata      Number of data strings
!    var         Variable to be filled
!    has_var     Has the variable been set?
!
subroutine read_xml_line_array( info, tag, endtag, attribs, noattribs, data, &
     nodata, var, has_var )
  type(XML_PARSE), intent(inout)                :: info
  character(len=*), intent(in)                  :: tag
  logical, intent(inout)                        :: endtag
  character(len=*), dimension(:,:), intent(in)  :: attribs
  integer, intent(in)                           :: noattribs
  character(len=*), dimension(:), intent(in)    :: data
  integer, intent(in)                           :: nodata
  character(len=*), dimension(:), pointer       :: var
  logical, intent(inout)                        :: has_var

  character(len=len(attribs(1,1)))              :: buffer
  integer                                       :: idx
  integer                                       :: idxv
  integer                                       :: ierr
  logical                                       :: started

!
! The value can be stored in an attribute values="..." or in
! the data
!
  has_var = .false.
  idx = xml_find_attrib( attribs, noattribs, 'values', buffer )
  if ( idx .gt. 0 ) then
     allocate( var(1:1) )
     var(1) = buffer
     if ( buffer .ne. ' ' ) then
        has_var = .true.
     endif
  else
     idxv    = 0
     started = .false.
     do idx = 1,nodata
        if ( data(idx) .ne. ' ' .or. started ) then
           if ( .not. started ) then
              allocate( var(1:nodata-idx+1) )
              started = .true.
           endif
           idxv = idxv + 1
           var(idxv) = data(idx)
        endif
     enddo
     if ( started ) then
        has_var = .true.
     endif
  endif
end subroutine read_xml_line_array

! read_xml_real_array, ... --
!    See read_xml_integer_array for an explanation
!
subroutine read_xml_real_array( info, tag, endtag, attribs, noattribs, data, &
     nodata, var, has_var )
  real, dimension(:), pointer :: var

  include 'read_xml_array.inc'

end subroutine read_xml_real_array

subroutine read_xml_double_array( info, tag, endtag, attribs, noattribs, data, &
     nodata, var, has_var )
  real(kind=kind(1.0d00)), dimension(:), pointer :: var

  include 'read_xml_array.inc'

end subroutine read_xml_double_array

subroutine read_xml_logical_array( info, tag, endtag, attribs, noattribs, data, &
     nodata, var, has_var )
  logical, dimension(:), pointer :: var

  include 'read_xml_array.inc'

end subroutine read_xml_logical_array

subroutine read_xml_word_array( info, tag, endtag, attribs, noattribs, data, &
     nodata, var, has_var )
  character(len=*), dimension(:), pointer :: var

  include 'read_xml_array.inc'

end subroutine read_xml_word_array

! read_from_buffer_integers --
!    Routine to read all integers from a long string
!
! Arguments:
!    buffer      String containing the data
!    var         Variable to be filled
!    ierror      Error flag
!
subroutine read_from_buffer_integers( buffer, var, ierror )
  integer, dimension(:), pointer                :: var
  integer, dimension(:), pointer                :: work

  include 'read_from_buffer.inc'

end subroutine read_from_buffer_integers

! read_xml_from_buffer_reals, ... -
!    See read_xml_from_buffer_integers for an explanation
!
subroutine read_from_buffer_reals( buffer, var, ierror )
  real, dimension(:), pointer                :: var
  real, dimension(:), pointer                :: work

  include 'read_from_buffer.inc'

end subroutine read_from_buffer_reals

subroutine read_from_buffer_doubles( buffer, var, ierror )
  real(kind=kind(1.0d00)), dimension(:), pointer :: var
  real(kind=kind(1.0d00)), dimension(:), pointer :: work

  include 'read_from_buffer.inc'

end subroutine read_from_buffer_doubles

subroutine read_from_buffer_logicals( buffer, var, ierror )
  logical, dimension(:), pointer :: var
  logical, dimension(:), pointer :: work

  include 'read_from_buffer.inc'

end subroutine read_from_buffer_logicals

subroutine read_from_buffer_words( buffer, var, ierror )
  character(len=*), dimension(:), pointer :: var
  character(len=len(var)), dimension(:), pointer :: work

  include 'read_from_buffer.inc'

end subroutine read_from_buffer_words

! read_xml_word_1dim, ... -
!    Read an array of "words" (or ...) but from different elements
!
subroutine read_xml_integer_1dim( info, tag, endtag, attribs, noattribs, data, nodata, &
     var, has_var )
  type(XML_PARSE), intent(inout)                :: info
  character(len=*), intent(in)                  :: tag
  logical, intent(inout)                        :: endtag
  character(len=*), dimension(:,:), intent(in)  :: attribs
  integer, intent(in)                           :: noattribs
  character(len=*), dimension(:), intent(in)    :: data
  integer, intent(in)                           :: nodata
  integer, dimension(:), pointer                :: var
  logical, intent(inout)                        :: has_var

  integer,dimension(:), pointer                 :: newvar
  character(len=len(attribs(1,1)))              :: buffer
  integer                                       :: newsize
  integer                                       :: ierr

  newsize = size(var) + 1
  allocate( newvar(1:newsize) )
  newvar(1:newsize-1) = var
  nullify( var )
  var => newvar

  call read_xml_integer( info, tag, endtag, attribs, noattribs, data, nodata, &
       var(newsize), has_var )

end subroutine read_xml_integer_1dim

subroutine read_xml_real_1dim( info, tag, endtag, attribs, noattribs, data, nodata, &
     var, has_var )
  type(XML_PARSE), intent(inout)                :: info
  character(len=*), intent(in)                  :: tag
  logical, intent(inout)                        :: endtag
  character(len=*), dimension(:,:), intent(in)  :: attribs
  integer, intent(in)                           :: noattribs
  character(len=*), dimension(:), intent(in)    :: data
  integer, intent(in)                           :: nodata
  real, dimension(:), pointer                   :: var
  logical, intent(inout)                        :: has_var

  real, dimension(:), pointer                   :: newvar
  character(len=len(attribs(1,1)))              :: buffer
  integer                                       :: newsize
  integer                                       :: ierr

  newsize = size(var) + 1
  allocate( newvar(1:newsize) )
  newvar(1:newsize-1) = var
  nullify( var )
  var => newvar

  call read_xml_real( info, tag, endtag, attribs, noattribs, data, nodata, &
       var(newsize), has_var )

end subroutine read_xml_real_1dim

subroutine read_xml_double_1dim( info, tag, endtag, attribs, noattribs, data, nodata, &
     var, has_var )
  type(XML_PARSE), intent(inout)                :: info
  character(len=*), intent(in)                  :: tag
  logical, intent(inout)                        :: endtag
  character(len=*), dimension(:,:), intent(in)  :: attribs
  integer, intent(in)                           :: noattribs
  character(len=*), dimension(:), intent(in)    :: data
  integer, intent(in)                           :: nodata
  real(kind=kind(1.0d00)), dimension(:), pointer:: var
  logical, intent(inout)                        :: has_var

  real(kind=kind(1.0d00)), dimension(:), pointer:: newvar
  character(len=len(attribs(1,1)))              :: buffer
  integer                                       :: newsize
  integer                                       :: ierr

  newsize = size(var) + 1
  allocate( newvar(1:newsize) )
  newvar(1:newsize-1) = var
  nullify( var )
  var => newvar

  call read_xml_double( info, tag, endtag, attribs, noattribs, data, nodata, &
       var(newsize), has_var )

end subroutine read_xml_double_1dim

subroutine read_xml_logical_1dim( info, tag, endtag, attribs, noattribs, data, nodata, &
     var, has_var )
  type(XML_PARSE), intent(inout)                :: info
  character(len=*), intent(in)                  :: tag
  logical, intent(inout)                        :: endtag
  character(len=*), dimension(:,:), intent(in)  :: attribs
  integer, intent(in)                           :: noattribs
  character(len=*), dimension(:), intent(in)    :: data
  integer, intent(in)                           :: nodata
  logical, dimension(:), pointer                :: var
  logical, intent(inout)                        :: has_var

  logical, dimension(:), pointer                :: newvar
  character(len=len(attribs(1,1)))              :: buffer
  integer                                       :: newsize
  integer                                       :: ierr

  newsize = size(var) + 1
  allocate( newvar(1:newsize) )
  newvar(1:newsize-1) = var
  nullify( var )
  var => newvar

  call read_xml_logical( info, tag, endtag, attribs, noattribs, data, nodata, &
       var(newsize), has_var )

end subroutine read_xml_logical_1dim

subroutine read_xml_word_1dim( info, tag, endtag, attribs, noattribs, data, nodata, &
     var, has_var )
  type(XML_PARSE), intent(inout)                :: info
  character(len=*), intent(in)                  :: tag
  logical, intent(inout)                        :: endtag
  character(len=*), dimension(:,:), intent(in)  :: attribs
  integer, intent(in)                           :: noattribs
  character(len=*), dimension(:), intent(in)    :: data
  integer, intent(in)                           :: nodata
  character(len=*), dimension(:), pointer       :: var
  logical, intent(inout)                        :: has_var

  character(len=len(var)),dimension(:), pointer :: newvar
  character(len=len(attribs(1,1)))              :: buffer
  integer                                       :: newsize
  integer                                       :: ierr

  newsize = size(var) + 1
  allocate( newvar(1:newsize) )
  newvar(1:newsize-1) = var
  nullify( var )
  var => newvar

  call read_xml_word( info, tag, endtag, attribs, noattribs, data, nodata, &
       var(newsize), has_var )

end subroutine read_xml_word_1dim

subroutine read_xml_line_1dim( info, tag, endtag, attribs, noattribs, data, nodata, &
     var, has_var )
  type(XML_PARSE), intent(inout)                :: info
  character(len=*), intent(in)                  :: tag
  logical, intent(inout)                        :: endtag
  character(len=*), dimension(:,:), intent(in)  :: attribs
  integer, intent(in)                           :: noattribs
  character(len=*), dimension(:), intent(in)    :: data
  integer, intent(in)                           :: nodata
  character(len=*), dimension(:), pointer       :: var
  logical, intent(inout)                        :: has_var

  character(len=len(var)),dimension(:), pointer :: newvar
  character(len=len(attribs(1,1)))              :: buffer
  integer                                       :: newsize
  integer                                       :: ierr

  newsize = size(var) + 1
  allocate( newvar(1:newsize) )
  newvar(1:newsize-1) = var
  nullify( var )
  var => newvar

  call read_xml_line( info, tag, endtag, attribs, noattribs, data, nodata, &
       var(newsize), has_var )

end subroutine read_xml_line_1dim


end module read_xml_primitives

module ModXMLCantera
use READ_XML_PRIMITIVES
use XMLPARSE
USE ModDataStruct
implicit none
integer, private :: lurep_
logical, private :: strict_
real(8), parameter :: R_calMolK = 1.9872041d0;

contains

subroutine read_xml_file_cantera_M0(fname, M0, lurep, errout)
  character(len=*), intent(in)           :: fname
  integer, intent(in), optional          :: lurep
  logical, intent(out), optional         :: errout
  type(t_model0), pointer                :: M0
  

  real(rfreal),pointer, dimension(:)   :: tmp_o

  type(XML_PARSE)                        :: info
  logical                                :: error
  character(len=80)                      :: tag
  logical                                :: endtag
  character(len=80), dimension(1:2,1:20) :: attribs
  integer                                :: noattribs
  character(len=200), dimension(1:100)   :: data
  integer                                :: nodata
  logical                                :: has_A=.false.
  logical                                :: has_b=.false.
  logical                                :: has_E=.false.
  logical                                :: has_eps=.false.
  logical                                :: has_O=.false.
  logical                                :: has_threeBody=.false.,has_efficiency=.false.
  integer                                :: kreaction, kspecies, m, katt, k3body
  character(len=20)                      :: species
  character(len=10),dimension(:),pointer :: charArray
  real(rfreal), dimension(:), allocatable :: thirdBodyEff
  real(rfreal), dimension(:), allocatable :: order
  real(rfreal) :: Da, E, b
  logical :: valid_reaction
  real(rfreal) :: amount
  character(len=80) :: reaction_id

  nullify(charArray)
  nullify(tmp_o);allocate(tmp_o(0))

  allocate(thirdBodyEff(M0%numSpecies+1))
  allocate(order(M0%numSpecies+1))

  allocate(M0%Da(0))
  allocate(M0%E(0))
  allocate(M0%b(0))
  allocate(M0%is3BR(0))
  allocate(M0%thirdBodyEff(M0%numSpecies+1,0))
  allocate(M0%order(M0%numSpecies+1,0))

  call xml_open( info, fname, .true. )
  call xml_options( info, report_errors=.true. )
  lurep_ = 0
  if ( present(lurep) ) then
     lurep_ = lurep
     call xml_options( info, report_lun=lurep )
  endif
  strict_ = .false.
  error = .false.
  do
     attribs = ''
     call xml_get( info, tag, endtag, attribs, noattribs, data, nodata )
     if ( xml_error(info) ) then
        write(*,*) 'Error reading input file!'
        stop
     endif
     if ( endtag .and. noattribs .eq. 0 ) then
        if ( xml_ok(info) ) then
           cycle
        else
           exit
        endif
     endif
     if(trim(tag) == 'reaction') then

        has_A=.false.;has_b=.false.;has_E=.false.;has_eps=.false.
        has_threeBody=.false.;has_O=.false.
        valid_reaction = .true.
        reaction_id = ''

        do katt = 1,ubound(attribs,2)
           if (trim(attribs(1,katt)) == 'id') then
              reaction_id = attribs(2,katt)
           else if(trim(attribs(1,katt)) == 'type' .and. trim(attribs(2,katt)) == 'threeBody')  then
              has_threeBody=.true.
           end if
        enddo

        reactionLoop: do
           call xml_get( info, tag, endtag, attribs, noattribs, data, nodata )

           if(endtag) then
              if(trim(tag) == 'reaction') then
                 exit reactionLoop
              else
                 cycle reactionLoop
              endif
           endif

           select case( trim(tag) )
           case('A')
              call read_xml_double( &
                   info, tag, endtag, attribs, noattribs, data, nodata, &
                   Da, has_A )
           case('b')
              call read_xml_double( &
                   info, tag, endtag, attribs, noattribs, data, nodata, &
                   b, has_b )
           case('E')
              call read_xml_double( &
                   info, tag, endtag, attribs, noattribs, data, nodata, &
                   E, has_E )
           case('order')
              if(.not. endtag) then

                 if(trim(attribs(1,1)) == 'species') then
                    select case( trim(trim(attribs(2,1))) )
                    case('H2')
                       kspecies = M0%iH2
                    case('H')
                       kspecies = M0%iH
                    case('O2')
                       kspecies = M0%iO2
                    end select
                    if (kspecies > M0%numSpecies+1) valid_reaction = .false.
                    m=pointer_extend(tmp_o,kspecies,0d0)
                    call read_xml_double( &
                         info, tag, endtag, attribs, noattribs, data, nodata, &
                         tmp_o(kspecies), has_O )
                 endif
              endif
           case('efficiencies')
              if(has_threeBody .and. .not. endtag) then
                 call read_xml_word_array( &
                      info, tag, endtag, attribs, noattribs, data, nodata, &
                      charArray, has_efficiency )
                 thirdBodyEff = 1.d0
                 do m = 1, ubound(charArray,1)
                   call parseReactionSpecies(M0,charArray(m),kspecies,amount)
                   if (kspecies <= M0%numSpecies+1) then
                     thirdBodyEff(kspecies) = amount
                   else
                     valid_reaction = .false.
                     exit
                   end if
                 end do
              endif
           case('reactants')
              if(.not. endtag) then
                 call read_xml_word_array( &
                      info, tag, endtag, attribs, noattribs, data, nodata, &
                      charArray, has_efficiency )
                 order = 0.d0
                 do m = 1, ubound(charArray,1)
                   call parseReactionSpecies(M0,charArray(m),kspecies,amount)
                   if (kspecies <= M0%numSpecies+1) then
                     order(kspecies) = amount
                   else
                     valid_reaction = .false.
                     exit
                   end if
                 end do
              endif
           case('products')
              if(.not. endtag) then
                 call read_xml_word_array( &
                      info, tag, endtag, attribs, noattribs, data, nodata, &
                      charArray, has_efficiency )
                 do m = 1, ubound(charArray,1)
                   call parseReactionSpecies(M0,charArray(m),kspecies,amount)
                   if (kspecies > M0%numSpecies+1) then
                     valid_reaction = .false.
                     exit
                   end if
                 end do
              endif
           end select
           if ( .not. xml_ok(info) ) exit

        end do reactionLoop

        if(has_O) then
           kspecies = ubound(tmp_o,1);
           order(1:kspecies) = tmp_o(1:kspecies)
           deallocate(tmp_o);allocate(tmp_o(0))
        endif

        if (valid_reaction) then
          kreaction = pointer_increment(M0%da,0d0)
          M0%Da(kreaction) = Da
          kreaction = pointer_increment(M0%b,0d0)
          M0%b(kreaction) = b
          kreaction = pointer_increment(M0%E,0d0)
          M0%E(kreaction) = E
          kreaction = pointerL_increment(M0%is3BR,.false.)
          M0%is3BR(kreaction) = has_threeBody
          kreaction = pointer2_increment(M0%thirdBodyEff,M0%numSpecies+1,0d0)
          if (has_threeBody) then
            M0%thirdBodyEff(:,kreaction) = thirdBodyEff
          end if
          kreaction = pointer2_increment(M0%order,M0%numSpecies+1,0d0)
          M0%order(:,kreaction) = order
        else
          write (*,'(3a)') "Ignoring invalid reaction '", trim(reaction_id), "'."
        end if

     endif
     if(associated(charArray)) deallocate(charArray)
     if ( .not. xml_ok(info) ) exit
  end do

  call xml_close(info)

  if ( present(errout) ) errout = error

!unit conversion:: the code use the activation temperature
  allocate(M0%theta(size(M0%E)))
  M0%theta = M0%E/R_calMolK
  
end subroutine read_xml_file_cantera_M0

subroutine read_xml_file_cantera_M1(fname, M1, lurep, errout)
  character(len=*), intent(in)           :: fname
  integer, intent(in), optional          :: lurep
  logical, intent(out), optional         :: errout
  type(t_model1),pointer                 :: M1
  

  real(rfreal),pointer, dimension(:)   :: tmp_o

  type(XML_PARSE)                        :: info
  logical                                :: error
  character(len=80)                      :: tag
  logical                                :: endtag
  character(len=80), dimension(1:2,1:20) :: attribs
  integer                                :: noattribs
  character(len=200), dimension(1:100)   :: data
  integer                                :: nodata
  logical                                :: has_A=.false.
  logical                                :: has_b=.false.
  logical                                :: has_E=.false.
  logical                                :: has_eps=.false.
  logical                                :: has_O=.false.
  logical                                :: has_threeBody=.false.,has_efficiency=.false., has_fallOff=.false.
  integer                                :: kreaction, kspecies, m, katt, k3body, kpoly
  character(len=20)                      :: species
  character(len=10),dimension(:),pointer :: charArray
  real(8),pointer :: dblePtr(:)
  type(t_kF0)  :: k0Empty

  nullify(charArray)
  nullify(dblePtr)
  nullify(tmp_o);allocate(tmp_o(0))

  allocate(M1%Da(0))
  allocate(M1%E(0))
  allocate(M1%b(0))
  allocate(M1%is3BR(0))
  allocate(M1%isFO(0))
  allocate(M1%k0(0))

  ! populate empty k0
  ! this always gets added to the k0 array, even if the falloff is not specified
  ! by the xml input file
  k0Empty%E = 0
  k0Empty%b = 0
  k0Empty%Da = 0
  k0Empty%theta = 0
  allocate(k0Empty%FallOff(0))

  call xml_open( info, fname, .true. )
  call xml_options( info, report_errors=.true. )
  lurep_ = 0
  if ( present(lurep) ) then
     lurep_ = lurep
     call xml_options( info, report_lun=lurep )
  endif
  strict_ = .false.
  error = .false.
  M1%numSpecies = 0
  do
     call xml_get( info, tag, endtag, attribs, noattribs, data, nodata )
     if ( xml_error(info) ) then
        write(*,*) 'Error reading input file!'
        stop
     endif
     if ( endtag .and. noattribs .eq. 0 ) then
        if ( xml_ok(info) ) then
           cycle
        else
           exit
        endif
      endif
      
      if(trim(tag) == 'phase' .and. M1%numSpecies == 0) then

        
        phaseLoop: do
          call xml_get( info, tag, endtag, attribs, noattribs, data, nodata )
          
          if(endtag) then
            if(trim(tag) == 'phase') then
              exit phaseLoop
            else
              cycle phaseLoop
            endif
          endif
          
          if(trim(tag) == 'speciesArray' ) then
            
            call read_xml_word_array( &
                 info, tag, endtag, attribs, noattribs, data, nodata, &
                 M1%speciesArray, has_efficiency )
          endif
        enddo phaseLoop
        M1%numspecies = ubound(M1%speciesArray,1)-1  !leave-out the nitrogen
        nullify(M1%thirdBodyEff);allocate(M1%thirdBodyEff(M1%numSpecies+1,0))
        nullify(M1%order);allocate(M1%order(M1%numSpecies+1,0))
        nullify(M1%products);allocate(M1%products(M1%numSpecies+1,0))
        nullify(M1%polynomials);allocate(M1%polynomials(M1%numSpecies+1))
      endif

      
      if(trim(tag) == 'speciesData') then
      
        speciesDataLoop: do
          call xml_get( info, tag, endtag, attribs, noattribs, data, nodata )
          
          if(endtag) then
            if(trim(tag) == 'speciesData') then
              exit speciesDataLoop
            else
              cycle speciesDataLoop
            endif
          endif
          
          if(trim(tag) == 'species' .and. .not. endtag) then
            kspecies = 0
            do katt = 1,M1%numspecies+1
              if(trim(M1%speciesArray(katt)) == trim(attribs(2,1))) then
                kspecies = katt
                exit
              endif
            enddo
          endif
          
          if(trim(tag) == 'NASA' .and. .not. endtag) then
            kpoly = 1
            do katt = 1,ubound(attribs,2)
              if(trim(attribs(1,katt)) == 'Tmax' .and. trim(attribs(2,katt)) == '1000.0')  then
                kpoly = 1
                allocate(M1%polynomials(kspecies)%coeffs(7,2))
              elseif(trim(attribs(1,katt)) == 'Tmax') then
                kpoly = 2
              end if
            enddo
          endif
          if(trim(tag) == 'floatArray' .and. .not. endtag) then
            call read_xml_double_array(info, tag, endtag, attribs, noattribs, data, nodata, &
                 dblePtr, has_efficiency )
            M1%polynomials(kspecies)%coeffs(:,kpoly) = dblePtr(1:7)
          endif
        enddo speciesDataLoop
      endif
      
     if(trim(tag) == 'reaction') then
        has_A=.false.;has_b=.false.;has_E=.false.;has_eps=.false.
        has_threeBody=.false.;has_O=.false.
        has_fallOff=.false.
        kreaction = pointer2_increment(M1%thirdBodyEff,M1%numSpecies+1,0d0)
        kreaction = pointer2_increment(M1%order,M1%numSpecies+1,0d0)
        kreaction = pointer2_increment(M1%products,M1%numSpecies+1,0d0)
        kreaction = pointerL_increment(M1%is3BR,.false.)
        kreaction = pointerL_increment(M1%isFO,.false.)
        kreaction = pointerT_increment(M1%k0,k0Empty)
        kreaction = pointer_increment(M1%da,0d0)
        kreaction = pointer_increment(M1%b,0d0)
        kreaction = pointer_increment(M1%E,0d0)
        do katt = 1,ubound(attribs,2)
           if(trim(attribs(1,katt)) == 'type' .and. trim(attribs(2,katt)) == 'threeBody')  then
              has_threeBody=.true.
              M1%is3BR(kreaction) = .true.
              exit
            elseif(trim(attribs(1,katt)) == 'type' .and. trim(attribs(2,katt)) == 'falloff')  then
              has_fallOff=.true.
              has_threeBody=.true.
              M1%isFO(kreaction) = .true.
              M1%is3BR(kreaction) = .true.
              exit
           endif
        enddo
        
        reactionLoop: do
           call xml_get( info, tag, endtag, attribs, noattribs, data, nodata )
           
           if(endtag) then
              if(trim(tag) == 'reaction') then
                 exit reactionLoop
              else
                 cycle reactionLoop
              endif
           endif

           if(trim(tag) == 'Arrhenius') then
              ArrAttLoop: do katt = 1,ubound(attribs,2)
                 if(trim(attribs(1,katt)) == 'name' .and. trim(attribs(2,katt)) == 'k0')  then
                    k0Loop: do
                       call xml_get( info, tag, endtag, attribs, noattribs, data, nodata )
                       if(endtag) then
                          if(trim(tag) == 'Arrhenius') then
                             exit ArrAttLoop
                          else
                             cycle k0Loop
                          endif
                       endif
                       select case( trim(tag) )
                       case('A')
                          call read_xml_double( &
                               info, tag, endtag, attribs, noattribs, data, nodata, &
                               M1%k0(kreaction)%Da, has_A )
                       case('b')
                          call read_xml_double( &
                               info, tag, endtag, attribs, noattribs, data, nodata, &
                               M1%k0(kreaction)%b, has_b )
                       case('E')
                          call read_xml_double( &
                               info, tag, endtag, attribs, noattribs, data, nodata, &
                               M1%k0(kreaction)%E, has_E )
                          if(trim(attribs(1,1)) == 'units' .and. trim(attribs(2,1)) == 'kJ/mol')  M1%k0(kreaction)%E =  M1%k0(kreaction)%E*1d3/4.184d0
                       end select 
                    enddo k0Loop
                 endif
              enddo ArrAttLoop
           endif

           
           select case( trim(tag) )
           case('A')
              call read_xml_double( &
                   info, tag, endtag, attribs, noattribs, data, nodata, &
                   M1%Da(kreaction), has_A )
           case('b')
              call read_xml_double( &
                   info, tag, endtag, attribs, noattribs, data, nodata, &
                   M1%b(kreaction), has_b )
           case('E')
              call read_xml_double( &
                   info, tag, endtag, attribs, noattribs, data, nodata, &
                   M1%E(kreaction), has_E )
              if(trim(attribs(1,1)) == 'units' .and. trim(attribs(2,1)) == 'kJ/mol')  M1%E(kreaction) =  M1%E(kreaction)*1d3/4.184d0
           case('order')
              if(.not. endtag) then

                 if(trim(attribs(1,1)) == 'species') then
                    select case( trim(trim(attribs(2,1))) )
                    case('H2')
                       kspecies = M1%iH2
                    case('H')
                       kspecies = M1%iH
                    case('O2')
                       kspecies = M1%iO2
                    end select
                    m=pointer_extend(tmp_o,kspecies,0d0)
                    call read_xml_double( &
                         info, tag, endtag, attribs, noattribs, data, nodata, &
                         tmp_o(kspecies), has_O )
                 endif
              endif
           case('efficiencies')
              if(has_threeBody .and. .not. endtag) then
                 call read_xml_word_array( &
                      info, tag, endtag, attribs, noattribs, data, nodata, &
                      charArray, has_efficiency )
                 call fillSpeciesBuffer(M1,charArray,M1%thirdBodyEff(:,kreaction),1d0, M1%speciesArray)
              endif
           case('reactants')
              if(.not. endtag) then
                 call read_xml_word_array( &
                      info, tag, endtag, attribs, noattribs, data, nodata, &
                      charArray, has_efficiency )
                 call fillSpeciesBuffer(M1,charArray,M1%order(:,kreaction),0d0, M1%speciesArray)
              endif
           case('products')
              if(.not. endtag) then
                 call read_xml_word_array( &
                      info, tag, endtag, attribs, noattribs, data, nodata, &
                      charArray, has_efficiency )
                 call fillSpeciesBuffer(M1,charArray,M1%products(:,kreaction),0d0, M1%speciesArray)
              endif

           case ('falloff') 
              do katt = 1,ubound(attribs,2)
                 if(trim(attribs(1,katt)) == 'type' .and. trim(attribs(2,katt)) == 'Troe')  then
                    if(.not. endtag) then
                       nullify(M1%k0(kreaction)%fallOff)
                       Allocate(M1%k0(kreaction)%fallOff(4))
                       call read_xml_double_array( &
                            info, tag, endtag, attribs, noattribs, data, nodata, &
                            M1%k0(kreaction)%fallOff, has_efficiency )
                    endif
                 endif
              enddo
           end select
           if ( .not. xml_ok(info) ) exit
        end do reactionLoop

     endif
     if(associated(charArray)) nullify(charArray)
     if(has_O) then
        kspecies = ubound(tmp_o,1);
        M1%order(1:kspecies,kreaction) = tmp_o(1:kspecies)
        nullify(tmp_o);allocate(tmp_o(0))
     endif
     if ( .not. xml_ok(info) ) exit
  end do

  call xml_close(info)

  if ( present(errout) ) errout = error

!unit conversion:: the code use the activation temperature
  allocate(M1%theta(size(M1%E)))
  M1%theta = M1%E/R_calMolK   !E should be in cal/mol
  M1%numReactions = kreaction
  
  do katt = 1, ubound(M1%k0, 1);
    if(associated(M1%k0(katt)%fallOff)) then
      M1%k0(katt)%theta = M1%k0(katt)%E / R_calMolK
    endif
  enddo

end subroutine read_xml_file_cantera_M1

function pointer_increment(ptr,val) result(k)
  real(rfreal),pointer   :: ptr(:)
  real(rfreal) :: val
  real(rfreal),allocatable   :: tmp(:)
  integer::k

  k = ubound(ptr,1)
  allocate(tmp(k))
  tmp(1:k) = ptr(1:k)
  allocate(ptr(k+1))
  ptr(1:k) = tmp(1:k)
  k = ubound(ptr,1)
  ptr(k) = val
  deallocate(tmp)

end function pointer_increment

function pointerT_increment(ptr,val) result(k)
  type(t_kF0),pointer   :: ptr(:)
  type(t_kF0) :: val
  type(t_kF0),allocatable   :: tmp(:)
  integer::k

  k = ubound(ptr,1)
  allocate(tmp(k))
  tmp(1:k) = ptr(1:k)
  allocate(ptr(k+1))
  ptr(1:k) = tmp(1:k)
  k = ubound(ptr,1)
  ptr(k) = val
  deallocate(tmp)

end function pointerT_increment

function pointerL_increment(ptr,val) result(k)
  logical,pointer   :: ptr(:)
  logical :: val
  logical,allocatable   :: tmp(:)
  integer::k

  k = ubound(ptr,1)
  allocate(tmp(k))
  tmp(1:k) = ptr(1:k)
  allocate(ptr(k+1))
  ptr(1:k) = tmp(1:k)
  k = ubound(ptr,1)
  ptr(k) = val
  deallocate(tmp)

end function pointerL_increment

function pointer_extend(ptr,n,val) result(k)
  real(rfreal),pointer   :: ptr(:)
  real(rfreal) :: val
  integer::n
  real(rfreal),allocatable   :: tmp(:)
  integer::k

  k = ubound(ptr,1)
  if(n>k) then
     allocate(tmp(k))
     tmp(1:k) = ptr(1:k)
     allocate(ptr(n))
     ptr(1:k) = tmp(1:k)
     ptr(k+1:n) = val
     k = ubound(ptr,1)
     deallocate(tmp)
  else
     ptr(n) = val
  endif
end function pointer_extend

function pointer2_increment(ptr,n,val) result(k)
  real(rfreal),pointer   :: ptr(:,:)
  real(rfreal) :: val
  integer::n
  real(rfreal),allocatable   :: tmp(:,:)
  integer::k

  k = ubound(ptr,2)
  if(k>0) then
     allocate(tmp(n,k))
     tmp(1:n,1:k) = ptr(1:n,1:k)
  end if
  allocate(ptr(n,k+1))
  if(k>0)ptr(1:n,1:k) = tmp(1:n,1:k)
  k = ubound(ptr,2)
  ptr(1:n,k) = val
  if(allocated(tmp)) deallocate(tmp)

end function pointer2_increment

subroutine parseReactionSpecies(M0,species_string,kspecies,amount)
  type(t_model0),pointer :: M0
  character(len=10) :: species_string
  integer :: kspecies
  real(rfreal) :: amount

   if(trim(species_string(1:3)) == "H2:") then
      kspecies = M0%iH2
      read(species_string(4:),*) amount
   endif
   if(trim(species_string(1:3)) == "O2:") then
      kspecies = M0%iO2
      read(species_string(4:),*) amount
   endif
   if(trim(species_string(1:2)) == "H:") then
      kspecies = M0%iH
      read(species_string(3:),*) amount
   endif
   if(trim(species_string(1:4)) == "H2O:") then
      kspecies = M0%iH2O
      read(species_string(5:),*) amount
   endif
end subroutine parseReactionSpecies

subroutine fillSpeciesBuffer(M1,charArray,ptr,default,speciesArray)
  type(t_model1),pointer :: M1
  real(rfreal)   :: ptr(:)
  character(len=10),dimension(:),pointer :: charArray
  character(len=10),dimension(:),pointer :: speciesArray
  real(rfreal) :: default
  integer :: l,m,n

  ptr(:)=default;
  do m =1,ubound(charArray,1)
     do n=1,ubound(speciesArray,1)
        l = min(len_trim(speciesArray(n)),len_trim(charArray(m))-1)
        if(trim(charArray(m)(1:l+1)) == trim(speciesArray(n))//':') then
           read(charArray(m)(l+2:),*) ptr(n)
        endif
     end do
  enddo
end subroutine fillSpeciesBuffer

end module ModXMLCantera
