! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
!-----------------------------------------------------------------------
!
! ModPETSc.f90
!
! - basic code for interfacing with PETSc
!
! Revision history
! - 11 Mar 2012 : CMO - initial code
!
! - Note: line numbers reported by Valgrind = # + 2856 after includes
!-----------------------------------------------------------------------
Module ModPETSc

#include "petsc_fortran.h"

  ! ... thermal solve variables
  Vec             :: xt,bt
  Mat             :: At
  KSP             :: kspt, subkspt
  PC              :: pct, subpct
  ! ... dynamic
  Mat             :: ThrmCap
  Vec             :: dxt

  ! ... structural solve variables
  Vec             :: xs,bs,rs,bc,bs_ext
  Mat             :: As
  KSP             :: ksps, subksps
  PC              :: pcs, subpcs
  ! ... dynamic
  Mat             :: Mass, ThermCap
  Vec             :: dxs, ddxs, dxsOld, ddxsOld, bsaux, bs_extOld, auxs

  PetscErrorCode  :: pierr
  PetscOffset     :: xxi

  ! ... when using ParMetis
  Vec             :: xtl, dxtl, xsl, dxsl, ddxsl,bs_extl
  IS              :: is_local, is_renum
  VecScatter      :: ctxt, ctxs

CONTAINS

  Subroutine InitializePETSc(region, ng)
    
    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    
    Implicit None

    Type(t_region), pointer :: region
    Integer :: ng
    
    ! ...local variables
    Type(t_sgrid), pointer :: TMgrid

    ! ... simplicity
    TMgrid => region%TMgrid(ng)

    PETSC_COMM_WORLD = TMgrid%Comm

    !Call PetscInitialize(PETSC_NULL_CHARACTER, pierr)
    Call PetscInitialize('PETScOptions.txt', pierr)
    
  end Subroutine InitializePETSc

  Subroutine RepartitionMatrix(region, ng, col_ptr, row_ptr, isVec, isgVec)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    
    Implicit None

    Type(t_region), pointer :: region
    Integer, pointer :: col_ptr(:), row_ptr(:),isVec(:), isgVec(:)
    Integer :: ng
    
    ! ...local variables
    Type(t_sgrid), pointer :: TMgrid
    Type(t_mixt_input), pointer :: input
    Integer :: TMRank
    Integer :: ln, GN, ND, i, j
    Integer, pointer :: nnz_ds(:), nnz_os(:)

#define IndAux(ib)  Indices(xxi+(ib))    
    ! ... PetSc variables
    Mat :: Adj
    MatPartitioning :: part
    PetscInt :: Indices(1)
!    MatPartitioningType :: parmetis
    IS :: is, isg, is2, isg2

    ! ... simplicity
    TMgrid => region%TMgrid(ng)
    input  => TMgrid%input
    TMRank =  region%TMRank
    ln     =  TMgrid%nPtsOwned
    GN     =  TMgrid%nPtsGlb
    ND     =  TMgrid%ND

    allocate(isVec(GN),isgVec(GN))

    ! ... Create adjacency matrix
    Call MatCreateMPIAdj(TMgrid%Comm, ln, GN, row_ptr, col_ptr, PETSC_NULL, Adj, pierr)
    Call MatPartitioningCreate(TMgrid%Comm, part, pierr)
    Call MatPartitioningSetAdjacency(part, Adj, pierr)
    Call MatPartitioningSetType(part, 'parmetis', pierr)
    Call MatPartitioningApply(part, is, pierr)
    Call MatPartitioningDestroy(part, pierr)
    call MatDestroy(Adj, pierr)
    Call ISPartitioningToNumbering(is,isg, pierr)
    Call ISAllGather(is,is2,pierr)
    Call ISAllGather(isg,isg2,pierr)

    !if(TMRank == 0) then
    !   Call ISView(is2,PETSC_VIEWER_STDOUT_SELF,pierr)
    !   Call ISView(isg2,PETSC_VIEWER_STDOUT_SELF, pierr)
    !end if
    
    Call ISGetIndices(is2,Indices,xxi,pierr)
!      print*,TMRank,' is'
      do j = 1, GN
         isVec(j) = IndAux(j)
!         print*,j,isVec(j)
      end do
     Call ISRestoreIndices(is2,Indices,xxi,pierr)
    Call ISGetIndices(isg2,Indices,xxi,pierr)
!     print*,TMrank,' isg'
    do j = 1, GN
       isgVec(j) = IndAux(j)
       !        print*,'isg',TMrank,j,isgVec(j)
     end do
     Call ISRestoreIndices(isg2,Indices,xxi, pierr)

     Call ISDestroy(is, pierr)
     Call ISDestroy(isg, pierr)
     Call ISDestroy(is2, pierr)
     Call ISDestroy(isg2, pierr)

  end Subroutine RepartitionMatrix
    

  Subroutine InitializePETScArrays(region, ng, nnz_d, nnz_o)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    
    implicit none
    
    Type(t_region), pointer :: region
    Integer, pointer :: nnz_d(:), nnz_o(:)
    Integer :: ng
    
    ! ...local variables
    Type(t_sgrid), pointer :: TMgrid
    Type(t_mixt_input), pointer :: input
    Integer :: TMRank
    Integer :: ln, GN, ND, i, j
    Integer, pointer :: nnz_ds(:), nnz_os(:)
#define IndAux(ib)  Index1(xxi+(ib))    
    ! ... PetSc variables
    PetscInt, pointer :: indices(:)
    PetscInt :: Index1(1)

    ! ... simplicity
    TMgrid => region%TMgrid(ng)
    input  => TMgrid%input
    TMRank =  region%TMRank
    ln     =  TMgrid%nPtsOwnedRenum
    GN     =  TMgrid%nPtsGlb
    ND     =  TMgrid%ND
    if(TMRank == 0) write(*,'(A)') 'RFLOCM: Initializing'
    if(input%TMSolve == 1 .OR. input%TMSolve == 3) then
       if(TMRank == 0) write(*,'(A)') '          - thermal arrays'
       ! ... initialize the A matrix
!       do i = 0, input%numTMproc
!          if(TMRank == i) then
!             print*,TMRank,':',(nnz_d(j),j=1,size(nnz_d)),';',(nnz_o(j),j=1,size(nnz_d))
!          end if
!          call MPI_BARRIER(PETSC_COMM_WORLD,pierr)
!       end do
       
#ifdef OLD_PETSC
       Call MatCreateMPIAIJ(PETSC_COMM_WORLD, ln, ln, GN, GN, &
            PETSC_DECIDE, nnz_d, PETSC_DECIDE, nnz_o, At, pierr)
#else
       Call MatCreateAIJ(PETSC_COMM_WORLD, ln, ln, GN, GN, &
            PETSC_DECIDE, nnz_d, PETSC_DECIDE, nnz_o, At, pierr)
#endif
       ! ... initialize the solution vector
       Call VecCreateMPI(PETSC_COMM_WORLD, ln, GN, xt, pierr)
       ! ... initialize the RHS
       Call VecCreateMPI(PETSC_COMM_WORLD, ln, GN, bt, pierr)

       if(input%ThermalTimeScheme == DYNAMIC_SOLN) then
          
          ! ... initialize the thermal capacitance vector
#ifdef OLD_PETSC
          Call MatCreateMPIAIJ(PETSC_COMM_WORLD, ln, ln, GN, GN, &
               PETSC_DECIDE, nnz_d, PETSC_DECIDE, nnz_o, ThrmCap, pierr)
#else
          Call MatCreateAIJ(PETSC_COMM_WORLD, ln, ln, GN, GN, &
               PETSC_DECIDE, nnz_d, PETSC_DECIDE, nnz_o, ThrmCap, pierr)
#endif
          ! ... initialize the themperature rate vector
          Call VecCreateMPI(PETSC_COMM_WORLD, ln, GN, dxt, pierr)

       end if

    end if

    if(input%TMSolve == 2 .OR. input%TMSolve == 3) then
       if(TMRank == 0) write(*,'(A)') '          - structural arrays'
       allocate(nnz_ds(ND*ln),nnz_os(ND*ln))
       do j = 1, ln
          do i = 1, ND
             nnz_ds((j-1)*ND+i) = ND*nnz_d(j)
             nnz_os((j-1)*ND+i) = ND*nnz_o(j)
          end do
       end do

!       do i = 0, input%numTMproc
!          if(TMRank == i) then
!             print*,TMRank,':',(nnz_ds(j),j=1,size(nnz_ds)),';',(nnz_os(j),j=1,size(nnz_ds))
!          end if
!          call MPI_BARRIER(PETSC_COMM_WORLD,pierr)
!       end do
          
       ! ... initialize the A matrix
#ifdef OLD_PETSC
       Call MatCreateMPIAIJ(PETSC_COMM_WORLD, ND*ln, ND*ln, ND*GN, ND*GN, &
            PETSC_DECIDE, nnz_ds, PETSC_DECIDE, nnz_os, As, pierr)
#else
       Call MatCreateAIJ(PETSC_COMM_WORLD, ND*ln, ND*ln, ND*GN, ND*GN, &
            PETSC_DECIDE, nnz_ds, PETSC_DECIDE, nnz_os, As, pierr)
#endif

       ! ... initialize the solution vector
       Call VecCreateMPI(PETSC_COMM_WORLD, ND*ln, ND*GN, xs, pierr)
       Call VecSet(xs,0.0,pierr)
       ! ... initialize the RHS
       Call VecCreateMPI(PETSC_COMM_WORLD, ND*ln, ND*GN, bs, pierr)
       Call VecSet(bs,0.0,pierr)
       ! ... initialize the external load vector
       Call VecCreateMPI(PETSC_COMM_WORLD, ND*ln, ND*GN, bs_ext, pierr)
       Call VecSet(bs_ext,0.0,pierr)
       ! ... intialize the residual vector
       Call VecCreateMPI(PETSC_COMM_WORLD, ND*ln, ND*GN, rs, pierr)
       Call VecSet(rs,0.0,pierr)
       ! ... intialize the bc vector
       Call VecCreateMPI(PETSC_COMM_WORLD, ND*ln, ND*GN, bc, pierr)
       Call VecSet(bc,0.0,pierr)

       if(input%StructuralTimeScheme == DYNAMIC_SOLN) then

          ! ... initialize the mass matrix
#ifdef OLD_PETSC
          Call MatCreateMPIAIJ(PETSC_COMM_WORLD, ND*ln, ND*ln, ND*GN, ND*GN, &
               PETSC_DECIDE, nnz_ds, PETSC_DECIDE, nnz_os, Mass, pierr)
#else
          Call MatCreateAIJ(PETSC_COMM_WORLD, ND*ln, ND*ln, ND*GN, ND*GN, &
               PETSC_DECIDE, nnz_ds, PETSC_DECIDE, nnz_os, Mass, pierr)
#endif
          
          ! ... initialize the velocity vector
          Call VecCreateMPI(PETSC_COMM_WORLD, ND*ln, ND*GN, dxs, pierr)
          Call VecSet(dxs,0.0,pierr)
          ! ... initialize the acceleration vector
          Call VecCreateMPI(PETSC_COMM_WORLD, ND*ln, ND*GN, ddxs, pierr)
          Call VecSet(ddxs,0.0,pierr)
          ! ... initialize the old velocity vector
          Call VecCreateMPI(PETSC_COMM_WORLD, ND*ln, ND*GN, dxsOld, pierr)
          Call VecSet(dxsOld,0.0,pierr)
          ! ... initialize the old acceleration vector
          Call VecCreateMPI(PETSC_COMM_WORLD, ND*ln, ND*GN, ddxsOld, pierr)
          Call VecSet(ddxsOld,0.0,pierr)
          ! ... initialize the auxiliary rhs vector (needed for matrix-vector operations)
          Call VecCreateMPI(PETSC_COMM_WORLD, ND*ln, ND*GN, bsaux, pierr)
          Call VecSet(bsaux,0.0,pierr)
          ! ... initialize the old external load vector
          Call VecCreateMPI(PETSC_COMM_WORLD, ND*ln, ND*GN, bs_extOld, pierr)
          Call VecSet(bs_extOld,0.0,pierr)
          ! ... initialize the auxiliary vector
          Call VecCreateMPI(PETSC_COMM_WORLD, ND*ln, ND*GN, auxs, pierr)
          Call VecSet(auxs,0.0,pierr)

       end if
       
       deallocate(nnz_ds,nnz_os)
    
    end if

    ! ... need to create scatters for ParMetis repartitioned problems
    if(input%UseParMetis == TRUE) then
       if(input%TMSolve == 1 .OR. input%TMSolve == 3) then
          ! ... create scatter for thermal vectors
          Allocate(indices(TMgrid%nPtsOwned))
          do i = 1, TMgrid%nPtsOwned
             indices(i) = TMgrid%L2GRenum(TMgrid%G2L(i+TMgrid%nStart-1))-1
!             print*,i,indices(i),i,TMgrid%G2L(i+TMgrid%nStart-1)
          end do

          
          ! ... initialize the local solution vector
          Call VecCreateSeq(PETSC_COMM_SELF, TMgrid%nPtsOwned, xtl, pierr)

          ! ... initialize vectors so that PetSc doesn't complain
          call VecSet(xt, 0.0, pierr)
          call VecSet(xtl, 0.0, pierr)

         if(input%ThermalTimeScheme == DYNAMIC_SOLN) then
             Call VecCreateSeq(PETSC_COMM_SELF, ND*TMgrid%nPtsOwned, dxtl, pierr)
          end if

          Call ISCreateGeneral(PETSC_COMM_WORLD, TMgrid%nPtsOwned, indices, PETSC_COPY_VALUES, is_renum, pierr)
          Call ISCreateStride(PETSC_COMM_SELF, TMgrid%nPtsOwned, 0, 1, is_local, pierr)
!          Call ISGetIndices(is_renum,Index1,xxi,pierr)
          !      print*,TMRank,' is'
!          do i = 1, TMgrid%nPtsOwned
!            print*,'is_renum',region%TMRank,i,IndAux(i)
!             !         print*,j,isVec(j)
!          end do

!          Call ISGetIndices(is_local,Index1,xxi,pierr)
          !      print*,TMRank,' is'
!          do i = 1, TMgrid%nPtsOwned
!            print*,'is_local',region%TMRank,i,IndAux(i)
!             !         print*,j,isVec(j)
!          end do
          

          Call VecScatterCreate(xt,is_renum,xtl,is_local,ctxt, pierr)
       
          Call ISDestroy(is_renum, pierr)
          Call ISDestroy(is_local, pierr)
          
          Deallocate(indices)
       endif
       
       if(input%TMSolve == 2 .OR. input%TMSolve == 3) then
          ! ... create scatter for structural vectors
          Allocate(indices(ND*TMgrid%nPtsOwned))
          do i = 1, TMgrid%nPtsOwned
             do j = 1, ND
!                indices(i) = TMgrid%L2GRenum(TMgrid%G2L(i+TMgrid%nStart-1))-1
                indices((i-1)*ND + j) = (TMgrid%L2GRenum(TMgrid%G2L(i+TMgrid%nStart-1))-1)*ND + j - 1
!                print*,'i,j,indices((i-1)*ND + j)',region%TMRank,i,j,indices((i-1)*ND + j)
             end do
          end do
          ! ... initialize the local solution vector
          Call VecCreateSeq(PETSC_COMM_SELF, ND*TMgrid%nPtsOwned, xsl, pierr)

          ! ... initialize vectors so that PetSc doesn't complain
          call VecSet(xs, 0.0, pierr)
          call VecSet(xsl, 0.0, pierr)

          if(input%StructuralTimeScheme == DYNAMIC_SOLN) then
             Call VecCreateSeq(PETSC_COMM_SELF, ND*TMgrid%nPtsOwned, dxsl, pierr)
             Call VecCreateSeq(PETSC_COMM_SELF, ND*TMgrid%nPtsOwned, ddxsl, pierr)
             Call VecCreateSeq(PETSC_COMM_SELF, ND*TMgrid%nPtsOwned, bs_extl, pierr)
          end if

          Call ISCreateGeneral(PETSC_COMM_WORLD, ND*TMgrid%nPtsOwned, indices, PETSC_COPY_VALUES, is_renum, pierr)
          Call ISCreateStride(PETSC_COMM_SELF, ND*TMgrid%nPtsOwned, 0, 1, is_local, pierr)
          
          !Call ISView(is_renum,PETSC_VIEWER_STDOUT_WORLD,pierr)
          !Call ISView(is_local,PETSC_VIEWER_STDOUT_SELF,pierr)

          Call VecScatterCreate(xs,is_renum, xsl,is_local,ctxs, pierr)
       
          Call ISDestroy(is_renum, pierr)
          Call ISDestroy(is_local, pierr)
          
          Deallocate(indices)
          
       end if
    end if
       

  end Subroutine InitializePETScArrays

  Subroutine InsertKt(region, ng, le, kt, NdsPerElem)
 
    
    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    
    implicit none
    
    Type(t_region), pointer :: region
    Real(rfreal), pointer :: kt(:,:), ktemp(:,:)
    Integer :: ng, le, NdsPerElem
    
    ! ...local variables
    Type(t_sgrid), pointer :: TMgrid
    Type(t_smixt), pointer :: TMstate
    Type(t_mixt_input), pointer :: input
    Integer, pointer :: LM(:,:), L2G(:)
    Integer :: TMRank, i, j, ci, cj
    
    ! ... Petsc Stuff
    PetscInt, pointer :: RowNColNums(:)
    PetscScalar, pointer :: Values(:)
    PetscInt :: m

    ! ... simplicity
    TMgrid => region%TMgrid(ng)
    TMstate => region%TMstate(ng)
    input  => TMgrid%input
    TMRank =  region%TMRank
    LM     => TMgrid%LM
    L2G    => TMgrid%L2G
    m      =  NdsPerElem
    
    Allocate(Values(NdsPerElem*NdsPerElem), RowNColNums(NdsPerElem))
    Allocate(ktemp(NdsPerElem,NdsPerElem))
    
      do i = 1, NdsPerElem
         ! ... adjust for c++ counting
         if(input%UseParMetis == FALSE) then
            RowNColNums(i) = L2G(LM(i,le)) - 1
         elseif(input%UseParMetis == TRUE) then
            RowNColNums(i) = TMgrid%L2GRenum(LM(i,le)) - 1
         end if
         do j = 1, NdsPerElem
            ! ... row major counting
            Values((i-1)*NdsPerElem + j) = kt(i,j)
            !          ktemp(L2G(LM(i,le)),L2G(LM(j,le))) = kt(i,j)
         end do
      end do

!    do i = 1, NdsPerElem
!       write(*,'(8(x,D10.4))') (ktemp(i,:))
!    end do
    
    
    ! ... add to At
    Call MatSetValues(At,m,RowNColNums,m,RowNColNums,Values,ADD_VALUES,pierr)

    deallocate(RowNColNums, Values, ktemp)

  end Subroutine InsertKt

  Subroutine InsertRt(region, npatch, NdsPerFace, le, rt)

    
    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    
    implicit none
    
    Type(t_region), pointer :: region
    Real(rfreal), pointer :: rt(:)
    Integer :: le, npatch, NdsPerFace
    
    ! ...local variables
    Type(t_sgrid), pointer :: TMgrid
    Type(t_spatch), pointer :: TMpatch
    Type(t_mixt_input), pointer :: input
    Integer, pointer :: LM2D(:,:), L2G(:)
    Integer :: TMRank, i, j, ci, cj, ng
    
    ! ... Petsc Stuff
    PetscInt, pointer :: RowNColNums(:)
    PetscScalar, pointer :: Values(:)
    PetscInt :: m

    ! ... simplicity
    TMpatch    => region%TMpatch(npatch)
    ng         =  TMpatch%GridID
    TMgrid     => region%TMgrid(ng)
    input      => TMgrid%input
    TMRank     =  region%TMRank
    LM2D       => TMpatch%LM2D
    L2G        => TMgrid%L2G
    m          =  NdsPerFace
    
    Allocate(Values(NdsPerFace), RowNColNums(NdsPerFace))

    do i = 1, NdsPerFace
       ! ... adjust for c++ counting

       if(input%UseParMetis == FALSE) then
          RowNColNums(i) = L2G(LM2D(i,le)) - 1
       elseif(input%UseParMetis == TRUE) then
          RowNColNums(i) = TMgrid%L2GRenum(LM2D(i,le)) - 1
       end if

!       RowNColNums(i) = L2G(LM2D(i,le)) - 1
       Values(i) = rt(i)
    end do

    ! ... add to bt
    Call VecSetValues(bt,m,RowNColNums,Values,ADD_VALUES,pierr)   
    
    deallocate(Values,RowNColNums)

  end Subroutine InsertRt

  Subroutine InsertCt(region, ng, le, ct, NdsPerElem)
 
    
    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    
    implicit none
    
    Type(t_region), pointer :: region
    Real(rfreal), pointer :: ct(:,:), ctemp(:,:)
    Integer :: ng, le, NdsPerElem
    
    ! ...local variables
    Type(t_sgrid), pointer :: TMgrid
    Type(t_smixt), pointer :: TMstate
    Type(t_mixt_input), pointer :: input
    Integer, pointer :: LM(:,:), L2G(:)
    Integer :: TMRank, i, j
   
    ! ... Petsc Stuff
    PetscInt, pointer :: RowNColNums(:)
    PetscScalar, pointer :: Values(:)
    PetscInt :: m

    ! ... simplicity
    TMgrid => region%TMgrid(ng)
    TMstate => region%TMstate(ng)
    input  => TMgrid%input
    TMRank =  region%TMRank
    LM     => TMgrid%LM
    L2G    => TMgrid%L2G
    m      =  NdsPerElem
    
    Allocate(Values(NdsPerElem*NdsPerElem), RowNColNums(NdsPerElem))
    Allocate(ctemp(NdsPerElem,NdsPerElem))
    
      do i = 1, NdsPerElem
         ! ... adjust for c++ counting
         if(input%UseParMetis == FALSE) then
            RowNColNums(i) = L2G(LM(i,le)) - 1
         elseif(input%UseParMetis == TRUE) then
            RowNColNums(i) = TMgrid%L2GRenum(LM(i,le)) - 1
         end if
!      RowNColNums(i) = L2G(LM(i,le)) - 1
       do j = 1, NdsPerElem
          ! ... row major counting
          Values((i-1)*NdsPerElem + j) = ct(i,j)
!          ctemp(L2G(LM(i,le)),L2G(LM(j,le))) = ct(i,j)
       end do
    end do

!    do i = 1, NdsPerElem
!       write(*,'(8(x,D10.4))') (ktemp(i,:))
!    end do
    
    
    ! ... add to Thermal capacitance
    Call MatSetValues(ThrmCap,m,RowNColNums,m,RowNColNums,Values,ADD_VALUES,pierr)

    deallocate(RowNColNums, Values, ctemp)

  end Subroutine InsertCt

  Subroutine InsertRt_int(region, ng, NdsPerElem, le, rt_int)

    
    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    
    implicit none
    
    Type(t_region), pointer :: region
    Real(rfreal), pointer :: rt_int(:)
    Integer :: le, ng, NdsPerElem
    
    ! ...local variables
    Type(t_sgrid), pointer :: TMgrid
    Type(t_spatch), pointer :: TMpatch
    Type(t_mixt_input), pointer :: input
    Integer, pointer :: LM(:,:), L2G(:)
    Integer :: TMRank, i, j
    
    ! ... Petsc Stuff
    PetscInt, pointer :: RowNColNums(:)
    PetscScalar, pointer :: Values(:)
    PetscInt :: m

    ! ... simplicity
    TMgrid     => region%TMgrid(ng)
    input      => TMgrid%input
    TMRank     =  region%TMRank
    LM         => TMgrid%LM
    L2G        => TMgrid%L2G
    m          =  NdsPerElem
    
    Allocate(Values(NdsPerElem), RowNColNums(NdsPerElem))

    do i = 1, NdsPerElem
       ! ... adjust for c++ counting
       if(input%UseParMetis == FALSE) then
          RowNColNums(i) = L2G(LM(i,le)) - 1
       elseif(input%UseParMetis == TRUE) then
          RowNColNums(i) = TMgrid%L2GRenum(LM(i,le)) - 1
       end if

!       RowNColNums(i) = L2G(LM(i,le)) - 1
       Values(i) = rt_int(i)
    end do

    ! ... add to bt
    Call VecSetValues(bt,m,RowNColNums,Values,ADD_VALUES,pierr)   
    
    deallocate(Values,RowNColNums)

  end Subroutine InsertRt_int

  Subroutine RemoveTempBCs(TMSolve, region, ng, ThrmCapMat)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    
    Implicit None

    Type(t_region), pointer :: region
    Integer :: TMSolve, ng
    Logical, optional :: ThrmCapMat

    ! ... local variables
    Type(t_smixt), pointer :: TMstate
    Type(t_sgrid), pointer :: TMgrid
    Integer :: TMRank, i, j, BCCount
    Integer, pointer :: G2L(:), L2GRenum(:)

    ! ... Petsc Stuff
    PetscInt, pointer :: RowNColNums(:)
    PetscScalar, pointer :: Values(:)
    PetscInt :: m

    ! ... simplicity
    TMgrid   => region%TMgrid(ng)
    TMstate  => region%TMstate(ng)
    L2GRenum => TMgrid%L2GRenum
    G2L      => TMgrid%G2L
    
    BCCount = TMstate%TempBCNum

    if(present(ThrmCapMat) .eqv. .false.) then
       if(BCCount > 0) then
          allocate(RowNColNums(BCCount),Values(BCCount))
          m = BCCount
          do i = 1, BCCount
             if(TMgrid%input%UseParMetis == FALSE) then
                RowNColNums(i) = TMstate%BCTempInd(i) - 1
             elseif(TMgrid%input%UseParMetis == TRUE) then
                RowNColNums(i) = L2GRenum(G2L(TMstate%BCTempInd(i))) - 1
             end if
             Values(i) = TMstate%BCTempVal(i)-TMstate%q(TMstate%BCTempInd(i),TMstate%nVars)
             !          print*,i,BCInd(i),BCVal(i)
          end do
       
          ! ... add known solution values to xt
          Call VecSetValues(xt,m,RowNColNums,Values,INSERT_VALUES,pierr)
          
       end if

       Call FinalVectorAssembly(TMSolve,1)
       
       if(BCCount > 0) then
          Call MatZeroRowsColumns(At,m,RowNColNums,PETSC_NULL,xt,bt,pierr)
          !    Call MatZeroRows(At,m,RowNColNums,1.0_rfreal,xt,bt,pierr)
          !Call MatView(At,PETSC_VIEWER_STDOUT_WORLD,pierr)          
          deallocate(RowNColNums,Values)
       else
          Call MatZeroRowsColumns(At,0,0,PETSC_NULL,xt,bt,pierr)
       end if
       

    elseif(present(ThrmCapMat) .and. ThrmCapMat .eqv. .true.) then
       if(BCCount > 0) then
          allocate(RowNColNums(BCCount),Values(BCCount))
          m = BCCount
          do i = 1, BCCount
             if(TMgrid%input%UseParMetis == FALSE) then
                RowNColNums(i) = TMstate%BCTempInd(i) - 1
             elseif(TMgrid%input%UseParMetis == TRUE) then
                RowNColNums(i) = L2GRenum(G2L(TMstate%BCTempInd(i))) - 1
             end if
             Values(i) = 0.0_rfreal
          end do
          
          ! ... add known solution values to dxt
          Call VecSetValues(dxt,m,RowNColNums,Values,INSERT_VALUES,pierr)
          
       end if
       
       Call FinalVectorAssembly(TMSolve,1,2)
       
       ! ... now remove the rows and columns
       if(BCCount > 0) then
          Call MatZeroRowsColumns(ThrmCap,m,RowNColNums,PETSC_NULL,dxt,bt,pierr)
          !    Call MatZeroRows(At,m,RowNColNums,1.0_rfreal,xt,bt,pierr)
          !Call MatView(ThrmCap,PETSC_VIEWER_STDOUT_WORLD,pierr)
          deallocate(RowNColNums,Values)
       else
          Call MatZeroRowsColumns(ThrmCap,0,0,PETSC_NULL,dxt,bt,pierr)
       end if
    end if

    !Call VecView(xt,PETSC_VIEWER_STDOUT_WORLD,pierr)
    !Call VecView(bt,PETSC_VIEWER_STDOUT_WORLD,pierr)
    !print*,'after remove temp'

  end Subroutine RemoveTempBCs

  Subroutine InsertRs(region, npatch, NdsPerFace, le, rs)

    
    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    
    implicit none
    
    Type(t_region), pointer :: region
    Real(rfreal), pointer :: rs(:)
    Integer :: le, npatch, NdsPerFace
    
    ! ...local variables
    Type(t_sgrid), pointer :: TMgrid
    Type(t_spatch), pointer :: TMpatch
    Type(t_mixt_input), pointer :: input
    Integer, pointer :: LM2D(:,:), L2G(:)
    Integer :: TMRank, i, j, l0, ND, dir, ng
    
    ! ... Petsc Stuff
    PetscInt, pointer :: RowNColNums(:)
    PetscScalar, pointer :: Values(:)
    PetscInt :: m

    ! ... simplicity
    TMpatch    => region%TMpatch(npatch)
    ng         =  TMpatch%GridID
    TMgrid     => region%TMgrid(ng)
    input      => TMgrid%input
    TMRank     =  region%TMRank
    LM2D       => TMpatch%LM2D
    L2G        => TMgrid%L2G
    ND         =  TMgrid%ND
    m          =  NdsPerFace*ND

    Allocate(Values(NdsPerFace*ND), RowNColNums(NdsPerFace*ND))
!         ! ... adjust for c++ counting
!          if(input%UseParMetis == FALSE) then
!             RowNColNums(i) = L2G(LM(i,le)) - 1
!          elseif(input%UseParMetis == TRUE) then
!             RowNColNums(i) = TMgrid%L2GRenum(LM(i,le)) - 1
!          end if
    do i = 1, NdsPerFace
       do dir = 1, ND
          l0 = (i-1)*ND + dir
        ! ... adjust for c++ counting
         if(input%UseParMetis == FALSE) then
            RowNColNums(l0) = (L2G(LM2D(i,le)) - 1)*ND + dir - 1  
         elseif(input%UseParMetis == TRUE) then
            RowNColNums(l0) = (TMgrid%L2GRenum(LM2D(i,le)) - 1)*ND + dir - 1  
         end if
          ! ... adjust for c++ counting
          
          Values(l0) = rs(l0) 
       end do
    end do

    

    ! ... add to bs
    Call VecSetValues(bs_ext,m,RowNColNums,Values,ADD_VALUES,pierr)   
    
    deallocate(Values,RowNColNums)

  end Subroutine InsertRs

  Subroutine InsertRs_tan(region, npatch, NdsPerFace, le, rs_tan)
 
    
    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    
    implicit none
    
    Type(t_region), pointer :: region
    Real(rfreal), pointer :: rs_tan(:,:)
    Integer :: npatch, le, NdsPerFace

        ! ...local variables
    Type(t_sgrid), pointer :: TMgrid
    Type(t_spatch), pointer :: TMpatch
    Type(t_mixt_input), pointer :: input
    Integer, pointer :: LM2D(:,:), L2G(:)
    Integer :: TMRank, i, j, l1, l2, ND, dir1, dir2, ng
    
    ! ... Petsc Stuff
    PetscInt, pointer :: RowNColNums(:)
    PetscScalar, pointer :: Values(:)
    PetscInt :: m

    ! ... simplicity
    TMpatch    => region%TMpatch(npatch)
    ng         =  TMpatch%GridID
    TMgrid     => region%TMgrid(ng)
    input      => TMgrid%input
    TMRank     =  region%TMRank
    LM2D       => TMpatch%LM2D
    L2G        => TMgrid%L2G
    ND         =  TMgrid%ND
    m          =  NdsPerFace*ND
    
    Allocate(Values(ND*ND*NdsPerFace*NdsPerFace), RowNColNums(ND*NdsPerFace))
    
    do i = 1, NdsPerFace
       do dir1 = 1, ND
          l1 = (i-1)*ND + dir1 
          ! ... adjust for c++ counting
         if(input%UseParMetis == FALSE) then
            RowNColNums(l1) = (L2G(LM2D(i,le)) - 1)*ND + dir1 - 1  
         elseif(input%UseParMetis == TRUE) then
            RowNColNums(l1) = (TMgrid%L2GRenum(LM2D(i,le)) - 1)*ND + dir1 - 1  
         end if

         
          do j = 1, NdsPerFace
             do dir2 = 1, ND
                l2 = (j-1)*ND + dir2
                ! ... row major counting
                Values((l1-1)*NdsPerFace*ND + l2) = rs_tan(l1,l2)
                !          ktemp(L2G(LM(i,le)),L2G(LM(j,le))) = kt(i,j)
             end do
          end do
       end do
    end do

!    do i = 1, NdsPerElem
!       write(*,'(8(x,D10.4))') (ktemp(i,:))
!    end do
    
    
    ! ... add to As
    Call MatSetValues(As,m,RowNColNums,m,RowNColNums,Values,ADD_VALUES,pierr)

    deallocate(RowNColNums, Values)

  end Subroutine InsertRs_tan

  Subroutine InsertKs(region, ng, le, ks, NdsPerElem) 
    
    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    
    implicit none
    
    Type(t_region), pointer :: region
    Real(rfreal), pointer :: ks(:,:), ktemp(:,:)
    Integer :: ng, le, NdsPerElem
    
    ! ...local variables
    Type(t_sgrid), pointer :: TMgrid
    Type(t_smixt), pointer :: TMstate
    Type(t_mixt_input), pointer :: input
    Integer, pointer :: LM(:,:), L2G(:)
    Integer :: TMRank, i, j, ci, cj, ND, dir1, dir2, l1, l2
    
    ! ... Petsc Stuff
    PetscInt, pointer :: RowNColNums(:)
    PetscScalar, pointer :: Values(:)
    PetscInt :: m

    ! ... simplicity
    TMgrid  => region%TMgrid(ng)
    TMstate => region%TMstate(ng)
    input   => TMgrid%input
    TMRank  =  region%TMRank
    LM      => TMgrid%LM
    L2G     => TMgrid%L2G
    ND      =  input%ND
    m       =  NdsPerElem*ND

    
    Allocate(Values(ND*ND*NdsPerElem*NdsPerElem), RowNColNums(ND*NdsPerElem))
    Allocate(ktemp(ND*NdsPerElem,ND*NdsPerElem))
    
    do i = 1, NdsPerElem
       do dir1 = 1, ND
          l1 = (i-1)*ND + dir1 
!          ! ... adjust for c++ counting
!          RowNColNums(l1) = (L2G(LM(i,le)) - 1)*ND + dir1 -1 
          ! ... adjust for c++ counting
         if(input%UseParMetis == FALSE) then
            RowNColNums(l1) = (L2G(LM(i,le)) - 1)*ND + dir1 - 1  
         elseif(input%UseParMetis == TRUE) then
            RowNColNums(l1) = (TMgrid%L2GRenum(LM(i,le)) - 1)*ND + dir1 - 1  
         end if

          do j = 1, NdsPerElem
             do dir2 = 1, ND
                l2 = (j-1)*ND + dir2
                ! ... row major counting
                Values((l1-1)*NdsPerElem*ND + l2) = ks(l1,l2)
                !          ktemp(L2G(LM(i,le)),L2G(LM(j,le))) = kt(i,j)
             end do
          end do
       end do
    end do

!    do i = 1, NdsPerElem
!       write(*,'(8(x,D10.4))') (ktemp(i,:))
!    end do
    
    
    ! ... add to As
    Call MatSetValues(As,m,RowNColNums,m,RowNColNums,Values,ADD_VALUES,pierr)

    deallocate(RowNColNums, Values, ktemp)

  end Subroutine InsertKs

  Subroutine InsertRs_int(region, ng, NdsPerElem, le, rs_int)

    
    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    
    implicit none
    
    Type(t_region), pointer :: region
    Real(rfreal), pointer :: rs_int(:)
    Integer :: le, ng, NdsPerElem
    
    ! ...local variables
    Type(t_sgrid), pointer :: TMgrid
    Type(t_spatch), pointer :: TMpatch
    Type(t_mixt_input), pointer :: input
    Integer, pointer :: LM(:,:), L2G(:)
    Integer :: TMRank, i, j, l0, ND, dir
    
    ! ... Petsc Stuff
    PetscInt, pointer :: RowNColNums(:)
    PetscScalar, pointer :: Values(:)
    PetscInt :: m

    ! ... simplicity
    TMgrid => region%TMgrid(ng)
    input  => TMgrid%input
    TMRank =  region%TMRank
    LM     => TMgrid%LM
    L2G    => TMgrid%L2G
    ND     =  input%ND
    m      =  NdsPerElem*ND
    
    Allocate(Values(NdsPerElem*ND), RowNColNums(NdsPerElem*ND))

    do i = 1, NdsPerElem
       do dir = 1, ND
          l0 = (i-1)*ND + dir
          ! ... adjust for c++ counting
          !RowNColNums(l0) = (L2G(LM(i,le)) - 1)*ND + dir - 1
          ! ... adjust for c++ counting
         if(input%UseParMetis == FALSE) then
            RowNColNums(l0) = (L2G(LM(i,le)) - 1)*ND + dir - 1  
         elseif(input%UseParMetis == TRUE) then
            RowNColNums(l0) = (TMgrid%L2GRenum(LM(i,le)) - 1)*ND + dir - 1  
         end if

          Values(l0) = rs_int(l0) 
       end do
    end do

    ! ... add to bt
    Call VecSetValues(bs,m,RowNColNums,Values,ADD_VALUES,pierr)   
    
    deallocate(Values,RowNColNums)

  end Subroutine InsertRs_int

  Subroutine InsertMass(region, ng, le, ms, NdsPerElem) 
    
    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    
    implicit none
    
    Type(t_region), pointer :: region
    Real(rfreal), pointer :: ms(:,:), ktemp(:,:)
    Integer :: ng, le, NdsPerElem
    
    ! ...local variables
    Type(t_sgrid), pointer :: TMgrid
    Type(t_smixt), pointer :: TMstate
    Type(t_mixt_input), pointer :: input
    Integer, pointer :: LM(:,:), L2G(:)
    Integer :: TMRank, i, j, ci, cj, ND, dir1, dir2, l1, l2
    
    ! ... Petsc Stuff
    PetscInt, pointer :: RowNColNums(:)
    PetscScalar, pointer :: Values(:)
    PetscInt :: m

    ! ... simplicity
    TMgrid  => region%TMgrid(ng)
    TMstate => region%TMstate(ng)
    input   => TMgrid%input
    TMRank  =  region%TMRank
    LM      => TMgrid%LM
    L2G     => TMgrid%L2G
    ND      =  input%ND
    m       =  NdsPerElem*ND

    
    Allocate(Values(ND*ND*NdsPerElem*NdsPerElem), RowNColNums(ND*NdsPerElem))
    Allocate(ktemp(ND*NdsPerElem,ND*NdsPerElem))
    
    do i = 1, NdsPerElem
       do dir1 = 1, ND
          l1 = (i-1)*ND + dir1 
          ! ... adjust for c++ counting
          !RowNColNums(l1) = (L2G(LM(i,le)) - 1)*ND + dir1 -1 
                    ! ... adjust for c++ counting
         if(input%UseParMetis == FALSE) then
            RowNColNums(l1) = (L2G(LM(i,le)) - 1)*ND + dir1 - 1  
         elseif(input%UseParMetis == TRUE) then
            RowNColNums(l1) = (TMgrid%L2GRenum(LM(i,le)) - 1)*ND + dir1 - 1  
         end if

          do j = 1, NdsPerElem
             do dir2 = 1, ND
                l2 = (j-1)*ND + dir2
                ! ... row major counting
                Values((l1-1)*NdsPerElem*ND + l2) = ms(l1,l2)
                !print*,Values((l1-1)*NdsPerElem*ND + l2),RowNColNums(l1)
                !          ktemp(L2G(LM(i,le)),L2G(LM(j,le))) = kt(i,j)
             end do
          end do
       end do
    end do

!    do i = 1, NdsPerElem
!       write(*,'(8(x,D10.4))') (ktemp(i,:))
!    end do
    
    
    ! ... add to Mass
    Call MatSetValues(Mass,m,RowNColNums,m,RowNColNums,Values,ADD_VALUES,pierr)

    deallocate(RowNColNums, Values, ktemp)

  end Subroutine InsertMass

  Subroutine RemoveDispBCs(TMSolve, region, ng, MassMat)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    
    implicit none
    
    Type(t_region), pointer :: region
    Integer :: ng
    Logical, optional :: MassMat

    Type(t_sgrid), pointer :: TMgrid
    Type(t_smixt), pointer :: TMstate
    Integer, pointer :: G2L(:), L2GRenum(:)
    Integer :: TMSolve

    ! ... PETSc variables
    Integer :: TMRank, i, j, BCCount, dir, ND
    
    ! ... Petsc Stuff
    PetscInt, pointer :: RowNColNums(:)
    PetscScalar, pointer :: Values(:)
    PetscInt :: m

    ! ... simplicity
    TMstate  => region%TMstate(ng)
    TMgrid   => region%TMgrid(ng)
    G2L      => TMgrid%G2L
    L2GRenum => TMgrid%L2GRenum
    BCCount  = TMstate%DispBCNum
    ND       = TMgrid%ND

!        allocate(RowNColNums(BCCount),Values(BCCount))
!        m = BCCount
!        do i = 1, BCCount
!           if(TMgrid%input%UseParMetis == FALSE) then
!              RowNColNums(i) = TMstate%BCTempInd(i) - 1
!           elseif(TMgrid%input%UseParMetis == TRUE) then
!              RowNColNums(i) = L2GRenum(G2L(TMstate%BCTempInd(i))) - 1
!           end if
!           Values(i) = TMstate%BCTempVal(i)
! !          print*,i,BCInd(i),BCVal(i)
!        end do

    if(BCCount > 0) then
       allocate(RowNColNums(BCCount),Values(BCCount))
       m = BCCount
       do i = 1, BCCount
          j = G2L((TMstate%BCDispInd(i)-1)/ND+1)
          dir = mod(TMstate%BCDispInd(i)-1,ND)+1
          if(TMgrid%input%UseParMetis == FALSE) then
             RowNColNums(i) = TMstate%BCDispInd(i) - 1
          elseif(TMgrid%input%UseParMetis == TRUE) then
             RowNColNums(i) = (L2GRenum(j)-1)*ND + dir - 1
          end if
          
          !          RowNColNums(i) = TMstate%BCDispInd(i) - 1
          Values(i) = TMstate%BCDispVal(i) - (TMstate%q(j,dir) - TMgrid%X(j,dir))
       end do
       
       
       ! ... add known solution values to xs
       Call VecSetValues(xs,m,RowNColNums,Values,INSERT_VALUES,pierr)
    end if
    !Call VecView(xs,PETSC_VIEWER_STDOUT_WORLD,pierr)
    Call FinalVectorAssembly(TMSolve,2,1)

!    Call VecView(bs,PETSC_VIEWER_STDOUT_WORLD,pierr)

!    Call MatView(As,PETSC_VIEWER_STDOUT_WORLD,pierr)


    ! ... now remove the rows and columns
       if(present(MassMat) .and. MassMat .eqv. .true.) then
          if(BCCount > 0) then
             Call MatZeroRowsColumns(Mass,m,RowNColNums,PETSC_NULL,xs,bs,pierr)

          
             deallocate(RowNColNums,Values)
          else
             Call MatZeroRowsColumns(Mass,0,0,PETSC_NULL,xs,bs,pierr)
          end if
          
       else
          if(BCCount > 0) then
             
             Call MatZeroRowsColumns(As,m,RowNColNums,PETSC_NULL,xs,bs,pierr)
!    Call MatZeroRows(At,m,RowNColNums,1.0_rfreal,xt,bt,pierr)
             
             deallocate(RowNColNums,Values)
          else
             Call MatZeroRowsColumns(As,0,0,PETSC_NULL,xs,bs,pierr)
          end if
       end if
    !print*,'here,after bcs'
    !Call MatView(As,PETSC_VIEWER_STDOUT_WORLD,pierr)
    !Call VecView(xs,PETSC_VIEWER_STDOUT_WORLD,pierr)
    !Call VecView(bs,PETSC_VIEWER_STDOUT_WORLD,pierr)


  end Subroutine RemoveDispBCs

  Subroutine FinalMatrixAssembly(TMSolve, Problem)

    USE ModMPI
    
    Implicit None
    
    Integer :: TMSolve, Problem
    
    ! ... Problem = 1, thermal
    ! ...         = 2, structural
    ! ...         = 3, structural, mass matrix
    ! ...         = 4, thermal, capacitance matrix

    if(Problem == 1) then
       if(TMSolve == 2) write(*,'(A)') 'RFLOCM: Error: trying to assemble thermal arrays in structural-only run'

       ! ... assemble At matrix
       Call MatAssemblyBegin(At,MAT_FINAL_ASSEMBLY, pierr)
       Call MatAssemblyEnd(At,MAT_FINAL_ASSEMBLY, pierr)

    elseif(Problem == 2) then
       if(TMSolve == 1) write(*,'(A)') 'RFLOCM: Error: trying to assemble structural arrays in thermal-only run'
       
       ! ... assemble As matrix
       Call MatAssemblyBegin(As, MAT_FINAL_ASSEMBLY, pierr)
       Call MatAssemblyEnd(As, MAT_FINAL_ASSEMBLY, pierr)
       !Call MatView(As,PETSC_VIEWER_STDOUT_WORLD,pierr)

    elseif(Problem == 3) then
       if(TMSolve == 1) write(*,'(A)') 'RFLOCM: Error: trying to assemble structural arrays in thermal-only run'
       ! ... assemble Mass matrix
       Call MatAssemblyBegin(Mass, MAT_FINAL_ASSEMBLY, pierr)
       Call MatAssemblyEnd(Mass, MAT_FINAL_ASSEMBLY, pierr)
    elseif(Problem == 4) then
       if(TMSolve == 2) write(*,'(A)') 'RFLOCM: Error: trying to assemble thermal arrays in structural-only run'
       ! ... assemble Mass matrix
       Call MatAssemblyBegin(ThrmCap, MAT_FINAL_ASSEMBLY, pierr)
       Call MatAssemblyEnd(ThrmCap, MAT_FINAL_ASSEMBLY, pierr)
    else
       write(*,'(A)') 'RFLOCM: Error: A matrix not assembled!!'
    end if
    
  end Subroutine FinalMatrixAssembly

  Subroutine FinalVectorAssembly(TMSolve, Problem, WhichVec)

    USE ModMPI
    
    Implicit None
    
    Integer :: TMSolve, Problem
    Integer, optional :: WhichVec
    
    ! ... Problem = 1, thermal
    ! ...         = 2, structural

    if(Problem == 1) then
       if(TMSolve == 2) write(*,'(A)') 'RFLOCM: Error: trying to assemble thermal vectors in structural-only run'

       ! ... assemble RHS vector
       Call VecAssemblyBegin(bt, pierr)
       Call VecAssemblyEnd(bt, pierr)

       ! ... assemble solution vector
       Call VecAssemblyBegin(xt, pierr)
       Call VecAssemblyEnd(xt, pierr)

       if(present(WhichVec) .and. (WhichVec == 2)) then
          Call VecAssemblyBegin(dxt, pierr)
          Call VecAssemblyEnd(dxt, pierr)
       end if

    elseif(Problem == 2) then
       if(TMSolve == 1) write(*,'(A)') 'RFLOCM: Error: trying to assemble structural vectors in thermal-only run'

       if(present(WhichVec) .and. (WhichVec == 2 .or. WhichVec == 3)) then
          ! ... assemble RHS vector
          call VecAssemblyBegin(bs, pierr)
          call VecAssemblyEnd(bs, pierr)
       end if
       
       if(present(WhichVec) .and. (WhichVec == 1 .or. WhichVec == 3)) then
          ! ... assemble solution vector
          Call VecAssemblyBegin(xs, pierr)
          Call VecAssemblyEnd(xs, pierr)
       end if

       if(present(WhichVec) .and. ( (WhichVec == 4) .or. (WhichVec == 5) )) then
          ! ... assemble external load vector 
          Call VecAssemblyBegin(bs_ext, pierr)
          Call VecAssemblyEnd(bs_ext, pierr)
          !Call VecView(bs_ext,PETSC_VIEWER_STDOUT_WORLD,pierr)
          if(WhichVec == 5) then
             ! ... this is the first iteration or a restart, need to record bs_extOld
             Call VecCopy(bs_ext,bs_extOld,pierr)
             !print*,'copied'
             !Call VecView(bs_extOld,PETSC_VIEWER_STDOUT_WORLD,pierr)
             
          end if

       end if

    else
       write(*,'(A)') 'RFLOCM: Error: A vectors not assembled!!'
    end if
    
  end Subroutine FinalVectorAssembly

  Subroutine ThermalSolve(region, ng)

    USE ModDataStruct
    USE ModMPI

    Implicit None

    Type(t_region), pointer :: region
    Integer :: ng

    ! ... local variables
    PetscReal :: rtol, atol, dtol
    PetscInt  :: mxit,nlocal, first, its
   
    rtol = region%input%PetscRtol
    atol = 1.0D-50
    dtol = 1.0D5
    mxit = 10000

    ! ... create linear solver
    Call KSPCreate(PETSC_COMM_WORLD, kspt, pierr)
    call KSPSetTolerances(kspt,rtol, atol, dtol,mxit, pierr)
    Call KSPSetOperators(kspt, At, At, SAME_NONZERO_PATTERN, pierr)
    Call KSPGetPC(kspt,pct,pierr)

    if(region%input%numTMproc > 1) then
       Call PCSetType(pct,PCBJACOBI,pierr)
       Call KSPSetUp(kspt,pierr)
       Call PCBJacobiGetSubKSP(pct,nlocal, first,subkspt,pierr)
       Call KSPGMRESSetRestart(kspt, 30, pierr)
       Call KSPGMRESSetRestart(subkspt, 30, pierr)
       Call KSPGetPC(subkspt,subpct,pierr)
       Call PCSetType(subpct, PCILU, pierr)
       Call PCFactorSetLevels(subpct,0,pierr)
       Call PCFactorSetMatOrderingType(subpct,MATORDERINGRCM,pierr)
       Call PCFactorSetShiftType(subpct,MAT_SHIFT_NONZERO,pierr)
    else
       Call KSPGetPC(kspt,pcs,pierr)
       Call PCSetType(pct, PCLU, pierr)
       Call PCFactorSetLevels(pct,3,pierr)
       Call PCFactorSetMatOrderingType(pct,MATORDERINGRCM,pierr)
       Call PCFactorSetShiftType(pct,MAT_SHIFT_NONZERO,pierr)
    end if
    Call KSPSolve(kspt,bt,xt, pierr)
    Call KSPGetIterationNumber(kspt,its,pierr)
    if(its > 1000) write(*,'(A,I9,A)') 'RFLOCM: WARNING: took ',its,' iterations!'
    Call KSPDestroy(kspt,pierr)
     ! ... create linear solver
!    Call KSPCreate(PETSC_COMM_WORLD, kspt, pierr)
!    call KSPSetTolerances(kspt,rtol, atol, dtol,mxit, pierr)
!    Call KSPSetOperators(kspt, At, At, SAME_NONZERO_PATTERN, pierr)
!    Call KSPSolve(kspt,bt,xt, pierr)
!    Call KSPView(kspt, PETSC_VIEWER_STDOUT_WORLD, pierr)
!    Call KSPDestroy(kspt,pierr)

    !PRINT*,'SOLN'
    !Call VecView(xt,PETSC_VIEWER_STDOUT_WORLD,pierr)

  end Subroutine ThermalSolve    

  Subroutine StructuralSolve(region, ng, MaxDisp,InitialGuess)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    Type(t_region), pointer :: region
    Real(rfreal) :: MaxDisp
    Integer :: ng
    Logical, optional :: InitialGuess

    ! ... local variables
    PetscReal :: MaxDispPetsc, rout
    PetscReal :: rtol, atol, dtol
    PetscInt  :: mxit, its,nlocal, first
    
    rtol = region%input%PetscRtol
    atol = 1.0D-50
    !tol = 1.0D-15
    dtol = 1.0D5
    mxit = 10000

    ! ... create linear solver
    Call KSPCreate(PETSC_COMM_WORLD, ksps, pierr)
    !call KSPSetTolerances(ksps,rtol, atol, dtol,mxit, pierr)
    !print*,'As'
    !Call MatView(As,PETSC_VIEWER_STDOUT_WORLD,pierr)
    !print*,'bs'
    !Call VecView(bs,PETSC_VIEWER_STDOUT_WORLD,pierr)
    Call KSPSetOperators(ksps, As, As, SAME_NONZERO_PATTERN, pierr)
    !Call KSPGetPC(ksps,pcs,pierr)
    if(present(InitialGuess) .and. InitialGuess .eqv. .true.) Call KSPSetInitialGuessNonzero(ksps, PETSC_TRUE, pierr)

    if(region%input%numTMproc > 1) then
       Call KSPSetOptionsPrefix(ksps,"stpar_",pierr)
       Call KSPSetFromOptions(ksps,pierr)
       ! Call PCSetType(pcs,PCBJACOBI,pierr)
!        Call KSPSetUp(ksps,pierr)
!        Call PCBJacobiGetSubKSP(pcs,nlocal, first,subksps,pierr)
!        Call KSPGMRESSetRestart(ksps, 300, pierr)
!        Call KSPGMRESSetRestart(subksps, 300, pierr)
!        !Call KSPSetType(subksps,KSPPREONLY,pierr)
!        Call KSPGetPC(subksps,subpcs,pierr)
!        Call PCSetType(subpcs, PCLU, pierr)
!        Call PCFactorSetLevels(subpcs,3,pierr)
!        !Call PCFactorSetMatOrderingType(subpcs,MATORDERINGRCM,pierr)
       !Call PCFactorSetShiftType(subpcs,MAT_SHIFT_NONZERO,pierr)
    else
       Call KSPSetOptionsPrefix(ksps,"stser_",pierr)
       Call KSPSetFromOptions(ksps,pierr)
       !Call KSPGetPC(ksps,pcs,pierr)
       !Call KSPSetType(ksps, KSPCG, pierr)
       !Call KSPGMRESSetRestart(ksps, 300, pierr)
       !Call PCSetType(pcs, PCLU, pierr)
       !Call PCFactorSetLevels(pcs,3,pierr)
       !Call PCFactorSetMatOrderingType(pcs,MATORDERINGRCM,pierr)
       !Call PCFactorSetShiftType(pcs,MAT_SHIFT_NONZERO,pierr)
!       Call KSPSetFromOptions(ksps,pierr)
    end if

    Call KSPSolve(ksps,bs,xs, pierr)
!    Call KSPView(ksps, PETSC_VIEWER_STDOUT_WORLD, pierr)
    Call KSPGetIterationNumber(ksps,its,pierr)
    !if(its > 1) write(*,'(A,I9,A)') 'RFLOCM: WARNING: took ',its,' iterations!'
    Call KSPGetResidualNorm(ksps,rout,pierr)
    !print*,'residual norm',rout
    !if(region%input%numTMproc > 1) Call KSPDestroy(subksps,pierr)
    Call KSPDestroy(ksps,pierr)

    !Call VecView(xs,PETSC_VIEWER_STDOUT_WORLD,pierr)
    Call VecNorm(xs,NORM_INFINITY, MaxDispPetsc, pierr)
    MaxDisp = dble(MaxDispPetsc)
  end Subroutine StructuralSolve    

  Subroutine GetInitialAcceleration(region, ng, MaxAccel)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    Type(t_region), pointer :: region
    Real(rfreal) :: MaxAccel
    Integer :: ng

    ! ... local variables
    PetscReal :: MaxAccelPetsc
    PetscReal :: rtol, atol, dtol
    PetscInt  :: mxit, its, nlocal, first
    PetscScalar :: fac

    rtol = region%input%PetscRtol
    atol = 1.0D-50
    dtol = 1.0D5
    mxit = 100000
    
    fac = 1.0_rfreal


    !call MatView(Mass,pierr)
    ! ... finalize RHS vector, 2 = structural problem, 2 = rhs
    call FinalVectorAssembly(region%input%TMSolve, 2, 2)

    ! ... get the rhs = bs_ext - bs_int
    ! ... note: rhs (bs) already filled with -1*bs_int
    call VecAXPY(bs,fac,bs_extOld,pierr)

    ! ... impose displacement boundary conditions
    call RemoveDispBCs(region%input%TMSolve, region, ng, .true.)

    ! ... create linear solver
    Call KSPCreate(PETSC_COMM_WORLD, ksps, pierr)
    !call KSPSetTolerances(ksps,rtol, atol, dtol,mxit, pierr)
    Call KSPSetOperators(ksps, Mass, Mass, SAME_NONZERO_PATTERN, pierr)
    !Call KSPGetPC(ksps,pcs,pierr)
    
!     if(region%input%numTMproc > 1) then
!        Call PCSetType(pcs,PCBJACOBI,pierr)
!        Call KSPSetUp(ksps,pierr)
!        Call PCBJacobiGetSubKSP(pcs,nlocal, first,subksps,pierr)
!        Call KSPGMRESSetRestart(ksps, 300, pierr)
!        Call KSPGMRESSetRestart(subksps, 300, pierr)
!        Call KSPGetPC(subksps,subpcs,pierr)
!        Call PCSetType(subpcs, PCLU, pierr)
!        Call PCFactorSetLevels(subpcs,3,pierr)
!        !Call PCFactorSetMatOrderingType(subpcs,MATORDERINGRCM,pierr)
!        Call PCFactorSetShiftType(subpcs,MAT_SHIFT_NONZERO,pierr)
!     else
!        !Call KSPGetPC(ksps,pcs,pierr)
!        Call PCSetType(pcs, PCLU, pierr)
!        Call PCFactorSetLevels(pcs,3,pierr)
!        !Call PCFactorSetMatOrderingType(pcs,MATORDERINGRCM,pierr)
!        Call PCFactorSetShiftType(pcs,MAT_SHIFT_NONZERO,pierr)
!     end if
 if(region%input%numTMproc > 1) then
       Call KSPSetOptionsPrefix(ksps,"accelpar_",pierr)
       Call KSPSetFromOptions(ksps,pierr)
       ! Call PCSetType(pcs,PCBJACOBI,pierr)
!        Call KSPSetUp(ksps,pierr)
!        Call PCBJacobiGetSubKSP(pcs,nlocal, first,subksps,pierr)
!        Call KSPGMRESSetRestart(ksps, 300, pierr)
!        Call KSPGMRESSetRestart(subksps, 300, pierr)
!        !Call KSPSetType(subksps,KSPPREONLY,pierr)
!        Call KSPGetPC(subksps,subpcs,pierr)
!        Call PCSetType(subpcs, PCLU, pierr)
!        Call PCFactorSetLevels(subpcs,3,pierr)
!        !Call PCFactorSetMatOrderingType(subpcs,MATORDERINGRCM,pierr)
       !Call PCFactorSetShiftType(subpcs,MAT_SHIFT_NONZERO,pierr)
    else
       Call KSPSetOptionsPrefix(ksps,"accelser_",pierr)
       Call KSPSetFromOptions(ksps,pierr)
       !Call KSPGetPC(ksps,pcs,pierr)
       !Call KSPSetType(ksps, KSPCG, pierr)
       !Call KSPGMRESSetRestart(ksps, 300, pierr)
       !Call PCSetType(pcs, PCLU, pierr)
       !Call PCFactorSetLevels(pcs,3,pierr)
       !Call PCFactorSetMatOrderingType(pcs,MATORDERINGRCM,pierr)
       !Call PCFactorSetShiftType(pcs,MAT_SHIFT_NONZERO,pierr)
!       Call KSPSetFromOptions(ksps,pierr)
    end if

    Call KSPSolve(ksps,bs,ddxs, pierr)
    !Call KSPView(ksps, PETSC_VIEWER_STDOUT_WORLD, pierr)
    Call KSPGetIterationNumber(ksps,its,pierr)
    if(its > 1) write(*,'(A,I9,A)') 'RFLOCM: WARNING: took ',its,' iterations!'
    Call KSPDestroy(ksps,pierr)

    !Call VecView(ddxs,PETSC_VIEWER_STDOUT_WORLD,pierr)
    Call VecNorm(ddxs,NORM_INFINITY, MaxAccelPetsc, pierr)
    MaxAccel = dble(MaxAccelPetsc)
  

    ! ... for now, just use zero initial velocity
    Call VecSet(dxs,0.0_rfreal,pierr)
    
    Call VecCopy(dxs,dxsOld,pierr)
    Call VecCopy(ddxs,ddxsOld,pierr)

  end Subroutine GetInitialAcceleration

  Subroutine GetInitialTemperatureRate(region, ng)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    Type(t_region), pointer :: region
    Integer :: ng

    ! ... local variables
    PetscReal :: rtol, atol, dtol
    PetscInt  :: mxit, its, nlocal, first
    
    rtol = region%input%PetscRtol
    atol = 1.0D-50
    dtol = 1.0D5
    mxit = 100000

    !call MatView(Mass,pierr)
    ! ... finalize RHS vector
    call FinalVectorAssembly(region%input%TMSolve, 1)

    !call VecView(bt,PETSC_VIEWER_STDOUT_WORLD,pierr)
    !call MatView(ThrmCap,PETSC_VIEWER_STDOUT_WORLD,pierr)

    ! ... impose displacement boundary conditions
    call RemoveTempBCs(region%input%TMSolve, region, ng, .true.)

    ! ... create linear solver
    Call KSPCreate(PETSC_COMM_WORLD, kspt, pierr)
    call KSPSetTolerances(kspt,rtol, atol, dtol,mxit, pierr)
    Call KSPSetOperators(kspt, ThrmCap, ThrmCap, SAME_NONZERO_PATTERN, pierr)
    Call KSPGetPC(kspt,pct,pierr)
    
    if(region%input%numTMproc > 1) then
       Call PCSetType(pct,PCBJACOBI,pierr)
       Call KSPSetUp(kspt,pierr)
       Call PCBJacobiGetSubKSP(pct,nlocal, first,subksps,pierr)
       Call KSPGMRESSetRestart(kspt, 300, pierr)
       Call KSPGMRESSetRestart(subkspt, 300, pierr)
       Call KSPGetPC(subkspt,subpct,pierr)
       Call PCSetType(subpct, PCLU, pierr)
       Call PCFactorSetLevels(subpct,3,pierr)
       Call PCFactorSetMatOrderingType(subpct,MATORDERINGRCM,pierr)
       Call PCFactorSetShiftType(subpct,MAT_SHIFT_NONZERO,pierr)
    else
       Call KSPGetPC(kspt,pct,pierr)
       Call PCSetType(pct, PCLU, pierr)
       Call PCFactorSetLevels(pct,3,pierr)
       Call PCFactorSetMatOrderingType(pct,MATORDERINGRCM,pierr)
       Call PCFactorSetShiftType(pct,MAT_SHIFT_NONZERO,pierr)
    end if

    Call KSPSolve(kspt,bt,dxt, pierr)
    !Call KSPView(ksps, PETSC_VIEWER_STDOUT_WORLD, pierr)
    Call KSPGetIterationNumber(kspt,its,pierr)
    if(its > 1000) write(*,'(A,I9,A)') 'RFLOCM: WARNING: took ',its,' iterations!'
    Call KSPDestroy(kspt,pierr)

    !Call VecView(dxt,PETSC_VIEWER_STDOUT_WORLD,pierr)
    !print*,'after initial temp rate'
    !Call VecNorm(ddxs,NORM_INFINITY, MaxAccelPetsc, pierr)
    !MaxAccel = dble(MaxAccelPetsc)
  

    ! ... for now, just use zero initial velocity
    !Call VecSet(dxs,0.0_rfreal,pierr)
    
    !Call VecCopy(ddxs,ddxsOld,pierr)
 
 end Subroutine GetInitialTemperatureRate

  Subroutine GetDynamicLHSRHS(region, ng, Problem, LHSOnly)
    
    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    
    Implicit none
    
    Type(t_region), pointer :: region
    Integer :: ng, Problem
    Logical, optional :: LHSOnly

    ! ... local variables
    Real(rfreal) :: dt

    ! ... petsc variables
    PetscReal :: fac
    
    ! ... Problem = 1: thermal
    ! ...         = 2: structural

    if(Problem == 1) then
       dt = region%TMstate(ng)%dt_thermal
       !Call MatView(At,PETSC_VIEWER_STDOUT_WORLD,pierr)
       !print*,'Kt'
       !Call MatView(ThrmCap,PETSC_VIEWER_STDOUT_WORLD,pierr)
       !print*,'C'

       ! ... LHS first
       fac = 2.0_rfreal/dt
       Call MatAXPY(At,fac,ThrmCap,SAME_NONZERO_PATTERN,pierr)

       !Call MatView(At,PETSC_VIEWER_STDOUT_WORLD,pierr)
       !print*,'LHS'

       if(present(LHSOnly) .and. LHSOnly .eqv. .true.) return

       ! ... assemble bt before adding C*dot(T_n)
       call FinalVectorAssembly(region%input%TMSolve, 1)

       ! ... now RHS
       ! ... bt = Q_(n+1)-k_n*T_n
       ! --> bt = C*dot(T_n) + bt
       !print*,'dxt'
       !Call VecView(dxt,PETSC_VIEWER_STDOUT_WORLD,pierr)
       !print*,'bt'
       !Call VecView(bt,PETSC_VIEWER_STDOUT_WORLD,pierr)
       Call MatMultAdd(ThrmCap,dxt,bt,bt,pierr)
       !print*,'bt_new'
       !Call VecView(bt,PETSC_VIEWER_STDOUT_WORLD,pierr)

    elseif(Problem == 2) then
       dt = region%TMstate(ng)%dt_struct

       ! ... LHS first
       fac = 4.0_rfreal/dt/dt
       Call MatAXPY(As,fac,Mass,SAME_NONZERO_PATTERN,pierr)

       if(present(LHSOnly) .and. LHSOnly .eqv. .true.) return

       ! ... now RHS
       ! ... accunt for changing external load
       !print*,'bs_extOld'
       !Call VecView(bs_extOld,PETSC_VIEWER_STDOUT_WORLD,pierr)
       !print*,'bs_ext'
       !Call VecView(bs_ext,PETSC_VIEWER_STDOUT_WORLD,pierr)
       fac = -1.0_rfreal
       Call VecAXPY(bs_ext,fac,bs_extOld,pierr)
       !print*,'bs_ext = bs_ext-bs_extOld'
       !Call VecView(bs_ext,PETSC_VIEWER_STDOUT_WORLD,pierr)
       fac = 4.0_rfreal/dt
       ! ... initialize bsaux
       Call VecSet(bsaux,0.0,pierr)
       Call VecAXPY(bsaux,fac,dxs,pierr)
       !print*,'dxs,fac',fac
       !Call VecView(dxs,PETSC_VIEWER_STDOUT_WORLD,pierr)
       !print*,'bsaux = 4/dt*dxs'
       !Call VecView(bsaux,PETSC_VIEWER_STDOUT_WORLD,pierr)
       !print*,'ddxs'
       !Call VecView(ddxs,PETSC_VIEWER_STDOUT_WORLD,pierr)
       fac = 2.0_rfreal
       Call VecAXPY(bsaux,fac,ddxs,pierr)
       !print*,'bsaux = bsaux+2*ddxs'
       !Call VecView(bsaux,PETSC_VIEWER_STDOUT_WORLD,pierr)
       Call MatMultAdd(Mass,bsaux,bs_ext,bs,pierr)
       !print*,'bs = M(bsaux)+bs_ext'
       !Call VecView(bs,PETSC_VIEWER_STDOUT_WORLD,pierr)
    end if
  end Subroutine GetDynamicLHSRHS

  Subroutine UpdateStructuralRates(region, ng, Stage)
    
    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    
    Implicit none
    
    Type(t_region), pointer :: region
    Integer :: ng, Stage

    ! ... local variables
    Real(rfreal) :: dt

    ! ... petsc variables
    PetscScalar :: fac
    
    dt = region%TMstate(ng)%dt_struct
    
    if(Stage == 1) then
       ! ... acceleration term first
       fac = 4.0_rfreal/dt
       Call VecAXPY(ddxs,fac,dxs,pierr)
       fac = -1.0_rfreal
       Call VecScale(ddxs,fac,pierr)
       fac = 4.0_rfreal/dt/dt
       Call VecAXPY(ddxs,fac,xs,pierr)
    
       ! ... now velocity term
       fac = -1.0_rfreal
       Call VecScale(dxs,fac,pierr)
       fac = 2.0_rfreal/dt
       Call VecAXPY(dxs,fac,xs,pierr)
    elseif(Stage == 2) then
       ! ... acceleration term first
       fac = 4.0_rfreal/dt/dt
       Call VecAXPY(ddxs,fac,xs,pierr)
    
       ! ... now velocity term
       fac = 2.0_rfreal/dt
       Call VecAXPY(dxs,fac,xs,pierr)
    end if

  end Subroutine UpdateStructuralRates

  Subroutine UpdateTemperatureRate(region, ng)
    
    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    
    Implicit none
    
   Type(t_region), pointer :: region
    Integer :: ng
    Logical :: SaveData
    
#define ValAux(ib)  Values(xxi+(ib))    
    ! ... local variables
    Type(t_sgrid), pointer :: TMgrid
    Type(t_smixt), pointer :: TMstate
    Real(rfreal) :: dt
    Integer, pointer :: G2L(:), L2G(:)
    Integer :: i, j, TMSolve, nVars, ND, nPtsOwned, nStart, nPtsLoc, dir
    
    ! ... PETSc variable
    !PetscScalar, pointer :: Values(:)
    PetscScalar :: Values(1)
    PetscInt :: ii, jj
    PetscScalar :: fac

    ! ... simplicity
    TMgrid    => region%TMgrid(ng)
    TMstate   => region%TMstate(ng)
    G2L       => TMgrid%G2L
    L2G       => TMgrid%L2G
    nPtsOwned =  TMgrid%nPtsOwned
    nPtsLoc   =  TMgrid%nPtsLoc
    nStart    =  TMgrid%nStart
    ND        =  TMgrid%input%ND
    TMSolve   =  TMgrid%input%TMSolve



    dt = region%TMstate(ng)%dt_thermal
    fac = -1.0_rfreal
    Call VecScale(dxt,fac,pierr)
    fac = 2.0_rfreal/dt
    Call VecAXPY(dxt,fac,xt,pierr)

    nVars = 1
    if(TMSolve == 3) nVars = ND*2+1

!       Call VecView(dxs,PETSC_VIEWER_STDOUT_WORLD,pierr)
       if(TMgrid%input%UseParMetis == FALSE) then
          ! ... get the temperature rate
          ! ... Note: ValAux is defined above
          call VecGetArray(dxt,Values,xxi,pierr)

          do ii = 1, nPtsOwned
             j = G2L(ii+nStart-1)
             TMstate%qdot(j,nVars) = ValAux(ii)
          end do

          call VecRestoreArray(dxt,Values,xxi,pierr)
          
          ! ... now fill Dirchlet boundary values
          do i = 1, TMstate%DispBCNum
             j = G2L(TMstate%BCDispInd(i))
             TMstate%qdot(j,nVars) = 0.0_rfreal
          end do

       elseif(TMgrid%input%UseParMetis == TRUE) then
          
          Call VecScatterBegin(ctxt,dxt,dxtl,INSERT_VALUES,SCATTER_FORWARD,pierr)
          Call VecScatterEnd(ctxt,dxt,dxtl,INSERT_VALUES,SCATTER_FORWARD,pierr)
          
          ! ... get the velocity
          ! ... Note: ValAux is defined above
          call VecGetArray(dxtl,Values,xxi,pierr)
          
          do ii = 1, nPtsOwned
             j = G2L(ii+nStart-1)
             TMstate%qdot(j,nVars) = ValAux(ii)
          end do
          call VecRestoreArray(dxtl,Values,xxi,pierr)
          

          ! ... now fill Dirchlet boundary values
          do i = 1, TMstate%DispBCNum
             j = G2L(TMstate%BCDispInd(i))
             TMstate%qdot(j,nVars) = 0.0_rfreal
          end do

       end if

  end Subroutine UpdateTemperatureRate

  Subroutine GetDynamicResidual(region, ng)
    
    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    
    Implicit none
    
    Type(t_region), pointer :: region
    Integer :: ng

    ! ... local variables
    Real(rfreal) :: dt

    ! ... petsc variables
    PetscReal :: fac
    
    dt = region%TMstate(ng)%dt_struct

    ! ... add the external load to the internal load
    ! ... bs contains -1*bs_int
    ! --> bs = bs + bs_ext = bs_ext-bs_int
    fac = 1.0_rfreal
    Call VecAXPY(bs,fac,bs_ext,pierr)

    Call MatMult(Mass,ddxs,bsaux,pierr)
    fac = -1.0_rfreal
    Call VecAXPY(bs,fac,bsaux,pierr)

   !print*,'here'
   
    !call VecView(ddxs,PETSC_VIEWER_STDOUT_WORLD,pierr)
    !call VecView(bs,PETSC_VIEWER_STDOUT_WORLD,pierr)
    !call graceful_exit(region%myrank, 'done')
  end Subroutine GetDynamicResidual 

  Subroutine GetSoln(region, ng, Problem)
    
    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    
    Implicit none

    Type(t_region), pointer :: region
    Integer :: ng, Problem
#define ValAux(ib)  Values(xxi+(ib))    
    ! ... local variables
    Type(t_sgrid), pointer :: TMgrid
    Type(t_smixt), pointer :: TMstate
    Integer, pointer :: G2L(:), L2G(:)
    Integer :: i, j, TMSolve, nVars, ND, nPtsOwned, nStart, nPtsLoc, dir
    
    ! ... PETSc variable
    !PetscScalar, pointer :: Values(:)
    PetscScalar :: Values(1)
    PetscInt :: ii, jj

    ! ... simplicity
    TMgrid    => region%TMgrid(ng)
    TMstate   => region%TMstate(ng)
    G2L       => TMgrid%G2L
    L2G       => TMgrid%L2G
    nPtsOwned =  TMgrid%nPtsOwned
    nPtsLoc   =  TMgrid%nPtsLoc
    nStart    =  TMgrid%nStart
    ND        =  TMgrid%input%ND
    nVars     =  TMstate%nVars
    TMSolve   =  TMgrid%input%TMSolve
    
    if(TMgrid%input%UseParMetis == FALSE) then
       if(Problem == 1) then
          ! ... get the thermal solution
          !allocate(Values(nPtsOwned))

          ! ... Note: xxi may be extremely negative, in which case the debugger may stumble
          ! ... VecGet/RestoreArrayF90 doesn't seem to be doing it's job correctly
          ! ... Note: ValAux is defined above
          call VecGetArray(xt,Values,xxi,pierr)
          !call VecGetArrayF90(xt,Values,pierr)
          do ii = 1, nPtsOwned
             j = G2L(ii+nStart-1)
             !          j = G2L(ii+nStart)
             TMstate%q(j,nVars) = TMstate%q(j,nVars) + ValAux(ii)
             !TMstate%q(j,nVars) = Values(i)
          end do

          call VecRestoreArray(xt,Values,xxi,pierr)
          !call VecRestoreArrayF90(xt,Values,pierr)



          !       do i = 1, size(TMstate%q,1)
          !          print*,'Temp',i,TMstate%q(i,nVars)
          !       end do

       elseif(Problem == 2) then 
          ! ... get the structural solution
          ! ... Note: xxi may be extremely negative, in which case the debugger may stumble
          ! ... VecGet/RestoreArrayF90 doesn't seem to be doing it's job correctly
          ! ... Note: ValAux is defined above
          !call VecView(xs,PETSC_VIEWER_STDOUT_WORLD,pierr)
          call VecGetArray(xs,Values,xxi,pierr)
          !call VecGetArrayF90(xt,Values,pierr)
          do ii = 1, nPtsOwned
             do dir = 1, ND
                j = G2L(ii+nStart-1)
                jj = (ii-1)*ND + dir
                TMstate%q(j,dir) = TMstate%q(j,dir) + ValAux(jj)
             end do
          end do
          call VecRestoreArray(xs,Values,xxi,pierr)
          !call VecRestoreArrayF90(xt,Values,pierr)



          ! do i = 1, size(TMstate%q,1)
          !    do dir = 1, ND
          !       print*,i,dir,TMstate%q(i,dir)
          !    end do
          ! end do
       end if

    elseif(TMgrid%input%UseParMetis == TRUE) then
       if(Problem == 1) then
          ! ... get the thermal solution
          ! ... need to gather the solution from ParMetis ordering
          Call VecScatterBegin(ctxt,xt,xtl,INSERT_VALUES,SCATTER_FORWARD,pierr)
          Call VecScatterEnd(ctxt,xt,xtl,INSERT_VALUES,SCATTER_FORWARD,pierr)
!          call VecView(xt,PETSC_VIEWER_STDOUT_WORLD,pierr)
!          call VecView(xtl,PETSC_VIEWER_STDOUT_WORLD,pierr)
!          call VecScatterView(ctxt,PETSC_VIEWER_STDOUT_WORLD,pierr)
          ! ... Note: xxi may be extremely negative, in which case the debugger may stumble
          ! ... VecGet/RestoreArrayF90 doesn't seem to be doing it's job correctly
          ! ... Note: ValAux is defined above
          call VecGetArray(xtl,Values,xxi,pierr)
          !call VecGetArrayF90(xt,Values,pierr)
          !if(TMgrid%input%ThermalTimeScheme == DYNAMIC_SOLN) then
          ! ... deltaT added to current temperature
          do ii = 1, nPtsOwned
             j = G2L(ii+nStart-1)
             TMstate%q(j,nVars) = TMstate%q(j,nVars) + ValAux(ii)
          end do
          !elseif(TMgrid%input%ThermalTimeScheme == STEADY_SOLN) then
          !   ! ... deltaT is the current temperature
          !   do ii = 1, nPtsOwned
          !      j = G2L(ii+nStart-1)
          !      TMstate%q(j,nVars) = ValAux(ii)
          !   end do
          !end if
          call VecRestoreArray(xtl,Values,xxi,pierr)
          !call VecRestoreArrayF90(xt,Values,pierr)



          !       do i = 1, size(TMstate%q,1)
          !          print*,'Temp',i,TMstate%q(i,nVars)
          !       end do

       elseif(Problem == 2) then 
          ! ... get the structural solution
          ! ... Note: xxi may be extremely negative, in which case the debugger may stumble
          ! ... VecGet/RestoreArrayF90 doesn't seem to be doing it's job correctly
          ! ... Note: ValAux is defined above
          
          Call VecScatterBegin(ctxs,xs,xsl,INSERT_VALUES,SCATTER_FORWARD,pierr)
          Call VecScatterEnd(ctxs,xs,xsl,INSERT_VALUES,SCATTER_FORWARD,pierr)

          call VecGetArray(xsl,Values,xxi,pierr)
          !call VecGetArrayF90(xt,Values,pierr)
          !if(TMgrid%input%StructuralTimeScheme == DYNAMIC_SOLN) then
             ! ... deltaU added to current configuration
             do ii = 1, nPtsOwned
                do dir = 1, ND
                   j = G2L(ii+nStart-1)
                   jj = (ii-1)*ND + dir
                   TMstate%q(j,dir) = TMstate%q(j,dir) + ValAux(jj)
                end do
             end do
          !elseif(TMgrid%input%ThermalTimeScheme == STEADY_SOLN) then
          !   ! ... deltaU is the current configuration
          !   do ii = 1, nPtsOwned
          !      do dir = 1, ND
          !         j = G2L(ii+nStart-1)
          !         jj = (ii-1)*ND + dir
          !         TMstate%q(j,dir) = ValAux(jj)
          !      end do
          !   end do
          !end if
          call VecRestoreArray(xsl,Values,xxi,pierr)
          !call VecRestoreArrayF90(xt,Values,pierr)



          ! do i = 1, size(TMstate%q,1)
          !    do dir = 1, ND
          !       print*,i,dir,TMstate%q(i,dir)
          !    end do
          ! end do
       end if
    end if

    ! ... now fill Dirchlet boundary values
    if(Problem == 1) then
       do i = 1, TMstate%TempBCNum
          TMstate%q(G2L(TMstate%BCTempInd(i)),nVars) = TMstate%BCTempVal(i)
       end do
    elseif(Problem == 2) then
       do i = 1, TMstate%DispBCNum
          j = G2L((TMstate%BCDispInd(i)-1)/ND+1)
          dir = mod(TMstate%BCDispInd(i)-1,ND)+1
          TMstate%q(j,dir) = TMgrid%X(j,dir) + TMstate%BCDispVal(i)
       end do
    end if


!    deallocate(Values)  

  end Subroutine GetSoln

  Subroutine StructuralRates(region, ng, SaveData)
    
    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    
    Implicit none

    Type(t_region), pointer :: region
    Integer :: ng
    Logical :: SaveData
    
#define ValAux(ib)  Values(xxi+(ib))    
    ! ... local variables
    Type(t_sgrid), pointer :: TMgrid
    Type(t_smixt), pointer :: TMstate
    Integer, pointer :: G2L(:), L2G(:)
    Integer :: i, j, TMSolve, nVars, ND, nPtsOwned, nStart, nPtsLoc, dir
    
    ! ... PETSc variable
    !PetscScalar, pointer :: Values(:)
    PetscScalar :: Values(1)
    PetscInt :: ii, jj

    ! ... simplicity
    TMgrid    => region%TMgrid(ng)
    TMstate   => region%TMstate(ng)
    G2L       => TMgrid%G2L
    L2G       => TMgrid%L2G
    nPtsOwned =  TMgrid%nPtsOwned
    nPtsLoc   =  TMgrid%nPtsLoc
    nStart    =  TMgrid%nStart
    ND        =  TMgrid%input%ND
    nVars     =  TMstate%nVars
    TMSolve   =  TMgrid%input%TMSolve

    
    if(SaveData .eqv. .false.) then
       ! ... reset the velocity
       Call VecCopy(dxsOld,dxs,pierr)
       ! ... reset the acceleration
       Call VecCopy(ddxsOld,ddxs,pierr)

    elseif(SaveData .eqv. .true.) then 

!       Call VecView(dxs,PETSC_VIEWER_STDOUT_WORLD,pierr)
       if(TMgrid%input%UseParMetis == FALSE) then
          ! ... get the velocity
          ! ... Note: ValAux is defined above
          call VecGetArray(dxs,Values,xxi,pierr)
          
          do ii = 1, nPtsOwned
             do dir = 1, ND
                j = G2L(ii+nStart-1)
                jj = (ii-1)*ND + dir
                TMstate%qdot(j,dir) = ValAux(jj)
                !print*,'getting vel: j,dir,TMstate%qdot(j,dir),ValAux(jj)',j,dir,TMstate%qdot(j,dir),ValAux(jj)
             end do
          end do
          call VecRestoreArray(dxs,Values,xxi,pierr)
          

          ! ... now fill Dirchlet boundary values
          do i = 1, TMstate%DispBCNum
             j = G2L((TMstate%BCDispInd(i)-1)/ND+1)
             dir = mod(TMstate%BCDispInd(i)-1,ND)+1
             TMstate%qdot(j,dir) = 0.0_rfreal
          end do
          
          ! ... get the acceleration
       ! ... Note: ValAux is defined above
          call VecGetArray(ddxs,Values,xxi,pierr)
          
          do ii = 1, nPtsOwned
             do dir = 1, ND
                j = G2L(ii+nStart-1)
                jj = (ii-1)*ND + dir
                TMstate%qdot(j,ND + dir) = ValAux(jj)
             end do
          end do
          call VecRestoreArray(ddxs,Values,xxi,pierr)
          
          
          ! ... now fill Dirchlet boundary values
          do i = 1, TMstate%DispBCNum
             j = G2L((TMstate%BCDispInd(i)-1)/ND+1)
             dir = mod(TMstate%BCDispInd(i)-1,ND)+1
             TMstate%qdot(j,ND + dir) = 0.0_rfreal
          end do
          
          ! do i = 1, size(TMstate%q,1)
          !    do dir = 1, ND
          !       print*,i,dir,TMstate%q(i,dir)
          !    end do
          ! end do
          
          Call VecCopy(dxs,dxsOld,pierr)
          Call VecCopy(ddxs,ddxsOld,pierr)

          ! ... also need to update old external load vector
          Call VecCopy(bs_ext,bs_extOld,pierr)

          ! ... get the external load
          ! ... Note: ValAux is defined above
          call VecGetArray(bs_extOld,Values,xxi,pierr)
          
          do ii = 1, nPtsOwned
             do dir = 1, ND
                j = G2L(ii+nStart-1)
                jj = (ii-1)*ND + dir
                TMstate%qdot(j,2*ND + dir) = ValAux(jj)
             end do
          end do
          call VecRestoreArray(bs_extOld,Values,xxi,pierr)

       !print*,'velout'
       !Call VecView(dxs,PETSC_VIEWER_STDOUT_WORLD,pierr)
       !print*,'accelout'
       !Call VecView(ddxs,PETSC_VIEWER_STDOUT_WORLD,pierr)



       elseif(TMgrid%input%UseParMetis == TRUE) then
          
          
          Call VecScatterBegin(ctxs,dxs,dxsl,INSERT_VALUES,SCATTER_FORWARD,pierr)
          Call VecScatterEnd(ctxs,dxs,dxsl,INSERT_VALUES,SCATTER_FORWARD,pierr)
          
          ! ... get the velocity
          ! ... Note: ValAux is defined above
          call VecGetArray(dxsl,Values,xxi,pierr)
          
          do ii = 1, nPtsOwned
             do dir = 1, ND
                j = G2L(ii+nStart-1)
                jj = (ii-1)*ND + dir
                TMstate%qdot(j,dir) = ValAux(jj)
             end do
          end do
          call VecRestoreArray(dxsl,Values,xxi,pierr)
          

          ! ... now fill Dirchlet boundary values
          do i = 1, TMstate%DispBCNum
             j = G2L((TMstate%BCDispInd(i)-1)/ND+1)
             dir = mod(TMstate%BCDispInd(i)-1,ND)+1
             TMstate%qdot(j,dir) = 0.0_rfreal
          end do



          Call VecScatterBegin(ctxs,ddxs,ddxsl,INSERT_VALUES,SCATTER_FORWARD,pierr)
          Call VecScatterEnd(ctxs,ddxs,ddxsl,INSERT_VALUES,SCATTER_FORWARD,pierr)
          
          ! ... get the acceleration
          ! ... Note: ValAux is defined above
          call VecGetArray(ddxsl,Values,xxi,pierr)
          
          do ii = 1, nPtsOwned
             do dir = 1, ND
                j = G2L(ii+nStart-1)
                jj = (ii-1)*ND + dir
                TMstate%qdot(j,ND + dir) = ValAux(jj)
             end do
          end do
          call VecRestoreArray(ddxsl,Values,xxi,pierr)
          
          
          ! ... now fill Dirchlet boundary values
          do i = 1, TMstate%DispBCNum
             j = G2L((TMstate%BCDispInd(i)-1)/ND+1)
             dir = mod(TMstate%BCDispInd(i)-1,ND)+1
             TMstate%qdot(j,ND + dir) = 0.0_rfreal
          end do
          
          ! do i = 1, size(TMstate%q,1)
          !    do dir = 1, ND
          !       print*,i,dir,TMstate%q(i,dir)
          !    end do
          ! end do
          
          Call VecCopy(dxs,dxsOld,pierr)
          Call VecCopy(ddxs,ddxsOld,pierr)

          ! ... also need to update old external load vector
          Call VecCopy(bs_ext,bs_extOld,pierr)

          Call VecScatterBegin(ctxs,bs_ext,bs_extl,INSERT_VALUES,SCATTER_FORWARD,pierr)
          Call VecScatterEnd(ctxs,bs_ext,bs_extl,INSERT_VALUES,SCATTER_FORWARD,pierr)
          
          ! ... get the acceleration
          ! ... Note: ValAux is defined above
          call VecGetArray(bs_extl,Values,xxi,pierr)
          
          do ii = 1, nPtsOwned
             do dir = 1, ND
                j = G2L(ii+nStart-1)
                jj = (ii-1)*ND + dir
                TMstate%qdot(j,2*ND + dir) = ValAux(jj)
             end do
          end do
          call VecRestoreArray(bs_extl,Values,xxi,pierr)


       end if ! ... TMgrid%input%ParMetis == FALSE
       
    end if


!    deallocate(Values)  

  end Subroutine StructuralRates

  Subroutine ResetMatrix(Problem)

    USE ModGlobal
    USE ModDataStruct

    Implicit None

    Integer :: Problem

    if(Problem == 1) then
       call MatZeroEntries(At, pierr)
       
       call VecSet(xt, 0.0, pierr)
       call VecSet(bt, 0.0, pierr)
    elseif(Problem == 2) then
       call MatZeroEntries(As, pierr)
       
       call VecSet(xs, 0.0, pierr)
       call VecSet(bs, 0.0, pierr)
       call VecSet(bs_ext,0.0,pierr)

    elseif(Problem == 3) then
       call MatZeroEntries(Mass, pierr)
       
       call VecSet(dxs, 0.0, pierr)
       call VecSet(ddxs, 0.0, pierr)
       call VecSet(dxsOld, 0.0, pierr)
       call VecSet(ddxsOld, 0.0, pierr)
    elseif(Problem == 4) then
       call MatZeroEntries(ThrmCap, pierr)
       
       call VecSet(dxt, 0.0, pierr)
    end if

  end Subroutine ResetMatrix

  Subroutine CreateBCZeros(region, ng)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    
    implicit none
    
    Type(t_region), pointer :: region
    Integer :: ng
    
    ! ... local variables
    
    Type(t_smixt), pointer :: TMstate
    Type(t_sgrid), pointer :: TMgrid

    ! ... PETSc variables
    Integer :: TMRank, i, j, BCCount,dir, ND
    
    ! ... Petsc Stuff
    PetscInt, pointer :: RowNColNums(:)
    PetscScalar, pointer :: Values(:)
    PetscInt :: m


    ! ... simplicity
    TMstate => region%TMstate(ng)
    TMgrid  => region%TMgrid(ng)
    BCCount = TMstate%DispBCNum
    ND      = TMgrid%ND

    ! ... initialize all values to 1
    Call VecSet(bc, 1.0_rfreal, pierr)
!    call MPI_BARRIER(mycomm,ierr)
!        m = BCCount
!        do i = 1, BCCount
!           j = G2L((TMstate%BCDispInd(i)-1)/ND+1)
!           dir = mod(TMstate%BCDispInd(i)-1,ND)+1
!           if(TMgrid%input%UseParMetis == FALSE) then
!              RowNColNums(i) = TMstate%BCDispInd(i) - 1
!           elseif(TMgrid%input%UseParMetis == TRUE) then
!              RowNColNums(i) = (L2GRenum(j)-1)*ND + dir - 1
!           end if
    
    if(BCCount > 0) then
       allocate(RowNColNums(BCCount),Values(BCCount))
       m = BCCount
       do i = 1, BCCount
          if(TMgrid%input%UseParMetis == FALSE) then
             RowNColNums(i) = TMstate%BCDispInd(i) - 1
          elseif(TMgrid%input%UseParMetis == TRUE) then
             j = TMgrid%G2L((TMstate%BCDispInd(i)-1)/ND+1)
             dir = mod(TMstate%BCDispInd(i)-1,ND)+1
             RowNColNums(i) = (TMgrid%L2GRenum(j)-1)*ND + dir - 1
          end if
          !RowNColNums(i) = TMstate%BCDispInd(i) - 1
          Values(i) = 0.0_rfreal
!          print*,i,BCInd(i),BCVal(i)
       end do
       
       !! ... initialize all values to 1
       !Call VecSet(bc, 1.0_rfreal, pierr)

       ! ... zero out displacement bcs
       Call VecSetValues(bc,m,RowNColNums,Values,INSERT_VALUES,pierr)
    !else
   
     !  ! ... initialize all values to 1
     !  Call VecSet(bc, 1.0_rfreal, pierr)

       ! ... zero out displacement bcs
       !Call VecSetValues(bc,0,0,PETSC_NULL,INSERT_VALUES,pierr)
    end if

    ! ... assemble vector
    call VecAssemblyBegin(bc, pierr)
    call VecAssemblyEnd(bc, pierr)

    

  end Subroutine CreateBCZeros

  Subroutine GetRes(Residual,SteadyState)
    
    USE ModGlobal
    USE ModDataStruct
    Use ModMPI
    
    Implicit None

    Integer, optional :: SteadyState
    Real(RFREAL) :: Residual
    PetscScalar :: PETSC_Residual, fac

    if(present(SteadyState) .and.  SteadyState == TRUE) then
       ! ... add the internal and external load vectors
       fac = 1.0_rfreal
       call VecAXPY(bs,fac,bs_ext,pierr)
    end if

    ! ... remove the Displacement BCs from the residual vector
    call VecPointwiseMult(rs,bs,bc,pierr)
    
    
    !Call VecView(bc,PETSC_VIEWER_STDOUT_WORLD,pierr)
!    Call VecView(bs,PETSC_VIEWER_STDOUT_WORLD,pierr)
    !Call VecView(rs,PETSC_VIEWER_STDOUT_WORLD,pierr)
    !call graceful_exit(region%myrank, 'done')
    ! ... now take the L2 norm of rs
    call VecNorm(rs,NORM_2,PETSC_Residual,pierr)
    Residual = dble(PETSC_Residual)
    !call VecNorm(bs,NORM_1,Residual,pierr)
  end Subroutine GetRes

Subroutine InitGuessSoln(region,ng,Problem)
    
    USE ModGlobal
    USE ModDataStruct
    Use ModMPI
    
    Implicit None

    Type(t_region), pointer :: region
    Integer :: Problem, ng

    PetscScalar ::  fac, dt
    !Call VecView(xs,PETSC_VIEWER_STDOUT_WORLD,pierr)
    if(Problem == 1) then
       ! ... thermal
    elseif(Problem == 2) then
       dt = region%TMstate(ng)%dt_struct
       ! ... make initial guess at xs
       ! ... dx = v*dt + 1/2*dt^2*a
       fac = dt
       Call VecAXPY(xs,fac,dxs,pierr)
       fac = 0.5_rfreal*dt*dt
       Call VecAXPY(xs,fac,ddxs,pierr)
    end if
    !print*,'made initial guess'
    !Call VecView(xs,PETSC_VIEWER_STDOUT_WORLD,pierr)
  end Subroutine InitGuessSoln

  Subroutine EnterStructuralRates(region, ng, var)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    
    implicit none
    
    Type(t_region), pointer :: region
    Integer :: ng, var
    
    ! ...local variables
    Type(t_sgrid), pointer :: TMgrid
    Type(t_smixt), pointer :: TMstate
    Type(t_mixt_input), pointer :: input
    Integer, pointer :: L2G(:), G2L(:)
    Integer :: TMRank, i, j, l0, ND, dir, nPtsOwned, nStart
    
    ! ... Petsc Stuff
    PetscInt, pointer :: RowNColNums(:)
    PetscScalar, pointer :: Values(:)
    PetscInt :: m

    ! ... simplicity
    TMgrid     => region%TMgrid(ng)
    TMstate    => region%TMstate(ng)
    input      => TMgrid%input
    TMRank     =  region%TMRank
    L2G        => TMgrid%L2G
    G2L        => TMgrid%G2L
    ND         =  TMgrid%ND
    nPtsOwned  =  TMgrid%nPtsOwned
    nStart     =  TMgrid%nStart

    Allocate(Values(nPtsOwned*ND), RowNColNums(nPtsOwned*ND))
!         ! ... adjust for c++ counting
!          if(input%UseParMetis == FALSE) then
!             RowNColNums(i) = L2G(LM(i,le)) - 1
!          elseif(input%UseParMetis == TRUE) then
!             RowNColNums(i) = TMgrid%L2GRenum(LM(i,le)) - 1
!          end if
    do i = 1, nPtsOwned
       do dir = 1, ND
          l0 = (i-1)*ND + dir
          
        ! ... adjust for c++ counting
         if(input%UseParMetis == FALSE) then
            RowNColNums(l0) = (nStart + i - 2)*ND + dir - 1  
         elseif(input%UseParMetis == TRUE) then
            RowNColNums(l0) = (TMgrid%L2GRenum(G2L(nStart + i - 1)) - 1)*ND + dir - 1  
         end if
          ! ... adjust for c++ counting
         if(var == 1) then
            ! ... velocity
            Values(l0) = TMstate%qdot(G2L(nStart + i - 1),dir)
         elseif(var == 2) then
            ! ... acceleration
            Values(l0) = TMstate%qdot(G2L(nStart + i - 1),ND+dir)
         elseif(var == 3) then
            ! ... old external load
            Values(l0) = TMstate%qdot(G2L(nStart + i - 1),2*ND+dir)
         end if
       end do
    end do

    if(var == 1) then
       Call VecSetValues(dxs,nPtsOwned*ND,RowNColNums,Values,ADD_VALUES,pierr)
       Call VecAssemblyBegin(dxs, pierr)
       Call VecAssemblyEnd(dxs, pierr)
       call VecCopy(dxs,dxsOld,pierr)
       !call VecSet(dxsOld, 0.0, pierr)
       !print*,'velin'
       !Call VecView(dxs,PETSC_VIEWER_STDOUT_WORLD,pierr)
    elseif(var == 2) then
       Call VecSetValues(ddxs,nPtsOwned*ND,RowNColNums,Values,ADD_VALUES,pierr)   
       Call VecAssemblyBegin(ddxs, pierr)
       Call VecAssemblyEnd(ddxs, pierr)
       call VecCopy(ddxs,ddxsOld,pierr)
!       call VecSet(ddxsOld, 0.0, pierr)
       !print*,'accelin'
       !Call VecView(ddxs,PETSC_VIEWER_STDOUT_WORLD,pierr)
    elseif(var == 3) then
       ! ... record old external load
       Call VecSetValues(bs_extOld,nPtsOwned*ND,RowNColNums,Values,INSERT_VALUES,pierr)   
       Call VecAssemblyBegin(bs_extOld, pierr)
       Call VecAssemblyEnd(bs_extOld, pierr)
       !print*,'bs_ext in'
       !Call VecView(bs_extOld,PETSC_VIEWER_STDOUT_WORLD,pierr)
    else
       write(*,'(A)') 'RFLOCM: Error in EnterStructuralRates, neither velocity nor acceleration specified.'
    end if
    


    deallocate(Values,RowNColNums)

  end Subroutine EnterStructuralRates

  Subroutine EnterTemperatureRate(region, ng)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    
    implicit none
    
    Type(t_region), pointer :: region
    Integer :: ng
    
    ! ...local variables
    Type(t_sgrid), pointer :: TMgrid
    Type(t_smixt), pointer :: TMstate
    Type(t_mixt_input), pointer :: input
    Integer, pointer :: L2G(:), G2L(:)
    Integer :: TMRank, i, j, l0, ND, dir, nPtsOwned, nStart, nVars
    
    ! ... Petsc Stuff
    PetscInt, pointer :: RowNColNums(:)
    PetscScalar, pointer :: Values(:)
    PetscInt :: m

    ! ... simplicity
    TMgrid     => region%TMgrid(ng)
    TMstate    => region%TMstate(ng)
    input      => TMgrid%input
    TMRank     =  region%TMRank
    L2G        => TMgrid%L2G
    G2L        => TMgrid%G2L
    ND         =  TMgrid%ND
    nPtsOwned  =  TMgrid%nPtsOwned
    nStart     =  TMgrid%nStart

    nVars = 1
    if(input%TMSolve == 3) nVars = ND*3+1

    Allocate(Values(nPtsOwned), RowNColNums(nPtsOwned))
!         ! ... adjust for c++ counting
!          if(input%UseParMetis == FALSE) then
!             RowNColNums(i) = L2G(LM(i,le)) - 1
!          elseif(input%UseParMetis == TRUE) then
!             RowNColNums(i) = TMgrid%L2GRenum(LM(i,le)) - 1
!          end if
    do i = 1, nPtsOwned
       
       l0 = i
          
       ! ... adjust for c++ counting
       if(input%UseParMetis == FALSE) then
          RowNColNums(l0) = (nStart + i - 2)  
       elseif(input%UseParMetis == TRUE) then
          RowNColNums(l0) = (TMgrid%L2GRenum(G2L(nStart + i - 1)) - 1)  
       end if
       ! ... adjust for c++ counting
       Values(l0) = TMstate%qdot(G2L(nStart + i - 1),nVars)

    end do

    Call VecSetValues(dxt,nPtsOwned,RowNColNums,Values,ADD_VALUES,pierr)
    Call VecAssemblyBegin(dxt, pierr)
    Call VecAssemblyEnd(dxt, pierr)
!    call VecCopy(dxt,dxt,pierr)
    !call VecSet(dxsOld, 0.0, pierr)
    !print*,'velin'
    !Call VecView(dxs,PETSC_VIEWER_STDOUT_WORLD,pierr)

    


    deallocate(Values,RowNColNums)

  end Subroutine EnterTemperatureRate

  Subroutine ComputeSolidData(PowerIn, KinEnergy)

    USE ModGlobal
    USE ModDataStruct
    
    Implicit None
    
    !Type(t_region), pointer :: region
    !Integer :: ng
    Real(rfreal) :: PowerIn, KinEnergy
    ! ... local variables
    !Type(t_smixt), pointer :: TMstate
    PetscScalar :: buffer

    ! ... simplicty
    !TMstate => region%TMstate(ng)

    ! ... increment the counter
    !TMstate%numDataSample = TMstate%numDataSample + 1

    ! ... compute the power input from external sources
    Call VecDot(bs_ext, dxs, buffer, pierr)

    !print*,'power',buffer
    ! ... store power in memory
    !TMstate%PowerIn(TMstate%numDataSample) = buffer
    PowerIn = buffer
    

    ! ... now compute the kinetic energy, 1/2*(m V^2)
    ! ... m*V
    Call MatMult(Mass,dxs,auxs,pierr)
    ! ... dot((m*V),V)
    Call VecDot(auxs,dxs,buffer,pierr)
    !print*,'energy',TMstate%numDataSample,buffer
    !TMstate%KinEnergy(TMstate%numDataSample) = buffer
    KinEnergy = buffer*0.5_rfreal
    
  end Subroutine ComputeSolidData

  Subroutine CleanUpPetSc(region, ng)
    
    USE ModGlobal
    USE ModDataStruct
    
    Implicit None
    
    Type(t_region), pointer :: region
    Integer :: ng
    
    ! ... local variables
    Type(t_sgrid), pointer :: TMgrid
    Type(t_mixt_input), pointer :: input

    do ng = 1, region%nTMGrids
       
       ! ... simplicity
       TMgrid => region%TMgrid(ng)
       input  => TMgrid%input
       
       if(input%TMSolve == 1 .OR. input%TMSolve == 3) then
          Call MatDestroy(At, pierr)
          Call VecDestroy(xt, pierr)
          Call VecDestroy(bt, pierr)

          if(input%ThermalTimeScheme == DYNAMIC_SOLN) then
             Call VecDestroy(dxt, pierr) 
          end if

       end if

       if(input%TMSolve == 2 .OR. input%TMSolve == 3) then
          
          Call MatDestroy(As, pierr)
          Call VecDestroy(xs, pierr)
          Call VecDestroy(bs, pierr)
          Call VecDestroy(rs, pierr)
          Call VecDestroy(bc, pierr)
          Call VecDestroy(bs_ext, pierr)

          if(input%StructuralTimeScheme == DYNAMIC_SOLN) then

             Call MatDestroy(Mass, pierr)
             Call VecDestroy(dxs, pierr)
             Call VecDestroy(ddxs, pierr)
             Call VecDestroy(dxsOld, pierr)
             Call VecDestroy(ddxsOld, pierr)
             Call VecDestroy(bsaux, pierr)
             Call VecDestroy(bs_extOld, pierr)
             Call VecDestroy(auxs, pierr)

          end if

       end if

       if(input%UseParMetis == TRUE) then
          if(input%TMSolve == 1 .OR. input%TMSolve == 3) then
             Call VecDestroy(xtl, pierr)
             Call VecScatterDestroy(ctxt, pierr)
          end if

          if(input%TMSolve == 2 .OR. input%TMSolve == 3) then
             Call VecDestroy(xsl, pierr)
             if(input%StructuralTimeScheme == DYNAMIC_SOLN) then
                Call VecDestroy(dxsl, pierr)
                Call VecDestroy(ddxsl, pierr)
                Call VecDestroy(bs_extl, pierr)
             end if
             Call VecScatterDestroy(ctxs, pierr)
          end if
       end if

    end do

    Call PetscFinalize(PETSC_NULL_CHARACTER, pierr)

  end Subroutine CleanUpPetSc
  
end Module ModPETSc
