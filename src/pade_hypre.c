/* Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC */
/* License: MIT, http://opensource.org/licenses/MIT */
/*
 * pade_hypre.c --- contains all necessary routines for using HYPRE with PlasComCM for implicit spatial operators
 *
 * $Header: /cvsroot/genx/Codes/RocfloCM/Source/pade_hypre.c,v 1.8 2011/05/30 18:46:43 bodony Exp $
 */

#include <stdio.h>
#include <stdlib.h>
#ifndef USE_HYPRE
#include "mpi.h"
#endif

/* HYPRE-specific headers */
#ifdef USE_HYPRE
#include "_hypre_utilities.h"
#include "HYPRE_sstruct_ls.h"
#include "HYPRE_krylov.h"

/* HYPRE-specific variables */
HYPRE_SStructGrid     *pade_grid = NULL;
HYPRE_SStructGraph    *pade_graph = NULL;
HYPRE_SStructStencil  *pade_stencil = NULL;
HYPRE_SStructMatrix   *pade_A = NULL;
HYPRE_SStructVector   *pade_b = NULL;
HYPRE_SStructVector   *pade_x = NULL;

/* We are using struct solvers for this example */
HYPRE_SStructSolver   *pade_solver = NULL;
HYPRE_SStructSolver   *pade_pc = NULL;

/* items for ParCSR preconditioner Euclid */
HYPRE_ParCSRMatrix    *pade_par_A = NULL;
HYPRE_ParVector       *pade_par_b = NULL;
HYPRE_ParVector       *pade_par_x = NULL;
HYPRE_Solver          *pade_par_solver = NULL;
HYPRE_Solver          *pade_par_precond = NULL;
HYPRE_Solver          *pade_eu = NULL;

/* object type for each operator can be different */
int                   *object_type = NULL;
#endif

/* mapping from (grid, fortranOpID) -> opID */
#define GET_OPID(a,b,c,d,e) ( ( (a-1) * (c) + (b-1) ) * (d) + (e) - 1 )

/* include global variables here */
int *pade_nparts = NULL;
int *pade_part   = NULL;
int *pade_nvars  = NULL;

void check_pade_hypre_ierr(int ierr, int line, char *fname)
{

  if (ierr != 0) {
    printf("RFLOCM: PADE_HYPRE: ERROR: ierr = %d at line %d in file %s\n", ierr, line, fname);
    exit(-1);
  }

}

void allocate_pade_hypre_objects_(int *region_nGrids, int *max_ops, int *ndim) 
{

#ifdef USE_HYPRE
  /* allocate memory, if needed */
  if (pade_nparts == NULL) {

    int numOps = (*region_nGrids) * (*max_ops) * (*ndim);

       pade_nparts = (int *)malloc(numOps*sizeof(int));
        pade_nvars = (int *)malloc(numOps*sizeof(int));
       object_type = (int *)malloc(numOps*sizeof(int));
         pade_grid = (HYPRE_SStructGrid *)malloc(numOps*sizeof(HYPRE_SStructGrid));
        pade_graph = (HYPRE_SStructGraph *)malloc(numOps*sizeof(HYPRE_SStructGraph));
            pade_A = (HYPRE_SStructMatrix *)malloc(numOps*sizeof(HYPRE_SStructMatrix));
            pade_b = (HYPRE_SStructVector *)malloc(numOps*sizeof(HYPRE_SStructVector));
            pade_x = (HYPRE_SStructVector *)malloc(numOps*sizeof(HYPRE_SStructVector));
       pade_solver = (HYPRE_SStructSolver *)malloc(numOps*sizeof(HYPRE_StructSolver));
           pade_pc = (HYPRE_SStructSolver *)malloc(numOps*sizeof(HYPRE_StructSolver));
      pade_stencil = (HYPRE_SStructStencil *)malloc(numOps*sizeof(HYPRE_SStructStencil));
        pade_par_A = (HYPRE_ParCSRMatrix *)malloc(numOps*sizeof(HYPRE_ParCSRMatrix));
        pade_par_b = (HYPRE_ParVector *)malloc(numOps*sizeof(HYPRE_ParVector));
        pade_par_x = (HYPRE_ParVector *)malloc(numOps*sizeof(HYPRE_ParVector));
   pade_par_solver = (HYPRE_Solver *)malloc(numOps*sizeof(HYPRE_Solver));
  pade_par_precond = (HYPRE_Solver *)malloc(numOps*sizeof(HYPRE_Solver));
           pade_eu = (HYPRE_Solver *)malloc(numOps*sizeof(HYPRE_Solver));

  }
#endif
  
}


void init_pade_hypre_objects_(int *nGridsGlobal, int *iGridGlobal, int *ng, int *region_nGrids, 
                              int *myrank, int *numproc, MPI_Fint *fcomm, int *fndim, int *fdir, 
                              int *fOp, int *is, int *ie, int *fstencil_size, int *periodic)
{

#ifdef USE_HYPRE
  int i, j, k, count, ierr, ndim, stencil_size, var, part, opID, dir, nentries;

  MPI_Comm comm;

  /* this operator ID */
  opID = GET_OPID(*fOp, *ng, *region_nGrids, *fndim, *fdir);

  /* default values */
                ndim = *fndim;
 	         dir = *fdir;
        stencil_size = *fstencil_size;
   pade_nparts[opID] = 1;
    pade_nvars[opID] = 1;

  /* convert communicator */
  comm = (MPI_Comm)MPI_Comm_f2c(*fcomm);

  /* create the empty grid object */
  ierr = HYPRE_SStructGridCreate(comm, ndim, pade_nparts[opID], &(pade_grid[opID]));
  check_pade_hypre_ierr(ierr, __LINE__, __FILE__);

  /* set the grid extents */
  int *ilower = NULL, *iupper = NULL;
  ilower = (int *)malloc(ndim*sizeof(int));
  iupper = (int *)malloc(ndim*sizeof(int));
  for (i = 0; i < ndim; i++) {
    ilower[i] = is[i]-1;
    iupper[i] = ie[i]-1;
  }
  for (part = 0; part < pade_nparts[opID]; part++) ierr += HYPRE_SStructGridSetExtents(pade_grid[opID], part, ilower, iupper);
  check_pade_hypre_ierr(ierr, __LINE__, __FILE__);

  /* apply periodicity : assumes 1-D stencils */
  if (periodic[dir-1] > 0) {

    int cur_exts[2][3], new_exts[2][3];
    int index_map[3] = {0, 1, 2};
    int index_dir[3] = {1, 1, 1};

    /* initialize extents */
    for (j = 0; j < ndim; j++) {
      cur_exts[0][j] = is[j]-1;
      cur_exts[1][j] = ie[j]-1;
      new_exts[0][j] = cur_exts[0][j];
      new_exts[1][j] = cur_exts[1][j];
    }

    /* sitting on the right, looking to the far left */
    /* assumes a simple 3-D decomposition */
    if (ie[dir-1] == periodic[dir-1]) {

      cur_exts[0][dir-1] = periodic[dir-1];
      cur_exts[1][dir-1] = periodic[dir-1];
      new_exts[0][dir-1] = 1;
      new_exts[1][dir-1] = 1;

      /* new indices are along the right edge */
      ierr += HYPRE_SStructGridSetNeighborPart(pade_grid[opID], 0, cur_exts[0], cur_exts[1], 0, new_exts[0], new_exts[1], index_map, index_dir);
      check_pade_hypre_ierr(ierr, __LINE__, __FILE__);

    }

    /* initialize extents */
    for (j = 0; j < ndim; j++) {
      cur_exts[0][j] = is[j]-1;
      cur_exts[1][j] = ie[j]-1;
      new_exts[0][j] = cur_exts[0][j];
      new_exts[1][j] = cur_exts[1][j];
    }

    /* sitting on the left, looking to the far right */
    /* assumes a simple 3-D decomposition */
    if (is[dir-1] == 1) {
      cur_exts[0][dir-1] = -1;
      cur_exts[1][dir-1] = -1;
      new_exts[0][dir-1] = periodic[dir-1]-2;
      new_exts[1][dir-1] = periodic[dir-1]-2;

      /* new indices are along the right edge */
      ierr += HYPRE_SStructGridSetNeighborPart(pade_grid[opID], 0, cur_exts[0], cur_exts[1], 0, new_exts[0], new_exts[1], index_map, index_dir);
      check_pade_hypre_ierr(ierr, __LINE__, __FILE__);
    }
  }

  /* set the variable type for each part */
  HYPRE_SStructVariable *vartypes = NULL;
  vartypes = (HYPRE_SStructVariable *)malloc(pade_nvars[opID]*sizeof(HYPRE_SStructVariable));
  for (i = 0; i < pade_nvars[opID]; i++) vartypes[i] = HYPRE_SSTRUCT_VARIABLE_CELL;
  for (part = 0; part < pade_nparts[opID]; part++) 
    ierr += HYPRE_SStructGridSetVariables(pade_grid[opID], part, pade_nvars[opID], vartypes);
  check_pade_hypre_ierr(ierr, __LINE__, __FILE__);
  free(vartypes);

/*
 * HYPRE developer Rob Falgout says that HYPRE_SStructGridSetPeriodic
 * does NOT work for HYPRE_PARCSR objects
 */
  if (periodic[dir-1] > 0) {
    part = 0;
    ilower[0] = 21;
    ilower[1] = 0;
    ilower[2] = 0;
    ierr += HYPRE_SStructGridSetPeriodic(pade_grid[opID], part, ilower);
  }
 
  /* This is a collective call finalizing the grid assembly.
     The grid is now ``ready to be used'' */
  ierr += HYPRE_SStructGridAssemble(pade_grid[opID]);
  check_pade_hypre_ierr(ierr, __LINE__, __FILE__);

  /* setup the stencil object */
  ierr += HYPRE_SStructStencilCreate(ndim, stencil_size, &(pade_stencil[opID]));
  check_pade_hypre_ierr(ierr, __LINE__, __FILE__);

  /* setup the stencil itself */
  /* counting: point 0 is always self, 
     then from min-to-max in direction 1, 
     then from min-to-max in direction 2, 
     then from min-to-max in direction 3 */

  /* 3-point stencils */
  int stencil1_1_3[3][1] = {{0},{-1},{1}};
  int stencil2_1_3[3][2] = {{0,0},{-1,0},{1,0}};
  int stencil2_2_3[3][2] = {{0,0},{0,-1},{0,1}};
  int stencil3_1_3[3][3] = {{0,0,0}, {-1,0,0}, {1,0,0}};
  int stencil3_2_3[3][3] = {{0,0,0}, {0,-1,0}, {0,1,0}};
  int stencil3_3_3[3][3] = {{0,0,0}, {0,0,-1}, {0,0,1}};

  /* 5-point stencils */
  int stencil1_1_5[5][1] = {{0},{-2},{-1},{1},{2}};
  int stencil2_1_5[5][2] = {{0,0},{-2,0},{-1,0},{1,0},{2,0}};
  int stencil2_2_5[5][2] = {{0,0},{0,-2},{0,-1},{0,1},{0,2}};
  int stencil3_1_5[5][3] = {{0,0,0},{-2,0,0},{-1,0,0},{1,0,0},{2,0,0}};
  int stencil3_2_5[5][3] = {{0,0,0},{0,-2,0},{0,-1,0},{0,1,0},{0,2,0}};
  int stencil3_3_5[5][3] = {{0,0,0},{0,0,-2},{0,0,-1},{0,0,1},{0,0,2}};

  if (stencil_size == 3) {
    if (ndim == 1) {
      for (var = 0; var < pade_nvars[opID]; var++) {
	for (j = 0; j < stencil_size; j++) {
	  ierr += HYPRE_SStructStencilSetEntry(pade_stencil[opID], var*stencil_size+j, stencil1_1_3[j], var);
	}
      }
    } else if (ndim == 2) {
      for (var = 0; var < pade_nvars[opID]; var++) {
	for (j = 0; j < stencil_size; j++) {
	  if (dir == 1) ierr += HYPRE_SStructStencilSetEntry(pade_stencil[opID], var*stencil_size+j, stencil2_1_3[j], var);
	  if (dir == 2) ierr += HYPRE_SStructStencilSetEntry(pade_stencil[opID], var*stencil_size+j, stencil2_2_3[j], var);
	}
      }
    } else if (ndim == 3) {
      for (var = 0; var < pade_nvars[opID]; var++) {
	for (j = 0; j < stencil_size; j++) {
	  if (dir == 1) ierr += HYPRE_SStructStencilSetEntry(pade_stencil[opID], var*stencil_size+j, stencil3_1_3[j], var);
	  if (dir == 2) ierr += HYPRE_SStructStencilSetEntry(pade_stencil[opID], var*stencil_size+j, stencil3_2_3[j], var);
	  if (dir == 3) ierr += HYPRE_SStructStencilSetEntry(pade_stencil[opID], var*stencil_size+j, stencil3_3_3[j], var);
	}
      }
    }
  } else if (stencil_size == 5) {
    if (ndim == 1) {
      for (var = 0; var < pade_nvars[opID]; var++) {
	for (j = 0; j < stencil_size; j++) {
	  ierr += HYPRE_SStructStencilSetEntry(pade_stencil[opID], var*stencil_size+j, stencil1_1_5[j], var);
	}
      }
    } else if (ndim == 2) {
      for (var = 0; var < pade_nvars[opID]; var++) {
	for (j = 0; j < stencil_size; j++) {
	  if (dir == 1) ierr += HYPRE_SStructStencilSetEntry(pade_stencil[opID], var*stencil_size+j, stencil2_1_5[j], var);
	  if (dir == 2) ierr += HYPRE_SStructStencilSetEntry(pade_stencil[opID], var*stencil_size+j, stencil2_2_5[j], var);
	}
      }
    } else if (ndim == 3) {
      for (var = 0; var < pade_nvars[opID]; var++) {
	for (j = 0; j < stencil_size; j++) {
	  if (dir == 1) ierr += HYPRE_SStructStencilSetEntry(pade_stencil[opID], var*stencil_size+j, stencil3_1_5[j], var);
	  if (dir == 2) ierr += HYPRE_SStructStencilSetEntry(pade_stencil[opID], var*stencil_size+j, stencil3_2_5[j], var);
	  if (dir == 3) ierr += HYPRE_SStructStencilSetEntry(pade_stencil[opID], var*stencil_size+j, stencil3_3_5[j], var);
	}
      }
    }
  }
  check_pade_hypre_ierr(ierr, __LINE__, __FILE__);

  /* set up the graph */
  ierr += HYPRE_SStructGraphCreate(comm, pade_grid[opID], &(pade_graph[opID]));
  check_pade_hypre_ierr(ierr, __LINE__, __FILE__);
  for (part = 0; part < pade_nparts[opID]; part++) {
    var = 0; ierr += HYPRE_SStructGraphSetStencil(pade_graph[opID], part, var, pade_stencil[opID]);
  }
  check_pade_hypre_ierr(ierr, __LINE__, __FILE__);
  ierr += HYPRE_SStructGraphAssemble(pade_graph[opID]);
  check_pade_hypre_ierr(ierr, __LINE__, __FILE__);

  /* Create the operator A */
  ierr += HYPRE_SStructMatrixCreate(comm, pade_graph[opID], &(pade_A[opID]));
  check_pade_hypre_ierr(ierr, __LINE__, __FILE__);
  // object_type[opID] = HYPRE_SSTRUCT;
  object_type[opID] = HYPRE_PARCSR;
  ierr += HYPRE_SStructMatrixSetObjectType(pade_A[opID], object_type[opID]);
  check_pade_hypre_ierr(ierr, __LINE__, __FILE__);
  ierr += HYPRE_SStructMatrixInitialize(pade_A[opID]);
  check_pade_hypre_ierr(ierr, __LINE__, __FILE__);

  /* setup solution and rhs vectors */
  ierr += HYPRE_SStructVectorCreate(comm, pade_grid[opID], &(pade_b[opID]));
  ierr += HYPRE_SStructVectorCreate(comm, pade_grid[opID], &(pade_x[opID]));
  check_pade_hypre_ierr(ierr, __LINE__, __FILE__);

  /* Set the object type (by default HYPRE_SSTRUCT). This determines the
     data structure used to store the matrix.  If you want to use unstructured
     solvers, e.g. BoomerAMG, the object type should be HYPRE_PARCSR.
     If the problem is purely structured (with one part), you may want to use
     HYPRE_STRUCT to access the structured solvers. */
  ierr += HYPRE_SStructVectorSetObjectType(pade_b[opID], object_type[opID]);
  ierr += HYPRE_SStructVectorSetObjectType(pade_x[opID], object_type[opID]);
  ierr += HYPRE_SStructVectorInitialize(pade_b[opID]);
  ierr += HYPRE_SStructVectorInitialize(pade_x[opID]);
  check_pade_hypre_ierr(ierr, __LINE__, __FILE__);

#endif

}

void fill_hypre_pade_implicit_matrix_(int *nGridsGlobal, int *iGridGlobal, int *ng, int *region_nGrids, 
                                      int *myrank, int *numproc, MPI_Fint *fcomm, 
                                      int *fndim, int *fdir, int *fOp, int *is, int *ie, 
                                      int *fstencil_size,
                                      double *values_in, int *lhs_var_in, int *rhs_var_in,
                                      int *periodic)
{

#ifdef USE_HYPRE
  int     ierr = 0;
  int     var, part, dir;
  int     i, j, opID;
  int    *indices = NULL, *perIndices = NULL, *perStencil = NULL;
  int     ndim, nentries, stencil_size, nvalues, lhs_var, rhs_var;
  double *values = NULL, *perValues = NULL;

  /* this operator ID */
  opID = GET_OPID(*fOp, *ng, *region_nGrids, *fndim, *fdir);

  /* set the indices */
  ndim = (*fndim);
  dir = (*fdir);
  stencil_size = *fstencil_size;
  lhs_var = (*lhs_var_in)-1;
  rhs_var = (*rhs_var_in)-1;
  indices = (int *)malloc(stencil_size*sizeof(int));
  for (i = 0; i < stencil_size; i++) {
    indices[i] = rhs_var*stencil_size + i;
  }

  /* set the grid extents */
  int *ilower = NULL, *iupper = NULL;
  ilower = (int *)malloc(ndim*sizeof(int));
  iupper = (int *)malloc(ndim*sizeof(int));
  for (i = 0; i < ndim; i++) {
    ilower[i] = is[i]-1;
    iupper[i] = ie[i]-1;
  }

  /* fill in the values */
  for (part = 0; part < pade_nparts[opID]; part++) {
    ierr += HYPRE_SStructMatrixSetBoxValues(pade_A[opID], part, ilower, iupper, lhs_var, stencil_size, indices, values_in);
  }
  check_pade_hypre_ierr(ierr, __LINE__, __FILE__);

  if (periodic[dir-1] < 0) {
    part = 0;
    nvalues = 3;
    perIndices = (int *)malloc(ndim*sizeof(int));
    perValues  = (double *)malloc(stencil_size*sizeof(double));
    perStencil = (int *)malloc(stencil_size*sizeof(int));
    perIndices[0] = 0;
    perIndices[1] = 0;
    perIndices[2] = 0;
    perValues[0] = 1.0;
    perValues[1] = 0.125; 
    perValues[2] = 0.125;
    perStencil[0] = 0;
    perStencil[1] = 1;
    perStencil[2] = 2;
    ierr += HYPRE_SStructMatrixSetValues(pade_A[opID], part, perIndices, lhs_var, nvalues, perStencil, perValues);
    free(perValues); free(perIndices);
    printf("Here.\n");
  }


  free(indices);
  free(ilower);
  free(iupper);
#endif

}

void fill_hypre_pade_implicit_rhs_vector_(int *nGridsGlobal, int *iGridGlobal, int *ng, int *region_nGrids, int *myrank, int *numproc, MPI_Fint *fcomm, 
                                          int *fndim, int *fdir, int *fOp, int *is, int *ie, double *buf)
{

#ifdef USE_HYPRE

  /* fill in the values */
  int     ierr = 0;
  int     var, part;
  int     i, j;
  int    *indices = NULL;
  int     ndim, nentries, nvalues, opID;

  /* this operator ID */
  opID = GET_OPID(*fOp, *ng, *region_nGrids, *fndim, *fdir);

  /* set the indices */
  ndim = (*fndim);

  /* set the values */
  nvalues = 1;
  for (i = 0; i < ndim; i++) nvalues *= (ie[i]-is[i]+1);

  /* set the grid extents */
  int *ilower = NULL, *iupper = NULL;
  ilower = (int *)malloc(ndim*sizeof(int));
  iupper = (int *)malloc(ndim*sizeof(int));
  for (i = 0; i < ndim; i++) {
    ilower[i] = is[i]-1;
    iupper[i] = ie[i]-1;
  }

  /* fill in the values for b */
  for (part = 0; part < pade_nparts[opID]; part++) {
    for (var = 0; var < pade_nvars[opID]; var++) {
      ierr += HYPRE_SStructVectorSetBoxValues(pade_b[opID], part, ilower, iupper, var, buf);
    }
  }
  check_pade_hypre_ierr(ierr, __LINE__, __FILE__);
  
  /* assemble the vectors */
  ierr += HYPRE_SStructVectorAssemble(pade_b[opID]);
  check_pade_hypre_ierr(ierr, __LINE__, __FILE__);
 
  free(ilower);
  free(iupper);

#endif

}

void fill_hypre_pade_implicit_soln_vector_(int *nGridsGlobal, int *iGridGlobal, int *ng, int *region_nGrids, int *myrank, int *numproc, MPI_Fint *fcomm, 
                                           int *fndim, int *fdir, int *fOp, int *is, int *ie, double *values)
{

#ifdef USE_HYPRE

  /* fill in the values */
  int     ierr = 0;
  int     var, part;
  int     i, j;
  int    *indices = NULL;
  int     ndim, nentries, stencil_size, nvalues, opID;

  /* this operator ID */
  opID = GET_OPID(*fOp, *ng, *region_nGrids, *fndim, *fdir);

  /* set the indices */
  ndim = (*fndim);

  /* set the values */
  nvalues = 1;
  for (i = 0; i < ndim; i++) nvalues *= (ie[i]-is[i]+1);

  /* set the grid extents */
  int *ilower = NULL, *iupper = NULL;
  ilower = (int *)malloc(ndim*sizeof(int));
  iupper = (int *)malloc(ndim*sizeof(int));
  for (i = 0; i < ndim; i++) {
    ilower[i] = is[i]-1;
    iupper[i] = ie[i]-1;
  }

  /* fill in the values for x */
  for (part = 0; part < pade_nparts[opID]; part++) {
    var = 0;
    ierr += HYPRE_SStructVectorSetBoxValues(pade_x[opID], part, ilower, iupper, var, values);
  }
  check_pade_hypre_ierr(ierr, __LINE__, __FILE__);
 
  /* assemble the vectors */
  ierr += HYPRE_SStructVectorAssemble(pade_x[opID]);
  check_pade_hypre_ierr(ierr, __LINE__, __FILE__);
 
  free(ilower);
  free(iupper);

#endif

}

void hypre_setup_solve_pade_implicit_(int *nGridsGlobal, int *iGridGlobal, int *ng, int *region_nGrids, int *myrank, int *numproc, MPI_Fint *fcomm, 
                                      int *fndim, int *fdir, int *fOp, int *is, int *ie)
{

#ifdef USE_HYPRE

  /* fill in the values */
  int      ierr = 0;
  int      var, part;
  double  *values = NULL;
  int      i, j;
  int     *indices = NULL;
  int      ndim, nentries, stencil_size, nvalues;
  MPI_Comm comm;
  double   final_res_norm;
  int      n_pre, n_post, opID;

  /* this operator ID */
  opID = GET_OPID(*fOp, *ng, *region_nGrids, *fndim, *fdir);

  /* set the dimension */
  ndim = (*fndim);

  /* convert communicator */
  comm = (MPI_Comm)MPI_Comm_f2c(*fcomm);

  /* set solver */
  ierr += HYPRE_SStructGMRESCreate(comm, &(pade_solver[opID]));
  check_pade_hypre_ierr(ierr, __LINE__, __FILE__);

  /* GMRES parameters */
  ierr += HYPRE_SStructGMRESSetMaxIter(pade_solver[opID], 5000);
  check_pade_hypre_ierr(ierr, __LINE__, __FILE__);
  ierr += HYPRE_SStructGMRESSetTol(pade_solver[opID], 1.0e-14);
  check_pade_hypre_ierr(ierr, __LINE__, __FILE__);
  ierr += HYPRE_SStructGMRESSetAbsoluteTol(pade_solver[opID], 1.0e-15);
  check_pade_hypre_ierr(ierr, __LINE__, __FILE__);
  ierr += HYPRE_SStructGMRESSetPrintLevel(pade_solver[opID], 0);
  check_pade_hypre_ierr(ierr, __LINE__, __FILE__);
  ierr += HYPRE_SStructGMRESSetLogging(pade_solver[opID], 0);
  check_pade_hypre_ierr(ierr, __LINE__, __FILE__);
  // ierr += HYPRE_SStructGMRESSetKDim(pade_solver[opID],5);
  // check_pade_hypre_ierr(ierr, __LINE__, __FILE__);

#if 0
  /* use SysPFMG as precondititioner */
  HYPRE_SStructSysPFMGCreate(comm, &(pade_pc[opID]));

  /* Set sysPFMG parameters */
  n_pre  = 1;
  n_post = 1;
  HYPRE_SStructSysPFMGSetTol(pade_pc[opID], 0.0);
  HYPRE_SStructSysPFMGSetMaxIter(pade_pc[opID], 1);
  HYPRE_SStructSysPFMGSetNumPreRelax(pade_pc[opID], n_pre);
  HYPRE_SStructSysPFMGSetNumPostRelax(pade_pc[opID], n_post);
  HYPRE_SStructSysPFMGSetPrintLevel(pade_pc[opID], 0);
  HYPRE_SStructSysPFMGSetZeroGuess(pade_pc[opID]);

  /* Set the preconditioner*/
  HYPRE_SStructGMRESSetPrecond(pade_solver[opID], HYPRE_SStructSysPFMGSolve, HYPRE_SStructSysPFMGSetup, pade_pc[opID]);
#endif

#if 0
  n_pre  = 1;
  n_post = 1;
  HYPRE_StructSMGCreate(comm, &(pade_pc[opID]));
  HYPRE_StructSMGSetMemoryUse(pade_pc[opID], 0);
  HYPRE_StructSMGSetMaxIter(pade_pc[opID], 1);
  HYPRE_StructSMGSetTol(pade_pc[opID], 0.0);
  HYPRE_StructSMGSetZeroGuess(pade_pc[opID]);
  HYPRE_StructSMGSetNumPreRelax(pade_pc[opID], n_pre);
  HYPRE_StructSMGSetNumPostRelax(pade_pc[opID], n_post);
  HYPRE_StructSMGSetPrintLevel(pade_pc[opID], 0);
  HYPRE_StructSMGSetLogging(pade_pc[opID], 0);
  HYPRE_StructGMRESSetPrecond(pade_solver[opID], HYPRE_StructSMGSolve, HYPRE_StructSMGSetup, pade_pc[opID]);
#endif 

#if 0
  /* use two-step Jacobi as preconditioner */
  HYPRE_StructJacobiCreate(comm, &(pade_pc[opID]));
  HYPRE_StructJacobiSetMaxIter(pade_pc[opID], 2);
  HYPRE_StructJacobiSetTol(pade_pc[opID], 0.0);
  HYPRE_StructJacobiSetZeroGuess(pade_pc[opID]);
  HYPRE_StructGMRESSetPrecond(pade_solver[opID], HYPRE_StructJacobiSolve, HYPRE_StructJacobiSetup, pade_pc[opID]);
#endif

#if 0
  pade_pc[opID] = NULL;
  HYPRE_SStructGMRESSetPrecond(pade_solver[opID], HYPRE_StructDiagScale, HYPRE_StructDiagScaleSetup, pade_pc[opID]);
#endif 

  /* do the setup */
  ierr += HYPRE_SStructGMRESSetup(pade_solver[opID], pade_A[opID], pade_b[opID], pade_x[opID]);
  check_pade_hypre_ierr(ierr, __LINE__, __FILE__);

  // HYPRE_StructJacobiDestroy(pc[opID]);

#endif 

}

void assemble_hypre_pade_implicit_matrix_(int *nGridsGlobal, int *iGridGlobal, int *ng, int *region_nGrids, 
                                          int *myrank, int *numproc, MPI_Fint *fcomm, int *fOp, int *fndim, int *fdir, int *per)
{

#ifdef USE_HYPRE

  int ierr = 0, opID;

  /* this operator ID */
  opID = GET_OPID(*fOp, *ng, *region_nGrids, *fndim, *fdir);

  /* assemble the matrix */
  ierr += HYPRE_SStructMatrixAssemble(pade_A[opID]);  
  check_pade_hypre_ierr(ierr, __LINE__, __FILE__);

  // if (*fdir == 1)  ierr += HYPRE_SStructMatrixPrint("Af.dat",pade_A[opID],0);

#endif

}

void hypre_update_pade_implicit_soln_vector_(int *nGridsGlobal, int *iGridGlobal, int *ng, int *region_nGrids, int *myrank, int *numproc, MPI_Fint *fcomm, 
                                             int *fndim, int *fdir, int *fOp, int *is, int *ie, double *q)
{

#ifdef USE_HYPRE

  /* fill in the values */
  int     ierr = 0;
  int     var, part;
  int     i, j;
  int    *indices = NULL;
  int     ndim, nentries, stencil_size, nvalues, opID;
  double  fac;

  /* this operator ID */
  opID = GET_OPID(*fOp, *ng, *region_nGrids, *fndim, *fdir);

  /* set the indices */
  ndim = (*fndim);

  /* set the grid extents */
  int *ilower = NULL, *iupper = NULL;
  ilower = (int *)malloc(ndim*sizeof(int));
  iupper = (int *)malloc(ndim*sizeof(int));
  for (i = 0; i < ndim; i++) {
    ilower[i] = is[i]-1;
    iupper[i] = ie[i]-1;
  }

  /* fill in the values for b */
  for (part = 0; part < pade_nparts[opID]; part++) {
    for (var = 0; var < pade_nvars[opID]; var++) {
      ierr += HYPRE_SStructVectorGetBoxValues(pade_x[opID], part, ilower, iupper, var, q);
      if (ierr != 0) printf("opID = %d, part = %d, var = %d, ierr = %d\n", opID, part, var, ierr);
    }
  }
  check_pade_hypre_ierr(ierr, __LINE__, __FILE__);
  
  free(ilower);
  free(iupper);

#endif

}


void hypre_sstruct_solve_pade_implicit_(int *nGridsGlobal, int *iGridGlobal, int *ng, int *region_nGrids, int *myrank, int *numproc, MPI_Fint *fcomm, 
                                int *fndim, int *fdir, int *fOp, int *is, int *ie, int *its)
{

#ifdef USE_HYPRE

  /* fill in the values */
  int      ierr = 0;
  int      var, part;
  double  *values = NULL;
  int      i, j;
  int     *indices = NULL;
  int      ndim, nentries, stencil_size, nvalues;
  MPI_Comm comm;
  double   final_res_norm;
  int      n_pre, n_post, opID;

  /* this operator ID */
  opID = GET_OPID(*fOp, *ng, *region_nGrids, *fndim, *fdir);

  /* do the solve */
  ierr += HYPRE_SStructGMRESSolve(pade_solver[opID], pade_A[opID], pade_b[opID], pade_x[opID]);
  check_pade_hypre_ierr(ierr, __LINE__, __FILE__);

  /* final residual */
  ierr += HYPRE_SStructGMRESGetFinalRelativeResidualNorm(pade_solver[opID], &final_res_norm);
  check_pade_hypre_ierr(ierr, __LINE__, __FILE__);

  /* number of interations */
  ierr += HYPRE_SStructGMRESGetNumIterations(pade_solver[opID], its);
  check_pade_hypre_ierr(ierr, __LINE__, __FILE__);

#endif 

}


void hypre_parcsr_solve_pade_implicit_(int *nGridsGlobal, int *iGridGlobal, int *ng, int *region_nGrids, int *myrank, int *numproc, MPI_Fint *fcomm, 
                                       int *fndim, int *fdir, int *fOp, int *is, int *ie, int *its)
{

#ifdef USE_HYPRE
  /* fill in the values */
  int                   ierr = 0;
  int                   var, part;
  double               *values = NULL;
  int                   i, j;
  int                  *indices = NULL;
  int                   ndim, nentries, stencil_size, nvalues;
  MPI_Comm              comm;
  double                final_res_norm;
  int                   n_pre, n_post, opID;

  /* this operator ID */
  opID = GET_OPID(*fOp, *ng, *region_nGrids, *fndim, *fdir);

  /* convert communicator */
  comm = (MPI_Comm)MPI_Comm_f2c(*fcomm);

  /* Solve */
  HYPRE_ParCSRGMRESSolve(pade_par_solver[opID], pade_par_A[opID], pade_par_b[opID], pade_par_x[opID]);

  /* get some info */
  HYPRE_GMRESGetNumIterations(pade_par_solver[opID], its);
  HYPRE_GMRESGetFinalRelativeResidualNorm(pade_par_solver[opID], &final_res_norm);

  /* Gather the solution vector. */
  HYPRE_SStructVectorGather(pade_x[opID]);

  /* clean up */
  // ierr += HYPRE_ParCSRGMRESDestroy(pade_par_solver[opID]);
  // ierr += HYPRE_EuclidDestroy(pade_eu[opID]);
  // check_hypre_ierr(ierr, __LINE__, __FILE__);

#endif

}

void hypre_parcsr_setup_pade_implicit_(int *nGridsGlobal, int *iGridGlobal, int *ng, 
                                       int *region_nGrids, int *myrank, int *numproc, MPI_Fint *fcomm, 
                                       int *fndim, int *fdir, int *fOp, double *alphaf)
{

#ifdef USE_HYPRE
  /* fill in the values */
  int                   ierr = 0;
  int                   var, part;
  double               *values = NULL;
  int                   i, j;
  int                  *indices = NULL;
  int                   ndim, nentries, stencil_size, nvalues;
  MPI_Comm              comm;
  double                final_res_norm;
  int                   n_pre, n_post, opID;

  /* this operator ID */
  opID = GET_OPID(*fOp, *ng, *region_nGrids, *fndim, *fdir);

  /* convert communicator */
  comm = (MPI_Comm)MPI_Comm_f2c(*fcomm);

  /* get the CSR-representations of A, b, x */
  HYPRE_SStructMatrixGetObject(pade_A[opID], (void **) &(pade_par_A[opID]));
  HYPRE_SStructVectorGetObject(pade_b[opID], (void **) &(pade_par_b[opID]));
  HYPRE_SStructVectorGetObject(pade_x[opID], (void **) &(pade_par_x[opID]));

  /* Euclid preconditioned GMRES */
  /* set solver */
  HYPRE_ParCSRGMRESCreate(comm, &(pade_par_solver[opID]));

  /* set the GMRES paramaters */
  HYPRE_ParCSRGMRESSetKDim(pade_par_solver[opID], 10);
  HYPRE_ParCSRGMRESSetMaxIter(pade_par_solver[opID], 10000);
  HYPRE_ParCSRGMRESSetTol(pade_par_solver[opID], 1.0e-14);
  HYPRE_ParCSRGMRESSetAbsoluteTol(pade_par_solver[opID], 1.0e-15);
  HYPRE_ParCSRGMRESSetPrintLevel(pade_par_solver[opID], 0);

  /* set the preconditioner */
  if (fabs(*alphaf) > 1e-6) {
    HYPRE_EuclidCreate(comm, &(pade_eu[opID]));
    HYPRE_EuclidSetLevel(pade_eu[opID], 8);
    HYPRE_EuclidSetBJ(pade_eu[opID], 0);

    /* set the preconditioner */
    HYPRE_ParCSRGMRESSetPrecond(pade_par_solver[opID], HYPRE_EuclidSolve, HYPRE_EuclidSetup, pade_eu[opID]);
  }

  /* do the setup */
  HYPRE_ParCSRGMRESSetup(pade_par_solver[opID], pade_par_A[opID], pade_par_b[opID], pade_par_x[opID]);

#endif

}

void assemble_hypre_pade_vectors_(int *nGridsGlobal, int *iGridGlobal, int *ng, int *region_nGrids, 
                                  int *myrank, int *numproc, MPI_Fint *fcomm, int *fOp, int *fndim, int *fdir)
{

#ifdef USE_HYPRE

  int ierr = 0, opID;

  /* this operator ID */
  opID = GET_OPID(*fOp, *ng, *region_nGrids, *fndim, *fdir);

  /* assemble the vectors */
  ierr += HYPRE_SStructVectorAssemble(pade_x[opID]);
  ierr += HYPRE_SStructVectorAssemble(pade_b[opID]);
  check_pade_hypre_ierr(ierr, __LINE__, __FILE__);

#endif

}

void hypre_sstruct_setup_pade_implicit_(int *nGridsGlobal, int *iGridGlobal, int *ng, 
                                        int *region_nGrids, int *myrank, int *numproc, MPI_Fint *fcomm, 
                                        int *fndim, int *fdir, int *fOp, double *alphaf)
{

#ifdef USE_HYPRE
  /* fill in the values */
  int                   ierr = 0;
  int                   var, part;
  double               *values = NULL;
  int                   i, j;
  int                  *indices = NULL;
  int                   ndim, nentries, stencil_size, nvalues;
  MPI_Comm              comm;
  double                final_res_norm;
  int                   n_pre, n_post, opID;

  /* this operator ID */
  opID = GET_OPID(*fOp, *ng, *region_nGrids, *fndim, *fdir);

  /* convert communicator */
  comm = (MPI_Comm)MPI_Comm_f2c(*fcomm);

  /* Euclid preconditioned GMRES */
  /* set solver */
  HYPRE_SStructGMRESCreate(comm, &(pade_solver[opID]));

  /* set the GMRES paramaters */
  HYPRE_SStructGMRESSetKDim(pade_solver[opID], 10);
  HYPRE_SStructGMRESSetMaxIter(pade_solver[opID], 10000);
  HYPRE_SStructGMRESSetTol(pade_solver[opID], 1.0e-14);
  HYPRE_SStructGMRESSetAbsoluteTol(pade_solver[opID], 1.0e-15);
  HYPRE_SStructGMRESSetPrintLevel(pade_solver[opID], 0);

  /* do the setup */
  HYPRE_SStructGMRESSetup(pade_solver[opID], pade_A[opID], pade_b[opID], pade_x[opID]);

#endif

}

void hypre_setup_pade_implicit_(int *nGridsGlobal, int *iGridGlobal, int *ng, 
                                int *region_nGrids, int *myrank, int *numproc, MPI_Fint *fcomm, 
                                int *fndim, int *fdir, int *fOp, double *alphaf)
{

  /* this operator ID */
  int opID = GET_OPID(*fOp, *ng, *region_nGrids, *fndim, *fdir);

  if (object_type[opID] == HYPRE_SSTRUCT) {
     hypre_sstruct_setup_pade_implicit_(nGridsGlobal, iGridGlobal, ng, 
                                        region_nGrids, myrank, numproc, fcomm, 
                                        fndim, fdir, fOp, alphaf);
  } else if (object_type[opID] == HYPRE_PARCSR) {
     hypre_parcsr_setup_pade_implicit_(nGridsGlobal, iGridGlobal, ng, 
                                       region_nGrids, myrank, numproc, fcomm, 
                                       fndim, fdir, fOp, alphaf);
  } else {
     printf("ERROR: Unknown object_type.\n");
     check_pade_hypre_ierr(1, __LINE__, __FILE__);
  }

  return;

}


void hypre_solve_pade_implicit_(int *nGridsGlobal, int *iGridGlobal, int *ng, int *region_nGrids, int *myrank, 
                                int *numproc, MPI_Fint *fcomm, int *fndim, int *fdir, int *fOp, 
                                int *is, int *ie, int *its)
{

  /* this operator ID */
  int opID = GET_OPID(*fOp, *ng, *region_nGrids, *fndim, *fdir);

  if (object_type[opID] == HYPRE_SSTRUCT) {
    hypre_sstruct_solve_pade_implicit_(nGridsGlobal, iGridGlobal, ng, region_nGrids, myrank, numproc, fcomm, 
                                       fndim, fdir, fOp, is, ie, its);
  } else if (object_type[opID] == HYPRE_PARCSR) {
    hypre_parcsr_solve_pade_implicit_(nGridsGlobal, iGridGlobal, ng, region_nGrids, myrank, numproc, fcomm, 
                                      fndim, fdir, fOp, is, ie, its);
  } else {
     printf("ERROR: Unknown object_type.\n");
     check_pade_hypre_ierr(1, __LINE__, __FILE__);
  }

  return;

}
