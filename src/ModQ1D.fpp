! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
!----------------------------------------------------------------------
!
! ModQ1D.f90
! 
! - basic code for the evaluation of the quasi-1D Navier-Stokes equations
!
! Revision history
! - 17 Aug 2009 : DJB : initial code
! 
! flags == FALSE    : compute inviscid fluxes only
! flags == TRUE     : compute dissipative fluxes only
! flags == COMBINED : inviscid + dissipative fluxes
!
! $Header: /cvsroot/genx/Codes/RocfloCM/Source/ModQ1D.f90,v 1.10 2011/02/02 04:12:10 bodony Exp $
!
!-----------------------------------------------------------------------
MODULE ModQ1D

CONTAINS

  subroutine Q1D_RHS(region, ng, flags)

    USE ModGlobal
    USE ModDataStruct
    USE ModDeriv
    USE ModMPI

    Implicit None

    ! ... local variables
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    type(t_mixt_input), pointer :: input
    type(t_region), pointer :: region
    type(t_mpi_timings), pointer :: mpi_timings
    real(rfreal) :: timer, Hstar, cf, cDelta, H, Ret, thetastar
    integer :: flags, ng, i, k, Nc, ND
    real(rfreal), pointer :: flux(:), dflux(:), dareadx(:), UVWhat(:), dUdx(:)

    ! ... simplicity
    input       => region%input
    grid        => region%grid(ng)
    state       => region%state(ng)
    mpi_timings => region%mpi_timings(ng)
    Nc          = grid%nCells
    ND          = grid%ND

    ! ... buffers
    allocate(flux(Nc),dflux(Nc),dareadx(Nc))

    ! ... area : d{A}/dx
    do i = 1, Nc
      flux(i) = grid%area(i)
    end do
    call APPLY_OPERATOR(region, ng, input%iFirstDeriv, 1, flux, dflux, .FALSE.)
    do i = 1, Nc
      dareadx(i) = dflux(i) * grid%JAC(i)
    end do

    ! ... continuity : d{rho}/dt = -1/A d{rho U A}/dx - rho/A d{A}/dt + mdot
    do i = 1, Nc
      flux(i) = state%cv(i,2) * grid%area(i)
    end do
    call APPLY_OPERATOR(region, ng, input%iFirstDeriv, 1, flux, dflux, .FALSE.)
    do i = 1, Nc
      state%rhs(i,1) = state%rhs(i,1) - dflux(i) / grid%area(i) * grid%JAC(i)
    end do

    ! ... momentum : d{rho u}/dt = -1/A d{(rho U U + p)A}/dx - (rho U)/A d{A}/dt + p/A d{A}/dx + F
    do i = 1, Nc
      flux(i) = (state%cv(i,2)**2 * state%dv(i,3) + state%dv(i,1)) * grid%area(i)
    end do
    call APPLY_OPERATOR(region, ng, input%iFirstDeriv, 1, flux, dflux, .FALSE.)
    do i = 1, Nc
      state%rhs(i,2) = state%rhs(i,2) - dflux(i) / grid%area(i) * grid%JAC(i) + state%dv(i,1) * dareadx(i) / grid%area(i)
    end do

    ! ... Energy : d{rho E}/dt = -1/A d{(rho E + p) U A}/dx - (rho E + p)/A d{A}/dt + Q
    do i = 1, Nc
      flux(i) = (state%cv(i,3) + state%dv(i,1)) * state%cv(i,2) * state%dv(i,3) * grid%area(i)
    end do
    call APPLY_OPERATOR(region, ng, input%iFirstDeriv, 1, flux, dflux, .FALSE.)
    do i = 1, Nc
      state%rhs(i,3) = state%rhs(i,3) - dflux(i) / grid%area(i) * grid%JAC(i)
    end do

    ! ... level Set, if present
    if (associated(state%levelSet) .eqv. .true.) then
      do i = 1, Nc
        flux(i) = state%levelSet(i)
      end do
      call APPLY_OPERATOR(region, ng, input%iFirstDeriv, 1, flux, dflux, .FALSE.)
      do i = 1, Nc
        state%rhs_levelSet(i) = -dflux(i) * grid%JAC(i) * state%cv(i,2) / state%cv(i,1)
      end do
    end if

    ! ... auxillary variables, if they are present
    if (associated(state%auxVars) .eqv. .true.) then
 
      ! ... d{delta^*}/dt = ... - delta^* d{rho_e u_e}/dt
      do i = 1, Nc
        state%rhs_auxVars(i,1) = - state%rhs_auxVars(i,1) * state%rhs(i,2)
      end do

      ! ... d{delta^*}/dt = ... - (1/R) d{R rho_e u_e^2 theta}/dx
      do i = 1, Nc
        flux(i) = sqrt(grid%area(i)/(0.5_dp*TWOPI)) * state%cv(i,2)**2 * state%dv(i,3) * state%auxVars(i,2)
      end do
      call APPLY_OPERATOR(region, ng, input%iFirstDeriv, 1, flux, dflux, .FALSE.)
      do i = 1, Nc
        state%rhs_auxVars(i,1) = state%rhs_auxVars(i,1) - (dflux(i)*grid%JAC(i)) / (sqrt(grid%area(i)/(0.5_dp*TWOPI)))
      end do

      ! ... d{delta^*}/dt = ... - rho_e u_e delta^* d{u_e}/dx
      do i = 1, Nc
        flux(i) = state%cv(i,2) * state%dv(i,3)
      end do
      call APPLY_OPERATOR(region, ng, input%iFirstDeriv, 1, flux, dflux, .FALSE.)
      do i = 1, Nc
        state%rhs_auxVars(i,1) = state%rhs_auxVars(i,1) - (dflux(i)*grid%JAC(i)) * state%cv(i,2) * state%auxVars(i,1)
      end do

      ! ... d{delta^*}/dt = ... + rho_e u_e^2 c_f/2
      ! ... d{theta  }/dt = ... + rho_e u_e^3 c_Delta 
      do i = 1, Nc

        ! ... shape factor
        H = state%auxVars(i,1) / (state%auxVars(i,2) + 10.0_dp * TINY)

        ! ... theta-based Reynolds number
        Ret = state%auxVars(i,2) * state%cv(i,2) / (state%tv(i,1)+1D-10)

        ! ... skin friction
        if (H <= 7.4_dp) then
          cf = 2.0_dp / (Ret + 10.0_dp * TINY) * ( -0.067_dp + 0.01977_dp * (7.4_dp - H)**2 / (H - 1.0_dp + 10.0_dp * TINY) ) 
        else
          cf = 2.0_dp / (Ret + 10.0_dp * TINY) * ( -0.067_dp + 0.02200_dp * (1.0_dp - 1.4_dp/(H - 6.0_dp + 10.0_dp * TINY))**2 )
        end if
        cf = 1.0_dp

        ! ... d{delta^*}/dt = ... + rho_e u_e^2 c_f/2
        state%rhs_auxVars(i,1) = state%rhs_auxVars(i,1) + state%cv(i,2)**2 * state%dv(i,3) * MIN(cf,1.0_rfreal) * 0.5_dp * state%REinv

!!$        ! ...kinetic energy shape factor, H^*
!!$        ! ... dissipation integral
!!$        if (H <= 4.0_dp) then
!!$          Hstar = 1.515_dp + 0.076_dp * (H - 4.0_dp)**2/(H + 10.0_8 * TINY)
!!$          cDelta = 0.207_dp + 0.00205_dp * (4.0_dp - H)**5.5_dp
!!$        else
!!$          Hstar = 1.515_dp + 0.040_dp * (H - 4.0_dp)**2/(H + 10.0_8 * TINY)
!!$          cDelta = 0.207_dp - 0.003_dp * (H - 4.0_dp)**2
!!$        end if
!!$        thetastar = Hstar * state%auxVars(i,2)
!!$        cDelta = cDelta * Hstar / (Ret + 10.0_8 * TINY) * 0.5_dp
!!$
!!$        ! ... d{theta  }/dt = ... + rho_e u_e^3 c_Delta 
!!$        state%rhs_auxVars(i,2) = state%rhs_auxVars(i,2) + state%cv(i,2)**3 * state%dv(i,3)**2 * cDelta

      end do

      ! ... normalize d{delta^*}/dt
      do i = 1, Nc
        state%rhs_auxVars(i,1) = state%rhs_auxVars(i,1) / (state%cv(i,2) + 10.0_dp * TINY)
      end do

      ! ... d{theta  }/dt = ... - rho_e u_e^3 d{delta^*}/dt
      do i = 1, Nc
        state%rhs_auxVars(i,2) = state%rhs_auxVars(i,2) - state%cv(i,2)**3 * state%dv(i,3)**2 * state%rhs_auxVars(i,1)
      end do

      ! ... d{theta  }/dt = ... - (theta + delta^*) u_e d{rho_e u_e}/dt
      do i = 1, Nc
        state%rhs_auxVars(i,2) = state%rhs_auxVars(i,2) - (state%auxVars(i,1) + state%auxVars(i,2)) * state%cv(i,2) * state%dv(i,3) * state%rhs(i,2)
      end do

      ! ... d{theta  }/dt = ... - (theta - delta^*) rho_e u_e d{u_e}/dt
      do i = 1, Nc
        state%rhs_auxVars(i,2) = state%rhs_auxVars(i,2) - (state%auxVars(i,1) - state%auxVars(i,2)) * state%cv(i,2) * (state%rhs(i,2)- state%cv(i,2) * state%dv(i,3) * state%rhs(i,1))
      end do

      ! ... d{theta}/dt = ... - (1/R) d{R rho_e u_e^3 theta^*}/dx
!!$      do i = 1, Nc
!!$        ! ... shape factor
!!$        H = state%auxVars(i,1)/(state%auxVars(i,2) + 10.0_8 * TINY)
!!$        ! ...kinetic energy shape factor, H^*
!!$        if (H <= 4.0_dp) then
!!$          Hstar = 1.515_dp + 0.076_dp * (H - 4.0_dp)**2/(H + 10.0_dp * TINY)
!!$        else
!!$          Hstar = 1.515_dp + 0.040_dp * (H - 4.0_dp)**2/(H + 10.0_dp * TINY)
!!$        end if
!!$        thetastar = Hstar * state%auxVars(i,2)
!!$        flux(i) = sqrt(grid%area(i)/(0.5_dp*TWOPI)) * state%cv(i,2)**3 * state%dv(i,3)**2 * Hstar
!!$      end do
!!$      call APPLY_OPERATOR(region, ng, input%iFirstDeriv, 1, flux, dflux, .FALSE.)
!!$      do i = 1, Nc
!!$        state%rhs_auxVars(i,2) = state%rhs_auxVars(i,2) + (dflux(i)*grid%JAC(i)) / (sqrt(grid%area(i)/(0.5_dp * TWOPI)))
!!$      end do

      ! ... normalize d{theta}/dt
      do i = 1, Nc
        state%rhs_auxVars(i,2) = state%rhs_auxVars(i,2) / (state%cv(i,2)**2 * state%dv(i,3) + 10.0_dp * TINY)
      end do

    end if

    if (input%RE <= 0.0_rfreal .and. input%shock == FALSE) then
      deallocate(flux,dflux,dareadx)
      return
    end if

    ! ... VISCOUS TERMS
    allocate(dUdx(Nc))
    do i = 1, Nc
      flux(i) = state%cv(i,2) * state%dv(i,3)
    end do
    call APPLY_OPERATOR(region, ng, input%iFirstDeriv, 1, flux, dflux, .FALSE.)
    do i = 1, Nc
      dUdx(i) = dflux(i) * grid%JAC(i)
    end do

    ! .... x-momentum & energy
    do i = 1, Nc
      flux(i) = (2.0_rfreal * state%tv(i,1) + state%tv(i,2))
      state%rhs(i,3) = state%rhs(i,3) + flux(i) * dUdx(i) * dUdx(i) * state%REinv
    end do
    call APPLY_OPERATOR(region, ng, input%iFirstDeriv, 1, flux, dflux, .FALSE.)
    do i = 1, Nc
      state%rhs(i,2) = state%rhs(i,2) + dUdx(i) * dflux(i) * grid%JAC(i) * state%REinv
      state%rhs(i,3) = state%rhs(i,3) + dUdx(i) * dflux(i) * grid%JAC(i) * state%REinv * state%cv(i,2) * state%dv(i,3)
    end do
    do i = 1, Nc
      flux(i)  = state%cv(i,2) * state%dv(i,3)
      dflux(i) = 0.0_rfreal
    end do
    call SECOND_DERIV(region, ng, 1, 1, flux, dflux, 1.0_rfreal, .FALSE.)
    do i = 1, Nc
      state%rhs(i,2) = state%rhs(i,2) + (2.0_rfreal * state%tv(i,1) + state%tv(i,2)) * dflux(i) * state%REinv
      state%rhs(i,3) = state%rhs(i,3) + (2.0_rfreal * state%tv(i,1) + state%tv(i,2)) * dflux(i) * state%cv(i,2) * state%dv(i,3) * state%REinv
    end do

    ! ... energy d{T}/dx
    do i = 1, Nc
      flux(i) = state%dv(i,2)
    end do
    call APPLY_OPERATOR(region, ng, input%iFirstDeriv, 1, flux, dflux, .FALSE.)
    do i = 1, Nc
      dUdx(i) = (dflux(i) * grid%JAC(i))
    end do

    ! ... energy d{kappa}/dx d{T}/dx
    do i = 1, Nc
      flux(i) = state%tv(i,3)
    end do
    call APPLY_OPERATOR(region, ng, input%iFirstDeriv, 1, flux, dflux, .FALSE.)
    do i = 1, Nc
      state%rhs(i,3) = state%rhs(i,3) + dflux(i) * grid%JAC(i) * dUdx(i) * state%REinv * state%PRinv
    end do

    ! ... energy kappa d^2{T}/dx^2
    do i = 1, Nc
      flux(i) = state%dv(i,2)
    end do
    call SECOND_DERIV(region, ng, 1, 1, flux, dflux, 1.0_rfreal, .FALSE.)
    do i = 1, Nc
      state%rhs(i,3) = state%rhs(i,3) + state%tv(i,3) * dflux(i) * state%REinv * state%PRinv
    end do

    deallocate(flux,dflux,dareadx,dUdx)

    return

  end subroutine Q1D_RHS

  subroutine Q1D_Timestep(region)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    ! ... global variables
    type(t_region), pointer :: region

    ! ... local variables
    integer :: i, j, k, jj, ng, ierr
    real(rfreal) :: UU(MAX_ND), absUU, snd_spd, metric_fac, fac0, dt_inv, dt_vis, dt
    real(rfreal) :: var, var_all, OSXI(MAX_ND), SXI(MAX_ND), dt_tmp(MAX_ND), THETA, RMU
    Type(t_grid), pointer :: grid
    Type(t_mixt), pointer :: state
    Type(t_mixt_input), pointer :: input
    real(rfreal), allocatable :: dt_local(:)
    real(rfreal) :: nu_star, numer, denom, dot_prod, ibfac

    ! ... allocate space for each grid on this processor
    allocate(dt_local(region%nGrids))

    ! ... simplicity
    input => region%input

    ! ... constant cfl mode
    if (input%cfl_mode == CONSTANT_CFL) then

      do ng = 1, region%nGrids

        state => region%state(ng)
        grid  => region%grid(ng)

        ! ... initialize
        state%cfl(:) = input%cfl

        ! ... inviscid timestep
        do k = 1, grid%nCells

          ! ... initialize
          numer = 0.0_rfreal
          denom = 0.0_rfreal
          ibfac = grid%ibfac(k)

          ! ... Cartesian velocity
          UU = 0.0_rfreal
          do i = 1, grid%ND
            UU(i) = state%dv(k,3) * state%cv(k,1+i) + grid%XI_TAU(k,i)
          end do
          do i = 1, grid%ND
            denom = denom + abs(DOT_PRODUCT(UU(1:grid%ND),grid%MT1(k,region%global%t2Map(i,1:grid%ND))))
          end do

          ! ... speed of sound
          snd_spd = sqrt(state%gv(k,1) * state%dv(k,1) * state%dv(k,3))

          dot_prod = 0.0_rfreal
          do i = 1, grid%ND**2
            dot_prod = dot_prod + grid%MT1(k,i) * grid%MT1(k,i)
          end do
          denom = denom + snd_spd * sqrt(dot_prod)

          ! ... put it all together
          dt_inv = grid%INVJAC(k) / (denom * ibfac + (1.0 - ibfac) * 1E-10) * ibfac
          dt_vis = dt_inv

          ! ... for the viscous terms (taken from FDL3DI)
          if (state%RE > 0.0_rfreal) then

            nu_star = max(state%tv(k,1)*state%REinv, state%tv(k,2)*state%REinv, state%tv(k,3)*state%REinv*state%PRinv)
            dot_prod = 0.0_rfreal
            do i = 1, grid%ND
              dot_prod = dot_prod + sqrt(DOT_PRODUCT(grid%MT1(k,region%global%t2Map(i,1:grid%ND)),grid%MT1(k,region%global%t2Map(i,1:grid%ND))))
            end do
            denom = denom + 2.0_rfreal * nu_star * grid%JAC(k) * dot_prod**2

            dt_vis = grid%INVJAC(k) / (denom * ibfac + (1.0 - ibfac) * 1E-10) * ibfac

          end if

          ! ... the timestep
          state%dt(k) = state%cfl(k) * MIN(dt_inv, dt_vis) * ibfac

        end do

        ! ... save the minimum dt for this grid
        dt_local(ng) = MINVAL(state%dt, MASK = state%dt .gt. 0.0_rfreal)

      end do

      call mpi_barrier(mycomm, ierr)

      ! ... find the minimum dt over all nodes
      var = MINVAL(dt_local, MASK = dt_local .gt. 0.0_rfreal)
      Call MPI_AllReduce(var, var_all, 1, MPI_REAL8, MPI_MIN, mycomm, ierr)

      ! ... copy to all grids on this processor
      do ng = 1, region%nGrids
        state => region%state(ng)
        state%dt(:) = var_all
      end do

    else if (input%cfl_mode == CONSTANT_DT) then

      do ng = 1, region%nGrids

        state => region%state(ng)
        grid  => region%grid(ng)

        ! ... initialize
        state%dt = input%dt

        ! ... inviscid timestep
        do k = 1, grid%nCells

          ! ... initialize
          numer = 0.0_rfreal
          denom = 0.0_rfreal
          ibfac = grid%ibfac(i)

          ! ... Cartesian velocity
          UU = 0.0_rfreal
          do i = 1, grid%ND
            UU(i) = state%dv(k,3) * state%cv(k,1+i) + grid%XI_TAU(k,i)
          end do
          do i = 1, grid%ND
            denom = denom + abs(DOT_PRODUCT(UU(1:grid%ND),grid%MT1(k,region%global%t2Map(i,1:grid%ND))))
          end do

          ! ... speed of sound
          snd_spd = sqrt(state%gv(k,1) * state%dv(k,1) * state%dv(k,3))

          dot_prod = 0.0_rfreal
          do i = 1, grid%ND
            dot_prod = dot_prod + DOT_PRODUCT(grid%MT1(k,region%global%t2Map(i,1:grid%ND)), grid%MT1(k,region%global%t2Map(i,1:grid%ND)))
          end do
          denom = denom + snd_spd * sqrt(dot_prod)

          ! ... put it all together
          dt_inv = grid%INVJAC(k) / (denom * ibfac + (1.0 - ibfac) * 1E-10) * ibfac
          dt_vis = dt_inv

          ! ... for the viscous terms (taken from FDL3DI)
          if (state%RE > 0.0_rfreal) then

            nu_star = max(state%tv(k,1)*state%REinv, state%tv(k,2)*state%REinv, state%tv(k,3)*state%REinv*state%PRinv)
            dot_prod = 0.0_rfreal
            do i = 1, grid%ND
              dot_prod = dot_prod + sqrt(DOT_PRODUCT(grid%MT1(k,region%global%t2Map(i,1:grid%ND)),grid%MT1(k,region%global%t2Map(i,1:grid%ND))))
            end do
            denom = denom + 2.0_rfreal * nu_star * grid%JAC(k) * dot_prod**2

            dt_vis = grid%INVJAC(k) / (denom * ibfac + (1.0 - ibfac) * 1E-10) * ibfac

          end if

          ! ... the cfl
          state%cfl(k) = state%dt(k) / MIN(dt_inv,dt_vis) * ibfac

        end do

        ! ... save the maximum cfl for this grid
        dt_local(ng) = MAXVAL(state%cfl)

      end do

      ! ... find the maximum cfl over all nodes
      var = MAXVAL(dt_local)
      Call MPI_AllReduce(var, var_all, 1, MPI_DOUBLE_PRECISION, MPI_MAX, mycomm, ierr)

      ! ... copy the cfl back to all the nodes
      do ng = 1, region%nGrids
        state => region%state(ng)
        state%cfl(:) = var_all
      end do

    end if

    deallocate(dt_local)

  end subroutine Q1D_Timestep

  subroutine Q1D_BC_Fix_Value(region)

    USE ModGlobal
    USE ModDataStruct
    USE ModDeriv
    USE ModMPI
    USE ModMatrixVectorOps

    Implicit None

    TYPE (t_region), POINTER :: region
    INTEGER :: ng
    INTEGER :: flags

    ! ... local variables
    type(t_patch), pointer :: patch
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    type(t_mixt_input), pointer :: input
    real(rfreal), pointer :: UVWhat(:,:), gvec(:), var(:)
    integer :: ND, Nc, N(MAX_ND), npatch, i, j, l0, k, jj, l1, l2, isign, lp, kk
    real(rfreal) :: UU(MAX_ND), norm(MAX_ND), mag_norm, tang(MAX_ND-1,MAX_ND)
    real(rfreal) :: mag_tang, constant, del, eta, rel_pos(MAX_ND), dsign
    integer :: normDir, tangDir(MAX_ND,MAX_ND-1), Np(MAX_ND)
    real(rfreal) :: d(5), wave_amp(5), wave_spd(5), spd_snd
    real(rfreal) :: spvol, un, ut(MAX_ND-1)
    real(rfreal) :: norm_vec(MAX_ND), tang_vec(2,MAX_ND), fvec(MAX_ND,2)
    real(rfreal) :: metricInverse(MAX_ND,MAX_ND), invProj(MAX_ND, MAX_ND)
    real(rfreal) :: sponge_amp, bndry_q(MAX_ND+2), bndry_p(MAX_ND+2), bndry_rhs(MAX_ND+2)
    integer :: sgn, handedness(MAX_ND), alpha
    integer, pointer :: inorm(:)
    real(rfreal), pointer :: dudn(:), drdn(:), dpdn(:), flux(:), drudn(:)
    real(rfreal) :: bndry_norm(MAX_ND), p_inf, u_inf, du_inf, un_old, metric_fac, T_stag, T_inf, dT_inf
    real(rfreal) :: T_wall, p_wall, V_wall
    real(rfreal) :: UVEL, VVEL, WVEL, CUVEL(5), CVVEL(5), CWVEL(5), PRESSURE, RHO
    real(rfreal) :: RREF, UREF, VREF, WREF, PREF, pert(5), charVar(5)
    real(rfreal) :: UX, UY, UZ, XI_X, XI_Y, XI_Z, SPD_SND_INV, KE, ENTHALPY, MACH2, GAMMA, V_DOT_XI
    real(rfreal) :: XI_X_TILDE, XI_Y_TILDE, XI_Z_TILDE, dsgn, denom, ui, Mi, Ti, T_inflow, u_inflow
    real(rfreal) :: w_of_t, t0
    real(rfreal) :: radNozz, Tc, u0, r0, coefVel, P2, RHO2, dRHO, dPRESSURE, lhs_fac, rhs_fac

    tangDir(1,:) = (/2, 3/)
    tangDir(2,:) = (/1, 3/)
    tangDir(3,:) = (/1, 2/)
    handedness = (/1, -1, 1/)

    ! ... simplicity
    input => region%input
    ND = input%ND

    allocate(gvec(size(input%bndry_explicit_deriv)))
    allocate(inorm(size(gvec)),var(size(gvec)))

    do npatch = 1, region%nPatches

      patch => region%patch(npatch)
      grid => region%grid(patch%gridID)
      state => region%state(patch%gridID)
      normDir = abs(patch%normDir)
      sgn = normDir / patch%normDir
      dsgn = dble(sgn)

      Nc = grid%nCells
      N  = 1; Np = 1;
      do j = 1, grid%ND
        N(j)  = grid%ie(j)-grid%is(j)+1
        Np(j) = patch%ie(j) - patch%is(j) + 1
      end do;

      if ( patch%bcType /= SPONGE ) then

        Do k = patch%is(3), patch%ie(3)
          Do j = patch%is(2), patch%ie(2)
            Do i = patch%is(1), patch%ie(1)

              l0 = (k- grid%is(3))* N(1)* N(2) + (j- grid%is(2))* N(1) + (i- grid%is(1)+1)
              lp = (k-patch%is(3))*Np(1)*Np(2) + (j-patch%is(2))*Np(1) + (i-patch%is(1)+1)

              ! ... normal indices
              do jj = 1, size(gvec)
                if (normDir == 1) then
                  l1 = l0 + sgn*(jj-1)
                else if (normDir == 2) then
                  l1 = l0 + sgn*(jj-1)*N(1)
                else
                  l1 = l0 + sgn*(jj-1)*N(1)*N(2)
                end if
                inorm(jj) = l1
              end do

              ! ... primative variables
              GAMMA = state%gv(l0,1)
              RHO   = state%cv(l0,1)
              SPVOL = 1.0_rfreal / RHO
              UX    = SPVOL * state%cv(l0,2); UY = 0.0_rfreal; UZ = 0.0_rfreal;
              if (ND >= 2) UY = SPVOL * state%cv(l0,3)
              if (ND == 3) UZ = SPVOL * state%cv(l0,4)
              PRESSURE = (GAMMA-1.0_rfreal)*(state%cv(l0,ND+2) - 0.5_rfreal * RHO * (UX**2 + UY**2 + UZ**2))
              SPD_SND = sqrt(GAMMA * PRESSURE * SPVOL)
              SPD_SND_INV = 1.0_rfreal / SPD_SND
              KE = 0.5_rfreal * (UX**2 + UY**2 + UZ**2)
              ENTHALPY = KE + SPD_SND**2 / (GAMMA - 1.0_rfreal)
              MACH2 = (2.0_rfreal * KE) * SPD_SND_INV**2

              ! ... construct the normalized metrics
              XI_X = grid%MT1(l0,region%global%t2Map(normDir,1)); XI_Y = 0.0_rfreal; XI_Z = 0.0_rfreal
              if (ND >= 2) XI_Y = grid%MT1(l0,region%global%t2Map(normDir,2))
              if (ND == 3) XI_Z = grid%MT1(l0,region%global%t2Map(normDir,3))

              ! ... multiply by the Jacobian
              XI_X = XI_X * grid%JAC(l0)
              XI_Y = XI_Y * grid%JAC(l0)
              XI_Z = XI_Z * grid%JAC(l0)
              denom = 1.0_rfreal / sqrt(XI_X**2+XI_Y**2+XI_Z**2)
              XI_X_TILDE = XI_X * denom
              XI_Y_TILDE = XI_Y * denom
              XI_Z_TILDE = XI_Z * denom
              ! ... the unit normal is (XI_X_TILDE, XI_Y_TILDE, XI_Z_TILDE)
              ! ... and points into the domain on + boundaries
              ! ... and points out of the domain on - boundaries
              V_DOT_XI = UX * XI_X_TILDE + UY * XI_Y_TILDE + UZ * XI_Z_TILDE

!!$              if ( (patch%bcType == WALL_ADIABATIC_SLIP_EXTRAPOLATION_0TH_ORDER) .OR. &
!!$                   (patch%bcType == WALL_ADIABATIC_SLIP_EXTRAPOLATION_1ST_ORDER) .OR. &
!!$                   (patch%bcType == WALL_ADIABATIC_SLIP_EXTRAPOLATION_2ND_ORDER) ) then
!!$
!!$                ! ... compute the normal and tangential velocity vectors
!!$                if (ND == 2) then ! JKim 11/2007
!!$
!!$                  un    =  XI_X_TILDE * UX + XI_Y_TILDE * UY
!!$                  ut(1) = -XI_Y_TILDE * UX + XI_X_TILDE * UY
!!$
!!$                else if (ND == 3) then ! JKim 11/2007
!!$
!!$                  norm_vec(:) = (/XI_X_TILDE, XI_Y_TILDE, XI_Z_TILDE/) ! boundary-normal vector in the
!!$                  ! direction of increasing xi or
!!$                  ! eta or zeta.
!!$                  tang_vec(1,:) = (/grid%MT1(l0,region%global%t2Map(tangDir(normDir,1),1)), & ! pick up another direction
!!$                       grid%MT1(l0,region%global%t2Map(tangDir(normDir,1),2)), & ! in computational domain
!!$                       grid%MT1(l0,region%global%t2Map(tangDir(normDir,1),3))/)  ! to get boundary-tangential vector
!!$                  tang_vec(1,:) = tang_vec(1,:) * handedness(normDir) ! adjust handed-ness for right-handed-ness
!!$                  tang_vec(1,:) = tang_vec(1,:) * grid%JAC(l0) ! multiply by the Jacobian
!!$                  denom = 1.0_rfreal / sqrt(dot_product(tang_vec(1,:), tang_vec(1,:)))
!!$                  tang_vec(1,:) = tang_vec(1,:) * denom ! normal vector for another direction, say,
!!$                  ! (eta_x_tilde, eta_y_tilde, eta_z_tilde)
!!$                  call cross_product(norm_vec, tang_vec(1,:), tang_vec(1,:))
!!$                  tang_vec(1,:) = tang_vec(1,:) / sqrt(dot_product(tang_vec(1,:), tang_vec(1,:))) ! now it's done ^^
!!$
!!$                  call cross_product(norm_vec, tang_vec(1,:), tang_vec(2,:))
!!$                  tang_vec(2,:) = tang_vec(2,:) / sqrt(dot_product(tang_vec(2,:), tang_vec(2,:))) ! now it's done ^^
!!$
!!$                  un    = dot_product((/UX, UY, UZ/), norm_vec(:)  )
!!$                  ut(1) = dot_product((/UX, UY, UZ/), tang_vec(1,:))
!!$                  ut(2) = dot_product((/UX, UY, UZ/), tang_vec(2,:))
!!$
!!$                else
!!$
!!$                  call graceful_exit(region%myrank, 'No adiabatic slip wall for ND /= (2,3).  Stopping...')
!!$
!!$                end if ! ND
!!$
!!$                ! ... update the wall-normal velocity
!!$                un = XI_X_TILDE * grid%XYZ_TAU(l0,1)
!!$                if (ND >= 2) un = un + XI_Y_TILDE * grid%XYZ_TAU(l0,2)
!!$                if (ND == 3) un = un + XI_Z_TILDE * grid%XYZ_TAU(l0,3)
!!$
!!$                ! ... return to Cartesian velocities
!!$                if (ND == 2) then ! JKim 11/2007
!!$                  UX = XI_X_TILDE * un - XI_Y_TILDE * ut(1)
!!$                  UY = XI_Y_TILDE * un + XI_X_TILDE * ut(1)
!!$                else if (ND == 3) then ! JKim 11/2007
!!$                  UX = dot_product((/un, ut(1), ut(2)/), &
!!$                       (/norm_vec(1), tang_vec(1,1), tang_vec(2,1)/))
!!$                  UY = dot_product((/un, ut(1), ut(2)/), &
!!$                       (/norm_vec(2), tang_vec(1,2), tang_vec(2,2)/))
!!$                  UZ = dot_product((/un, ut(1), ut(2)/), &
!!$                       (/norm_vec(3), tang_vec(1,3), tang_vec(2,3)/))
!!$                end if ! ND
!!$
!!$                ! ... copy pressure & density values onto wall from interior
!!$                if ( patch%bcType == WALL_ADIABATIC_SLIP_EXTRAPOLATION_0TH_ORDER ) then
!!$
!!$                  RHO = state%cv(inorm(2),1)
!!$                  PRESSURE = (GAMMA-1.0_rfreal) * state%cv(inorm(2),ND+2)
!!$                  do jj = 1, ND
!!$                    PRESSURE = PRESSURE - 0.5_rfreal * (GAMMA - 1.0_rfreal) * state%cv(inorm(2),jj+1)**2 / state%cv(inorm(2),1)
!!$                  end do
!!$
!!$                  ! ... copy pressure & density values onto wall from interior
!!$                else if ( patch%bcType == WALL_ADIABATIC_SLIP_EXTRAPOLATION_1ST_ORDER ) then
!!$
!!$                  ! ... density and pressure at first point off wall
!!$                  RHO2 = state%cv(inorm(2),1)
!!$                  P2   = (GAMMA-1.0_rfreal) * state%cv(inorm(2),ND+2)
!!$                  do jj = 1, ND
!!$                    P2 = P2 - 0.5_rfreal * (GAMMA - 1.0_rfreal) * state%cv(inorm(2),jj+1)**2 / state%cv(inorm(2),1)
!!$                  end do
!!$
!!$                  ! ... compute the pressure gradient at the wall based on the wall-normal momentum equation
!!$                  lhs_fac = 0.0_rfreal
!!$                  do jj = 1, ND
!!$                    lhs_fac = lhs_fac + grid%MT1(l0,region%global%t2Map(normDir,jj))**2
!!$                  end do
!!$
!!$                  rhs_fac = 0.0_rfreal
!!$                  do jj = 1, ND
!!$                    do alpha = 1, ND-1
!!$                      rhs_fac = rhs_fac &
!!$                           - (patch%Deriv(jj+1,lp,tangDir(normDir,alpha)) &
!!$                           - state%cv(l0,jj+1)/state%cv(l0,1) * patch%Deriv(1,lp,tangDir(normDir,alpha))) * grid%MT1(l0,region%global%t2Map(normDir,jj))
!!$                    end do
!!$                  end do
!!$
!!$                  ! ... compute the correction
!!$                  dPRESSURE = rhs_fac / lhs_fac
!!$                  dRHO = RHO / PRESSURE * dPRESSURE
!!$
!!$                  ! ... distance between on-wall 
!!$                  ! ... and the point first off the wall
!!$                  ! ... in computational coordinates
!!$                  del = 1.0_rfreal
!!$
!!$                  ! ... update the density and pressure
!!$                  RHO = RHO2 + del * dRHO
!!$                  PRESSURE = P2 + del * dPRESSURE
!!$
!!$                end if
!!$
!!$                ! ... update the density
!!$                state%cv(l0,1) = RHO
!!$
!!$                ! ... update Cartesian velocities
!!$                state%cv(l0,2) = RHO * UX
!!$                if (ND >= 2) state%cv(l0,3) = RHO * UY
!!$                if (ND == 3) state%cv(l0,4) = RHO * UZ
!!$
!!$                ! ... update the total energy
!!$                state%cv(l0,ND+2) = PRESSURE / (GAMMA-1.0_rfreal) + 0.5_rfreal * RHO * (UX**2 + UY**2 + UZ**2)

              if ( (patch%bcType == NSCBC_INFLOW_VELOCITY_TEMPERATURE_FROM_SPONGE) .OR. &
                   (patch%bcType == NSCBC_INFLOW_VELOCITY_TEMPERATURE) ) then

                if (MACH2 >= 1.0_rfreal) then

                  state%cv(l0,1:ND+2)  = state%cvTarget(l0,1:ND+2)
                  state%rhs(l0,1:ND+2) = 0.0_rfreal

                else

                  ! ... assume density equation is good

                  ! ... update momentum
                  UX = patch%u_inflow(lp); UY = 0.0_rfreal; UZ = 0.0_rfreal
                  If (ND >= 2) UY = patch%v_inflow(lp)
                  If (ND == 3) UZ = patch%w_inflow(lp)
                  state%cv(l0,2) = RHO * UX
                  if (ND >= 2) state%cv(l0,3) = RHO * UY
                  if (ND == 3) state%cv(l0,4) = RHO * UZ

                  ! ... update the total energy
                  PRESSURE = (GAMMA-1.0_rfreal)/GAMMA * RHO * patch%T_inflow(lp)
                  state%cv(l0,ND+2) = PRESSURE / (GAMMA-1.0_rfreal) + 0.5_rfreal * RHO * (UX**2 + UY**2 + UZ**2)

                end if


              else if ( patch%bcType == NSCBC_SUBSONIC_INFLOW ) Then

                UX = state%cvTarget(l0,2) / state%cvTarget(l0,1); Uy = 0.0_rfreal; Uz = 0.0_rfreal
                if (ND >= 2) UY = state%cvTarget(l0,3) / state%cvTarget(l0,1)
                if (ND == 3) UZ = state%cvTarget(l0,4) / state%cvTarget(l0,1)

                ! ... fix the velocity
                state%cv(l0,2) = RHO * UX
                if (ND >= 2) state%cv(l0,3) = RHO * UY
                if (ND == 3) state%cv(l0,4) = RHO * UZ

                ! ... update the total energy
                state%cv(l0,ND+2) = PRESSURE / (GAMMA-1.0_rfreal) + 0.5_rfreal * RHO * (UX**2 + UY**2 + UZ**2)

              else if ( patch%bcType == NSCBC_WALL_ADIABATIC_SLIP ) then

                ! ... compute the normal and tangential velocity vectors
                if (ND == 2) then ! JKim 11/2007
                  un    = XI_X_TILDE * UX + XI_Y_TILDE * UY
                  ut(1) =-XI_Y_TILDE * UX + XI_X_TILDE * UY
                else if (ND == 3) then ! JKim 11/2007
                  norm_vec(:) = (/XI_X_TILDE, XI_Y_TILDE, XI_Z_TILDE/) ! boundary-normal vector in the
                  ! direction of increasing xi or
                  ! eta or zeta.
                  tang_vec(1,:) = (/grid%MT1(l0,region%global%t2Map(tangDir(normDir,1),1)), & ! pick up another direction
                       grid%MT1(l0,region%global%t2Map(tangDir(normDir,1),2)), & ! in computational domain
                       grid%MT1(l0,region%global%t2Map(tangDir(normDir,1),3))/)  ! to get boundary-tangential vector
                  tang_vec(1,:) = tang_vec(1,:) * handedness(normDir) ! adjust handed-ness for right-handed-ness
                  tang_vec(1,:) = tang_vec(1,:) * grid%JAC(l0) ! multiply by the Jacobian
                  denom = 1.0_rfreal / sqrt(dot_product(tang_vec(1,:), tang_vec(1,:)))
                  tang_vec(1,:) = tang_vec(1,:) * denom ! normal vector for another direction, say,
                  ! (eta_x_tilde, eta_y_tilde, eta_z_tilde)
                  call cross_product(norm_vec, tang_vec(1,:), tang_vec(1,:))
                  tang_vec(1,:) = tang_vec(1,:) / sqrt(dot_product(tang_vec(1,:), tang_vec(1,:))) ! now it's done ^^

                  call cross_product(norm_vec, tang_vec(1,:), tang_vec(2,:))
                  tang_vec(2,:) = tang_vec(2,:) / sqrt(dot_product(tang_vec(2,:), tang_vec(2,:))) ! now it's done ^^

                  un    = dot_product((/UX, UY, UZ/), norm_vec(:)  )
                  ut(1) = dot_product((/UX, UY, UZ/), tang_vec(1,:))
                  ut(2) = dot_product((/UX, UY, UZ/), tang_vec(2,:))
                else
                  call graceful_exit(region%myrank, 'No adiabatic slip wall for ND /= (2,3).  Stopping...')
                end if ! ND

                ! ... update the wall-normal velocity
                un = XI_X_TILDE * grid%XYZ_TAU(l0,1)
                if (ND >= 2) un = un + XI_Y_TILDE * grid%XYZ_TAU(l0,2)
                if (ND == 3) un = un + XI_Z_TILDE * grid%XYZ_TAU(l0,3)

                ! ... return to Cartesian velocities
                if (ND == 2) then ! JKim 11/2007
                  UX = XI_X_TILDE * un - XI_Y_TILDE * ut(1)
                  UY = XI_Y_TILDE * un + XI_X_TILDE * ut(1)
                else if (ND == 3) then ! JKim 11/2007
                  UX = dot_product((/un, ut(1), ut(2)/), &
                       (/norm_vec(1), tang_vec(1,1), tang_vec(2,1)/))
                  UY = dot_product((/un, ut(1), ut(2)/), &
                       (/norm_vec(2), tang_vec(1,2), tang_vec(2,2)/))
                  UZ = dot_product((/un, ut(1), ut(2)/), &
                       (/norm_vec(3), tang_vec(1,3), tang_vec(2,3)/))
                end if ! ND

                ! ... update Cartesian velocities
                state%cv(l0,2) = state%cv(l0,1) * UX
                if (ND >= 2) state%cv(l0,3) = state%cv(l0,1) * UY
                if (ND == 3) state%cv(l0,4) = state%cv(l0,1) * UZ

                ! ... update the total energy
                state%cv(l0,ND+2) = PRESSURE / (GAMMA-1.0_rfreal) + 0.5_rfreal * RHO * (UX**2 + UY**2 + UZ**2)

              else if ( patch%bcType == NSCBC_WALL_ISOTHERMAL_NOSLIP .OR. & 
                        patch%bcType == NSCBC_WALL_ISOTHERMAL_NOSLIP_COUPLED ) then

                ! ... momentum
                state%cv(l0,2:ND+1) = grid%XYZ_TAU(l0,1:ND) * state%cv(l0,1)

                ! ... update the total energy
                T_wall = 1.0_rfreal / (state%gv(l0,1) - 1.0_rfreal) 
                p_wall = (state%gv(l0,1) - 1.0_rfreal) / state%gv(l0,1) * state%cv(l0,1) * T_wall
                state%cv(l0,ND+2) = p_wall / (state%gv(l0,1) - 1.0_rfreal) + 0.5_rfreal * &
                     spvol * SUM(state%cv(l0,2:ND+1)**2)

                ! ... zero out the rhs for momentum
                state%rhs(l0,2:ND+1) = 0.0_rfreal

                ! ... set the rhs for the total energy
                state%rhs(l0,ND+2) = T_wall * state%rhs(l0,1) / state%gv(l0,1)

              else if (patch%bcType == NSCBC_WALL_NOSLIP_THERMALLY_COUPLED) then

                ! ... momentum
                state%cv(l0,2:ND+1) = grid%XYZ_TAU(l0,1:ND) * state%cv(l0,1)

                ! ... update the total energy
                ! ... The variable CellTemp is already non-dimensionalized in ModRocstar
                ! ... subroutine SetInterfaceTemp
                T_wall = patch%CellTemp(lp)! / (GAMMA - 1.0_rfreal) / input%T_inf  

                ! ... update the total energy based on either calorically perfect
                ! ... or thermally perfect gas model
!                if (input%tp_gas .EQV. .TRUE.) then
!                   state%cv(l0,ND+2) = RHO * (int_energy(T_wall, input%T_inf, GAMMA)&
!                        + 0.5_rfreal * (UX**2 + UY**2 + UZ**2))
!                else
                   p_wall = (GAMMA - 1.0_rfreal) / GAMMA * RHO * T_wall
                   state%cv(l0,ND+2) = p_wall / (GAMMA - 1.0_rfreal) + 0.5_rfreal * RHO * (UX**2 + UY**2 + UZ**2)
!                end if                

                ! ... zero out the rhs for momentum
                state%rhs(l0,2:ND+1) = 0.0_rfreal

                ! ... set the rhs for the total energy
                state%rhs(l0,ND+2) = state%rhs(l0,1) * state%cv(l0,ND+2) / state%cv(l0,1)

              else if ( patch%bcType == NSCBC_SUPERSONIC_INFLOW ) then

                ! ... specify everything
                state%cv(l0,:)  = state%cvTarget(l0,:)
                state%rhs(l0,:) = 0.0_rfreal

              end if

            end do
          end do
        end do

      end if

    end do

    deallocate(gvec,inorm)

  end subroutine Q1D_BC_Fix_Value

  !
  ! Also includes SAT boundary conditions of Kreiss, Nordstrom, Carpenter, ...
  !
  subroutine Q1D_BC(region, ng, flags)

    USE ModGlobal
    USE ModDataStruct
    USE ModDeriv
    USE ModMPI

    implicit none

    TYPE (t_region), POINTER :: region
    INTEGER :: ng
    INTEGER :: flags

    ! ... local variables
    type(t_patch), pointer :: patch
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    type(t_mixt_input), pointer :: input
    real(rfreal), pointer :: gvec(:), SC(:), Pinv(:,:), Pfor(:,:), Tmat(:,:), Tinv(:,:), cvPredictor(:), rhsPredictor(:)
    real(rfreal), pointer :: cons_inviscid_flux(:,:), L_vector(:), new_normal_flux(:)
    integer :: ND, Nc, N(MAX_ND), npatch, i, j, l0, k, ii, jj, l1, l2, isign, lp, kk
    real(rfreal) :: UU(MAX_ND), norm(MAX_ND), mag_norm, tang(MAX_ND-1,MAX_ND)
    real(rfreal) :: mag_tang, constant, del, eta, rel_pos(MAX_ND), dsign, denom
    integer :: normDir, tangDir(MAX_ND,MAX_ND-1), Np(MAX_ND)
    real(rfreal) :: d(5), wave_amp(5), wave_spd(5), spd_snd
    real(rfreal) :: un, ut(MAX_ND-1)
    real(rfreal) :: norm_vec(MAX_ND), tang_vec(2,MAX_ND), fvec(MAX_ND,2)
    real(rfreal) :: metricInverse(MAX_ND,MAX_ND), invProj(MAX_ND, MAX_ND)
    real(rfreal) :: sponge_amp, bndry_q(MAX_ND+2), bndry_p(MAX_ND+2), bndry_rhs(MAX_ND+2)
    integer :: sgn, handedness(MAX_ND)
    integer, pointer :: inorm(:)
    real(rfreal) :: dundn, drdn, dpdn, drundn, dut1dn, dut2dn, sigma
    real(rfreal) :: bndry_norm(MAX_ND), p_inf, u_inf, du_inf, un_old, metric_fac, T_stag, T_inf, dT_inf, theta, P_stag
    real(rfreal) :: charVar(MAX_ND+2), primVar(MAX_ND+2), dsgn
    real(rfreal) :: V_wall, V_wall_vec(MAX_ND), V_wall_target, V_wall_target_vec(MAX_ND), A_wall, drhodn, dp_inf, T_wall
    real(rfreal) :: Uhat_alpha(MAX_ND), alpha_x(MAX_ND,MAX_ND), alpha_t(MAX_ND)
    real(rfreal) :: x_alpha(MAX_ND,MAX_ND), primVar_RHS(MAX_ND+2), M2(2,2), M3(3,3)
    real(rfreal) :: SPVOL, RHO, UX, UY, UZ, PRESSURE, XI_X, XI_Y, XI_Z, SPD_SND_INV, KE, ENTHALPY, MACH2, GAMMA, V_DOT_XI
    real(rfreal) :: XI_X_TILDE, XI_Y_TILDE, XI_Z_TILDE, epsilon, omega, dUdt_inflow, dTdt_inflow, Mi, Ui, Ti, u_inflow, T_inflow
    real(rfreal) :: w_of_t, dwdt, t0, wi, w_inflow, v_inflow, dVdt_inflow, vi, dWdt_inflow
    real(rfreal), pointer :: uB(:), gI1(:), gI2(:), Aprime(:,:), Lambda(:,:), matX(:,:), matXTrans(:,:), penalty(:), gI1w(:)
    real(rfreal) :: radNozz, Tc, u0, r0, coefVel, sigmaI1, sigmaI2, sigma_inter
    real(rfreal) :: junk ! JKim 11/2007
    real(rfreal) :: RHO_TARGET, UX_TARGET, UY_TARGET, UZ_TARGET, PRESSURE_TARGET, U_wall, UVWhat(MAX_ND), UVW(MAX_ND)
    real(rfreal) :: alpha, beta, phi2, ucon, PRESSURE2, bndry_h
    real(rfreal), pointer :: pnorm_vec(:), Lambda_in(:,:), Lambda_out(:,:)
    real(rfreal) :: p_in, p_out, sig_v, sigR(MAX_ND+2), sigL(MAX_ND+2), tmp(MAX_ND+2), tmp2(MAX_ND+2), sigma_scale

    tangDir(1,:) = (/2, 3/)
    tangDir(2,:) = (/1, 3/)
    tangDir(3,:) = (/1, 2/)
    handedness = (/1, -1, 1/)

    ! ... simplicity
    input => region%input
    ND = input%ND

    !allocate(gvec(size(input%bndry_explicit_deriv)))
    !allocate(inorm(size(gvec)))
    allocate(SC(ND+2),Pinv(ND+2,ND+2),Pfor(ND+2,ND+2),Tmat(ND+2,ND+2),Tinv(ND+2,ND+2))
    allocate(cons_inviscid_flux(ND,ND+2),L_vector(ND+2),new_normal_flux(ND+2), pnorm_vec(ND))
    allocate(uB(ND+2), gI1(ND+2), gI2(ND+2), Aprime(ND+2,ND+2), Lambda(ND+2,ND+2), matX(ND+2,ND+2), matXTrans(ND+2,ND+2), penalty(ND+2))
    allocate(Lambda_in(ND+2,ND+2), Lambda_out(ND+2,ND+2))

    do npatch = 1, region%nPatches

      patch => region%patch(npatch)
      grid => region%grid(patch%gridID)
      state => region%state(patch%gridID)
      normDir = abs(patch%normDir)
      sgn = normDir / patch%normDir
      dsgn = dble(sgn)

      Nc = grid%nCells
      N  = 1; Np = 1;
      do j = 1, grid%ND
        N(j)  =  grid%ie(j) -  grid%is(j) + 1
        Np(j) = patch%ie(j) - patch%is(j) + 1
      end do;

      if ( patch%bcType /= SPONGE ) then

        Do k = patch%is(3), patch%ie(3)
          Do j = patch%is(2), patch%ie(2)
            Do i = patch%is(1), patch%ie(1)

              l0 = (k-grid%is(3))*N(1)*N(2) + (j-grid%is(2))*N(1) + i-grid%is(1)+1
              lp = (k-patch%is(3))*Np(1)*Np(2) &
                   + (j-patch%is(2))*Np(1) + (i-patch%is(1)+1)

              ! ... primative variables
              GAMMA = state%gv(l0,1)
              RHO = state%cv(l0,1)
              SPVOL = 1.0_rfreal / RHO
              UX  = SPVOL * state%cv(l0,2); UY = 0.0_rfreal; UZ = 0.0_rfreal;
              PRESSURE = state%dv(l0,1)
              SPD_SND = sqrt(GAMMA * PRESSURE * SPVOL)
              SPD_SND_INV = 1.0_rfreal / SPD_SND
              KE = 0.5_rfreal * UX**2
              ENTHALPY = KE + SPD_SND**2 / (GAMMA - 1.0_rfreal)
              MACH2 = (2.0_rfreal * KE) * SPD_SND_INV**2

              ! ... construct the normalized metrics
              XI_X = grid%MT1(l0,region%global%t2Map(normDir,1)); XI_Y = 0.0_rfreal; XI_Z = 0.0_rfreal

              ! ... multiply by the Jacobian
              XI_X = XI_X * grid%JAC(l0)
              denom = 1.0_rfreal / sqrt(XI_X**2)
              XI_X_TILDE = XI_X * denom
              XI_Y_TILDE = XI_Y * denom
              XI_Z_TILDE = XI_Z * denom
              bndry_h = denom

              ! ... wall-normal velocity
              V_DOT_XI = UX

              ! ... construct the forward and backward transformation matrices
              ! ... these matrices are used for both NSCBC and SAT
              Pinv(:,:) = 0.0_rfreal; Pfor(:,:) = 0.0_rfreal; 

              ! ... the forward matrix P
              Pfor(1,1) = 1.0_rfreal
              Pfor(1,2) =  0.5_rfreal * RHO * SPD_SND_INV
              Pfor(1,3) = -0.5_rfreal * RHO * SPD_SND_INV
              Pfor(2,1) = UX
              Pfor(2,2) = Pfor(1,2) * (UX + SPD_SND)
              Pfor(2,3) = Pfor(1,3) * (UX - SPD_SND)
              Pfor(3,1) = KE
              Pfor(3,2) = Pfor(1,2) * (ENTHALPY + SPD_SND * UX)
              Pfor(3,3) = Pfor(1,3) * (ENTHALPY - SPD_SND * UX)

              ! ... the inverse matrix Pinv
              Pinv(1,1) = 1.0 - 0.5_rfreal * (GAMMA-1.0_rfreal) * MACH2
              Pinv(1,2) =  (GAMMA-1.0_rfreal) * UX * SPD_SND_INV**2
              Pinv(1,3) = -(GAMMA-1.0_rfreal) * SPD_SND_INV**2
              Pinv(2,1) =  SPD_SND_INV * SPVOL * (0.5_rfreal * (GAMMA-1.0_rfreal) * UX**2 - UX * SPD_SND)
              Pinv(2,2) =  SPD_SND_INV * SPVOL * (SPD_SND - (GAMMA-1.0_rfreal)*UX)
              Pinv(2,3) =  (GAMMA-1.0_rfreal) * SPVOL * SPD_SND_INV
              Pinv(3,1) = -SPD_SND_INV * SPVOL * (0.5_rfreal * (GAMMA-1.0_rfreal) * UX**2 + UX * SPD_SND)
              Pinv(3,2) =  SPD_SND_INV * SPVOL * (SPD_SND + (GAMMA-1.0_rfreal)*UX)
              Pinv(3,3) = -(GAMMA-1.0_rfreal) * SPVOL * SPD_SND_INV

              if ( (patch%bcType == NSCBC_INFLOW_VELOCITY_TEMPERATURE) .OR. &
                   (patch%bcType == NSCBC_INFLOW_VELOCITY_TEMPERATURE_FROM_SPONGE) .OR. &
                   (patch%bcType == NSCBC_OUTFLOW_PERFECT_NONREFLECTION) .OR. &
                   (patch%bcType == NSCBC_OUTFLOW_PERFECT_REFLECTION) .OR. &
                   (patch%bcType == NSCBC_WALL_ADIABATIC_SLIP) .OR. &
                   (patch%bcType == NSCBC_WALL_ISOTHERMAL_NOSLIP) .OR. &
                   (patch%bcType == NSCBC_WALL_ISOTHERMAL_NOSLIP_COUPLED) .OR. &
                   (patch%bcType == NSCBC_WALL_NOSLIP_THERMALLY_COUPLED) .OR. &
                   (patch%bcType == NSCBC_SUBSONIC_INFLOW) .OR. &
                   (patch%bcType == NSCBC_SUPERSONIC_INFLOW) ) then

                ! ... compute the conservative flux variables
                cons_inviscid_flux(:,:)    = 0.0_rfreal

                ! ... one-dimensional fluxes
                cons_inviscid_flux(1,1)    = state%cv(l0,2)
                cons_inviscid_flux(1,2)    = RHO * UX**2 + PRESSURE
                cons_inviscid_flux(1,ND+2) = (state%cv(l0,ND+2) + PRESSURE) * UX

                ! ... store the current rhs in SC(:)
                SC(:) = state%rhs(l0,:)

                ! ... subtract off the normal flux
                SC(:) = SC(:) + patch%deriv(:,lp,normDir) * grid%JAC(l0)

                ! ... subtract off the flux/metric derivative terms
                SC(:) = SC(:) - grid%JAC(l0) * state%cv(l0,:) * grid%XI_TAU_XI(l0,normDir)
                do jj = 1, ND
                  SC(:) = SC(:) &
                       - grid%JAC(l0) * cons_inviscid_flux(jj,:) * patch%MT2(lp,region%global%t2Map(normDir,jj))
                end do
                SC(:) = 0.0_rfreal

                ! ... compute the characteristic L_vector
                L_vector(:) = grid%JAC(l0) * ( patch%deriv(:,lp,normDir) &
                     - ( cons_inviscid_flux(1,:) * patch%MT2(lp,region%global%t2Map(normDir,1)) &
                     + state%cv(l0,:) * grid%XI_TAU_XI(l0,normDir) ) )

                ! ... scale the L_vector by Pinv
                L_vector = MATMUL(Pinv,L_vector)

                ! ... transform SC with Pinv
                SC = MATMUL(Pinv,SC)

                ! ... wavespeeds
                wave_spd(1) = UX * XI_X
                wave_spd(2) = wave_spd(1) + SPD_SND / denom
                wave_spd(3) = wave_spd(1) - SPD_SND / denom

              end if


              ! ... apply bc...
              ! ... solid wall first because no normal velocity thru wall
              if ( patch%bcType == NSCBC_WALL_ADIABATIC_SLIP ) then

                A_wall = grid%XYZ_TAU2(l0,1) * XI_X_TILDE

                if (patch%normDir > 0) then
                  L_vector(ND+1) = L_vector(ND+2) + SC(ND+1) - SC(ND+2) - 2.0_rfreal * A_wall
                else
                  L_vector(ND+2) = L_vector(ND+1) - SC(ND+1) + SC(ND+2) + 2.0_rfreal * A_wall
                end if

              else if ( (patch%bcType == NSCBC_INFLOW_VELOCITY_TEMPERATURE_FROM_SPONGE) .OR. &
                   (patch%bcType == NSCBC_INFLOW_VELOCITY_TEMPERATURE) ) then

                if (patch%normDir > 0) then
                  L_vector(ND+1) = L_vector(ND+2) + SC(ND+1) - SC(ND+2) &
                       - 2.0_rfreal * (patch%dudt_inflow(lp) * XI_X_TILDE)
                else
                  L_vector(ND+2) = L_vector(ND+1) - SC(ND+1) + SC(ND+2) &
                       + 2.0_rfreal * (patch%dudt_inflow(lp) * XI_X_TILDE)
                end if

                L_vector(1) = + 0.5_rfreal * (GAMMA-1.0_rfreal) * RHO * SPD_SND_INV * (L_vector(ND+1)+L_vector(ND+2)) &
                     - 0.5_rfreal * (GAMMA-1.0_rfreal) * RHO * SPD_SND_INV * (SC(ND+1)+SC(ND+2)) + SC(1) &
                     + SPD_SND**2 / (GAMMA-1.0_rfreal) * patch%dTdt_inflow(lp)

              else if ( patch%bcType == NSCBC_OUTFLOW_PERFECT_NONREFLECTION ) then

                SC(:) = 0.0_rfreal
                if (patch%normDir > 0) then
                  L_vector(ND+1) = 0.0_rfreal
                else
                  L_vector(ND+2) = 0.0_rfreal
                end if

              else if ( patch%bcType == NSCBC_WALL_ISOTHERMAL_NOSLIP .OR. &
                   patch%bcType == NSCBC_WALL_ISOTHERMAL_NOSLIP_COUPLED .OR. &
                   patch%bcType == NSCBC_WALL_NOSLIP_THERMALLY_COUPLED ) then

                L_vector(2)  = 0.0_rfreal
                L_vector(ND) = 0.0_rfreal
                SC(2)  = 0.0_rfreal
                SC(ND) = 0.0_rfreal
                if (patch%normDir > 0) then
                  L_vector(ND+1) = L_vector(ND+2) + SC(ND+1) - SC(ND+2)
                else
                  L_vector(ND+2) = L_vector(ND+1) - SC(ND+1) + SC(ND+2)
                end if

              else if ( patch%bcType == NSCBC_OUTFLOW_PERFECT_REFLECTION) Then

                if (patch%normDir > 0) then
                  L_vector(ND+1) = -L_vector(ND+2) - SC(ND+1) - SC(ND+2)
                else
                  L_vector(ND+2) = -L_vector(ND+1) - SC(ND+1) - SC(ND+2)
                end if

              else if ( patch%bcType == NSCBC_SUPERSONIC_INFLOW ) Then

                ! ... do nothing, everything is specified in NS_BC_Fix_Value

              end if ! patch%bcType

              if ( (patch%bcType == NSCBC_INFLOW_VELOCITY_TEMPERATURE) .OR. &
                   (patch%bcType == NSCBC_INFLOW_VELOCITY_TEMPERATURE_FROM_SPONGE) .OR. &
                   (patch%bcType == NSCBC_OUTFLOW_PERFECT_NONREFLECTION) .OR. &
                   (patch%bcType == NSCBC_OUTFLOW_PERFECT_REFLECTION) .OR. &
                   (patch%bcType == NSCBC_WALL_ADIABATIC_SLIP) .OR. &
                   (patch%bcType == NSCBC_WALL_ISOTHERMAL_NOSLIP) .OR. &
                   (patch%bcType == NSCBC_WALL_ISOTHERMAL_NOSLIP_COUPLED) .OR. &
                   (patch%bcType == NSCBC_WALL_NOSLIP_THERMALLY_COUPLED) .OR. &
                   (patch%bcType == NSCBC_SUBSONIC_INFLOW) .OR. &
                   (patch%bcType == NSCBC_SUPERSONIC_INFLOW) ) then

                ! ... compute the new normal flux
                new_normal_flux(:) = MATMUL(Pfor,L_vector)
                new_normal_flux(:) = grid%INVJAC(l0) * new_normal_flux(:)
                do jj = 1, ND
                  new_normal_flux(:) = new_normal_flux(:) + cons_inviscid_flux(jj,:) * patch%MT2(lp,region%global%t2Map(normDir,jj))
                end do
                new_normal_flux(:) = new_normal_flux + state%cv(l0,:) * grid%XI_TAU_XI(l0,normDir)

                ! ... reset the rhs
                if (grid%iblank(l0) /= 0) then
                  state%rhs(l0,:) = state%rhs(l0,:) + grid%JAC(l0) * patch%deriv(:,lp,normDir) - grid%JAC(l0) * new_normal_flux(:)
                else
                  state%rhs(l0,:) = 0.0_rfreal
                end if

              end if

              if ( patch%bcType == SAT_FAR_FIELD .or. patch%bcType == SAT_PRESSURE ) then

                ! ... wall-normal vector
                norm_vec(1:MAX_ND) = (/ 1.0_rfreal, 0.0_rfreal, 0.0_rfreal /)

                ! ... compute the current wall-normal velocity
                V_wall_vec(1:ND) = V_DOT_XI * norm_vec(1:ND)

                ! ... target normal wall velocity
                V_wall_target_vec(1:ND) = dot_product(state%cvTarget(l0,2:ND+1),norm_vec(1:ND)) * norm_vec(1:ND) / state%cvTarget(l0,1)

                ! ... inviscid penalty parameter
                sigmaI1 = -input%SAT_sigmaI1_FF * dsgn

                ! ... pull off the boundary data 
                uB(:) = state%cv(l0,:)

                ! ... target data
                gI1(:) = state%cvTarget(l0,:)

                ! ... follow Magnus's algorithm (norm_vec has unit length)
                pnorm_vec(:) = norm_vec(1:ND) * sqrt(XI_X**2)
                Call Q1D_SAT_Form_Roe_Matrices(uB, gI1, gamma, pnorm_vec, Tmat, Tinv, Lambda)

                ! ... save ucon, speed_sound
                ucon    = Lambda(1,1)
                spd_snd = Lambda(ND+1,ND+1) - ucon

                ! ... only pick those Lambda that are incoming
                if ( sgn == 1 ) then
                  do jj = 1, ND+2
                    Lambda(jj,jj) = max(Lambda(jj,jj),0.0_rfreal)
                  end do
                else 
                  do jj = 1, ND+2
                    Lambda(jj,jj) = min(Lambda(jj,jj),0.0_rfreal)
                  end do
                end if

                ! ... modify Lambda if subsonic outflow
                if(patch%bcType == SAT_FAR_FIELD) then
                ! ... left boundary
                   if (sgn == 1 .and. ucon < 0.0_rfreal .and. ucon + spd_snd > 0.0_rfreal) then
                      Lambda(ND+2,ND+1) = Lambda(ND+2,ND+1) - ucon - spd_snd
                  ! right boundary
                   else if (sgn == -1 .and. ucon > 0.0_rfreal .and. ucon - spd_snd < 0.0_rfreal) then
                      Lambda(ND+1,ND+2) = Lambda(ND+1,ND+2) - ucon + spd_snd
                   end if
                endif
                if(patch%bcType == SAT_PRESSURE) then
                   if(sgn == 1) then
                      Lambda(ND+2,ND+1) = Lambda(ND+2,ND+1) - ucon - spd_snd
                      Lambda(ND+2,ND+2) = Lambda(ND+2,ND+2) - ucon - spd_snd
                      Lambda(ND+1,ND+2) = Lambda(ND+1,ND+2) + ucon + spd_snd
                   endif
                   if(sgn == -1) then
                      Lambda(ND+1,ND+2) = Lambda(ND+1,ND+2) - ucon + spd_snd
                      Lambda(ND+1,ND+1) = Lambda(ND+1,ND+1) - ucon + spd_snd
                      Lambda(ND+2,ND+1) = Lambda(ND+2,ND+1) + ucon - spd_snd
                   endif
                endif
                

                ! ... multiply Lambda' into X
                matX = MATMUL(Lambda,Tinv)

                ! ... compute the characteristic matrix
                Aprime = MATMUL(Tmat,matX)

                ! ... subtract off the target
                ub(1:ND+2) = ub(1:ND+2) - gI1(1:ND+2)

                ! ... compute the characteristic penalty vector
                gI1(1:ND+2) = MATMUL(Aprime,uB)

                ! ... compute penalty (Bndry_h is included in Lambda already)
                penalty(:) = sigmaI1 * grid%SBP_invPdiag_block1(1) * gI1(:)

                ! ... add the rhs
                state%rhs(l0,:) = state%rhs(l0,:) + penalty(:)

                if ( state%REinv > 0.0_rfreal ) then

                  ! ... factor
                  sigmaI2 = dsgn * input%SAT_sigmaI2_FF * grid%SBP_invPdiag_block1(1) / bndry_h ! * grid%JAC(l0)

                  ! ... target state
                  gI2(:) = 0.0_rfreal

                  ! ... boundary data (already includes 1/Re factor)
                  uB(:) = patch%ViscousFlux(:,lp,1) * XI_X_TILDE

                  ! penalty term
                  penalty(:) = penalty(:) + sigmaI2 * (uB(:) - gI2(:))

                  ! ... add to RHS
                  state%rhs(l0,:) = state%rhs(l0,:) + penalty(:)

                end if

              end if


              if ( patch%bcType == SAT_BLOCK_INTERFACE ) then

                ! ... wall-normal vector
                norm_vec(1:MAX_ND) = (/ XI_X_TILDE, XI_Y_TILDE, XI_Z_TILDE /)

                ! ... inviscid penalty parameter
                sigmaI1 = 1.0_rfreal ! -input%SAT_sigmaI1_BI * dsgn

                ! ... pull off the boundary data 
                uB(:) = state%cv(l0,:)

                ! ... target data
                gI1(:) = patch%cv_out(lp,:)

                ! ... follow Magnus's algorithm (norm_vec has unit length)
                pnorm_vec(:) = norm_vec(1:ND) * sqrt(XI_X**2 + XI_Y**2 + XI_Z**2)
                Call Q1D_SAT_Form_Roe_Matrices(uB, gI1, gamma, pnorm_vec, Tmat, Tinv, Lambda)

                ! ... minimal dissipation if sigma_scale = 1
                ! ... complete upwinding if sigma_scale = 2
                sigma_scale = 2.0
                do jj = 1, ND+2
                  sigR(jj) = 0.5_rfreal * Lambda(jj,jj) * sigma_scale
                  sigL(jj) = sigR(jj) - Lambda(jj,jj)
                end do

                ! ... only pick those Lambda that are incoming
                Lambda_in(:,:)  = 0.0_rfreal
                Lambda_out(:,:) = 0.0_rfreal
                if ( sgn == 1 ) then
                  do jj = 1, ND+2
                    if (Lambda(jj,jj) > 0.0_rfreal) then
                      Lambda_in(jj,jj)  = -sigR(jj)
                    else
                      Lambda_out(jj,jj) = sigL(jj) 
                    end if
                  end do
                else 
                  do jj = 1, ND+2
                    if (Lambda(jj,jj) < 0.0_rfreal) then
                      Lambda_in(jj,jj)  = sigR(jj)
                    else
                      Lambda_out(jj,jj) = -sigL(jj) 
                    end if
                  end do
                end if

                ! ... scalings across interface
                ! ... assumes same FD scheme on both sides
                p_in  = 1.0_rfreal / grid%SBP_invPdiag_block1(1);
                p_out = 1.0_rfreal / grid%SBP_invPdiag_block1(1);

                if ( sgn == 1 ) then
                  sig_v = 1.0_rfreal - p_in / (p_in + p_out);
                else
                  sig_v = -p_out / (p_in + p_out);
                end if

                ! ... compute extra contribution to inviscid term
                if (state%REinv > 0.0_rfreal) then

                  sigma_inter = - 0.25_rfreal / (p_in + p_out) * state%REinv

                  ! ... for block interface, add sigma_interface to Lambda
                  do jj = 1, ND+2
                    Lambda_in(jj,jj)  = Lambda_in(jj,jj)  + sigma_inter * (XI_X**2 + XI_Y**2 + XI_Z**2)
                    Lambda_out(jj,jj) = Lambda_out(jj,jj) + sigma_inter * (XI_X**2 + XI_Y**2 + XI_Z**2)
                  end do

                end if


                ! ... incoming characteristics

                ! ... multiply Lambda' into X
                matX = MATMUL(Lambda_in,Tinv)

                ! ... compute the characteristic matrix
                Aprime = MATMUL(Tmat,matX)

                ! ... subtract off the target
                tmp(1:ND+2) = ub(1:ND+2) - gI1(1:ND+2)

                ! ... compute the characteristic penalty vector
                tmp2(1:ND+2) = MATMUL(Aprime,tmp(1:ND+2))

                ! ... compute penalty (Bndry_h is included in Lambda already)
                penalty(1:ND+2) = sigmaI1 * grid%SBP_invPdiag_block1(1) * tmp2(1:ND+2)


                ! ... outgoing characteristcs

                ! ... multiply Lambda' into X
                matX = MATMUL(Lambda_out,Tinv)

                ! ... compute the characteristic matrix
                Aprime = MATMUL(Tmat,matX)

                ! ... subtract off the target
                tmp(1:ND+2) = ub(1:ND+2) - gI1(1:ND+2)

                ! ... compute the characteristic penalty vector
                tmp2(1:ND+2) = MATMUL(Aprime,tmp(1:ND+2))

                ! ... compute penalty (Bndry_h is included in Lambda already)
                penalty(1:ND+2) = penalty(1:ND+2) + sigmaI1 * grid%SBP_invPdiag_block1(1) * tmp2(1:ND+2)

                ! ... add the rhs
                state%rhs(l0,:) = state%rhs(l0,:) + penalty(:)

                if ( state%REinv > 0.0_rfreal ) then

                  ! ... factor
                  sigmaI2 = dsgn * grid%SBP_invPdiag_block1(1) !/ bndry_h ! * grid%JAC(l0)

                  ! ... target state
                  gI2(:) = patch%ViscousFlux_out(:,lp,1) * XI_X_TILDE

                  ! ... boundary data (already includes 1/Re factor)
                  uB(:) = patch%ViscousFlux(:,lp,1) * XI_X_TILDE

                  ! if (normDir == 1 .and. sgn == -1) print *, uB(2), XI_X_TILDE, XI_Y_TILDE

                  ! penalty term
                  penalty(:) = penalty(:) + sigmaI2 * (uB(:) - gI2(:))

                  ! ... add to RHS
                  state%rhs(l0,:) = state%rhs(l0,:) + penalty(:)

                end if

              end if


              ! ... 
              ! ... SAT boundary conditions as given in 
              ! ... Svard & Nordstrom, JCP, Vol. 227, 2008
              if ( patch%bcType == SAT_SLIP_ADIABATIC_WALL .OR. &
                   patch%bcType == SAT_NOSLIP_ISOTHERMAL_WALL ) then

                ! ... wall-normal vector
                norm_vec(1:MAX_ND) = (/ XI_X_TILDE, XI_Y_TILDE, XI_Z_TILDE /)

                ! ... compute the current wall-normal velocity
                V_wall_vec(1:ND) = V_DOT_XI * norm_vec(1:ND)

                ! ... target normal wall velocity
                V_wall_target_vec(1:ND) = dot_product(grid%XYZ_TAU(l0,1:ND),norm_vec(1:ND)) * norm_vec(1:ND)

                ! ... penalty parameter
                sigmaI1 = -input%SAT_sigmaI1 * dsgn

                ! ... pull off the boundary data 
                uB(:) = state%cv(l0,:)

                ! ... target data
                RHO_TARGET      = state%cv(l0,1)
                UX_TARGET       = UX - (V_wall_vec(1)-V_wall_target_vec(1))
                PRESSURE_TARGET = PRESSURE

                ! ... compute the target data for the inviscid conditions
                gI1(1)    = RHO_TARGET
                gI1(2)    = RHO_TARGET * UX_TARGET
                gI1(ND+2) = PRESSURE_TARGET / (GAMMA - 1.0_rfreal) + 0.5_rfreal * RHO_TARGET * UX_TARGET**2

                ! ... follow Magnus's algorithm (norm_vec has unit length)
                pnorm_vec(:) = norm_vec(1:ND) * sqrt(XI_X**2)
                Call Q1D_SAT_Form_Roe_Matrices(uB, gI1, gamma, pnorm_vec, Tmat, Tinv, Lambda)

                ! ... only pick those Lambda that are incoming
                if ( sgn == 1 ) then
                  do jj = 1, ND+2
                    Lambda(jj,jj) = max(Lambda(jj,jj),0.0_rfreal)
                  end do
                else 
                  do jj = 1, ND+2
                    Lambda(jj,jj) = min(Lambda(jj,jj),0.0_rfreal)
                  end do
                end if

                ! ... multiply Lambda' into X
                matX = MATMUL(Lambda,Tinv)

                ! ... compute the characteristic matrix
                Aprime = MATMUL(Tmat,matX)

                ! ... subtract off the target
                ub(1:ND+2) = ub(1:ND+2) - gI1(1:ND+2)

                ! ... compute the characteristic penalty vector
                gI1(1:ND+2) = MATMUL(Aprime,uB)

                ! ... compute penalty (Bndry_h is included in Lambda already)
                penalty(:) = sigmaI1 * grid%SBP_invPdiag_block1(1) * gI1(:)

                ! ... add the rhs
                state%rhs(l0,:) = state%rhs(l0,:) + penalty(:)

                if ( patch%bcType == SAT_NOSLIP_ISOTHERMAL_WALL ) then

                  ! ... boundary data
                  uB(:) = state%cv(l0,:)

                  ! ... wall temperature
                  T_wall = 1.0_rfreal / ( GAMMA - 1.0_rfreal )

                  ! ... target state
                  gI2(1)      = RHO_TARGET
                  gI2(2:ND+1) = grid%XYZ_TAU(l0,1:ND) * RHO_TARGET
                  gI2(ND+2)   = RHO_TARGET * T_wall / GAMMA + 0.5_rfreal * SUM(gI2(2:ND+1)**2) / RHO_TARGET

                  ! ... penalty parameter
                  sigmaI2 = -(input%SAT_sigmaI2 / 4.0_rfreal) * (grid%SBP_invPdiag_block1(1)/bndry_h)**2 * state%tv(l0,1) / RHO_TARGET * MAX(GAMMA / state%Pr, 5.0_rfreal / 3.0_rfreal)
                  sigmaI2 = sigmaI2 * state%REinv

                  ! ... compute penalty
                  penalty(:) = sigmaI2 * (uB(1:ND+2) - gI2(1:ND+2))

                  ! ... add to the rhs
                  if ( input%timeScheme == IMEX_RK4 ) then
                    patch%sat_fac(lp) = sigmaI2 
                    state%rhs(l0,:)   = state%rhs(l0,:) - sigmaI2 * gI2(1:ND+2)
                  else
                    state%rhs(l0,:)   = state%rhs(l0,:) + penalty(:)
                  end if

                end if

              end if

            end do
          end do
        end do

      else if (patch%bcType == SPONGE) then

        do k = 1, grid%vds(normDir)
          do j = patch%is(normDir), patch%ie(normDir)

            ! ... this point
            l0 = grid%vec_ind(k,j-grid%is(normDir)+1,normDir)

            ! ... distance from boundary to inner sponge start
            rel_pos(1:grid%ND) = patch%sponge_xs(k,1:grid%ND) - patch%sponge_xe(k,1:grid%ND)
            del = sqrt(dot_product(rel_pos(1:grid%ND),rel_pos(1:grid%ND)))

            ! ... distance from inner sponge start to point
            rel_pos(1:grid%ND) = patch%sponge_xs(k,1:grid%ND) - grid%XYZ(l0,1:grid%ND)
            eta = sqrt(dot_product(rel_pos(1:grid%ND),rel_pos(1:grid%ND))) / del

            sponge_amp = 0.0_rfreal
            if (eta .le. 1.0_rfreal) sponge_amp = (input%sponge_amp) * eta**(input%sponge_pow)

            if (grid%iblank(l0) /= 0) then
              state%rhs(l0,:) = state%rhs(l0,:) - sponge_amp * (state%cv(l0,:) - state%cvTarget(l0,:))
            else
              state%rhs(l0,:) = 0.0_rfreal
            end if

          end do
        end do

      end if

    end do

    !deallocate(gvec,inorm)
    deallocate(SC,Pinv,Pfor)
    deallocate(cons_inviscid_flux,L_vector,new_normal_flux, pnorm_vec)
    deallocate(uB, gI1, gI2, Aprime, Lambda, matX, matXTrans, penalty)
    deallocate(Lambda_in, Lambda_out)

    return

  end subroutine Q1D_BC

  subroutine Q1D_RHS_ViscousTerms_CARTESIAN(region, ng, flags, VelGrad, TempGrad, tvCor) ! JKim 01/2008

    USE ModGlobal
    USE ModDataStruct
    USE ModDeriv
    USE ModMPI

    Implicit None

    ! ... subroutine arguments
    real(rfreal), pointer :: VelGrad(:,:), div(:), divTau(:,:), TempGrad(:,:)
    real(rfreal), pointer, optional :: tvCor(:,:) ! JKim 01/2008

    ! ... local variables
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    type(t_mixt_input), pointer :: input
    type(t_region), pointer :: region
    type(t_patch), pointer :: patch
    integer :: flags
    real(rfreal), pointer :: UVW(:,:)
    integer :: ND, Nc, N(MAX_ND), ng, i, j, Np(MAX_ND), patchID
    integer :: ip, jp, kp

    ! ... local variables and arrays for viscous and heat fluxes, JKim 02/2007
    integer :: k, l, m, p
    real(rfreal) :: viscous_time
    real(rfreal), pointer :: VelGrad2nd(:,:,:), TempGrad2nd(:,:)
    real(rfreal), pointer :: MTProd(:,:), MTProdBKUP(:,:) ! JKim 01/2008
    real(rfreal), pointer :: tv(:,:)

    ! ... simplicity
    input => region%input
    grid => region%grid(ng)
    state => region%state(ng)
    ND = grid%ND
    Nc = grid%nCells
    N  = 1
    do j = 1, grid%ND
      N(j) = grid%ie(j)-grid%is(j)+1
    end do ! j

    if (flags == INVISCID_ONLY) return ! because it's inviscid computation

    if (state%RE == 0.0_rfreal .AND. input%shock == 0 .AND. input%LES == 0) then
      call graceful_exit(region%myrank, 'At least either shock-capturing or LES should be on to compute viscous terms.')
    end if ! state%RE

    ! ... allocate space for arrays
    ! nullify(flux); allocate(flux(Nc)); flux = 0.0_rfreal
    ! nullify(dflux); allocate(dflux(Nc)); dflux = 0.0_rfreal
    nullify(UVW); allocate(UVW(Nc,ND)); UVW = 0.0_rfreal

    ! ... Step 1: viscosity correction is now considered
    ! ...         state%tv still not updated, JKim 01/2008
    ! nullify(tv); allocate(tv(Nc,input%nTv)); tv(:,:) = 0.0_rfreal  ! ... JKim 06/2008
    ! tv(:,1:input%nTv) = state%tv(:,1:input%nTv)                    ! ... JKim 06/2008
    ! if (PRESENT(tvCor) .eqv. .true.) then
    !   tv(:,1:input%nTv) = tv(:,1:input%nTv) + tvCor(:,1:input%nTv) ! ... JKim 06/2008
    ! end if ! PRESENT(tvCor)

    ! ... precompute primitive velocities and store in UVW(:,:).
    do l = 1, ND
      do i = 1, Nc
        UVW(i,l) = state%cv(i,region%global%vMap(l+1)) * state%dv(i,3)
      end do
    end do ! l

!!$    ! ... compute the velocity gradient tensor (in CARTESIAN coordinates)
!!$    do i = 1, ND
!!$
!!$      do k = 1, Nc
!!$        state%flux(k) = UVW(k,i)
!!$      end do
!!$
!!$      do j = 1, ND
!!$        state%dflux(:) = 0.0_rfreal
!!$        Call FIRST_DERIV(region, ng, j, state%flux, state%dflux, 1.0_rfreal, .FALSE.)
!!$
!!$        do k = 1, Nc
!!$          VelGrad(k,region%global%t2Map(i,j)) = state%dflux(k)
!!$        end do
!!$      end do
!!$
!!$    end do

    ! ... convert the velocity gradient tensor to (x,y,z) from (xi,eta,zeta)
    do i = 1, ND
      do j = 1, ND
        do k = 1, Nc
          VelGrad(k,region%global%t2Map(i,j)) = VelGrad(k,region%global%t2Map(i,j)) * grid%MT1(k, region%global%t2Map(j,j)) * grid%JAC(k)
        end do
      end do
    end do

    ! ... compute the divergence
    nullify(div); allocate(div(Nc)); div(:) = 0.0_rfreal
    do i = 1, ND
      do j = 1, Nc
        div(j) = div(j) + VelGrad(j,region%global%t2Map(i,i))
      end do
    end do

    ! ... allocate space for div_j tau_{ij}
    nullify(divTau); allocate(divTau(Nc,ND)); divTau(:,:) = 0.0_rfreal

    ! ... store viscous fluxes on the boundary
    Call Save_Patch_Viscous_Flux(region, ng, 0, 0, state%flux) ! zero things out first

    do i = 1, ND
      do j = 1, ND

        ! momentum: (rho u_i)_t = ... (\mu { (u_i)_j + (u_j)_i })_j + (\lambda (u_k)_k)_i
        do k = 1, Nc
          state%flux(k) = state%tv(k,1) * (VelGrad(k,region%global%t2Map(i,j)) + VelGrad(k,region%global%t2Map(j,i))) * state%REinv &
                        + region%global%delta(i,j) * state%tv(k,2) * div(k) * state%REinv
        end do
        Call Save_Patch_Viscous_Flux(region, ng, i+1, j, state%flux)

        ! energy: (rho E)_t = ... [(\mu { (u_i)_j + (u_j)_i } + \lambda (u_k)_k \delta_{ij}) u_j]_i
        do k = 1, Nc
          state%flux(k) = state%flux(k) * UVW(k,j)
        end do
        Call Save_Patch_Viscous_Flux(region, ng, ND+2, i, state%flux)

      end do
    end do   
    
    ! ... compute the divergence of the viscous stress tensor
    ! ... compute (dmu/dx_j (du_i/dx_j + du_j/dx_i)
    do i = 1, Nc
      state%flux(i) = state%tv(i,1)
    end do
    do j = 1, ND
      state%dflux(:) = 0.0_rfreal
      Call FIRST_DERIV(region, ng, j, state%flux, state%dflux, 1.0_rfreal, .FALSE.)
      do i = 1, ND
        do k = 1, Nc
          divTau(k,i) = divTau(k,i) + state%dflux(k) * (VelGrad(k,region%global%t2Map(i,j)) + VelGrad(k,region%global%t2Map(j,i)))
        end do
      end do
    end do

    ! ... compute (dlambda/dx_i div)
    do i = 1, Nc
      state%flux(i) = state%tv(i,2)
    end do
    do i = 1, ND
      state%dflux(:) = 0.0_rfreal
      Call FIRST_DERIV(region, ng, i, state%flux, state%dflux, 1.0_rfreal, .FALSE.)
      do k = 1, Nc
        divTau(k,i) = divTau(k,i) + state%dflux(k) * div(k)
      end do
    end do

!!$    ! ... compute mu d2u_j/dx_idx_j + lambda d2u_k/dx_idx_k
!!$    do i = 1, Nc
!!$      state%flux(i) = div(i)
!!$    end do
!!$    do i = 1, ND
!!$      state%dflux(:) = 0.0_rfreal
!!$      call FIRST_DERIV(region, ng, i, state%flux, state%dflux, 1.0_rfreal, .FALSE.)
!!$      do k = 1, Nc
!!$        divTau(k,i) = divTau(k,i) + (state%tv(k,1) + state%tv(k,2)) * state%dflux(k)
!!$      end do
!!$    end do

    ! ... compute mu d2u_j/dx_idx_j + lambda d2u_k/dx_idx_k
    do i = 1, ND
      do j = 1, ND
        do k = 1, Nc
          state%flux(k) = UVW(k,j)
          state%dflux(k) = 0.0_rfreal
        end do
        call SECOND_DERIV(region, ng, i, j, state%flux, state%dflux, 1.0_rfreal, .FALSE.)
        do k = 1, Nc
          divTau(k,i) = divTau(k,i) + (state%tv(k,1) + state%tv(k,2)) * state%dflux(k)
        end do
      end do
    end do

    ! ... compute mu d2u_i/dx_jdx_j
    do i = 1, ND
      do k = 1, Nc
        state%flux(k) = UVW(k,i)
      end do
      do j = 1, ND
        state%dflux(:) = 0.0_rfreal
        call SECOND_DERIV(region, ng, j, j, state%flux, state%dflux, 1.0_rfreal, .FALSE.)
        do k = 1, Nc
          divTau(k,i) = divTau(k,i) + state%tv(k,1) * state%dflux(k)
        end do
      end do
    end do

    ! ... multiply by Reinv
    do i = 1, ND
      do k = 1, Nc
        divTau(k,i) = state%REinv * divTau(k,i)
      end do
    end do

    ! ... add in viscous terms to momentum equation
    do i = 1, ND
      do k = 1, Nc
        state%rhs(k,i+1) = state%rhs(k,i+1) + divTau(k,i) * grid%INVJAC(k)
      end do
    end do

    ! ... add in viscous terms to energy equation
    do i = 1, ND
      do k = 1, Nc
        state%rhs(k,ND+2) = state%rhs(k,ND+2) + divTau(k,i) * UVW(k,i)* grid%INVJAC(k)
      end do
      do j = 1, ND
        do k = 1, Nc
          state%flux(k) = state%tv(k,1) * (VelGrad(k,region%global%t2Map(i,j)) + VelGrad(k,region%global%t2Map(j,i))) + region%global%delta(i,j) * state%tv(k,2) * div(k)
          state%rhs(k,ND+2) = state%rhs(k,ND+2) + state%flux(k) * VelGrad(k,region%global%t2Map(i,j)) * state%REinv * grid%INVJAC(k)
        end do
      end do
    end do

    ! ... work on temperature-terms in energy equation
    TempGrad(:,:) = 0.0_rfreal
    do i = 1, ND

      ! ... dk/dx_i dT/dx_i
      do k = 1, Nc
        state%flux(k) = state%dv(k,2); state%dflux(k) = 0.0_rfreal
      end do
      Call FIRST_DERIV(region, ng, i, state%flux, state%dflux, 1.0_rfreal, .FALSE.)
      do k = 1, Nc
        TempGrad(k,i) = state%dflux(k)
      end do

      ! energy: (rho E)_t = ... (k T_i)_i
      do k = 1, Nc
        state%flux(k) = state%tv(k,3) * TempGrad(k,i) * state%REinv * state%PRinv
      end do
      Call Save_Patch_Viscous_Flux(region, ng, ND+2, i, state%flux)
 
      do k = 1, Nc
        state%flux(k) = state%tv(k,3); state%dflux(k) = 0.0_rfreal
      end do
      Call FIRST_DERIV(region, ng, i, state%flux, state%dflux, 1.0_rfreal, .FALSE.)
      do k = 1, Nc
        state%rhs(k,ND+2) = state%rhs(k,ND+2) + TempGrad(k,i) * state%dflux(k) * grid%INVJAC(k) * state%REinv * state%PRinv
      end do

      ! ... k d2T/dx_idx_i
      do k = 1, Nc
        state%flux(k) = state%dv(k,2); state%dflux(k) = 0.0_rfreal
      end do
      Call SECOND_DERIV(region, ng, i, i, state%flux, state%dflux, 1.0_rfreal, .FALSE.)
      do k = 1, Nc
        state%rhs(k,ND+2) = state%rhs(k,ND+2) + state%tv(k,3) * state%dflux(k) * grid%INVJAC(k) * state%REinv * state%PRinv
      end do
  
    end do

    ! ... deallocate
    ! deallocate(flux, dflux, tv)
    deallocate(UVW, div, divTau)

    return

  end subroutine Q1D_RHS_ViscousTerms_CARTESIAN

  subroutine Q1D_Update_Target(region, ng)

    USE ModGlobal
    USE ModDataStruct
    USE ModDeriv
    USE ModMPI

    implicit none

    TYPE (t_region), POINTER :: region
    INTEGER :: ng
    INTEGER :: flags

    ! ... local variables
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    type(t_mixt_input), pointer :: input
    type(t_patch), pointer :: patch
    integer :: i, j, k, l0, lp, npatch, ND, N(MAX_ND), Np(MAX_ND)
    real(rfreal) :: RHO, UX, UY, UZ, GAMMA, PRESSURE, TEMPERATURE
    real(rfreal) :: BASE_RHO, BASE_UX, BASE_UY, BASE_UZ, BASE_PRESSURE, BASE_TEMPERATURE, SPD_SND
    real(rfreal) :: epsilon, omega, sincos_arg, to_rad, angle, Heaviside, Heaviside_start_loc

    ! ... simplicity
    input => region%input
    state => region%state(ng)
    grid  => region%grid(ng)
    ND = input%ND
    to_rad = TWOPI / 360.0_rfreal
    Heaviside_start_loc = -12.0_rfreal ! ... sets the location of the Heavisde front at time t = 0. 

    ! ... by default
    Do k = 1, size(state%cv,2)
      Do l0 = 1, size(state%cv,1)
        state%cvTarget(l0,k) = state%cvTargetOld(l0,k)
      End Do
    End Do

    ! ... PLANE WAVE EXCITATION --- update target
    If ( input%bcic_planewave == TRUE ) then

      ! ... parameters
      epsilon = 10.0_rfreal**(input%bcic_amplitude/20.0_rfreal - 9.701_rfreal)
      omega   = input%bcic_frequency * TWOPI * input%LengRef / input%SndSpdRef
      angle   = input%bcic_angle_theta * to_rad

      ! ... update the interior target data
      Do l0 = 1, size(state%cv,1)

        ! ... ratio of specific heats
        GAMMA = state%gv(l0,1)

        ! ... base values
        BASE_RHO = state%cvTargetOld(l0,1)
        BASE_UX  = state%cvTargetOld(l0,2) / BASE_RHO; BASE_UY = 0.0_rfreal; BASE_UZ = 0.0_rfreal;
        if (ND >= 2) BASE_UY  = state%cvTargetOld(l0,3) / BASE_RHO
        if (ND >= 3) BASE_UZ  = state%cvTargetOld(l0,4) / BASE_RHO
        BASE_PRESSURE = (GAMMA-1.0_rfreal) * (state%cvTargetOld(l0,ND+2) - 0.5_rfreal * (BASE_UX**2+BASE_UY**2+BASE_UZ**2) * BASE_RHO)
        BASE_TEMPERATURE = GAMMA * BASE_PRESSURE / (GAMMA-1.0_rfreal) / BASE_RHO
        SPD_SND = Sqrt(GAMMA * BASE_PRESSURE / BASE_RHO)
       
        ! ... limit plane wave by Heaviside
        Heaviside = 0.0_rfreal
        if ((state%time(l0) -  (grid%XYZ(l0,1) - Heaviside_start_loc)/(BASE_UX + SPD_SND)) > 0.0_rfreal) then
           Heaviside = 1.0_rfreal
        end if

        ! ... plane wave
        sincos_arg = -omega * (grid%XYZ(l0,1) * sin(angle) + grid%XYZ(l0,2) * cos(angle) + state%time(l0) * (SPD_SND + BASE_UX)) 
        RHO        = epsilon * cos(sincos_arg) * Heaviside
        UX         = -sin(angle) * RHO * Heaviside
        UY         = -cos(angle) * RHO * Heaviside
        UZ         = 0.0_rfreal * Heaviside
        PRESSURE   = RHO * SPD_SND**2

        ! ... add in the wave
        state%cvTarget(l0,1) = BASE_RHO + RHO
        state%cvTarget(l0,2) = state%cvTarget(l0,1) * ( UX + BASE_UX )
        if (ND >= 2) state%cvTarget(l0,3) = state%cvTarget(l0,1) * ( UY + BASE_UY )
        if (ND == 3) state%cvTarget(l0,4) = state%cvTarget(l0,1) * ( UZ + BASE_UZ )
        state%cvTarget(l0,ND+2) = (BASE_PRESSURE + PRESSURE) / (GAMMA-1.0_rfreal) + 0.5_rfreal * ((BASE_UX+UX)**2+(BASE_UY+UY)**2+(BASE_UZ+UZ)**2) * state%cvTarget(l0,1)

      End Do

    End If

    ! ... update the boundary patch data
    Do npatch = 1, region%npatches

      patch => region%patch(npatch)
      grid => region%grid(patch%gridID)
      state => region%state(patch%gridID)

      N  = 1; Np = 1;
      do j = 1, grid%ND
        N(j)  = grid%ie(j)-grid%is(j)+1
        Np(j) = patch%ie(j) - patch%is(j) + 1
      end do;

      If ( (patch%bcType == NSCBC_INFLOW_VELOCITY_TEMPERATURE_FROM_SPONGE) .OR. &
           (patch%bcType == NSCBC_INFLOW_VELOCITY_TEMPERATURE) ) Then

        Do k = patch%is(3), patch%ie(3)
          Do j = patch%is(2), patch%ie(2)
            Do i = patch%is(1), patch%ie(1)

              l0 = (k- grid%is(3))* N(1)* N(2) + (j- grid%is(2))* N(1) + (i- grid%is(1)+1)
              lp = (k-patch%is(3))*Np(1)*Np(2) + (j-patch%is(2))*Np(1) + (i-patch%is(1)+1)

              ! ... ratio of specific heats
              GAMMA = state%gv(l0,1)

              ! ... base values
              BASE_RHO = state%cvTargetOld(l0,1)
              BASE_UX  = state%cvTargetOld(l0,2) / BASE_RHO; BASE_UY = 0.0_rfreal; BASE_UZ = 0.0_rfreal;
              if (ND >= 2) BASE_UY  = state%cvTargetOld(l0,3) / BASE_RHO
              if (ND >= 3) BASE_UZ  = state%cvTargetOld(l0,4) / BASE_RHO
              BASE_PRESSURE = (GAMMA-1.0_rfreal) * (state%cvTargetOld(l0,ND+2) - 0.5_rfreal * (BASE_UX**2+BASE_UY**2+BASE_UZ**2)*BASE_RHO)
              BASE_TEMPERATURE = GAMMA * BASE_PRESSURE / (GAMMA-1.0_rfreal) / BASE_RHO
              SPD_SND = Sqrt(GAMMA * BASE_PRESSURE / BASE_RHO)
       
              ! ... limit plane wave by Heaviside
              Heaviside = 0.0_rfreal
              if ((state%time(l0) -  (grid%XYZ(l0,1) - Heaviside_start_loc)/(BASE_UX + SPD_SND)) > 0.0_rfreal) then
                Heaviside = 1.0_rfreal
              end if

              patch%dudt_inflow(lp) = 0.0_rfreal
              If (ND >= 2) patch%dvdt_inflow(lp) = 0.0_rfreal
              If (ND == 3) patch%dwdt_inflow(lp) = 0.0_rfreal
              patch%dTdt_inflow(lp) = 0.0_rfreal

              If ( (input%bcic_planewave == TRUE) ) then

                ! ... plane wave
                sincos_arg = -omega * (grid%XYZ(l0,1) * sin(angle) + grid%XYZ(l0,2) * cos(angle) + state%time(l0) * (SPD_SND + BASE_UX)) 
                RHO        = epsilon * cos(sincos_arg) * Heaviside
                UX         = -sin(angle) * RHO * Heaviside
                UY         = -cos(angle) * RHO * Heaviside
                UZ         = 0.0_rfreal * Heaviside
                PRESSURE   = RHO * SPD_SND**2

                ! ... time derivative
                patch%dudt_inflow(lp) = -omega * UX * Heaviside
                if (ND >= 2) patch%dvdt_inflow(lp) = -omega * UY * Heaviside
                if (ND == 3) patch%dwdt_inflow(lp) = 0.0_rfreal * Heaviside
                patch%dTdt_inflow(lp) = -omega * BASE_TEMPERATURE * ( PRESSURE / BASE_PRESSURE - RHO / BASE_RHO ) * Heaviside

              End If

              if ( patch%bcType == NSCBC_INFLOW_VELOCITY_TEMPERATURE_FROM_SPONGE ) then
                patch%u_inflow(lp) = state%cvTarget(l0,2) / state%cvTarget(l0,1)
                if (ND >= 2) patch%v_inflow(lp) = state%cvTarget(l0,3) / state%cvTarget(l0,1)
                if (ND == 3) patch%w_inflow(lp) = state%cvTarget(l0,4) / state%cvTarget(l0,1)
                PRESSURE = (GAMMA-1.0_rfreal) * (state%cvTarget(l0,ND+2) - 0.5_rfreal * state%cvTarget(l0,1) * patch%u_inflow(lp)**2)
                if (ND >= 2) PRESSURE = PRESSURE - (GAMMA-1.0_rfreal) * 0.5_rfreal * state%cvTarget(l0,1)* patch%v_inflow(lp)**2
                if (ND == 3) PRESSURE = PRESSURE - (GAMMA-1.0_rfreal) * 0.5_rfreal * state%cvTarget(l0,1)* patch%w_inflow(lp)**2
                patch%T_inflow(lp) = GAMMA * PRESSURE / (GAMMA - 1.0_rfreal) / state%cvTarget(l0,1)
              else
                patch%u_inflow(lp) = 0.1_rfreal
                if (ND >= 2) patch%v_inflow(lp) = 0.0_rfreal
                if (ND == 3) patch%w_inflow(lp) = 0.0_rfreal
                patch%T_inflow(lp) = 1.0_rfreal / (GAMMA - 1.0_rfreal)
              end if

            End Do
          End Do
        End Do

      End If
    End Do

    !           ! ... add time-dependence to 'ramp' up values
    !           t0     = 5.0_rfreal
    !           region%global%delta  = 1.0_rfreal
    !           w_of_t = 0.5_rfreal * (1.0_rfreal + tanh((state%time(l0) - t0)/region%global%delta))
    !           dwdt   = 0.5_rfreal / (region%global%delta * (cosh((state%time(l0)-t0)/region%global%delta))**2)
    !           w_of_t = 1.0_rfreal
    !           dwdt   = 0.0_rfreal

  end subroutine Q1D_Update_Target

  subroutine Q1D_RHS_ViscousTerms_Strong(region, ng, flags, VelGrad, TempGrad, tvCor)

    USE ModGlobal
    USE ModDataStruct
    USE ModDeriv
    USE ModMPI
    USE ModNavierStokesRHS

    Implicit None

    ! ... subroutine arguments
    integer :: ng
    integer :: flags
    real(rfreal), pointer :: VelGrad(:,:), TempGrad(:,:)
    real(rfreal), pointer, optional :: tvCor(:,:) ! JKim 01/2008
    type(t_region), pointer :: region

    ! ... local variables
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    type(t_mixt_input), pointer :: input
    type(t_patch), pointer :: patch
    real(rfreal), pointer ::  UVW(:,:)
    integer :: ND, Nc, N(MAX_ND), i, j, Np(MAX_ND), patchID
    integer :: ip, jp, kp

    ! ... local variables and arrays for viscous and heat fluxes, JKim 02/2007
    integer :: k, l, m, p
    real(rfreal) :: viscous_time
    real(rfreal), pointer :: StrnRt(:,:), HeatFlux(:,:) ! ... JKim 07/2008
    real(rfreal), pointer :: div(:)
    real(rfreal), pointer :: tv(:,:) ! ... JKim 06/2008

    ! ... simplicity
    input => region%input
    grid => region%grid(ng)
    state => region%state(ng)
    ND = grid%ND
    Nc = grid%nCells
    N  = 1
    do j = 1, grid%ND
      N(j) = grid%ie(j)-grid%is(j)+1
    end do ! j

    if (flags == INVISCID_ONLY) return ! because it's inviscid computation

    if (state%RE == 0.0_rfreal .AND. input%shock == 0 .AND. input%LES == 0) then
      call graceful_exit(region%myrank, 'At least either shock-capturing or LES should be on to compute viscous terms.')
    end if ! state%RE

    ! ... allocate space for arrays
    ! nullify(flux); allocate(flux(Nc)); flux = 0.0_rfreal
    ! nullify(dflux); allocate(dflux(Nc)); dflux = 0.0_rfreal
    nullify(UVW); allocate(UVW(Nc,ND)); UVW = 0.0_rfreal


!!$    ! ... Step 1: viscosity correction is now considered
!!$    ! ...         state%tv still not updated, JKim 01/2008
!!$    nullify(tv); allocate(tv(Nc,input%nTv)); tv = 0.0_rfreal ! ... JKim 06/2008
!!$    tv(:,1:input%nTv) = state%tv(:,1:input%nTv)              ! ... JKim 06/2008
!!$    if (PRESENT(tvCor) .eqv. .true.) then
!!$      tv(:,1:input%nTv) = tv(:,1:input%nTv) + tvCor(:,1:input%nTv) ! ... JKim 06/2008
!!$    end if ! PRESENT(tvCor)

    ! ... Step 2: precompute primitive velocities and store in UVW(:,:)
    do l = 1, ND
      UVW(:,l) = state%cv(:,region%global%vMap(l+1)) * state%dv(:,3)
    end do ! l


    ! ... Step 3: precompute strain-rate tensor using VelGrad
    nullify(StrnRt); allocate(StrnRt(Nc,ND*(ND+1)/2)); StrnRt = 0.0_rfreal
    CALL NS_RHS_StrnRt(region, grid, VelGrad, StrnRt)

    ! ... Step 4: compute the divergence
    nullify(div); allocate(div(Nc)); div = 0.0_rfreal
    do i = 1, ND
      div(:) = div(:) + StrnRt(:,region%global%t2MapSym(i,i))
    end do

    ! ... store viscous fluxes on the boundary
    Call Save_Patch_Viscous_Flux(region, ng, 0, 0, state%flux) ! zero things out first

    ! ... Step 5: compute heat-flux vector
    nullify(HeatFlux); allocate(HeatFlux(Nc,ND)); HeatFlux = 0.0_rfreal
    do j = 1,ND
      HeatFlux(:,j) = 0.0_rfreal ! defensive programming
      do k = 1,ND
        HeatFlux(:,j) = HeatFlux(:,j) + grid%MT1(:,region%global%t2Map(k,j)) * TempGrad(:,k)
      end do ! k
      HeatFlux(:,j) = -state%tv(:,3) * grid%JAC(:) * HeatFlux(:,j)
    end do ! j

    ! ... Step 6: directly compute viscous terms in momentum equations
    do i = 1,ND
      do l = 1,ND
        state%flux(:) = 0.0_rfreal
        do j = 1,ND
          state%dflux(:) = state%tv(:,2) * region%global%delta(i,j) * div(:) + & ! viscous stress tensor
                           state%tv(:,1) * 2.0_rfreal * StrnRt(:,region%global%t2MapSym(i,j))
          state%flux(:) = state%flux(:) + grid%MT1(:,region%global%t2Map(l,j)) * state%dflux(:)
        end do ! j
        call APPLY_OPERATOR(region, ng, 1, l, state%flux, state%dflux, .FALSE.)
        state%rhs(:,i+1) = state%rhs(:,i+1) + state%dflux(:) * state%REinv
        
        state%flux(:) = state%flux(:) * state%REinv
        Call Save_Patch_Viscous_Flux(region, ng, i+1, l, state%flux)
      end do ! l
    end do ! i

    ! ... Step 7: directly compute viscous terms in energy equations
    do l = 1,ND
      state%flux(:) = 0.0_rfreal
      do j = 1,ND
        state%dflux(:) = 0.0_rfreal
        do k = 1,ND
          state%dflux(:) = state%dflux(:) + UVW(:,k) * &
                    (state%tv(:,2) * region%global%delta(j,k) * div(:) + & ! viscous stress tensor
                     state%tv(:,1) * 2.0_rfreal * StrnRt(:,region%global%t2MapSym(j,k)))
        end do ! k
        state%flux(:) = state%flux(:) + grid%MT1(:,region%global%t2Map(l,j)) * state%dflux(:)
      end do ! j
      call APPLY_OPERATOR(region, ng, 1, l, state%flux, state%dflux, .FALSE.)
      state%rhs(:,ND+2) = state%rhs(:,ND+2) + state%dflux(:) * state%REinv

      state%flux(:) = state%flux(:) * state%REinv
      Call Save_Patch_Viscous_Flux(region, ng, ND+2, l, state%flux)
    end do ! l

    ! ... Step 8: directly compute heat transfer terms in energy equations
    do l = 1,ND
      state%flux(:) = 0.0_rfreal
      do j = 1,ND
        state%flux(:) = state%flux(:) + grid%MT1(:,region%global%t2Map(l,j)) * HeatFlux(:,j)
      end do ! j
      call APPLY_OPERATOR(region, ng, 1, l, state%flux, state%dflux, .FALSE.)
      state%rhs(:,ND+2) = state%rhs(:,ND+2) - state%dflux(:) * state%REinv * state%PRinv

      state%flux(:) = -state%flux(:) * state%REinv * state%PRinv
      Call Save_Patch_Viscous_Flux(region, ng, ND+2, l, state%flux)
    end do ! l

    ! ... deallocate
    ! deallocate(flux, dflux
    deallocate(UVW)
    deallocate(StrnRt, div, HeatFlux)
    ! deallocate(tv) ! ... JKim 06/2008

    return

  end subroutine Q1D_RHS_ViscousTerms_Strong

  subroutine Q1D_SAT_Form_Roe_Matrices(u_in, u_out, gamma, norm, tmat, tinv, lambda)

    USE ModGlobal

    Implicit None

    Real(rfreal), Pointer :: u_in(:), u_out(:), tmat(:,:), tinv(:,:), norm(:), lambda(:,:)
    Real(rfreal) :: gamma, rho_in, ux_in, uy_in, uz_in, ke_in, p_in, en_in, h_in
    Real(rfreal) :: gm1, rho_out, ux_out, uy_out, uz_out, ke_out, p_out, en_out, h_out
    Real(rfreal) :: cc, bb, ux_Roe, uy_Roe, uz_Roe, h_Roe, ke_Roe, spd_snd_Roe
    Real(rfreal) :: XI_X_TILDE, XI_Y_TILDE, XI_Z_TILDE, ucon, rho, alpha, theta, beta, phi2
    Real(rfreal) :: XI_X, XI_Y, XI_Z, denom
    Integer :: N, ND, jj

    ! ... size of this problem
    ND = size(u_in) - 2

    ! ... constants
    gm1 = gamma - 1.0_rfreal

    ! ... form primative variables from "in"
    rho_in = u_in(1)
    ux_in  = u_in(2) / rho_in; uy_in = 0.0_rfreal; uz_in = 0.0_rfreal
    ke_in  = 0.5_rfreal * (ux_in**2 + uy_in**2 + uz_in**2)
    en_in  = u_in(ND+2)/rho_in - ke_in
    p_in   = gm1 * rho_in * en_in
    h_in   = (gamma / gm1) * p_in / rho_in + ke_in 

    ! ... form primative variables from "out"
    rho_out = u_out(1)
    ux_out  = u_out(2) / rho_out; uy_out = 0.0_rfreal; uz_out = 0.0_rfreal
    ke_out  = 0.5_rfreal * (ux_out**2 + uy_out**2 + uz_out**2)
    en_out  = u_out(ND+2)/rho_out - ke_out
    p_out   = gm1 * rho_out * en_out
    h_out   = (gamma / gm1) * p_out / rho_out + ke_out 

    ! ... form Roe variables
    cc = sqrt(rho_out / rho_in)
    bb = 1.0_rfreal / (1.0_rfreal + cc)
    ux_Roe = (Ux_in + Ux_out * cc) * bb
    uy_Roe = (Uy_in + Uy_out * cc) * bb
    uz_Roe = (Uz_in + Uz_out * cc) * bb
    h_Roe  = ( h_in +  h_out * cc) * bb
    ke_Roe = 0.5_rfreal * (ux_Roe**2 + uy_Roe**2 + uz_Roe**2)
    spd_snd_Roe = sqrt(gm1*(h_Roe - ke_Roe))

    ! ... variable renaming
    XI_X = norm(1); XI_Y = 0.0_rfreal; XI_Z = 0.0_rfreal
    denom = 1.0_rfreal / sqrt(XI_X**2 + XI_Y**2 + XI_Z**2)
    XI_X_TILDE = XI_X * denom
    XI_Y_TILDE = XI_Y * denom
    XI_Z_TILDE = XI_Z * denom
    RHO = rho_in

    ! ... form transformation matrices
    ucon = ux_Roe * XI_X + uy_Roe * XI_Y + uz_Roe * XI_Z

    ! ... directly from Pulliam & Chaussee
    alpha = 0.5_rfreal * RHO / SPD_SND_ROE
    beta = 1.0_rfreal / (RHO * SPD_SND_ROE)
    theta = XI_X_TILDE * UX_ROE + XI_Y_TILDE * UY_ROE
    phi2 = 0.5_rfreal * (GAMMA - 1.0_rfreal) * (UX_ROE**2 + UY_ROE**2)
    Tmat(1,1) = 1.0_rfreal
    Tmat(1,2) =  alpha
    Tmat(1,3) = -alpha
    Tmat(2,1) = UX_ROE
    Tmat(2,2) =  alpha * (UX_ROE + SPD_SND_ROE)
    Tmat(2,3) = -alpha * (UX_ROE - SPD_SND_ROE)
    Tmat(3,1) = PHI2 / (GAMMA - 1.0_rfreal)
    Tmat(3,2) =  alpha * ((PHI2 + SPD_SND_ROE**2)/(GAMMA - 1.0_rfreal) + SPD_SND_ROE * THETA)
    Tmat(3,3) = -alpha * ((PHI2 + SPD_SND_ROE**2)/(GAMMA - 1.0_rfreal) - SPD_SND_ROE * THETA)

    Tinv(1,1) = 1.0_rfreal - PHI2 / SPD_SND_ROE**2
    Tinv(1,2) = (GAMMA - 1.0_rfreal) * UX_ROE / SPD_SND_ROE**2
    Tinv(1,3) = -(GAMMA - 1.0_Rfreal) / SPD_SND_ROE**2
    Tinv(2,1) = beta * (PHI2 - SPD_SND_ROE * theta)
    Tinv(2,2) = beta * (XI_X_TILDE * SPD_SND_ROE - (GAMMA - 1.0_rfreal) * UX_ROE)
    Tinv(2,3) = beta * (GAMMA - 1.0_rfreal)
    Tinv(3,1) = -beta * (PHI2 + SPD_SND_ROE * theta)
    Tinv(3,2) = beta * (XI_X_TILDE * SPD_SND_ROE + (GAMMA - 1.0_rfreal) * UX_ROE)
    Tinv(3,3) = -beta * (GAMMA - 1.0_rfreal)

    ! ... compute the diagonal matrix lambda
    Lambda(:,:) = 0.0_rfreal
    do jj = 1, ND
      Lambda(jj,jj) = ucon
    end do
    Lambda(ND+1,ND+1) = ucon + SPD_SND_ROE * sqrt(XI_X**2 + XI_Y**2)
    Lambda(ND+2,ND+2) = ucon - SPD_SND_ROE * sqrt(XI_X**2 + XI_Y**2)

  end subroutine Q1D_SAT_Form_Roe_Matrices

  subroutine Q1D_SAT_Artificial_Dissipation (region, ng)

    USE ModGlobal
    USE ModDataStruct
    USE ModDeriv
    USE ModMPI

    Implicit None

    ! ... local variables
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    type(t_mixt_input), pointer :: input
    type(t_region), pointer :: region
    real(rfreal), pointer :: SpectralRadius(:,:)
    integer :: ng, i, ii, var, dir, ND, Nc
    real(rfreal) :: delta_x, diss_power

    ! ... simplicity
    input => region%input
    grid => region%grid(ng)
    state => region%state(ng)
    ND = grid%ND
    Nc = grid%nCells

    ! ... determine diss_power
!!$    Select Case ( input%spaceOrder )
!!$      Case ( 5 ) diss_power = 8.0_rfreal
!!$      Case ( 3 ) diss_power = 4.0_rfreal
!!$      Case ( 2 ) diss_power = 2.0_rfreal
!!$    End Select
  
    ! ... compute the spectral radius
    allocate(SpectralRadius(Nc,ND))
    Call Q1D_Spectral_Radius(region, ng, SpectralRadius)

    ! ... Loop over all variables
    Do var = 1, ND + 2

      ! ... Loop over each direction
      Do dir = 1, ND

        ! ... apply the DI operator first
        Do ii = 1, Nc
          state%flux(ii) = state%cv(ii,var)
        End Do
        Call APPLY_OPERATOR(region, ng, input%iSATArtDiss, dir, state%flux, state%dflux, .FALSE.)

        ! ... scale the result by the spectral radius
        Do ii = 1, Nc
          state%flux(ii) = state%dflux(ii) * SpectralRadius(ii,dir)
        End Do

        ! ... apply the DI^(Transpose) operator next
        Call APPLY_OPERATOR(region, ng, input%iSATArtDissSplit, dir, state%flux, state%dflux, .FALSE.)

        ! ... add the result to the rhs
        Do ii = 1, Nc

          ! ... construct the normalized metrics
          !XI_X = grid%MT1(ii,region%global%t2Map(dir,1)); XI_Y = 0.0_rfreal; XI_Z = 0.0_rfreal
          !if (ND >= 2) XI_Y = grid%MT1(ii,region%global%t2Map(dir,2))
          !if (ND == 3) XI_Z = grid%MT1(ii,region%global%t2Map(dir,3))

          ! ... multiply by the Jacobian
          ! delta_x = grid%INVJAC(ii) / sqrt(XI_X**2 + XI_Y**2 + XI_Z**2)
          ! state%rhs(ii,var) = state%rhs(ii,var) + input%amount_SAT_diss * (delta_x)**diss_power * state%dflux(ii)
          state%rhs(ii,var) = state%rhs(ii,var) + input%amount_SAT_diss * state%dflux(ii) * grid%INVJAC(ii)

       End Do

      End Do

    End Do

    ! ... clean up
    deallocate(SpectralRadius)

  end subroutine Q1D_SAT_Artificial_Dissipation

  subroutine Q1D_Spectral_Radius(region, ng, SpectralRadius)

    USE ModGlobal
    USE ModDataStruct
    USE ModDeriv
    USE ModMPI

    Implicit None

    ! ... local variables
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    type(t_mixt_input), pointer :: input
    type(t_region), pointer :: region
    type(t_patch), pointer :: patch
    real(rfreal), pointer :: SpectralRadius(:,:)
    integer :: ng, i, j, k, l0, ii, var, dir, ND, Nc, N(MAX_ND), Np(MAX_ND), npatch, l1, jj, sgn, normDir
    real(rfreal) :: rho, u, v, w, p, gam, en, a, rj11, rj12, rj13, rj21, rj22, rj23, rj31, rj32, rj33
    real(rfreal) :: sq1, sq2, sq3, ucon, vcon, wcon
    integer, allocatable :: inorm(:)
    integer :: length_inorm

    ! ... simplicity
    input => region%input
    grid => region%grid(ng)
    state => region%state(ng)
    ND = grid%ND
    Nc = grid%nCells

    ! ... Modeled after Magnus's code (it is not clear this is the right way to do this)
    SELECT CASE (ND)

    CASE (1)

      Do ii = 1, Nc

        gam = state%gv(ii,1)
        rho = state%cv(ii,1)
        u   = state%cv(ii,2) / rho
        en  = state%cv(ii,3) - 0.5_rfreal * rho * (u*u)
        p   = (gam - 1.0_rfreal) * en
        a   = sqrt(gam * p / rho)

        rj11 = grid%MT1(ii,region%global%t2Map(1,1))

        ucon = rj11 * u

        sq1  = sqrt( rj11 * rj11 )

        SpectralRadius(ii,1) = (abs(ucon) + a*sq1)

      End Do

    CASE (2) 

      Do ii = 1, Nc

        gam = state%gv(ii,1)
        rho = state%cv(ii,1)
        u   = state%cv(ii,2) / rho
        v   = state%cv(ii,3) / rho
        en  = state%cv(ii,4) - 0.5_rfreal * rho * (u*u + v*v)
        p   = (gam - 1.0_rfreal) * en
        a   = sqrt(gam * p / rho)

        rj11 = grid%MT1(ii,region%global%t2Map(1,1))
        rj12 = grid%MT1(ii,region%global%t2Map(1,2))

        rj21 = grid%MT1(ii,region%global%t2Map(2,1))
        rj22 = grid%MT1(ii,region%global%t2Map(2,2))

        ucon = rj11 * u + rj12 * v
        vcon = rj21 * u + rj22 * v

        sq1  = sqrt( rj11 * rj11 + rj12 * rj12 )
        sq2  = sqrt( rj21 * rj21 + rj22 * rj22 )

        SpectralRadius(ii,1) = (abs(ucon) + a*sq1)
        SpectralRadius(ii,2) = (abs(vcon) + a*sq2)

      End Do

    Case (3)

      Do ii = 1, Nc

        gam = state%gv(ii,1)
        rho = state%cv(ii,1)
        u   = state%cv(ii,2) / rho
        v   = state%cv(ii,3) / rho
        w   = state%cv(ii,4) / rho
        en  = state%cv(ii,5) - 0.5_rfreal * rho * (u*u + v*v + w*w)
        p   = (gam - 1.0_rfreal) * en
        a   = sqrt(gam * p / rho)

        rj11 = grid%MT1(ii,region%global%t2Map(1,1))
        rj12 = grid%MT1(ii,region%global%t2Map(1,2))
        rj13 = grid%MT1(ii,region%global%t2Map(1,3))

        rj21 = grid%MT1(ii,region%global%t2Map(2,1))
        rj22 = grid%MT1(ii,region%global%t2Map(2,2))
        rj23 = grid%MT1(ii,region%global%t2Map(2,3))

        rj31 = grid%MT1(ii,region%global%t2Map(3,1))
        rj32 = grid%MT1(ii,region%global%t2Map(3,2))
        rj33 = grid%MT1(ii,region%global%t2Map(3,3))

        ucon = rj11 * u + rj12 * v + rj13 * w
        vcon = rj21 * u + rj22 * v + rj23 * w
        wcon = rj31 * u + rj32 * v + rj33 * w

        sq1  = sqrt( rj11 * rj11 + rj12 * rj12 + rj13 * rj13 )
        sq2  = sqrt( rj21 * rj21 + rj22 * rj22 + rj23 * rj23 )
        sq3  = sqrt( rj31 * rj31 + rj32 * rj32 + rj33 * rj33 )

        SpectralRadius(ii,1) = (abs(ucon) + a*sq1)
        SpectralRadius(ii,2) = (abs(vcon) + a*sq2)
        SpectralRadius(ii,3) = (abs(wcon) + a*sq3)

      End Do

    End Select

    ! ... the SpectralRadius should diminish near the boundaries
    SELECT CASE (region%input%spaceOrder)
    CASE (5)
      allocate(inorm(2))    
    CASE DEFAULT
      allocate(inorm(1))
    END SELECT
    length_inorm = size(inorm)

    if ( length_inorm > 0 ) Then

      Do npatch = 1, region%npatches

        patch => region%patch(npatch)
        grid => region%grid(patch%gridID)
        state => region%state(patch%gridID)
        normDir = abs(patch%normDir)
        sgn = normDir / patch%normDir

        N  = 1; Np = 1;
        do j = 1, grid%ND
          N(j)  = grid%ie(j)-grid%is(j)+1
          Np(j) = patch%ie(j) - patch%is(j) + 1
        end do;

        if ( patch%bcType /= SPONGE ) then

          Do k = patch%is(3), patch%ie(3)
            Do j = patch%is(2), patch%ie(2)
              Do i = patch%is(1), patch%ie(1)

                l0 = (k- grid%is(3))* N(1)* N(2) + (j- grid%is(2))* N(1) + (i- grid%is(1)+1)

                ! ... normal indices
                do jj = 1, length_inorm
                  if (normDir == 1) then
                    l1 = l0 + sgn*(jj-1)
                  else if (normDir == 2) then
                    l1 = l0 + sgn*(jj-1)*N(1)
                  else
                    l1 = l0 + sgn*(jj-1)*N(1)*N(2)
                  end if
                  inorm(jj) = l1
                end do

                SpectralRadius(inorm(1:length_inorm),:) = 0.0_rfreal

              End Do
            End Do
          End Do

        end if

      end do

    End If

  End Subroutine Q1D_Spectral_Radius

  Subroutine Q1D_Shock_Capture(region, ng, flags)

    USE ModGlobal
    USE ModDataStruct
    USE ModDeriv
    USE ModMPI

    Implicit None

    ! ... local variables
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    type(t_mixt_input), pointer :: input
    type(t_region), pointer :: region
    type(t_mpi_timings), pointer :: mpi_timings
    real(rfreal) :: timer
    integer :: flags, ng, i, Nc
    real(rfreal), pointer :: flux(:), dflux(:)

    ! ... simplicity
    input       => region%input
    grid        => region%grid(ng)
    state       => region%state(ng)
    mpi_timings => region%mpi_timings(ng)
    Nc          = grid%nCells

    ! ... buffers
    allocate(flux(Nc),dflux(Nc))

    ! ... compute the strain rate
    do i = 1, Nc
      flux(i) = state%cv(i,2) * state%dv(i,3)
    end do
    call APPLY_OPERATOR_box(region, ng, input%iFirstDeriv, 1, flux, dflux, .FALSE., 1)
    do i = 1, Nc
      flux(i) = abs(dflux(i) * grid%JAC(i))
    end do

    ! ... compute the gradient of the strain rate tensor
    call APPLY_OPERATOR_box(region, ng, input%iFourthDeriv, 1, flux, dflux, .FALSE., 1)

    ! ... multiply by density and filter
    do i = 1, Nc
      flux(i) = abs(dflux(i) * grid%JAC(i)**4) * state%cv(i,1)
    end do
    call internal_filter_box(region, ng, input%iFilterShockTG, 1, flux, dflux, .FALSE., 0)

    ! ... use the gradient |region%global%delta^r S| to compute the {mu,beta} coefficients
    do i = 1, Nc
      state%tvCor(i,1) = input%HyperCmu   * dflux(i) * grid%INVJAC(i)**6 * state%RE
      state%tvCor(i,2) = input%HyperCbeta * dflux(i) * grid%INVJAC(i)**6 * state%RE 
    end do
 
    ! ... repeat for the energy equation
    do i = 1, Nc
      flux(i) = state%dv(i,1) * state%dv(i,3) / (state%gv(i,1) - 1.0_rfreal)
    end do
    call APPLY_OPERATOR_box(region, ng, input%iFourthDeriv, 1, flux, dflux, .FALSE., 1)
    ! ... multiply by density x speed_of_sound / Temperature and filter
    do i = 1, Nc
      flux(i) = abs(dflux(i) * grid%JAC(i)**4) * state%cv(i,1) / state%dv(i,2) * sqrt(state%gv(i,1) * state%dv(i,1) * state%dv(i,3))
    end do
    call internal_filter_box(region, ng, input%iFilterShockTG, 1, flux, dflux, .FALSE., 0)

    do i = 1, Nc
      state%tvCor(i,3) = input%HyperCkappa * dflux(i) * grid%INVJAC(i)**5 * state%RE * state%PR
    end do

    ! ... convert {mu,beta} into lambda
    do i = 1, Nc
      state%tvCor(i,2) = state%tvCor(i,2) - 2.0_rfreal * state%tvCor(i,1) / 3.0_rfreal
    end do

    ! ... clean up
    deallocate(flux,dflux)

  End Subroutine Q1D_Shock_Capture

  Subroutine Q1D_Shock_Filter(region, ng)

    USE ModGlobal
    USE ModDataStruct
    USE ModDeriv
    USE ModMPI

    Implicit None

    ! ... local variables
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    type(t_mixt_input), pointer :: input
    type(t_region), pointer :: region
    type(t_mpi_timings), pointer :: mpi_timings
    real(rfreal) :: timer, ri, rth, xshockv(1)
    integer :: flags, ng, i, j, Nc, ND, dir, local_max_loc(1), ierr
    real(rfreal), pointer :: flux(:), dflux(:), DF(:), DB(:), dil(:), detect(:), coeff(:), sensor(:), coeffF(:,:), coeffB(:,:)
    real(rfreal), pointer :: global_shock_loc(:), global_shock_sensor(:)

    ! ... simplicity
    input       => region%input
    grid        => region%grid(ng)
    state       => region%state(ng)
    mpi_timings => region%mpi_timings(ng)
    Nc          = grid%nCells
    ND          = grid%ND
    rth         = input%ShockFilterRTH

    ! ... buffers
    allocate(flux(Nc),dflux(Nc),DF(Nc),DB(Nc),dil(Nc),detect(Nc),coeff(Nc),coeffF(Nc,ND),coeffB(Nc,ND),sensor(Nc))

    ! ... compute the dilatation
    do i = 1, Nc
      dil(i) = state%dv(i,1)
    end do

!!$    do j = 1, ND
!!$      do i = 1, Nc
!!$        flux(i) = state%cv(i,j) * state%dv(i,3)
!!$      end do
!!$      Call FIRST_DERIV(region, ng, j, flux, dil, 1.0_rfreal, .FALSE.)
!!$    end do

    ! ... compute the shock sensor & coeff
    do j = 1, ND
      Call Internal_Filter_box(region, ng, input%iShockFilterDetect, j, dil, detect, .FALSE., 1)
      do i = 1, Nc
        flux(i) = detect(i)
      end do
      Call Internal_Filter_box(region, ng, input%iShockFilterSensorForward, j, flux, dflux, .FALSE., 1)
      do i = 1, Nc
        sensor(i) = 0.5_rfreal * dflux(i)**2
      end do
      Call Internal_Filter_box(region, ng, input%iShockFilterSensorBackward, j, flux, dflux, .FALSE., 1)
      do i = 1, Nc
        sensor(i) = sensor(i) + 0.5_rfreal * dflux(i)**2
      end do
      do i = 1, Nc
        ri = sensor(i) * grid%INVJAC(i)**2 * state%cv(i,1) / (state%gv(i,1) * state%dv(i,1)) + 1E-15
        coeff(i) = 0.5_rfreal * (1.0_rfreal - rth/ri + abs(1.0_rfreal - rth/ri))
      end do
      Call Internal_Filter_box(region, ng, input%iShockFilterCoeffForward, j, coeff, dflux, .FALSE., 0)
      do i = 1, Nc
        coeffF(i,j) = dflux(i)
      end do
      Call Internal_Filter_box(region, ng, input%iShockFilterCoeffBackward, j, coeff, dflux, .FALSE., 0)
      do i = 1, Nc
        coeffB(i,j) = dflux(i)
      end do
    end do

    ! ... find shock location
    allocate(global_shock_loc(numproc), global_shock_sensor(numproc))
    Call MPI_Allgather(maxval(sensor), 1, MPI_DOUBLE_PRECISION, global_shock_sensor, 1, MPI_DOUBLE_PRECISION, grid%comm, ierr)
    Call MPI_Allgather(grid%xyz(maxloc(sensor),1), 1, MPI_DOUBLE_PRECISION, global_shock_loc, 1, MPI_DOUBLE_PRECISION, grid%comm, ierr)
    xshockv = global_shock_loc(maxloc(global_shock_sensor))
    state%xshock = xshockv(1)
    deallocate(global_shock_loc, global_shock_sensor)

    ! ... loop over all directions
    do dir = 1, ND

      ! ... loop over all variables
      do j = 1, size(state%cv,2)

        ! ... first compute the dissipation 'fluxes' D_{i+1/2} and D_{i-1/2}
        do i = 1, Nc
          flux(i) = state%cv(i,j)
        end do
        call internal_filter_box(region, ng, input%iShockFilterFluxForward,  dir, flux, DF, .FALSE., 1)
        call internal_filter_box(region, ng, input%iShockFilterFluxBackward, dir, flux, DB, .FALSE., 1)

        ! ... now update
        do i = 1, Nc
          state%cv(i,j) = state%cv(i,j) - (coeffF(i,dir) * DF(i) - coeffB(i,dir) * DB(i))
        end do

      end do

      ! ... repeat for auxillary variables
      if (associated(state%auxVars) .eqv. .true.) then

        do j = 1, size(state%auxVars,2)

          ! ... first compute the dissipation 'fluxes' D_{i+1/2} and D_{i-1/2}
          do i = 1, Nc
            flux(i) = state%auxVars(i,j)
          end do
          call internal_filter_box(region, ng, input%iShockFilterFluxForward,  dir, flux, DF, .FALSE., 1)
          call internal_filter_box(region, ng, input%iShockFilterFluxBackward, dir, flux, DB, .FALSE., 1)

          ! ... now update
          do i = 1, Nc
            state%auxVars(i,j) = state%auxVars(i,j) - (coeffF(i,dir) * DF(i) - coeffB(i,dir) * DB(i))
          end do

        end do

      end if

    end do

    ! ... cleanup
    deallocate(flux,dflux,DF,DB,dil,detect,coeff,coeffF,coeffB,sensor)

  End Subroutine Q1D_Shock_Filter

END MODULE ModQ1D
