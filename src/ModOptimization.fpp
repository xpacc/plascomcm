! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
!----------------------------------------------------------------------
!
! ModOptimization.f90
! 
! - module doing control optimization
!
! Revision history
! - 30 Jul 2009 : JKim : initial code
! - 04 Sep 2009 : JKim : forward & adjoint N-S solvers integrated w/o restarting
! - 04 Nov 2009 : JKim : restarting at arbitrary point of optimization
! - 29 Apr 2010 : JKim : integration into CVS
!
! TO-DOs
! - Currently, when optimization is restarted, we cannot change number of proc-
!   essors; this is related to reading gradient and CG vector files; see 
!      ControlGradient_PreviousControlGradient
!      ReadCGVectors
!   for this restriction and think about more general solution!!
! - initializing avTarget from an av-target file is ad-hoc in InitializeState
! - solid wall BC of adjoint N-S not implemented
!
! MUST-DOs in optimization
! - Time-averaged field must be obtained from a baseline case before optimization, 
!   which will be the target state for sponge buffer zone (no RANS solution!).  
!   Make sure you run a baseline simulation with this target state for a while 
!   before applying control since flow needs to adjust itself to new target.  
! - Time-averaged field also provide uncontrolled time-averaged data to define 
!   cost functional defined in terms of fluctuation.
! - Restart file (restart_fname_cv) is the last baseline simulation *.restart 
!   file and should be kept throughout optimization; *.q file is not usable so 
!   rename the extension to be *.restart.
! - The file 'stdin_actPtID' should not be modified during optimization; it may 
!   be generated under whatever initial simulation condition such as # or proc-
!   essors per se.  If one changes number of processors of an optimization run, 
!   this file keeps track of IDs of each actuating point so that the code can 
!   access saved conjugate gradient data.
!
! Misc.
! - region%ITMAX = 50 in ModRegion.f90
! - mnbrak always starting from ax = input%AdjOptim_initAlpha?
! - xx = 5.0_rfreal * input%AdjOptim_initAlpha okay or not?
! - Check files starting from "stdout_" for convergence history
! - First uncontrolled forward & adjoint N-S runs are kept at ./IC;
!   ./iterxxx stores all solution files from 'xxx'-th conjugate gradient 
!   iteration.
! - CostFunctional & ControlGradient are always called in pair (CostFunctional 
!   first and then ControlGradient) except in mnbrak where only CostFunctional 
!   is called.  This is quite important in bookkeeping output solution files; 
!   i.e. forward solutions should stay in the current directory until adjoint 
!   run is finalized; we don't need to do that in mnbrak.  Module global variable 
!   curRunningMode controls this.
!   P.S. one exception in dbrent exists; look for "IF (fu > fx) EXIT".  In this 
!        case, a corresponding adjoint run is not made.
! - Target and actuation regions almost always have spatial distribution to 
!   avoid spatial discontinuity that finite difference can never handle; thus 
!   targ%distFunc(:) or act%distFunc(:) almost always are needed in manipulating 
!   target or actuation variables
!
! Things to consider when something's gone wrong
! (BEFORE RUNNING)
! - Is time horizon long enough to collect information on flow physics?
! - Is my actuator (or control target) placed correctly; does it have spatial 
!   distribution that our finite difference can resolve?
! - Is frequency of writing forward solutions optimal with respect to interpol-
!   ation of forward N-S solutions?; if too small, waste of resource, if too 
!   large, inaccurate forward solutions are fed to adjoint N-S solver.
! (WHEN CRASHES)
! - Is my initial alpha too large?; i.e. input%AdjOptim_initAlpha
! (IF INACCURATE OR NOT CONVERGING)
! - Is cost functional or gradient sensitive to shape function of target or 
!   actuation region?; i.e. targ%distFunc(:), act%distFunc(:)
! - Epsilons for determining CG convergence too small or too large?:
!     input%AdjOptim_eps, input%AdjOptim_deps, input%AdjOptim_dbrent_eps
! - input%AdjOptim_dbrent_eps can be 1.0 to accelerate initial optimization 
!   test; effective turn-off of Brent minimization
!
!-----------------------------------------------------------------------
MODULE ModOptimization

  USE ModGlobal

  implicit none

  ! ... module variables and arrays controlling optimization steps
  INTEGER, PARAMETER :: OPTIM_IC = 1, & ! ... forward & adjoint N-S runs for initial cost
                        OPTIM_dlinmin = 2, & ! ... line minimization
                        OPTIM_mnbrak = 3, & ! ... mnbrak
                        OPTIM_dbrent = 4, & ! ... dbrent
                        OPTIM_optimum = 5, & ! ... run with optimal control variable
                        OPTIM_frprmn = 6
  
  INTEGER, PARAMETER :: DIR_ROOT = 1, &
                        DIR_FWD  = 2, &
                        DIR_ADJ  = 3
  
  CHARACTER(LEN=80), DIMENSION(6), PARAMETER :: runningMode = (/'IC     ', 'dlinmin', 'mnbrak ', 'dbrent ', 'optimum', 'frprmn '/)

  ! ... module variables regarding saving intermediate data
  CHARACTER(LEN=30), PARAMETER :: fname_stdout_fwd        = 'stdout_fwd', &
                                  fname_stdout_adj        = 'stdout_adj', &
                                  fname_stdout_iterCG     = 'stdout_iterCG', &
                                  fname_stdout_J_vs_alpha = 'stdout_J_vs_alpha.iter', &
                                  fname_stdout_J_vs_time  = 'stdout_J_vs_time', &
                                  fname_stdout_optHistory = 'stdout_optHistory', &
                                  fname_stdout_gradient   = 'stdout_gradient.ntime', &
                                  fname_stdout_CGvectors  = 'stdout_CGvectors.iter', &
                                  fname_stdin_actPtID     = 'stdin_actPtID'
  
  INTEGER, PARAMETER :: ndev_stdout            = 36, & ! ... device number for standard output
                        ndev_stdout_iterCG     = 41, & ! ... device number for CG iteration
                        ndev_stdout_J_vs_alpha = 47, & ! ... device number for J-versus-alpha diagram
                        ndev_stdout_J_vs_time  = 48, & ! ... device number for J-versus-time plot
                        ndev_stdout_gradient   = 49, & ! ... device number for gradient
                        ndev_stdout_CGvectors  = 55, & ! ... device number for CG vectors
                        ndev_stdout_optHistory = 57    ! ... device number for optimization history

  CHARACTER(LEN=PATH_LENGTH) :: restart_fname_cv ! ... name of the last uncontrolled solution file 

CONTAINS

  subroutine optimizeCG(region, fname)

    USE ModGlobal
    USE ModDataStruct
    USE ModIO
    USE ModMPI
    USE ModDerivBuildOps
    USE ModDeriv
    USE ModMetrics
    USE ModInterp

    Implicit None

    ! ... subroutine arguments
    Type(t_region), Pointer :: region
    Character(LEN=PATH_LENGTH) :: fname

    ! ... local variables and arrays
    Type(t_grid), Pointer :: grid
    Type(t_mixt_input), Pointer :: input
    Integer :: ng, i, p, ierr
    Real(rfreal) :: timer
    integer, allocatable :: err(:)

    ! ... simplicity
    input => region%input
    region%global%filtercount(:) = 1
    allocate(err(region%nGrids)); err(:) = 0
    ! Global variable 'restart_fname_cv' is now final, must not be modified
    restart_fname_cv = fname ! ... file having the last uncontrolled solution

    ! ... set up the interpolation data once for static grids
    if (input%gridType == CHIMERA) call Setup_Interpolation(region)

    ! ... alternative OPERATOR_SETUP which does not require all processors to have global_iblank at the same time
    If (input%use_lowmem_operator_setup == TRUE) Then
      If (region%myrank == 0) Write (*,'(A)') 'PlasComCM: Building operators using low-memory option.  This may take a while...'
      Do i = 1, region%nGridsGlobal
        Do ng = 1, region%nGrids
          grid => region%grid(ng)
          If (grid%iGridGlobal == i) Then
            Do p = 0, grid%numproc_inComm-1
              Call Request_Global_Mask(region, ng, p)
              If (grid%myrank_inComm == p) Call Operator_Setup(region, grid%input, grid, err(ng), ng)
              Call MPI_Barrier(grid%comm, ierr)
            End Do
          End if
          Call MPI_Barrier(mycomm, ierr)
        End Do
      End Do
      Do ng = 1, region%nGrids
        grid => region%grid(ng)
        Call HYPRE_Pade_Operator_Setup(region, ng)
      End Do
    Else
      ! ... compute the derivative matrices in the computational
      ! ... coordinates.  These are independent of the mesh coordinates, but depend on IBLANK
      Call create_global_mask(region)
      do ng = 1, region%nGrids
        grid => region%grid(ng)
        Call Operator_Setup(region, grid%input, grid, err(ng), ng)
        Call HYPRE_Pade_Operator_Setup(region, ng)
      end do
    End If
    if (sum(err(:)) /= 0) Call Output_Rank_As_IBLANK(region)

    call mpi_barrier(mycomm, ierr)

    ! ... if the grid is not moving, compute metrics OUTSIDE loop
    do ng = 1, region%nGrids
      timer = MPI_Wtime()
      call metrics(region, ng)
      region%mpi_timings(ng)%metrics = region%mpi_timings(ng)%metrics + (MPI_WTIME() - timer)
    end do
    timer = MPI_Wtime()
    !call write_plot3d_file(region, 'met')
    if (input%useMetricIdentities == TRUE) call WriteData(region, 'mid')
    region%mpi_timings(:)%io_write_soln = region%mpi_timings(:)%io_write_soln + (MPI_WTIME() - timer)

    ! ... output diagnostic file if asked
    if (input%outputBC_asIBLANK == TRUE) call output_bc_as_iblank(region)

    call mpi_barrier(mycomm, ierr)

    if (.NOT. input%AdjOptim_Restart) then
      if (region%myrank == 0) then
        write(*,*)
        write(*,*) "PlasComCM: ==> Adjoint-based control optimization <=="
        write(*,*)
      end if ! myrank
   else
      if (region%myrank == 0) then
        write(*,*)
        write(*,*) "PlasComCM: ==> Adjoint-based control optimization RESTART <=="
        write(*,*)
      end if ! myrank      
   end if ! .NOT.
   
    ! ... initialize optimization parameters and variables
    call InitializeOptimization(region)
    call mpi_barrier(mycomm, ierr)
    if (region%myrank == 0) write(*,*) 'PlasComCM: Optimization Initialized.'

    ! ... main loop of conjugate gradient optimization
    ! ... named after Fletcher-Reeves-Polak-Ribiere (FRPR) minimization algorithm
    ! ... see Numerical Recipes for the original implementation
    call frprmn(region)

    call mpi_barrier(mycomm, ierr)
    if (region%myrank == 0) then
      write(*,*)
      write(*,*) "PlasComCM: ==> Adjoint-based control optimization finished <=="
      write(*,*)
    end if ! myrank

  end subroutine optimizeCG

  subroutine DefineControlHorizon(region)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    ! ... subroutine arguments
    Type(t_region), Pointer :: region

    ! ... local variables and arrays
    Type(t_mixt_input), Pointer :: input

    ! ... simplicity
    input => region%input

    ! ... define control time horizon in terms of iteration and time
    region%ctrlStartIter = input%nstepi
    region%ctrlEndIter   = input%nstepi + input%nstepmax

    region%ctrlStartTime = region%state(1)%time(1)
    region%ctrlEndTime   = region%state(1)%time(1) + REAL(input%nstepmax, rfreal)*input%dt

  end subroutine DefineControlHorizon

   subroutine InitializeOptimization(region)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    USE ModIO
    USE ModActuator

    Implicit None

    ! ... subroutine arguments
    Type(t_region), Pointer :: region

    ! ... local variables and arrays
    Type(t_mixt_input), Pointer :: input
    Type(t_sub_actuator), Pointer :: act
    Type(t_sub_ctrl_target), Pointer :: targ
    Integer :: na, nt, ng, ierr
    Integer :: io_err, idum1, idum2, idum3, idum4
    Character(Len=80) :: cdum

    ! ... simplicity
    input => region%input

    allocate(region%optimization)

    ! ... names and device numbers of standard output files
    !fname_stdout_fwd    = 'stdout_fwd'
    !fname_stdout_adj    = 'stdout_adj'
    !fname_stdout_iterCG = 'stdout_iterCG'
    !fname_stdout_J_vs_alpha = 'stdout_J_vs_alpha.iter'
    !fname_stdout_J_vs_time  = 'stdout_J_vs_time'
    !fname_stdout_optHistory = 'stdout_optHistory'
    !fname_stdout_gradient   = 'stdout_gradient.ntime'
    !fname_stdout_CGvectors  = 'stdout_CGvectors.iter'
    !fname_stdin_actPtID = 'stdin_actPtID'
    !ndev_stdout            = 36
    !ndev_stdout_iterCG     = 41
    !ndev_stdout_J_vs_alpha = 47
    !ndev_stdout_J_vs_time  = 48
    !ndev_stdout_optHistory = 49
    !ndev_stdout_gradient   = 55
    !ndev_stdout_CGvectors  = 57

    ! ... open standard output files recording every detail of optimization
    ! ... for restarting optimization, POSITION='APPEND'
    if (region%myrank == 0) then
      open(ndev_stdout_iterCG, file=TRIM(fname_stdout_iterCG), POSITION='APPEND')
      open(ndev_stdout_optHistory, file=TRIM(fname_stdout_optHistory), POSITION='APPEND')

      if (.NOT. input%AdjOptim_Restart) then
        write(ndev_stdout_iterCG,*) 'variables = iteration, J(<greek>f</greek>), <greek>a</greek>'
      end if ! input%AdjOptim_Restart
    end if ! myrank

    ! ... whether or not our optimization solves forward & adjoint N-S equations
    ! ... input%AdjOptim_typeOfOptim = 1, 2 are with known functional
    region%optimization%solveNS = TRUE
    if (input%AdjOptim_typeOfOptim <= 2) region%optimization%solveNS = FALSE

    ! ... initialize (again) flow-control variables for optimization
    region%GLOBAL_ITER = 0
    region%optimization%curRunningMode = 0
    region%optimization%subIter = 0
    region%global%main_ts_loop_index = region%ctrlStartIter
    region%optimization%whichNS = 'FWD'
    ! ... variables storing restarting status
    region%optimization%restart_GLOBAL_ITER = 0
    region%optimization%restart_curRunningMode = 0
    region%optimization%restart_subIter = 0
    region%optimization%restart_main_ts_loop_index = region%ctrlStartIter
    region%optimization%restart_whichNS = 'FWD'

    ! ... jobs regarding restart
    if (input%AdjOptim_Restart) then
      ! ... all processors open up the history file and initialize variables for restarting
      if (region%myrank /= 0) open(ndev_stdout_optHistory, file=TRIM(fname_stdout_optHistory), POSITION='APPEND')

      rewind(ndev_stdout_optHistory)
      io_err = FALSE
      do while(io_err == FALSE)
        read(ndev_stdout_optHistory,'(3I7,I20,A)',iostat=io_err) idum1, idum2, idum3, idum4, cdum
      end do ! io_err

      ! ... flow-control variables from the last run
      region%optimization%restart_GLOBAL_ITER = idum1
      region%optimization%restart_curRunningMode = idum2
      region%optimization%restart_subIter = idum3
      region%optimization%restart_main_ts_loop_index = idum4
      region%optimization%restart_whichNS = TRIM(ADJUSTL(cdum))

      if (region%myrank /= 0) close(ndev_stdout_optHistory)
    end if ! input%AdjOptim_Restart
    call mpi_barrier(mycomm, ierr)

    region%optimization%getMean = FALSE ! ... by default, our functional is based on total quantity
    if (input%AdjOptim_Func == FUNCTIONAL_SOUND) then
      region%optimization%getMean = TRUE ! ... retrieve mean pressure at control target region
    else
      call graceful_exit(region%myrank, "... ERROR: unknown cost functional for optimal control")
    end if ! input%AdjOptim_Func
    call mpi_barrier(mycomm, ierr)

    ! ... without assuming types of actuators, initialize their locations and 
    ! ... let the code know which processors and grids take them or part of them
    call PlaceActuator4Optimization(region, fname_stdin_actPtID)

    ! ... initialize the locations of control target region and 
    ! ... let the code know which processors and grids take them or part of them
    call PlaceCtrlTarget(region)

    ! ... initialize interpolation information of forward N-S solutions
    call Initialize_AdjInterpolate_cv(region, .FALSE.)

    ! ... initialize conjugate gradient variables and vectors
    if (region%nSubActuators > 0) then ! ... if there is any actuator in this region
      do na = 1, region%nSubActuators
        act => region%subActuator(na)

        nullify(act%gPhi); allocate(act%gPhi(region%numFwdSolnFiles,act%numPts)); act%gPhi = 0.0_rfreal
        nullify(act%phi); allocate(act%phi(act%numPts)); act%phi = 0.0_rfreal
        nullify(act%gPhiCtrl); allocate(act%gPhiCtrl(region%numFwdSolnFiles,act%numPts)); act%gPhiCtrl = 0.0_rfreal
        nullify(act%gradient); allocate(act%gradient(region%numFwdSolnFiles,act%numPts)); act%gradient = 0.0_rfreal
        nullify(act%xi); allocate(act%xi(region%numFwdSolnFiles,act%numPts)); act%xi = 0.0_rfreal
        nullify(act%g); allocate(act%g(region%numFwdSolnFiles,act%numPts)); act%g = 0.0_rfreal
        nullify(act%h); allocate(act%h(region%numFwdSolnFiles,act%numPts)); act%h = 0.0_rfreal

        act%gAlpha = input%AdjOptim_initAlpha
        act%gAlpha_old = act%gAlpha
      end do ! na
    end if ! region%nSubActuators
    call mpi_barrier(mycomm, ierr)

    if (region%nSubCtrlTarget > 0) then ! ... if there is any control target in this region
      do nt = 1, region%nSubCtrlTarget
        targ => region%subCtrlTarget(nt)

        targ%gCost     = 0.0_rfreal ! ... cost of the global system (same for every sub-target object)
        targ%gCost_old = 0.0_rfreal ! ... cost at the previous conjugate gradient iteration
        targ%gCost_initial = 0.0_rfreal ! ... cost of the very 1st iteration

        nullify(targ%meanQ); allocate(targ%meanQ(targ%numPts)); targ%meanQ = 0.0_rfreal
      end do ! nt
    end if ! region%nSubCtrlTarget
    call mpi_barrier(mycomm, ierr)

  end subroutine InitializeOptimization

  subroutine PlaceCtrlTarget(region)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    ! ... subroutine arguments
    Type(t_region), Pointer :: region

    ! ... local variables and arrays
    Type(t_grid), Pointer :: grid
    Type(t_mixt_input), Pointer :: input
    Type(t_sub_ctrl_target), Pointer :: targ
    Integer :: ng, l0, i, ierr
    Integer :: numPts
    Integer, Pointer :: index(:)
    Real(rfreal) :: xMin, xMax, yMin, yMax, zMin, zMax
    Real(rfreal) :: x0, dx, fac_x, y0, dy, fac_y, z0, dz, fac_z, xProd, yProd, zProd
    Real(rfreal) :: facL_x, facR_x, rMin, rMax, r0, dr, rLoc, rProd, fac_r

    ! ... simplicity
    input => region%input

    ! ... given information on location of control target by user, 
    ! ... keep the local indices of control target points in each grid

    SELECT CASE( input%AdjOptim_typeOfOptim )
    CASE( 1:2 ) ! ... target region is a straight line from x = -1 to x = 1 at y = -1
                ! ... invented for model controller test
      ! ... geometry of target region (1-D straight line)
      xMin =-1.0_rfreal; xMax = 1.0_rfreal
      yMin =-1.0_rfreal; yMax =-1.0_rfreal

      ! ... first, check how many grids in this processor divide this target in space
      ngLoop1: do ng = 1, region%nGrids
        grid => region%grid(ng)

        do l0 = 1, grid%nCells
          if (grid%xyz(l0,2) >= yMin .AND. grid%xyz(l0,2) <= yMax) then
            region%nSubCtrlTarget = region%nSubCtrlTarget + 1
            CYCLE ngLoop1 ! ... finding a single point is okay because we have a single target zone
          end if ! grid%xyz(l0,2)
        end do ! l0
      end do ngLoop1

      if (region%nSubCtrlTarget > 0) then
        nullify(region%subCtrlTarget); allocate(region%subCtrlTarget(region%nSubCtrlTarget))

        region%nSubCtrlTarget = 0
        ! ... count them again and complete to fill out information
        do ng = 1, region%nGrids
          grid => region%grid(ng)
          nullify(index); allocate(index(grid%nCells)); index = 0

          numPts = 0 ! ... now we gotta count all target points
          do l0 = 1, grid%nCells
            if (grid%xyz(l0,2) >= yMin .AND. grid%xyz(l0,2) <= yMax) then
              numPts = numPts + 1
              index(numPts) = l0
            end if ! grid%xyz(l0,2)
          end do ! l0

          if (numPts > 0) then ! ... fill up the target information
            region%nSubCtrlTarget = region%nSubCtrlTarget + 1

            nullify(targ)
            targ => region%subCtrlTarget(region%nSubCtrlTarget)

            targ%subTargID = region%nSubCtrlTarget
            targ%targID = 1 ! ... we have a single model target globally
            targ%gridID = ng
            targ%gridID_global = grid%iGridGlobal
            targ%numPts = numPts
            nullify(targ%index); allocate(targ%index(numPts)); targ%index = 0
            do i = 1,numPts
              targ%index(i) = index(i)
            end do ! i
            nullify(targ%distFunc); allocate(targ%distFunc(numPts)); targ%distFunc = 1.0_rfreal
          end if ! numPts

          deallocate(index)
        end do ! ng
      end if ! region%nSubCtrlTarget

    CASE( 3 ) ! ... target region is a straight line from y = -8 to y = 8 at x = 0
              ! ... invented for anti-sound cancellation (Mingjun's Ph.D thesis)
!!$   ! ... geometry of target region (1-D vertical straight line)
!!$   xMin = 0.0_rfreal; xMax = 0.0_rfreal
!!$   yMin =-10.0_rfreal; yMax = 10.0_rfreal

!!$   ! ... first, check how many grids in this processor divide this target in space
!!$   ngLoop2: do ng = 1, region%nGrids
!!$     grid => region%grid(ng)
!!$
!!$     do l0 = 1, grid%nCells
!!$       if ((grid%xyz(l0,1) >= xMin .AND. grid%xyz(l0,1) <= xMax) .AND. &
!!$           (grid%xyz(l0,2) >= yMin .AND. grid%xyz(l0,2) <= yMax)) then
!!$         region%nSubCtrlTarget = region%nSubCtrlTarget + 1
!!$         CYCLE ngLoop2 ! ... finding a single point is okay because we have a single target zone
!!$       end if ! grid%xyz(l0,1)
!!$     end do ! l0
!!$   end do ngLoop2

      region%nSubCtrlTarget = region%nGrids ! ... because every point is a target

      if (region%nSubCtrlTarget > 0) then
        nullify(region%subCtrlTarget); allocate(region%subCtrlTarget(region%nSubCtrlTarget))

        region%nSubCtrlTarget = 0
        ! ... count them again and complete to fill out information
        do ng = 1, region%nGrids
          grid => region%grid(ng)
          nullify(index); allocate(index(grid%nCells)); index = 0

          numPts = 0 ! ... now we gotta count all target points
          do l0 = 1, grid%nCells
!!$       if ((grid%xyz(l0,1) >= xMin .AND. grid%xyz(l0,1) <= xMax) .AND. &
!!$           (grid%xyz(l0,2) >= yMin .AND. grid%xyz(l0,2) <= yMax)) then
              numPts = numPts + 1
              index(numPts) = l0
!!$       end if ! grid%xyz(l0,2)
          end do ! l0

          if (numPts > 0) then ! ... fill up the target information
            region%nSubCtrlTarget = region%nSubCtrlTarget + 1

            nullify(targ)
            targ => region%subCtrlTarget(region%nSubCtrlTarget)

            targ%subTargID = region%nSubCtrlTarget
            targ%targID = 1 ! ... we have a single target globally
            targ%gridID = ng
            targ%gridID_global = grid%iGridGlobal
            targ%numPts = numPts
            nullify(targ%index); allocate(targ%index(numPts)); targ%index = 0
            do i = 1,numPts
              targ%index(i) = index(i)
            end do ! i
            nullify(targ%distFunc); allocate(targ%distFunc(numPts)); targ%distFunc = 1.0_rfreal
            ! ... approximated delta function
            do i = 1,numPts
              l0 = targ%index(i)

              ! ... x-wise distribution
              x0 = 0.0_rfreal
              dx = 20.0_rfreal / REAL(800,rfreal)
              fac_x = EXP(-(grid%xyz(l0,1)-x0)**2 / (2.0_rfreal*dx)**2)

              ! ... y-wise distribution
              y0 = 8.0_rfreal
              fac_y = 1.0_rfreal
              if (ABS(grid%xyz(l0,2)) > y0) then
                dy = 20.0_rfreal / REAL(200,rfreal)
                fac_y = EXP(-(ABS(grid%xyz(l0,2))-y0)**2 / (2.0_rfreal*dy)**2)
              end if ! ABS(grid%xyz(l0,2))

              targ%distFunc(i) = fac_x * fac_y
            end do ! i
          end if ! numPts

          deallocate(index)
        end do ! ng
      end if ! region%nSubCtrlTarget

    CASE( 4 ) ! ... anti-sound cancellation across 2-D mixing layer

      ! ... first, find out if any grid in this region supports target zone
      xMin = 900.0_rfreal
      xMax = 1100.0_rfreal
      x0 = 0.5_rfreal * (xMin + xMax)
      dx = 2000.0_rfreal / REAL(170,rfreal) ! ... uniform 170 points in [0, 2000]
      yMin = 1500.0_rfreal
      yMax = 2000.0_rfreal
      y0 = 0.5_rfreal * (yMin + yMax)
      dy = (yMax - yMin) / REAL(10-1,rfreal) ! ... 10 points in [1500, 2000]

      ngLoop2: do ng = 1, region%nGrids
        grid => region%grid(ng)

        do l0 = 1,grid%nCells
          yProd = (grid%xyz(l0,2) - yMin) * (yMax - grid%xyz(l0,2))

          if (yProd >= 0.0_rfreal) then
            region%nSubCtrlTarget = region%nSubCtrlTarget + 1
            CYCLE ngLoop2 ! ... finding a single point is okay because we have a single target zone
          end if ! yProd
        end do ! l0
      end do ngLoop2

      if (region%nSubCtrlTarget > 0) then
        nullify(region%subCtrlTarget); allocate(region%subCtrlTarget(region%nSubCtrlTarget))

        region%nSubCtrlTarget = 0
        ! ... count them again and complete to fill out information
        do ng = 1, region%nGrids
          grid => region%grid(ng)
          nullify(index); allocate(index(grid%nCells)); index = 0

          numPts = 0 ! ... now we gotta count all target points
          do l0 = 1, grid%nCells
            yProd = (grid%xyz(l0,2) - yMin) * (yMax - grid%xyz(l0,2))

            if (yProd >= 0.0_rfreal) then
              numPts = numPts + 1
              index(numPts) = l0
            end if ! yProd
          end do ! l0

          if (numPts > 0) then ! ... fill up the target information
            region%nSubCtrlTarget = region%nSubCtrlTarget + 1

            nullify(targ)
            targ => region%subCtrlTarget(region%nSubCtrlTarget)

            targ%subTargID = region%nSubCtrlTarget
            targ%targID = 1 ! ... we have a single target globally
            targ%gridID = ng
            targ%gridID_global = grid%iGridGlobal
            targ%numPts = numPts
            nullify(targ%index); allocate(targ%index(numPts)); targ%index = 0
            do i = 1,numPts
              targ%index(i) = index(i)
            end do ! i
            nullify(targ%distFunc); allocate(targ%distFunc(numPts)); targ%distFunc = 1.0_rfreal
            ! ... approximated delta function
            do i = 1,numPts
              l0 = targ%index(i)

              fac_x = EXP(-(grid%xyz(l0,1)-x0)**2 / (2.0_rfreal*dx)**2)
              fac_y = EXP(-(grid%xyz(l0,2)-y0)**2 / (2.0_rfreal*dy)**2)
              targ%distFunc(i) = fac_x * fac_y
            end do ! i
          end if ! numPts

          deallocate(index)
        end do ! ng
      end if ! region%nSubCtrlTarget

    CASE( 5 ) ! ... anti-sound cancellation of 3-D M=1.3 cold jet

      ! ... first, find out if any grid in this region supports target zone
      xMin = 14.5_rfreal
      xMax = 17.5_rfreal
      x0 = 0.5_rfreal * (xMin + xMax)
      dx = (xMax - xMin) / REAL(28-1,rfreal) ! ... 28 points in [14.5, 17.5] in x
      yMin = 5.5_rfreal
      yMax = 8.5_rfreal
      y0 = 0.5_rfreal * (yMin + yMax)
      dy = (yMax - yMin) / REAL(12-1,rfreal) ! ... 12 points in [5.5, 8.5] in y
      zMin = -3.0_rfreal
      zMax = 3.0_rfreal
      z0 = 0.5_rfreal * (zMin + zMax)
      dz = (zMax - zMin) / REAL(5-1,rfreal) ! ... 5 points in [-3.0, 3.0] in z

      ngLoop3: do ng = 1, region%nGrids
        grid => region%grid(ng)

        do l0 = 1,grid%nCells
          xProd = (grid%xyz(l0,1) - xMin) * (xMax - grid%xyz(l0,1))
          yProd = (grid%xyz(l0,2) - yMin) * (yMax - grid%xyz(l0,2))
          zProd = (grid%xyz(l0,3) - zMin) * (zMax - grid%xyz(l0,3))

          if (xProd >= 0.0_rfreal .AND. yProd >= 0.0_rfreal .AND. zProd >= 0.0_rfreal) then
            region%nSubCtrlTarget = region%nSubCtrlTarget + 1
            CYCLE ngLoop3 ! ... finding a single point is okay because we have a single target zone
          end if ! xProd
        end do ! l0
      end do ngLoop3

      if (region%nSubCtrlTarget > 0) then
        nullify(region%subCtrlTarget); allocate(region%subCtrlTarget(region%nSubCtrlTarget))

        region%nSubCtrlTarget = 0
        ! ... count them again and complete to fill out information
        do ng = 1, region%nGrids
          grid => region%grid(ng)
          nullify(index); allocate(index(grid%nCells)); index = 0

          numPts = 0 ! ... now we gotta count all target points
          do l0 = 1, grid%nCells
            xProd = (grid%xyz(l0,1) - xMin) * (xMax - grid%xyz(l0,1))
            yProd = (grid%xyz(l0,2) - yMin) * (yMax - grid%xyz(l0,2))
            zProd = (grid%xyz(l0,3) - zMin) * (zMax - grid%xyz(l0,3))

            if (xProd >= 0.0_rfreal .AND. yProd >= 0.0_rfreal .AND. zProd >= 0.0_rfreal) then
              numPts = numPts + 1
              index(numPts) = l0
            end if ! xProd
          end do ! l0

          if (numPts > 0) then ! ... fill up the target information
            region%nSubCtrlTarget = region%nSubCtrlTarget + 1

            nullify(targ)
            targ => region%subCtrlTarget(region%nSubCtrlTarget)

            targ%subTargID = region%nSubCtrlTarget
            targ%targID = 1 ! ... we have a single target globally
            targ%gridID = ng
            targ%gridID_global = grid%iGridGlobal
            targ%numPts = numPts
            nullify(targ%index); allocate(targ%index(numPts)); targ%index = 0
            do i = 1,numPts
              targ%index(i) = index(i)
            end do ! i
            nullify(targ%distFunc); allocate(targ%distFunc(numPts)); targ%distFunc = 1.0_rfreal
            ! ... approximated delta function
            do i = 1,numPts
              l0 = targ%index(i)

              fac_x = EXP(-(grid%xyz(l0,1)-x0)**2 / (2.0_rfreal*dx)**2)
              fac_y = EXP(-(grid%xyz(l0,2)-y0)**2 / (1.0_rfreal*dy)**2)
              fac_z = EXP(-(grid%xyz(l0,3)-z0)**2 / (1.0_rfreal*dz)**2)
              targ%distFunc(i) = fac_x * fac_y * fac_z
            end do ! i
          end if ! numPts

          deallocate(index)
        end do ! ng
      end if ! region%nSubCtrlTarget

    CASE( 6 ) ! ... flow control of 3-D M=1.3 cold jet without a nozzle

      ! ... first, find out if any grid in this region supports target zone
      xMin = 1.0_rfreal
      xMax = 24.0_rfreal
      x0 = 0.5_rfreal * (xMin + xMax)
      facL_x = 30.0_rfreal ! ... determines how steep the hyperbolic tangent is
      facR_x = 1.0_rfreal ! ... determines how steep the hyperbolic tangent is

      rMin = 6.0_rfreal
      rMax = 10.0_rfreal
      r0 = 0.5_rfreal * (rMin + rMax)
      dr = (rMax - rMin) / REAL(18-1,rfreal) ! ... 18 points in [6.0, 10.0] in r

      ngLoop4: do ng = 1, region%nGrids
        grid => region%grid(ng)

        do l0 = 1,grid%nCells
          xProd = (grid%xyz(l0,1) - xMin) * (xMax - grid%xyz(l0,1))
          rLoc = SQRT((grid%xyz(l0,2))**2 + (grid%xyz(l0,3))**2)
          rProd = (rLoc - rMin) * (rMax - rLoc)

          if (xProd >= 0.0_rfreal .AND. rProd >= 0.0_rfreal) then
            region%nSubCtrlTarget = region%nSubCtrlTarget + 1
            CYCLE ngLoop4 ! ... finding a single point is okay because we have a single target zone
          end if ! xProd
        end do ! l0
      end do ngLoop4

      if (region%nSubCtrlTarget > 0) then
        nullify(region%subCtrlTarget); allocate(region%subCtrlTarget(region%nSubCtrlTarget))

        region%nSubCtrlTarget = 0
        ! ... count them again and complete to fill out information
        do ng = 1, region%nGrids
          grid => region%grid(ng)
          nullify(index); allocate(index(grid%nCells)); index = 0

          numPts = 0 ! ... now we gotta count all target points
          do l0 = 1, grid%nCells
            xProd = (grid%xyz(l0,1) - xMin) * (xMax - grid%xyz(l0,1))
            rLoc = SQRT((grid%xyz(l0,2))**2 + (grid%xyz(l0,3))**2)
            rProd = (rLoc - rMin) * (rMax - rLoc)

            if (xProd >= 0.0_rfreal .AND. rProd >= 0.0_rfreal) then
              numPts = numPts + 1
              index(numPts) = l0
            end if ! xProd
          end do ! l0

          if (numPts > 0) then ! ... fill up the target information
            region%nSubCtrlTarget = region%nSubCtrlTarget + 1

            nullify(targ)
            targ => region%subCtrlTarget(region%nSubCtrlTarget)

            targ%subTargID = region%nSubCtrlTarget
            targ%targID = 1 ! ... we have a single target globally
            targ%gridID = ng
            targ%gridID_global = grid%iGridGlobal
            targ%numPts = numPts
            nullify(targ%index); allocate(targ%index(numPts)); targ%index = 0
            do i = 1,numPts
              targ%index(i) = index(i)
            end do ! i
            nullify(targ%distFunc); allocate(targ%distFunc(numPts)); targ%distFunc = 1.0_rfreal
            ! ... approximated delta function
            do i = 1,numPts
              l0 = targ%index(i)

              fac_x = TANH(facL_x * (grid%xyz(l0,1) - xMin)) + TANH(facR_x * (xMax - grid%xyz(l0,1))) - 1.0_rfreal
              rLoc = SQRT((grid%xyz(l0,2))**2 + (grid%xyz(l0,3))**2)
              fac_r = EXP(-(rLoc-r0)**2 / (2.0_rfreal*dr)**2)
              targ%distFunc(i) = fac_x * fac_r
            end do ! i
          end if ! numPts

          deallocate(index)
        end do ! ng
      end if ! region%nSubCtrlTarget

    CASE DEFAULT
      call graceful_exit(region%myrank, "... ERROR: placing target failed: unknown type of test optimization")

    END SELECT ! input%AdjOptim_typeOfOptim

    call mpi_barrier(mycomm, ierr)

  end subroutine PlaceCtrlTarget

  subroutine frprmn(region)

    USE ModGlobal
    USE ModDataStruct
    USE ModIO
    USE ModMPI

    Implicit None

    ! ... subroutine arguments
    Type(t_region), Pointer :: region

    ! ... local variables and arrays
    Type(t_grid), Pointer :: grid
    Type(t_mixt), Pointer :: state
    Type(t_mixt_input), Pointer :: input
    Type(t_sub_ctrl_target), Pointer :: targ
    Type(t_sub_actuator), Pointer :: act
!
!	 BUGFIX: This is a function not a variable (gzagaris, 7-22-2010)
!    Logical :: GoOnIteration
!
    Integer :: ng, nt, na, i, j, l0, idum1, idum2, io_err, ierr
    Real(rfreal) :: fret ! ... local minimum cost functional from the current line minimization
    Real(rfreal) :: fp ! ... local minimum cost functional at the previous iteration
    Real(rfreal) :: fi ! ... initial local minimum cost functional
    Real(rfreal) :: gAlpha ! ... generalized distance in control variable phi-space
                           ! ... passed to forward solver to update working 
                           ! ... control input gPhi from previously optimized 
                           ! ... input gPhiCtrl
    Real(rfreal) :: gAlpha_old ! ... last iteration's gAlpha
    Real(rfreal) :: dummy, rdum1, rdum2
    Real(rfreal) :: ggP, dggP, gg, dgg, gam
    Character(Len=80) :: cdum
    Character(30) :: citer
    Character(LEN=200) :: dirname

    ! ... simplicity
    input => region%input

    ! ... Step 1: compute initial conditions for conjugate gradient iterations
    ! ...         e.g. forward N-S solver gives cost functional
    ! ...              adjoint N-S solver gives gradient to control input
    region%GLOBAL_ITER = 0

    region%optimization%curRunningMode = OPTIM_IC; region%optimization%subIter = 0
    if (region%optimization%solveNS == TRUE) dirname = CreateDirectory(region, DIR_ROOT)
    fret = CostFunctional(region) ! ... run to get initial functional; no alpha 
                                  ! ... because it's a baseline case and therefore 
                                  ! ... there is no control update
    dummy = ControlGradient(region)
    if (input%AdjOptim_Restart) then
      ! ... CASE 1: the last run was stopped at later than IC calculation
      ! ...         at least read the initial cost and alpha
      if (region%myrank /= 0) then
        open(ndev_stdout_iterCG, file=TRIM(fname_stdout_iterCG), STATUS='OLD')
      end if ! myrank
      rewind(ndev_stdout_iterCG)

      read(ndev_stdout_iterCG,'(A)') cdum
      read(ndev_stdout_iterCG,*) idum1, rdum2, rdum1
      gAlpha = rdum1; fret = rdum2

      if (region%myrank /= 0) then
        close(ndev_stdout_iterCG)
      end if ! myrank

    else
      ! ... CASE 2: no restart; just save the calculated cost and initial alpha
      gAlpha = input%AdjOptim_initAlpha
      if (region%myrank == 0) then
        write(ndev_stdout_iterCG,'(I6,2F36.25)') region%GLOBAL_ITER, fret, gAlpha
      end if ! myrank
    end if ! input%AdjOptim_Restart
    fp = fret
    call mpi_barrier(mycomm, ierr)

    ! ... initialize conjugate gradient vectors with information obtained above
    if (.NOT. input%AdjOptim_Restart) then
      if (region%nSubActuators > 0) then
        do na = 1,region%nSubActuators
          act => region%subActuator(na)

          do j = 1,SIZE(act%g,2)
            do i = 1,SIZE(act%g,1)
              act%g(i,j)  =-act%gradient(i,j) ! ... g-vector of CG; g_0
              act%h(i,j)  = act%g(i,j) ! ... h-vector of CG; h_0 = g_0
              act%xi(i,j) = act%h(i,j)

              act%gPhi(i,j) = 0.0_rfreal ! ... defensive programming
              act%gPhiCtrl(i,j) = 0.0_rfreal ! ... defensive programming
            end do ! i
          end do ! j
        end do ! na
      end if ! region%nSubActuators
      call WriteCGVectors(region, region%GLOBAL_ITER) ! ... write initial CG vectors

    end if ! input%AdjOptim_Restart
    call mpi_barrier(mycomm, ierr)

    ! ... save initial cost functional
    if (region%nSubCtrlTarget > 0) then
        do nt = 1,region%nSubCtrlTarget
          targ => region%subCtrlTarget(nt)

          targ%gCost_initial = fret
        end do ! nt
    end if ! region%nSubCtrlTarget
    fi = fret
    call mpi_barrier(mycomm, ierr)



    ! ... Step 2: initial convergence check
    if (.NOT. GoOnIteration(region,fret,fp)) & ! ... no way!
      call graceful_exit(region%myrank, "... ERROR: GLOBAL_ITER or ITMAX incorrectly initialized, or already optimal control")
    call mpi_barrier(mycomm, ierr)



    ! ... Step 3: iterate until told not to
    mainCGLoop: do while (GoOnIteration(region,fret,fp))

      ! ... increase CG iteration counter
      region%GLOBAL_ITER = region%GLOBAL_ITER + 1
      write(citer,'(I3.3)') region%GLOBAL_ITER

      ! ... create root directory for this iteration
      region%optimization%curRunningMode = OPTIM_frprmn; region%optimization%subIter = 0
      if (region%optimization%solveNS == TRUE) dirname = CreateDirectory(region, DIR_ROOT)

      ! ... restart makes life complicated... :(
      if (input%AdjOptim_Restart) then
        if (region%GLOBAL_ITER < region%optimization%restart_GLOBAL_ITER) then
          ! ... CASE 1: we're behind the last CG iteration
          ! ...         keep only the optimized alpha and cost, and jump to the next iteration
          ! ...         every processor should read the file
          open(ndev_stdout_J_vs_alpha, file=TRIM(fname_stdout_J_vs_alpha)//TRIM(citer), STATUS='OLD')
          rewind(ndev_stdout_J_vs_alpha)
          read(ndev_stdout_J_vs_alpha,'(A)') cdum
          idum1 = OPTIM_frprmn
          do while(idum1 /= OPTIM_optimum)
            read(ndev_stdout_J_vs_alpha,*) rdum1, rdum2, idum1, idum2
          end do ! idum1
          gAlpha = rdum1; fret = rdum2
          close(ndev_stdout_J_vs_alpha)

          ! ... no need to read CG vectors since we're going to the next iteration

          CYCLE mainCGLoop

        else 
          ! ... CASE 2: we're exactly at the same CG iteration as the last one
          ! ...         open the file and read the last iteration's set of variables and proceed
          open(ndev_stdout_J_vs_alpha, file=TRIM(fname_stdout_J_vs_alpha)//TRIM(citer), STATUS='OLD')
          rewind(ndev_stdout_J_vs_alpha)
          read(ndev_stdout_J_vs_alpha,'(A)') cdum
          read(ndev_stdout_J_vs_alpha,*) rdum1, rdum2, idum1, idum2 ! ... read only the last optimized alpha & cost
          gAlpha = rdum1; fret = rdum2
          close(ndev_stdout_J_vs_alpha)

          ! ... read the last iteration's CG vectors
          call ReadCGVectors(region, region%optimization%restart_GLOBAL_ITER-1)

        end if ! region%GLOBAL_ITER

      else
        ! ... CASE 3: no restarting; continue first writing the last iteration's results
        if (region%myrank == 0) then
          open(ndev_stdout_J_vs_alpha, file=TRIM(fname_stdout_J_vs_alpha)//TRIM(citer))
          write(ndev_stdout_J_vs_alpha,*) 'variables = <greek>a</greek>, J(<greek>f</greek>), mode, sub-iter'
          write(ndev_stdout_J_vs_alpha,'(2F36.25,I3,I5)') gAlpha, fret, region%optimization%curRunningMode, region%optimization%subIter
        end if ! myrank

      end if ! input%AdjOptim_Restart
      call mpi_barrier(mycomm, ierr)

      ! ... save cost functional at the previous iteration
      if (region%nSubCtrlTarget > 0) then
        do nt = 1,region%nSubCtrlTarget
          targ => region%subCtrlTarget(nt)

          targ%gCost_old = fret
        end do ! nt
      end if ! region%nSubCtrlTarget
      fp = fret ! ... so that all processors know both previous and current costs

      ! ... save optimal alpha at the previous iteration
      if (region%nSubActuators > 0) then
        do na = 1,region%nSubActuators
          act => region%subActuator(na)

          act%gAlpha_old = gAlpha
        end do ! na
      end if ! region%nSubActuators
      gAlpha_old = gAlpha ! ... so that all processors know both previous and current alphas
      call mpi_barrier(mycomm, ierr)

      ! ... bracket the minimum cost functional by downhill line minimization: 
      ! ... i.e. generalized distance in phi-space is searched so that it results 
      ! ... in the local minimum cost functional; gradient (thus line-minimizing 
      ! ... direction) is readily computed from the previous iteration and fixed 
      ! ... during line searching
      ! ... control input is also updated inside of dlinmin:
      ! ... phi_(k+1) = phi_(k) + alpha_(k) * h_(k)
      ! ... even restart_curRunningMode = OPTIM_optimum, we need to go through 
      ! ... all line minimization to pick up a correct set of alpha & cost
      region%optimization%curRunningMode = OPTIM_dlinmin; region%optimization%subIter = 0
      call dlinmin(region, fret, gAlpha)
      call mpi_barrier(mycomm, ierr)

      ! ... compute cost functional by solving forward problem.
      ! ... control applied here is expected to lead to local minimum of functional.
      ! ... according to Randy's code, dbrent doesn't always take the most recent 
      ! ... alpha value; therefore cost functional computed once again with the 
      ! ... optimal alpha, gAlpha.
      region%optimization%curRunningMode = OPTIM_optimum; region%optimization%subIter = 0
      if (region%optimization%solveNS == TRUE) dirname = CreateDirectory(region, DIR_ROOT)
      fret = CostFunctional(region, gAlpha)
      dummy = ControlGradient(region)
      call mpi_barrier(mycomm, ierr)

      ! ... update conjugate gradient vectors and update the next line-search 
      ! ... direction, h_(k+1) using gradient information
      ggP  = 0.0_rfreal
      dggP = 0.0_rfreal
      if (region%nSubActuators > 0) then
        do na = 1,region%nSubActuators
          act => region%subActuator(na)

          do j = 1,SIZE(act%xi,2)
            do i = 1,SIZE(act%xi,1)
              act%xi(i,j) = act%gradient(i,j) ! ... gradient_(k+1)
            end do ! i
          end do ! j

          ! ... compute gamma_(k) to update h_(k)
          do j = 1,SIZE(act%g,2)
            do i = 1,SIZE(act%g,1)
              ggP  = ggP  +  act%g(i,j) * act%g(i,j) ! ... g_(k) times g_(k)
              dggP = dggP + (act%xi(i,j) + act%g(i,j)) * act%xi(i,j) ! ... (g_(k+1) - g_(k)) times g_(k+1)
                                                                     ! ... g_(k+1) = -gradient_(k+1)
            end do ! i
          end do ! j
        end do ! na
      end if ! region%nSubActuators
      call mpi_barrier(mycomm, ierr)

      ! ... now sum numerators and denominators over all processors
      call mpi_allreduce( ggP, gg,1,MPI_REAL8,MPI_SUM,mycomm,ierr)
      call mpi_allreduce(dggP,dgg,1,MPI_REAL8,MPI_SUM,mycomm,ierr)
      if (gg == 0.0_rfreal) then
        call graceful_exit(region%myrank, "... gg is zero, fabulous but what does this mean?")
      else
        gam = dgg / gg
      end if ! gg

      if (region%nSubActuators > 0) then
        do na = 1,region%nSubActuators
          act => region%subActuator(na)

          do j = 1,SIZE(act%g,2)
            do i = 1,SIZE(act%g,1)
              act%g(i,j)  = -act%gradient(i,j) ! ... or -act%xi(i,j)
              act%h(i,j)  =  act%g(i,j) + gam * act%h(i,j)
              act%xi(i,j) =  act%h(i,j)
            end do ! i
          end do ! j
        end do ! na
      end if ! region%nSubActuators
      call mpi_barrier(mycomm, ierr)

      ! ... report locally optimized system parameters
      if (region%myrank == 0) then
        write(ndev_stdout_iterCG,'(I6,2F36.25)') region%GLOBAL_ITER, fret, gAlpha
      end if ! myrank
      call WriteCGVectors(region, region%GLOBAL_ITER)

      if (region%myrank == 0) then
        close(ndev_stdout_J_vs_alpha)
      end if ! myrank

    end do mainCGLoop
    call mpi_barrier(mycomm, ierr)



    ! ... Step 4: see why we're stopped
    if (region%GLOBAL_ITER == region%ITMAX) then
      call graceful_exit(region%myrank, "... ERROR: maximum iteration count reached")
    else
      ! ... conjugate gradient is converged!

    end if ! region%GLOBAL_ITER
    call mpi_barrier(mycomm, ierr)

    if (region%myrank == 0) then
      close(ndev_stdout_iterCG)
      close(ndev_stdout_optHistory)
    end if ! myrank

  end subroutine frprmn

  function GoOnIteration(region,gCost,gCost_old)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    ! ... subroutine arguments
    Type(t_region), Pointer :: region
    Real(rfreal) :: gCost,gCost_old
    Integer :: ierr
    LOGICAL :: GoOnIteration

    ! ... local variables and arrays
    Type(t_mixt_input), Pointer :: input

    ! ... simplicity
    input => region%input

    ! ... by default, we go on iteration
    GoOnIteration = .TRUE.

    ! ... see if maximum iteration number is reached
    if (region%GLOBAL_ITER == region%ITMAX) then
      GoOnIteration = .FALSE.
    end if ! region%GLOBAL_ITER

    ! ... note that convergence is checked at every processor even where there 
    ! ... is no target zone
    if (gCost < input%AdjOptim_eps .AND. &
        DABS(gCost - gCost_old) < input%AdjOptim_deps) then
      GoOnIteration = .FALSE.
    end if ! targ%gCost

    ! ... don't proceed until all processors are checked for convergence
    call mpi_barrier(mycomm, ierr)

  end function GoOnIteration

  subroutine dlinmin(region, fret, gAlpha)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    ! ... subroutine arguments
    Type(t_region), Pointer :: region
    Real(rfreal), INTENT(OUT) :: fret ! ... local minimum cost functional from the current line minimization
    Real(rfreal), INTENT(OUT) :: gAlpha ! ... this, when returned, leads to the minimum local 
                                        ! ... functional, fret

    ! ... local variables and arrays
    Type(t_mixt_input), Pointer :: input
    Type(t_sub_actuator), Pointer :: act
    Integer :: na, i, j, ierr
    Real(rfreal) :: ax, xx, bx, fa, fb, fx
    Real(rfreal) :: TOL, minAlpha
    Character(LEN=200) :: dirname

    ! ... simplicity
    input => region%input

    ! ... dlinmin successively calls mnbrak and dbrent to perform line 
    ! ... minimization.  If something's unclear, the original implementation 
    ! ... in Numerical Recipes (conjugate gradient, minimization or maximization) 
    ! ... should always be consulted.  Also Mingjun and Randy's codes need to be 
    ! ... checked.  Sometimes Bewley et al. (2001)'s explanation sounds more 
    ! ... comprehendible so should not be forgotten, JKim 08/2009

    ! ... Step 1: initial guess of generalized distance in phi-space for brackets
    ! ...         Wei & Freund used r while Bewley et al. liked alpha_(k)
    ! ...         this value is expected to minimize cost functional in the 
    ! ...         direction of gradient
    ax = input%AdjOptim_initAlpha
    xx = 5.0_rfreal * input%AdjOptim_initAlpha ! ... 5.0 comes from Randy's code



    ! ... Step 2: call mnbrak to bracket local minimum of cost functional 
    ! ...         in the direction of gradient; more accurate search is performed 
    ! ...         later by dbrent
    ! ...         xx is alpha (or r) where we have the local minimum, fx
    ! ...
    ! ...         Whether or not we restart, we need to execute the entire mnbrak 
    ! ...         subroutine; however, when CostFunctional is called, it simply 
    ! ...         reads the computed cost and return without running forward N-S.  
    ! ...         The reason doing this is that mnbrak has too many if-statements 
    ! ...         so actually implementing restart inside is virtually impossible
    region%optimization%curRunningMode = OPTIM_mnbrak; region%optimization%subIter = 1
    region%optimization%mnbrak_suffix = ' '
    if (region%optimization%solveNS == TRUE) dirname = CreateDirectory(region, DIR_ROOT)
    call mnbrak(ax,xx,bx,fa,fx,fb,CostFunctional,region)
    call mpi_barrier(mycomm, ierr)



    ! ... Step 3: dbrent to refine the local minimum and its location
    region%optimization%curRunningMode = OPTIM_dbrent; region%optimization%subIter = 1
    if (region%optimization%solveNS == TRUE) dirname = CreateDirectory(region, DIR_ROOT)
    TOL = input%AdjOptim_dbrent_eps
    fret = dbrent(ax,xx,bx,CostFunctional,ControlGradient,TOL,minAlpha,region)
    call mpi_barrier(mycomm, ierr)

    region%optimization%curRunningMode = OPTIM_dlinmin; region%optimization%subIter = 0



    ! ... Step 4: given optimized generalized distance in phi-space, minAlpha and 
    ! ...         previously computed gradient, update control vector
    if (region%nSubActuators > 0) then ! ... if there is any actuator in this region
      do na = 1,region%nSubActuators
        act => region%subActuator(na)

        act%gAlpha = minAlpha ! ... get alpha_(k) leading the functional to the 
                              ! ... local minimum in the gradient direction
        do j = 1,SIZE(act%xi,2)
          do i = 1,SIZE(act%xi,1)
            ! ... act%xi overwritten by minAlpha*act%xi in Numerical Recipes and 
            ! ... Randy's code but since act%xi is used in CostFunctional just 
            ! ... after dlinmin, we need to keep act%xi. BTW act%xi = h_(k)
            ! ... JKim 08/2009
            act%gPhiCtrl(i,j) = act%gPhiCtrl(i,j) + minAlpha * act%xi(i,j) ! ... optimal control input
                                                                           ! ... for the next iteration
          end do ! i
        end do ! j
      end do ! na
    end if ! region%nSubActuators
    gAlpha = minAlpha
    call mpi_barrier(mycomm, ierr)

  end subroutine dlinmin

  SUBROUTINE mnbrak(ax,bx,cx,fa,fb,fc,func,region)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    ! ... subroutine arguments
    REAL(WP) :: ax, bx, cx, fa, fb, fc
    Type(t_region), Pointer :: region

    INTERFACE

      FUNCTION func(region, x) ! ... mnbrak gets any function with t_region 
                               ! ... pointer and a real value, x

        USE ModGlobal
        USE ModDataStruct
        USE ModMPI

        IMPLICIT NONE

        Type(t_region), Pointer :: region

        !
        ! BUGFIX: To match the signature of CostFunctional
        ! the parameter x, must be declared as Optional
        !
        REAL(WP), Optional :: x
        REAL(WP)             :: func

      END FUNCTION func

    END INTERFACE

    ! ... local variables and arrays
    REAL(WP), PARAMETER :: GOLD   = 1.618034_WP
    REAL(WP), PARAMETER :: GLIMIT = 100.0_WP
    REAL(WP), PARAMETER :: TINI   = 1.E-20_WP
    REAL(WP) :: temp, fu, q, r, u, ulim
    INTEGER  :: ie

    ! ... Step 1: compute three initial guesses for bracketing
    region%optimization%mnbrak_suffix = '-init_ax'
    fa = func(region, ax) ! ... functional, fa computed at ax
    region%optimization%mnbrak_suffix = '-init_bx'
    fb = func(region, bx) ! ... functional, fb computed at bx
    IF (fb > fa) THEN ! ... when going from ax to bx, we want downhill (fa > fb)
      temp = ax
      ax = bx
      bx = temp

      temp = fa
      fa = fb
      fb = temp
    END IF ! fb
    region%optimization%mnbrak_suffix = '-init_cx'
    cx = bx + GOLD*(bx - ax) ! ... estimate cx by stretching in downhill direction
    fc = func(region, cx) ! ... functional, fc computed at cx



    ! ... Step 2: iterate until the local minimum is captured
    DO

       IF (fb < fc) RETURN ! ... fb is the local minimum, work done!;
                           ! ... otherwise we're still on downhill, keep going..

       ! ... u is parabolic-extrapolated with ax, bx, cx; ulim limits extrapolation
       r = (bx-ax)*(fb-fc)
       q = (bx-cx)*(fb-fa)
       u = bx-((bx-cx)*q-(bx-ax)*r)/(2.0_WP*SIGN(MAX(ABS(q-r),TINI),q-r))
       ulim = bx + GLIMIT*(cx-bx)

       IF ((bx-u)*(u-cx) > 0.0_WP) THEN ! ... u is between bx and cx

         region%optimization%mnbrak_suffix = '-u_betw_bxcx'
         fu = func(region, u) ! ... then compute u's functional
         IF (fu < fc) THEN ! ... (bx, u, cx) brackets the minimum, fu
            ax = bx
            fa = fb
            bx = u
            fb = fu
            RETURN

         ELSE IF(fu > fb) THEN ! ... (ax, bx, u) brackets the minimum, fb
            cx = u
            fc = fu
            RETURN
         END IF ! fu

         ! ... parabolic extrapolation didn't find local minimum and we're still 
         ! ... on downhill; use default magnification and move further on downhill
         region%optimization%mnbrak_suffix = '-moreDownhill'
         u = cx + GOLD*(cx - bx)
         fu = func(region, u)

       ELSE IF ((cx-u)*(u-ulim) > 0.0_WP) THEN ! ... u is outside of cx

         region%optimization%mnbrak_suffix = '-u_out_cx'
         fu = func(region, u)

         IF (fu < fc) THEN ! ... still downhill; if fu > fc, minimum is found at 
                           ! ... the next iteration
           bx = cx
           cx = u
           u = cx + GOLD*(cx - bx)
           region%optimization%mnbrak_suffix = '-moreDownhill'
           CALL shft(fb,fc,fu,func(region, u))
         END IF

       ELSE IF ((u-ulim)*(ulim-cx) >= 0.0_WP) THEN

          region%optimization%mnbrak_suffix = '-max_stretch'
          u = ulim
          fu = func(region, u)

       ELSE

          region%optimization%mnbrak_suffix = '-gold_stretch'
          u = cx + GOLD*(cx-bx)
          fu = func(region, u)

       END IF ! (bx-u)*(u-cx)

       CALL shft(ax,bx,cx,u)
       CALL shft(fa,fb,fc,fu)

    END DO

  CONTAINS

    SUBROUTINE shft(a,b,c,d) ! ... shift values to left; value in d gets disposed

      REAL(WP), INTENT(OUT)   :: a
      REAL(WP), INTENT(INOUT) :: b, c
      REAL(WP), INTENT(IN)    :: d

      a = b
      b = c
      c = d

    END SUBROUTINE shft

  END SUBROUTINE mnbrak

  FUNCTION dbrent(ax,bx,cx,func,dfunc,tol,xmin,region)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    ! ... subroutine arguments
    REAL(WP) :: ax, bx, cx, tol, xmin
    Type(t_region), Pointer :: region
    INTERFACE

      FUNCTION func(region, x)

        USE ModGlobal
        USE ModDataStruct
        USE ModMPI

        IMPLICIT NONE

        Type(t_region), Pointer :: region
        !
        ! BUGFIX: To match the signature of CostFunctional
        ! the parameter x, must be declared as Optional
        ! (gzagaris 7-22-2010)
        !
        REAL(WP), Optional :: x
        REAL(WP)             :: func

      END FUNCTION func

      FUNCTION dfunc(region, x)

        USE ModGlobal
        USE ModDataStruct
        USE ModMPI

        IMPLICIT NONE

        Type(t_region), Pointer :: region
        !
        ! BUGFIX: To match the signature of ControlGradient
        ! the parameter x, must be declared as Optional
        ! (gzagaris 7-22-2010)
        !
        REAL(WP), Optional :: x
        REAL(WP)             :: dfunc

      END FUNCTION dfunc

    END INTERFACE
    REAL(WP) :: dbrent

    ! ... local variables and arrays
!    INTEGER, PARAMETER :: ITMAX = 100        ! ... maximum allowed iterations
    Type(t_mixt_input), Pointer :: input
    REAL(WP), PARAMETER :: ZEPS =1.0e-3_WP * EPSILON(ax) ! ... small number that protects against being divided by exact zero.

    INTEGER :: iter,istart,ITMAX
    REAL(WP) :: a,b,d,d1,d2,du,dv,dw,dx,e,fu,fv,fw,fx
    REAL(WP) :: olde,tol1,tol2,u,u1,u2,v,w,x,xm
    LOGICAL :: ok1, ok2
    INTEGER :: ie
    Character(LEN=200) :: dirname

    ! ... simplicity
    input => region%input
    ITMAX = input%AdjOptim_DBrentMaxIt 
    IF((debug_on .eqv. .true.).and.(region%myrank == 0)) write(*,*) 'PlasComCM: DBRENT ITMAX =',ITMAX
    a = MIN(ax,cx) ! ... a and b in ascending order
    b = MAX(ax,cx)

    v = bx ! ... iterating variables, v, w, and, x
    w = v
    x = v

    e = 0.0_WP ! ... distance moved on the step before last

    fx = func(region, x) ! ... although fx is passed (from mnbrak), run once again 
                         ! ... for dfunc (need forward solutions!)
    fv = fx
    fw = fx
    dx = dfunc(region, x)
    dv = dx
    dw = dx

    fu = 0.0_WP
    u = 0.0_WP
    du = 0.0_WP

    istart = 1 ! ... not considering restart of dbrent yet
    DO iter = istart,ITMAX
      xm = 0.5_WP * (a + b)
      tol1 = tol * ABS(x) + ZEPS ! ... fractional precision of |x|; ZEPS in case of x = 0
      tol2 = 2.0_WP * tol1 ! ... two-sided tolerance

      ! ... escaping condition
      IF (ABS(x - xm) <= (tol2 - 0.5_WP * (b - a))) EXIT
      IF (ABS(e) > tol1) THEN ! ... going farther than the tolerance
        d1 = 2.0_WP * (b - a)
        d2 = d1
        IF (dw /= dx) d1 = (w-x) * dx / (dx-dw) ! ... secant method
        IF (dv /= dx) d2 = (v-x) * dx / (dx-dv) ! ... secant method
        u1 = x + d1
        u2 = x + d2

        ok1 = ((a-u1)*(u1-b) > 0.0_WP).AND.(dx*d1 <= 0.0_WP)
        ok2 = ((a-u2)*(u2-b) > 0.0_WP).AND.(dx*d2 <= 0.0_WP)
        olde = e
        e = d

        IF (ok1 .OR. ok2) THEN
           IF (ok1 .AND. ok2) THEN
             d = MERGE(d1, d2, ABS(d1) < ABS(d2))
           ELSE
             d = MERGE(d1, d2, ok1)
           END IF ! ok1

           IF (ABS(d) <= ABS(0.5_WP*olde)) THEN
              u = x + d
              IF (u-a < tol2 .OR. b-u < tol2) d=SIGN(tol1,xm-x)
           ELSE
              e = MERGE(a, b, dx >= 0.0) - x
              d = 0.5_WP * e ! ... bisect
           END IF ! ABS(d)
        ELSE
           e = MERGE(a, b, dx >= 0.0) - x ! ... if non-negative dx, e = a-x; otherwise e = b-x
           d = 0.5_WP * e ! ... bisect
        END IF ! ok1
      ELSE
        e = MERGE(a, b, dx >= 0.0) - x ! ... if non-negative dx, e = a-x; otherwise e = b-x
        d = 0.5_WP * e ! ... bisect
      END IF ! ABS(e)

      ! ... determine u first
      IF (ABS(d) >= tol1) THEN ! ... if moving farther than tol1
        u = x + d
        fu = func(region, u)
      ELSE ! ... moving within tol1
        u = x + SIGN(tol1, d) ! ... move by tol1 in the direction of sign of d
        fu = func(region, u)
        IF (fu > fx) THEN ! ... when moving within tol1i, fx is still the minimum; so get out
          if (.NOT. input%AdjOptim_Restart) then
            dirname = CreateDirectory(region, DIR_FWD)
            call MoveSolutionFiles(region, DIR_FWD, dirname)
          end if ! .NOT.

          EXIT
        END IF ! fu
      END IF

      ! ... compute gradient
      du = dfunc(region, u)

      IF (fu <= fx) THEN ! ... fu is a new minimum
        IF (u >= x) THEN ! ... set up new bracket
           a = x
        ELSE
           b = x
        END IF ! u

        CALL mov3(v,fv,dv,w,fw,dw)
        CALL mov3(w,fw,dw,x,fx,dx)
        CALL mov3(x,fx,dx,u,fu,du)

      ELSE ! ... fx is still minimum
        IF (u < x) THEN ! ... set up new bracket
           a = u
        ELSE
           b = u
        END IF ! u

        IF (fu <= fw .OR. w == x) THEN
          CALL mov3(v,fv,dv,w,fw,dw)
          CALL mov3(w,fw,dw,u,fu,du)
        ELSE IF (fu <= fv .OR. v == x .OR. v == w) THEN
          CALL mov3(v,fv,dv,u,fu,du)
        END IF
      END IF ! fu
    END DO ! iter
    if (iter > ITMAX) call graceful_exit(region%myrank, "... ERROR: dbrent: ITMAX exceeded before converging")
    xmin = x
    dbrent = fx

  CONTAINS

    SUBROUTINE mov3(a,b,c,d,e,f) 

      REAL(WP), INTENT(IN)  :: d,e,f
      REAL(WP), INTENT(OUT) :: a,b,c

      a = d
      b = e
      c = f

    END SUBROUTINE mov3

  END FUNCTION dbrent

  ! ... forward simulation carried out to compute cost functional with 
  ! ... initial or iterated control input
  function CostFunctional(region, alpha)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    USE ModTimemarch
    USE ModIO
    USE ModEOS
    USE ModActuator
    USE ModRungeKutta

    Implicit None

    ! ... subroutine arguments
    Type(t_region), Pointer :: region
    Real(rfreal), Optional :: alpha
    Real(rfreal) :: CostFunctional

    ! ... local variables and arrays
    Type(t_grid), Pointer :: grid
    Type(t_mixt), Pointer :: state
    Type(t_mixt_input), Pointer :: input
    Type(t_sub_actuator), Pointer :: act
    Type(t_sub_ctrl_target), Pointer :: targ
    Integer :: na, ng, nt, i, j, l0, ierr
    Integer :: idum1, idum2, ctrlStartIter
    Integer, Pointer :: main_ts_loop_index
    Real(rfreal) :: lCost, gCost, instCost, dt, dx, spaceDist, tmp, numCur_cvFile
    Real(rfreal) :: rdum1, rdum2
    Real(rfreal), Pointer :: ptCost(:), buf(:), ctrlMin(:), ptr_to_var(:)
    Character(200) :: command
    Character(30) :: citer
    Character(LEN=200) :: dirname
    Character(LEN=PATH_LENGTH) :: restart_fname(2)
    Character(LEN=80) :: cdum
    
    ! ... simplicity
    input => region%input
    main_ts_loop_index => region%global%main_ts_loop_index

    ! ... Step 0: If the functional is in terms of fluctuating quantities, we assume 
    ! ...         that statistics is already collected enough to compute mean value.
    ! ...         We therefore need to read or process statistics to explicitly store 
    ! ...         reference quantity
    ! ...         This should come before the following restart-related routines. 
    if (region%optimization%getMean == TRUE) then
      call GetMeanAtControlTarget(region)
      region%optimization%getMean = FALSE ! ... defensive programming
    end if ! getMean
    call mpi_barrier(mycomm, ierr)



    ! ... Step 1: decide how to evaluate cost functional; depending on restarting 
    ! ...         status, we may 
    ! ...         (i) run everything from beginning (no restart); 
    ! ...         (ii) skip everything; 
    ! ...         (iii) run part of time marching (restart)
    ! ... followings assume no restarting
    CostFunctional = 0.0_rfreal
    restart_fname(1) = ' '; restart_fname(1) = restart_fname_cv
    numCur_cvFile = 1
    ctrlStartIter = region%ctrlStartIter
    if (input%AdjOptim_Restart) then ! ... if not restarting, just proceed with above parameters
      if (region%GLOBAL_ITER < region%optimization%restart_GLOBAL_ITER) then ! ... we're behind the last CG iteration
                                                         ! ... not even have to report cost per se; go back
        CostFunctional = 0.0_rfreal; return

      else ! ... now we're at the same CG iteration as the last run (region%GLOBAL_ITER = restart_GLOBAL_ITER)
        ! ... subIter being non-zero, CostFunctional is called by either mnbrak or dbrent
        if (region%optimization%subIter /= 0) then
          if (.NOT. PRESENT(alpha)) call graceful_exit(region%myrank, "... ERROR: non-zero subIter needs alpha")

          if (region%optimization%curRunningMode < region%optimization%restart_curRunningMode .OR. & ! ... catch up ONLY one sub-iteration and return
              region%optimization%subIter < region%optimization%restart_subIter) then
            write(citer,'(I3.3)') region%GLOBAL_ITER
            open(ndev_stdout_J_vs_alpha, file=TRIM(fname_stdout_J_vs_alpha)//TRIM(citer), STATUS='OLD')
            rewind(ndev_stdout_J_vs_alpha)
            read(ndev_stdout_J_vs_alpha,'(A)') cdum

            idum1 = OPTIM_dlinmin; idum2 = 0
            do while(idum1 < region%optimization%curRunningMode .OR. idum2 < region%optimization%subIter)
              read(ndev_stdout_J_vs_alpha,*) rdum1, rdum2, idum1, idum2
            end do ! idum2
            if (rdum1 /= alpha) call graceful_exit(region%myrank, "... ERROR: alpha in CostFunctional inconsistent")

            close(ndev_stdout_J_vs_alpha)

            ! ... report a previous functional
            CostFunctional = rdum2
            call CostFunctional_UpdateCost(region, CostFunctional)

            ! ... dbrent increases subIter after computing gradient
            if (region%optimization%curRunningMode == OPTIM_mnbrak) region%optimization%subIter = region%optimization%subIter + 1

            return

          else ! ... same running mode and same sub-iteration as before
               ! ... must be restarting at this sub-iteration; proceed to restart

          end if ! curRunningMode
        end if ! subIter

        ! ... see if we really need to run forward N-S
        if (region%optimization%restart_whichNS == 'ADJ') then ! ... the last run stopped while solving adjoint N-S
                                           ! ... add up instantaneous cost functional written in 
                                           ! ... fname_stdout_J_vs_time, report it, and return
          CostFunctional = CostFunctional_PreviousCostFunctional(input)
          call CostFunctional_UpdateCost(region, CostFunctional)
          return

        else ! ... forward N-S was running last time
             ! ... decide when we restart
          if (PRESENT(alpha)) then
            write(citer,'(I3.3)') region%GLOBAL_ITER
            if (region%myrank == 0) open(ndev_stdout_J_vs_alpha, file=TRIM(fname_stdout_J_vs_alpha)//TRIM(citer), POSITION='APPEND')
          end if ! PRESENT(alpha)

          if (region%optimization%restart_main_ts_loop_index == region%ctrlEndIter) then ! ... the last run happened to stop 
                                                                     ! ... at the end of forward N-S solver
            CostFunctional = CostFunctional_PreviousCostFunctional(input)
            call CostFunctional_UpdateCost(region, CostFunctional)

            gCost = CostFunctional
            if (region%optimization%solveNS == TRUE) then
              if (region%myrank == 0) then
                if (PRESENT(alpha)) &
                  write(ndev_stdout_J_vs_alpha,'(2F36.25,I3,I5)') alpha, gCost, region%optimization%curRunningMode, region%optimization%subIter
              end if ! myrank
            end if ! solveNS
            call mpi_barrier(mycomm, ierr)

            if (region%optimization%solveNS == TRUE) then
              call CostFunctional_MoveSolutionFiles(region)
            end if ! solveNS

            ! ... now that we've restarted, no more of it
            input%AdjOptim_Restart = .FALSE.

            return

          else ! ... now we finally restart our forward N-S run
               ! ... reconstruct cost functional up to the latest time step
            CostFunctional = CostFunctional_PreviousCostFunctional(input)
            restart_fname = ' '; write (restart_fname(1),'(A10,I8.8,A2)') 'RocFlo-CM.', region%optimization%restart_main_ts_loop_index, '.q'
            numCur_cvFile = (region%optimization%restart_main_ts_loop_index - region%ctrlStartIter) / input%noutput
            ctrlStartIter = region%optimization%restart_main_ts_loop_index

            ! ... now that we've restarted, no more of it
            region%optimization%subIter = region%optimization%restart_subIter
            input%AdjOptim_Restart = .FALSE.

          end if ! restart_main_ts_loop_index
        end if ! restart_whichNS
      end if ! region%GLOBAL_ITER

    end if ! input%AdjOptim_Restart



    ! ... Step 2: update control if indicated; act%xi = h_(k) computed by 
    ! ...         conjugate gradient algorithm from the last iteration
    if (PRESENT(alpha)) then
      if (region%optimization%curRunningMode == OPTIM_optimum) then
        tmp = alpha
        alpha = 0.0_rfreal ! ... to use readily (locally) optimized control
      end if ! curRunningMode

      if (region%nSubActuators > 0) then ! ... if there is any actuator in this region
        do na = 1,region%nSubActuators
          act => region%subActuator(na)

          act%switch = CONTROL_ON
          do j = 1,SIZE(act%gPhi,2)
            do i = 1,SIZE(act%gPhi,1)
              act%gPhi(i,j) = act%gPhiCtrl(i,j) + alpha * act%xi(i,j)
            end do ! i
          end do ! j
        end do ! na
      end if ! region%nSubActuators

      if (region%optimization%curRunningMode == OPTIM_optimum) then
        alpha = tmp
      end if ! curRunningMode

    else
      if (region%nSubActuators > 0) then ! ... if there is any actuator in this region
        do na = 1,region%nSubActuators
          act => region%subActuator(na)

          act%switch = CONTROL_OFF
          act%gPhi(:,:) = 0.0_rfreal
        end do ! na
      end if ! region%nSubActuators

    end if ! PRESENT(alpha)
    call mpi_barrier(mycomm, ierr)



    ! ... Step 3: run forward simulation to calculate local cost functional. 
    ! ...         Unless functional is specified, forward N-S equations should 
    ! ...         be solved.
    lCost = 0.0_rfreal ! ... by default, no cost functional in every processor

    SELECT CASE( input%AdjOptim_typeOfOptim )
    CASE( 1:2 ) ! ... a 2-D rectangular-shape controller
                ! ... invented for model controller test
                ! ... due to the definition of cost functional, local cost functionals 
                ! ... computed in actuator region and sent to control target zone.
      if (region%nSubActuators > 0) then ! ... if there is any actuator in this region
        do na = 1,region%nSubActuators
          act => region%subActuator(na)
          ng = act%gridID
          grid => region%grid(ng)

          nullify(ptCost); allocate(ptCost(act%numPts)); ptCost = 0.0_rfreal
          nullify(ctrlMin); allocate(ctrlMin(act%numPts)); ctrlMin = 0.0_rfreal

          ! ... optimal control variable in quadratic form centered at origin
          ! ... this has nothing to do with functional
          do j = 1,act%numPts
            l0 = act%index(j)
            ctrlMin(j) = (grid%xyz(l0,1)**2 + grid%xyz(l0,2)**2)
          end do ! j

          if (input%AdjOptim_typeOfOptim == 1) then ! ... quadratic functional of control variable
            ! ... mid-point integration to compute local cost functional
            do j = 1,SIZE(act%gPhi,2)
              do i = 1,SIZE(act%gPhi,1) ! ... i = 1 (no time series needed)
                l0 = act%index(j)
                ptCost(j) = ptCost(j) + grid%INVJAC(l0)*(act%gPhi(i,j) - ctrlMin(j))**2
              end do ! i
            end do ! j

          else if (input%AdjOptim_typeOfOptim == 2) then ! ... quartic functional of control variable
            ! ... mid-point integration to compute local cost functional
            do j = 1,SIZE(act%gPhi,2)
              do i = 1,SIZE(act%gPhi,1) ! ... i = 1 (no time series needed)
                l0 = act%index(j)
                ptCost(j) = ptCost(j) + grid%INVJAC(l0)*(act%gPhi(i,j) - ctrlMin(j))**4
              end do ! i
            end do ! j

          end if ! input%AdjOptim_typeOfOptim
          lCost = lCost + SUM(ptCost(:))

          deallocate(ptCost, ctrlMin)
        end do ! na
      end if ! region%nSubActuators

    CASE DEFAULT ! ... cost functional calculated by solving forward N-S

      ! ... read initial data (i.e. conserved variables, state%cv) from the 
      ! ... last uncontrolled forward N-S solution
      ! ... this file is same for every call of CostFunctional
      ! ... state%cvTarget, state%cvTargetOld also re-read (paranoid)
      if (region%optimization%solveNS == FALSE) &
          call graceful_exit(region%myrank, "... ERROR: forward N-S should be solved")

      region%optimization%whichNS = 'FWD'
      input%AdjNS = .FALSE. ! ... defensive programming
      call mpi_barrier(mycomm,ierr)
      IF((debug_on .eqv. .true.).and.(region%myrank == 0)) write(*,*) 'PlasComCM: Adj reading restart'
      call ReadRestart(region, restart_fname); input%nstepi = region%ctrlStartIter ! ... read initial state%cv, state%time, input%nstepi
      call mpi_barrier(mycomm,ierr)
      IF((debug_on .eqv. .true.).and.(region%myrank == 0)) write(*,*) 'PlasComCM: Adj read_restart done, read_target'
      call ReadTarget(region, input%target_fname) ! ... read state%cvTarget
      call mpi_barrier(mycomm,ierr)
      IF((debug_on .eqv. .true.).and.(region%myrank == 0)) write(*,*) 'PlasComCM: Adj read_target done'
      ! ... set the basic target state
      do ng = 1, region%nGrids
        state => region%state(ng)

          do j = 1, size(state%cvTarget,2)
            do i = 1, size(state%cvTarget,1)
              state%cvTargetOld(i,j) = state%cvTarget(i,j)
            end do
          end do
      end do ! ng

      ! ... move the cv-file pointer at the very first location; 
      ! ... this is necessary because we need to interpolate from readily stored 
      ! ... act%gPhi(:,:) to get instantaneous control act%phi(:)
      do ng = 1,region%nGrids
        state => region%state(ng)

        state%numCur_cvFile = numCur_cvFile
      end do ! ng

      main_ts_loop_index = ctrlStartIter
      ! ... if not restarting, write initial data
      if (ctrlStartIter == region%ctrlStartIter) then
        ! ... write initial state
         CALL WriteData(region,'xyz')
         CALL WriteData(region,'cv ')
         !        call write_plot3d_file(region, 'xyz')
         !       call write_plot3d_file(region, 'cv ')
      end if ! ctrlStartIter

      ! ... prepare for time advancement when N-S equations are solved
      if (region%optimization%solveNS == TRUE) then
        if (region%myrank == 0) open(ndev_stdout, file=TRIM(fname_stdout_fwd), POSITION='APPEND')
        if (region%myrank == 0) open(ndev_stdout_J_vs_time, file=TRIM(fname_stdout_J_vs_time), POSITION='APPEND')

        if (input%timeScheme == EXPLICIT_RK4) then
          if (ctrlStartIter == region%ctrlStartIter) then
            if (region%GLOBAL_ITER > 0) then
              if (region%myrank == 0) write (*,'(A)') 'PlasComCM: ==> Using RK4 time integration <=='
            end if ! region%GLOBAL_ITER
            if (region%myrank == 0) write (ndev_stdout,'(A)') 'PlasComCM: ==> Using RK4 time integration <=='
          end if ! ctrlStartIter
        end if ! input%timeScheme
      end if ! solveNS

      ! ... if not restarting, write initial cost and control
      if (ctrlStartIter == region%ctrlStartIter) then
        ! ... compute initial cost
        call computeInstantaneousCost(region, instCost) ! ... instantaneous cost functional in this processor

        ! ... temporal mid-point integration
        dt = REAL(input%noutput,rfreal) * input%dt
        lCost = lCost + dt * instCost ! ... time-integrated cost functional in this processor up to now

        ! ... global cost functional: instantaneous and time-integrated
        gCost = 0.0_rfreal
        call mpi_allreduce(instCost,gCost,1,MPI_REAL8,MPI_SUM,mycomm,ierr)
        if (region%myrank == 0) then
          write(ndev_stdout_J_vs_time,'(F20.10,I12,F36.25)') region%state(1)%time(1), main_ts_loop_index, gCost
          write(ndev_stdout_optHistory,'(3I7,I20,A)') region%GLOBAL_ITER, region%optimization%curRunningMode, region%optimization%subIter, main_ts_loop_index, '      FWD'
        end if ! myrank

        ! ... write initial control
        if (region%optimization%curRunningMode /= OPTIM_IC) then
          if (region%nSubActuators > 0) then
            do na = 1,region%nSubActuators
              act => region%subActuator(na)

              call computeInstantaneousControl(region, act)
            end do ! na
          end if ! region%nSubActuators
          call WriteData(region, 'ctr')
        end if ! curRunningMode
      end if ! ctrlStartIter

      Do main_ts_loop_index = ctrlStartIter+1, region%ctrlEndIter

        if (input%timeScheme == EXPLICIT_RK4) then
          call ARK2(region)
        else
          call graceful_exit(region%myrank, "... ERROR: only RK4 available for optimization; how did you get here?")
        end if ! input%timeScheme

        ! ... iteration output
        if (region%myrank == 0) &
             write(*,'(A,I8,3(A,E20.8))') 'PlasComCM: iteration = ', &
             main_ts_loop_index, ', dt = ', region%state(1)%dt(1), &
             ', time = ', region%state(1)%time(1), ', cfl = ', &
             region%state(1)%cfl(1)

        ! ... iteration output to a file
        if (region%optimization%solveNS == TRUE) then
          if (region%myrank == 0) then
             write(ndev_stdout,'(A,I8,3(A,E20.8))') 'PlasComCM: iteration = ', &
             main_ts_loop_index, ', dt = ', region%state(1)%dt(1), &
             ', time = ', region%state(1)%time(1), ', cfl = ', &
             region%state(1)%cfl(1)
          end if ! myrank
        end if ! solveNS

        ! ... compute cost functional at target region
        if (mod(main_ts_loop_index,input%noutput) == 0) then
          call computeInstantaneousCost(region, instCost) ! ... instantaneous cost functional in this processor

          ! ... temporal mid-point integration
          dt = REAL(input%noutput,rfreal) * input%dt
          lCost = lCost + dt * instCost ! ... time-integrated cost functional in this processor up to now

          ! ... global cost functional: instantaneous and time-integrated
          gCost = 0.0_rfreal
          call mpi_allreduce(instCost,gCost,1,MPI_REAL8,MPI_SUM,mycomm,ierr)
          if (region%myrank == 0) then
            write(ndev_stdout_J_vs_time,'(F20.10,I12,F36.25)') region%state(1)%time(1), main_ts_loop_index, gCost
            write(ndev_stdout_optHistory,'(3I7,I20,A)') region%GLOBAL_ITER, region%optimization%curRunningMode, region%optimization%subIter, main_ts_loop_index, '      FWD'
          end if ! myrank
        end if ! mod(main_ts_loop_index,input%noutput)

        ! ... output routines
        if (mod(main_ts_loop_index,input%noutput) == 0) then
          call WriteData(region, 'cv ')
          if (region%optimization%curRunningMode /= OPTIM_IC) then
            if (region%nSubActuators > 0) then
              do na = 1,region%nSubActuators
                act => region%subActuator(na)

                call computeInstantaneousControl(region, act)
              end do ! na
            end if ! region%nSubActuators
            call WriteData(region, 'ctr')
          end if ! curRunningMode
        end if ! mod(main_ts_loop_index,input%noutput)

        ! ... restart routines
!!$     if ((mod(main_ts_loop_index,input%nrestart) == 0) .or. &
!!$          main_ts_loop_index == (input%nstepi+input%nstepmax)) then
!!$       call write_plot3d_file(region, 'rst')
!!$     end if ! mod(main_ts_loop_index,input%nrestart)

        call store_probe_data(region) ! ... JKim 12/2008

        if (mod(main_ts_loop_index,input%noutput) == 0) then
          call write_probe_data(region) ! ... JKim 12/2008
        end if ! mod(main_ts_loop_index,input%noutput)

      End Do ! main_ts_loop_index
      if (region%optimization%solveNS == TRUE) then
        if (region%myrank == 0) close(ndev_stdout)
        if (region%myrank == 0) close(ndev_stdout_J_vs_time)
      end if ! solveNS

    END SELECT ! input%AdjOptim_typeOfOptim

    call mpi_barrier(mycomm, ierr)



    ! ... Step 4: update global cost functional right here
    ! ...         each processor has its own local cost (not every cost is non-trivial)
    gCost = 0.0_rfreal
    call mpi_allreduce(lCost,gCost,1,MPI_REAL8,MPI_SUM,mycomm,ierr)
    CostFunctional = CostFunctional + gCost ! ... if restarting, CostFunctional on RHS may not be zero
    gCost = CostFunctional
    call CostFunctional_UpdateCost(region, gCost)
    call mpi_barrier(mycomm, ierr)



    ! ... Step 6: report cost functional at this given alpha
    if (region%optimization%solveNS == TRUE) then
      if (region%myrank == 0) then
        if (PRESENT(alpha)) &
          write(ndev_stdout_J_vs_alpha,'(2F36.25,I3,I5)') alpha, gCost, region%optimization%curRunningMode, region%optimization%subIter
      end if ! myrank
    end if ! solveNS
    call mpi_barrier(mycomm, ierr)



    ! ... Step 5: move & store forward solutions when N-S equations are solved
    ! ...         only done when CostFunctional is called by mnbrak; 
    ! ...         i.e. curRunningMode = OPTIM_mnbrak
    if (region%optimization%solveNS == TRUE) then
      call CostFunctional_MoveSolutionFiles(region)
    end if ! solveNS
    call mpi_barrier(mycomm, ierr)

  contains

    function CostFunctional_PreviousCostFunctional(input)

      USE ModGlobal
      USE ModDataStruct
      USE ModMPI

      Implicit None

      ! ... subroutine arguments
      Type(t_mixt_input), Pointer :: input
      Real(rfreal) :: CostFunctional_PreviousCostFunctional

      ! ... local variables and arrays
      Integer :: io_err, idummy
      Real(rfreal) :: fret, fp, dummy

      CostFunctional_PreviousCostFunctional = 0.0_rfreal

      if (region%optimization%restart_whichNS == 'FWD' .AND. &
          region%optimization%restart_main_ts_loop_index == region%ctrlStartIter) return ! ... defensive programming

      open(ndev_stdout_J_vs_time, file=TRIM(fname_stdout_J_vs_time), STATUS='OLD')
      rewind(ndev_stdout_J_vs_time)

      fret = 0.0_rfreal
      io_err = FALSE
      do while(io_err == FALSE)
        read(ndev_stdout_J_vs_time,*,iostat=io_err) dummy, idummy, fp
        fret = fret + fp
      end do ! io_err

      close(ndev_stdout_J_vs_time)

      fret = fret - fp ! ... remove the last one (counted two times)
      fret = fret * REAL(input%noutput,rfreal) * input%dt ! ... temporal mid-point integration

      CostFunctional_PreviousCostFunctional = fret

    end function CostFunctional_PreviousCostFunctional

    subroutine CostFunctional_UpdateCost(region, gCost)

      USE ModGlobal
      USE ModDataStruct
      USE ModMPI

      Implicit None

      ! ... subroutine arguments
      Type(t_region), Pointer :: region
      Real(rfreal) :: gCost

      ! ... local variables and arrays
      Type(t_sub_ctrl_target), Pointer :: targ
      Integer :: nt

      if (region%nSubCtrlTarget > 0) then
        do nt = 1,region%nSubCtrlTarget
          targ => region%subCtrlTarget(nt)

          targ%gCost = gCost
        end do ! nt
      end if ! region%nSubCtrlTarget

      call mpi_barrier(mycomm, ierr)

    end subroutine CostFunctional_UpdateCost

    subroutine CostFunctional_MoveSolutionFiles(region)

      USE ModGlobal
      USE ModDataStruct
      USE ModMPI

      Implicit None

      ! ... subroutine arguments
      Type(t_region), Pointer :: region

      ! ... local variables and arrays
      Character(LEN=200) :: dirname

      ! ... for all the other running modes, forward solutions should remain 
      ! ... where they are since their adjoint run will be coming
      if (region%optimization%curRunningMode == OPTIM_mnbrak) then
        dirname = CreateDirectory(region, DIR_FWD)
        call MoveSolutionFiles(region, DIR_FWD, dirname)

        region%optimization%subIter = region%optimization%subIter + 1
      end if ! curRunningMode

      call mpi_barrier(mycomm, ierr)

    end subroutine CostFunctional_MoveSolutionFiles

  end function CostFunctional

  ! ... adjoint simulation carried out to compute gradient to control input 
  ! ... using forward simulation results
  function ControlGradient(region, alpha)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    USE ModTimemarch
    USE ModIO
    USE ModEOS
    USE ModRungeKutta

    Implicit None

    ! ... subroutine arguments
    Type(t_region), Pointer :: region
    Real(rfreal), Optional :: alpha
    Real(rfreal) :: ControlGradient

    ! ... local variables and arrays
    Type(t_grid), Pointer :: grid
    Type(t_mixt), Pointer :: state
    Type(t_mixt_input), Pointer :: input
    Type(t_sub_actuator), Pointer :: act
    Integer :: na, ng, i, j, l0, numCur_cvFile, ctrlEndIter, idum1, idum2, ierr
    Integer, Pointer :: main_ts_loop_index
    Real(rfreal) :: df1dim, df1dimP
    Real(rfreal), Pointer :: ctrlMin(:)
    Real(rfreal) :: rdum1, rdum2
    Character(LEN=200) :: dirname
    Character(LEN=PATH_LENGTH) :: restart_fname(2)
    Character(30) :: citer,csubiter
    Character(Len=80) :: cdum

    ! ... simplicity
    input => region%input
    main_ts_loop_index => region%global%main_ts_loop_index

    ! ... decide how to evaluate control gradient; depending on restarting 
    ! ... status, we may 
    ! ... (i) run everything from beginning (no restart); 
    ! ... (ii) skip everything; 
    ! ... (iii) run part of backward time marching (restart)
    ! ... followings assume no restarting
    ControlGradient = 0.0_rfreal
    restart_fname = ' ' ! ... by default, state%av = 0 (no time boundary condition)
    numCur_cvFile = region%numFwdSolnFiles - 1
    ctrlEndIter = region%ctrlEndIter ! ... because integrating backward in time
    if (input%AdjOptim_Restart) then ! ... if not restarting, just proceed with above parameters
      if (region%GLOBAL_ITER < region%optimization%restart_GLOBAL_ITER) then ! ... we're behind the last CG iteration; go back
        ControlGradient = 0.0_rfreal; return

      else ! ... now we're at the same CG iteration as the last run (region%GLOBAL_ITER = restart_GLOBAL_ITER)
        ! ... subIter being non-zero, ControlGradient is called only by dbrent
        if (region%optimization%subIter /= 0) then
          if (.NOT. PRESENT(alpha)) call graceful_exit(region%myrank, "... ERROR: non-zero subIter needs alpha")

          if (region%optimization%curRunningMode /= OPTIM_dbrent) call graceful_exit(region%myrank, "... ERROR: inconsistency between subIter and curRunningMode")

          if (region%optimization%curRunningMode < region%optimization%restart_curRunningMode .OR. & ! ... catch up ONLY one sub-iteration and return
              region%optimization%subIter < region%optimization%restart_subIter) then
            write(citer,'(I3.3)') region%GLOBAL_ITER
            open(ndev_stdout_J_vs_alpha, file=TRIM(fname_stdout_J_vs_alpha)//TRIM(citer), STATUS='OLD')
            rewind(ndev_stdout_J_vs_alpha)
            read(ndev_stdout_J_vs_alpha,'(A)') cdum

            idum1 = OPTIM_dlinmin; idum2 = 0
            do while(idum1 < region%optimization%curRunningMode .OR. idum2 < region%optimization%subIter)
              read(ndev_stdout_J_vs_alpha,*) rdum1, rdum2, idum1, idum2
            end do ! idum2
            if (rdum1 /= alpha) call graceful_exit(region%myrank, "... ERROR: alpha in ControlGradient inconsistent")

            close(ndev_stdout_J_vs_alpha)

            ! ... compute and report function return value
            write(csubiter,'(I3.3)') region%optimization%subIter
            dirname = './'//'iter'//TRIM(citer)//'/dbrent/AdjNS-'//TRIM(csubiter)//'/'
            call ControlGradient_PreviousControlGradient(region, 1, dirname)
            ControlGradient = ControlGradient_ComputeReturn(region)

            ! ... mnbrak doesn't call gradient subroutine
            if (region%optimization%curRunningMode == OPTIM_dbrent) region%optimization%subIter = region%optimization%subIter + 1

            return

          else ! ... same sub-iteration as before
               ! ... must be restarting at this sub-iteration; proceed to restart

          end if ! curRunningMode
        end if ! subIter

        ! ... see if we really need to run adjoint N-S
        if (region%optimization%restart_whichNS == 'FWD') then
          if (region%optimization%restart_main_ts_loop_index < region%ctrlEndIter) then ! ... the last run stopped while solving forward N-S
                                                                    ! ... therefore adjoint run has not been even made yet; 
            ControlGradient = 0.0_rfreal; return

          else                                                      ! ... restart_main_ts_loop_index = region%ctrlEndIter 
                                                                    ! ... same as starting adjoint N-S from scratch; proceed
            ! ... now that we've restarted, no more of it
            input%AdjOptim_Restart = .FALSE.

          end if ! restart_main_ts_loop_index

        else ! ... adjoint N-S was running last time
             ! ... decide when we restart
          if (PRESENT(alpha)) then
            write(citer,'(I3.3)') region%GLOBAL_ITER
            if (region%myrank == 0) open(ndev_stdout_J_vs_alpha, file=TRIM(fname_stdout_J_vs_alpha)//TRIM(citer), POSITION='APPEND')
          end if ! PRESENT(alpha)

          if (region%optimization%restart_main_ts_loop_index == region%ctrlStartIter) then ! ... the last run happened to stop 
                                                                       ! ... at the last step of adjoint N-S solver
            ! ... read the whole computed gradients and save
            call Initialize_AdjInterpolate_cv(region, .FALSE., 1)
            dirname = './'
            call ControlGradient_PreviousControlGradient(region, 1, dirname)

            ! ... compute function return if any
            if (PRESENT(alpha)) then
              ControlGradient = ControlGradient_ComputeReturn(region)
            else
              ControlGradient = 0.0_rfreal ! ... dummy value when there is no alpha
            end if ! PRESENT(alpha)

            ! ... store files
            if (region%optimization%solveNS == TRUE) then
              call ControlGradient_MoveSolutionFiles(region)
            end if ! solveNS

            ! ... now that we've restarted, no more of it
            input%AdjOptim_Restart = .FALSE.

            return

          else ! ... now we finally restart our adjoint N-S run
            restart_fname = ' '; write (restart_fname(1),'(A10,I8.8,A5)') 'RocFlo-CM.', region%optimization%restart_main_ts_loop_index, '.av.q'
            numCur_cvFile = (region%optimization%restart_main_ts_loop_index - region%ctrlStartIter) / input%noutput
            ctrlEndIter = region%optimization%restart_main_ts_loop_index

            ! ... read the computed gradient up to now and proceed
            call Initialize_AdjInterpolate_cv(region, .FALSE., numCur_cvFile)
            dirname = './'
            call ControlGradient_PreviousControlGradient(region, numCur_cvFile+1, dirname)

            ! ... now that we've restarted, no more of it
            region%optimization%subIter = region%optimization%restart_subIter
            input%AdjOptim_Restart = .FALSE.

          end if ! restart_main_ts_loop_index
        end if ! restart_whichNS
      end if ! region%GLOBAL_ITER
    end if ! input%AdjOptim_Restart



    SELECT CASE( input%AdjOptim_typeOfOptim )
    CASE( 1:2 ) ! ... a 2-D rectangular-shape controller
                ! ... invented for model controller test
      if (region%nSubActuators > 0) then
        do na = 1,region%nSubActuators
          act => region%subActuator(na)
          ng = act%gridID
          grid => region%grid(ng)

          nullify(ctrlMin); allocate(ctrlMin(act%numPts)); ctrlMin = 0.0_rfreal

           ! ... optimal control variable in quadratic form centered at origin
          do j = 1,act%numPts
            l0 = act%index(j)
            ctrlMin(j) = (grid%xyz(l0,1)**2 + grid%xyz(l0,2)**2)
          end do ! j

          if (input%AdjOptim_typeOfOptim == 1) then ! ... quadratic functional of control variable
            do j = 1,SIZE(act%gradient,2)
              do i = 1,SIZE(act%gradient,1) ! ... i = 1 (no time series needed)
                act%gradient(i,j) = 2.0_rfreal * (act%gPhi(i,j) - ctrlMin(j))
              end do ! i
            end do ! j

          else if (input%AdjOptim_typeOfOptim == 2) then ! ... quartic functional of control variable
            do j = 1,SIZE(act%gradient,2)
              do i = 1,SIZE(act%gradient,1) ! ... i = 1 (no time series needed)
                act%gradient(i,j) = 4.0_rfreal * (act%gPhi(i,j) - ctrlMin(j))**3
              end do ! i
            end do ! j

          end if ! input%AdjOptim_typeOfOptim

          deallocate(ctrlMin)
        end do ! na
      end if ! region%nSubActuators

    CASE DEFAULT ! ... adjoint N-S solved to compute gradient to control input
                 ! ... gradient has support only at controller region

      ! ... initialize adjoint variables, av and target state, avTarget
      ! ... by definition, initial state%av = 0 and state%avTarget = 0; thus 
      ! ... unlike forward N-S, read_restart and read_target are not called; 
      ! ... however, if adjoint N-S is restarted, read_restart should be called
      region%optimization%whichNS = 'ADJ'
      input%AdjNS = .TRUE. ! ... defensive programming

      if (ctrlEndIter == region%ctrlEndIter) then ! ... no adjoint restarting, yeah
        do ng = 1, region%nGrids
          state => region%state(ng)

          state%time(:) = region%ctrlEndTime

          do j = 1, size(state%av,2)
            do i = 1, size(state%av,1)
              state%av(i,j) = 0.0_rfreal ! ... zero time boundary condition
              state%avTarget(i,j) = 0.0_rfreal ! ... no effect of adjoint at far-field
              state%avTargetOld(i,j) = state%avTarget(i,j)
            end do ! i
          end do ! j
        end do ! ng

      else ! ... we have to restart..sigh..
        call ReadRestart(region, restart_fname); input%nstepi = region%ctrlStartIter ! ... read initial state%av, state%time, input%nstepi

        do ng = 1, region%nGrids
          state => region%state(ng)

!!$       state%time(:) = region%ctrlEndTime

          do j = 1, size(state%av,2)
            do i = 1, size(state%av,1)
!!$           state%av(i,j) = 0.0_rfreal ! ... zero time boundary condition
              state%avTarget(i,j) = 0.0_rfreal ! ... no effect of adjoint at far-field
              state%avTargetOld(i,j) = state%avTarget(i,j)
            end do ! i
          end do ! j
        end do ! ng

      end if ! ctrlEndIter

      main_ts_loop_index = ctrlEndIter

      ! ... set up information on forward solution files to be read
      ! ... called only when N-S equations are solved
      if (input%AdjNS_ReadNSSoln) then
        ! ... zero out conserved variables of forward N-S problem
        do ng = 1, region%nGrids
          grid => region%grid(ng)
          state => region%state(ng)

          state%cv(:,1) = 1.0_rfreal
          do i = 1,grid%ND
            state%cv(:,i+1) = state%cv(:,1) * 0.0_rfreal
          end do ! 
          state%cv(:,grid%ND+2) = 1.0_rfreal/1.4_rfreal/0.4_rfreal
        end do ! ng

        call Initialize_AdjInterpolate_cv(region, .TRUE., numCur_cvFile)

        do ng = 1, region%nGrids
          call AdjInterpolate_cv(region, ng)
        end do ! ng
      end if ! input%AdjNS_ReadNSSoln

      ! ... write initial state when not restarting
      if (ctrlEndIter == region%ctrlEndIter) then
        call WriteData(region, 'av ')

        if (region%myrank == 0) then
          write(ndev_stdout_optHistory,'(3I7,I20,A)') region%GLOBAL_ITER, region%optimization%curRunningMode, region%optimization%subIter, main_ts_loop_index, '      ADJ'
        end if ! myrank
      end if ! ctrlEndIter

      if (ctrlEndIter == region%ctrlEndIter) then
        ! ... compute initial gradient
        call ComputeGradient(region, region%numFwdSolnFiles)
        call WriteGradient(region, region%numFwdSolnFiles)
      end if ! ctrlEndIter

      ! ... prepare for time advancement when N-S equations are solved
      if (region%optimization%solveNS == TRUE) then
        if (region%myrank == 0) open(ndev_stdout, file=TRIM(fname_stdout_adj), POSITION='APPEND')

        if (input%timeScheme == EXPLICIT_RK4) then
          if (ctrlEndIter == region%ctrlEndIter) then
            if (region%GLOBAL_ITER > 0) then
              if (region%myrank == 0) write (*,'(A)') 'PlasComCM: ==> Using RK4 backward time integration <=='
            end if ! region%GLOBAL_ITER
            if (region%myrank == 0) write (ndev_stdout,'(A)') 'PlasComCM: ==> Using RK4 backward time integration <=='
          end if ! ctrlEndIter
        end if ! input%timeScheme
      end if ! solveNS

      ! ... adjoint N-S time marching
      Do main_ts_loop_index = ctrlEndIter-1,region%ctrlStartIter,-1

        if (input%timeScheme == EXPLICIT_RK4) then
          call RK4_AdjNS(region)
        else
          call graceful_exit(region%myrank, "... ERROR: only RK4 available for optimization; how did you get here?")
        end if ! input%timeScheme

        ! ... iteration output
        if (region%myrank == 0) &
             write(*,'(A,I8,3(A,E20.8))') 'PlasComCM: iteration = ', &
             main_ts_loop_index, ', dt = ', region%state(1)%dt(1), &
             ', time = ', region%state(1)%time(1), ', cfl = ', &
             region%state(1)%cfl(1)

        ! ... iteration output to a file
        if (region%optimization%solveNS == TRUE) then
          if (region%myrank == 0) then
             write(ndev_stdout,'(A,I8,3(A,E20.8))') 'PlasComCM: iteration = ', &
             main_ts_loop_index, ', dt = ', region%state(1)%dt(1), &
             ', time = ', region%state(1)%time(1), ', cfl = ', &
             region%state(1)%cfl(1)
          end if ! myrank
        end if ! solveNS

        ! ... update gradient at each actuating location
        if (mod(main_ts_loop_index,input%noutput) == 0) then
          call ComputeGradient(region, region%state(1)%numCur_cvFile)
          call WriteGradient(region, region%state(1)%numCur_cvFile)
        end if ! mod(main_ts_loop_index,input%noutput)

        ! ... output routines
        if (mod(main_ts_loop_index,input%noutput) == 0) then
          call WriteData(region, 'av ')

          if (region%myrank == 0) then
            write(ndev_stdout_optHistory,'(3I7,I20,A)') region%GLOBAL_ITER, region%optimization%curRunningMode, region%optimization%subIter, main_ts_loop_index, '      ADJ'
          end if ! myrank
        end if ! mod(main_ts_loop_index,input%noutput)

        ! ... restart routines
!!$     if ((mod(main_ts_loop_index,input%nrestart) == 0) .or. &
!!$          main_ts_loop_index == input%nstepi) then
!!$       call write_plot3d_file(region, 'rst')
!!$     end if ! mod(main_ts_loop_index,input%nrestart)

      End Do ! main_ts_loop_index
      if (region%optimization%solveNS == TRUE) then
        if (region%myrank == 0) close(ndev_stdout)
      end if ! solveNS

    END SELECT ! input%AdjOptim_typeOfOptim
    call mpi_barrier(mycomm, ierr)



    ! ... Step 3: when called by dbrent, do an extra job and incoming alpha used
    if (PRESENT(alpha)) then
      ControlGradient = ControlGradient_ComputeReturn(region)
    else
      ControlGradient = 0.0_rfreal
    end if ! PRESENT(alpha)
    call mpi_barrier(mycomm, ierr)



    ! ... Step 4: move & store forward & adjoint solutions when N-S equations are solved
    if (region%optimization%solveNS == TRUE) then
      call ControlGradient_MoveSolutionFiles(region)
    end if ! solveNS
    call mpi_barrier(mycomm, ierr)

  contains ! ... subroutines specific only to this function

    subroutine ControlGradient_PreviousControlGradient(region, iFwdSolnFile, dirname)

      USE ModGlobal
      USE ModDataStruct
      USE ModMPI

      Implicit None

      ! ... subroutine arguments
      Type(t_region), Pointer :: region
      Integer :: iFwdSolnFile
      Character(LEN=200) :: dirname

      ! ... local variables and arrays
      Type(t_grid), Pointer :: grid
      Type(t_mixt), Pointer :: state
      Type(t_mixt_input), Pointer :: input
      Type(t_sub_actuator), Pointer :: act
      Integer :: na, ng, np, i, j, l0, idummy, io_err, found, curLine
      Real(rfreal) :: rdummy
      Character(30) :: citer

      ! ... simplicity
      input => region%input

      do j = region%numFwdSolnFiles,iFwdSolnFile,-1  ! ... since it's backward time integration, we're 
                                                     ! ... reading the existing gradient files from 
                                                     ! ... region%numFwdSolnFiles to iFwdSolnFile backwards
        ! ... get the current iteration number
        citer = ' '
        write(citer,'(I8.8)') region%state(1)%iter_cvFiles(j)

        open(ndev_stdout_gradient, file=trim(dirname)//trim(fname_stdout_gradient)//trim(citer), form='unformatted', STATUS = 'OLD')

!!$     ... Since reading the gradient files takes too much time, I did a hack 
!!$     ... assuming restarted optimization has the same number of processors 
!!$     ... all the time.  This assumption is not at all general and should be 
!!$     ... generalized later, JKim 01/2010
!!$     if (region%nSubActuators > 0) then
!!$       do na = 1,region%nSubActuators
!!$         act => region%subActuator(na)
!!$
!!$         do i = 1,act%numPts
!!$           rewind(ndev_stdout_gradient)
!!$           io_err = FALSE; found = FALSE
!!$           do while(io_err == FALSE .AND. found == FALSE)
!!$             read(ndev_stdout_gradient,iostat=io_err) idummy, rdummy
!!$             if (idummy == act%ptID(i)) then
!!$               act%gradient(j,i) = rdummy
!!$               found = TRUE
!!$             end if ! idummy
!!$           end do ! io_err
!!$           if (found == FALSE) & ! ... defensive programming
!!$             call graceful_exit(region%myrank, "... ERROR: something wrong in reading previous gradients")
!!$         end do ! i
!!$       end do ! na
!!$     end if ! region%nSubActuators

!!$
!!$     ! ... This is a hack copied & modified from WriteGradient.
!!$
        curLine = 0 ! ... to put the file-reading in the correct order
        do np = 0,numproc-1
          if (region%myrank == np) then
            if (region%nSubActuators > 0) then
!!$           open(ndev_stdout_gradient, file=trim(fname_stdout_gradient)//trim(citer), form='unformatted', POSITION='APPEND')

              ! ... read the portions of the previous processors
              do i = 1,curLine
                read(ndev_stdout_gradient) idummy, rdummy
              end do ! i

              do na = 1,region%nSubActuators
                act => region%subActuator(na)

                do i = 1,act%numPts
                  read(ndev_stdout_gradient) act%ptID(i), act%gradient(j,i)
                end do ! i

                curLine = curLine + act%numPts
              end do ! na

!!$           close(ndev_stdout_gradient)
            end if ! region%nSubActuators
          end if ! myrank
          call mpi_barrier(mycomm, ierr)

          ! ... now that this processor np read its portion, broadcast curLine.
          call mpi_bcast(curLine,1,MPI_INTEGER,np,mycomm,ierr)
          call mpi_barrier(mycomm, ierr)
        end do ! np
!!$
!!$     ! ... The hack ends here
!!$

        close(ndev_stdout_gradient)
      end do ! j

      call mpi_barrier(mycomm, ierr)

    end subroutine ControlGradient_PreviousControlGradient

    function ControlGradient_ComputeReturn(region)

      USE ModGlobal
      USE ModDataStruct
      USE ModMPI

      Implicit None

      ! ... subroutine arguments
      Type(t_region), Pointer :: region
      Real(rfreal) :: ControlGradient_ComputeReturn

      ! ... local variables and arrays
      Type(t_mixt_input), Pointer :: input
      Type(t_sub_actuator), Pointer :: act
      Integer :: na, i, j
      Real(rfreal) :: df1dim, df1dimP

      ! ... simplicity
      input => region%input

      df1dim  = 0.0_rfreal
      df1dimP = 0.0_rfreal

      if (region%nSubActuators > 0) then
        do na = 1,region%nSubActuators
          act => region%subActuator(na)

          do j = 1,SIZE(act%gradient,2)
            do i = 1,SIZE(act%gradient,1)
              df1dimP = df1dimP + act%gradient(i,j) * act%xi(i,j)
            end do ! i
          end do ! j
        end do ! na
      end if ! region%nSubActuators

      call mpi_allreduce(df1dimP,df1dim,1,MPI_REAL8,MPI_SUM,mycomm,ierr)
      call mpi_bcast(df1dim,1,MPI_REAL8,0,mycomm,ierr)
      ControlGradient_ComputeReturn = df1dim

      call mpi_barrier(mycomm, ierr)

    end function ControlGradient_ComputeReturn

    subroutine ControlGradient_MoveSolutionFiles(region)

      USE ModGlobal
      USE ModDataStruct
      USE ModMPI

      Implicit None

      ! ... subroutine arguments
      Type(t_region), Pointer :: region

      ! ... local variables and arrays
      Character(LEN=200) :: dirname

      ! ... simplicity
      input => region%input

      ! ... move forward solutions
      dirname = CreateDirectory(region, DIR_FWD)
      call MoveSolutionFiles(region, DIR_FWD, dirname)

      ! ... move adjoint solutions
      dirname = CreateDirectory(region, DIR_ADJ)
      call MoveSolutionFiles(region, DIR_ADJ, dirname)

      ! ... if it's called by Brent algorithm, increase subiteration count by 1
      if (region%optimization%curRunningMode == OPTIM_dbrent) region%optimization%subIter = region%optimization%subIter + 1

      call mpi_barrier(mycomm, ierr)

    end subroutine ControlGradient_MoveSolutionFiles

  end function ControlGradient

  ! ... instantaneous cost functional from forward N-S solutions
  subroutine computeInstantaneousCost(region, instCost)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    USE ModEOS

    Implicit None

    ! ... subroutine arguments
    Type(t_region), Pointer :: region
    Real(rfreal) :: instCost

    ! ... local variables and arrays
    Type(t_grid), Pointer :: grid
    Type(t_mixt), Pointer :: state
    Type(t_mixt_input), Pointer :: input
    Type(t_sub_ctrl_target), Pointer :: targ
    Integer :: nt, ng, i, j, l0, ierr
    Real(rfreal) :: dt, dx, spaceDist
    Real(rfreal), Pointer :: ptr_to_var(:)

    ! ... simplicity
    input => region%input

    instCost = 0.0_rfreal ! ... in case this processor doesn't have any target
    if (region%nSubCtrlTarget > 0) then
      do nt = 1,region%nSubCtrlTarget
        targ => region%subCtrlTarget(nt)
        ng = targ%gridID
        grid => region%grid(ng) ! ... grid to this target belongs
        state => region%state(ng) ! ... state to this target belongs

        ! ... depending on cost functional, choose relevant instantaneous flow 
        ! ... variables used to compute cost
        nullify(ptr_to_var)
        if (input%AdjOptim_Func == FUNCTIONAL_SOUND) then
          call computeDv(region%myrank, grid, state)
          ptr_to_var => state%dv(:,1) ! ... static pressure
        else
          call graceful_exit(region%myrank, "... ERROR: unknown cost functional for optimal control")
        end if ! input%AdjOptim_Func

        ! ... mid-point integration with respect to space and time
        ! ... in case target region has a spatial distribution, scaling 
        ! ... function is multiplied as well
        do i = 1,targ%numPts
          l0 = targ%index(i)

          spaceDist = targ%distFunc(i)
          dx = grid%INVJAC(l0)

          ! ... spatial mid-point integration
          instCost = instCost + spaceDist * dx * (ptr_to_var(l0) - targ%meanQ(i))**2
        end do ! i
      end do ! nt
    end if ! region%nSubCtrlTarget
    call mpi_barrier(mycomm, ierr)

  end subroutine computeInstantaneousCost

  subroutine WriteGradient(region, iFwdSolnFile)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    ! ... subroutine arguments
    Type(t_region), Pointer :: region
    Integer :: iFwdSolnFile

    ! ... local variables and arrays
    Type(t_grid), Pointer :: grid
    Type(t_mixt), Pointer :: state
    Type(t_mixt_input), Pointer :: input
    Type(t_sub_actuator), Pointer :: act
    Integer :: na, ng, np, i, j, l0, ierr
    Character(30) :: citer

    ! ... simplicity
    input => region%input

    ! ... get the current iteration number
    citer = ' '
    write(citer,'(I8.8)') region%state(1)%iter_cvFiles(iFwdSolnFile)

    do np = 0,numproc-1
      if (region%myrank == np) then
        if (region%nSubActuators > 0) then
          open(ndev_stdout_gradient, file=trim(fname_stdout_gradient)//trim(citer), form='unformatted', POSITION='APPEND')
          do na = 1,region%nSubActuators
            act => region%subActuator(na)

            do i = 1,act%numPts
              write(ndev_stdout_gradient) act%ptID(i), act%gradient(iFwdSolnFile,i)
            end do ! i
          end do ! na
          close(ndev_stdout_gradient)
        end if ! region%nSubActuators
      end if ! myrank
      call mpi_barrier(mycomm, ierr)
    end do ! np

    call mpi_barrier(mycomm, ierr)

  end subroutine WriteGradient

  ! ... forward and adjoint N-S solutions at certain time combined to give 
  ! ... gradient to control input
  subroutine ComputeGradient(region, iFwdSolnFile)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    USE ModActuator

    Implicit None

    ! ... subroutine arguments
    Type(t_region), Pointer :: region
    Integer :: iFwdSolnFile

    ! ... local variables and arrays
    Type(t_grid), Pointer :: grid
    Type(t_mixt), Pointer :: state
    Type(t_mixt_input), Pointer :: input
    Type(t_sub_actuator), Pointer :: act
    Integer :: na, ng, i, j, l0, ind2D(2), ierr
    Real(rfreal) :: x0, y0, lGrad, gGrad, lVol, gVol, theta, fac2D(2)
    Real(rfreal) :: spaceDist, tempDist, vol_assoc, grad_space_constrained

    ! ... simplicity
    input => region%input

    if (region%nSubActuators > 0) then
      do na = 1,region%nSubActuators
        act => region%subActuator(na)
        ng = act%gridID
        grid => region%grid(ng)
        state => region%state(ng)

        SELECT CASE( input%AdjOptim_Func )
        CASE( FUNCTIONAL_SOUND )

          SELECT CASE( input%AdjOptim_CtrlType )
          CASE( CONTROL_MASS )
            do i = 1,act%numPts
              l0 = act%index(i)

              act%gradient(iFwdSolnFile,i) = state%av(l0,1) + &
                                             state%av(l0,grid%ND+2)/((state%gv(l0,1) - 1.0_rfreal)*state%gv(l0,1))
            end do ! i

          CASE( CONTROL_BODY_FORCE_X:CONTROL_BODY_FORCE_Z )
            do i = 1,act%numPts
              l0 = act%index(i)

              act%gradient(iFwdSolnFile,i) = state%av(l0,input%AdjOptim_CtrlType) + &
                                             state%cv(l0,input%AdjOptim_CtrlType)/state%cv(l0,1) * state%av(l0,grid%ND+2)
            end do ! i

          CASE( CONTROL_BODY_FORCE_XY:CONTROL_BODY_FORCE_ZX )
            ind2D(:) = 0
            if (input%AdjOptim_CtrlType == CONTROL_BODY_FORCE_XY) then
              ind2D(1) = 1; ind2D(2) = 2
            else if (input%AdjOptim_CtrlType == CONTROL_BODY_FORCE_YZ) then
              ind2D(1) = 2; ind2D(2) = 3
            else if (input%AdjOptim_CtrlType == CONTROL_BODY_FORCE_ZX) then
              ind2D(1) = 3; ind2D(2) = 1
            end if ! input%AdjOptim_CtrlType
            ind2D(:) = ind2D(:) + 1

            do i = 1,act%numPts
              l0 = act%index(i)

              act%gradient(iFwdSolnFile,i) = 0.0_rfreal
              do j = 1,2
                act%gradient(iFwdSolnFile,i) = act%gradient(iFwdSolnFile,i) + &
                                               state%av(l0,ind2D(j)) + &
                                               state%cv(l0,ind2D(j))*state%dv(l0,3)*state%av(l0,grid%ND+2)
              end do ! j
            end do ! i

          CASE( CONTROL_BODY_FORCE_XYZ )
            do i = 1,act%numPts
              l0 = act%index(i)

              act%gradient(iFwdSolnFile,i) = 0.0_rfreal
              do j = 1,3 ! ... forcing in every direction; assumes 3-D
                act%gradient(iFwdSolnFile,i) = act%gradient(iFwdSolnFile,i) + &
                                               state%av(l0,j+1) + &
                                               state%cv(l0,j+1)*state%dv(l0,3)*state%av(l0,5)
              end do ! j
            end do ! i

          CASE( CONTROL_BODY_FORCE_R )
            ind2D(:) = 0
            if (input%AdjOptim_CtrlType == CONTROL_BODY_FORCE_XY) then
              ind2D(1) = 1; ind2D(2) = 2
            else if (input%AdjOptim_CtrlType == CONTROL_BODY_FORCE_YZ) then
              ind2D(1) = 2; ind2D(2) = 3
            else if (input%AdjOptim_CtrlType == CONTROL_BODY_FORCE_ZX) then
              ind2D(1) = 3; ind2D(2) = 1
            end if ! input%AdjOptim_CtrlType
            ind2D(:) = ind2D(:) + 1

            do i = 1,act%numPts
              l0 = act%index(i)

              theta = ATAN2(grid%xyz(l0,ind2D(2)-1), grid%xyz(l0,ind2D(1)-1))
              fac2D(1) =  COS(theta)
              fac2D(2) = -SIN(theta)

              act%gradient(iFwdSolnFile,i) = 0.0_rfreal
              do j = 1,2
                act%gradient(iFwdSolnFile,i) = act%gradient(iFwdSolnFile,i) + &
                      (state%av(l0,ind2D(j)) + state%cv(l0,ind2D(j))*state%dv(l0,3)*state%av(l0,grid%ND+2)) * fac2D(j)
              end do ! j
            end do ! i

          CASE( CONTROL_BODY_FORCE_THETA )
            ind2D(:) = 0
            if (input%AdjOptim_CtrlType == CONTROL_BODY_FORCE_XY) then
              ind2D(1) = 1; ind2D(2) = 2
            else if (input%AdjOptim_CtrlType == CONTROL_BODY_FORCE_YZ) then
              ind2D(1) = 2; ind2D(2) = 3
            else if (input%AdjOptim_CtrlType == CONTROL_BODY_FORCE_ZX) then
              ind2D(1) = 3; ind2D(2) = 1
            end if ! input%AdjOptim_CtrlType
            ind2D(:) = ind2D(:) + 1

            do i = 1,act%numPts
              l0 = act%index(i)

              theta = ATAN2(grid%xyz(l0,ind2D(2)-1), grid%xyz(l0,ind2D(1)-1))
              fac2D(1) =  SIN(theta)
              fac2D(2) =  COS(theta)

              act%gradient(iFwdSolnFile,i) = 0.0_rfreal
              do j = 1,2
                act%gradient(iFwdSolnFile,i) = act%gradient(iFwdSolnFile,i) + &
                      (state%av(l0,ind2D(j)) + state%cv(l0,ind2D(j))*state%dv(l0,3)*state%av(l0,grid%ND+2)) * fac2D(j)
              end do ! j
            end do ! i

          CASE( CONTROL_INTERNAL_ENERGY )
            do i = 1,act%numPts
              l0 = act%index(i)

              act%gradient(iFwdSolnFile,i) = state%av(l0,grid%ND+2)
            end do ! i

          CASE DEFAULT

          END SELECT ! input%AdjOptim_CtrlType

        CASE DEFAULT

        END SELECT ! input%AdjOptim_Func
      end do ! na
    end if ! region%nSubActuators
    call mpi_barrier(mycomm, ierr)

!!$    SELECT CASE( input%AdjOptim_typeOfOptim )
!!$    CASE( 3 ) ! ... 2-D anti-sound cancellation controller
!!$              ! ... since we have only time as a free parameter, set all gradient 
!!$              ! ... values same as some constant
!!$      x0 = 3.0_rfreal
!!$      y0 = 0.0_rfreal
!!$      lGrad = 0.0_rfreal ! ... gradient (or weighted sum of gradient) local to this processor
!!$      lVol  = 0.0_rfreal ! ... volume belonging to the set of actuators in this processor
!!$      gGrad = 0.0_rfreal ! ... gradient (or weighted sum of gradient) over the entire processors
!!$      gVol  = 0.0_rfreal ! ... volume of the actuator as a whole
!!$      if (region%nSubActuators > 0) then
!!$        do na = 1,region%nSubActuators
!!$          do i = 1,act%numPts
!!$            l0 = act%index(i)
!!$
!!$            ! ... take only value at the controller center
!!$            if (ABS(grid%xyz(l0,1) - x0) < 1E6*TINY .AND. &
!!$                ABS(grid%xyz(l0,2) - y0) < 1E6*TINY) then
!!$              lGrad = act%gradient(iFwdSolnFile,i)
!!$            end if ! ABS(grid%xyz(l0,1) - x0)
!!$            ! ... average over controller region
!!$!!$         lGrad = lGrad + act%gradient(iFwdSolnFile,i)*grid%INVJAC(l0)*act%distFunc(i)
!!$!!$         lVol  = lVol  + grid%INVJAC(l0)*act%distFunc(i)
!!$          end do ! i
!!$        end do ! na
!!$      end if ! region%nSubActuators
!!$      call mpi_allreduce( lGrad, gGrad,1,MPI_REAL8,MPI_SUM,mycomm,ierr)
!!$!!$   call mpi_allreduce( lVol, gVol,1,MPI_REAL8,MPI_SUM,mycomm,ierr)
!!$      if (region%nSubActuators > 0) then
!!$        do na = 1,region%nSubActuators
!!$          do i = 1,act%numPts
!!$            l0 = act%index(i)
!!$
!!$            act%gradient(iFwdSolnFile,i) = gGrad * act%distFunc(i)
!!$!!$         act%gradient(iFwdSolnFile,i) = gGrad / gVol
!!$          end do ! i
!!$        end do ! na
!!$      end if ! region%nSubActuators
!!$
!!$    CASE DEFAULT
    ! ... depending on a type of problem, restrict the space-time profile of gradient
    if (region%nSubActuators > 0) then
      do na = 1,region%nSubActuators
        act => region%subActuator(na)
        ng = act%gridID
        state => region%state(ng)

        tempDist = temporalConstraintOnControl(state%time(1), region%ctrlStartTime, region%ctrlEndTime)

        do i = 1,act%numPts
          l0 = act%index(i)

          spaceDist = act%distFunc(i)
          act%gradient(iFwdSolnFile,i) = act%gradient(iFwdSolnFile,i) * spaceDist * tempDist
        end do ! i
      end do ! na
    end if ! region%nSubActuators
    call mpi_barrier(mycomm, ierr)

    ! ... for a space-constrained optimization, average over a controller region
    if (input%AdjOptim_Constraint == CONTROL_CONSTRAINED_SPACE) then
      lGrad = 0.0_rfreal ! ... gradient (or weighted sum of gradient) local to this processor
      lVol  = 0.0_rfreal ! ... volume belonging to the set of actuators in this processor
      gGrad = 0.0_rfreal ! ... gradient (or weighted sum of gradient) over the entire processors
      gVol  = 0.0_rfreal ! ... volume of the actuator as a whole
      if (region%nSubActuators > 0) then
        do na = 1,region%nSubActuators
          do i = 1,act%numPts
            l0 = act%index(i)
            vol_assoc = grid%INVJAC(l0)

            ! ... sum over controller region
            lGrad = lGrad + act%gradient(iFwdSolnFile,i) * vol_assoc
            lVol  = lVol  + vol_assoc
          end do ! i
        end do ! na
      end if ! region%nSubActuators
      call mpi_allreduce(lGrad, gGrad,1,MPI_REAL8,MPI_SUM,mycomm,ierr)
      call mpi_allreduce(lVol,  gVol, 1,MPI_REAL8,MPI_SUM,mycomm,ierr)
      grad_space_constrained = gGrad / gVol
      if (region%nSubActuators > 0) then
        do na = 1,region%nSubActuators
          do i = 1,act%numPts
            l0 = act%index(i)

            act%gradient(iFwdSolnFile,i) = grad_space_constrained
          end do ! i
        end do ! na
      end if ! region%nSubActuators
      call mpi_barrier(mycomm, ierr)
    end if ! input%AdjOptim_Constraint

  end subroutine ComputeGradient

  subroutine GetMeanAtControlTarget(region)

    USE ModGlobal
    USE ModDataStruct
    USE ModIO
    USE ModMPI

    Implicit None

    ! ... subroutine arguments
    Type(t_region), Pointer :: region

    ! ... local variables and arrays
    Type(t_grid), Pointer :: grid
    Type(t_mixt), Pointer :: state, mean
    Type(t_mixt_input), Pointer :: input
    Type(t_sub_ctrl_target), Pointer :: targ
    Integer :: ng, nt, i, j, l0, ierr
    logical :: mf_exists

    ! ... simplicity
    input => region%input

    SELECT CASE( input%AdjOptim_Func )
    CASE( FUNCTIONAL_SOUND )
      ! ... see if the mean file exists
      inquire(file=trim(input%AdjOptim_mean), exist=mf_exists)
      if (.not. mf_exists) call graceful_exit(region%myrank, "... ERROR: mean data for sound cost functional not found")

      ! ... ad-hoc placement of routines allocating mean data
      do ng = 1,region%nGrids
        grid => region%grid(ng)
        mean => region%mean(ng)

        nullify(mean%cv); allocate(mean%cv(grid%nCells,input%nCv)); mean%cv = 0.0_rfreal
      end do ! ng
      call mpi_barrier(mycomm, ierr)

      ! ... read a converged time-average in the entire domain
      call ReadMean(region, input%AdjOptim_mean)
      call mpi_barrier(mycomm, ierr)

      ! ... assuming the mean file has conserved variables, compute primitive variables
      SELECT CASE( input%AdjOptim_typeOfOptim )
      CASE( 3:4 ) ! ... anti-sound problem
        do ng = 1,region%nGrids
          grid  => region%grid(ng)
          state => region%state(ng)
          mean  => region%mean(ng)

         !mean%cv(:,1) = mean%cv(:,1) ! ... density is density
          do i = 1,grid%ND
            mean%cv(:,i+1) = mean%cv(:,i+1) / mean%cv(:,1) ! ... primitive velocity
            mean%cv(:,grid%ND+2) = mean%cv(:,grid%ND+2) - 0.5_rfreal * mean%cv(:,1) * mean%cv(:,i+1)**2
          end do ! i
          mean%cv(:,grid%ND+2) = mean%cv(:,grid%ND+2) * (state%gv(:,1) - 1.0_rfreal) ! ... pressure
        end do ! ng

      CASE( 6 ) ! ... control optimization of NASA M = 1.3 jet
        do ng = 1,region%nGrids
          grid  => region%grid(ng)
          state => region%state(ng)
          mean  => region%mean(ng)

          mean%cv(:,grid%ND+2) = mean%cv(:,2) ! ... my rule of writing time average: {rho, p, T, mu, dummy}^T
        end do ! ng

      CASE DEFAULT
        call graceful_exit(region%myrank, "... ERROR: unknown optimization in defining time mean.")

      END SELECT ! input%AdjOptim_typeOfOptim
      call mpi_barrier(mycomm, ierr)

      ! ... need time-averaged pressure at control target region
      if (region%nSubCtrlTarget > 0) then
        do nt = 1,region%nSubCtrlTarget
          targ => region%subCtrlTarget(nt)
          ng = targ%gridID
          grid => region%grid(ng) ! ... grid to this target belongs
          state => region%state(ng) ! ... state to this target belongs

          ! ... compute time-averaged pressure BEFORE control is applied
          ! ... state%cvTarget assumed to contain mean flow field
          do i = 1,targ%numPts
            l0 = targ%index(i)

!!$         targ%meanQ(i) = state%cvTarget(l0,grid%ND+2)
!!$         do j = 1,grid%ND
!!$           targ%meanQ(i) = targ%meanQ(i) - 0.5_rfreal*(state%cvTarget(l0,j+1)**2)/state%cvTarget(l0,1)
!!$         end do ! j
!!$         targ%meanQ(i) = targ%meanQ(i) * (state%gv(l0,1) - 1.0_rfreal)

            targ%meanQ(i) = mean%cv(l0,grid%ND+2)
          end do ! i

        end do ! nt
      end if ! region%nSubCtrlTarget

    CASE DEFAULT
      call graceful_exit(region%myrank, "... ERROR: unknown cost functional for optimal control")

    END SELECT ! input%AdjOptim_Func
    region%optimization%getMean = FALSE ! ... mean is retrieved only once
    call mpi_barrier(mycomm, ierr)

  end subroutine GetMeanAtControlTarget

  function CreateDirectory(region, whichDir)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    ! ... subroutine arguments
    Type(t_region), Pointer :: region
    Integer :: whichDir, ierr
    Character(LEN=200) :: CreateDirectory

    ! ... local variables and arrays
    Type(t_mixt_input), Pointer :: input
    Character(200) :: command
    Character(30) :: citer
    Character(LEN=200) :: dirname

    ! ... simplicity
    input => region%input

    if (region%optimization%solveNS == FALSE) & ! ... defensive programming
      call graceful_exit(region%myrank, "... ERROR: files are saved only when N-S equations are solved")

    write(citer,'(I3.3)') region%GLOBAL_ITER

    SELECT CASE( region%optimization%curRunningMode )
    CASE( OPTIM_IC ) ! ... IC for conjugate gradient: no iteration count appears
      dirname = runningMode(region%optimization%curRunningMode)

      if (whichDir == DIR_ROOT) then

      else if (whichDir == DIR_FWD) then
        dirname = TRIM(dirname)//'/'//'FwdNS'

      else if (whichDir == DIR_ADJ) then
        dirname = TRIM(dirname)//'/'//'AdjNS'

      end if ! whichDir

    CASE( OPTIM_mnbrak ) ! ... 3-pt bracketing: only forward solvers are called
      dirname = 'iter'//TRIM(citer)//'/'//TRIM(runningMode(region%optimization%curRunningMode))

      if (whichDir == DIR_ROOT) then

      else if (whichDir == DIR_FWD) then
        write(citer,'(I3.3)') region%optimization%subIter
        dirname = TRIM(dirname)//'/'//'FwdNS-'//TRIM(citer)//TRIM(region%optimization%mnbrak_suffix)

      end if ! whichDir

    CASE( OPTIM_dbrent ) ! ... Brent's algorithm to refine local minimum searching
      dirname = 'iter'//TRIM(citer)//'/'//TRIM(runningMode(region%optimization%curRunningMode))

      if (whichDir == DIR_ROOT) then

      else if (whichDir == DIR_FWD) then
        write(citer,'(I3.3)') region%optimization%subIter
        dirname = TRIM(dirname)//'/'//'FwdNS-'//TRIM(citer)

      else if (whichDir == DIR_ADJ) then
        write(citer,'(I3.3)') region%optimization%subIter
        dirname = TRIM(dirname)//'/'//'AdjNS-'//TRIM(citer)

      end if ! whichDir

    CASE( OPTIM_optimum ) ! ... forward & adjoint runs with locally optimal control
      dirname = 'iter'//TRIM(citer)//'/'//TRIM(runningMode(region%optimization%curRunningMode))

      if (whichDir == DIR_ROOT) then

      else if (whichDir == DIR_FWD) then
        dirname = TRIM(dirname)//'/'//'FwdNS'

      else if (whichDir == DIR_ADJ) then
        dirname = TRIM(dirname)//'/'//'AdjNS'

      end if ! whichDir

    CASE( OPTIM_frprmn ) ! ... root directory for this iteration
      dirname = 'iter'//TRIM(citer)

    END SELECT ! curRunningMode

    ! ... actually create an directory
    if (region%myrank == 0) then
      command = 'mkdir '//TRIM(dirname)
      call SYSTEM(TRIM(command))
    end if ! myrank

    ! ... return name of the directory
    CreateDirectory = TRIM(dirname)

    call mpi_barrier(mycomm, ierr)

  end function CreateDirectory

  subroutine MoveSolutionFiles(region, whichDir, dirname)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    ! ... subroutine arguments
    Type(t_region), Pointer :: region
    Integer :: whichDir, ierr
    Character(LEN=200) :: dirname

    ! ... local variables and arrays
    Type(t_mixt_input), Pointer :: input
    Character(200) :: command

    ! ... simplicity
    input => region%input

    if (region%optimization%solveNS == FALSE) & ! ... defensive programming
      call graceful_exit(region%myrank, "... ERROR: files are saved only when N-S equations are solved")

    call mpi_barrier(mycomm, ierr)

    if (region%myrank == 0) then
      SELECT CASE( whichDir )
      CASE( DIR_FWD ) ! ... forward solution files to forward solution directory
        command = 'mv RocFlo-CM.????????.q '//TRIM(dirname)
        call SYSTEM(TRIM(command))
        command = 'mv '//TRIM(fname_stdout_fwd)//' '//TRIM(dirname)
        call SYSTEM(TRIM(command))
        command = 'mv '//TRIM(fname_stdout_J_vs_time)//' '//TRIM(dirname)
        call SYSTEM(TRIM(command))
        if (region%optimization%curRunningMode /= OPTIM_IC) then
          command = 'mv RocFlo-CM.????????.ctrl.q '//TRIM(dirname)
          call SYSTEM(TRIM(command))
        end if ! curRunningMode
        if (input%GetTrace) then
          command = 'mv probe.*.dat '//TRIM(dirname)
          call SYSTEM(TRIM(command))
        end if ! input%GetTrace

      CASE( DIR_ADJ ) ! ... adjoint solution files to adjoint solution directory
        command = 'mv RocFlo-CM.????????.av.q '//TRIM(dirname)
        call SYSTEM(TRIM(command))
        command = 'mv '//TRIM(fname_stdout_adj)//' '//TRIM(dirname)
        call SYSTEM(TRIM(command))
        command = 'mv '//TRIM(fname_stdout_gradient)//'* '//TRIM(dirname)
        call SYSTEM(TRIM(command))

      END SELECT ! whichDir

    end if ! myrank

    call mpi_barrier(mycomm, ierr)

  end subroutine MoveSolutionFiles

  subroutine WriteCGVectors(region, niter)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    ! ... subroutine arguments
    Type(t_region), Pointer :: region
    Integer :: niter

    ! ... local variables and arrays
    Type(t_grid), Pointer :: grid
    Type(t_mixt), Pointer :: state
    Type(t_mixt_input), Pointer :: input
    Type(t_sub_actuator), Pointer :: act
    Integer :: na, ng, np, i, j, l0, iFwdSolnFile, ierr
    Character(30) :: citer

    ! ... simplicity
    input => region%input

    citer = ' '
    write(citer,'(I3.3)') niter

    do iFwdSolnFile = 1,region%numFwdSolnFiles
      ! ... write, by only one processor, forward solution file index
      if (region%myrank == 0) then
        open(ndev_stdout_CGvectors, file=TRIM(fname_stdout_CGvectors)//TRIM(citer), form='unformatted', POSITION='APPEND')
        write(ndev_stdout_CGvectors) iFwdSolnFile
        close(ndev_stdout_CGvectors)
      end if ! myrank
      call mpi_barrier(mycomm, ierr)

      do np = 0,numproc-1
        if (region%myrank == np) then
          if (region%nSubActuators > 0) then
            open(ndev_stdout_CGvectors, file=TRIM(fname_stdout_CGvectors)//TRIM(citer), form='unformatted', POSITION='APPEND')
            do na = 1,region%nSubActuators
              act => region%subActuator(na)

              do i = 1,act%numPts
                write(ndev_stdout_CGvectors) act%ptID(i), act%g(iFwdSolnFile,i), &
                                                          act%h(iFwdSolnFile,i), &
                                                          act%xi(iFwdSolnFile,i), &
                                                          act%gPhiCtrl(iFwdSolnFile,i)
              end do ! i
            end do ! na
            close(ndev_stdout_CGvectors)
          end if ! region%nSubActuators
        end if ! myrank
        call mpi_barrier(mycomm, ierr)
      end do ! np

      ! ... add a tag indicating the end-of-record by '-1'
      if (region%myrank == 0) then
        open(ndev_stdout_CGvectors, file=TRIM(fname_stdout_CGvectors)//TRIM(citer), form='unformatted', POSITION='APPEND')
        write(ndev_stdout_CGvectors) -1, 0.0_rfreal, 0.0_rfreal, 0.0_rfreal, 0.0_rfreal
        close(ndev_stdout_CGvectors)
      end if ! myrank
      call mpi_barrier(mycomm, ierr)
    end do ! iFwdSolnFile

    call mpi_barrier(mycomm, ierr)

  end subroutine WriteCGVectors

  subroutine ReadCGVectors(region, niter)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    ! ... subroutine arguments
    Type(t_region), Pointer :: region
    Integer :: niter

    ! ... local variables and arrays
    Type(t_grid), Pointer :: grid
    Type(t_mixt), Pointer :: state
    Type(t_mixt_input), Pointer :: input
    Type(t_sub_actuator), Pointer :: act
    Integer :: na, ng, np, i, j, l0, io_err, found, nfound, iFwdSolnFile, idum1, actPtID, nMoreLines, ierr
    Real(rfreal) :: rdum1, rdum2, rdum3, rdum4, rdum5, rdum6
    Character(30) :: citer

    ! ... simplicity
    input => region%input

    ! ... CG vector file from the last run
    citer = ' '
    write(citer,'(I3.3)') niter
    open(ndev_stdout_CGvectors, file=TRIM(fname_stdout_CGvectors)//TRIM(citer), form='unformatted')

!!$ ... Since reading the CG vector files takes too much time, I did a hack 
!!$ ... assuming restarted optimization has the same number of processors 
!!$ ... all the time.  This assumption is not at all general and should be 
!!$ ... generalized later, JKim 01/2010
!!$ ! ... read files at once by every processor
!!$ if (region%nSubActuators > 0) then
!!$   do na = 1,region%nSubActuators
!!$     act => region%subActuator(na)
!!$
!!$     do i = 1,act%numPts
!!$       actPtID = act%ptID(i)
!!$       rewind(ndev_stdout_CGvectors); nfound = 0
!!$
!!$       do iFwdSolnFile = 1,region%numFwdSolnFiles
!!$         ! ... STEP 1: read each header
!!$         read(ndev_stdout_CGvectors) idum1
!!$         if (idum1 /= iFwdSolnFile) &
!!$           call graceful_exit(region%myrank, "... ERROR: forward solution index wrong in CG-vector files.")
!!$
!!$         ! ... STEP 2: read until we find a right index
!!$         io_err = FALSE; found = FALSE
!!$         do while(io_err == FALSE .AND. found == FALSE)
!!$           read(ndev_stdout_CGvectors,iostat=io_err) idum1, rdum1, rdum2, rdum3, rdum4
!!$
!!$           if (idum1 == actPtID) then
!!$             act%g(iFwdSolnFile,i) = rdum1
!!$             act%h(iFwdSolnFile,i) = rdum2
!!$             act%xi(iFwdSolnFile,i) = rdum3
!!$             act%gPhiCtrl(iFwdSolnFile,i) = rdum4
!!$             found = TRUE
!!$             nfound = nfound + 1
!!$           else if (idum1 == -1) then ! ... defensive programming
!!$             call graceful_exit(region%myrank, "... ERROR: End-of-record mis-specified")
!!$           end if ! idum1
!!$
!!$         end do ! io_err
!!$
!!$         ! ... STEP 3: proceed to the end-of-record for this iFwdSolnFile
!!$         io_err = FALSE; found = FALSE
!!$         do while(io_err == FALSE .AND. found == FALSE)
!!$           read(ndev_stdout_CGvectors,iostat=io_err) idum1, rdum1, rdum2, rdum3, rdum4
!!$
!!$           if (idum1 == -1) found = TRUE
!!$         end do ! io_err
!!$       end do ! iFwdSolnFile
!!$
!!$       ! ... STEP 4: final consistency check
!!$       if (nfound /= region%numFwdSolnFiles .OR. found == FALSE) &
!!$         call graceful_exit(region%myrank, "... ERROR: something wrong in finding data in CG vector.")
!!$     end do ! i
!!$   end do ! na
!!$ end if ! region%nSubActuators

!!$
!!$ ! ... This is a hack copied & modified from WriteCGVectors.
!!$
    do iFwdSolnFile = 1,region%numFwdSolnFiles
!!$   ! ... write, by only one processor, forward solution file index
!!$   if (region%myrank == 0) then
!!$     open(ndev_stdout_CGvectors, file=TRIM(fname_stdout_CGvectors)//TRIM(citer), form='unformatted', POSITION='APPEND')
!!$     write(ndev_stdout_CGvectors) iFwdSolnFile
!!$     close(ndev_stdout_CGvectors)
!!$   end if ! myrank
      read(ndev_stdout_CGvectors) idum1
      if (idum1 /= iFwdSolnFile) &
        call graceful_exit(region%myrank, "... ERROR: forward solution index wrong in CG-vector files.")
      call mpi_barrier(mycomm, ierr)

      do np = 0,numproc-1
        ! ... how many more lines to read due to this processor, np?
        nMoreLines = 0
        if (region%myrank == np) then
          if (region%nSubActuators > 0) then
            do na = 1,region%nSubActuators
              act => region%subActuator(na)

              nMoreLines = nMoreLines + act%numPts ! ... other processors need to read this MORE lines
            end do ! na
          end if ! region%nSubActuators
        end if ! myrank
        call mpi_barrier(mycomm, ierr)

        call mpi_bcast(nMoreLines,1,MPI_INTEGER,np,mycomm,ierr)
        call mpi_barrier(mycomm, ierr)

        if (region%myrank == np) then
          if (region%nSubActuators > 0) then
!!$         open(ndev_stdout_CGvectors, file=TRIM(fname_stdout_CGvectors)//TRIM(citer), form='unformatted', POSITION='APPEND')
            do na = 1,region%nSubActuators
              act => region%subActuator(na)

              do i = 1,act%numPts
                read(ndev_stdout_CGvectors) act%ptID(i), act%g(iFwdSolnFile,i), &
                                                         act%h(iFwdSolnFile,i), &
                                                         act%xi(iFwdSolnFile,i), &
                                                         act%gPhiCtrl(iFwdSolnFile,i)
              end do ! i
            end do ! na
!!$         close(ndev_stdout_CGvectors)
          end if ! region%nSubActuators

        else
          do i = 1,nMoreLines
            read(ndev_stdout_CGvectors) idum1, rdum1, rdum2, rdum3, rdum4
          end do ! i

        end if ! myrank
        call mpi_barrier(mycomm, ierr)
      end do ! np

!!$   ! ... add a tag indicating the end-of-record by '-1'
!!$   if (region%myrank == 0) then
!!$     open(ndev_stdout_CGvectors, file=TRIM(fname_stdout_CGvectors)//TRIM(citer), form='unformatted', POSITION='APPEND')
!!$     write(ndev_stdout_CGvectors) -1, 0.0_rfreal, 0.0_rfreal, 0.0_rfreal, 0.0_rfreal
!!$     close(ndev_stdout_CGvectors)
!!$   end if ! myrank
      read(ndev_stdout_CGvectors) idum1, rdum1, rdum2, rdum3, rdum4
      if (idum1 /= -1) call graceful_exit(region%myrank, "... ERROR: EOF of CG vectors not identified.")

      call mpi_barrier(mycomm, ierr)
    end do ! iFwdSolnFile
!!$
!!$ ! ... The hack ends here
!!$

    close(ndev_stdout_CGvectors)
    call mpi_barrier(mycomm, ierr)

  end subroutine ReadCGVectors

END MODULE ModOptimization
