!>  
!> @file ModInput.fpp
!>\defgroup inputVariables User Input Variables
!>
!
! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
!-----------------------------------------------------------------------
!
! ModInput.f90
!
! - read the input file
!
! Revision history
! - 20 Dec 2006 : DJB : initial code
! - 17 May 2007 : DJB : evolution (added gridType & metricType)
! - 10 Jul 2007 : DJB : initial CVS commit
! - 29 Oct 2008 : DJB : conversion to operators
!
! $Header: /cvsroot/genx/Codes/RocfloCM/Source/ModInput.f90,v 1.56 2011/11/05 11:11:43 bodony Exp $
!
!-----------------------------------------------------------------------
MODULE ModInput

CONTAINS

  subroutine read_input(region, input, input_fname, init_file)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    USE ModParam
    USE ModString

    IMPLICIT NONE 

    TYPE(t_region), pointer :: region
    TYPE(t_mixt_input), pointer :: input
    REAL(rfreal) :: L1, L2, L3, T4
    character(len=80) :: str, error_message
    integer :: i, j, disableIO
    character(len=*) :: input_fname
    logical :: init_file
    type(t_param), pointer :: first_param_ptr
    logical :: includeChemistryOld

    ! allocate first_param_ptr 
    if (init_file) then
      allocate(region%first_param_ptr)
      region%first_param_ptr%recursion_level = 0
      region%first_param_ptr%line = char(0)
      region%first_param_ptr%argc = 0
      region%first_param_ptr%request_count = 0
      NULLIFY(region%first_param_ptr%argv)
      region%first_param_ptr%lineInit = .false.
      region%first_param_ptr%nextInit = .false.
      NULLIFY(region%first_param_ptr%next)
    end if

    first_param_ptr => region%first_param_ptr

    ! ... set up the input linked list
    if (init_file) call init_param(region%myrank, first_param_ptr, input_fname)

    ! ... read case id
    !> some comment here
    !> \var CASE_ID 
    !> DEFAULT NULL
    !> DESCRIPTION ???
    call set_param(region%myrank,input%caseID,'CASE_ID',first_param_ptr,default='NULL')
    CALL Set_Param(region%myrank,input%outputDirectory,'OUTPUT_DIRECTORY',first_param_ptr,default='NULL')

    ! ... fluid model
    input%fluidModel = get_integer_param(region%myrank,'FLUID_MODEL',first_param_ptr,default=CNS)

    ! ... timeScheme = IMPLICIT (implicit time advancement, not supported)
    ! ... timeScheme = EXPLICIT_RK4 (explicit time advancement, e.g., RK4)
    ! ... timeScheme = EXPLICIT_RK5 (explicit time advancement, e.g., RK5)
    input%timeScheme = get_integer_param(region%myrank,'TIMESCHEME',first_param_ptr,default=EXPLICIT_RK4)

    ! ... read approximate linear operator epsilon
    input%approximate_linear_operator_epsilon = get_real_param(region%myrank,'APPROXIMATE_LINEAR_OPERATOR_EPSILON',first_param_ptr,default=1d-7)

    ! ... read approximate linear power method steps
    input%approximate_linear_operator_power_method_steps = get_integer_param(region%myrank,'APPROXIMATE_LINEAR_OPERATOR_POWER_METHOD_STEPS',first_param_ptr,default=1000) 

    ! ... read selective frequency damping
    input%SelectiveFrequencyDamping = get_integer_param(region%myrank,'SELECTIVE_FREQUENCY_DAMPING',first_param_ptr,default=FALSE)
    input%SFD_X = get_real_param(region%myrank,'SELECTIVE_FREQUENCY_DAMPING_CHI',first_param_ptr,default=0.0_8)
    input%SFD_D = get_real_param(region%myrank,'SELECTIVE_FREQUENCY_DAMPING_DELTA',first_param_ptr,default=1.0_8)

    ! ... low Mach number preconditioner
    input%USE_LOCAL_LOW_MACH_PRECONDITIONER = get_integer_param(region%myrank,'USE_LOCAL_LOW_MACH_PRECONDITIONER',first_param_ptr,default=FALSE)
    input%LOCAL_LOW_MACH_PRECONDITIONER_PARAM_ALPHA = get_real_param(region%myrank,'LOCAL_LOW_MACH_PRECONDITIONER_PARAM_ALPHA',first_param_ptr,default=1.0_8)

    ! ... output the infinity-norm of the residual
    input%RESIDUAL_INF_NORM_CALCULATE = get_integer_param(region%myrank,'RESIDUAL_INF_NORM_CALCULATE',first_param_ptr,default=FALSE)
    input%RESIDUAL_INF_NORM_OUTPUT_FILE = get_integer_param(region%myrank,'RESIDUAL_INF_NORM_OUTPUT_FILE',first_param_ptr,default=FALSE)
    input%RESIDUAL_INF_NORM_STOP = get_integer_param(region%myrank,'RESIDUAL_INF_NORM_STOP',first_param_ptr,default=FALSE)
    call set_param(region%myrank,input%RESIDUAL_INF_NORM_OUTPUT_FILENAME,'RESIDUAL_INF_NORM_OUTPUT_FILENAME',first_param_ptr,default='residual-inf-norm.dat')

    ! ... maxsubits = number of sub-iterations needed if timeScheme = IMPLICIT
    input%maxsubits = get_integer_param(region%myrank,'MAXSUBITS',first_param_ptr,default=1)

    ! ... nstepi = initial iteration number.  over-written if using restart
    input%nstepi = get_integer_param(region%myrank,'NSTEPI',first_param_ptr,default=0)

    ! ... nstepmax = number of iterations to take
    input%nstepmax = get_integer_param(region%myrank,'NSTEPMAX',first_param_ptr,default=101)

    ! ... noutput = output a solution file when iteration modulo noutput = 0
    input%noutput = get_integer_param(region%myrank,'NOUTPUT',first_param_ptr,default=0)

    ! ... nrestart = output a restart file when iteration modulo nrestart = 0
    input%nrestart = get_integer_param(region%myrank,'NRESTART',first_param_ptr,default=0)

    ! ... useLowMemIO = TRUE/FALSE : use low memory, but slower, i/o
    input%useLowMemIO = get_integer_param(region%myrank,'USE_LOW_MEM_IO',first_param_ptr,default=0)
    
    ! ... useMPIIO = TRUE/FALSE : use fast mpiio ala Ed
    input%useMPIIO = get_integer_param(region%myrank,'USE_MPI_IO',first_param_ptr,default=0)
    IF(input%useMPIIO == 1 .and. region%myrank == 0) THEN
       WRITE(*,'(A)') 'PlasComCM: Using MPI-I/O for all I/O operations.'
    ENDIF
#ifdef HAVE_HDF5
    ! ... useHDF5 = output the solution using PHDF5
    input%useHDF5 = get_integer_param(region%myrank,'USEHDF5',first_param_ptr,default=0)
#else
    input%useHDF5 = 0
#endif

    ! ... usePP = use post processing routine, no timemarching
    input%usePP   = get_integer_param(region%myrank,'POSTPROCESS',first_param_ptr,default=0)
    input%PP_Case = get_integer_param(region%myrank,'PP_CASE',first_param_ptr,default=0)

    ! Start/stop/skip iterations for postprocessing
    input%startIter = get_integer_param(region%myrank,'START_ITER',first_param_ptr,default=0)
    input%stopIter  = get_integer_param(region%myrank,'STOP_ITER',first_param_ptr,default=0)
    input%skipIter  = get_integer_param(region%myrank,'SKIP_ITER',first_param_ptr,default=0)
    input%X1        = get_real_param(region%myrank,'PP_START_X',first_param_ptr,default=0.0_8)
    input%X2        = get_real_param(region%myrank,'PP_END_X',first_param_ptr,default=0.0_8)

    ! On-the-fly statistics
    input%use_stats = get_integer_param(region%myrank,'RUN_STATS',first_param_ptr,default=0)
    input%stat_grid = get_integer_param(region%myrank,'STAT_GRID',first_param_ptr,default=1)
    input%stat_nvar = get_integer_param(region%myrank,'STAT_NVARS',first_param_ptr,default=1)
    input%stat_noutput = get_integer_param(region%myrank,'NOUTPUT_STATS',first_param_ptr,default=0)
    input%stat_symdir1 = get_integer_param(region%myrank,'SYMMETRIC_DIRECTION1',first_param_ptr,default=0)
    input%stat_symdir2 = get_integer_param(region%myrank,'SYMMETRIC_DIRECTION2',first_param_ptr,default=0)
    input%stat_symdir3 = get_integer_param(region%myrank,'SYMMETRIC_DIRECTION3',first_param_ptr,default=0)

    ! ... code-based Reynolds number.  RocFlo-CM is non-dimensionalized
    ! ... wrt ambient density, ambient speed of sound, ambient viscosity
    ! ... and an unspecified length.
    input%RE = get_real_param(region%myrank,'REYNOLDS_NUMBER',first_param_ptr,default=0.0_WP)
    input%machFactor = get_real_param(region%myrank,'MACH_FACTOR',first_param_ptr,default=1.0_WP)
    input%useActivityStandardPressure = get_logical_param(region%myrank,'USE_ACTIVITY_STANDARD_PRESSURE',first_param_ptr,default=.TRUE.)

    ! ... Prandtl number
    input%PR = get_real_param(region%myrank,'PRANDTL_NUMBER',first_param_ptr,default=0.72_WP)

    ! ... Froude number
    input%Froude = get_real_param(region%myrank,'FROUDE_NUMBER',first_param_ptr,default=0.0_WP)
    input%gravity_angle_phi   = get_real_param(region%myrank,'GRAVITY_ANGLE_PHI',first_param_ptr,default=0.0_WP)
    input%gravity_angle_theta = get_real_param(region%myrank,'GRAVITY_ANGLE_THETA',first_param_ptr,default=0.0_WP)
    input%gravity_angle_phi   = input%gravity_angle_phi * 0.5_WP * TWOPI / 180.0_WP
    input%gravity_angle_theta = input%gravity_angle_theta * 0.5_WP * TWOPI / 180.0_WP
    input%invFroude = 0.0_WP
    if (input%Froude > 0.0_WP) input%invFroude = 1.0_WP / input%Froude

    ! ... dimension of the problem, can be = 1, 2 or = 3.
    input%ND = get_integer_param(region%myrank,'ND',first_param_ptr,default=2)
    if (input%ND /= 1 .and. input%fluidModel == Q1D) call graceful_exit(region%myrank, 'PlasComCM: ERROR: Must have ND = 1 if using quasi-1-D fluid model.')

    ! ... number of passive/active scalars
    input%nScalars = get_integer_param(region%myrank,'NUMBER_OF_SCALARS',first_param_ptr,default=0)
    input%nAuxVars = input%nScalars
    if (input%fluidModel == Q1D) input%nAuxVars = input%nAuxVars + 2

    ! Old chemistry flag (remove this once everyone has migrated to the new flag)
    includeChemistryOld = get_integer_param(region%myrank,'INCLUDE_CHEMISTRY',first_param_ptr,default=FALSE) == TRUE
    if (includeChemistryOld) then
      call graceful_exit(region%myrank, "ERROR: INCLUDE_CHEMISTRY flag is no longer supported. Please set CHEMISTRY_MODEL instead.")
    end if

    ! ... chemistry model = NONE (single-component non-reacting flow)
    ! ... chemistry model = ONESTEP (one-step H2-O2 chemistry)
    ! ... chemistry model = MODEL0 (XPACC Model0 chemistry)
    ! ... chemistry model = MODEL1 (XPACC Model1 chemistry)
    input%chemistry_model  = get_integer_param(region%myrank,'CHEMISTRY_MODEL',first_param_ptr,default=FALSE)
    input%chemModelConvert = get_integer_param(region%myrank,'CHEMISTRY_MODEL_CONVERT',first_param_ptr,default=FALSE) == TRUE

    ! ... include efield
    input%includeElectric = get_integer_param(region%myrank,'INCLUDE_ELECTRIC',first_param_ptr,default=FALSE) == TRUE
    if (input%includeElectric) then
#ifndef BUILD_ELECTRIC
      call graceful_exit(region%myrank, ' ERROR: Must build with electric field solver support enabled to run with INCLUDE_ELECTRIC set to TRUE.')
#endif
    end if

    ! ... default Schmidt number
    input%SC = get_real_param(region%myrank,'SCHMIDT_NUMBER',first_param_ptr,default=1.0_WP)

    ! ... determine whether we should write other data periodically
    input%write_xyz     = get_integer_param(region%myrank,'WRITE_XYZ',first_param_ptr,default = TRUE)
    input%write_cv      = get_integer_param(region%myrank,'WRITE_CV',first_param_ptr,default = TRUE)
    input%write_dv      = get_integer_param(region%myrank,'WRITE_DV',first_param_ptr,default = FALSE)
    input%write_cvt     = get_integer_param(region%myrank,'WRITE_CVT',first_param_ptr,default = FALSE)
    input%write_met     = get_integer_param(region%myrank,'WRITE_MET',first_param_ptr,default = FALSE)
    input%write_jac     = get_integer_param(region%myrank,'WRITE_JAC',first_param_ptr,default = FALSE)
    input%write_rhs     = get_integer_param(region%myrank,'WRITE_RHS',first_param_ptr,default = FALSE)
    input%write_targ    = get_integer_param(region%myrank,'WRITE_TARGET',first_param_ptr,default = FALSE)
    input%write_nothing = get_integer_param(region%myrank,'WRITE_NOTHING',first_param_ptr,default = FALSE)
    input%read_nothing  = get_integer_param(region%myrank,'READ_NOTHING',first_param_ptr,default = FALSE)
    disableIO = get_integer_param(region%myrank,'DISABLE_IO',first_param_ptr,default = FALSE)
    IF(disableIO == TRUE) THEN
       IF(region%myrank == 0) THEN
          WRITE(*,'(A)') 'PlasComCM: Disabling I/O operations.'
       ENDIF
       input%read_nothing  = TRUE
       input%write_nothing = TRUE
    ENDIF
    
    
!    input%write_rank = get_integer_param(region%myrank,'WRITE_RANK',first_param_ptr,default = FALSE)
    IF(input%write_targ == TRUE) input%write_cvt = TRUE
#ifdef BUILD_ELECTRIC
    if (input%includeElectric) then
      input%write_phi = get_integer_param(region%myrank,'WRITE_PHI',first_param_ptr,default = FALSE)
    else
      input%write_phi = FALSE
    end if
#endif

    ! ... Type of grid to be used
    ! ... MONOLITHIC :: A *single* grid is supplied and the code will
    ! ...               parallelize along the longest dimension (by DEFAULT)
    ! ...               or by user-specified direction
    ! ... CHIMERA    :: Multiple grids supplied which overlap.
    ! ...               Parallelization achieved through user-supplied
    ! ...               grid partitioning.
    input%gridType             = get_integer_param(region%myrank,'GRIDTYPE',first_param_ptr,default=MONOLITHIC)
    input%interpType           = get_integer_param(region%myrank,'INTERP_TYPE',first_param_ptr,default=BELLERO)
    input%bcfringe             = get_integer_param(region%myrank,'BCFRINGE',first_param_ptr,default=1)
    input%fringe               = get_integer_param(region%myrank,'FRINGE',first_param_ptr,default=1)
    input%volumeIntersection   = get_integer_param(region%myrank,'VOLUME_INTERSECTION',first_param_ptr,default=FALSE)
    input%boundaryIntersection = get_integer_param(region%myrank,'BOUNDARY_INTERSECTION', first_param_ptr,default=FALSE)
    input%bgGridGlobalId       = get_integer_param(region%myrank,'BGGRID_GLOBAL',first_param_ptr,default=-1)
    if (input%gridType == CHIMERA .and. input%interpType == ROCSMASH .and. input%periodicStorage == NO_OVERLAP_PERIODIC) Then
      call graceful_exit(region%myrank, 'PlasComCM: ERROR: Must use OVERLAP_PERIODIC with Rocsmash.')
    end if

    ! ... default to volume interpolation for BELLERO
    If (input%interpType == BELLERO .OR. input%interpType == OGEN) Then
      input%volumeIntersection = TRUE
    End if

    ! ... read order of OGEN interpolation
    If (input%interpType == OGEN) Then
      input%OgenOrder = get_integer_param(region%myrank,'OGEN_ORDER_INTERPOLATION',first_param_ptr,default=2)
      If ((input%OgenOrder /= 2)) Then
        If ((input%OgenOrder /= 4)) Then
          Call graceful_exit(region%myrank, 'PlasComCM: ERROR: Only order={2,4} Ogen Interpolation Supported.')
        End If
      End If
    End If

    ! ... used for processing SAT boundary patches
    input%useNativeSBI = get_integer_param(region%myrank,'USE_NATIVE_SBI',first_param_ptr,default=FALSE)

    ! ... check for consistency
    if (input%boundaryIntersection == TRUE .and. input%useNativeSBI == FALSE) &
      call graceful_exit(region%myrank, 'PlasComCM: ERROR: Must use native SBI when requesting boundary intersections.')
    if (input%boundaryIntersection == TRUE .and. input%useNativeSBI == TRUE .and. input%gridType == MONOLITHIC) &
      call graceful_exit(region%myrank, 'PlasComCM: ERROR: Must use CHIMERA grid type when requesting Native SBI.')
    if (input%boundaryIntersection == TRUE .and. input%useNativeSBI == TRUE .and. input%nAuxVars > 0) &
      call graceful_exit(region%myrank, 'PlasComCM: ERROR: Native SBI currently does not support auxiliary variables.')
    
    ! ... flag to adjust BELLERO data for o-periodic grids
    input%fixBelleroOPeriodic = get_integer_param(region%myrank,'FIX_BELLERO_O_PERIODIC',first_param_ptr,default=FALSE)
    input%belleroOPeriodicOverlap = get_integer_param(region%myrank,'BELLERO_O_PERIODIC_OVERLAP',first_param_ptr,default=5)

    ! ... TRUE  :: the grid is moving 
    ! ... FALSE :: the grid is static
    input%moveGrid = get_integer_param(region%myrank,'MOVEGRID',first_param_ptr,default=FALSE)
    if(input%moveGrid == TRUE) input%UseTFI = get_integer_param(region%myrank,'USE_TRANSFINITE_INTERP',first_param_ptr,default=FALSE)

    ! ... RIGID :: the grid does not deform (metrics are not re-computed)
    ! ... DEFORMING :: the grid is deforming (metrics are re-computed)
    input%motionType = get_integer_param(region%myrank,'MOTION_TYPE',first_param_ptr,default=RIGID)

    ! ... spaceDiscr = IMPLICIT :: Pade (3-4-6-4-3) finite differences
    ! ... spaceDiscr = EXPLICIT :: Explicit SBP (4-4-4-4-8-4-4-4-4) FD or
    ! ...                          Explicit DRP (11pt-13pt-11pt) FD
    input%spaceDiscr = get_integer_param(region%myrank,'SPACEDISC',first_param_ptr,default=EXPLICIT)

    ! ... spaceOrder = selects scheme based on value of spaceDiscr
    ! ... spaceDiscr = EXPLICIT and spaceOrder = 5 :: SBP 8-4
    ! ... spaceDiscr = EXPLICIT and spaceOrder = 3 :: SBP 4-2
    ! ... spaceDiscr = EXPLICIT and spaceOrder = 2 :: 2nd order FD
    ! ... spaceDiscr = EXPLICIT and spaceOrder = 4 :: DRP 13pt-11pt
    ! ... spaceDiscr = IMPLICIT and spaceOrder = 4 :: tri-diagonal PADE
    ! ... spaceDiscr = IMPLICIT and spaceOrder = 3 :: penta-diagonal PADE of Kim & Lee
    input%spaceOrder = get_integer_param(region%myrank,'SPACEORDER',first_param_ptr,default=5)

    ! ... which implicit solver to use for pade operators?
    input%operator_implicit_solver = get_integer_param(region%myrank,'OPERATOR_IMPLICIT_SOLVER',first_param_ptr,default=DIRECT_PENTA)

    ! ... read the user-desired parallelization topology
    input%parallelTopology = get_integer_param(region%myrank,'PARALLEL_TOPOLOGY',first_param_ptr,default=CUBE)

    ! ... read the user-desired parallelization direction
    input%parDir = get_integer_param(region%myrank,'PARDIR',first_param_ptr,default=0)

    ! ... use low memory operator setup?
    input%use_lowmem_operator_setup = get_integer_param(region%myrank,'USE_LOW_MEM_OPERATOR_SETUP',first_param_ptr,default=FALSE)

    ! ... read the user-supplied decomposition map
    input%useDecompMap = get_integer_param(region%myrank,'USE_DECOMP_MAP',first_param_ptr,default=FALSE)
    if (input%useDecompMap == TRUE) call set_param(region%myrank,input%decompMap,'DECOMP_MAP',first_param_ptr,default='decomp.map')

    ! ... use Local decomposition algorithm
    input%useLocalDecomp = get_integer_param(region%myrank,'LOCAL_DECOMP',first_param_ptr,default=FALSE)

    ! ... read any overlap override
    input%nOverlapOverride = get_integer_param(region%myrank,'OVERLAP_OVERRIDE',first_param_ptr,default=0)

    ! ... what to do if a stencil does not fit
    input%fix_stencil_cannot_fit = get_integer_param(region%myrank,'FIX_STENCIL_CANNOT_FIT',first_param_ptr,default=FALSE)

    ! ... name of PLOT3D file that contains the flow variables to be
    ! ... used in cvTarget (sponge solution) in ModNavierStokes.f90
    call set_param(region%myrank,input%target_fname,'TARGET_FNAME',first_param_ptr,default='RocFlo-CM.00000000.target.q')

    ! ... check if we should read a target file on startup, or use the IC as the target
    input%readTargetOnStartup = get_integer_param(region%myrank,'READ_TARGET_ON_STARTUP',first_param_ptr,default=FALSE)

    ! ... name of PLOT3D file that contains the flow variables to be
    ! ... used in mean in ModLinNavierStokes.f90
    call set_param(region%myrank,input%mean_fname,'MEAN_FNAME',first_param_ptr,default='RocFlo-CM.00000000.mean.q')

    ! ... name of PLOT3D grid file which may contain IBLANKs
    call set_param(region%myrank,input%grid_fname,'GRID_FNAME',first_param_ptr,default='RocFlo-CM.xyz')

    ! ... name of text file containing boundary condition specification
    call set_param(region%myrank,input%bc_fname,'BC_FNAME',first_param_ptr,default='bc.dat')

    ! ... name of text file containing boundary condition specification
    input%outputBC_asIBLANK = get_integer_param(region%myrank,'OUTPUT_BC_AS_IBLANK',first_param_ptr,default=FALSE)

    ! ... array describing periodicity.  if o-periodic (e.g., circular)
    ! ... set to TRUE, if plane periodic set to PLANE_PERIODIC and adjust
    ! ... values of L1, L2, L3 as above.
    input%periodic(:) = FALSE
    do j = 1, input%ND
      write(str,'(A,I1)') 'PERIODIC', j
      input%periodic(j) = get_integer_param(region%myrank,trim(str),first_param_ptr,default=FALSE)
      write(str,'(A,I1)') 'PERIODICL', j
      ! ... periodic lengths.  Only used when PLANE_PERIODIC flag
      input%periodicL(j) = get_real_param(region%myrank,trim(str),first_param_ptr,default=1.0_WP)
    end do

    ! ... flag describing data storage of periodic grids
    ! ... NO_OVERLAP_PERIODIC = periodic data are NOT repeated (typical `fft-style')
    ! ...    OVERLAP_PERIODIC = periodic data are repeated along one plane
    input%periodicStorage = get_integer_param(region%myrank,'PERIODIC_STORAGE',first_param_ptr,default=NO_OVERLAP_PERIODIC)

    ! ... GCL enforcement
    input%useMetricIdentities = get_integer_param(region%myrank,'USE_METRIC_IDENTITIES',first_param_ptr,default=FALSE)

    ! ... number of auxillary dependent variables
    input%nDv = 3 ! p, T, 1/rho

    ! ... number of transport variables
    input%nTv = 3 ! mu, lambda, k
    input%massDiffusivityIndex = 1

    ! ... number of gas variables
    input%nGv = 1 ! gamma

    ! ... number of conserved dependent variables
    input%nCv = input%ND + 2

    ! ... type of time marching.
    ! ... CONSTANT_CFL => CFL number held fixed
    ! ... CONSTANT_DT  => timestep held fixed
    input%cfl_mode = get_integer_param(region%myrank,'CFL_MODE',first_param_ptr,default=CONSTANT_CFL)

    ! ... initial timestep (in non-dimensional units)
    input%dt = get_real_param(region%myrank,'TIMESTEP',first_param_ptr,default=0.01_WP)

    ! ... initial CFL
    input%cfl = get_real_param(region%myrank,'CFL',first_param_ptr,default=0.5_WP)

    ! ... output sample mode
    input%output_mode = get_integer_param(region%myrank,'OUTPUT_MODE',first_param_ptr,default=ITERATION)

    ! ... sample time (if OUTPUT_MODE = SAMPLE_TIME)
    input%t_output = get_real_param(region%myrank,'T_OUTPUT',first_param_ptr,default=0.01_WP)

    ! ... unused at this time
    input%interpOrder = 6

    ! ... Penta-diagonal Pade filter
    ! ... .TRUE.  => applied every time step
    ! ... .FALSE. => never used
    input%filter = get_logical_param(region%myrank,'FILTER',first_param_ptr,default=.FALSE.)

    ! ... Boundary filter flag
    ! ... .TRUE.  => filter from i = 2 to i = N-1 used biased stencils
    ! ... .FALSE. => do not use any biased filter stencils
    input%bndry_filter(1) = get_logical_param(region%myrank,'BNDRY_FILTER_LEFT',first_param_ptr,default=.FALSE.)
    input%bndry_filter(2) = get_logical_param(region%myrank,'BNDRY_FILTER_RIGHT',first_param_ptr,default=.FALSE.)

    ! ... Filtering scheme (EXPLICIT or IMPLICIT)
    input%filterDiscr = get_integer_param(region%myrank,'FILTERDISC',first_param_ptr,default=EXPLICIT)

    ! ... filter coefficients
    input%filter_alphaf = get_real_param(region%myrank,'FILTER_ALPHAF',first_param_ptr,default=0.49_WP)
    input%filter_fac    = get_real_param(region%myrank,'FILTER_FAC',first_param_ptr,default=0.20_WP)
    input%bndry_filter_alphaf = get_real_param(region%myrank,'BNDRY_FILTER_ALPHAF',first_param_ptr,default=0.49_WP)

    ! ... SAT artificial dissipation
    input%sat_art_diss = get_logical_param(region%myrank,'SAT_ARTIFICIAL_DISSIPATION',first_param_ptr,default=.FALSE.)
    input%amount_SAT_diss = get_real_param(region%myrank,'AMOUNT_SAT_ARTIFICIAL_DISSIPATION',first_param_ptr,default=0.0_WP)

    ! ... SAT boundary strengths
    input%SAT_sigmaI1 = get_real_param(region%myrank,'AMOUNT_SAT_SIGMAI1',first_param_ptr,default=2.0_WP)
    input%SAT_sigmaI2 = get_real_param(region%myrank,'AMOUNT_SAT_SIGMAI2',first_param_ptr,default=2.0_WP)

    ! ... SAT far-field boundary strengths
    input%SAT_sigmaI1_FF = get_real_param(region%myrank,'AMOUNT_SAT_SIGMAI1_FARFIELD',first_param_ptr,default=0.5_WP)
    input%SAT_sigmaI2_FF = get_real_param(region%myrank,'AMOUNT_SAT_SIGMAI2_FARFIELD',first_param_ptr,default=1.0_WP)

    ! ... SAT block_interface strengths
    input%SAT_sigmaI1_BI = get_real_param(region%myrank,'AMOUNT_SAT_SIGMAI1_BLOCK_INTERFACE',first_param_ptr,default=0.5_WP)
    input%SAT_sigmaI2_BI = get_real_param(region%myrank,'AMOUNT_SAT_SIGMAI2_BLOCK_INTERFACE',first_param_ptr,default=1.0_WP)

    ! Shock-capturing scheme only (See ModDataStruct.f90 for their usage)
    input%shock           = get_integer_param(region%myrank,'SHOCK_CAPTURE',first_param_ptr,default=FALSE)
    input%ArtPropDrvOrder = get_integer_param(region%myrank,'SHOCK_CAPTURE_ArtPropDrvOrder',first_param_ptr,default=4)
    input%nArtProp        = get_integer_param(region%myrank,'SHOCK_CAPTURE_nArtProp',first_param_ptr,default=2)
    input%HyperCmu        = get_real_param(region%myrank,'SHOCK_CAPTURE_HyperCmu',first_param_ptr,default=0.002_WP)
    input%HyperCbeta      = get_real_param(region%myrank,'SHOCK_CAPTURE_HyperCbeta',first_param_ptr,default=1.0_WP)
    input%HyperCKappa     = get_real_param(region%myrank,'SHOCK_CAPTURE_HyperCKappa',first_param_ptr,default=0.01_WP)
    input%shock_clip      = get_logical_param(region%myrank,'SHOCK_CAPTURE_CLIP',first_param_ptr,default=.FALSE.)
    input%MaxHyperMu      = get_real_param(region%myrank,'SHOCK_CAPTURE_MaxHyperMu',first_param_ptr,default=0.00025_WP)
    input%MaxHyperBeta    = get_real_param(region%myrank,'SHOCK_CAPTURE_MaxHyperBeta',first_param_ptr,default=0.222_WP)
    input%MaxHyperKappa   = get_real_param(region%myrank,'SHOCK_CAPTURE_MaxHyperKappa',first_param_ptr,default=0.00073_WP)
    input%ShockFilterRTH  = get_real_param(region%myrank,'SHOCK_FILTER_RTH',first_param_ptr,default=0.00001_WP)

    ! ... interpolation filenames
    call set_param(region%myrank,input%xintout_x_fname,'XINTOUT_X_FNAME',first_param_ptr,default='XINTOUT.X')
    call set_param(region%myrank,input%xintout_ho_fname,'XINTOUT_HO_FNAME',first_param_ptr,default='XINTOUT.HO')
    call set_param(region%myrank,input%xintout_ogen_fname,'XINTOUT_OGEN_FNAME',first_param_ptr,default='XINTOUT.ogen')

    ! ... optionally accept XINTOUT files that do not contain IBLANK array
    input%xintout_has_iblank = get_integer_param(region%myrank,'XINTOUT_HAS_IBLANK',first_param_ptr,default=TRUE) == TRUE

    ! ... Type of metrics needed.
    ! ... NONORTHOGONAL_WEAKFORM :: compute all metrics, expanded 2nd derivatives
    ! ... NONORTHOGONAL_STRONGFORM :: compute all metrics, repeated 1st derivatives
    ! ... CARTESIAN :: non-uniform, but Cartesian, grid with expanded 2nd derivatives
    ! ... STRONGFORM_NARROW :: narrow stencils for second-order variable coeifficients
    input%metricType = get_integer_param(region%myrank,'METRICTYPE',first_param_ptr,default=NONORTHOGONAL_STRONGFORM)
    if (input%metricType == CARTESIAN) call graceful_exit(region%myrank, ' ERROR: Using METRICTYPE = CARTESIAN is not supported.')

    ! ... Type of advection equation.
    ! ... STANDARD_EULER : Standard representation of advection terms. 
    ! ... SKEW_SYMMETRIC: Skew symmetric form of advection terms.
    input%advectionType = get_integer_param(region%myrank,'ADVECTION_TYPE',first_param_ptr,default=STANDARD_EULER)


    ! ... amplitude parameter of sponge zone :: the 'A' in
    ! ... sponge strength = A (eta)**n
    input%sponge_amp = get_real_param(region%myrank,'SPONGE_AMP',first_param_ptr,default=5.0_WP)

    ! ... power parameter of sponge zone :: the 'n' in
    ! ... sponge strength = A (eta)**n
    input%sponge_pow = get_integer_param(region%myrank,'SPONGE_POW',first_param_ptr,default=2)

    ! ... LES (0 = DNS, 101 = classical Smagorinsky, 200:299 = dynamic Smagorinsky)
    input%LES = get_integer_param(region%myrank,'LES_MODEL',first_param_ptr,default=0)
    input%LES_RATIOFILTERWIDTH = get_real_param(region%myrank,'LES_RATIOFILTERWIDTH',first_param_ptr,default=2.0_WP)
    input%LES_ONCE = get_logical_param(region%myrank,'LES_ONCE',first_param_ptr,default=.FALSE.)
    input%LES_CLIP = get_logical_param(region%myrank,'LES_CLIP',first_param_ptr,default=.FALSE.)
    input%LES_CLIP_MAX_C = get_real_param(region%myrank,'LES_CLIP_MAX_C',first_param_ptr,default=0.1_WP)
    input%LES_CLIP_MAX_CI = get_real_param(region%myrank,'LES_CLIP_MAX_CI',first_param_ptr,default=0.1_WP)
    input%LES_CLIP_MAX_PrT = get_real_param(region%myrank,'LES_CLIP_MAX_PrT',first_param_ptr,default=1.0_WP)
    input%LES_CLIP_MAX_C = DABS(input%LES_CLIP_MAX_C) ! ... just in case
    input%LES_CLIP_MAX_CI = DABS(input%LES_CLIP_MAX_CI) ! ... just in case
    input%LES_CLIP_MAX_PrT = DABS(input%LES_CLIP_MAX_PrT) ! ... just in case

    ! ... Electric field parameters
#ifdef BUILD_ELECTRIC
    if (input%includeElectric) then
      input%ef_solve_interval = get_integer_param(region%myrank,'EF_SOLVE_INTERVAL',first_param_ptr,default=0)
      input%ef_dielectric_permittivity = get_real_param(region%myrank,'EF_DIELECTRIC_PERMITTIVITY',first_param_ptr,default=4.65_WP)  !default for quartz
      input%ef_voltage_scale = get_real_param(region%myrank,'EF_VOLTAGE_SCALE',first_param_ptr,default=1._WP)
      input%ef_screening_length = get_real_param(region%myrank,'EF_SCREENING_LENGTH',first_param_ptr,default=0._WP)
      input%ef_plasma_potential = get_real_param(region%myrank,'EF_PLASMA_POTENTIAL',first_param_ptr,default=0._WP)
      input%ef_schwarz_iters = get_integer_param(region%myrank,'EF_SCHWARZ_ITERS',first_param_ptr,default=50)
      input%ef_subset = get_integer_param(region%myrank,'EF_SUBSET',first_param_ptr,default=FALSE) == TRUE
      call set_param(region%myrank,input%ef_subset_fname,'EF_SUBSET_FNAME',first_param_ptr,default='ef_subset.dat')
    end if
#endif

    ! ... read BC/IC data
    input%bcic_planewave   = get_integer_param(region%myrank,'BCIC_PLANEWAVE',first_param_ptr,default=FALSE)
    input%bcic_frequency   = get_real_param(region%myrank,'BCIC_FREQUENCY',first_param_ptr,default=0.0_WP)
    input%bcic_amplitude   = get_real_param(region%myrank,'BCIC_AMPLITUDE',first_param_ptr,default=0.0_WP)
    input%bcic_angle_phi   = get_real_param(region%myrank,'BCIC_ANGLE_PHI',first_param_ptr,default=0.0_WP)
    input%bcic_angle_theta = get_real_param(region%myrank,'BCIC_ANGLE_THETA',first_param_ptr,default=0.0_WP)
    input%bcic_eigenfunction = get_integer_param(region%myrank,'BCIC_EIGENFUNCTION',first_param_ptr,default=FALSE)
    input%bcic_eigenfunction_imax = get_integer_param(region%myrank,'BCIC_EIGENFUNCTION_IMAX',first_param_ptr,default=0)
    call set_param(region%myrank,input%bcic_eigenfunction_fname,'BCIC_EIGENFUNCTION_FNAME',first_param_ptr,default='sl0.pert')

    ! ... read acoustic source data
    input%acoustic_source = get_integer_param(region%myrank,'ACOUSTIC_SOURCE',first_param_ptr,default=FALSE)
    If (input%acoustic_source == TRUE) Then
      input%acoustic_source_nfreq = get_integer_param(region%myrank,'ACOUSTIC_SOURCE_NFREQ',first_param_ptr,default=0)
      if (input%acoustic_source_nfreq > 0) then
        allocate(input%acoustic_source_freq(input%acoustic_source_nfreq))
        allocate(input%acoustic_source_amp(input%acoustic_source_nfreq))
        allocate(input%acoustic_source_phase(input%acoustic_source_nfreq))
        call set_param(region%myrank,str,'ACOUSTIC_SOURCE_FREQ',first_param_ptr,default='1.0')
        Read (str(2:len_trim(str)-1),*) input%acoustic_source_freq
        call set_param(region%myrank,str,'ACOUSTIC_SOURCE_AMP',first_param_ptr,default='1.0')
        Read (str(2:len_trim(str)-1),*) input%acoustic_source_amp
        call set_param(region%myrank,str,'ACOUSTIC_SOURCE_PHASE',first_param_ptr,default='0.0')
        Read (str(2:len_trim(str)-1),*) input%acoustic_source_phase
      end if
      input%acoustic_source_x0(1) = get_real_param(region%myrank,'ACOUSTIC_SOURCE_X0',first_param_ptr,default=0.0_8)
      input%acoustic_source_x0(2) = get_real_param(region%myrank,'ACOUSTIC_SOURCE_Y0',first_param_ptr,default=0.0_8)
      input%acoustic_source_x0(3) = get_real_param(region%myrank,'ACOUSTIC_SOURCE_Z0',first_param_ptr,default=0.0_8)
      input%acoustic_source_lx(1) = get_real_param(region%myrank,'ACOUSTIC_SOURCE_LX',first_param_ptr,default=1.0_8)
      input%acoustic_source_lx(2) = get_real_param(region%myrank,'ACOUSTIC_SOURCE_LY',first_param_ptr,default=1.0_8)
      input%acoustic_source_lx(3) = get_real_param(region%myrank,'ACOUSTIC_SOURCE_LZ',first_param_ptr,default=1.0_8)
    End If

    ! ... Adjoint N-S
    input%AdjNS = get_logical_param(region%myrank,'ADJOINT_NS',first_param_ptr,default=.FALSE.)
    input%AdjNS_ReadNSSoln = get_logical_param(region%myrank,'ADJOINT_NS_READ_NS',first_param_ptr,default=.TRUE.)
    call set_param(region%myrank,input%AdjNS_SolnList,'ADJOINT_NS_FNAME',first_param_ptr,default='soln_list.dat')

    ! ... conjugate gradient optimization (by default, noise control)
    input%AdjOptim = get_logical_param(region%myrank,'ADJOINT_OPTIMIZATION',first_param_ptr,default=.FALSE.)
    input%AdjOptim_typeOfOptim = get_integer_param(region%myrank,'ADJOINT_OPTIMIZATION_TYPE',first_param_ptr,default=0)
    input%AdjOptim_CGMaxIt = get_integer_param(region%myrank,'ADJOINT_OPTIMIZATION_CG_MAXIT',first_param_ptr,default=50)
    input%AdjOptim_DBrentMaxIt = get_integer_param(region%myrank,'ADJOINT_OPTIMIZATION_DBRENT_MAXIT',first_param_ptr,default=100)
    input%AdjOptim_Restart = get_logical_param(region%myrank,'ADJOINT_OPTIMIZATION_RESTART',first_param_ptr,default=.FALSE.)
    input%AdjOptim_Func = get_integer_param(region%myrank,'ADJOINT_OPTIMIZATION_FUNCTIONAL',first_param_ptr,default=FUNCTIONAL_SOUND)
    call set_param(region%myrank,input%AdjOptim_mean,'ADJOINT_NS_FNAME_MEAN',first_param_ptr,default='RocFlo-CM.target.q')
    input%AdjOptim_CtrlType = get_integer_param(region%myrank,'ADJOINT_OPTIMIZATION_CONTROL',first_param_ptr,default=CONTROL_INTERNAL_ENERGY)
    input%AdjOptim_Constraint = get_integer_param(region%myrank,'ADJOINT_OPTIMIZATION_CONSTRAINT',first_param_ptr,default=CONTROL_UNCONSTRAINED)
    input%AdjOptim_axis_of_rot = get_integer_param(region%myrank,'ADJOINT_OPTIMIZATION_AXIS_OF_ROT',first_param_ptr,default=0)
    input%AdjOptim_eps = get_real_param(region%myrank,'ADJOINT_OPTIMIZATION_EPS',first_param_ptr,default=1.0E-5_WP)
    input%AdjOptim_deps = get_real_param(region%myrank,'ADJOINT_OPTIMIZATION_DEPS',first_param_ptr,default=1.0E-6_WP)
    input%AdjOptim_dbrent_eps = get_real_param(region%myrank,'ADJOINT_OPTIMIZATION_DBRENT_EPS',first_param_ptr,default=1.0E-2_WP)
    input%AdjOptim_initAlpha = get_real_param(region%myrank,'ADJOINT_OPTIMIZATION_INIT_ALPHA',first_param_ptr,default=0.01_WP)
    ! ... consistency check
    if (input%AdjNS .OR. input%AdjOptim) then
      ! ... no moving-grid allowed
      if (input%moveGrid == TRUE) then
        call graceful_exit(region%myrank, "... ERROR: no way to use moving-grid in adjoint N-S")
      end if ! input%moveGrid

!!$   ! ... no overset grid; this would be relieved pretty soon
!!$   if (input%gridType == CHIMERA) then
!!$     call graceful_exit(region%myrank, "... ERROR: overset grid not available in adjoint N-S")
!!$   end if ! input%gridType

      if (input%timeScheme /= EXPLICIT_RK4) then
        call graceful_exit(region%myrank, "... ERROR: only RK4 available for optimization")
      end if ! input%timeScheme

      if (input%cfl_mode == CONSTANT_CFL) then
        call graceful_exit(region%myrank, "... ERROR: use constant-DT time-stepping in adjoint N-S")
      end if ! input%cfl_mode

      ! ... no weak formulation of viscous terms
      if (input%metricType /= NONORTHOGONAL_STRONGFORM) then
        input%metricType = NONORTHOGONAL_STRONGFORM
        if (region%myrank == 0) write (*,'(A)') '... ERROR: viscous term formulation other than strong form not supported in adjoint N-S'
      end if ! input%metricType

      ! ... no shock-capturing scheme associated with adjoint N-S solver
      if (input%shock /= 0) then
        input%shock = 0
        if (region%myrank == 0) write (*,'(A)') '... shock-capturing scheme disabled in adjoint N-S'
      end if ! input%shock

!!$   ! ... no LES associated with adjoint N-S solver
!!$   if (input%LES /= 0) then
!!$     input%LES = 0
!!$     call graceful_exit(region%myrank, "... ERROR: LES in adjoint N-S?")
!!$   end if ! input%LES
    end if ! input%AdjNS
    ! ... more consistency check
    if (input%AdjOptim) then
      ! ... defensive programming
      input%AdjNS = .FALSE.
      input%AdjNS_ReadNSSoln = .FALSE.
      if (region%global%USE_RESTART_FILE == TRUE) input%AdjNS_ReadNSSoln = .TRUE. ! ... if N-S equations are solved

      if (input%fluidModel == Q1D) then
        call graceful_exit(region%myrank, "... ERROR: Quasi 1-D fluid model not supported in adjoint-based optimization.")
      end if ! input%fluidModel

      if (input%AdjOptim_Func /= FUNCTIONAL_SOUND) then
        call graceful_exit(region%myrank, "... ERROR: only sound optimization supported for now")
      end if ! input%AdjOptim_Func

      ! ... controller consistency-check
      if (input%AdjOptim_CtrlType >= CONTROL_BODY_FORCE_XY .and. &
          input%AdjOptim_CtrlType <= CONTROL_BODY_FORCE_XYZ) then
        call graceful_exit(region%myrank, "... ERROR: multi-component body-force forcing not available yet.")
      end if ! input%AdjOptim_Func
      if (input%AdjOptim_CtrlType >= CONTROL_BODY_FORCE_YZ .and. &
          input%AdjOptim_CtrlType <= CONTROL_BODY_FORCE_THETA ) then ! ... three coordinates are assumed
        if (input%ND /= 3) &
          call graceful_exit(region%myrank, "... ERROR: should be 3-D for multi-component body-force control (except on xy).")
      end if ! input%AdjOptim_CtrlType
      if (input%AdjOptim_CtrlType >= CONTROL_BODY_FORCE_R .and. input%AdjOptim_CtrlType <= CONTROL_BODY_FORCE_THETA) then
        if (input%AdjOptim_axis_of_rot /= 1 .and. input%AdjOptim_axis_of_rot /= 2 .and. input%AdjOptim_axis_of_rot /= 3) &
          call graceful_exit(region%myrank, "... ERROR: axis of rotation should be specified for either r or theta body force control.")
      end if ! input%AdjOptim_CtrlType
    end if ! input%AdjOptim

    ! ... read info on getting pointwise time history
    input%GetTrace = get_logical_param(region%myrank,'PROBE_GET_TRACE',first_param_ptr,default=.FALSE.)
    input%numProbeGlobal = get_integer_param(region%myrank,'PROBE_NUM',first_param_ptr,default=0)
    input%sampleRate = get_integer_param(region%myrank,'PROBE_SAMPLE_RATE',first_param_ptr,default=1)
    call set_param(region%myrank,input%probe_fname,'PROBE_LOCATION_FILENAME',first_param_ptr,default='probe.pos')

    ! ... inflow forcing using the linear stability theory in an axisymmetric domain
    input%LST_num_mode_temporal = get_integer_param(region%myrank,'LST_NUM_MODE_TEMPORAL',first_param_ptr,default=0)
    input%LST_num_mode_azimuthal = get_integer_param(region%myrank,'LST_NUM_MODE_AZIMUTHAL',first_param_ptr,default=0)
    input%LST_amp = get_real_param(region%myrank,'LST_AMPLITUDE',first_param_ptr,default=0.0_rfreal)
    input%LST_x0 = get_real_param(region%myrank,'LST_X0',first_param_ptr,default=0.0_rfreal)
    call set_param(region%myrank,input%LST_fname,'LST_FILENAME',first_param_ptr,default='LST_Qp_hat.w')

    ! ... Volumetric drag
    input%volumetric_drag = get_integer_param(region%myrank,'VOLUMETRIC_DRAG',first_param_ptr,default=FALSE)
    input%volumetric_drag_coefficient = get_real_param(region%myrank,'VOLUMETRIC_DRAG_COEFFICIENT',first_param_ptr,default=0.0_WP)

    ! ... Free stream values for (Non)dimensionalization
    ! ... Some values may be reset if using thermally perfect gas model
    input%TempRef        = get_real_param(region%myrank,'TEMPERATURE_REFERENCE',first_param_ptr,default = 300.0_WP) ! Kelvin
    input%GamRef         = get_real_param(region%myrank,'GAMMA_REFERENCE',first_param_ptr,default = 1.401513467320151_WP) 
    input%gasConstantRef = get_real_param(region%myrank,'SPECIFIC_GAS_CONSTANT_REFERENCE',first_param_ptr,default = 286.9_WP) ! J/(kg . K)
    input%UnivGasConstant = get_real_param(region%myrank,'UNIVERSAL_GAS_CONSTANT',first_param_ptr,default = 8314.4621_WP) ! J/(kmol . K)
    input%PresRef        = get_real_param(region%myrank,'PRESSURE_REFERENCE',first_param_ptr,default = 101325.0_WP) ! Pascals
    input%LengRef        = get_real_param(region%myrank,'LENGTH_REFERENCE',first_param_ptr,default = 1.0_WP) ! meters
    input%SndSpdRef      = get_real_param(region%myrank,'SNDSPD_REFERENCE',first_param_ptr,default = 347.1282183862326_WP) ! m/s
    input%DensRef        = get_real_param(region%myrank,'DENSITY_REFERENCE',first_param_ptr,default = 1.177239456256535_WP) ! kg/m^3
    input%EintRef        = input%PresRef / (input%GamRef - 1.0_8) / input%DensRef
    input%Cpref          = input%gasConstantRef * input%GamRef / (input%GamRef - 1.0_8)
    input%Cvref          = input%Cpref - input%gasConstantRef

    ! ... isothermal wall temperature
    input%bcic_WallTemp    = get_real_param(region%myrank,'BCIC_WALL_TEMP',first_param_ptr,default = input%TempRef)

    ! ... wall velocity
    input%bcic_wall_vel_x = get_real_param(region%myrank,'BCIC_WALL_VEL_X',first_param_ptr,default = 0.0_WP)
    input%bcic_wall_vel_y = get_real_param(region%myrank,'BCIC_WALL_VEL_Y',first_param_ptr,default = 0.0_WP)
    input%bcic_wall_vel_z = get_real_param(region%myrank,'BCIC_WALL_VEL_Z',first_param_ptr,default = 0.0_WP)

    ! ... input parameters for Implicit Code
    input%DTauDtRatio = get_real_param(region%myrank,'DTAU_DT_RATIO',first_param_ptr,default=0.1_WP)
    input%DTauDecreaseFac = get_real_param(region%myrank,'DTAU_DECREASE_FACTOR',first_param_ptr,default=0.5_WP)
    input%DTauIncFac = get_real_param(region%myrank,'DTAU_INCREASE_FACTOR',first_param_ptr,default=0.5_WP)

    ! ... preconditioner length
    input%Lu = get_real_param(region%myrank,'PRECONDITIONER_LU',first_param_ptr,default=1.0_WP)

    ! ... Free stream Mach number for unsteady preconditioner
    input%MachInf = get_real_param(region%myrank,'FREE_STREAM_MACH',first_param_ptr,default=0.0_WP)

    ! ... Flow Initialization
    call set_param(region%myrank,input%initflow_name,'INITFLOW_NAME',first_param_ptr,default='quiescent')
    input%initflow_xloc            = get_real_param(region%myrank,'INITFLOW_XLOC',     first_param_ptr,default=0.0_WP)
    input%initflow_yloc            = get_real_param(region%myrank,'INITFLOW_YLOC',     first_param_ptr,default=0.0_WP)
    input%initflow_zloc            = get_real_param(region%myrank,'INITFLOW_ZLOC',     first_param_ptr,default=0.0_WP)
    input%initflow_xwidth          = get_real_param(region%myrank,'INITFLOW_XWIDTH',   first_param_ptr,default=1.0_WP)
    input%initflow_ywidth          = get_real_param(region%myrank,'INITFLOW_YWIDTH',   first_param_ptr,default=1.0_WP)
    input%initflow_zwidth          = get_real_param(region%myrank,'INITFLOW_ZWIDTH',   first_param_ptr,default=1.0_WP)
    input%initflow_amplitude       = get_real_param(region%myrank,'INITFLOW_AMPLITUDE',first_param_ptr,default=0.1_WP)
    input%initflow_frequency       = get_real_param(region%myrank,'INITFLOW_FREQUENCY',first_param_ptr,default=0.0_WP)
    input%initflow_radius          = get_real_param(region%myrank,'INITFLOW_RADIUS',   first_param_ptr,default=0.0_WP)
    input%initflow_theta           = get_real_param(region%myrank,'INITFLOW_THETA',    first_param_ptr,default=90.0_WP)
    input%initflow_phi             = get_real_param(region%myrank,'INITFLOW_PHI',      first_param_ptr,default=0.0_WP)
    input%initflow_mach            = get_real_param(region%myrank,'INITFLOW_MACH',     first_param_ptr,default=0.0_WP)
    input%initflow_rpm             = get_real_param(region%myrank,'INITFLOW_RPM',      first_param_ptr,default=0.0_WP)
    input%initflow_tstart          = get_real_param(region%myrank,'INITFLOW_TSTART',   first_param_ptr,default=0.0_WP)
    input%initflow_tramp           = get_real_param(region%myrank,'INITFLOW_TRAMP',    first_param_ptr,default=1.0_WP) 
    input%initflow_delta           = get_real_param(region%myrank,'INITFLOW_DELTA',    first_param_ptr,default=1.0_WP)
    input%initflow_deltastar       = get_real_param(region%myrank,'INITFLOW_DELTASTAR',first_param_ptr,default=1.0_WP)

    ! ... Backwards compatability
    T4 = get_real_param(region%myrank,'UIF_THETA',first_param_ptr,default=360.0_WP)
    IF(T4 < 360.0) input%initflow_theta = T4
    T4 = get_real_param(region%myrank,'UIF_PHI',first_param_ptr,default=360.0_WP)
    IF(T4 < 360.0) input%initflow_phi   = T4

    ! ... increase Dtau if GMRES converges very quickly
    input%DTauIncThresh = get_integer_param(region%myrank,'DTAU_INCREASE_THRESHOLD',first_param_ptr,default = 2)
    input%DivThresh = get_integer_param(region%myrank,'DTAU_DIVERGENCE_THRESHOLD',first_param_ptr,default = 20)

    ! ... Steady State...
    input%ImplicitSteadyState = get_integer_param(region%myrank,'STEADY_STATE_SOLUTION',first_param_ptr,default = FALSE)
    input%ImplicitTrueSteadyState = get_integer_param(region%myrank,'TRUE_STEADY_STATE_SOLUTION',first_param_ptr,default = FALSE)

    ! ... Quasi Stead State...
    input%QuasiSS = get_integer_param(region%myrank,'QUASI_STEADY_STATE_SOLUTION',first_param_ptr,default = FALSE)
    input%ConvCrit = get_real_param(region%myrank,'QUASI_SS_CONVERGE_CRIT',first_param_ptr,default=0.00000001_WP)

    ! ... Hypre Parameters
    ! ... Integers (Precondtioners 0-4: None, Euclid, ParaSails, BoomerAMG, Block Jacobi
    input%HypreParamsInt(1) = get_integer_param(region%myrank,'PRECONDITIONER',first_param_ptr,default=0)    
    input%HypreParamsInt(2) = get_integer_param(region%myrank,'GMRES_MAX_ITERS',first_param_ptr,default=100)
    input%HypreParamsInt(3) = get_integer_param(region%myrank,'KRYLOV_DIMENSION',first_param_ptr,default=5)        
    input%HypreParamsInt(4) = get_integer_param(region%myrank,'EUCLID_LEVEL',first_param_ptr,default=0)    
    input%HypreParamsInt(5) = get_integer_param(region%myrank,'BLOCK_JACOBI',first_param_ptr,default=0)    
    input%HypreParamsInt(6) = get_integer_param(region%myrank,'GMRES_PRINT_LEVEL',first_param_ptr,default=0)    
    input%HypreParamsInt(7) = get_integer_param(region%myrank,'PRECOND_PRINT_LEVEL',first_param_ptr,default=0)    
    input%HypreParamsInt(8) = get_integer_param(region%myrank,'PARASAILS_NLEVELS',first_param_ptr,default=1)    
    ! ... Reals
    input%HypreParamsReal(1) = get_real_param(region%myrank,'GMRES_TOLERANCE',first_param_ptr,default=0.00000001_WP)
    input%HypreParamsReal(2) = get_real_param(region%myrank,'PARASAILS_THRESHOLD',first_param_ptr,default=0.1_WP)
    input%HypreParamsReal(3) = get_real_param(region%myrank,'PARASAILS_FILTERLEVEL',first_param_ptr,default=0.05_WP)

    ! ... write subiteration data
    input%writeSubItData = get_integer_param(region%myrank,'WRITE_SUBITERATION_DATA',first_param_ptr,default = FALSE)

    ! ... LHS artificial dissipation
    input%Implicit_SAT_Art_Diss = get_integer_param(region%myrank,'IMPLICIT_SAT_ARTIFICIAL_DISSIPATION',first_param_ptr,default = FALSE)
    input%Amount_Implicit_SAT_Art_Diss = get_real_param(region%myrank,'AMOUNT_IMPLICIT_SAT_ARTIFICIAL_DISSIPATION',first_param_ptr,default = 0.0_WP)

    ! ... LHS filter
    input%LHS_Filter = get_integer_param(region%myrank,'LHS_FILTER',first_param_ptr,default=TRUE)
    input%LHS_Filter_Fac = get_real_param(region%myrank,'LHS_FILTER_FAC',first_param_ptr,default=1.00_WP)
    input%LHS_Filter_AlphaF = get_real_param(region%myrank,'LHS_FILTER_ALPHAF',first_param_ptr,default=0.499_WP)

    ! ... convergence flag
    input%Converged = FALSE

    input%CheckingLevel = get_integer_param(region%myrank,'DATALIMIT_CHECKING_LEVEL',first_param_ptr,default=0)
    input%DataLimit_LOWER_RHO = get_real_param(region%myrank,'DATALIMIT_LOWER_RHO',first_param_ptr,default=0.0_WP)
    input%DataLimit_UPPER_RHO = get_real_param(region%myrank,'DATALIMIT_UPPER_RHO',first_param_ptr,default=10.0_WP)
    input%DataLimit_SPEED     = get_real_param(region%myrank,'DATALIMIT_SPEED',first_param_ptr,default=7.0_WP)
    input%DataLimit_LOWER_ENERGY = get_real_param(region%myrank,'DATALIMIT_LOWER_ENERGY',first_param_ptr,default=0.0_WP)
    input%DataLimit_UPPER_ENERGY = get_real_param(region%myrank,'DATALIMIT_UPPER_ENERGY',first_param_ptr,default=25.0_WP)
    input%FailOnDataError = get_logical_param(region%myrank,'DATALIMIT_FAIL_ON_VIOLATION',first_param_ptr,default=.FALSE.)

    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! ...
    ! ... finite volume-related
    ! ...
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    ! ... string of ones and zeros denoted which grid will be FD or FV
    ! ... ones correspond to FV and zeros correspond to FD
    call set_param(region%myrank,input%fv_grid_array,'FV_GRID_ARRAY',first_param_ptr,default='0000000000000000000')

    input%fv_grid(:) = FINITE_DIF 
    do i = 1, LEN_TRIM((input%fv_grid_array))
      if (input%fv_grid_array(i:i)=='0') then
        input%fv_grid(i) = FINITE_DIF
      else if (input%fv_grid_array(i:i)=='1') then
        input%fv_grid(i) = FINITE_VOL
      end if
    end do

    ! ... only set if any grids are finite volume
    if (sum(input%fv_grid(:)) /= 0) then

      ! ... read finite-volume options
      input%fv_recon = get_integer_param(region%myrank,'FV_RECONSTRUCTION_SCHEME',first_param_ptr,default=PIECE_CNST)
      input%fv_flux  = get_integer_param(region%myrank,'FV_FLUX',first_param_ptr,default=HLLC)

      ! ... error check FV reconstruction scheme
      Select Case (input%fv_recon)
      Case (PIECE_CNST,THIRD_ORDER_MUSCL)
        input%reconstruction_scheme = input%fv_recon
      Case DEFAULT
        Call graceful_exit(region%myrank, 'ERROR: Invalid FV_RECONSTRUCTION_SCHEME Entry')
      End Select

      ! ... error check whether we are using 2nd order FD for metric consistency
      If (input%spaceOrder /= 2 .or. input%spaceDiscr /= EXPLICIT) Then
        Call graceful_exit(region%myrank, 'ERROR: FV Scheme needs 2nd order explicit FD approximation for metrics.')
      End If

      ! ... read finite-volume periodicity
      input%fv_periodic(1) = get_integer_param(region%myrank,'FV_PERIODIC1',first_param_ptr,default=FALSE)
      input%fv_periodic(2) = get_integer_param(region%myrank,'FV_PERIODIC2',first_param_ptr,default=FALSE)
      input%fv_periodic(3) = get_integer_param(region%myrank,'FV_PERIODIC3',first_param_ptr,default=FALSE)

    end if

    ! ... delete linked list
    ! call destroy_param(first_param_ptr)

    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! ... define operators
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    ! ... integer indices for derivative operators
    input%iFirstDeriv  = 1
    input%iFirstDerivBiased = MAX_OPERATORS
    input%iSecondDeriv = 2
    input%iFourthDeriv = FALSE ! ... not on by default

    ! ... integer indices for SAT dissipation operators
    input%iSATArtDiss = FALSE ! ... not on by default
    input%iSATArtDissSplit = FALSE ! ... not on by default

    ! ... "normal" artificial dissipation operators
    input%iArtDiss = FALSE ! ... not on by default


    ! filter frequency, JByun 03/2014
    input%filt_freq = get_integer_param(region%myrank,'FREQ_FILTER_ITER',first_param_ptr,default = 1)
 
    ! to deal with more than one filter in the code, JKim 09/2007
    input%numFilter    = 0  ! by default, no filter
    input%iFilter      = FALSE ! by default, no filter
    input%iFilterTest  = FALSE ! by default, no test filter for dynamic LES model
    input%iFilterGaussianHyperviscosity = FALSE ! by default, no truncated Gaussian filter
    if (input%filter .eqv. .true.) then ! use centered Pade filter on conserved variables for stability purpose
      input%numFilter   = input%numFilter + 1
      input%iFilter     = 20
    end if ! input%filter
    if (input%LES >= 200 .AND. input%LES < 300) then ! Dynamic LES model using a test filter, JKim 12/2007
      input%numFilter   = input%numFilter + 1
      input%iFilterTest = 21
      call set_param(region%myrank,input%LESAvgDir,'LES_AvgDir',first_param_ptr,default='averaging.inp')
    end if ! input%LES
    if (input%shock == TRUE) then ! explicit truncated Gaussian filter for hyperviscosity shock capturing
      input%numFilter = input%numFilter + 1
      input%iFourthDeriv = 4
      input%iFilterGaussianHyperviscosity = 22
      input%iFilterShockTG  = 22
    end if ! input%shock
    if (input%shock == 3) then
      input%numFilter                  = input%numFilter + 1
      input%iShockFilterFluxForward    = 23
      input%numFilter                  = input%numFilter + 1
      input%iShockFilterFluxBackward   = 24
      input%iShockFilterCoeffForward   = 25
      input%iShockFilterCoeffBackward  = 26
      input%iShockFilterDetect         = 27
      input%iShockFilterSensorForward  = 28
      input%iShockFilterSensorBackward = 29
      input%iShockFilterCoeffForward   = 30
      input%iShockFilterCoeffBackward  = 31
    end if

    ! ... spatial operators for time implicit
    if (input%timeScheme == BDF_GMRES_IMPLICIT) then
      input%iFirstDerivImplicit = 32
      input%iSecondDerivImplicit = 33
      input%iSecondDerivImplicitArtDiss = 34
      input%iLHS_Filter = 35
    end if

    ! ... gas models
    input%gas_dv_model = get_integer_param(region%myrank,'GAS_EQUATION_OF_STATE',first_param_ptr,default=DV_MODEL_IDEALGAS)
    input%gas_tv_model = get_integer_param(region%myrank,'GAS_TRANSPORT_VAR_MODEL',first_param_ptr,default=TV_MODEL_POWERLAW)
    if (input%gas_tv_model == 1) then
      call graceful_exit(region%myrank, 'PlasComCM: ERROR: GAS_EQUATION_OF_STATE cannot be set to 1.')
    end if
    call set_param(region%myrank,input%TPTableName,'GAS_LOOKUP_TABLE_NAME',first_param_ptr,default='ThermPerf.tbl')
    call set_param(region%myrank,input%DVTableName,'DV_LOOKUP_TABLE_NAME',first_param_ptr,default='ThermPerf.tbl')
    call set_param(region%myrank,input%TVTableName,'TV_LOOKUP_TABLE_NAME',first_param_ptr,default='ThermPerf.tbl')

    ! ... post processing
    ! ... surface values
    input%PPTau = get_integer_param(region%myrank,'SURFACE_TRACTION',first_param_ptr,default=FALSE)
    input%PPYplus = get_integer_param(region%myrank,'SURFACE_YPLUS',first_param_ptr,default=FALSE)
    input%PPHtFlux = get_integer_param(region%myrank,'SURFACE_HEAT_FLUX',first_param_ptr,default=FALSE)
    input%PPReAnlg = get_integer_param(region%myrank,'REYNOLDS_ANALOGY',first_param_ptr,default=FALSE)

    ! ... volume values
    input%PPrhs = get_integer_param(region%myrank,'VOLUME_RHS',first_param_ptr,default=FALSE)
    input%PPTemp = get_integer_param(region%myrank,'VOLUME_TEMPERATURE',first_param_ptr,default=FALSE)
    input%PPPressure = get_integer_param(region%myrank,'VOLUME_PRESSURE',first_param_ptr,default=FALSE)
    input%PPVort = get_integer_param(region%myrank,'VOLUME_VORTICITY',first_param_ptr,default=FALSE)
    input%PPDilat = get_integer_param(region%myrank,'VOLUME_DILATATION',first_param_ptr,default=FALSE)

    ! ... plasma actuators
    input%use_plasma_actuator = get_integer_param(region%myrank,'USE_PLASMA_ACTUATORS',first_param_ptr,default=FALSE)
    input%actuator_loc_x = get_real_param(region%myrank,'ACTUATOR_LOC_X',first_param_ptr,default=1.0_WP)
    input%actuator_loc_y = get_real_param(region%myrank,'ACTUATOR_LOC_Y',first_param_ptr,default=1.0_WP)
    input%actuator_loc_z = get_real_param(region%myrank,'ACTUATOR_LOC_Z',first_param_ptr,default=1.0_WP)
    input%actuator_radius = get_real_param(region%myrank,'ACTUATOR_RADIUS',first_param_ptr,default=1.0_WP)
    input%actuator_length = get_real_param(region%myrank,'ACTUATOR_LENGTH',first_param_ptr,default=1.0_WP)
    input%actuator_efficiency = get_real_param(region%myrank,'ACTUATOR_EFFICIENCY',first_param_ptr,default=1.0_WP)
    input%actuator_max_power = get_real_param(region%myrank,'ACTUATOR_MAX_POWER',first_param_ptr,default=1.0_WP)
    input%actuator_duty_cycle = get_real_param(region%myrank,'ACTUATOR_DUTY_CYCLE',first_param_ptr,default=1.0_WP)
    input%actuator_frequency = get_real_param(region%myrank,'ACTUATOR_FREQUENCY',first_param_ptr,default=1.0_WP)
    input%actuator_delay = get_real_param(region%myrank,'ACTUATOR_DELAY',first_param_ptr,default=1.0_WP)
    input%actuator_rise_time = get_real_param(region%myrank,'ACTUATOR_RISE_TIME',first_param_ptr,default=1.0_WP)
    input%actuator_fall_time = get_real_param(region%myrank,'ACTUATOR_FALL_TIME',first_param_ptr,default=1.0_WP)
    input%actuator_sigma_xy = get_real_param(region%myrank,'ACTUATOR_SIGMA_XY',first_param_ptr,default=1.0_WP)
    input%actuator_sigma_z = get_real_param(region%myrank,'ACTUATOR_SIGMA_Z',first_param_ptr,default=1.0_WP)

   ! ... momentum Transfer and Radical actuators
    input%use_momentum_actuator = get_integer_param(region%myrank,'USE_MOMENTUM_ACTUATOR',first_param_ptr,default=FALSE)
    input%use_radical_actuator = get_integer_param(region%myrank,'USE_RADICAL_ACTUATOR',first_param_ptr,default=FALSE)
    input%momentum_frequency= get_real_param(region%myrank,'MOMENTUM_FREQUENCY',first_param_ptr,default=0.0_WP)
    input%Radical_rise_time = get_real_param(region%myrank,'RADICAL_RISE_TIME',first_param_ptr,default=0.0_WP) !fraction of period
    input%Radical_duty_halfcycle = get_real_param(region%myrank,'RADICAL_DUTY_HALFCYCLE',first_param_ptr,default=1.0_WP) !fraction
    input%Radical_delay = get_real_param(region%myrank,'RADICAL_DELAY',first_param_ptr,default=0.0_WP) !fraction of period
    call set_param(region%myrank,input%momentum_fname,'MOMENTUM_FNAME',first_param_ptr,default='momentumTransfer.dat')
    call set_param(region%myrank,input%eMag_fname,'EMAGNITUDE_FNAME',first_param_ptr,default='default.dat')
    call set_param(region%myrank,input%BolsigTableName,'BOLSIG_TABLE_FNAME',first_param_ptr,default='default.dat')

    ! ... thermomechanical
    input%TMSolve = get_integer_param(region%myrank,'USE_THERMOMECHANICAL_SOLVER',first_param_ptr,default=FALSE)
    if(input%TMSolve /= FALSE) then

       ! ... time advancement
       input%ThermalTimeScheme = get_integer_param(region%myrank,'THERMAL_TIME_ADV',first_param_ptr,default=DYNAMIC_SOLN)
       input%StructuralTimeScheme = get_integer_param(region%myrank,'STRUCTURAL_TIME_ADV',first_param_ptr,default=DYNAMIC_SOLN)

       ! ... solution frequency with respect to the fluid
       input%StructSolnFreq = get_integer_param(region%myrank,'STRUCTURAL_FREQUENCY',first_param_ptr,default=1)
       input%ThermalSolnFreq = get_integer_param(region%myrank,'THERMAL_FREQUENCY',first_param_ptr,default=1)

       ! ... thermomechanical only analysis?
       input%TMOnly = get_integer_param(region%myrank,'NO_FLUID',first_param_ptr,default=FALSE) 

       ! ... number of processors for TM problem
       input%numTMproc = get_integer_param(region%myrank,'NUMBER_TM_PROCESSORS',first_param_ptr,default=0)
       
       input%UseParMetis = get_integer_param(region%myrank,'USE_PARMETIS',first_param_ptr,default=FALSE)

       ! ... grid name
       call set_param(region%myrank,input%TMgrid_fname,'SOLID_GRID_FNAME',first_param_ptr,default='solid.xyz')

       ! ... restart
       input%TMrestart = get_integer_param(region%myrank,'RESTART_TM_SOLN',first_param_ptr,default=FALSE)
       call set_param(region%myrank,input%TMrestart_fname,'TM_RESTART_FNAME',first_param_ptr,default='RocFlo-CM.TM.00000000.restart') 
       call set_param(region%myrank,input%TMrestartdot_fname,'TM_RESTART_AUX_FNAME',first_param_ptr,default='RocFlo-CM.TM.00000000.restart') 

       ! ... read the user-desired parallelization topology
       input%TMparallelTopology = get_integer_param(region%myrank,'TM_PARALLEL_TOPOLOGY',first_param_ptr,default=CUBE)

       ! ... read the user-desired parallelization direction
       input%TMparDir = get_integer_param(region%myrank,'TM_PARDIR',first_param_ptr,default=0)

       ! ... read the user-supplied decomposition map
       input%useTMDecompMap = get_integer_param(region%myrank,'USE_TM_DECOMP_MAP',first_param_ptr,default=FALSE)
       if (input%useTMDecompMap == TRUE) call set_param(region%myrank,input%TMdecompMap,'TM_DECOMP_MAP',first_param_ptr,default='TMdecomp.map')

       ! ... name of text file containing boundary condition specification
       call set_param(region%myrank,input%TMbc_fname,'THERMOMECHANICAL_BC_FNAME',first_param_ptr,default='TMbc.dat')

       ! ... element type
       input%ElemType = get_integer_param(region%myrank,'ELEMENT_TYPE',first_param_ptr,default=QUADRATIC)

       ! ... read the user-supplied decomposition map
       input%useTMDecompMap = get_integer_param(region%myrank,'USE_TM_DECOMP_MAP',first_param_ptr,default=FALSE)
       if (input%useTMDecompMap == TRUE) call set_param(region%myrank,input%TMdecompMap,'TM_DECOMP_MAP',first_param_ptr,default='TMdecomp.map')

       ! ... initial conditions
       input%TMInitialTemp = get_real_param(region%myrank,'TM_INITIAL_TEMPERATURE',first_param_ptr,default=300.0_rfreal)
       ! ... initial velocity?
       input%TMInitialCondition = get_integer_param(region%myrank,'STRUCTURAL_INITIAL_CONDITION',first_param_ptr,default=FALSE)
       input%BeamMode = get_integer_param(region%myrank,'BEAM_MODE',first_param_ptr,default=0)
       input%TMInitialVelocity = get_real_param(region%myrank,'TM_INITIAL_VELOCITY',first_param_ptr,default=0.0_rfreal)
       
       ! ... pinned lower edges
       input%PinnedBC = get_integer_param(region%myrank,'PINNED_BOUNDARY_CONDITIONS',first_param_ptr,default=FALSE)

       ! ... temporal advancement
       input%ThermalTimeScheme = get_integer_param(region%myrank,'THERMAL_TIME_SCHEME',first_param_ptr,default = DYNAMIC_SOLN)
       input%StructuralTimeScheme = get_integer_param(region%myrank,'STRUCTURAL_TIME_SCHEME',first_param_ptr,default = DYNAMIC_SOLN)
       input%MaxTMSubits = get_integer_param(region%myrank,'MAX_TM_SUBITS',first_param_ptr,default = 10)

       ! ... constitutive model
       input%ConstModel = get_integer_param(region%myrank,'CONSTITUTIVE_MODEL',first_param_ptr,default = SVK)

       ! ... petsc
       input%PetscRtol = get_real_param(region%myrank,'PETSC_REL_TOLERANCE',first_param_ptr,default=1D-5)

       ! ... material properties
       input%YngMod = get_real_param(region%myrank,'YOUNGS_MODULUS',first_param_ptr,default=200.0D9)
       input%TMDensity = get_real_param(region%myrank,'TM_DENSITY',first_param_ptr,default=8000.0_rfreal)
       input%PsnRto = get_real_param(region%myrank,'POISSON_RATIO',first_param_ptr,default=0.27_rfreal)
       input%SpecHt = get_real_param(region%myrank,'SPECIFIC_HEAT',first_param_ptr,default=500.0_rfreal)
       input%ThrmCond = get_real_param(region%myrank,'THERMAL_CONDUCTIVITY',first_param_ptr,default=40.0_rfreal)
       input%alpha = get_real_param(region%myrank,'THERMAL_EXPANSION',first_param_ptr,default=5.33D-6)
       
       ! ... temperature dependent mechanical properties
       input%TempDpndMtl = get_integer_param(region%myrank,'TEMPERATURE_DEPENDENT',first_param_ptr,default = FALSE)
       if (input%TempDpndMtl == TRUE) then
          call set_param(region%myrank,input%prp_fname,'MATERIAL_PROP_FNAME',first_param_ptr,default='MaterialData.txt')
          input%nTblPts = get_integer_param(region%myrank,'NUM_PTS_IN_TABLE',first_param_ptr,default=100)
       end if
       
       ! ... data record
       input%SaveSolidData = get_integer_param(region%myrank,'SAVE_SOLID_DATA',first_param_ptr,default=FALSE)

    else

      input%TMOnly = FALSE

    end if ! ... TMSolve /= FALSE

    ! ... RHS forcing for temporal simulation
    input%TemporalRHSForcing = get_integer_param(region%myrank,'TEMPORAL_RHS_FORCING',first_param_ptr,default=FALSE)
    ! ... streamwise direction
    input%StrmDir = get_integer_param(region%myrank,'STREAMWISE_DIRECTION',first_param_ptr,default=1)
    input%NrmDir = get_integer_param(region%myrank,'WALL_NORMAL_DIRECTION',first_param_ptr,default=2)

    ! ================= !
    ! Jet in Cross Flow !
    ! ================= !
    If (input%caseID=='JETXFLOW') Then
       ! Jet in crossflow dimensions
       input%GRID1_LZ = get_real_param(region%myrank,'GRID1_LZ',first_param_ptr,default=0.0_rfreal)
       input%XJET = get_real_param(region%myrank,'JET_POSITION',first_param_ptr,default=0.0_rfreal)

       ! Laser ignition source
       input%useLaser = get_integer_param(region%myrank,'USE_LASER',first_param_ptr,default=FALSE)
       input%LASER_R  = get_real_param(region%myrank,'LASER_R',first_param_ptr,default=0.0_rfreal)
       input%LASER_L  = get_real_param(region%myrank,'LASER_L',first_param_ptr,default=0.0_rfreal)
       input%LASER_FP = get_real_param(region%myrank,'LASER_FP',first_param_ptr,default=0.0_rfreal)
    End If

    ! ... scaling data
    IF(input%caseID(1:7) == 'SCALING') THEN
       input%write_nothing = TRUE
       input%read_nothing = TRUE
    ENDIF

    ! Grid generation
    input%generateGrid    = get_integer_param(region%myrank,'GENERATE_GRID',first_param_ptr,default=0)
    IF(input%generateGrid == TRUE) THEN
       CALL SET_PARAM(region%myrank,input%geometryFileName,'GEOMETRY_FNAME',first_param_ptr,default='')
    ENDIF

    ! Old grid generation parameters (delete later)
    IF(input%generateGrid == TRUE) THEN
       CALL SET_PARAM(region%myrank,input%geometryName,'GEOMETRY_NAME',first_param_ptr,default='NULL')
    ENDIF
    input%numGenGrid  = get_integer_param(region%myrank,'GENGRID_NUMGRID',first_param_ptr,default=1)
    input%nxGenGrid   = get_integer_param(region%myrank,'GENGRID_NX',first_param_ptr,default=101)
    input%nyGenGrid   = get_integer_param(region%myrank,'GENGRID_NY',first_param_ptr,default=101)
    input%nzGenGrid   = get_integer_param(region%myrank,'GENGRID_NZ',first_param_ptr,default=101)
    if (input%generateGrid == TRUE) then
      input%xminGenGrid = get_real_param(region%myrank,'GENGRID_XMIN',first_param_ptr,default=-1.0_rfreal)
      input%xmaxGenGrid = get_real_param(region%myrank,'GENGRID_XMAX',first_param_ptr,default=1.0_rfreal)
      input%yminGenGrid = get_real_param(region%myrank,'GENGRID_YMIN',first_param_ptr,default=-1.0_rfreal)
      input%ymaxGenGrid = get_real_param(region%myrank,'GENGRID_YMAX',first_param_ptr,default=1.0_rfreal)
      input%zminGenGrid = get_real_param(region%myrank,'GENGRID_ZMIN',first_param_ptr,default=-1.0_rfreal)
      input%zmaxGenGrid = get_real_param(region%myrank,'GENGRID_ZMAX',first_param_ptr,default=1.0_rfreal)
    end if

  end subroutine read_input

  subroutine initialize_global_data(region, input)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    ! ... incoming variables
    TYPE(t_region), pointer :: region
    TYPE(t_mixt_input), pointer :: input

    ! ... local variables
    integer :: i, j, k

    region%global%rk_alloc = .true.
    
    ! Initialize hdf5_io variables here... 
    ! Before ModMain's call to DecomposeDomain!
    allocate(region%hdf5_io)
    region%hdf5_io%m_xmf = 12
    region%hdf5_io%m_HDFinitialized = .false.
    region%hdf5_io%m_info = MPI_INFO_NULL

    ! ... create mappings
    nullify(region%global%vMap); allocate(region%global%vMap(input%ND+2))
    do i = 1, input%ND+2
      region%global%vMap(i) = i
    end do

    nullify(region%global%t2Map); allocate(region%global%t2Map(input%ND,input%ND))
    do i = 1, input%ND
      do j = 1, input%ND
        region%global%t2Map(i,j) = (j-1)*input%ND + i
      end do
    end do

    nullify(region%global%t3Map); allocate(region%global%t3Map(input%ND,input%ND,input%ND))
    do i = 1, input%ND
      do j = 1, input%ND
        do k = 1, input%ND
          region%global%t3Map(i,j,k) = (k-1)*input%ND**2 + (j-1)*input%ND + i
        end do
      end do
    end do

    nullify(region%global%t2MapSymVisc); allocate(region%global%t2MapSymVisc(input%ND,input%ND));
    region%global%t2MapSymVisc = 0 ! JKim 05/14/2007
    do i = 1, input%ND
      do j = 1, input%ND
        region%global%t2MapSymVisc(i,j) = (i - 1) + j
      end do ! j
      region%global%t2MapSymVisc(i,i) = 1 ! only store a single diagonal
    end do ! i

    ! ... for 2nd order symmetric tensor indexing, JKim 01/2008
    nullify(region%global%t2MapSym); allocate(region%global%t2MapSym(input%ND,input%ND))
    do i = 1,input%ND
      do j = 1,input%ND
        region%global%t2MapSym(i,j) = i + j + (input%ND - 2)
      end do ! j
      region%global%t2MapSym(i,i) = i ! only store a single diagonal
    end do ! i

    ! ... Kronecker delta
    nullify(region%global%delta); allocate(region%global%delta(input%ND,input%ND))
    region%global%delta = 0.0_rfreal
    do i = 1, input%ND
      region%global%delta(i,i) = 1.0_rfreal
    end do

  end subroutine initialize_global_data

  subroutine initialize_deriv_stencils_SBP_4_8_diag(region, input)

    USE ModGlobal
    USE ModDataStruct

    type(t_region), pointer :: region
    type(t_mixt_input), pointer :: input

    ! ... local variables
    real(rfreal) :: lhs_alpha, lhs_beta
    real(rfreal) :: rhs_a, rhs_b, rhs_c, rhs_d, rhs_e, rhs_f, rhs_g, rhs_h, rhs_i, rhs_j, rhs_k
    real(rfreal) :: rhs_l
    real(rfreal) :: r67, r68, r78
    integer :: i

    !----------------------------------------------------------------------
    ! derivative stencils
    !----------------------------------------------------------------------
    ! ... initialize
    input%overlap(input%iFirstDeriv) = 6
    input%operator_type(input%iFirstDeriv) = EXPLICIT
    input%max_operator_width(input%iFirstDeriv) = 12
    input%operator_bndry_width(input%iFirstDeriv) = 12
    input%operator_bndry_depth(input%iFirstDeriv) = 8
    input%rhs_interior_stencil_start(input%iFirstDeriv) = -4
    input%rhs_interior_stencil_end(input%iFirstDeriv)   =  4

    ! ... interior first derivative :: 8th order :: SBP
    rhs_a = 0.8_rfreal
    rhs_b = -0.2_rfreal
    rhs_c = 4.0_rfreal / 105.0_rfreal
    rhs_d = -1.0_rfreal / 280.0_rfreal
    input%rhs_interior(input%iFirstDeriv,-4:4)  = (/ -rhs_d, -rhs_c, -rhs_b, -rhs_a, 0.0_rfreal, &
                                                      rhs_a,  rhs_b,  rhs_c,  rhs_d /)

    ! minimal bandwidth
    !r67 =  1714837.0_rfreal/4354560.0_rfreal
    !r68 = -1022551.0_rfreal/30481920.0_rfreal
    !r78 =  6445687.0_rfreal/8709120.0_rfreal

    ! minimal spectral radius
    r67 =  0.649_dp
    r68 = -0.104_dp
    r78 =  0.755_dp

    ! ... (i = 1) on boundary first derivative :: SBP
    input%rhs_block1(input%iFirstDeriv,1,1:12) = (/-2540160.0_dp/1498139.0_dp,                     &
         -142642467.0_dp/5992556.0_dp                                     &
         +50803200.0_dp/1498139.0_dp*r78+                                 &
         5080320.0_dp/1498139.0_dp*r67+25401600.0_dp/1498139.0_dp*r68,    &
         705710031.0_dp/5992556.0_dp-228614400.0_dp/1498139.0_dp*r78      &
         -25401600.0_dp/1498139.0_dp*r67-121927680.0_dp/1498139.0_dp*r68, &
         -3577778591.0_dp/17977668.0_dp+381024000.0_dp/1498139.0_dp*r78+  &
         50803200.0_dp/1498139.0_dp*r67+228614400.0_dp/1498139.0_dp*r68,  &
         203718909.0_dp/1498139.0_dp-254016000.0_dp/1498139.0_dp*r78-     &
         50803200.0_dp/1498139.0_dp*r67-203212800.0_dp/1498139.0_dp*r68,  &
         -32111205.0_dp/5992556.0_dp+25401600.0_dp/1498139.0_dp*r67+      &
         76204800.0_dp/1498139.0_dp*r68,-652789417.0_dp/17977668.0_dp+    &
         76204800.0_dp/1498139.0_dp*r78-5080320.0_dp/1498139.0_dp*r67,    &
         74517981.0_dp/5992556.0_dp-25401600.0_dp/1498139.0_dp*r78-       &
         5080320.0_dp/1498139.0_dp*r68, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp/)


    ! ... (i = 2) boundary first derivative :: SBP
    input%rhs_block1(input%iFirstDeriv,2,1:12) = (/ 142642467.0_dp/31004596.0_dp-                      &
         7257600.0_dp/1107307.0_dp*r78-725760.0_dp/1107307.0_dp*r67-          &
         3628800.0_dp/1107307.0_dp*r68, 0.0_dp, -141502371.0_dp/2214614.0_dp+ &
         91445760.0_dp/1107307.0_dp*r78+10886400.0_dp/1107307.0_dp*r67+       &
         50803200.0_dp/1107307.0_dp*r68, 159673719.0_dp/1107307.0_dp-         &
         203212800.0_dp/1107307.0_dp*r78-29030400.0_dp/1107307.0_dp*r67-      &
         127008000.0_dp/1107307.0_dp*r68, -1477714693.0_dp/13287684.0_dp+     &
         152409600.0_dp/1107307.0_dp*r78+                                     &
         32659200.0_dp/1107307.0_dp*r67+127008000.0_dp/1107307.0_dp*r68,      &
         11652351.0_dp/2214614.0_dp-17418240.0_dp/1107307.0_dp*r67-           &
         50803200.0_dp/1107307.0_dp*r68, 36069450.0_dp/1107307.0_dp-          &
         50803200.0_dp/1107307.0_dp*r78+3628800.0_dp/1107307.0_dp*r67,        &
         -536324953.0_dp/46506894.0_dp+17418240.0_dp/1107307.0_dp*r78+        &
         3628800.0_dp/1107307.0_dp*r68, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp/)


    ! ... (i = 3) boundary first derivative :: SBP
    input%rhs_block1(input%iFirstDeriv,3,1:12) = (/-18095129.0_dp/134148.0_dp+3628800.0_dp/20761.0_dp*r78&
         +403200.0_dp/20761.0_dp*r67+                                           &
         1935360.0_dp/20761.0_dp*r68, 47167457.0_dp/124566.0_dp-                &
         10160640.0_dp/20761.0_dp*r78-1209600.0_dp/20761.0_dp*r67-              &
         5644800.0_dp/20761.0_dp*r68, 0.0_dp, -120219461.0_dp/124566.0_dp+      &
         25401600.0_dp/20761.0_dp*r78+4032000.0_dp/20761.0_dp*r67+              &
         16934400.0_dp/20761.0_dp*r68, 249289259.0_dp/249132.0_dp-              &
         25401600.0_dp/20761.0_dp*r78                                           &
         -6048000.0_dp/20761.0_dp*r67-22579200.0_dp/20761.0_dp*r68,             &
         -2611503.0_dp/41522.0_dp+3628800.0_dp/20761.0_dp*r67+                  &
         10160640.0_dp/20761.0_dp*r68, -7149666.0_dp/20761.0_dp+                &
         10160640.0_dp/20761.0_dp*r78-806400.0_dp/20761.0_dp*r67,               &
         37199165.0_dp/290654.0_dp-3628800.0_dp/20761.0_dp*r78-                 &
         806400.0_dp/20761.0_dp*r68, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp/)

    ! ... (i = 4) boundary first derivative :: SBP
    input%rhs_block1(input%iFirstDeriv,4,1:12) = (/ 3577778591.0_dp/109619916.0_dp-               &
         54432000.0_dp/1304999.0_dp*r78-7257600.0_dp/1304999.0_dp*r67-   &
         32659200.0_dp/1304999.0_dp*r68, -159673719.0_dp/1304999.0_dp+   &
         203212800.0_dp/1304999.0_dp*r78+                                &
         29030400.0_dp/1304999.0_dp*r67+127008000.0_dp/1304999.0_dp*r68, &
         360658383.0_dp/2609998.0_dp-                                    &
         228614400.0_dp/1304999.0_dp*r78-36288000.0_dp/1304999.0_dp*r67- &
         152409600.0_dp/1304999.0_dp*r68, 0.0_dp,                        &
         -424854441.0_dp/5219996.0_dp+127008000.0_dp/1304999.0_dp*r78+   &
         36288000.0_dp/1304999.0_dp*r67+                                 &
         127008000.0_dp/1304999.0_dp*r68, 22885113.0_dp/2609998.0_dp-    &
         29030400.0_dp/1304999.0_dp*r67-                                 &
         76204800.0_dp/1304999.0_dp*r68, 158096578.0_dp/3914997.0_dp-    &
         76204800.0_dp/1304999.0_dp*r78+                                 &
         7257600.0_dp/1304999.0_dp*r67, -296462325.0_dp/18269986.0_dp+   &
         29030400.0_dp/1304999.0_dp*r78+                                 &
         7257600.0_dp/1304999.0_dp*r68, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp/)


    ! ... (i = 5) boundary first derivative :: SBP
    input%rhs_block1(input%iFirstDeriv,5,1:12) = (/ -203718909.0_dp/2096689.0_dp+               &
         36288000.0_dp/299527.0_dp*r78+                                &
         7257600.0_dp/299527.0_dp*r67+29030400.0_dp/299527.0_dp*r68,   &
         1477714693.0_dp/3594324.0_dp-152409600.0_dp/299527.0_dp*r78-  &
         32659200.0_dp/299527.0_dp*r67-127008000.0_dp/299527.0_dp*r68, &
         -747867777.0_dp/1198108.0_dp+228614400.0_dp/299527.0_dp*r78+  &
         54432000.0_dp/299527.0_dp*r67+203212800.0_dp/299527.0_dp*r68, &
         424854441.0_dp/1198108.0_dp-127008000.0_dp/299527.0_dp*r78-   &
         36288000.0_dp/299527.0_dp*r67-127008000.0_dp/299527.0_dp*r68, &
         0.0_dp,                                                       &
         -17380335.0_dp/1198108.0_dp+10886400.0_dp/299527.0_dp*r67+    &
         25401600.0_dp/299527.0_dp*r68,                                &
         -67080435.0_dp/1198108.0_dp+25401600.0_dp/299527.0_dp*r78-    &
         3628800.0_dp/299527.0_dp*r67,                                 &
         657798011.0_dp/25160268.0_dp-10886400.0_dp/299527.0_dp*r78-   &
         3628800.0_dp/299527.0_dp*r68,                                 &
         -2592.0_dp/299527.0_dp, 0.0_dp, 0.0_dp, 0.0_dp/)


    ! ... (i = 6) boundary first derivative :: SBP
    input%rhs_block1(input%iFirstDeriv,6,1:12) = (/ 1529105.0_dp/1237164.0_dp-                    &
         403200.0_dp/103097.0_dp*r67-                                    &
         1209600.0_dp/103097.0_dp*r68,                                   &
         -3884117.0_dp/618582.0_dp+1935360.0_dp/103097.0_dp*r67+         &
         5644800.0_dp/103097.0_dp*r68, 2611503.0_dp/206194.0_dp-         &
         3628800.0_dp/103097.0_dp*r67-10160640.0_dp/103097.0_dp*r68,     &
         -7628371.0_dp/618582.0_dp+3225600.0_dp/103097.0_dp*r67+         &
         8467200.0_dp/103097.0_dp*r68, 5793445.0_dp/1237164.0_dp-        &
         1209600.0_dp/103097.0_dp*r67-2822400.0_dp/103097.0_dp*r68,      &
         0.0_dp, 80640.0_dp/103097.0_dp*r67, 80640.0_dp/103097.0_dp*r68, &
         3072.0_dp/103097.0_dp, -288.0_dp/103097.0_dp, 0.0_dp, 0.0_dp/)


    ! ... (i = 7) boundary first derivative :: SBP
    input%rhs_block1(input%iFirstDeriv,7,1:12) = (/ 93255631.0_dp/8041092.0_dp-               &
         10886400.0_dp/670091.0_dp*r78+                              &
         725760.0_dp/670091.0_dp*r67, -36069450.0_dp/670091.0_dp+    &
         50803200.0_dp/670091.0_dp*r78-3628800.0_dp/670091.0_dp*r67, &
         64346994.0_dp/670091.0_dp-91445760.0_dp/670091.0_dp*r78+    &
         7257600.0_dp/670091.0_dp*r67, -158096578.0_dp/2010273.0_dp+ &
         76204800.0_dp/670091.0_dp*r78 -7257600.0_dp/670091.0_dp*r67,&
         67080435.0_dp/2680364.0_dp-25401600.0_dp/670091.0_dp*r78    &
         +3628800.0_dp/670091.0_dp*r67, -725760.0_dp/670091.0_dp*r67,&
         0.0_dp, 725760.0_dp/670091.0_dp*r78,                        &
         -145152.0_dp/670091.0_dp, 27648.0_dp/670091.0_dp,           &
         -2592.0_dp/670091.0_dp, 0.0_dp/)

    ! ... (i = 8) boundary first derivative :: SBP
   input%rhs_block1(input%iFirstDeriv,8,1:12) = (/ -3921999.0_dp/1079524.0_dp+                  &
         25401600.0_dp/5127739.0_dp*r78+                                 &
         5080320.0_dp/5127739.0_dp*r68,                                  &
         536324953.0_dp/30766434.0_dp-121927680.0_dp/5127739.0_dp*r78-   &
         25401600.0_dp/5127739.0_dp*r68, -334792485.0_dp/10255478.0_dp   &
         +228614400.0_dp/5127739.0_dp*r78+50803200.0_dp/5127739.0_dp*r68,&
         296462325.0_dp/10255478.0_dp-                                   &
         203212800.0_dp/5127739.0_dp*r78-50803200.0_dp/5127739.0_dp*r68, &
         -657798011.0_dp/61532868.0_dp+76204800.0_dp/5127739.0_dp*r78+   &
         25401600.0_dp/5127739.0_dp*r68,                                 &
         -5080320.0_dp/5127739.0_dp*r68, -5080320.0_dp/5127739.0_dp*r78, &
         0.0_dp, 4064256.0_dp/5127739.0_dp, -1016064.0_dp/5127739.0_dp,  &
         193536.0_dp/5127739.0_dp, -18144.0_dp/5127739.0_dp/)

    Call Mirror_block1_to_block2(input, input%iFirstDeriv)

    input%bndry_explicit_deriv(:) = 0.0_rfreal
    input%bndry_explicit_deriv(1:12) = input%rhs_block1(input%iFirstDeriv,1,1:12)

    ! ... interior second derivative :: SBP
    input%overlap(input%iSecondDeriv) = 4
    input%operator_type(input%iSecondDeriv) = EXPLICIT
    input%max_operator_width(input%iSecondDeriv) = 12
    input%operator_bndry_width(input%iSecondDeriv) = 12
    input%operator_bndry_depth(input%iSecondDeriv) = 8
    input%rhs_interior_stencil_start(input%iSecondDeriv) = -4
    input%rhs_interior_stencil_end(input%iSecondDeriv)   =  4


    input%rhs_interior(input%iSecondDeriv,-4:4) = (/ -1.0_dp/560.0_dp, 8.0_dp/315.0_dp, -1.0_dp/5.0_dp,        &
         8.0_dp/5.0_dp, -205.0_dp/72.0_dp  ,8.0_dp/5.0_dp, -1.0_dp/5.0_dp,           &
         8.0_dp/315.0_dp, -1.0_dp/560.0_dp /)

    ! ... (i = 1) on boundary second derivative :: SBP
    input%rhs_block1(input%iSecondDeriv,1,1:12) = (/ 4870382994799.0_dp/1358976868290.0_dp,                &
         -893640087518.0_dp/75498714905.0_dp, 926594825119.0_dp/60398971924.0_dp,    &
         -1315109406200.0_dp/135897686829.0_dp, 39126983272.0_dp/15099742981.0_dp,   &
         12344491342.0_dp/75498714905.0_dp, -451560522577.0_dp/2717953736580.0_dp,   &
         0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp /)


    ! ... (i = 2) boundary second derivative :: SBP
    input%rhs_block1(input%iSecondDeriv,2,1:12) = (/ 333806012194.0_dp/390619153855.0_dp,                  &
         -154646272029.0_dp/111605472530.0_dp, 1168338040.0_dp/33481641759.0_dp,     &
         82699112501.0_dp/133926567036.0_dp, -171562838.0_dp/11160547253.0_dp,       &
         -28244698346.0_dp/167408208795.0_dp, 11904122576.0_dp/167408208795.0_dp,    &
         -2598164715.0_dp/312495323084.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp /)

    ! ... (i = 3) boundary first derivative :: SBP
    input%rhs_block1(input%iSecondDeriv,3,1:12) =  (/ 7838984095.0_dp/52731029988.0_dp,                     &
         1168338040.0_dp/5649753213.0_dp, -88747895.0_dp/144865467.0_dp,             &
         423587231.0_dp/627750357.0_dp, -43205598281.0_dp/22599012852.0_dp,          &
         4876378562.0_dp/1883251071.0_dp, -5124426509.0_dp/3766502142.0_dp,          &
         10496900965.0_dp/39548272491.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp /)

    ! ... (i = 4) boundary first derivative :: SBP
    input%rhs_block1(input%iSecondDeriv,4,1:12) = (/ -94978241528.0_dp/828644350023.0_dp,                  &
         82699112501.0_dp/157837019052.0_dp, 1270761693.0_dp/13153084921.0_dp,       &
         -167389605005.0_dp/118377764289.0_dp, 48242560214.0_dp/39459254763.0_dp,    &
         -31673996013.0_dp/52612339684.0_dp, 43556319241.0_dp/118377764289.0_dp,     &
         -44430275135.0_dp/552429566682.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp /)

    ! ... (i = 5) boundary first derivative :: SBP
    input%rhs_block1(input%iSecondDeriv,5,1:12) = (/ 1455067816.0_dp/21132528431.0_dp,                     &
         -171562838.0_dp/3018932633.0_dp, -43205598281.0_dp/36227191596.0_dp,        &
         48242560214.0_dp/9056797899.0_dp, -52276055645.0_dp/6037865266.0_dp,        &
         57521587238.0_dp/9056797899.0_dp, -80321706377.0_dp/36227191596.0_dp,       &
         8078087158.0_dp/21132528431.0_dp, -1296.0_dp/299527.0_dp, 0.0_dp, 0.0_dp,   &
         0.0_dp /)

    ! ... (i = 6) boundary first derivative :: SBP
    input%rhs_block1(input%iSecondDeriv,6,1:12) = (/ 10881504334.0_dp/327321118845.0_dp,                   &
         -28244698346.0_dp/140280479505.0_dp,  4876378562.0_dp/9352031967.0_dp,      &
         -10557998671.0_dp/12469375956.0_dp, 57521587238.0_dp/28056095901.0_dp,      &
         -278531401019.0_dp/93520319670.0_dp, 73790130002.0_dp/46760159835.0_dp,     &
         -137529995233.0_dp/785570685228.0_dp, 2048.0_dp/103097.0_dp,                &
         -144.0_dp/103097.0_dp, 0.0_dp, 0.0_dp /)

    ! ... (i = 7) boundary first derivative :: SBP
    input%rhs_block1(input%iSecondDeriv,7,1:12) = (/ -135555328849.0_dp/8509847458140.0_dp,                &
         11904122576.0_dp/101307707835.0_dp, -5124426509.0_dp/13507694378.0_dp,      &
         43556319241.0_dp/60784624701.0_dp, -80321706377.0_dp/81046166268.0_dp,      &
         73790130002.0_dp/33769235945.0_dp, -950494905688.0_dp/303923123505.0_dp,    &
         239073018673.0_dp/141830790969.0_dp, -145152.0_dp/670091.0_dp,              &
         18432.0_dp/670091.0_dp, -1296.0_dp/670091.0_dp, 0.0_dp /)

    ! ... (i = 8) boundary first derivative :: SBP
    input%rhs_block1(input%iSecondDeriv,8,1:12) = (/0.0_dp, -2598164715.0_dp/206729925524.0_dp,            &
         10496900965.0_dp/155047444143.0_dp, -44430275135.0_dp/310094888286.0_dp,    &
         425162482.0_dp/2720130599.0_dp, -137529995233.0_dp/620189776572.0_dp,       &
         239073018673.0_dp/155047444143.0_dp, -144648000000.0_dp/51682481381.0_dp,   &
         8128512.0_dp/5127739.0_dp, -1016064.0_dp/5127739.0_dp,                      &
         129024.0_dp/5127739.0_dp, -9072.0_dp/5127739.0_dp /)

    ! ... diagonal

    Call Mirror_block1_to_block2(input, input%iSecondDeriv)

    ! ... interior fourth derivative---this is not SBP...
    input%iFourthDeriv = 4
    input%overlap(input%iFourthDeriv) = 4
    input%operator_type(input%iFourthDeriv) = EXPLICIT
    input%max_operator_width(input%iFourthDeriv) = 5
    input%operator_bndry_width(input%iFourthDeriv) = 5
    input%operator_bndry_depth(input%iFourthDeriv) = 3
    input%rhs_interior_stencil_start(input%iFourthDeriv) = -2
    input%rhs_interior_stencil_end(input%iFourthDeriv)   = 2
    input%rhs_interior(input%iFourthDeriv,-2:2) = (/ 1.0_dp, -4.0_dp, 6.0_dp, -4.0_dp, 1.0_dp /)  !... yes, these are correct
    input%rhs_block1(input%iFourthDeriv,1,1:5)  = (/ 1.0_dp, -4.0_dp, 6.0_dp, -4.0_dp, 1.0_dp /)
    input%rhs_block1(input%iFourthDeriv,2,1:5)  = (/ 1.0_dp, -4.0_dp, 6.0_dp, -4.0_dp, 1.0_dp /)
    input%rhs_block1(input%iFourthDeriv,3,1:5)  = (/ 1.0_dp, -4.0_dp, 6.0_dp, -4.0_dp, 1.0_dp /)
    Call Mirror_block1_to_block2(input, input%iFourthDeriv)


    ! ... diagonal
    input%SBP_Pdiag(1) =  1.0_rfreal
    input%SBP_invPdiag(1) = 1.0_rfreal / input%SBP_Pdiag(1)

    ! ... boundary blocks
    input%SBP_invPdiag_block1(1:8) = 1.0_dp / (/ 1498139.0_dp / 5080320.0_dp, &
                                                 1107307.0_dp /  725760.0_dp, &
                                                   20761.0_dp /   80640.0_dp, &
                                                 1304999.0_dp /  725760.0_dp, &
                                                  299527.0_dp /  725760.0_dp, &
                                                  103097.0_dp /   80640.0_dp, &
                                                  670091.0_dp /  725760.0_dp, &
                                                 5127739.0_dp / 5080320.0_dp /)

    ! ... boundary blocks
    do i = 1, 8
      input%SBP_invPdiag_block2(i) = input%SBP_invPdiag_block1(9-i)
    end do

    if ( input%SAT_Art_Diss .eqv. .true. ) then

      ! ... initialize
      input%iSATArtDiss = 10
      input%overlap(input%iSATArtDiss) = 3
      input%operator_type(input%iSATArtDiss) = EXPLICIT
      input%max_operator_width(input%iSATArtDiss) = 5
      input%operator_bndry_width(input%iSATArtDiss) = 5
      input%operator_bndry_depth(input%iSATArtDiss) = 3
      input%rhs_interior_stencil_start(input%iSATArtDiss) = -2
      input%rhs_interior_stencil_end(input%iSATArtDiss)   =  2
      input%rhs_interior(input%iSATArtDiss,-2:2) = (/ 1.0_dp, -4.0_dp, 6.0_dp, -4.0_dp, 1.0_dp /)
      input%rhs_block1(input%iSATArtDiss,1,1:5)  = (/ 1.0_dp, -4.0_dp, 6.0_dp, -4.0_dp, 1.0_dp /)
      input%rhs_block1(input%iSATArtDiss,2,1:5)  = (/ 1.0_dp, -4.0_dp, 6.0_dp, -4.0_dp, 1.0_dp /)
      input%rhs_block1(input%iSATArtDiss,3,1:5)  = (/ 1.0_dp, -4.0_dp, 6.0_dp, -4.0_dp, 1.0_dp /)
      Call Mirror_block1_to_block2(input, input%iSATArtDiss)


      ! ... initialize
      input%iSATArtDissSplit = 11
      input%overlap(input%iSATArtDissSplit) = 5
      input%operator_type(input%iSATArtDissSplit) = EXPLICIT
      input%max_operator_width(input%iSATArtDissSplit) = 10
      input%operator_bndry_width(input%iSATArtDissSplit) = 10
      input%operator_bndry_depth(input%iSATArtDissSplit) = 8
      input%rhs_interior_stencil_start(input%iSATArtDissSplit) = -2
      input%rhs_interior_stencil_end(input%iSATArtDissSplit)   =  2
      input%rhs_interior(input%iSATArtDissSplit,-2:2)  = (/  1.0_dp, -4.0_dp,  6.0_dp, -4.0_dp,  1.0_dp /) * (-input%SBP_invPdiag(1))
      input%rhs_block1(input%iSATArtDissSplit,1,1:10)  = (/  1.0_dp,  1.0_dp,  1.0_dp,  0.0_dp,  0.0_dp,  0.0_dp,  0.0_dp,  0.0_dp,  0.0_dp,  0.0_dp /) * (-input%SBP_invPdiag_block1(1))
      input%rhs_block1(input%iSATArtDissSplit,2,1:10)  = (/ -4.0_dp, -4.0_dp, -4.0_dp,  1.0_dp,  0.0_dp,  0.0_dp,  0.0_dp,  0.0_dp,  0.0_dp,  0.0_dp /) * (-input%SBP_invPdiag_block1(2))
      input%rhs_block1(input%iSATArtDissSplit,3,1:10)  = (/  6.0_dp,  6.0_dp,  6.0_dp, -4.0_dp,  1.0_dp,  0.0_dp,  0.0_dp,  0.0_dp,  0.0_dp,  0.0_dp /) * (-input%SBP_invPdiag_block1(3))
      input%rhs_block1(input%iSATArtDissSplit,4,1:10)  = (/ -4.0_dp, -4.0_dp, -4.0_dp,  6.0_dp, -4.0_dp,  1.0_dp,  0.0_dp,  0.0_dp,  0.0_dp,  0.0_dp /) * (-input%SBP_invPdiag_block1(4))
      input%rhs_block1(input%iSATArtDissSplit,5,1:10)  = (/  1.0_dp,  1.0_dp,  1.0_dp, -4.0_dp,  6.0_dp,  1.0_dp,  0.0_dp,  0.0_dp,  0.0_dp,  0.0_dp /) * (-input%SBP_invPdiag_block1(5))
      input%rhs_block1(input%iSATArtDissSplit,6,1:10)  = (/  0.0_dp,  0.0_dp,  0.0_dp,  1.0_dp, -4.0_dp,  6.0_dp, -4.0_dp,  1.0_dp,  0.0_dp,  0.0_dp /) * (-input%SBP_invPdiag_block1(6))
      input%rhs_block1(input%iSATArtDissSplit,7,1:10)  = (/  0.0_dp,  0.0_dp,  0.0_dp,  0.0_dp,  1.0_dp, -4.0_dp,  6.0_dp, -4.0_dp,  1.0_dp,  0.0_dp /) * (-input%SBP_invPdiag_block1(7))
      input%rhs_block1(input%iSATArtDissSplit,8,1:10)  = (/  0.0_dp,  0.0_dp,  0.0_dp,  0.0_dp,  0.0_dp,  1.0_dp, -4.0_dp,  6.0_dp, -4.0_dp,  1.0_dp /) * (-input%SBP_invPdiag_block1(8))
      Call Mirror_block1_to_block2(input, input%iSATArtDissSplit)

    end if

  end subroutine initialize_deriv_stencils_SBP_4_8_diag

  subroutine initialize_deriv_stencils_SBP_2_4_diag(input, opID)

    USE ModGlobal
    USE ModDataStruct

    type(t_mixt_input), pointer :: input
    integer :: opID

    ! ... local variables
    real(rfreal) :: lhs_alpha, lhs_beta
    real(rfreal) :: rhs_a, rhs_b, rhs_c, rhs_d, rhs_e, rhs_f, rhs_g, rhs_h, rhs_i, rhs_j, rhs_k
    real(rfreal) :: rhs_l
    real(rfreal) :: r67, r68, r78
    integer :: i

    !----------------------------------------------------------------------
    ! derivative stencils
    !----------------------------------------------------------------------
    ! ... initialize
    input%overlap(opID) = 2
    input%operator_type(opID) = EXPLICIT
    input%max_operator_width(opID) = 6
    input%operator_bndry_width(opID) = 6
    input%operator_bndry_depth(opID) = 4
    input%rhs_interior_stencil_start(opID) = -2
    input%rhs_interior_stencil_end(opID)   =  2

    ! ... interior first derivative :: 8th order :: SBP
    rhs_a =  8.0_rfreal / 12.0_rfreal
    rhs_b = -1.0_rfreal / 12.0_rfreal
    input%rhs_interior(opID,-2:2)  = (/ -rhs_b, -rhs_a, 0.0_rfreal, rhs_a,  rhs_b /)

    ! minimal bandwidth
    !r67 =  1714837.0_rfreal/4354560.0_rfreal
    !r68 = -1022551.0_rfreal/30481920.0_rfreal
    !r78 =  6445687.0_rfreal/8709120.0_rfreal

    ! minimal spectral radius
    r67 =  0.649_dp
    r68 = -0.104_dp
    r78 =  0.755_dp

    ! ... (i = 1) on boundary first derivative :: SBP
    input%rhs_block1(opID,1,1:6) = (/ -24.0_dp/17.0_dp, 59.0_dp/34.0_dp, -4.0_dp/17.0_dp, -3.0_dp/34.0_dp, 0.0_dp, 0.0_dp /)

    ! ... (i = 2) on boundary first derivative :: SBP
    input%rhs_block1(opID,2,1:6) = (/-0.5_dp, 0.0_dp, 0.5_dp, 0.0_dp, 0.0_dp, 0.0_dp /)

    ! ... (i = 3) on boundary first derivative :: SBP
    input%rhs_block1(opID,3,1:6) = (/ 4.0_dp/43.0_dp, -59.0_dp/86.0_dp, 0.0_dp, 59.0_dp/86.0_dp, -4.0_dp/43.0_dp, 0.0_dp /)

    ! ... (i = 4) on boundary first derivative :: SBP
    input%rhs_block1(opID,4,1:6) = (/ 3.0_dp/98.0_dp, 0.0_rfreal, -59.0_dp/98.0_dp, 0.0_dp, 32.0_dp/49.0_dp, -4.0_dp/49.0_dp /)

    Call Mirror_block1_to_block2(input, opID)

    ! ---- Change from C.O.'s version
    input%bndry_explicit_deriv(:) = 0.0_rfreal
    input%bndry_explicit_deriv(1:6) = input%rhs_block1(opID,1,1:6)
    ! ---------------------------------

    ! ... second derivative :: SBP
    input%overlap(input%iSecondDeriv) = 4
    input%operator_type(input%iSecondDeriv) = EXPLICIT
    input%max_operator_width(input%iSecondDeriv) = 6
    input%operator_bndry_width(input%iSecondDeriv) = 6
    input%operator_bndry_depth(input%iSecondDeriv) = 4
    input%rhs_interior_stencil_start(input%iSecondDeriv) = -2
    input%rhs_interior_stencil_end(input%iSecondDeriv)   =  2

    ! ... interior second derivative :: SBP
    input%rhs_interior(input%iSecondDeriv,-2:2) = (/ -1.0_dp/12.0_dp, 4.0_dp/3.0_dp, &
                                                     -5.0_dp/2.0_dp, &
                                                      4.0_dp/3.0_dp, -1.0_dp/12.0_dp /)


    ! ... (i = 1) on boundary second derivative :: SBP
    input%rhs_block1(input%iSecondDeriv,1,1:6) =  (/ 2.0_dp, -5.0_dp,  4.0_dp, -1.0_dp, 0.0_dp, 0.0_dp /)

    ! ... (i = 2) boundary second derivative :: SBP
    input%rhs_block1(input%iSecondDeriv,2,1:6) = (/ 1.0_dp, -2.0_dp, 1.0_dp, 0.0_dp, 0.0_dp, 0.0_dp /)

    ! ... (i = 3) boundary second derivative :: SBP
    input%rhs_block1(input%iSecondDeriv,3,1:6) = (/ -4.0_dp/43.0_dp, 59.0_dp/43.0_dp, &
                                                  -110.0_dp/43.0_dp,   &
                                                    59.0_dp/43.0_dp, -4.0_dp/43.0_dp, 0.0_dp /)

     ! ... (i = 4) boundary second derivative :: SBP
    input%rhs_block1(input%iSecondDeriv,4,1:6) = (/ -1.0_dp/49.0_dp, 0.0_dp, 59.0_dp/49.0_dp, &
                                       -118.0_dp/49.0_dp, 64.0_dp/49.0_dp, -4.0_dp/49.0_dp /)

    Call Mirror_block1_to_block2(input, input%iSecondDeriv)

    ! ... diagonal
    input%SBP_Pdiag(1) =  1.0_rfreal
    input%SBP_invPdiag(1) = 1.0_rfreal / input%SBP_Pdiag(1)

    ! ... boundary blocks
    input%SBP_invPdiag_block1(1:4) =  (/ 48.0_rfreal/17.0_rfreal, &
                                         48.0_rfreal/59.0_rfreal, &
                                         48.0_rfreal/43.0_rfreal, &
                                         48.0_rfreal/49.0_rfreal /)

    ! ... boundary blocks
    input%SBP_invPdiag_block2(1:4) =  (/ 48.0_rfreal/49.0_rfreal, &
                                         48.0_rfreal/43.0_rfreal, &
                                         48.0_rfreal/59.0_rfreal, &
                                         48.0_rfreal/17.0_rfreal /)

    ! ... interior fourth derivative---this is not SBP...
    input%iFourthDeriv = 4
    input%overlap(input%iFourthDeriv) = 4
    input%operator_type(input%iFourthDeriv) = EXPLICIT
    input%max_operator_width(input%iFourthDeriv) = 5
    input%operator_bndry_width(input%iFourthDeriv) = 5
    input%operator_bndry_depth(input%iFourthDeriv) = 3
    input%rhs_interior_stencil_start(input%iFourthDeriv) = -2
    input%rhs_interior_stencil_end(input%iFourthDeriv)   = 2
    input%rhs_interior(input%iFourthDeriv,-2:2) = (/ 1.0_dp, -4.0_dp, 6.0_dp, -4.0_dp, 1.0_dp /)  !... yes, these are correct
    input%rhs_block1(input%iFourthDeriv,1,1:5)  = (/ 1.0_dp, -4.0_dp, 6.0_dp, -4.0_dp, 1.0_dp /)
    input%rhs_block1(input%iFourthDeriv,2,1:5)  = (/ 1.0_dp, -4.0_dp, 6.0_dp, -4.0_dp, 1.0_dp /)
    input%rhs_block1(input%iFourthDeriv,3,1:5)  = (/ 1.0_dp, -4.0_dp, 6.0_dp, -4.0_dp, 1.0_dp /)
    Call Mirror_block1_to_block2(input, input%iFourthDeriv)


    if ( input%SAT_Art_Diss .eqv. .true. ) then

      ! ... initialize
      input%iSATArtDiss = 10
      input%overlap(input%iSATArtDiss) = 1
      input%operator_type(input%iSATArtDiss) = EXPLICIT
      input%max_operator_width(input%iSATArtDiss) = 3
      input%operator_bndry_width(input%iSATArtDiss) = 3
      input%operator_bndry_depth(input%iSATArtDiss) = 1
      input%rhs_interior_stencil_start(input%iSATArtDiss) = -1
      input%rhs_interior_stencil_end(input%iSATArtDiss)   =  1
      input%rhs_interior(input%iSATArtDiss,-1:1) = (/  1.0_dp, -2.0_dp, 1.0_dp /)
      input%rhs_block1(input%iSATArtDiss,1,1:3) = (/ 1.0_dp, -2.0_dp, 1.0_dp /)
      input%rhs_block2(input%iSATArtDiss,1,1:3) = (/ 1.0_dp, -2.0_dp, 1.0_dp /)


      ! ... initialize
      input%iSATArtDissSplit = 11
      input%overlap(input%iSATArtDissSplit) = 1
      input%operator_type(input%iSATArtDissSplit) = EXPLICIT
      input%max_operator_width(input%iSATArtDissSplit) = 5
      input%operator_bndry_width(input%iSATArtDissSplit) = 5
      input%operator_bndry_depth(input%iSATArtDissSplit) = 4
      input%rhs_interior_stencil_start(input%iSATArtDissSplit) = -1
      input%rhs_interior_stencil_end(input%iSATArtDissSplit)   =  1
      input%rhs_interior(input%iSATArtDissSplit,-1:1) = (/  1.0_dp, -2.0_dp,  1.0_dp /) * (-input%SBP_invPdiag(1))
      input%rhs_block1(input%iSATArtDissSplit,1,1:5)  = (/  1.0_dp,  1.0_dp,  0.0_dp,  0.0_dp,  0.0_dp /) * (-input%SBP_invPdiag_block1(1))
      input%rhs_block1(input%iSATArtDissSplit,2,1:5)  = (/ -2.0_dp, -2.0_dp,  1.0_dp,  0.0_dp,  0.0_dp /) * (-input%SBP_invPdiag_block1(2))
      input%rhs_block1(input%iSATArtDissSplit,3,1:5)  = (/  1.0_dp,  1.0_dp, -2.0_dp,  1.0_dp,  0.0_dp /) * (-input%SBP_invPdiag_block1(3))
      input%rhs_block1(input%iSATArtDissSplit,4,1:5)  = (/  0.0_dp,  0.0_dp,  1.0_dp, -2.0_dp,  1.0_dp /) * (-input%SBP_invPdiag_block1(4))

      input%rhs_block2(input%iSATArtDissSplit,1,1:5)  = (/  1.0_dp, -2.0_dp,  1.0_dp,  0.0_dp,  0.0_dp /) * (-input%SBP_invPdiag_block2(1))
      input%rhs_block2(input%iSATArtDissSplit,2,1:5)  = (/  0.0_dp,  1.0_dp, -2.0_dp,  1.0_dp,  1.0_dp /) * (-input%SBP_invPdiag_block2(2))
      input%rhs_block2(input%iSATArtDissSplit,3,1:5)  = (/  0.0_dp,  0.0_dp,  1.0_dp, -2.0_dp, -2.0_dp /) * (-input%SBP_invPdiag_block2(3))
      input%rhs_block2(input%iSATArtDissSplit,4,1:5)  = (/  0.0_dp,  0.0_dp,  0.0_dp,  1.0_dp,  1.0_dp /) * (-input%SBP_invPdiag_block2(4))

    end if

  end subroutine initialize_deriv_stencils_SBP_2_4_diag

  subroutine initialize_deriv_stencils_SBP_1_2_diag(input)

    USE ModGlobal
    USE ModDataStruct

    type(t_mixt_input), pointer :: input

    ! ... local variables
    real(rfreal) :: lhs_alpha, lhs_beta
    real(rfreal) :: rhs_a, rhs_b, rhs_c, rhs_d, rhs_e, rhs_f, rhs_g, rhs_h, rhs_i, rhs_j, rhs_k
    real(rfreal) :: rhs_l
    real(rfreal) :: r67, r68, r78
    integer :: i

    !----------------------------------------------------------------------
    ! derivative stencils
    !----------------------------------------------------------------------
    ! ... initialize
    input%overlap(input%iFirstDeriv) = 1
    input%operator_type(input%iFirstDeriv) = EXPLICIT
    input%max_operator_width(input%iFirstDeriv) = 3
    input%operator_bndry_width(input%iFirstDeriv) = 2
    input%operator_bndry_depth(input%iFirstDeriv) = 1
    input%rhs_interior_stencil_start(input%iFirstDeriv) = -1
    input%rhs_interior_stencil_end(input%iFirstDeriv)   =  1

    ! ... interior first derivative :: 2nd
    rhs_a = 0.5_rfreal
    input%rhs_interior(input%iFirstDeriv,-1:1)  = (/ -rhs_a, 0.0_rfreal, rhs_a /)

    ! ... (i = 1) on boundary first derivative
    input%rhs_block1(input%iFirstDeriv,1,1:2) = (/ -1.0_rfreal, 1.0_rfreal /)

    Call Mirror_block1_to_block2(input, input%iFirstDeriv)

    ! ... explicit expressions for first derivatives on the boundary
    input%bndry_explicit_deriv(:) = 0.0_rfreal
    input%bndry_explicit_deriv(1:2) = input%rhs_block1(input%iFirstDeriv,1,1:2)

    ! ... interior second derivative :: SBP
    input%overlap(input%iSecondDeriv) = 1
    input%operator_type(input%iSecondDeriv) = EXPLICIT
    input%max_operator_width(input%iSecondDeriv) = 3
    input%operator_bndry_width(input%iSecondDeriv) = 3
    input%operator_bndry_depth(input%iSecondDeriv) = 1
    input%rhs_interior_stencil_start(input%iSecondDeriv) = -1
    input%rhs_interior_stencil_end(input%iSecondDeriv)   =  1
    input%rhs_interior(input%iSecondDeriv,-1:1) = (/ 1.0_dp, -2.0_dp, 1.0_dp /)

    ! ... (i = 1) on boundary second derivative :: SBP
    input%rhs_block1(input%iSecondDeriv,1,1:3) = (/ 1.0_dp, -2.0_dp, 1.0_dp /)

    Call Mirror_block1_to_block2(input, input%iSecondDeriv)

  ! ... interior fourth derivative---this is not SBP...
    input%iFourthDeriv = 4
    input%overlap(input%iFourthDeriv) = 4
    input%operator_type(input%iFourthDeriv) = EXPLICIT
    input%max_operator_width(input%iFourthDeriv) = 5
    input%operator_bndry_width(input%iFourthDeriv) = 5
    input%operator_bndry_depth(input%iFourthDeriv) = 3
    input%rhs_interior_stencil_start(input%iFourthDeriv) = -2
    input%rhs_interior_stencil_end(input%iFourthDeriv)   = 2
    input%rhs_interior(input%iFourthDeriv,-2:2) = (/ 1.0_dp, -4.0_dp, 6.0_dp, -4.0_dp, 1.0_dp /)  !... yes, these are correct
    input%rhs_block1(input%iFourthDeriv,1,1:5)  = (/ 1.0_dp, -4.0_dp, 6.0_dp, -4.0_dp, 1.0_dp /)
    input%rhs_block1(input%iFourthDeriv,2,1:5)  = (/ 1.0_dp, -4.0_dp, 6.0_dp, -4.0_dp, 1.0_dp /)
    input%rhs_block1(input%iFourthDeriv,3,1:5)  = (/ 1.0_dp, -4.0_dp, 6.0_dp, -4.0_dp, 1.0_dp /)
    Call Mirror_block1_to_block2(input, input%iFourthDeriv)

    ! ... diagonal
    input%SBP_Pdiag(1) =  1.0_rfreal
    input%SBP_invPdiag(1) = 1.0_rfreal / input%SBP_Pdiag(1)

    ! ... boundary blocks
    input%SBP_invPdiag_block1(1) = 2.0_dp
    input%SBP_invPdiag_block2(1) = 2.0_dp

    if ( input%SAT_Art_Diss .eqv. .true. ) then

      ! ... initialize
      input%iSATArtDiss = 10
      input%overlap(input%iSATArtDiss) = 1
      input%operator_type(input%iSATArtDiss) = EXPLICIT
      input%max_operator_width(input%iSATArtDiss) = 3
      input%operator_bndry_width(input%iSATArtDiss) = 2
      input%operator_bndry_depth(input%iSATArtDiss) = 1
      input%rhs_interior_stencil_start(input%iSATArtDiss) = -1
      input%rhs_interior_stencil_end(input%iSATArtDiss)   =  1
      input%rhs_interior(input%iSATArtDiss,-1:1) = (/ -1.0_dp, 1.0_dp, 0.0_dp /)
      input%rhs_block1(input%iSATArtDiss,1,1:2) = (/ -1.0_dp, 1.0_dp /)
      input%rhs_block2(input%iSATArtDiss,1,1:2) = (/ -1.0_dp, 1.0_dp /)


      ! ... initialize
      input%iSATArtDissSplit = 11
      input%overlap(input%iSATArtDissSplit) = 1
      input%operator_type(input%iSATArtDissSplit) = EXPLICIT
      input%max_operator_width(input%iSATArtDissSplit) = 3
      input%operator_bndry_width(input%iSATArtDissSplit) = 3
      input%operator_bndry_depth(input%iSATArtDissSplit) = 2
      input%rhs_interior_stencil_start(input%iSATArtDissSplit) = -1
      input%rhs_interior_stencil_end(input%iSATArtDissSplit)   =  1
      input%rhs_interior(input%iSATArtDissSplit,-1:1) = (/ 0.0_dp,  1.0_dp, -1.0_dp /) * (-input%SBP_invPdiag(1))
      input%rhs_block1(input%iSATArtDissSplit,1,1:3) = (/ -1.0_dp, -1.0_dp,  0.0_dp /) * (-input%SBP_invPdiag_block1(1))
      input%rhs_block1(input%iSATArtDissSplit,2,1:3) = (/  1.0_dp,  1.0_dp, -1.0_dp /) * (-input%SBP_invPdiag(1))
      input%rhs_block2(input%iSATArtDissSplit,1,1:3) = (/  0.0_dp,  1.0_dp, -1.0_dp /) * (-input%SBP_invPdiag(1))
      input%rhs_block2(input%iSATArtDissSplit,2,1:3) = (/  0.0_dp,  0.0_dp,  1.0_dp /) * (-input%SBP_invPdiag_block2(1))

    end if

    Return

  end subroutine initialize_deriv_stencils_SBP_1_2_diag

  subroutine initialize_deriv_stencils_SBP_1_2_diag_implicit(input)

    USE ModGlobal
    USE ModDataStruct

    type(t_mixt_input), pointer :: input

    ! ... local variables
    real(rfreal) :: lhs_alpha, lhs_beta
    real(rfreal) :: rhs_a, rhs_b, rhs_c, rhs_d, rhs_e, rhs_f, rhs_g, rhs_h, rhs_i, rhs_j, rhs_k
    real(rfreal) :: rhs_l
    real(rfreal) :: r67, r68, r78
    integer :: i, opID

    !----------------------------------------------------------------------
    ! derivative stencils
    !----------------------------------------------------------------------
    ! ... initialize
    input%overlap(input%iFirstDerivImplicit) = 1
    input%operator_type(input%iFirstDerivImplicit) = EXPLICIT
    input%max_operator_width(input%iFirstDerivImplicit) = 3
    input%operator_bndry_width(input%iFirstDerivImplicit) = 2
    input%operator_bndry_depth(input%iFirstDerivImplicit) = 1
    input%rhs_interior_stencil_start(input%iFirstDerivImplicit) = -1
    input%rhs_interior_stencil_end(input%iFirstDerivImplicit)   =  1
    input%max_stencil_start(input%iFirstDerivImplicit) = -1
    input%max_stencil_end(input%iFirstDerivImplicit)   =  1

    ! ... interior first derivative :: 2nd
    rhs_a = 0.5_rfreal
    input%rhs_interior(input%iFirstDerivImplicit,-1:1)  = (/ -rhs_a, 0.0_rfreal, rhs_a /)

    ! ... (i = 1) on boundary first derivative
    input%rhs_block1(input%iFirstDerivImplicit,1,1:2) = (/ -1.0_rfreal, 1.0_rfreal /)

    Call Mirror_block1_to_block2(input, input%iFirstDerivImplicit)

    ! ... interior second derivative :: SBP
    input%overlap(input%iSecondDerivImplicit) = 1
    input%operator_type(input%iSecondDerivImplicit) = EXPLICIT
    input%max_operator_width(input%iSecondDerivImplicit) = 3
    input%operator_bndry_width(input%iSecondDerivImplicit) = 3
    input%operator_bndry_depth(input%iSecondDerivImplicit) = 1
    input%rhs_interior_stencil_start(input%iSecondDerivImplicit) = -1
    input%rhs_interior_stencil_end(input%iSecondDerivImplicit)   =  1
    input%max_stencil_start(input%iSecondDerivImplicit) = -2
    input%max_stencil_end(input%iSecondDerivImplicit)   =  2
    input%rhs_interior(input%iSecondDerivImplicit,-1:1) = (/ 1.0_dp, -2.0_dp, 1.0_dp /)

    ! ... (i = 1) on boundary second derivative :: SBP
    input%rhs_block1(input%iSecondDerivImplicit,1,1:3) = (/ 1.0_dp, -2.0_dp, 1.0_dp /)

    Call Mirror_block1_to_block2(input, input%iSecondDerivImplicit)

    ! ... interior second derivative :: SBP
    input%overlap(input%iSecondDerivImplicitArtDiss) = 1
    input%operator_type(input%iSecondDerivImplicitArtDiss) = EXPLICIT
    input%max_operator_width(input%iSecondDerivImplicitArtDiss) = 3
    input%operator_bndry_width(input%iSecondDerivImplicitArtDiss) = 3
    input%operator_bndry_depth(input%iSecondDerivImplicitArtDiss) = 1
    input%rhs_interior_stencil_start(input%iSecondDerivImplicitArtDiss) = -1
    input%rhs_interior_stencil_end(input%iSecondDerivImplicitArtDiss)   =  1
    input%max_stencil_start(input%iSecondDerivImplicitArtDiss) = -2
    input%max_stencil_end(input%iSecondDerivImplicitArtDiss)   =  2
    input%rhs_interior(input%iSecondDerivImplicitArtDiss,-1:1) = (/ 1.0_dp, -2.0_dp, 1.0_dp /)

    ! ... (i = 1) on boundary second derivative :: SBP
    input%rhs_block1(input%iSecondDerivImplicitArtDiss,1,1:3) = (/ 1.0_dp, -2.0_dp, 1.0_dp /)

    Call Mirror_block1_to_block2(input, input%iSecondDerivImplicitArtDiss)

    ! ... fill mapping array; must be consistent with stencil definition in hypre.c
    if (associated(input%HYPREStencilIndex) .eqv. .false.) then
       ! ... stencil different for inviscid and viscous
       if ((input%RE > 0.0_rfreal) .OR. (input%Implicit_SAT_Art_Diss == TRUE)) then
          allocate(input%HYPREStencilIndex(MAX_OPERATORS,-2:2,-2:2,-2:2))
       else
          allocate(input%HYPREStencilIndex(MAX_OPERATORS,-1:1,-1:1,-1:1))
       end if
      input%HYPREStencilIndex = 0
    end if

    ! ... first derivative (inviscid)
    opID = input%iFirstDerivImplicit
    input%HYPREStencilIndex(opID, 0, 0, 0) = 0   ! ... self point
    input%HYPREStencilIndex(opID,-1, 0, 0) = 1   ! ... i = -1
    input%HYPREStencilIndex(opID,+1, 0, 0) = 2   ! ... i = +1
    input%HYPREStencilIndex(opID, 0,-1, 0) = 3   ! ... j = -1
    input%HYPREStencilIndex(opID, 0,+1, 0) = 4   ! ... j = +1
    input%HYPREStencilIndex(opID, 0, 0,-1) = 5   ! ... k = -1
    input%HYPREStencilIndex(opID, 0, 0,+1) = 6   ! ... k = +1

    ! ! ... second derivative (viscous)
    ! opID = input%iSecondDerivImplicit
    ! input%HYPREStencilIndex(opID, 0, 0, 0) = 0   ! ... self point
    ! input%HYPREStencilIndex(opID,-1, 0, 0) = 1   ! ... i = -1
    ! input%HYPREStencilIndex(opID,+1, 0, 0) = 2   ! ... i = +1
    ! input%HYPREStencilIndex(opID, 0,-1, 0) = 3   ! ... j = -1
    ! input%HYPREStencilIndex(opID, 0,+1, 0) = 4   ! ... j = +1
    ! input%HYPREStencilIndex(opID, 0, 0,-1) = 5   ! ... k = -1
    ! input%HYPREStencilIndex(opID, 0, 0,+1) = 6   ! ... k = +1

!    if(myrank == 0) write(*,*) 'opID,input%HYPREStencilIndex(OpID,:,:,:)',opID,input%HYPREStencilIndex(OpID,:,:,:)

    ! ... second derivative (art diss)
    opID = input%iSecondDerivImplicitArtDiss
    input%HYPREStencilIndex(opID, 0, 0, 0) = 0   ! ... self point
    input%HYPREStencilIndex(opID,-1, 0, 0) = 1   ! ... i = -1
    input%HYPREStencilIndex(opID,+1, 0, 0) = 2   ! ... i = +1
    input%HYPREStencilIndex(opID, 0,-1, 0) = 3   ! ... j = -1
    input%HYPREStencilIndex(opID, 0,+1, 0) = 4   ! ... j = +1
    input%HYPREStencilIndex(opID, 0, 0,-1) = 5   ! ... k = -1
    input%HYPREStencilIndex(opID, 0, 0,+1) = 6   ! ... k = +1

    ! ... viscous or artificial dissipation
    if ((input%RE > 0.0_rfreal) .OR. (input%Implicit_SAT_Art_Diss == TRUE)) then

      ! ... first derivative
      opID = input%iFirstDerivImplicit
      input%HYPREStencilIndex(opID, 0, 0, 0) = 0
      input%HYPREStencilIndex(opID,-2, 0, 0) = 1
      input%HYPREStencilIndex(opID,-1, 0, 0) = 2
      input%HYPREStencilIndex(opID,+1, 0, 0) = 3
      input%HYPREStencilIndex(opID,+2, 0, 0) = 4
      input%HYPREStencilIndex(opID, 0,-2, 0) = 5
      input%HYPREStencilIndex(opID, 0,-1, 0) = 6
      input%HYPREStencilIndex(opID, 0,+1, 0) = 7
      input%HYPREStencilIndex(opID, 0,+2, 0) = 8
      input%HYPREStencilIndex(opID, 0, 0,-2) = 9
      input%HYPREStencilIndex(opID, 0, 0,-1) = 10
      input%HYPREStencilIndex(opID, 0, 0,+1) = 11
      input%HYPREStencilIndex(opID, 0, 0,+2) = 12

      ! ... second derivative
      opID = input%iSecondDerivImplicit
      input%HYPREStencilIndex(opID, 0, 0, 0) = 0
      input%HYPREStencilIndex(opID,-2, 0, 0) = 1
      input%HYPREStencilIndex(opID,-1, 0, 0) = 2
      input%HYPREStencilIndex(opID,+1, 0, 0) = 3
      input%HYPREStencilIndex(opID,+2, 0, 0) = 4
      input%HYPREStencilIndex(opID, 0,-2, 0) = 5
      input%HYPREStencilIndex(opID, 0,-1, 0) = 6
      input%HYPREStencilIndex(opID, 0,+1, 0) = 7
      input%HYPREStencilIndex(opID, 0,+2, 0) = 8
      input%HYPREStencilIndex(opID, 0, 0,-2) = 9
      input%HYPREStencilIndex(opID, 0, 0,-1) = 10
      input%HYPREStencilIndex(opID, 0, 0,+1) = 11
      input%HYPREStencilIndex(opID, 0, 0,+2) = 12

    end if

    Return

  end subroutine initialize_deriv_stencils_SBP_1_2_diag_implicit

  subroutine initialize_deriv_stencils_SBP_2_4_diag_implicit(input)

    USE ModGlobal
    USE ModDataStruct

    type(t_mixt_input), pointer :: input

    ! ... local variables
    real(rfreal) :: lhs_alpha, lhs_beta
    real(rfreal) :: rhs_a, rhs_b, rhs_c, rhs_d, rhs_e, rhs_f, rhs_g, rhs_h, rhs_i, rhs_j, rhs_k
    real(rfreal) :: rhs_l
    integer :: i, opID

    !----------------------------------------------------------------------
    ! derivative stencils
    !----------------------------------------------------------------------
    ! ... initialize
    input%overlap(input%iFirstDerivImplicit) = 2
    input%operator_type(input%iFirstDerivImplicit) = EXPLICIT
    input%max_operator_width(input%iFirstDerivImplicit) = 6
    input%operator_bndry_width(input%iFirstDerivImplicit) = 6
    input%operator_bndry_depth(input%iFirstDerivImplicit) = 4
    input%rhs_interior_stencil_start(input%iFirstDerivImplicit) = -2
    input%rhs_interior_stencil_end(input%iFirstDerivImplicit)   =  2
    input%max_stencil_start(input%iFirstDerivImplicit) = -6
    input%max_stencil_end(input%iFirstDerivImplicit)   =  6

    ! ... interior first derivative :: 8th order :: SBP
    rhs_a =  8.0_rfreal / 12.0_rfreal
    rhs_b = -1.0_rfreal / 12.0_rfreal
    input%rhs_interior(input%iFirstDerivImplicit,-2:2)  = (/ -rhs_b, -rhs_a, 0.0_rfreal, rhs_a,  rhs_b /)

    ! ... (i = 1) on boundary first derivative :: SBP
    input%rhs_block1(input%iFirstDerivImplicit,1,1:6) = (/ -24.0_dp/17.0_dp, 59.0_dp/34.0_dp, -4.0_dp/17.0_dp, -3.0_dp/34.0_dp, 0.0_dp, 0.0_dp /)

    ! ... (i = 2) on boundary first derivative :: SBP
    input%rhs_block1(input%iFirstDerivImplicit,2,1:6) = (/-0.5_dp, 0.0_dp, 0.5_dp, 0.0_dp, 0.0_dp, 0.0_dp /)

    ! ... (i = 3) on boundary first derivative :: SBP
    input%rhs_block1(input%iFirstDerivImplicit,3,1:6) = (/ 4.0_dp/43.0_dp, -59.0_dp/86.0_dp, 0.0_dp, 59.0_dp/86.0_dp, -4.0_dp/43.0_dp, 0.0_dp /)

    ! ... (i = 4) on boundary first derivative :: SBP
    input%rhs_block1(input%iFirstDerivImplicit,4,1:6) = (/ 3.0_dp/98.0_dp, 0.0_rfreal, -59.0_dp/98.0_dp, 0.0_dp, 32.0_dp/49.0_dp, -4.0_dp/49.0_dp /)

    Call Mirror_block1_to_block2(input, input%iFirstDerivImplicit)

    ! ... second derivative :: SBP
    input%overlap(input%iSecondDerivImplicit) = 4
    input%operator_type(input%iSecondDerivImplicit) = EXPLICIT
    input%max_operator_width(input%iSecondDerivImplicit) = 6
    input%operator_bndry_width(input%iSecondDerivImplicit) = 6
    input%operator_bndry_depth(input%iSecondDerivImplicit) = 4
    input%rhs_interior_stencil_start(input%iSecondDerivImplicit) = -2
    input%rhs_interior_stencil_end(input%iSecondDerivImplicit)   =  2
    input%max_stencil_start(input%iSecondDerivImplicit) = -6
    input%max_stencil_end(input%iSecondDerivImplicit)   =  6

    ! ... interior second derivative :: SBP
    input%rhs_interior(input%iSecondDerivImplicit,-2:2) = (/ -1.0_dp/12.0_dp, 4.0_dp/3.0_dp, &
                                                     -5.0_dp/2.0_dp, &
                                                      4.0_dp/3.0_dp, -1.0_dp/12.0_dp /)


    ! ... (i = 1) on boundary second derivative :: SBP
    input%rhs_block1(input%iSecondDerivImplicit,1,1:6) =  (/ 2.0_dp, -5.0_dp,  4.0_dp, -1.0_dp, 0.0_dp, 0.0_dp /)

    ! ... (i = 2) boundary second derivative :: SBP
    input%rhs_block1(input%iSecondDerivImplicit,2,1:6) = (/ 1.0_dp, -2.0_dp, 1.0_dp, 0.0_dp, 0.0_dp, 0.0_dp /)

    ! ... (i = 3) boundary second derivative :: SBP
    input%rhs_block1(input%iSecondDerivImplicit,3,1:6) = (/ -4.0_dp/43.0_dp, 59.0_dp/43.0_dp, -110.0_dp/43.0_dp, 59.0_dp/43.0_dp, -4.0_dp/43.0_dp, 0.0_dp /)

     ! ... (i = 4) boundary second derivative :: SBP
    input%rhs_block1(input%iSecondDerivImplicit,4,1:6) = (/ -1.0_dp/49.0_dp, 0.0_dp, 59.0_dp/49.0_dp, -118.0_dp/49.0_dp, 64.0_dp/49.0_dp, -4.0_dp/49.0_dp /)

    Call Mirror_block1_to_block2(input, input%iSecondDerivImplicit)

    ! ... fill mapping array; must be consistent with stencil definition in hypre.c
    ! ... if allocated already, but small, resize
    if (associated(input%HYPREStencilIndex) .eqv. .false.) then
     allocate(input%HYPREStencilIndex(MAX_OPERATORS,-6:6,-6:6,-6:6))
      input%HYPREStencilIndex = 0
    elseif ((associated(input%HYPREStencilIndex) .eqv. .true.) .and. &
         (size(input%HYPREStencilIndex,2) < 13) ) then
       deallocate(input%HYPREStencilIndex)
       allocate(input%HYPREStencilIndex(MAX_OPERATORS,-6:6,-6:6,-6:6))
       input%HYPREStencilIndex = 0
    end if

    ! ... first derivative
    opID = input%iFirstDerivImplicit
!!$   print*,opID,size(input%HYPREStencilIndex,1),size(input%HYPREStencilIndex,2),size(input%HYPREStencilIndex,3),size(input%HYPREStencilIndex,4)

    input%HYPREStencilIndex(opID, 0, 0, 0) = 0   ! ... self point
    input%HYPREStencilIndex(opID,-6, 0, 0) = 1   ! ... i = -6
    input%HYPREStencilIndex(opID,-5, 0, 0) = 2   ! ... i = -5
    input%HYPREStencilIndex(opID,-4, 0, 0) = 3   ! ... i = -4
    input%HYPREStencilIndex(opID,-3, 0, 0) = 4   ! ... i = -3
    input%HYPREStencilIndex(opID,-2, 0, 0) = 5   ! ... i = -2
    input%HYPREStencilIndex(opID,-1, 0, 0) = 6   ! ... i = -1
    input%HYPREStencilIndex(opID,+1, 0, 0) = 7   ! ... i = +1
    input%HYPREStencilIndex(opID,+2, 0, 0) = 8   ! ... i = +2
    input%HYPREStencilIndex(opID,+3, 0, 0) = 9   ! ... i = +3
    input%HYPREStencilIndex(opID,+4, 0, 0) = 10  ! ... i = +4
    input%HYPREStencilIndex(opID,+5, 0, 0) = 11  ! ... i = +5
    input%HYPREStencilIndex(opID,+6, 0, 0) = 12  ! ... i = +6
    input%HYPREStencilIndex(opID, 0,-6, 0) = 13  ! ... j = -6
    input%HYPREStencilIndex(opID, 0,-5, 0) = 14  ! ... j = -5
    input%HYPREStencilIndex(opID, 0,-4, 0) = 15  ! ... j = -4
    input%HYPREStencilIndex(opID, 0,-3, 0) = 16  ! ... j = -3
    input%HYPREStencilIndex(opID, 0,-2, 0) = 17  ! ... j = -2
    input%HYPREStencilIndex(opID, 0,-1, 0) = 18  ! ... j = -1
    input%HYPREStencilIndex(opID, 0,+1, 0) = 19  ! ... j = +1
    input%HYPREStencilIndex(opID, 0,+2, 0) = 20  ! ... j = +2
    input%HYPREStencilIndex(opID, 0,+3, 0) = 21  ! ... j = +3
    input%HYPREStencilIndex(opID, 0,+4, 0) = 22  ! ... j = +4
    input%HYPREStencilIndex(opID, 0,+5, 0) = 23  ! ... j = +5
    input%HYPREStencilIndex(opID, 0,+6, 0) = 24  ! ... j = +6
    input%HYPREStencilIndex(opID, 0, 0,-6) = 25  ! ... k = -6
    input%HYPREStencilIndex(opID, 0, 0,-5) = 26  ! ... k = -5
    input%HYPREStencilIndex(opID, 0, 0,-4) = 27  ! ... k = -4
    input%HYPREStencilIndex(opID, 0, 0,-3) = 28  ! ... k = -3
    input%HYPREStencilIndex(opID, 0, 0,-2) = 29  ! ... k = -2
    input%HYPREStencilIndex(opID, 0, 0,-1) = 30  ! ... k = -1
    input%HYPREStencilIndex(opID, 0, 0,+1) = 31  ! ... k = +1
    input%HYPREStencilIndex(opID, 0, 0,+2) = 32  ! ... k = +2
    input%HYPREStencilIndex(opID, 0, 0,+3) = 33  ! ... k = +3
    input%HYPREStencilIndex(opID, 0, 0,+4) = 34  ! ... k = +4
    input%HYPREStencilIndex(opID, 0, 0,+5) = 35  ! ... k = +5
    input%HYPREStencilIndex(opID, 0, 0,+6) = 36  ! ... k = +6

    ! ... second derivative
    opID = input%iSecondDerivImplicit
    input%HYPREStencilIndex(opID, 0, 0, 0) = 0   ! ... self point
    input%HYPREStencilIndex(opID,-6, 0, 0) = 1   ! ... i = -6
    input%HYPREStencilIndex(opID,-5, 0, 0) = 2   ! ... i = -5
    input%HYPREStencilIndex(opID,-4, 0, 0) = 3   ! ... i = -4
    input%HYPREStencilIndex(opID,-3, 0, 0) = 4   ! ... i = -3
    input%HYPREStencilIndex(opID,-2, 0, 0) = 5   ! ... i = -2
    input%HYPREStencilIndex(opID,-1, 0, 0) = 6   ! ... i = -1
    input%HYPREStencilIndex(opID,+1, 0, 0) = 7   ! ... i = +1
    input%HYPREStencilIndex(opID,+2, 0, 0) = 8   ! ... i = +2
    input%HYPREStencilIndex(opID,+3, 0, 0) = 9   ! ... i = +3
    input%HYPREStencilIndex(opID,+4, 0, 0) = 10  ! ... i = +4
    input%HYPREStencilIndex(opID,+5, 0, 0) = 11  ! ... i = +5
    input%HYPREStencilIndex(opID,+6, 0, 0) = 12  ! ... i = +6
    input%HYPREStencilIndex(opID, 0,-6, 0) = 13  ! ... j = -6
    input%HYPREStencilIndex(opID, 0,-5, 0) = 14  ! ... j = -5
    input%HYPREStencilIndex(opID, 0,-4, 0) = 15  ! ... j = -4
    input%HYPREStencilIndex(opID, 0,-3, 0) = 16  ! ... j = -3
    input%HYPREStencilIndex(opID, 0,-2, 0) = 17  ! ... j = -2
    input%HYPREStencilIndex(opID, 0,-1, 0) = 18  ! ... j = -1
    input%HYPREStencilIndex(opID, 0,+1, 0) = 19  ! ... j = +1
    input%HYPREStencilIndex(opID, 0,+2, 0) = 20  ! ... j = +2
    input%HYPREStencilIndex(opID, 0,+3, 0) = 21  ! ... j = +3
    input%HYPREStencilIndex(opID, 0,+4, 0) = 22  ! ... j = +4
    input%HYPREStencilIndex(opID, 0,+5, 0) = 23  ! ... j = +5
    input%HYPREStencilIndex(opID, 0,+6, 0) = 24  ! ... j = +6
    input%HYPREStencilIndex(opID, 0, 0,-6) = 25  ! ... k = -6
    input%HYPREStencilIndex(opID, 0, 0,-5) = 26  ! ... k = -5
    input%HYPREStencilIndex(opID, 0, 0,-4) = 27  ! ... k = -4
    input%HYPREStencilIndex(opID, 0, 0,-3) = 28  ! ... k = -3
    input%HYPREStencilIndex(opID, 0, 0,-2) = 29  ! ... k = -2
    input%HYPREStencilIndex(opID, 0, 0,-1) = 30  ! ... k = -1
    input%HYPREStencilIndex(opID, 0, 0,+1) = 31  ! ... k = +1
    input%HYPREStencilIndex(opID, 0, 0,+2) = 32  ! ... k = +2
    input%HYPREStencilIndex(opID, 0, 0,+3) = 33  ! ... k = +3
    input%HYPREStencilIndex(opID, 0, 0,+4) = 34  ! ... k = +4
    input%HYPREStencilIndex(opID, 0, 0,+5) = 35  ! ... k = +5
    input%HYPREStencilIndex(opID, 0, 0,+6) = 36  ! ... k = +6

  end subroutine initialize_deriv_stencils_SBP_2_4_diag_implicit

  subroutine initialize_filter_stencils_PADE(myrank, input)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    integer :: myrank
    type(t_mixt_input), pointer :: input

    ! ... local variables
    real(rfreal) :: lhs_alpha, lhs_beta
    real(rfreal) :: rhs_a, rhs_b, rhs_c, rhs_d, rhs_e, rhs_f, rhs_g, rhs_h, rhs_i, rhs_j, rhs_k
    real(rfreal) :: rhs_l
    integer :: i

    !----------------------------------------------------------------------
    ! filter stencils
    !----------------------------------------------------------------------
    ! ... initialize
    input%overlap(input%iFilter) = 5
    input%operator_type(input%iFilter) = IMPLICIT
    input%max_operator_width(input%iFilter) = 11
    input%operator_bndry_depth(input%iFilter) = 5
    input%operator_bndry_width(input%iFilter) = 11
    input%lhs_interior_stencil_start(input%iFilter) = -1
    input%lhs_interior_stencil_end(input%iFilter)   =  1
    input%rhs_interior_stencil_start(input%iFilter) = -5
    input%rhs_interior_stencil_end(input%iFilter)   =  5

    ! ... filter free parameter
    lhs_beta  = 0.0_rfreal
    lhs_alpha = input%filter_alphaf

    ! ... interior filter
    rhs_a = (193.0_rfreal + 126.0_rfreal * lhs_alpha) / 256.0_rfreal
    rhs_b = (105.0_rfreal + 302.0_rfreal * lhs_alpha) / 256.0_rfreal
    rhs_c = 15.0_rfreal * (-1.0_rfreal + 2.0_rfreal * lhs_alpha) / 64.0_rfreal
    rhs_d = 45.0_rfreal * (1.0_rfreal - 2.0_rfreal * lhs_alpha) / 512.0_rfreal
    rhs_e = 5.0_rfreal * (-1.0_rfreal + 2.0_rfreal * lhs_alpha) / 256.0_rfreal
    rhs_f = (1.0_rfreal - 2.0_rfreal * lhs_alpha) / 512.0_rfreal
    input%lhs_interior(input%iFilter,-2:2) = (/ lhs_beta, lhs_alpha, 1.0_rfreal, lhs_alpha, lhs_beta /)
    input%rhs_interior(input%iFilter,-5:5) = (/ rhs_f, rhs_e, rhs_d, rhs_c, rhs_b, 2.0_rfreal * rhs_a, &
                                                rhs_b, rhs_c, rhs_d, rhs_e, rhs_f /) / 2.0_rfreal

    ! ... filter free parameter
    lhs_alpha = input%bndry_filter_alphaf

    ! ... (i = 1) on boundary filter :: no filtering
    input%lhs_block1(input%iFilter,1,0) = 1.0_rfreal
    input%rhs_block1(input%iFilter,1,1) = 1.0_rfreal

    ! ... (i = 2) boundary filter :: from Visbal report
    rhs_a = (1.0_rfreal + 1022.0_rfreal * lhs_alpha) / 1024.0_rfreal
    rhs_b = (507.0_rfreal + 10.0_rfreal * lhs_alpha) / 512.0_rfreal
    rhs_c = (45.0_rfreal + 934.0_rfreal * lhs_alpha) / 1024.0_rfreal
    rhs_d = 15.0_rfreal * (-1.0_rfreal + 2.0_rfreal * lhs_alpha) / 128.0_rfreal
    rhs_e = 105.0_rfreal * (1.0_rfreal - 2.0_rfreal * lhs_alpha) / 512.0_rfreal
    rhs_f = 63.0_rfreal * (-1.0_rfreal + 2.0_rfreal * lhs_alpha) / 256.0_rfreal
    rhs_g = 105.0_rfreal * (1.0_rfreal - 2.0_rfreal * lhs_alpha) / 512.0_rfreal
    rhs_h = 15.0_rfreal * (-1.0_rfreal + 2.0_rfreal * lhs_alpha) / 128.0_rfreal
    rhs_i = 45.0_rfreal * (1.0_rfreal - 2.0_rfreal * lhs_alpha) / 1024.0_rfreal
    rhs_j = 5.0_rfreal * (-1.0_rfreal + 2.0_rfreal * lhs_alpha) / 512.0_rfreal
    rhs_k = (1.0_rfreal - 2.0_rfreal * lhs_alpha) / 1024.0_rfreal
    input%lhs_block1(input%iFilter,2,-1:1) = (/ lhs_alpha, 1.0_rfreal, lhs_alpha /)
    input%rhs_block1(input%iFilter,2,1:11) = (/ rhs_a, rhs_b, rhs_c, rhs_d, rhs_e, rhs_f, rhs_g, rhs_h, rhs_i, rhs_j, rhs_k /)

    ! ... (i = 3) boundary filter :: from Visbal report
    rhs_a = (-1.0_rfreal + 2.0_rfreal * lhs_alpha) / 1024.0_rfreal
    rhs_b = (5.0_rfreal + 502.0_rfreal * lhs_alpha) / 512.0_rfreal
    rhs_c = (979.0_rfreal + 90.0_rfreal * lhs_alpha) / 1024.0_rfreal
    rhs_d = (15.0_rfreal + 98.0_rfreal * lhs_alpha) / 128.0_rfreal
    rhs_e = 105.0_rfreal * (-1.0_rfreal + 2.0_rfreal * lhs_alpha) / 512.0_rfreal
    rhs_f = 63.0_rfreal * (1.0_rfreal - 2.0_rfreal * lhs_alpha) / 256.0_rfreal
    rhs_g = 105.0_rfreal * (-1.0_rfreal + 2.0_rfreal * lhs_alpha) / 512.0_rfreal
    rhs_h = 15.0_rfreal * (1.0_rfreal - 2.0_rfreal * lhs_alpha) / 128.0_rfreal
    rhs_i = 45.0_rfreal * (-1.0_rfreal + 2.0_rfreal * lhs_alpha) / 1024.0_rfreal
    rhs_j = 5.0_rfreal * (1.0_rfreal - 2.0_rfreal * lhs_alpha) / 512.0_rfreal
    rhs_k = (-1.0_rfreal + 2.0_rfreal * lhs_alpha) / 1024.0_rfreal
    input%lhs_block1(input%iFilter,3,-1:1) = (/ lhs_alpha, 1.0_rfreal, lhs_alpha /)
    input%rhs_block1(input%iFilter,3,1:11) = (/ rhs_a, rhs_b, rhs_c, rhs_d, rhs_e, rhs_f, rhs_g, rhs_h, rhs_i, rhs_j, rhs_k /)

    ! ... (i = 4) boundary filter :: from Visbal report
    rhs_a = (1.0_rfreal - 2.0_rfreal * lhs_alpha) / 1024.0_rfreal
    rhs_b = 5.0_rfreal * (-1.0_rfreal + 2.0_rfreal * lhs_alpha) / 512.0_rfreal
    rhs_c = (45.0_rfreal + 934.0_rfreal * lhs_alpha) / 1024.0_rfreal
    rhs_d = (113.0_rfreal + 30.0_rfreal * lhs_alpha) / 128.0_rfreal
    rhs_e = (105.0_rfreal + 302.0_rfreal * lhs_alpha) / 512.0_rfreal
    rhs_f = 63.0_rfreal * (-1.0_rfreal + 2.0_rfreal * lhs_alpha) / 256.0_rfreal
    rhs_g = 105.0_rfreal * (1.0_rfreal - 2.0_rfreal * lhs_alpha) / 512.0_rfreal
    rhs_h = 15.0_rfreal * (-1.0_rfreal + 2.0_rfreal * lhs_alpha) / 128.0_rfreal
    rhs_i = 45.0_rfreal * (1.0_rfreal - 2.0_rfreal * lhs_alpha) / 1024.0_rfreal
    rhs_j = 5.0_rfreal * (-1.0_rfreal + 2.0_rfreal * lhs_alpha) / 512.0_rfreal
    rhs_k = (1.0_rfreal - 2.0_rfreal * lhs_alpha) / 1024.0_rfreal
    input%lhs_block1(input%iFilter,4,-1:1) = (/ lhs_alpha, 1.0_rfreal, lhs_alpha /)
    input%rhs_block1(input%iFilter,4,1:11) = (/ rhs_a, rhs_b, rhs_c, rhs_d, rhs_e, rhs_f, rhs_g, rhs_h, rhs_i, rhs_j, rhs_k /)

    ! ... (i = 5) boundary filter :: from Visbal report
    rhs_a = (-1.0_rfreal + 2.0_rfreal * lhs_alpha) / 1024.0_rfreal
    rhs_b = 5.0_rfreal * (1.0_rfreal - 2.0_rfreal * lhs_alpha) / 512.0_rfreal
    rhs_c = 45.0_rfreal * (-1.0_rfreal + 2.0_rfreal * lhs_alpha) / 1024.0_rfreal
    rhs_d = (15.0_rfreal + 98.0_rfreal * lhs_alpha) / 128.0_rfreal
    rhs_e = (407.0_rfreal + 210.0_rfreal * lhs_alpha) / 512.0_rfreal
    rhs_f = (63.0_rfreal + 130.0_rfreal * lhs_alpha) / 256.0_rfreal
    rhs_g = 105.0_rfreal * (-1.0_rfreal + 2.0_rfreal * lhs_alpha) / 512.0_rfreal
    rhs_h = 15.0_rfreal * (1.0_rfreal - 2.0_rfreal * lhs_alpha) / 128.0_rfreal
    rhs_i = 45.0_rfreal * (-1.0_rfreal + 2.0_rfreal * lhs_alpha) / 1024.0_rfreal
    rhs_j = 5.0_rfreal * (1.0_rfreal - 2.0_rfreal * lhs_alpha) / 512.0_rfreal
    rhs_k = (-1.0_rfreal + 2.0_rfreal * lhs_alpha) / 1024.0_rfreal
    input%lhs_block1(input%iFilter,5,-1:1) = (/ lhs_alpha, 1.0_rfreal, lhs_alpha /)
    input%rhs_block1(input%iFilter,5,1:11) = (/ rhs_a, rhs_b, rhs_c, rhs_d, rhs_e, rhs_f, rhs_g, rhs_h, rhs_i, rhs_j, rhs_k /)

    do i = 1, input%operator_bndry_depth(input%iFilter)
      input%lhs_block2(input%iFilter,i,-2:2) = input%lhs_block1(input%iFilter,input%operator_bndry_depth(input%iFilter)-(i-1),2:-2:-1)
    end do
    Call Mirror_block1_to_block2(input, input%iFilter)

    ! ... fill mapping array; must be consistent with stencil definition in hypre.c
    if (associated(input%HYPREStencilIndex) .eqv. .false.) then
      allocate(input%HYPREStencilIndex(MAX_OPERATORS,-2:2,-2:2,-2:2))
      input%HYPREStencilIndex = 0
    end if

    if (lhs_beta /= 0.0_rfreal) then
      call graceful_exit(myrank, 'ERROR: lhs_beta /= 0 in initialize_filter_stencils_PADE.')
    else
      input%HYPREStencilIndex(input%iFilter, :, :, :) = 0
      input%HYPREStencilIndex(input%iFilter, 0, 0, 0) = 0   ! ... self point
      input%HYPREStencilIndex(input%iFilter,-1, 0, 0) = 1   ! ... i = -1
      input%HYPREStencilIndex(input%iFilter,+1, 0, 0) = 2   ! ... i = +1
      input%HYPREStencilIndex(input%iFilter, 0,-1, 0) = 3   ! ... j = -1
      input%HYPREStencilIndex(input%iFilter, 0,+1, 0) = 4   ! ... j = +1
      input%HYPREStencilIndex(input%iFilter, 0, 0,-1) = 5   ! ... k = -1
      input%HYPREStencilIndex(input%iFilter, 0, 0,+1) = 6   ! ... k = +1
    end if

  end subroutine initialize_filter_stencils_PADE


  subroutine initialize_filter_stencils_PADE_new(myrank, input, rank_in)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    integer :: myrank
    type(t_mixt_input), pointer :: input
    integer :: rank_in

    ! ... local variables
    real(rfreal) :: lhs_alpha, lhs_beta
    real(rfreal) :: rhs_a, rhs_b, rhs_c, rhs_d, rhs_e, rhs_f, rhs_g, rhs_h, rhs_i, rhs_j, rhs_k
    real(rfreal) :: rhs_l
    integer :: i, bndry_num

    !----------------------------------------------------------------------
    ! filter stencils
    !----------------------------------------------------------------------
    ! ... initialize
    input%overlap(input%iFilter) = 5
    input%operator_type(input%iFilter) = IMPLICIT
    input%max_operator_width(input%iFilter) = 11
    input%operator_bndry_depth(input%iFilter) = 5
    input%operator_bndry_width(input%iFilter) = 11
    input%lhs_interior_stencil_start(input%iFilter) = -1
    input%lhs_interior_stencil_end(input%iFilter)   =  1
    input%rhs_interior_stencil_start(input%iFilter) = -5
    input%rhs_interior_stencil_end(input%iFilter)   =  5

    ! ... filter free parameter
    lhs_beta  = 0.0_rfreal
    lhs_alpha = input%filter_alphaf

    ! ... interior filter
    rhs_a = (193.0_rfreal + 126.0_rfreal * lhs_alpha) / 256.0_rfreal
    rhs_b = (105.0_rfreal + 302.0_rfreal * lhs_alpha) / 256.0_rfreal
    rhs_c = 15.0_rfreal * (-1.0_rfreal + 2.0_rfreal * lhs_alpha) / 64.0_rfreal
    rhs_d = 45.0_rfreal * (1.0_rfreal - 2.0_rfreal * lhs_alpha) / 512.0_rfreal
    rhs_e = 5.0_rfreal * (-1.0_rfreal + 2.0_rfreal * lhs_alpha) / 256.0_rfreal
    rhs_f = (1.0_rfreal - 2.0_rfreal * lhs_alpha) / 512.0_rfreal
    input%lhs_interior(input%iFilter,-2:2) = (/ lhs_beta, lhs_alpha, 1.0_rfreal, lhs_alpha, lhs_beta /)
    input%rhs_interior(input%iFilter,-5:5) = (/ rhs_f, rhs_e, rhs_d, rhs_c, rhs_b, 2.0_rfreal * rhs_a, &
                                                rhs_b, rhs_c, rhs_d, rhs_e, rhs_f /) / 2.0_rfreal

    ! ... now take care of boundaries
    do bndry_num = 1, 2
       call initialize_filter_bnds_Pade(input, bndry_num, rank_in)
    end do

    ! ... fill mapping array; must be consistent with stencil definition in hypre.c
    if (associated(input%HYPREStencilIndex) .eqv. .false.) then
      allocate(input%HYPREStencilIndex(MAX_OPERATORS,-2:2,-2:2,-2:2))
      input%HYPREStencilIndex = 0
    end if

    if (lhs_beta /= 0.0_rfreal) then
      call graceful_exit(myrank, 'ERROR: lhs_beta /= 0 in initialize_filter_stencils_PADE.')
    else
      input%HYPREStencilIndex(input%iFilter, :, :, :) = 0
      input%HYPREStencilIndex(input%iFilter, 0, 0, 0) = 0   ! ... self point
      input%HYPREStencilIndex(input%iFilter,-1, 0, 0) = 1   ! ... i = -1
      input%HYPREStencilIndex(input%iFilter,+1, 0, 0) = 2   ! ... i = +1
      input%HYPREStencilIndex(input%iFilter, 0,-1, 0) = 3   ! ... j = -1
      input%HYPREStencilIndex(input%iFilter, 0,+1, 0) = 4   ! ... j = +1
      input%HYPREStencilIndex(input%iFilter, 0, 0,-1) = 5   ! ... k = -1
      input%HYPREStencilIndex(input%iFilter, 0, 0,+1) = 6   ! ... k = +1
    end if

  end subroutine initialize_filter_stencils_PADE_new

  subroutine initialize_filter_bnds_Pade(input,bndry_num,rank_in)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    type(t_mixt_input), pointer :: input
    integer :: rank_in, bndry_num

    ! ... local variables
    integer :: i
    real(rfreal) :: lhs_alpha, lhs_beta
    real(rfreal) :: rhs_a, rhs_b, rhs_c, rhs_d, rhs_e, rhs_f
    real(rfreal) :: rhs_g, rhs_h, rhs_i, rhs_j, rhs_k
    real(rfreal) :: rhs_l
    real(rfreal) :: rhs_block(20,12)

    ! ... initialize
    rhs_block(:,:) = 0.0_rfreal

    if (input%bndry_filter(bndry_num) .eqv. .false.) then    

       if (rank_in == 0) write (*,'(A,I1,A)') 'PlasComCM: ==> Using Pade filter (centered) on side',bndry_num,'. <=='

       ! ... boundary filter alphaf
       lhs_alpha = input%bndry_filter_alphaf

       ! ... (i = 1) on boundary filter :: no filtering
       input%lhs_block1(input%iFilter,1,0) = 1.0_rfreal
       rhs_block(1,1) = 1.0_rfreal

       ! ... (i = 2) boundary filter
       rhs_a = (1.0_rfreal + 2.0_rfreal * lhs_alpha) / 2.0_rfreal
       rhs_b = (1.0_rfreal + 2.0_rfreal * lhs_alpha) / 2.0_rfreal
       input%lhs_block1(input%iFilter,2,-1:1) = (/ lhs_alpha, 1.0_rfreal, lhs_alpha /)
       rhs_block(2,1:9) = (/ rhs_b, 2.0_rfreal * rhs_a, rhs_b, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp /) / 2.0_rfreal

       ! ... (i = 3) boundary filter
       rhs_a = 0.625_rfreal + 0.75_rfreal * lhs_alpha
       rhs_b = 0.5_rfreal + lhs_alpha
       rhs_c = -0.125_rfreal + 0.25_rfreal * lhs_alpha
       input%lhs_block1(input%iFilter,3,-1:1) = (/ lhs_alpha, 1.0_rfreal, lhs_alpha /)
       rhs_block(3,1:9) = (/ rhs_c, rhs_b, 2.0_rfreal * rhs_a, rhs_b, rhs_c, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp /) / 2.0_rfreal

       ! ... (i = 4) boundary filter
       rhs_a = 11.0_rfreal / 16.0_rfreal + 0.625_rfreal * lhs_alpha
       rhs_b = 15.0_rfreal / 32.0_rfreal + 17.0_rfreal / 16.0_rfreal * lhs_alpha
       rhs_c = -3.0_rfreal / 16.0_rfreal + 0.375_rfreal * lhs_alpha
       rhs_d = 1.0_rfreal / 32.0_rfreal - lhs_alpha / 16.0_rfreal
       input%lhs_block1(input%iFilter,4,-1:1) = (/ lhs_alpha, 1.0_rfreal, lhs_alpha /)
       rhs_block(4,1:9) = (/ rhs_d, rhs_c, rhs_b, 2.0_rfreal * rhs_a, rhs_b, rhs_c, rhs_d, 0.0_dp, 0.0_dp /) / 2.0_rfreal

       ! ... (i = 5) boundary filter
       rhs_a = (93.0_rfreal + 70.0_rfreal * lhs_alpha ) / 128.0_rfreal
       rhs_b = (7.0_rfreal + 18.0_rfreal * lhs_alpha) / 16.0_rfreal
       rhs_c = (-7.0_rfreal + 14.0_rfreal * lhs_alpha) / 32.0_rfreal
       rhs_d = 1.0_rfreal / 16.0_rfreal - lhs_alpha / 8.0_rfreal
       rhs_e = -1.0_rfreal / 128.0_rfreal + lhs_alpha / 64.0_rfreal
       input%lhs_block1(input%iFilter,5,-1:1) = (/ lhs_alpha, 1.0_rfreal, lhs_alpha /)
       rhs_block(5,1:9) = (/ rhs_e, rhs_d, rhs_c, rhs_b, 2.0_rfreal * rhs_a, rhs_b, rhs_c, rhs_d, rhs_e /) / 2.0_rfreal

    else

       if (rank_in == 0) write (*,'(A,I1,A)') 'PlasComCM: ==> Using Pade filter (biased) on side',bndry_num,'. <=='
       ! ... filter free parameter
       lhs_alpha = input%bndry_filter_alphaf

       ! ... (i = 1) on boundary filter :: no filtering
       input%lhs_block1(input%iFilter,1,0) = 1.0_rfreal
       rhs_block(1,1) = 1.0_rfreal

       ! ... (i = 2) boundary filter :: from Visbal report
       rhs_a = (1.0_rfreal + 1022.0_rfreal * lhs_alpha) / 1024.0_rfreal
       rhs_b = (507.0_rfreal + 10.0_rfreal * lhs_alpha) / 512.0_rfreal
       rhs_c = (45.0_rfreal + 934.0_rfreal * lhs_alpha) / 1024.0_rfreal
       rhs_d = 15.0_rfreal * (-1.0_rfreal + 2.0_rfreal * lhs_alpha) / 128.0_rfreal
       rhs_e = 105.0_rfreal * (1.0_rfreal - 2.0_rfreal * lhs_alpha) / 512.0_rfreal
       rhs_f = 63.0_rfreal * (-1.0_rfreal + 2.0_rfreal * lhs_alpha) / 256.0_rfreal
       rhs_g = 105.0_rfreal * (1.0_rfreal - 2.0_rfreal * lhs_alpha) / 512.0_rfreal
       rhs_h = 15.0_rfreal * (-1.0_rfreal + 2.0_rfreal * lhs_alpha) / 128.0_rfreal
       rhs_i = 45.0_rfreal * (1.0_rfreal - 2.0_rfreal * lhs_alpha) / 1024.0_rfreal
       rhs_j = 5.0_rfreal * (-1.0_rfreal + 2.0_rfreal * lhs_alpha) / 512.0_rfreal
       rhs_k = (1.0_rfreal - 2.0_rfreal * lhs_alpha) / 1024.0_rfreal
       input%lhs_block1(input%iFilter,2,-1:1) = (/ lhs_alpha, 1.0_rfreal, lhs_alpha /)
       rhs_block(2,1:11) = (/ rhs_a, rhs_b, rhs_c, rhs_d, rhs_e, rhs_f, rhs_g, rhs_h, rhs_i, rhs_j, rhs_k /)

       ! ... (i = 3) boundary filter :: from Visbal report
       rhs_a = (-1.0_rfreal + 2.0_rfreal * lhs_alpha) / 1024.0_rfreal
       rhs_b = (5.0_rfreal + 502.0_rfreal * lhs_alpha) / 512.0_rfreal
       rhs_c = (979.0_rfreal + 90.0_rfreal * lhs_alpha) / 1024.0_rfreal
       rhs_d = (15.0_rfreal + 98.0_rfreal * lhs_alpha) / 128.0_rfreal
       rhs_e = 105.0_rfreal * (-1.0_rfreal + 2.0_rfreal * lhs_alpha) / 512.0_rfreal
       rhs_f = 63.0_rfreal * (1.0_rfreal - 2.0_rfreal * lhs_alpha) / 256.0_rfreal
       rhs_g = 105.0_rfreal * (-1.0_rfreal + 2.0_rfreal * lhs_alpha) / 512.0_rfreal
       rhs_h = 15.0_rfreal * (1.0_rfreal - 2.0_rfreal * lhs_alpha) / 128.0_rfreal
       rhs_i = 45.0_rfreal * (-1.0_rfreal + 2.0_rfreal * lhs_alpha) / 1024.0_rfreal
       rhs_j = 5.0_rfreal * (1.0_rfreal - 2.0_rfreal * lhs_alpha) / 512.0_rfreal
       rhs_k = (-1.0_rfreal + 2.0_rfreal * lhs_alpha) / 1024.0_rfreal
       input%lhs_block1(input%iFilter,3,-1:1) = (/ lhs_alpha, 1.0_rfreal, lhs_alpha /)
       rhs_block(3,1:11) = (/ rhs_a, rhs_b, rhs_c, rhs_d, rhs_e, rhs_f, rhs_g, rhs_h, rhs_i, rhs_j, rhs_k /)

       ! ... (i = 4) boundary filter :: from Visbal report
       rhs_a = (1.0_rfreal - 2.0_rfreal * lhs_alpha) / 1024.0_rfreal
       rhs_b = 5.0_rfreal * (-1.0_rfreal + 2.0_rfreal * lhs_alpha) / 512.0_rfreal
       rhs_c = (45.0_rfreal + 934.0_rfreal * lhs_alpha) / 1024.0_rfreal
       rhs_d = (113.0_rfreal + 30.0_rfreal * lhs_alpha) / 128.0_rfreal
       rhs_e = (105.0_rfreal + 302.0_rfreal * lhs_alpha) / 512.0_rfreal
       rhs_f = 63.0_rfreal * (-1.0_rfreal + 2.0_rfreal * lhs_alpha) / 256.0_rfreal
       rhs_g = 105.0_rfreal * (1.0_rfreal - 2.0_rfreal * lhs_alpha) / 512.0_rfreal
       rhs_h = 15.0_rfreal * (-1.0_rfreal + 2.0_rfreal * lhs_alpha) / 128.0_rfreal
       rhs_i = 45.0_rfreal * (1.0_rfreal - 2.0_rfreal * lhs_alpha) / 1024.0_rfreal
       rhs_j = 5.0_rfreal * (-1.0_rfreal + 2.0_rfreal * lhs_alpha) / 512.0_rfreal
       rhs_k = (1.0_rfreal - 2.0_rfreal * lhs_alpha) / 1024.0_rfreal
       input%lhs_block1(input%iFilter,4,-1:1) = (/ lhs_alpha, 1.0_rfreal, lhs_alpha /)
       rhs_block(4,1:11) = (/ rhs_a, rhs_b, rhs_c, rhs_d, rhs_e, rhs_f, rhs_g, rhs_h, rhs_i, rhs_j, rhs_k /)

       ! ... (i = 5) boundary filter :: from Visbal report
       rhs_a = (-1.0_rfreal + 2.0_rfreal * lhs_alpha) / 1024.0_rfreal
       rhs_b = 5.0_rfreal * (1.0_rfreal - 2.0_rfreal * lhs_alpha) / 512.0_rfreal
       rhs_c = 45.0_rfreal * (-1.0_rfreal + 2.0_rfreal * lhs_alpha) / 1024.0_rfreal
       rhs_d = (15.0_rfreal + 98.0_rfreal * lhs_alpha) / 128.0_rfreal
       rhs_e = (407.0_rfreal + 210.0_rfreal * lhs_alpha) / 512.0_rfreal
       rhs_f = (63.0_rfreal + 130.0_rfreal * lhs_alpha) / 256.0_rfreal
       rhs_g = 105.0_rfreal * (-1.0_rfreal + 2.0_rfreal * lhs_alpha) / 512.0_rfreal
       rhs_h = 15.0_rfreal * (1.0_rfreal - 2.0_rfreal * lhs_alpha) / 128.0_rfreal
       rhs_i = 45.0_rfreal * (-1.0_rfreal + 2.0_rfreal * lhs_alpha) / 1024.0_rfreal
       rhs_j = 5.0_rfreal * (1.0_rfreal - 2.0_rfreal * lhs_alpha) / 512.0_rfreal
       rhs_k = (-1.0_rfreal + 2.0_rfreal * lhs_alpha) / 1024.0_rfreal
       input%lhs_block1(input%iFilter,5,-1:1) = (/ lhs_alpha, 1.0_rfreal, lhs_alpha /)
       rhs_block(5,1:11) = (/ rhs_a, rhs_b, rhs_c, rhs_d, rhs_e, rhs_f, rhs_g, rhs_h, rhs_i, rhs_j, rhs_k /)



    ! do i = 1, input%operator_bndry_depth(input%iFilter)
    !   input%lhs_block2(input%iFilter,i,-2:2) = input%lhs_block1(input%iFilter,input%operator_bndry_depth(input%iFilter)-(i-1),2:-2:-1)
    ! end do
    ! Call Mirror_block1_to_block2(input, input%iFilter)

    end if

    ! ... fill the corresponding block
    if(bndry_num == 1) then
       ! ... left boundary, block 1
       do i = 1, input%operator_bndry_depth(input%iFilter)
          input%rhs_block1(input%iFilter,i,1:11) = rhs_block(i,1:11)
       end do
    elseif(bndry_num == 2) then
       ! ... right boundary, block 2
       do i = 1, input%operator_bndry_depth(input%iFilter)
          input%rhs_block2(input%iFilter,i,1:11) = rhs_block(input%operator_bndry_depth(input%iFilter)- i + 1,11:1:-1)
       end do
    do i = 1, input%operator_bndry_depth(input%iFilter)
      input%lhs_block2(input%iFilter,i,-2:2) = input%lhs_block1(input%iFilter,input%operator_bndry_depth(input%iFilter)-(i-1),2:-2:-1)
    end do

    end if

  end subroutine initialize_filter_bnds_Pade

  subroutine initialize_deriv_stencils_PADE(myrank, input)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    integer :: myrank
    type(t_mixt_input), pointer :: input

    ! ... local variables
    real(rfreal) :: lhs_alpha, lhs_beta
    real(rfreal) :: rhs_a, rhs_b, rhs_c, rhs_d, rhs_e, rhs_f, rhs_g, rhs_h, rhs_i, rhs_j, rhs_k
    real(rfreal) :: rhs_l
    integer :: i

    !----------------------------------------------------------------------
    ! derivative stencils
    !----------------------------------------------------------------------
    ! ... initialize
    input%overlap(input%iFirstDeriv) = 3
    input%operator_type(input%iFirstDeriv) = IMPLICIT
    input%max_operator_width(input%iFirstDeriv) = 7
    input%operator_bndry_width(input%iFirstDeriv) = 4
    input%operator_bndry_depth(input%iFirstDeriv) = 3
    input%lhs_interior_stencil_start(input%iFirstDeriv) = -1
    input%lhs_interior_stencil_end(input%iFirstDeriv)   =  1
    input%rhs_interior_stencil_start(input%iFirstDeriv) = -3
    input%rhs_interior_stencil_end(input%iFirstDeriv)   =  3

    ! ... interior first derivative
    lhs_beta  = 0.0_rfreal
    lhs_alpha = 1.0_rfreal / 3.0_rfreal
    rhs_c     = 0.1_rfreal * (1.0_rfreal - 3.0_rfreal * lhs_alpha + 12.0_rfreal * lhs_beta)
    rhs_b     = 1.0_rfreal / 15.0_rfreal * (-9.0_rfreal + 32.0_rfreal * lhs_alpha + 62.0_rfreal * lhs_beta)
    rhs_a     = 1.0_rfreal / 6.0_rfreal * (9.0_rfreal + lhs_alpha - 20.0_rfreal * lhs_beta)
    input%lhs_interior(input%iFirstDeriv,-2:2)  = (/ lhs_beta, lhs_alpha, 1.0_rfreal, lhs_alpha, lhs_beta /)
    input%rhs_interior(input%iFirstDeriv,-3:3)  = (/ -rhs_c/6.0_rfreal, -rhs_b/4.0_rfreal, -rhs_a/2.0_rfreal, 0.0_rfreal, &
         rhs_a/2.0_rfreal,  rhs_b/4.0_rfreal,  rhs_c/6.0_rfreal /)

    ! ... (i = 1) on boundary first derivative
    lhs_alpha = 2.0_rfreal
    rhs_d     = 0.0_rfreal
    rhs_a = -(3.0_rfreal + lhs_alpha + 2.0_rfreal * rhs_d) / 2.0_rfreal
    rhs_b = 2.0_rfreal + 3.0_rfreal * rhs_d
    rhs_c = -(1.0_rfreal - lhs_alpha + 6.0_rfreal * rhs_d) / 2.0_rfreal
    input%lhs_block1(input%iFirstDeriv,1,0:1) = (/ 1.0_rfreal, lhs_alpha /)
    input%rhs_block1(input%iFirstDeriv,1,1:4) = (/ rhs_a, rhs_b, rhs_c, rhs_d /)

    ! ... (i = 2) boundary first derivative
    lhs_alpha = 0.25_rfreal;
    rhs_a     = 0.75_rfreal;
    input%lhs_block1(input%iFirstDeriv,2,-1:1) = (/ lhs_alpha, 1.0_rfreal, lhs_alpha /)
    input%rhs_block1(input%iFirstDeriv,2, 1:4) = (/ -rhs_a,    0.0_rfreal, rhs_a, 0.0_rfreal /)

    do i = 1, input%operator_bndry_depth(input%iFirstDeriv)
      input%lhs_block2(input%iFirstDeriv,i,-1:1) = input%lhs_block1(input%iFirstDeriv,input%operator_bndry_depth(input%iFirstDeriv)-(i-1),1:-1:-1)
    end do
    Call Mirror_block1_to_block2(input, input%iFirstDeriv)


    ! ... interior second derivative
    input%overlap(input%iSecondDeriv) = 3
    input%operator_type(input%iSecondDeriv) = IMPLICIT
    input%max_operator_width(input%iSecondDeriv) = 7
    input%operator_bndry_width(input%iSecondDeriv) = 5
    input%operator_bndry_depth(input%iSecondDeriv) = 3
    input%rhs_interior_stencil_start(input%iSecondDeriv) = -3
    input%rhs_interior_stencil_end(input%iSecondDeriv)   =  3
    input%lhs_interior_stencil_start(input%iSecondDeriv) = -1
    input%lhs_interior_stencil_end(input%iSecondDeriv)   =  1

    lhs_beta  = 0.0_rfreal
    lhs_alpha = 2.0_rfreal / 11.0_rfreal
    rhs_a     = 0.25_rfreal * (6.0_rfreal - 9.0_rfreal * lhs_alpha - 12.0_rfreal * lhs_beta)
    rhs_b     = 0.20_rfreal * (-3.0_rfreal + 24.0_rfreal * lhs_alpha - 6.0_rfreal * lhs_beta)
    rhs_c     = 0.05_rfreal * (2.0_rfreal - 11.0_rfreal * lhs_alpha + 124.0_rfreal * lhs_beta)
    input%lhs_interior(input%iSecondDeriv,-2:2) = (/lhs_beta, lhs_alpha, 1.0_rfreal, lhs_alpha, lhs_beta /)
    input%rhs_interior(input%iSecondDeriv,-3:3) = (/ rhs_c/9.0_rfreal, rhs_b/4.0_rfreal, rhs_a, &
         -2.0_rfreal * ( rhs_c/9.0_rfreal + rhs_b/4.0_rfreal + rhs_a ), &
         rhs_a, rhs_b/4.0_rfreal, rhs_c/9.0_rfreal /)

    ! ... (i = 1) on boundary second derivative
    lhs_alpha = 11.0_rfreal
    rhs_e     = 0.0_rfreal
    rhs_a     = lhs_alpha + 2.0_rfreal + rhs_e
    rhs_b     = - (2.0_rfreal * lhs_alpha + 5.0_rfreal + 4.0_rfreal * rhs_e)
    rhs_c     = lhs_alpha + 4.0_rfreal + 6.0_rfreal * rhs_e
    rhs_d     = - (1.0_rfreal + 4.0_rfreal * rhs_e)
    input%lhs_block1(input%iSecondDeriv,1,0:1) = (/ 1.0_rfreal, lhs_alpha /)
    input%rhs_block1(input%iSecondDeriv,1,1:5) = (/ rhs_a, rhs_b, rhs_c, rhs_d, rhs_e /)

    ! ... (i = 2) boundary second derivative
    lhs_beta  = 0.0_rfreal
    lhs_alpha = 1.0_rfreal / 10.0_rfreal
    rhs_a     =  12.0_rfreal / 10.0_rfreal
    rhs_b     = -24.0_rfreal / 10.0_rfreal
    rhs_c     =  12.0_rfreal / 10.0_rfreal
    input%lhs_block1(input%iSecondDeriv,2,-1:1) = (/ lhs_alpha, 1.0_rfreal, lhs_alpha /)
    input%rhs_block1(input%iSecondDeriv,2,1:5) = (/ rhs_a, rhs_b, rhs_c, 0.0_dp, 0.0_dp /)

    do i = 1, input%operator_bndry_depth(input%iSecondDeriv)
      input%lhs_block2(input%iSecondDeriv,i,-2:2) = input%lhs_block1(input%iSecondDeriv,input%operator_bndry_depth(input%iSecondDeriv)-(i-1),2:-2:-1)
    end do
    Call Mirror_block1_to_block2(input, input%iSecondDeriv)


    ! ... interior fourth derivative (Kawai & Lele (JCP 2008)), JKim 10/2008
    if (input%shock /= 0) then
      input%iFourthDeriv = 4
      input%overlap(input%iFourthDeriv) = 3
      input%operator_type(input%iFourthDeriv) = IMPLICIT
      input%max_operator_width(input%iFourthDeriv) = 7
      input%operator_bndry_width(input%iFourthDeriv) = 6
      input%operator_bndry_depth(input%iFourthDeriv) = 3
      input%lhs_interior_stencil_start(input%iFourthDeriv) = -1
      input%lhs_interior_stencil_end(input%iFourthDeriv)   =  1
      input%rhs_interior_stencil_start(input%iFourthDeriv) = -3
      input%rhs_interior_stencil_end(input%iFourthDeriv)   =  3

      if (input%iFourthDeriv /= input%ArtPropDrvOrder) then
        call graceful_exit(myrank, 'ERROR: input%iFourthDeriv /= input%ArtPropDrvOrder')
      end if

      lhs_beta  = 0.0_rfreal
      lhs_alpha = 7.0_rfreal / 26.0_rfreal
      rhs_a     = 2.0_rfreal * (1.0_rfreal - lhs_alpha)
      rhs_b     = 4.0_rfreal * lhs_alpha - 1.0_rfreal
      rhs_c     = 0.0_rfreal
      input%lhs_interior(input%iFourthDeriv,-2:2) = (/lhs_beta, lhs_alpha, 1.0_rfreal, lhs_alpha, lhs_beta /)
      input%rhs_interior(input%iFourthDeriv,-3:3) = &
                      (/ rhs_b/6.0_rfreal, &
                         rhs_a, &
                        (-9.0_rfreal * rhs_b / 6.0_rfreal - 4.0_rfreal * rhs_a), &
                        (16.0_rfreal * rhs_b / 6.0_rfreal + 6.0_rfreal * rhs_a), &
                        (-9.0_rfreal * rhs_b / 6.0_rfreal - 4.0_rfreal * rhs_a), &
                         rhs_a, &
                         rhs_b/6.0_rfreal /)

      ! ... (i = 1) on boundary fourth derivative
      lhs_alpha = 0.0_rfreal
      rhs_a     = 3.0_rfreal
      rhs_b     = -14.0_rfreal
      rhs_c     = 26.0_rfreal
      rhs_d     = -24.0_rfreal
      rhs_e     = 11.0_rfreal
      rhs_f     = -2.0_rfreal
      input%lhs_block1(input%iFourthDeriv,1,-1:1) = (/ lhs_alpha, 1.0_rfreal, lhs_alpha /)
      input%rhs_block1(input%iFourthDeriv,1,1:6) = (/ rhs_a, rhs_b, rhs_c, rhs_d, rhs_e, rhs_f /)

      ! ... (i = 2) on boundary fourth derivative
      lhs_alpha = 0.0_rfreal
      rhs_a     = 2.0_rfreal
      rhs_b     = -9.0_rfreal
      rhs_c     = 16.0_rfreal
      rhs_d     = -14.0_rfreal
      rhs_e     = 6.0_rfreal
      rhs_f     = -1.0_rfreal
      input%lhs_block1(input%iFourthDeriv,2,-1:1) = (/ lhs_alpha, 1.0_rfreal, lhs_alpha /)
      input%rhs_block1(input%iFourthDeriv,2,1:6) = (/ rhs_a, rhs_b, rhs_c, rhs_d, rhs_e, rhs_f /)

      ! ... (i = 3) on boundary fourth derivative
      lhs_alpha = 0.0_rfreal
      rhs_a     = 1.0_rfreal
      rhs_b     =-4.0_rfreal
      rhs_c     = 6.0_rfreal
      input%lhs_block1(input%iFourthDeriv,3,-1:1) = (/ lhs_alpha, 1.0_rfreal, lhs_alpha /)
      input%rhs_block1(input%iFourthDeriv,3,1:6) = (/ rhs_a, rhs_b, rhs_c, rhs_b, rhs_a, 0.0_dp /)

      do i = 1, input%operator_bndry_depth(input%iFourthDeriv)
        input%lhs_block2(input%iFourthDeriv,i,-2:2) = input%lhs_block1(input%iFourthDeriv,input%operator_bndry_depth(input%iFourthDeriv)-(i-1),2:-2:-1)
      end do
      Call Mirror_block1_to_block2(input, input%iFourthDeriv)
    end if

    ! ... fill mapping array; must be consistent with stencil definition in hypre.c
    if (associated(input%HYPREStencilIndex) .eqv. .false.) then
      allocate(input%HYPREStencilIndex(MAX_OPERATORS,-2:2,-2:2,-2:2))
      input%HYPREStencilIndex = 0
    end if

    if (lhs_beta /= 0.0_rfreal) then
      call graceful_exit(myrank, 'ERROR: lhs_beta /= 0 in initialize_deriv_stencils_PADE.')
    else

      ! ... first derivative
      input%HYPREStencilIndex(input%iFirstDeriv, :, :, :) = 0
      input%HYPREStencilIndex(input%iFirstDeriv, 0, 0, 0) = 0   ! ... self point
      input%HYPREStencilIndex(input%iFirstDeriv,-1, 0, 0) = 1   ! ... i = -1
      input%HYPREStencilIndex(input%iFirstDeriv,+1, 0, 0) = 2   ! ... i = +1
      input%HYPREStencilIndex(input%iFirstDeriv, 0,-1, 0) = 3   ! ... j = -1
      input%HYPREStencilIndex(input%iFirstDeriv, 0,+1, 0) = 4   ! ... j = +1
      input%HYPREStencilIndex(input%iFirstDeriv, 0, 0,-1) = 5   ! ... k = -1
      input%HYPREStencilIndex(input%iFirstDeriv, 0, 0,+1) = 6   ! ... k = +1

      ! ... second derivative
      input%HYPREStencilIndex(input%iSecondDeriv, :, :, :) = 0
      input%HYPREStencilIndex(input%iSecondDeriv, 0, 0, 0) = 0   ! ... self point
      input%HYPREStencilIndex(input%iSecondDeriv,-1, 0, 0) = 1   ! ... i = -1
      input%HYPREStencilIndex(input%iSecondDeriv,+1, 0, 0) = 2   ! ... i = +1
      input%HYPREStencilIndex(input%iSecondDeriv, 0,-1, 0) = 3   ! ... j = -1
      input%HYPREStencilIndex(input%iSecondDeriv, 0,+1, 0) = 4   ! ... j = +1
      input%HYPREStencilIndex(input%iSecondDeriv, 0, 0,-1) = 5   ! ... k = -1
      input%HYPREStencilIndex(input%iSecondDeriv, 0, 0,+1) = 6   ! ... k = +1

      ! ... fourth derivative
      if (input%shock /= 0) then
        input%HYPREStencilIndex(input%iFourthDeriv, :, :, :) = 0
        input%HYPREStencilIndex(input%iFourthDeriv, 0, 0, 0) = 0   ! ... self point
        input%HYPREStencilIndex(input%iFourthDeriv,-1, 0, 0) = 1   ! ... i = -1
        input%HYPREStencilIndex(input%iFourthDeriv,+1, 0, 0) = 2   ! ... i = +1
        input%HYPREStencilIndex(input%iFourthDeriv, 0,-1, 0) = 3   ! ... j = -1
        input%HYPREStencilIndex(input%iFourthDeriv, 0,+1, 0) = 4   ! ... j = +1
        input%HYPREStencilIndex(input%iFourthDeriv, 0, 0,-1) = 5   ! ... k = -1
        input%HYPREStencilIndex(input%iFourthDeriv, 0, 0,+1) = 6   ! ... k = +1
      end if
    end if

    return

  end subroutine initialize_deriv_stencils_PADE

  subroutine initialize_filter_stencils_2nd(input)

    USE ModGlobal
    USE ModDataStruct

    type(t_mixt_input), pointer :: input

    ! ... local variables
    real(rfreal) :: lhs_alpha, lhs_beta
    real(rfreal) :: rhs_a, rhs_b, rhs_c, rhs_d, rhs_e, rhs_f, rhs_g, rhs_h, rhs_i, rhs_j, rhs_k
    real(rfreal) :: rhs_l
    integer :: i

    !----------------------------------------------------------------------
    ! filter stencils
    !----------------------------------------------------------------------
    ! ... initialize
    input%overlap(input%iFilter) = 1
    input%operator_type(input%iFilter) = EXPLICIT
    input%max_operator_width(input%iFilter) = 3
    input%operator_bndry_width(input%iFilter) = 2
    input%operator_bndry_depth(input%iFilter) = 1
    input%rhs_interior_stencil_start(input%iFilter) = -1
    input%rhs_interior_stencil_end(input%iFilter)   =  1

    ! ... interior filter
    rhs_a = 100.0_rfreal
    rhs_b =   1.0_rfreal
    input%rhs_interior(input%iFilter,-1:1) = (/ rhs_b, rhs_a, rhs_b /) / (2.0_rfreal * rhs_b + rhs_a)

    ! ... (i = 1) on boundary filter :: no filtering
    input%rhs_block1(input%iFilter,1,1) = 1.0_rfreal

    Call Mirror_block1_to_block2(input, input%iFilter)

  end subroutine initialize_filter_stencils_2nd

  subroutine initialize_filter_stencils_drp_SFs11p(input)

    USE ModGlobal
    USE ModDataStruct

    type(t_mixt_input), pointer :: input

    ! ... local variables
    real(rfreal) :: sigma_d
    integer :: i

    !----------------------------------------------------------------------
    ! filter stencils
    !----------------------------------------------------------------------
    ! ... initialize
    input%overlap(input%iFilter) = 5
    input%operator_type(input%iFilter) = EXPLICIT
    input%max_operator_width(input%iFilter) = 11
    input%operator_bndry_width(input%iFilter) = 11
    input%operator_bndry_depth(input%iFilter) = 5
    input%rhs_interior_stencil_start(input%iFilter) = -5
    input%rhs_interior_stencil_end(input%iFilter)   =  5

    ! ... interior filter
    input%rhs_interior(input%iFilter,-5:5) = (/ -1.0_dp/1024.0_dp, 5.0_dp/512.0_dp, -45.0_dp/1024.0_dp, 15.0_dp/128.0_dp, -105.0_dp/512.0_dp, 63.0_dp/256.0_dp, &
                                               -105.0_dp/512.0_dp, 15.0_dp/128.0_dp, -45.0_dp/1024.0_dp, 5.0_dp/512.0_dp, -1.0_dp/1024.0_dp /)

    ! ... scale by sigma_d (per Bogey et al.)
    sigma_d = input%filter_fac
    input%rhs_interior(input%iFilter,-5:5) = - sigma_d * input%rhs_interior(input%iFilter,-5:5)
    input%rhs_interior(input%iFilter, 0:0) = 1.0_dp + input%rhs_interior(input%iFilter, 0:0)

    ! ... zero out the boundary filter
    do i = 1, input%operator_bndry_depth(input%iFilter)
      input%rhs_block1(input%iFilter,i,:) = 0.0_dp
    end do

    ! ... (i = 1) on boundary filter :: no filtering
    input%rhs_block1(input%iFilter,1,1) = 1.0_dp

    ! ... (i = 2) 7-pt selective filter
    input%rhs_block1(input%iFilter,2,1:7) = -sigma_d * (/ -0.085777408970_dp, &
     0.277628171524_dp, -0.356848072173_dp, 0.223119093072_dp, -0.057347064865_dp, -0.000747264596_dp, -0.000027453993_dp /)

    ! ... (i = 3) 7-pt selective filter
    input%rhs_block1(input%iFilter,3,1:7) = -sigma_d * (/ 0.032649010764_dp, &
     -0.143339502575_dp, 0.273321177980_dp, -0.294622121167_dp, 0.186711738069_dp, -0.062038376258_dp, 0.007318073189_dp /)

    ! ... (i = 4) 11-pt selective filter
    input%rhs_block1(input%iFilter,4,1:11) = -sigma_d * (/ -0.000054596010_dp, &
     0.042124772446_dp, -0.173103107841_dp, 0.299615871352_dp, -0.276543612935_dp, &
     0.131223506571_dp, -0.023424966418_dp, 0.013937561779_dp, -0.024565095706_dp, 0.013098287852_dp, -0.002308621090_dp /)

    ! ... (i = 5) 11-pt selective filter
    input%rhs_block1(input%iFilter,5,1:11) = -sigma_d * (/ 0.008391235145_dp, &
     -0.047402506444_dp, 0.121438547725_dp, -0.200063042812_dp, 0.240069047836_dp, &
     -0.207269200140_dp, 0.122263107844_dp, -0.047121062819_dp, 0.009014891495_dp, 0.001855812216_dp, -0.001176830044_dp /)

    do i = 2, 5
      input%rhs_block1(input%iFilter,i,i) = 1.0_dp + input%rhs_block1(input%iFilter,i,i)
    end do

    Call Mirror_block1_to_block2(input, input%iFilter)

  end subroutine initialize_filter_stencils_drp_SFs11p

  subroutine initialize_filter_stencils_drp_SFs13p(input)

    USE ModGlobal
    USE ModDataStruct

    type(t_mixt_input), pointer :: input

    ! ... local variables
    real(rfreal) :: sigma_d
    integer :: i

    !----------------------------------------------------------------------
    ! filter stencils
    !----------------------------------------------------------------------
    ! ... initialize
    input%overlap(input%iFilter) = 6
    input%operator_type(input%iFilter) = EXPLICIT
    input%max_operator_width(input%iFilter) = 13
    input%operator_bndry_width(input%iFilter) = 11
    input%operator_bndry_depth(input%iFilter) = 6
    input%rhs_interior_stencil_start(input%iFilter) = -6
    input%rhs_interior_stencil_end(input%iFilter)   =  6

    ! ... interior filter
    input%rhs_interior(input%iFilter,-6:6) = (/ 1.0_dp/4096.0_dp, -3.0_dp/1024.0_dp, 33.0_dp/2048.0_dp, -55.0_dp/1024.0_dp, 495.0_dp/4096.0_dp, -99.0_dp/512.0_dp, 231.0_dp/1024.0_dp, &
                                                -99.0_dp/512.0_dp, 495.0_dp/4096.0_dp, -55.0_dp/1024.0_dp, 33.0_dp/2048.0_dp, -3.0_dp/1024.0_dp, 1.0_dp/4096.0_dp /)

    ! ... scale by sigma_d (per Bogey et al.)
    sigma_d = input%filter_fac
    input%rhs_interior(input%iFilter,-6:6) = - sigma_d * input%rhs_interior(input%iFilter,-6:6)
    input%rhs_interior(input%iFilter, 0:0) = 1.0_dp + input%rhs_interior(input%iFilter, 0:0)

    ! ... zero out the boundary filter
    do i = 1, input%operator_bndry_depth(input%iFilter)
      input%rhs_block1(input%iFilter,i,:) = 0.0_dp
    end do

    ! ... (i = 1) on boundary filter :: no filtering
    input%rhs_block1(input%iFilter,1,1) = 1.0_dp

    ! ... (i = 2) 7-pt selective filter
    input%rhs_block1(input%iFilter,2,1:7) = -sigma_d * (/ -0.085777408970_dp, &
     0.277628171524_dp, -0.356848072173_dp, 0.223119093072_dp, -0.057347064865_dp, -0.000747264596_dp, -0.000027453993_dp /)

    ! ... (i = 3) 7-pt selective filter
    input%rhs_block1(input%iFilter,3,1:7) = -sigma_d * (/ 0.032649010764_dp, &
     -0.143339502575_dp, 0.273321177980_dp, -0.294622121167_dp, 0.186711738069_dp, -0.062038376258_dp, 0.007318073189_dp /)

    ! ... (i = 4) 11-pt selective filter
    input%rhs_block1(input%iFilter,4,1:11) = -sigma_d * (/ -0.000054596010_dp, &
     0.042124772446_dp, -0.173103107841_dp, 0.299615871352_dp, -0.276543612935_dp, 0.131223506571_dp, -0.023424966418_dp, 0.013937561779_dp, -0.024565095706_dp, 0.013098287852_dp, -0.002308621090_dp /)

    ! ... (i = 5) 11-pt selective filter
    input%rhs_block1(input%iFilter,5,1:11) = -sigma_d * (/ 0.008391235145_dp, &
     -0.047402506444_dp, 0.121438547725_dp, -0.200063042812_dp, 0.240069047836_dp, -0.207269200140_dp, 0.122263107844_dp, -0.047121062819_dp, 0.009014891495_dp, 0.001855812216_dp, -0.001176830044_dp /)

    ! ... (i = 6) 11-pt standard filter
    input%rhs_block1(input%iFilter,6,1:11) = -sigma_d * (/ -1.0_dp/1024.0_dp, 5.0_dp/512.0_dp, -45.0_dp/1024.0_dp, 15.0_dp/128.0_dp, -105.0_dp/512.0_dp, 63.0_dp/256.0_dp, &
                                               -105.0_dp/512.0_dp, 15.0_dp/128.0_dp, -45.0_dp/1024.0_dp, 5.0_dp/512.0_dp, -1.0_dp/1024.0_dp /)

    do i = 2, 6
      input%rhs_block1(input%iFilter,i,i) = 1.0_dp + input%rhs_block1(input%iFilter,i,i)
    end do

    Call Mirror_block1_to_block2(input, input%iFilter)

  end subroutine initialize_filter_stencils_drp_SFs13p

  subroutine initialize_filter_stencils_drp_SFo13p(input)

    USE ModGlobal
    USE ModDataStruct

    type(t_mixt_input), pointer :: input

    ! ... local variables
    real(rfreal) :: sigma_d
    integer :: i

    !----------------------------------------------------------------------
    ! filter stencils
    !----------------------------------------------------------------------
    ! ... initialize
    input%overlap(input%iFilter) = 6
    input%operator_type(input%iFilter) = EXPLICIT
    input%max_operator_width(input%iFilter) = 13
    input%operator_bndry_width(input%iFilter) = 11
    input%operator_bndry_depth(input%iFilter) = 6
    input%rhs_interior_stencil_start(input%iFilter) = -6
    input%rhs_interior_stencil_end(input%iFilter)   =  6

    ! ... interior filter
    input%rhs_interior(input%iFilter, 0:-6:-1) = (/ 0.190899511506_dp, -0.171503832236_dp, 0.123632891797_dp, -0.069975429105_dp, 0.029662754736_dp, -0.008520738659_dp, 0.001254597714_dp /)
    input%rhs_interior(input%iFilter, 1:6) = input%rhs_interior(input%iFilter,-1:-6:-1)

    ! ... scale by sigma_d (per Bogey et al.)
    sigma_d = input%filter_fac
    input%rhs_interior(input%iFilter,-6:6) = - sigma_d * input%rhs_interior(input%iFilter,-6:6)
    input%rhs_interior(input%iFilter, 0:0) = 1.0_dp + input%rhs_interior(input%iFilter, 0:0)

    ! ... zero out the boundary filter
    do i = 1, input%operator_bndry_depth(input%iFilter)
      input%rhs_block1(input%iFilter,i,:) = 0.0_dp
    end do

    ! ... (i = 1) on boundary filter :: no filtering
    input%rhs_block1(input%iFilter,1,1) = 1.0_dp

    ! ... (i = 2) 7-pt selective filter
    input%rhs_block1(input%iFilter,2,1:7) = -sigma_d * (/ -0.085777408970_dp, 0.277628171524_dp, -0.356848072173_dp, 0.223119093072_dp, -0.057347064865_dp, -0.000747264596_dp, -0.000027453993_dp /)

    ! ... (i = 3) 7-pt selective filter
    input%rhs_block1(input%iFilter,3,1:7) = -sigma_d * (/ 0.032649010764_dp, -0.143339502575_dp, 0.273321177980_dp, -0.294622121167_dp, 0.186711738069_dp, -0.062038376258_dp, 0.007318073189_dp /)

    ! ... (i = 4) 11-pt selective filter
    input%rhs_block1(input%iFilter,4,1:11) = -sigma_d * (/ -0.000054596010_dp, &
     0.042124772446_dp, -0.173103107841_dp, 0.299615871352_dp, -0.276543612935_dp, 0.131223506571_dp, -0.023424966418_dp, 0.013937561779_dp, -0.024565095706_dp, 0.013098287852_dp, -0.002308621090_dp /)

    ! ... (i = 5) 11-pt selective filter
    input%rhs_block1(input%iFilter,5,1:11) = -sigma_d * (/ 0.008391235145_dp, &
     -0.047402506444_dp, 0.121438547725_dp, -0.200063042812_dp, 0.240069047836_dp, -0.207269200140_dp, 0.122263107844_dp, -0.047121062819_dp, 0.009014891495_dp, 0.001855812216_dp, -0.001176830044_dp /)

    ! ... (i = 6) 11-pt standard filter
    input%rhs_block1(input%iFilter,6,1:11) = -sigma_d * (/ -1.0_dp/1024.0_dp, 5.0_dp/512.0_dp, -45.0_dp/1024.0_dp, 15.0_dp/128.0_dp, -105.0_dp/512.0_dp, 63.0_dp/256.0_dp, &
                                               -105.0_dp/512.0_dp, 15.0_dp/128.0_dp, -45.0_dp/1024.0_dp, 5.0_dp/512.0_dp, -1.0_dp/1024.0_dp /)
    do i = 2, 6
      input%rhs_block1(input%iFilter,i,i) = 1.0_dp + input%rhs_block1(input%iFilter,i,i)
    end do


    Call Mirror_block1_to_block2(input, input%iFilter)

  end subroutine initialize_filter_stencils_drp_SFo13p


  subroutine initialize_filter_stencils_drp_TFo11p(input)

    USE ModGlobal
    USE ModDataStruct

    type(t_mixt_input), pointer :: input

    ! ... local variables
    real(rfreal) :: sigma_d
    integer :: i

    !----------------------------------------------------------------------
    ! filter stencils
    !----------------------------------------------------------------------
    ! ... initialize
    input%overlap(input%iFilterTest) = 6
    input%operator_type(input%iFilterTest) = EXPLICIT
    input%max_operator_width(input%iFilterTest) = 11
    input%operator_bndry_width(input%iFilterTest) = 6
    input%operator_bndry_depth(input%iFilterTest) = 6
    input%rhs_interior_stencil_start(input%iFilterTest) = -5
    input%rhs_interior_stencil_end(input%iFilterTest)   =  5

    ! ... interior filter
    input%rhs_interior(input%iFilterTest,-5:0) = (/ -0.01481379_dp, 0.0_dp, 0.06880899_dp, 0.0_dp, -0.30399520_dp, 0.5_dp /)
    input%rhs_interior(input%iFilterTest, 1:5) = input%rhs_interior(input%iFilterTest,-1:-5:-1)

    ! ... scale by sigma_d (per Bogey et al.)
    sigma_d = input%filter_fac
    input%rhs_interior(input%iFilterTest,-5:5) = - sigma_d * input%rhs_interior(input%iFilterTest,-5:5)
    input%rhs_interior(input%iFilterTest, 0:0) = 1.0_dp + input%rhs_interior(input%iFilterTest, 0:0)

    ! ... (i = 1) on boundary filter :: no filtering
    do i = 1, input%operator_bndry_depth(input%iFilterTest)
      input%rhs_block1(input%iFilterTest,i,:) = 0.0_dp
      input%rhs_block1(input%iFilterTest,i,i) = 1.0_dp
    end do
    Call Mirror_block1_to_block2(input, input%iFilterTest)

  end subroutine initialize_filter_stencils_drp_TFo11p

  subroutine initialize_filter_stencils_Pade_centered(myrank, input)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    integer :: myrank
    type(t_mixt_input), pointer :: input

    ! ... local variables
    integer :: i
    real(rfreal) :: lhs_alpha, lhs_beta
    real(rfreal) :: rhs_a, rhs_b, rhs_c, rhs_d, rhs_e, rhs_f
    real(rfreal) :: rhs_g, rhs_h, rhs_i, rhs_j, rhs_k
    real(rfreal) :: rhs_l

    !----------------------------------------------------------------------
    ! filter stencils
    !----------------------------------------------------------------------
    ! ... initialize
    input%overlap(input%iFilter) = 5
    input%operator_type(input%iFilter) = IMPLICIT
    input%max_operator_width(input%iFilter) = 11
    input%operator_bndry_depth(input%iFilter) = 5
    input%operator_bndry_width(input%iFilter) = 9
    input%rhs_interior_stencil_start(input%iFilter) = -5
    input%rhs_interior_stencil_end(input%iFilter)   =  5
    input%lhs_interior_stencil_start(input%iFilter) = -1
    input%lhs_interior_stencil_end(input%iFilter)   =  1

    ! ... filter free parameter
    lhs_beta  = 0.0_rfreal
    lhs_alpha = input%filter_alphaf

    ! ... interior filter
    rhs_a = (193.0_rfreal + 126.0_rfreal * lhs_alpha) / 256.0_rfreal
    rhs_b = (105.0_rfreal + 302.0_rfreal * lhs_alpha) / 256.0_rfreal
    rhs_c = 15.0_rfreal * (-1.0_rfreal + 2.0_rfreal * lhs_alpha) / 64.0_rfreal
    rhs_d = 45.0_rfreal * (1.0_rfreal - 2.0_rfreal * lhs_alpha) / 512.0_rfreal
    rhs_e = 5.0_rfreal * (-1.0_rfreal + 2.0_rfreal * lhs_alpha) / 256.0_rfreal
    rhs_f = (1.0_rfreal - 2.0_rfreal * lhs_alpha) / 512.0_rfreal
    input%lhs_interior(input%iFilter,-2:2) = (/lhs_beta, lhs_alpha, 1.0_rfreal, lhs_alpha, lhs_beta /)
    input%rhs_interior(input%iFilter,-5:5) = (/ rhs_f, rhs_e, rhs_d, rhs_c, rhs_b, 2.0_rfreal * rhs_a, &
                                                rhs_b, rhs_c, rhs_d, rhs_e, rhs_f /) / 2.0_rfreal

    ! ... boundary filter alphaf
    lhs_alpha = input%bndry_filter_alphaf

    ! ... (i = 1) on boundary filter :: no filtering
    input%lhs_block1(input%iFilter,1,0) = 1.0_rfreal
    input%rhs_block1(input%iFilter,1,1) = 1.0_rfreal

    ! ... (i = 2) boundary filter
    rhs_a = (1.0_rfreal + 2.0_rfreal * lhs_alpha) / 2.0_rfreal
    rhs_b = (1.0_rfreal + 2.0_rfreal * lhs_alpha) / 2.0_rfreal
    input%lhs_block1(input%iFilter,2,-1:1) = (/ lhs_alpha, 1.0_rfreal, lhs_alpha /)
    input%rhs_block1(input%iFilter,2,1:9) = (/ rhs_b, 2.0_rfreal * rhs_a, rhs_b, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp /) / 2.0_rfreal

    ! ... (i = 3) boundary filter
    rhs_a = 0.625_rfreal + 0.75_rfreal * lhs_alpha
    rhs_b = 0.5_rfreal + lhs_alpha
    rhs_c = -0.125_rfreal + 0.25_rfreal * lhs_alpha
    input%lhs_block1(input%iFilter,3,-1:1) = (/ lhs_alpha, 1.0_rfreal, lhs_alpha /)
    input%rhs_block1(input%iFilter,3,1:9) = (/ rhs_c, rhs_b, 2.0_rfreal * rhs_a, rhs_b, rhs_c, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp /) / 2.0_rfreal

    ! ... (i = 4) boundary filter
    rhs_a = 11.0_rfreal / 16.0_rfreal + 0.625_rfreal * lhs_alpha
    rhs_b = 15.0_rfreal / 32.0_rfreal + 17.0_rfreal / 16.0_rfreal * lhs_alpha
    rhs_c = -3.0_rfreal / 16.0_rfreal + 0.375_rfreal * lhs_alpha
    rhs_d = 1.0_rfreal / 32.0_rfreal - lhs_alpha / 16.0_rfreal
    input%lhs_block1(input%iFilter,4,-1:1) = (/ lhs_alpha, 1.0_rfreal, lhs_alpha /)
    input%rhs_block1(input%iFilter,4,1:9) = (/ rhs_d, rhs_c, rhs_b, 2.0_rfreal * rhs_a, rhs_b, rhs_c, rhs_d, 0.0_dp, 0.0_dp /) / 2.0_rfreal

    ! ... (i = 5) boundary filter
    rhs_a = (93.0_rfreal + 70.0_rfreal * lhs_alpha ) / 128.0_rfreal
    rhs_b = (7.0_rfreal + 18.0_rfreal * lhs_alpha) / 16.0_rfreal
    rhs_c = (-7.0_rfreal + 14.0_rfreal * lhs_alpha) / 32.0_rfreal
    rhs_d = 1.0_rfreal / 16.0_rfreal - lhs_alpha / 8.0_rfreal
    rhs_e = -1.0_rfreal / 128.0_rfreal + lhs_alpha / 64.0_rfreal
    input%lhs_block1(input%iFilter,5,-1:1) = (/ lhs_alpha, 1.0_rfreal, lhs_alpha /)
    input%rhs_block1(input%iFilter,5,1:9) = (/ rhs_e, rhs_d, rhs_c, rhs_b, 2.0_rfreal * rhs_a, rhs_b, rhs_c, rhs_d, rhs_e /) / 2.0_rfreal

    do i = 1, input%operator_bndry_depth(input%iFilter)
      input%lhs_block2(input%iFilter,i,-2:2) = input%lhs_block1(input%iFilter,input%operator_bndry_depth(input%iFilter)-(i-1),2:-2:-1)
    end do
    Call Mirror_block1_to_block2(input, input%iFilter)

    ! ... fill mapping array; must be consistent with stencil definition in hypre.c
    if (associated(input%HYPREStencilIndex) .eqv. .false.) then
      allocate(input%HYPREStencilIndex(MAX_OPERATORS,-2:2,-2:2,-2:2))
      input%HYPREStencilIndex = 0
    end if

    if (lhs_beta /= 0) then
      call graceful_exit(myrank, 'ERROR: lhs_beta /= 0 in initialize_filter_stencils_PADE_centered.')
    else
      input%HYPREStencilIndex(input%iFilter, :, :, :) = 0
      input%HYPREStencilIndex(input%iFilter, 0, 0, 0) = 0   ! ... self point
      input%HYPREStencilIndex(input%iFilter,-1, 0, 0) = 1   ! ... i = -1
      input%HYPREStencilIndex(input%iFilter,+1, 0, 0) = 2   ! ... i = +1
      input%HYPREStencilIndex(input%iFilter, 0,-1, 0) = 3   ! ... j = -1
      input%HYPREStencilIndex(input%iFilter, 0,+1, 0) = 4   ! ... j = +1
      input%HYPREStencilIndex(input%iFilter, 0, 0,-1) = 5   ! ... k = -1
      input%HYPREStencilIndex(input%iFilter, 0, 0,+1) = 6   ! ... k = +1
    end if

  end subroutine initialize_filter_stencils_Pade_centered


  subroutine initialize_filter_stencils_Pade_centered_2nd_Order(myrank, input, opID_in, alphaf_in)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    integer :: myrank
    type(t_mixt_input), pointer :: input
    integer, optional :: opID_in
    real(rfreal), optional :: alphaf_in

    ! ... local variables
    integer :: i, opID
    real(rfreal) :: lhs_alpha, lhs_beta
    real(rfreal) :: rhs_a, rhs_b, rhs_c, rhs_d, rhs_e, rhs_f
    real(rfreal) :: rhs_g, rhs_h, rhs_i, rhs_j, rhs_k
    real(rfreal) :: rhs_l

    ! ... defaults
    opID = input%iFilter
    lhs_alpha = input%filter_alphaf

    ! ... optional inputs
    if (present(opID_in) .eqv. .true.) then
      opID = opID_in
      lhs_alpha = alphaf_in
    end if

    !----------------------------------------------------------------------
    ! filter stencils
    !----------------------------------------------------------------------
    ! ... initialize
    input%overlap(opID) = 1
    input%operator_type(opID) = IMPLICIT
    input%max_operator_width(opID) = 3
    input%operator_bndry_depth(opID) = 1
    input%operator_bndry_width(opID) = 1
    input%rhs_interior_stencil_start(opID) = -1
    input%rhs_interior_stencil_end(opID)   =  1
    input%lhs_interior_stencil_start(opID) = -1
    input%lhs_interior_stencil_end(opID)   =  1

    ! ... filter free parameter
    lhs_beta  = 0.0_rfreal

    ! ... all points use same filter
    rhs_a = (1.0_rfreal + 2.0_rfreal * lhs_alpha) / 2.0_rfreal
    rhs_b = (1.0_rfreal + 2.0_rfreal * lhs_alpha) / 2.0_rfreal

    ! ... interior filter
    input%lhs_interior(opID,-1:1) = (/ lhs_alpha, 1.0_rfreal, lhs_alpha /)
    input%rhs_interior(opID,-1:1) = (/ rhs_b, 2.0_rfreal * rhs_a, rhs_b /) / 2.0_rfreal

    ! ... (i = 1) on boundary filter :: no filtering
    input%lhs_block1(opID,1,0) = 1.0_rfreal
    input%rhs_block1(opID,1,1) = 1.0_rfreal

    do i = 1, input%operator_bndry_depth(opID)
      input%lhs_block2(opID,i,-2:2) = input%lhs_block1(opID,input%operator_bndry_depth(opID)-(i-1),2:-2:-1)
    end do
    Call Mirror_block1_to_block2(input, opID)

    ! ... fill mapping array; must be consistent with stencil definition in hypre.c
    if (associated(input%HYPREStencilIndex) .eqv. .false.) then
      allocate(input%HYPREStencilIndex(MAX_OPERATORS,-2:2,-2:2,-2:2))
      input%HYPREStencilIndex = 0
    end if

    if (lhs_beta /= 0) then
      call graceful_exit(myrank, 'ERROR: lhs_beta /= 0 in initialize_filter_stencils_PADE_centered.')
    else
      input%HYPREStencilIndex(opID, :, :, :) = 0
      input%HYPREStencilIndex(opID, 0, 0, 0) = 0   ! ... self point
      input%HYPREStencilIndex(opID,-1, 0, 0) = 1   ! ... i = -1
      input%HYPREStencilIndex(opID,+1, 0, 0) = 2   ! ... i = +1
      input%HYPREStencilIndex(opID, 0,-1, 0) = 3   ! ... j = -1
      input%HYPREStencilIndex(opID, 0,+1, 0) = 4   ! ... j = +1
      input%HYPREStencilIndex(opID, 0, 0,-1) = 5   ! ... k = -1
      input%HYPREStencilIndex(opID, 0, 0,+1) = 6   ! ... k = +1
    end if

  end subroutine initialize_filter_stencils_Pade_centered_2nd_order

  subroutine initialize_filter_stencils_TestFilter(myrank, input)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    integer :: myrank
    type(t_mixt_input), pointer :: input

    ! ... local variables
    integer :: i
    real(rfreal) :: lhs_alpha, lhs_beta
    real(rfreal) :: rhs_a, rhs_b, rhs_c, rhs_d, rhs_e, rhs_f
    real(rfreal) :: rhs_g, rhs_h, rhs_i, rhs_j, rhs_k
    real(rfreal) :: rhs_l

    !----------------------------------------------------------------------
    ! filter stencils
    !----------------------------------------------------------------------

    SELECT CASE( input%LES )
    CASE( 201 ) ! ... Standard dynamic LES model with penta-diagonal test-filter
                ! ... (Bodony, Ph.D thesis p168)
                ! ... alternative interior filter
                ! ... taken from shear layer LES code
      ! ... initialize
      input%overlap(input%iFilterTest)  = 3 ! ... (7 - 1) / 2
      input%operator_type(input%iFilterTest) = IMPLICIT
      input%max_operator_width(input%iFilterTest) = 7
      input%operator_bndry_width(input%iFilterTest) = 9 ! ... max size of boundary stencil
      input%operator_bndry_depth(input%iFilterTest) = 3
      input%rhs_interior_stencil_start(input%iFilterTest) = -3
      input%rhs_interior_stencil_end(input%iFilterTest)   =  3
      input%lhs_interior_stencil_start(input%iFilterTest) = -2
      input%lhs_interior_stencil_end(input%iFilterTest)   =  2

      lhs_alpha = real( 1.6985175214536456D-02,rfreal)
      lhs_beta  = real( 3.0917953843297519D-01,rfreal)
      rhs_a     = real( 5.1273888141090229D-01,rfreal)
      rhs_b     = real( 7.7272238673514593D-01,rfreal)
      rhs_c     = real( 3.1342583223660930D-01,rfreal)
      rhs_d     = real( 5.3442326912365695D-02,rfreal)
      rhs_e     = 0.0_rfreal
      rhs_f     = 0.0_rfreal

      input%lhs_interior(input%iFilterTest,-2:2) = (/lhs_beta, lhs_alpha, 1.0_rfreal, lhs_alpha, lhs_beta /)
      input%rhs_interior(input%iFilterTest,-5:5) = (/ rhs_f, rhs_e, rhs_d, rhs_c, rhs_b, 2.0_rfreal * rhs_a, &
                                                      rhs_b, rhs_c, rhs_d, rhs_e, rhs_f /) / 2.0_rfreal

    CASE( 202 ) ! Standard dynamic LES model with test-filter from Spyropoulos & Blaisdell (AIAA J 1996).
                ! See their Table 2 on p996; 7-pt explicit, least-square filter is the following one.
                ! Since their implicit filter formula (eqn 24 on p994) is different from ours (eqn 5.6
                ! on p162 of prof Bodony's thesis), division by two was made, JKim 12/2007
      ! ... initialize
      input%overlap(input%iFilterTest)  = 3 ! ... (7 - 1) / 2
      input%operator_type(input%iFilterTest) = EXPLICIT
      input%max_operator_width(input%iFilterTest) = 7
      input%operator_bndry_width(input%iFilterTest) = 11 ! ... max size of boundary stencil
      input%operator_bndry_depth(input%iFilterTest) = 3
      input%rhs_interior_stencil_start(input%iFilterTest) = -3
      input%rhs_interior_stencil_end(input%iFilterTest)   =  3

      rhs_a     = 0.5_rfreal
      rhs_b     = 0.6744132_rfreal
      rhs_c     = 0.0_rfreal
      rhs_d     = -0.1744132_rfreal

      input%rhs_interior(input%iFilterTest,-3:3) = (/ rhs_d, rhs_c, rhs_b, 2.0_rfreal * rhs_a, &
                                                      rhs_b, rhs_c, rhs_d /) / 2.0_rfreal

    CASE( 203 ) ! Standard dynamic LES model with test-filter from Spyropoulos & Blaisdell (AIAA J 1996).
                ! See their Table 2 on p996; 5-pt implicit, 7-pt explicit, compromise filter is the following one.
                ! Since their implicit filter formula (eqn 24 on p994) is different from ours (eqn 5.6
                ! on p162 of prof Bodony's thesis), division by two was made, JKim 12/2007
      ! ... initialize
      input%overlap(input%iFilterTest)  = 3 ! ... (7 - 1) / 2
      input%operator_type(input%iFilterTest) = IMPLICIT
      input%max_operator_width(input%iFilterTest) = 7
      input%operator_bndry_width(input%iFilterTest) = 9 ! ... max size of boundary stencil
      input%operator_bndry_depth(input%iFilterTest) = 3
      input%rhs_interior_stencil_start(input%iFilterTest) = -3
      input%rhs_interior_stencil_end(input%iFilterTest)   =  3
      input%lhs_interior_stencil_start(input%iFilterTest) = -2
      input%lhs_interior_stencil_end(input%iFilterTest)   =  2

      lhs_alpha = 0.0_rfreal / 2.0_rfreal
      lhs_beta  = 0.9368185_rfreal / 2.0_rfreal
      rhs_a     = 0.5_rfreal
      rhs_b     = 0.8056734_rfreal
      rhs_c     = 0.4684092_rfreal
      rhs_d     = 0.1627358_rfreal
      rhs_e     = 0.0_rfreal
      rhs_f     = 0.0_rfreal

      input%lhs_interior(input%iFilterTest,-2:2) = (/lhs_beta, lhs_alpha, 1.0_rfreal, lhs_alpha, lhs_beta /)
      input%rhs_interior(input%iFilterTest,-5:5) = (/ rhs_f, rhs_e, rhs_d, rhs_c, rhs_b, 2.0_rfreal * rhs_a, &
                                                      rhs_b, rhs_c, rhs_d, rhs_e, rhs_f /) / 2.0_rfreal

    CASE( 204 ) ! Standard dynamic LES model with test-filter from Bogey & Bailly (JCP 2004)
                ! See Appendix D in their paper for TFo15p-pi/2, JKim 04/2009
      ! ... initialize
      input%overlap(input%iFilterTest)  = 7 ! ... (15 - 1) / 2
      input%operator_type(input%iFilterTest) = EXPLICIT
      input%max_operator_width(input%iFilterTest) = 15
      input%operator_bndry_width(input%iFilterTest) = 11 ! ... max size of boundary stencil
      input%operator_bndry_depth(input%iFilterTest) = 7
      input%rhs_interior_stencil_start(input%iFilterTest) = -7
      input%rhs_interior_stencil_end(input%iFilterTest)   =  7

      rhs_a = 0.5_rfreal
      rhs_b =-0.3099568696454062_rfreal
      rhs_c = 0.0_rfreal
      rhs_d = 0.0823401815052307_rfreal
      rhs_e = 0.0_rfreal
      rhs_f =-0.0303531467960564_rfreal
      rhs_g = 0.0_rfreal
      rhs_h = 0.0079698349362320_rfreal

      input%rhs_interior(input%iFilterTest,0:7) = (/ rhs_a, rhs_b, rhs_c, rhs_d, &
                                                     rhs_e, rhs_f, rhs_g, rhs_h /)
      input%rhs_interior(input%iFilterTest,-1:-7:-1) = input%rhs_interior(input%iFilterTest,1:7)
      input%rhs_interior(input%iFilterTest,-7:7) = - input%rhs_interior(input%iFilterTest,-7:7)
      input%rhs_interior(input%iFilterTest,0:0) = 1.0_rfreal + input%rhs_interior(input%iFilterTest,0:0)

    CASE( 205 ) ! Simple (1-2-1)/4 filter
      input%overlap(input%iFilterTest)  = 1 ! ... (15 - 1) / 2
      input%operator_type(input%iFilterTest) = EXPLICIT
      input%max_operator_width(input%iFilterTest) = 3
      input%operator_bndry_width(input%iFilterTest) = 1 ! ... max size of boundary stencil
      input%operator_bndry_depth(input%iFilterTest) = 1
      input%rhs_interior_stencil_start(input%iFilterTest) = -1
      input%rhs_interior_stencil_end(input%iFilterTest)   =  1

      input%rhs_interior(input%iFilterTest,-1:1) = (/ 1.0_8, 2.0_8, 1.0_8 /) / 4.0_8
      input%rhs_block1(input%iFilterTest,1,1) = 1.0_8
      Call Mirror_block1_to_block2(input, input%iFilterTest)
      return

    CASE DEFAULT
      call graceful_exit(myrank, 'Type of LES is not correctly specified.')

    END SELECT ! input%LES

    ! ... now boundary test-filter
    if (input%operator_type(input%iFilterTest) == IMPLICIT) then

!!$   if (input%parallelTopology /= SLAB) &
!!$     call graceful_exit(region%myrank, 'ERROR: must use PARALLEL_TOPOLOGY=SLAB for implicit test filter.')

      lhs_alpha = 0.45_rfreal

      ! ... (i = 1) on boundary filter :: no filtering
      input%lhs_block1(input%iFilterTest,1,0) = 1.0_rfreal
      input%rhs_block1(input%iFilterTest,1,1) = 1.0_rfreal

      ! ... (i = 2) boundary filter
      rhs_a = (1.0_rfreal + 2.0_rfreal * lhs_alpha) / 2.0_rfreal
      rhs_b = (1.0_rfreal + 2.0_rfreal * lhs_alpha) / 2.0_rfreal
      input%lhs_block1(input%iFilterTest,2,-1:1) = (/ lhs_alpha, 1.0_rfreal, lhs_alpha /)
      input%rhs_block1(input%iFilterTest,2,1:9) = (/ rhs_b, 2.0_rfreal * rhs_a, rhs_b, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp /) / 2.0_rfreal

      ! ... (i = 3) boundary filter
      rhs_a = 0.625_rfreal + 0.75_rfreal * lhs_alpha
      rhs_b = 0.5_rfreal + lhs_alpha
      rhs_c = -0.125_rfreal + 0.25_rfreal * lhs_alpha
      input%lhs_block1(input%iFilterTest,3,-1:1) = (/ lhs_alpha, 1.0_rfreal, lhs_alpha /)
      input%rhs_block1(input%iFilterTest,3,1:9) = (/ rhs_c, rhs_b, 2.0_rfreal * rhs_a, rhs_b, rhs_c, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp /) / 2.0_rfreal

      ! ... (i = 4) boundary filter
      rhs_a = 11.0_rfreal / 16.0_rfreal + 0.625_rfreal * lhs_alpha
      rhs_b = 15.0_rfreal / 32.0_rfreal + 17.0_rfreal / 16.0_rfreal * lhs_alpha
      rhs_c = -3.0_rfreal / 16.0_rfreal + 0.375_rfreal * lhs_alpha
      rhs_d = 1.0_rfreal / 32.0_rfreal - lhs_alpha / 16.0_rfreal
      input%lhs_block1(input%iFilterTest,4,-1:1) = (/ lhs_alpha, 1.0_rfreal, lhs_alpha /)
      input%rhs_block1(input%iFilterTest,4,1:9) = (/ rhs_d, rhs_c, rhs_b, 2.0_rfreal * rhs_a, rhs_b, rhs_c, rhs_d, 0.0_dp, 0.0_dp /) / 2.0_rfreal

      ! ... (i = 5) boundary filter
      rhs_a = (93.0_rfreal + 70.0_rfreal * lhs_alpha ) / 128.0_rfreal
      rhs_b = (7.0_rfreal + 18.0_rfreal * lhs_alpha) / 16.0_rfreal
      rhs_c = (-7.0_rfreal + 14.0_rfreal * lhs_alpha) / 32.0_rfreal
      rhs_d = 1.0_rfreal / 16.0_rfreal - lhs_alpha / 8.0_rfreal
      rhs_e = -1.0_rfreal / 128.0_rfreal + lhs_alpha / 64.0_rfreal
      input%lhs_block1(input%iFilterTest,5,-1:1) = (/ lhs_alpha, 1.0_rfreal, lhs_alpha /)
      input%rhs_block1(input%iFilterTest,5,1:9) = (/ rhs_e, rhs_d, rhs_c, rhs_b, 2.0_rfreal * rhs_a, rhs_b, rhs_c, rhs_d, rhs_e /) / 2.0_rfreal

      do i = 1, input%operator_bndry_depth(input%iFilterTest)
        input%lhs_block2(input%iFilterTest,i,-2:2) = input%lhs_block1(input%iFilterTest,input%operator_bndry_depth(input%iFilterTest)-(i-1),2:-2:-1)
      end do

      Call Mirror_block1_to_block2(input, input%iFilterTest)

      ! ... fill mapping array; must be consistent with stencil definition in hypre.c
      if (associated(input%HYPREStencilIndex) .eqv. .false.) then
        allocate(input%HYPREStencilIndex(MAX_OPERATORS,-2:2,-2:2,-2:2))
        input%HYPREStencilIndex = 0
      end if

      if (lhs_beta /= 0.0_rfreal) then
        input%HYPREStencilIndex(input%iFilterTest, :, :, :) = 0
        input%HYPREStencilIndex(input%iFilterTest, 0, 0, 0) = 0
        input%HYPREStencilIndex(input%iFilterTest,-2, 0, 0) = 1
        input%HYPREStencilIndex(input%iFilterTest,-1, 0, 0) = 2
        input%HYPREStencilIndex(input%iFilterTest,+1, 0, 0) = 3
        input%HYPREStencilIndex(input%iFilterTest,+2, 0, 0) = 4
        input%HYPREStencilIndex(input%iFilterTest, 0,-2, 0) = 5
        input%HYPREStencilIndex(input%iFilterTest, 0,-1, 0) = 6
        input%HYPREStencilIndex(input%iFilterTest, 0,+1, 0) = 7
        input%HYPREStencilIndex(input%iFilterTest, 0,+2, 0) = 8
        input%HYPREStencilIndex(input%iFilterTest, 0, 0,-2) = 9
        input%HYPREStencilIndex(input%iFilterTest, 0, 0,-1) = 10
        input%HYPREStencilIndex(input%iFilterTest, 0, 0,+1) = 11
        input%HYPREStencilIndex(input%iFilterTest, 0, 0,+2) = 12
      else
        input%HYPREStencilIndex(input%iFilterTest, :, :, :) = 0
        input%HYPREStencilIndex(input%iFilterTest, 0, 0, 0) = 0   ! ... self point
        input%HYPREStencilIndex(input%iFilterTest,-1, 0, 0) = 1   ! ... i = -1
        input%HYPREStencilIndex(input%iFilterTest,+1, 0, 0) = 2   ! ... i = +1
        input%HYPREStencilIndex(input%iFilterTest, 0,-1, 0) = 3   ! ... j = -1
        input%HYPREStencilIndex(input%iFilterTest, 0,+1, 0) = 4   ! ... j = +1
        input%HYPREStencilIndex(input%iFilterTest, 0, 0,-1) = 5   ! ... k = -1
        input%HYPREStencilIndex(input%iFilterTest, 0, 0,+1) = 6   ! ... k = +1
      end if

    else if (input%operator_type(input%iFilterTest) == EXPLICIT) then
      ! ... DRP wavenumber-optimized explicit test filter near domain boundary
      ! ... See Appendix D of Bogey & Bailly (JCP 2004), JKim 04/2009

      ! ... (i = 1) :: not test-filtered
      input%rhs_block1(input%iFilterTest,1,1) = 1.0_rfreal

      ! ... (i = 2) :: not test-filtered
      input%rhs_block1(input%iFilterTest,2,2) = 1.0_rfreal

      ! ... (i = 3) :: not test-filtered
      input%rhs_block1(input%iFilterTest,3,3) = 1.0_rfreal

      ! ... (i = 4) boundary test filter :: 7pt DRP test filter, TFo7p-pi/2
      rhs_a = 0.5_rfreal
      rhs_b =-0.3046962713975999_rfreal
      rhs_c = 0.0_rfreal
      rhs_d = 0.0546962713975999_rfreal
      input%rhs_block1(input%iFilterTest,4,1:7) = (/ rhs_d, rhs_c, rhs_b, &
                                                     rhs_a,               &
                                                     rhs_b, rhs_c, rhs_d /)

      ! ... (i = 5) boundary test filter :: 7pt DRP test filter, TFo7p-pi/2
      input%rhs_block1(input%iFilterTest,5,2:8) = (/ rhs_d, rhs_c, rhs_b, &
                                                     rhs_a,               &
                                                     rhs_b, rhs_c, rhs_d /)

      ! ... (i = 6) boundary test filter :: 11pt DRP test filter, TFo11p-pi/2
      rhs_a = 0.5_rfreal
      rhs_b =-0.3062243901271355_rfreal
      rhs_c = 0.0_rfreal
      rhs_d = 0.0758387730462204_rfreal
      rhs_e = 0.0_rfreal
      rhs_f =-0.0196143829190849_rfreal
      input%rhs_block1(input%iFilterTest,6,1:11) = &
                                         (/ rhs_f, rhs_e, rhs_d, rhs_c, rhs_b, &
                                            rhs_a,                             &
                                            rhs_b, rhs_c, rhs_d, rhs_e, rhs_f/)

      ! ... (i = 7) boundary test filter :: 11pt DRP test filter, TFo11p-pi/2
      input%rhs_block1(input%iFilterTest,7,2:12) = &
                                         (/ rhs_f, rhs_e, rhs_d, rhs_c, rhs_b, &
                                            rhs_a,                             &
                                            rhs_b, rhs_c, rhs_d, rhs_e, rhs_f/)

      input%rhs_block1(input%iFilterTest,4:7,:) = - input%rhs_block1(input%iFilterTest,4:7,:)
      do i = 4,7
        input%rhs_block1(input%iFilterTest,i,i) = 1.0_rfreal + input%rhs_block1(input%iFilterTest,i,i)
      end do ! i

      Call Mirror_block1_to_block2(input, input%iFilterTest)

    end if ! input%operator_type(input%iFilterTest)

  end subroutine initialize_filter_stencils_TestFilter

  subroutine initialize_filter_stencils_TEST(myrank, input)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    integer :: myrank
    type(t_mixt_input), pointer :: input

    ! ... local variables
    integer :: i
    real(rfreal) :: lhs_alpha, lhs_beta
    real(rfreal) :: rhs_a, rhs_b, rhs_c, rhs_d, rhs_e, rhs_f
    real(rfreal) :: rhs_g, rhs_h, rhs_i, rhs_j, rhs_k
    real(rfreal) :: rhs_l

    !----------------------------------------------------------------------
    ! filter stencils
    !----------------------------------------------------------------------
    ! ... initialize
    input%overlap(input%iFilterTest)  = 3
    input%operator_type(input%iFilterTest) = IMPLICIT
    input%max_operator_width(input%iFilterTest) = 7
    input%operator_bndry_depth(input%iFilterTest) = 5
    input%operator_bndry_width(input%iFilterTest) = 9
    input%rhs_interior_stencil_start(input%iFilterTest) = -5
    input%rhs_interior_stencil_end(input%iFilterTest)   =  5
    input%lhs_interior_stencil_start(input%iFilterTest) = -2
    input%lhs_interior_stencil_end(input%iFilterTest)   =  2

    SELECT CASE( input%LES )
    CASE( 201 ) ! Standard dynamic LES model with test-filter from prof Bodony's thesis p168.
      ! ... alternative interior filter
      ! ... taken from shear layer LES code
      lhs_alpha = real( 1.6985175214536456D-02,rfreal)
      lhs_beta  = real( 3.0917953843297519D-01,rfreal)
      rhs_a     = real( 5.1273888141090229D-01,rfreal)
      rhs_b     = real( 7.7272238673514593D-01,rfreal)
      rhs_c     = real( 3.1342583223660930D-01,rfreal)
      rhs_d     = real( 5.3442326912365695D-02,rfreal)
      rhs_e     = 0.0_rfreal
      rhs_f     = 0.0_rfreal

    CASE( 202 ) ! Standard dynamic LES model with test-filter from Spyropoulos & Blaisdell (AIAA J 1996).
      ! See their Table 2 on p996; 7-pt explicit, least-square filter is the following one.
      ! Since their implicit filter formula (eqn 24 on p994) is different from ours (eqn 5.6
      ! on p162 of prof Bodony's thesis), division by two was made, JKim 12/2007
      lhs_alpha = 0.0_rfreal / 2.0_rfreal ! explicit filter; discrepancy in filter formula
      lhs_beta  = 0.0_rfreal / 2.0_rfreal ! explicit filter; discrepancy in filter formula
      rhs_a     = 0.5_rfreal
      rhs_b     = 0.6744132_rfreal
      rhs_c     = 0.0_rfreal
      rhs_d     = -0.1744132_rfreal
      rhs_e     = 0.0_rfreal
      rhs_f     = 0.0_rfreal

    CASE( 203 ) ! Standard dynamic LES model with test-filter from Spyropoulos & Blaisdell (AIAA J 1996).
      ! See their Table 2 on p996; 5-pt implicit, 7-pt explicit, compromise filter is the following one.
      ! Since their implicit filter formula (eqn 24 on p994) is different from ours (eqn 5.6
      ! on p162 of prof Bodony's thesis), division by two was made, JKim 12/2007
      lhs_alpha = 0.0_rfreal / 2.0_rfreal ! explicit filter; discrepancy in filter formula
      lhs_beta  = 0.9368185_rfreal / 2.0_rfreal ! explicit filter; discrepancy in filter formula
      rhs_a     = 0.5_rfreal
      rhs_b     = 0.8056734_rfreal
      rhs_c     = 0.4684092_rfreal
      rhs_d     = 0.1627358_rfreal
      rhs_e     = 0.0_rfreal
      rhs_f     = 0.0_rfreal

    CASE DEFAULT
      call graceful_exit(myrank, 'Type of LES is not correctly specified.')
    END SELECT ! input%LES

    input%lhs_interior(input%iFilterTest,-2:2) = (/lhs_beta, lhs_alpha, 1.0_rfreal, lhs_alpha, lhs_beta /)
    input%rhs_interior(input%iFilterTest,-5:5) = (/ rhs_f, rhs_e, rhs_d, rhs_c, rhs_b, 2.0_rfreal * rhs_a, &
         rhs_b, rhs_c, rhs_d, rhs_e, rhs_f /) / 2.0_rfreal

    lhs_alpha = 0.45_rfreal

    ! ... (i = 1) on boundary filter :: no filtering
    input%lhs_block1(input%iFilterTest,1,0) = 1.0_rfreal
    input%rhs_block1(input%iFilterTest,1,1) = 1.0_rfreal

    ! ... (i = 2) boundary filter
    rhs_a = (1.0_rfreal + 2.0_rfreal * lhs_alpha) / 2.0_rfreal
    rhs_b = (1.0_rfreal + 2.0_rfreal * lhs_alpha) / 2.0_rfreal
    input%lhs_block1(input%iFilterTest,2,-1:1) = (/ lhs_alpha, 1.0_rfreal, lhs_alpha /)
    input%rhs_block1(input%iFilterTest,2,1:9) = (/ rhs_b, 2.0_rfreal * rhs_a, rhs_b, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp /) / 2.0_rfreal

    ! ... (i = 3) boundary filter
    rhs_a = 0.625_rfreal + 0.75_rfreal * lhs_alpha
    rhs_b = 0.5_rfreal + lhs_alpha
    rhs_c = -0.125_rfreal + 0.25_rfreal * lhs_alpha
    input%lhs_block1(input%iFilterTest,3,-1:1) = (/ lhs_alpha, 1.0_rfreal, lhs_alpha /)
    input%rhs_block1(input%iFilterTest,3,1:9) = (/ rhs_c, rhs_b, 2.0_rfreal * rhs_a, rhs_b, rhs_c, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp /) / 2.0_rfreal

    ! ... (i = 4) boundary filter
    rhs_a = 11.0_rfreal / 16.0_rfreal + 0.625_rfreal * lhs_alpha
    rhs_b = 15.0_rfreal / 32.0_rfreal + 17.0_rfreal / 16.0_rfreal * lhs_alpha
    rhs_c = -3.0_rfreal / 16.0_rfreal + 0.375_rfreal * lhs_alpha
    rhs_d = 1.0_rfreal / 32.0_rfreal - lhs_alpha / 16.0_rfreal
    input%lhs_block1(input%iFilterTest,4,-1:1) = (/ lhs_alpha, 1.0_rfreal, lhs_alpha /)
    input%rhs_block1(input%iFilterTest,4,1:9) = (/ rhs_d, rhs_c, rhs_b, 2.0_rfreal * rhs_a, rhs_b, rhs_c, rhs_d, 0.0_dp, 0.0_dp /) / 2.0_rfreal

    ! ... (i = 5) boundary filter
    rhs_a = (93.0_rfreal + 70.0_rfreal * lhs_alpha ) / 128.0_rfreal
    rhs_b = (7.0_rfreal + 18.0_rfreal * lhs_alpha) / 16.0_rfreal
    rhs_c = (-7.0_rfreal + 14.0_rfreal * lhs_alpha) / 32.0_rfreal
    rhs_d = 1.0_rfreal / 16.0_rfreal - lhs_alpha / 8.0_rfreal
    rhs_e = -1.0_rfreal / 128.0_rfreal + lhs_alpha / 64.0_rfreal
    input%lhs_block1(input%iFilterTest,5,-1:1) = (/ lhs_alpha, 1.0_rfreal, lhs_alpha /)
    input%rhs_block1(input%iFilterTest,5,1:9) = (/ rhs_e, rhs_d, rhs_c, rhs_b, 2.0_rfreal * rhs_a, rhs_b, rhs_c, rhs_d, rhs_e /) / 2.0_rfreal

    do i = 1, input%operator_bndry_depth(input%iFilterTest)
      input%lhs_block2(input%iFilterTest,i,-2:2) = input%lhs_block1(input%iFilterTest,input%operator_bndry_depth(input%iFilterTest)-(i-1),2:-2:-1)
    end do

    Call Mirror_block1_to_block2(input, input%iFilterTest)

    ! ... fill mapping array; must be consistent with stencil definition in hypre.c
    if (associated(input%HYPREStencilIndex) .eqv. .false.) then
      allocate(input%HYPREStencilIndex(MAX_OPERATORS,-2:2,-2:2,-2:2))
      input%HYPREStencilIndex = 0
    end if

    if (lhs_beta /= 0.0_rfreal) then
      input%HYPREStencilIndex(input%iFilterTest, :, :, :) = 0
      input%HYPREStencilIndex(input%iFilterTest, 0, 0, 0) = 0
      input%HYPREStencilIndex(input%iFilterTest,-2, 0, 0) = 1
      input%HYPREStencilIndex(input%iFilterTest,-1, 0, 0) = 2
      input%HYPREStencilIndex(input%iFilterTest,+1, 0, 0) = 3
      input%HYPREStencilIndex(input%iFilterTest,+2, 0, 0) = 4
      input%HYPREStencilIndex(input%iFilterTest, 0,-2, 0) = 5
      input%HYPREStencilIndex(input%iFilterTest, 0,-1, 0) = 6
      input%HYPREStencilIndex(input%iFilterTest, 0,+1, 0) = 7
      input%HYPREStencilIndex(input%iFilterTest, 0,+2, 0) = 8
      input%HYPREStencilIndex(input%iFilterTest, 0, 0,-2) = 9
      input%HYPREStencilIndex(input%iFilterTest, 0, 0,-1) = 10
      input%HYPREStencilIndex(input%iFilterTest, 0, 0,+1) = 11
      input%HYPREStencilIndex(input%iFilterTest, 0, 0,+2) = 12
    else
      input%HYPREStencilIndex(input%iFilterTest, :, :, :) = 0
      input%HYPREStencilIndex(input%iFilterTest, 0, 0, 0) = 0   ! ... self point
      input%HYPREStencilIndex(input%iFilterTest,-1, 0, 0) = 1   ! ... i = -1
      input%HYPREStencilIndex(input%iFilterTest,+1, 0, 0) = 2   ! ... i = +1
      input%HYPREStencilIndex(input%iFilterTest, 0,-1, 0) = 3   ! ... j = -1
      input%HYPREStencilIndex(input%iFilterTest, 0,+1, 0) = 4   ! ... j = +1
      input%HYPREStencilIndex(input%iFilterTest, 0, 0,-1) = 5   ! ... k = -1
      input%HYPREStencilIndex(input%iFilterTest, 0, 0,+1) = 6   ! ... k = +1
    end if


  end subroutine initialize_filter_stencils_TEST

  subroutine initialize_filter_stencils_GaussianHyperviscosity(input)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    type(t_mixt_input), pointer :: input

    ! ... local variables
    integer :: i
    real(rfreal) :: lhs_alpha, lhs_beta
    real(rfreal) :: rhs_a, rhs_b, rhs_c, rhs_d, rhs_e, rhs_f
    real(rfreal) :: rhs_g, rhs_h, rhs_i, rhs_j, rhs_k
    real(rfreal) :: rhs_l

    !----------------------------------------------------------------------
    ! filter stencils
    !----------------------------------------------------------------------

    if (input%shock == FALSE) return ! ... defensive programming

    ! ... in the following, the approximate truncated 9-pt. Gaussian filter of 
    ! ... Cook & Cabot (JCP 2004) is implemented.  Boundary treatment is same 
    ! ... as Kawai & Lele (JCP 2008); i.e. mirror stencil points outside of 
    ! ... boundary into.

    ! ... initialize
    input%overlap(input%iFilterGaussianHyperviscosity)  = 4 ! ... (9 - 1) / 2
    input%operator_type(input%iFilterGaussianHyperviscosity) = EXPLICIT
    input%max_operator_width(input%iFilterGaussianHyperviscosity) = 9
    input%operator_bndry_width(input%iFilterGaussianHyperviscosity) = 8 ! ... max size of boundary stencil
    input%operator_bndry_depth(input%iFilterGaussianHyperviscosity) = 4
    input%rhs_interior_stencil_start(input%iFilterGaussianHyperviscosity) = -4
    input%rhs_interior_stencil_end(input%iFilterGaussianHyperviscosity)   =  4

    rhs_a = 3565.0_rfreal / 10368.0_rfreal
    rhs_b = 3091.0_rfreal / 12960.0_rfreal
    rhs_c = 1997.0_rfreal / 25920.0_rfreal
    rhs_d = 149.0_rfreal  / 12960.0_rfreal
    rhs_e = 107.0_rfreal  / 103680.0_rfreal

    input%rhs_interior(input%iFilterGaussianHyperviscosity,-4:4) = (/ rhs_e, rhs_d, rhs_c, rhs_b, rhs_a, &
                                                                      rhs_b, rhs_c, rhs_d, rhs_e /)

    ! ... boundary treatment
    ! ... as in Kawai & Lele (JCP 2008), unavailable stencil points are simply 
    ! ... mirrored from the opposite sided ones.

    ! ... (i = 1) :: all lefthand side points are mirrored
    input%rhs_block1(input%iFilterGaussianHyperviscosity,1,1:5) = &
                                                (/ rhs_a, 2.0_rfreal*rhs_b, &
                                                          2.0_rfreal*rhs_c, &
                                                          2.0_rfreal*rhs_d, &
                                                          2.0_rfreal*rhs_e /)

    ! ... (i = 2) :: left three points are mirrored
    input%rhs_block1(input%iFilterGaussianHyperviscosity,2,1:6) = &
                                         (/ rhs_b, rhs_a, rhs_b, &
                                                          2.0_rfreal*rhs_c, &
                                                          2.0_rfreal*rhs_d, &
                                                          2.0_rfreal*rhs_e /)

    ! ... (i = 3) :: left two points are mirrored
    input%rhs_block1(input%iFilterGaussianHyperviscosity,3,1:7) = &
                                  (/ rhs_c, rhs_b, rhs_a, rhs_b, rhs_c,     &
                                                          2.0_rfreal*rhs_d, &
                                                          2.0_rfreal*rhs_e /)

    ! ... (i = 3) :: left one points are mirrored
    input%rhs_block1(input%iFilterGaussianHyperviscosity,4,1:8) = &
                           (/ rhs_d, rhs_c, rhs_b, rhs_a, rhs_b, rhs_c, rhs_d, &
                                                          2.0_rfreal*rhs_e /)

    Call Mirror_block1_to_block2(input, input%iFilterGaussianHyperviscosity)

  end subroutine initialize_filter_stencils_GaussianHyperviscosity

  subroutine Mirror_block1_to_block2(input, opID)

    USE ModGlobal
    USE ModDataStruct

    Implicit None

    type(t_mixt_input), pointer :: input
    Integer :: opID, bndry_depth, bndry_width, ii, jj
    Real(rfreal) :: sgn

    sgn = 1.0_rfreal
    If (opID == input%iFirstDeriv .OR. opID == input%iFirstDerivImplicit .or. opID == input%iFirstDerivBiased) sgn = -1.0_rfreal
    If (opID == input%iSecondDerivImplicitArtDiss) sgn = -1.0_rfreal

    ! ... boundary size
    bndry_depth = input%operator_bndry_depth(opID)
    bndry_width = input%operator_bndry_width(opID)

    ! ... add in the new portion
    do ii = 0, bndry_depth-1
      do jj = 0, bndry_width-1
        input%rhs_block2(opID,ii+1,jj+1) = sgn * input%rhs_block1(opID,bndry_depth-ii,bndry_width-jj)
      end do
    end do

  end subroutine Mirror_block1_to_block2

  subroutine initialize_deriv_stencils_DRP_11pt_13pt(myrank, input)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    integer :: myrank
    type(t_mixt_input), pointer :: input

    ! ... local variables
    real(rfreal) :: rhs_a, rhs_b, rhs_c, rhs_d, rhs_e, rhs_f, rhs_g, rhs_h
    real(rfreal) :: rhs_i, rhs_j, rhs_k, rhs_l
    integer :: i

    !----------------------------------------------------------------------
    ! derivative stencils
    !----------------------------------------------------------------------

    ! ... interior first derivative :: 4th order 13pt DRP, FDo13p (Bogey & Bailly, JCP 2004)
    ! ... initialize
    input%overlap(input%iFirstDeriv) = 6 ! ... (13 - 1) / 2
    input%operator_type(input%iFirstDeriv) = EXPLICIT
    input%max_operator_width(input%iFirstDeriv) = 13
    input%operator_bndry_width(input%iFirstDeriv) = 11 ! ... max size of boundary stencil
    input%operator_bndry_depth(input%iFirstDeriv) = 6 ! ... (13 - 1) / 2
    input%rhs_interior_stencil_start(input%iFirstDeriv) = -6
    input%rhs_interior_stencil_end(input%iFirstDeriv)   =  6

    ! ... coefficients from Appendix A of Bogey & Bailly, JCP 2004
    !rhs_a = 0.907646591371_rfreal
    !rhs_b =-0.337048393268_rfreal
    !rhs_c = 0.133442885327_rfreal
    !rhs_d =-0.045246480208_rfreal
    !rhs_e = 0.011169294114_rfreal
    !rhs_f =-0.001456501759_rfreal
    ! ... interior first derivative :: 4th order 13pt DRP (unconstrained L2 minimization)
    rhs_a = 0.9108399939350763_rfreal
    rhs_b =-0.3419530103834698_rfreal
    rhs_c = 0.1380392000060904_rfreal
    rhs_d =-0.0482698633738275_rfreal
    rhs_e = 0.0124728499314050_rfreal
    rhs_f =-0.0017227282246871_rfreal

    input%rhs_interior(input%iFirstDeriv,-6:6)  = &
                            (/ -rhs_f, -rhs_e, -rhs_d, -rhs_c, -rhs_b, -rhs_a, &
                                0.0_rfreal,                                    &
                                rhs_a,  rhs_b,  rhs_c,  rhs_d,  rhs_e,  rhs_f /)

    ! ... (i = 1) on boundary first derivative :: 4th order 7pt DRP, FD06 (Berland et al., JCP 2007)
    input%rhs_block1(input%iFirstDeriv,1,1:7) = (/ -2.225833963270_rfreal, &
                                                    4.827779580575_rfreal, &
                                                   -5.001388453836_rfreal, &
                                                    3.911103941646_rfreal, &
                                                   -2.115267458633_rfreal, &
                                                    0.718882784412_rfreal, &
                                                   -0.115276430894_rfreal /)

    ! ... (i = 2) on boundary first derivative :: 4th order 7pt DRP, FD15 (Berland et al., JCP 2007)
    input%rhs_block1(input%iFirstDeriv,2,1:7) = (/ -0.212932721951_rfreal, &
                                                   -1.060320390770_rfreal, &
                                                    2.078926116439_rfreal, &
                                                   -1.287179452384_rfreal, &
                                                    0.685176395471_rfreal, &
                                                   -0.245320613994_rfreal, &
                                                    0.041650667189_rfreal /)

!!$ ! ... (i = 1) on boundary first derivative :: 4th order 5pt explicit (Carpenter et al., JCP 1993)
!!$ input%rhs_block1(input%iFirstDeriv,1,1:5) = (/ -25.0_dp, 48.0_dp, -36.0_dp, 16.0_dp, -3.0_dp /) / 12.0_dp

!!$ ! ... (i = 2) on boundary first derivative :: 4th order 5pt explicit (Carpenter et al., JCP 1993)
!!$ input%rhs_block1(input%iFirstDeriv,2,1:5) = (/ -3.0_dp, -10.0_dp, 18.0_dp, -6.0_dp, 1.0_dp /) / 12.0_dp

!!$ ! ... (i = 3) on boundary first derivative :: 4th order 7pt DRP, FD24 (Berland et al., JCP 2007)
!!$ input%rhs_block1(input%iFirstDeriv,3,1:7) = (/  0.048264094108_rfreal, &
!!$                                                -0.488255830845_rfreal, &
!!$                                                -0.366015590723_rfreal, &
!!$                                                 1.048005455857_rfreal, &
!!$                                                -0.289325926394_rfreal, &
!!$                                                 0.050392437692_rfreal, &
!!$                                                -0.003064639693_rfreal /)

    ! ... (i = 3) on boundary first derivative :: 4th order 5pt standard scheme
    rhs_a = 8.0_rfreal / 12.0_rfreal
    rhs_b =-1.0_rfreal / 12.0_rfreal
    input%rhs_block1(input%iFirstDeriv,3,1:5) = (/ -rhs_b, -rhs_a, &
                                                    0.0_rfreal,    &
                                                    rhs_a,  rhs_b /)

    ! ... (i = 4) on boundary first derivative :: 4th order 7pt DRP, FDo7p (Bogey & Bailly, JCP 2004)
    rhs_a = 0.797921633542548_rfreal
    rhs_b =-0.18833730683403838_rfreal
    rhs_c = 0.02625099337517627_rfreal
    input%rhs_block1(input%iFirstDeriv,4,1:7) = &
                                    (/ -rhs_c, -rhs_b, -rhs_a, &
                                        0.0_rfreal,            &
                                        rhs_a,  rhs_b,  rhs_c /)

    ! ... (i = 5) on boundary first derivative :: 4th order 9pt DRP, FDo9p (Bogey & Bailly, JCP 2004)
    rhs_a = 0.841570125482_rfreal
    rhs_b =-0.244678631765_rfreal
    rhs_c = 0.059463584768_rfreal
    rhs_d =-0.007650904064_rfreal
    input%rhs_block1(input%iFirstDeriv,5,1:9) = &
                                    (/ -rhs_d, -rhs_c, -rhs_b, -rhs_a, &
                                        0.0_rfreal,                    &
                                        rhs_a,  rhs_b,  rhs_c,  rhs_d /)

    if (.false.) then
    ! ... (i = 1) on boundary first derivative :: 4th order 11pt DRP, FD010 (Berland et al., JCP 2007)
    input%rhs_block1(input%iFirstDeriv,1,1:11) = (/ -2.391602219538_rfreal, &
                                                     5.832490322294_rfreal, &
                                                    -7.650218001182_rfreal, &
                                                     7.907810563576_rfreal, &
                                                    -5.922599052629_rfreal, &
                                                     3.071037015445_rfreal, &
                                                    -1.014956769726_rfreal, &
                                                     0.170022256519_rfreal, &
                                                     0.002819958377_rfreal, &
                                                    -0.004791009708_rfreal, &
                                                    -0.000013063429_rfreal /)

    ! ... (i = 2) on boundary first derivative :: 4th order 11pt DRP, FD19 (Berland et al., JCP 2007)
    input%rhs_block1(input%iFirstDeriv,2,1:11) = (/ -0.180022054228_rfreal, &
                                                    -1.237550583044_rfreal, &
                                                     2.484731692990_rfreal, &
                                                    -1.810320814061_rfreal, &
                                                     1.112990048440_rfreal, &
                                                    -0.481086916514_rfreal, &
                                                     0.126598690230_rfreal, &
                                                    -0.015510730165_rfreal, &
                                                     0.000021609059_rfreal, &
                                                     0.000156447571_rfreal, &
                                                    -0.000007390277_rfreal /)

    ! ... (i = 3) on boundary first derivative :: 4th order 11pt DRP, FD28 (Berland et al., JCP 2007)
    input%rhs_block1(input%iFirstDeriv,3,1:11) = (/  0.057982271137_rfreal, &
                                                    -0.536135360383_rfreal, &
                                                    -0.264089548967_rfreal, &
                                                     0.917445877606_rfreal, &
                                                    -0.169688364841_rfreal, &
                                                    -0.029716326170_rfreal, &
                                                     0.029681617641_rfreal, &
                                                    -0.005222483773_rfreal, &
                                                    -0.000118806260_rfreal, &
                                                    -0.000118806260_rfreal, &
                                                    -0.000020069730_rfreal /)

    ! ... (i = 4) on boundary first derivative :: 4th order 11pt DRP, FD37 (Berland et al., JCP 2007)
    input%rhs_block1(input%iFirstDeriv,4,1:11) = (/ -0.013277273810_rfreal, &
                                                     0.115976072920_rfreal, &
                                                    -0.617479187931_rfreal, &
                                                    -0.274113948206_rfreal, &
                                                     1.086208764655_rfreal, &
                                                    -0.402951626982_rfreal, &
                                                     0.131066986242_rfreal, &
                                                    -0.028154858354_rfreal, &
                                                     0.002596328316_rfreal, &
                                                     0.000128743150_rfreal, &
                                                     0.0_rfreal /)

    ! ... (i = 5) on boundary first derivative :: 4th order 11pt DRP, FD46 (Berland et al., JCP 2007)
    input%rhs_block1(input%iFirstDeriv,5,1:11) = (/  0.016756572303_rfreal, &
                                                    -0.117478455239_rfreal, &
                                                     0.411034935097_rfreal, &
                                                    -1.130286765151_rfreal, &
                                                     0.341435872100_rfreal, &
                                                     0.556396830543_rfreal, &
                                                    -0.082525734207_rfreal, &
                                                     0.003565834658_rfreal, &
                                                     0.001173034777_rfreal, &
                                                    -0.000071772671_rfreal, &
                                                    -0.000000352273_rfreal /)
    end if ! .false.

    ! ... (i = 6) on boundary first derivative :: 4th order 11pt DRP, FDo11p (Bogey & Bailly, JCP 2004)
    rhs_a = 0.872756993962_rfreal
    rhs_b =-0.286511173973_rfreal
    rhs_c = 0.090320001280_rfreal
    rhs_d =-0.020779405824_rfreal
    rhs_e = 0.002484594688_rfreal
    input%rhs_block1(input%iFirstDeriv,6,1:11) = &
                                    (/ -rhs_e, -rhs_d, -rhs_c, -rhs_b, -rhs_a, &
                                        0.0_rfreal,                            &
                                        rhs_a,  rhs_b,  rhs_c,  rhs_d,  rhs_e /)

    Call Mirror_block1_to_block2(input, input%iFirstDeriv)

    ! ... explicit expressions for first derivatives on the boundary
    input%bndry_explicit_deriv(:) = 0.0_rfreal
    input%bndry_explicit_deriv(1:11) = input%rhs_block1(input%iFirstDeriv,1,1:11)

    ! ... interior second derivative :: 4th order 13pt DRP (unconstrained L2 minimization)
    ! ... initialize
    input%overlap(input%iSecondDeriv) = 6 ! ... (13 - 1) / 2
    input%operator_type(input%iSecondDeriv) = EXPLICIT
    input%max_operator_width(input%iSecondDeriv) = 13
    input%operator_bndry_width(input%iSecondDeriv) = 11 ! ... max size of boundary stencil
    input%operator_bndry_depth(input%iSecondDeriv) = 6 ! ... (13 - 1) / 2
    input%rhs_interior_stencil_start(input%iSecondDeriv) = -6
    input%rhs_interior_stencil_end(input%iSecondDeriv)   =  6

    rhs_a =-3.093254498934464_rfreal
    rhs_b = 1.811745716855425_rfreal
    rhs_c =-0.334371988994845_rfreal
    rhs_d = 0.087342298002619_rfreal
    rhs_e =-0.021865409836906_rfreal
    rhs_f = 0.004222789943907_rfreal
    rhs_g =-0.000446156502965_rfreal
    input%rhs_interior(input%iSecondDeriv,-6:6) = &
                            (/ rhs_g, rhs_f, rhs_e, rhs_d, rhs_c, rhs_b, &
                               rhs_a,                                    &
                               rhs_b, rhs_c, rhs_d, rhs_e, rhs_f, rhs_g /)

    ! ... (i = 1) on boundary second derivative :: 2nd order non-optimized off-centered (unstable!)
    input%rhs_block1(input%iSecondDeriv,1,1:4) = (/ 2.0_rfreal,-5.0_rfreal, 4.0_rfreal,-1.0_rfreal /)

    ! ... (i = 2) on boundary second derivative :: 2nd order non-optimized centered
    input%rhs_block1(input%iSecondDeriv,2,1:3) = (/ 1.0_rfreal,-2.0_rfreal, 1.0_rfreal /)

    ! ... (i = 3) on boundary second derivative :: 2nd order 5pt DRP (unconstrained L2 minimization)
    rhs_a =-2.6501878863946975_rfreal
    rhs_b = 1.4334585909297983_rfreal
    rhs_c =-0.1083646477324495_rfreal
    input%rhs_block1(input%iSecondDeriv,3,1:5) = &
                                (/ rhs_c, rhs_b, &
                                   rhs_a,        &
                                   rhs_b, rhs_c /)

    ! ... (i = 4) on boundary second derivative :: 4th order 7pt DRP (unconstrained L2 minimization)
    rhs_a =-2.8132133628971876_rfreal
    rhs_b = 1.5682433555062238_rfreal
    rhs_c =-0.1772973422024896_rfreal
    rhs_d = 0.0156606681448593_rfreal
    input%rhs_block1(input%iSecondDeriv,4,1:7) = &
                         (/ rhs_d, rhs_c, rhs_b, &
                            rhs_a,               &
                            rhs_b, rhs_c, rhs_d /)

    ! ... (i = 5) on boundary second derivative :: 4th order 9pt DRP (unconstrained L2 minimization)
    rhs_a =-2.940231881416821_rfreal
    rhs_b = 1.676016911509531_rfreal
    rhs_c =-0.240422231985544_rfreal
    rhs_d = 0.038095451788304_rfreal
    rhs_e =-0.003574190603880_rfreal
    input%rhs_block1(input%iSecondDeriv,5,1:9) = &
                  (/ rhs_e, rhs_d, rhs_c, rhs_b, &
                     rhs_a,                      &
                     rhs_b, rhs_c, rhs_d, rhs_e /)

    ! ... (i = 6) on boundary second derivative :: 4th order 11pt DRP (unconstrained L2 minimization)
    rhs_a =-3.0123727089085417_rfreal
    rhs_b = 1.7391545107767232_rfreal
    rhs_c =-0.2822843037811674_rfreal
    rhs_d = 0.0581657362048410_rfreal
    rhs_e =-0.0097478663508361_rfreal
    rhs_f = 0.0008982776047101_rfreal
    input%rhs_block1(input%iSecondDeriv,6,1:11) = &
            (/ rhs_f, rhs_e, rhs_d, rhs_c, rhs_b, &
               rhs_a,                             &
               rhs_b, rhs_c, rhs_d, rhs_e, rhs_f /)

    Call Mirror_block1_to_block2(input, input%iSecondDeriv)

    ! ... interior fourth derivative :: 4th order 13pt DRP (unconstrained L2 minimization)
    ! ... initialize
    input%iFourthDeriv = 4
    input%overlap(input%iFourthDeriv) = 6 ! ... (13 - 1) / 2
    input%operator_type(input%iFourthDeriv) = EXPLICIT
    input%max_operator_width(input%iFourthDeriv) = 13
    input%operator_bndry_width(input%iFourthDeriv) = 11 ! ... max size of boundary stencil
    input%operator_bndry_depth(input%iFourthDeriv) = 6 ! ... (13 - 1) / 2
    input%rhs_interior_stencil_start(input%iFourthDeriv) = -6
    input%rhs_interior_stencil_end(input%iFourthDeriv)   =  6

    if (input%iFourthDeriv /= input%ArtPropDrvOrder) then
      call graceful_exit(myrank, 'ERROR: input%iFourthDeriv /= input%ArtPropDrvOrder')
    end if

    rhs_a = 15.53434242252894_rfreal
    rhs_b =-11.70502896612137_rfreal
    rhs_c =  5.06622328028830_rfreal
    rhs_d = -1.41994874382408_rfreal
    rhs_e =  0.34825585767883_rfreal
    rhs_f = -0.06252689371137_rfreal
    rhs_g =  0.00585425442522_rfreal
    input%rhs_interior(input%iFourthDeriv,-6:6) = &
                            (/ rhs_g, rhs_f, rhs_e, rhs_d, rhs_c, rhs_b, &
                               rhs_a,                                    &
                               rhs_b, rhs_c, rhs_d, rhs_e, rhs_f, rhs_g /)
    ! ... (i = 1) on boundary fourth derivative :: 2nd order one-sided (Kawai & Lele, JCP 2008)
    input%rhs_block1(input%iFourthDeriv,1,1:6) = (/ 3.0_rfreal,-14.0_rfreal, 26.0_rfreal,-24.0_rfreal, 11.0_rfreal, -2.0_rfreal /)

    ! ... (i = 2) on boundary fourth derivative :: 2nd order one-sided (Kawai & Lele, JCP 2008)
    input%rhs_block1(input%iFourthDeriv,2,1:6) = (/ 2.0_rfreal, -9.0_rfreal, 16.0_rfreal,-14.0_rfreal, 6.0_rfreal, -1.0_rfreal /)

    ! ... (i = 3) on boundary fourth derivative :: 2nd order non-optimized centered
    rhs_a = 6.0_rfreal
    rhs_b =-4.0_rfreal
    rhs_c = 1.0_rfreal
    input%rhs_block1(input%iFourthDeriv,3,1:5) = &
                                (/ rhs_c, rhs_b, &
                                   rhs_a,        &
                                   rhs_b, rhs_c /)

    ! ... (i = 4) on boundary fourth derivative :: 4th order non-optimized centered
    rhs_a =28.0_rfreal / 3.0_rfreal
    rhs_b =-6.5_rfreal
    rhs_c = 2.0_rfreal
    rhs_d =-1.0_rfreal / 6.0_rfreal
    input%rhs_block1(input%iFourthDeriv,4,1:7) = &
                         (/ rhs_d, rhs_c, rhs_b, &
                            rhs_a,               &
                            rhs_b, rhs_c, rhs_d /)

    ! ... (i = 5) on boundary fourth derivative :: 4th order 9pt DRP (unconstrained L2 minimization)
    rhs_a =12.43257420895253_rfreal
    rhs_b =-8.97939270049536_rfreal
    rhs_c = 3.23969635024767_rfreal
    rhs_d =-0.52086562388028_rfreal
    rhs_e = 0.04427486965170_rfreal
    input%rhs_block1(input%iFourthDeriv,5,1:9) = &
                  (/ rhs_e, rhs_d, rhs_c, rhs_b, &
                     rhs_a,                      &
                     rhs_b, rhs_c, rhs_d, rhs_e /)

    ! ... (i = 6) on boundary fourth derivative :: 4th order 11pt DRP (unconstrained L2 minimization)
    rhs_a = 13.97615640205749_rfreal
    rhs_b =-10.31623938400544_rfreal
    rhs_c =  4.09022849383507_rfreal
    rhs_d = -0.89395252335694_rfreal
    rhs_e =  0.14402579928738_rfreal
    rhs_f = -0.01214058678882_rfreal
    input%rhs_block1(input%iFourthDeriv,6,1:11) = &
            (/ rhs_f, rhs_e, rhs_d, rhs_c, rhs_b, &
               rhs_a,                             &
               rhs_b, rhs_c, rhs_d, rhs_e, rhs_f /)

    Call Mirror_block1_to_block2(input, input%iFourthDeriv)

    Return

  end subroutine initialize_deriv_stencils_DRP_11pt_13pt

  subroutine initialize_filter_stencils_DRP_7pt_11pt_13pt(input)

    USE ModGlobal
    USE ModDataStruct

    type(t_mixt_input), pointer :: input

    ! ... local variables
    real(rfreal) :: sigma_d
    integer :: i

    !----------------------------------------------------------------------
    ! filter stencils
    !----------------------------------------------------------------------
    ! ... initialize
    input%overlap(input%iFilter) = 6 ! ... (13 - 1) / 2
    input%operator_type(input%iFilter) = EXPLICIT
    input%max_operator_width(input%iFilter) = 13
    input%operator_bndry_width(input%iFilter) = 11 ! ... max size of boundary stencil
    input%operator_bndry_depth(input%iFilter) = 6 ! ... (13 - 1) / 2
    input%rhs_interior_stencil_start(input%iFilter) = -6
    input%rhs_interior_stencil_end(input%iFilter)   =  6

    ! ... interior selective filter :: 4th order 13pt DRP filter, SFo13p (Bogey & Bailly, JCP 2004)
    !input%rhs_interior(input%iFilter,0:6) = (/ 0.190899511506_dp, &
    !                                          -0.171503832236_dp, &
    !                                           0.123632891797_dp, &
    !                                          -0.069975429105_dp, &
    !                                           0.029662754736_dp, &
    !                                          -0.008520738659_dp, &
    !                                           0.001254597714_dp /)
    ! ... interior selective filter :: 4th order 13pt DRP (unconstrained L2 minimization)
    input%rhs_interior(input%iFilter,0:6) = (/ 0.19139637117780534_dp, &
                                              -0.17191053896767167_dp, &
                                               0.12373966793390559_dp, &
                                              -0.06976712554095532_dp, &
                                               0.02936614443969654_dp, &
                                              -0.00832233549137301_dp, &
                                               0.00119600203749521_dp /)
    input%rhs_interior(input%iFilter,-1:-6:-1) = input%rhs_interior(input%iFilter,1:6)

    sigma_d = input%filter_fac
    input%rhs_interior(input%iFilter,-6:6) = - sigma_d * input%rhs_interior(input%iFilter,-6:6)
    input%rhs_interior(input%iFilter, 0:0) = 1.0_dp + input%rhs_interior(input%iFilter, 0:0)

    ! ... zero out the boundary filter
    do i = 1, input%operator_bndry_depth(input%iFilter)
      input%rhs_block1(input%iFilter,i,:) = 0.0_dp
    end do

    ! ... (i = 1) on boundary filter :: no filtering
    input%rhs_block1(input%iFilter,1,1) = 1.0_dp

    ! ... (i = 2) 7-pt selective filter :: 2nd order 7pt DRP filter, SF15 (Berland et al., JCP 2007)
    input%rhs_block1(input%iFilter,2,1:7) = (/ -0.085777408970_dp, 0.277628171524_dp, -0.356848072173_dp, 0.223119093072_dp, -0.057347064865_dp, -0.000747264596_dp, -0.000027453992_dp /)

    ! ... (i = 3) 11-pt selective filter :: 2nd order 11pt DRP filter, SF28 (Berland et al., JCP 2007)
    input%rhs_block1(input%iFilter,3,1:11) = (/ 0.052523901012_dp, &
     -0.206299133811_dp, 0.353527998250_dp, -0.348142394842_dp, 0.181481803619_dp, 0.009440804370_dp, -0.077675100452_dp, 0.044887364863_dp, -0.009971961849_dp, 0.000113359420_dp, 0.000113359420_dp /)

    ! ... (i = 4) 11-pt selective filter :: 2nd order 11pt DRP filter, SF37 (Berland et al., JCP 2007)
    input%rhs_block1(input%iFilter,4,1:11) = (/ -0.000054596010_dp, &
     0.042124772446_dp, -0.173103107841_dp, 0.299615871352_dp, -0.276543612935_dp, 0.131223506571_dp, -0.023424966418_dp, 0.013937561779_dp, -0.024565095706_dp, 0.013098287852_dp, -0.002308621090_dp /)

    ! ... (i = 5) 11-pt selective filter :: 2nd order 11pt DRP filter, SF46 (Berland et al., JCP 2007)
    input%rhs_block1(input%iFilter,5,1:11) = (/ 0.008391235145_dp, & 
     -0.047402506444_dp, 0.121438547725_dp, -0.200063042812_dp, 0.240069047836_dp, -0.207269200140_dp, 0.122263107844_dp, -0.047121062819_dp, 0.009014891495_dp, 0.001855812216_dp, -0.001176830044_dp /)

    sigma_d = input%filter_fac
    input%rhs_block1(input%iFilter,2:5,:) = - sigma_d * input%rhs_block1(input%iFilter,2:5,:)

    ! ... (i = 6) 11-pt selective filter :: 2nd order 11pt DRP filter, SFo11p (Bogey & Bailly, JCP 2004)
    input%rhs_block1(input%iFilter,6,6:11) = (/ 0.215044884112_dp, -0.187772883589_dp, 0.123755948787_dp, -0.059227575576_dp, 0.018721609157_dp, -0.002999540835_dp /)
    input%rhs_block1(input%iFilter,6,5:1:-1) = input%rhs_block1(input%iFilter,6,7:11)

    sigma_d = input%filter_fac
    input%rhs_block1(input%iFilter,6,:) = - sigma_d * input%rhs_block1(input%iFilter,6,:)

    do i = 2, 6
      input%rhs_block1(input%iFilter,i,i) = 1.0_dp + input%rhs_block1(input%iFilter,i,i)
    end do

    Call Mirror_block1_to_block2(input, input%iFilter)

  end subroutine initialize_filter_stencils_DRP_7pt_11pt_13pt

  subroutine initialize_filter_stencils_standard_explicit_11pt_centered(input)

    USE ModGlobal
    USE ModDataStruct

    type(t_mixt_input), pointer :: input

    ! ... local variables
    real(rfreal) :: lhs_alpha, rhs_a, rhs_b, rhs_c, rhs_d, rhs_e, rhs_f
    integer :: i

    !----------------------------------------------------------------------
    ! filter stencils
    !----------------------------------------------------------------------

    ! ... This standard filter is a variant of implicit centered Pade filter by 
    ! ... setting alpha_f = 0.0 following the formulation of Gaitonde & Visbal 
    ! ... (AFRL-TR-98-XXXX).  This filter is useful since it assumes an explicit 
    ! ... operator form (faster since no matrix inversion and easier for domain-
    ! ... decomposition).  However it's a very strong filter and small-scales 
    ! ... are mostly suppressed.  Therefore, this filter should only be used for 
    ! ... initial transient runs and is not suitable for time-accurate turbulence 
    ! ... simulation.
    ! ... JKim 01/2010

    ! ... initialize
    input%overlap(input%iFilter) = 5 ! ... (11 - 1) / 2
    input%operator_type(input%iFilter) = EXPLICIT
    input%max_operator_width(input%iFilter) = 11
    input%operator_bndry_depth(input%iFilter) = 5
    input%operator_bndry_width(input%iFilter) = 9 ! ... max size of boundary stencil
    input%rhs_interior_stencil_start(input%iFilter) = -5
    input%rhs_interior_stencil_end(input%iFilter)   =  5

    ! ... filter free parameter
    lhs_alpha = 0.0_rfreal

    ! ... interior filter
    rhs_a = (193.0_rfreal + 126.0_rfreal * lhs_alpha) / 256.0_rfreal
    rhs_b = (105.0_rfreal + 302.0_rfreal * lhs_alpha) / 256.0_rfreal
    rhs_c = 15.0_rfreal * (-1.0_rfreal + 2.0_rfreal * lhs_alpha) / 64.0_rfreal
    rhs_d = 45.0_rfreal * (1.0_rfreal - 2.0_rfreal * lhs_alpha) / 512.0_rfreal
    rhs_e = 5.0_rfreal * (-1.0_rfreal + 2.0_rfreal * lhs_alpha) / 256.0_rfreal
    rhs_f = (1.0_rfreal - 2.0_rfreal * lhs_alpha) / 512.0_rfreal
    input%rhs_interior(input%iFilter,-5:5) = (/ rhs_f, rhs_e, rhs_d, rhs_c, rhs_b, 2.0_rfreal * rhs_a, &
                                                rhs_b, rhs_c, rhs_d, rhs_e, rhs_f /) / 2.0_rfreal

    ! ... zero out the boundary filter
    do i = 1, input%operator_bndry_depth(input%iFilter)
      input%rhs_block1(input%iFilter,i,:) = 0.0_dp
    end do

    ! ... As an alternative approach, a working boundary filter closure of Pade 
    ! ... type filter is invoked.  Filter size is reduced by 2 approaching boundary 
    ! ... and to remain as centered, the first and the last points are not filtered.  
    ! ... Also the free parameter alpha_f = 0.0 so that we can incorporate them 
    ! ... into the explicit filter framework.
    ! ... JKim 10/2009
    lhs_alpha = 0.0_rfreal

    ! ... (i = 1) on boundary filter :: no filtering
    input%rhs_block1(input%iFilter,1,1) = 1.0_rfreal

    ! ... (i = 2) boundary filter
    rhs_a = (1.0_rfreal + 2.0_rfreal * lhs_alpha) / 2.0_rfreal
    rhs_b = (1.0_rfreal + 2.0_rfreal * lhs_alpha) / 2.0_rfreal
    input%rhs_block1(input%iFilter,2,1:3) = (/ rhs_b, 2.0_rfreal * rhs_a, rhs_b /) / 2.0_rfreal

    ! ... (i = 3) boundary filter
    rhs_a = 0.625_rfreal + 0.75_rfreal * lhs_alpha
    rhs_b = 0.5_rfreal + lhs_alpha
    rhs_c = -0.125_rfreal + 0.25_rfreal * lhs_alpha
    input%rhs_block1(input%iFilter,3,1:5) = (/ rhs_c, rhs_b, 2.0_rfreal * rhs_a, rhs_b, rhs_c /) / 2.0_rfreal

    ! ... (i = 4) boundary filter
    rhs_a = 11.0_rfreal / 16.0_rfreal + 0.625_rfreal * lhs_alpha
    rhs_b = 15.0_rfreal / 32.0_rfreal + 17.0_rfreal / 16.0_rfreal * lhs_alpha
    rhs_c = -3.0_rfreal / 16.0_rfreal + 0.375_rfreal * lhs_alpha
    rhs_d = 1.0_rfreal / 32.0_rfreal - lhs_alpha / 16.0_rfreal
    input%rhs_block1(input%iFilter,4,1:7) = (/ rhs_d, rhs_c, rhs_b, 2.0_rfreal * rhs_a, rhs_b, rhs_c, rhs_d /) / 2.0_rfreal

    ! ... (i = 5) boundary filter
    rhs_a = (93.0_rfreal + 70.0_rfreal * lhs_alpha ) / 128.0_rfreal
    rhs_b = (7.0_rfreal + 18.0_rfreal * lhs_alpha) / 16.0_rfreal
    rhs_c = (-7.0_rfreal + 14.0_rfreal * lhs_alpha) / 32.0_rfreal
    rhs_d = 1.0_rfreal / 16.0_rfreal - lhs_alpha / 8.0_rfreal
    rhs_e = -1.0_rfreal / 128.0_rfreal + lhs_alpha / 64.0_rfreal
    input%rhs_block1(input%iFilter,5,1:9) = (/ rhs_e, rhs_d, rhs_c, rhs_b, 2.0_rfreal * rhs_a, rhs_b, rhs_c, rhs_d, rhs_e /) / 2.0_rfreal

!!$ ! ... (i = 6) boundary filter
!!$ rhs_a = (193.0_rfreal + 126.0_rfreal * lhs_alpha) / 256.0_rfreal
!!$ rhs_b = (105.0_rfreal + 302.0_rfreal * lhs_alpha) / 256.0_rfreal
!!$ rhs_c = 15.0_rfreal * (-1.0_rfreal + 2.0_rfreal * lhs_alpha) / 64.0_rfreal
!!$ rhs_d = 45.0_rfreal * (1.0_rfreal - 2.0_rfreal * lhs_alpha) / 512.0_rfreal
!!$ rhs_e = 5.0_rfreal * (-1.0_rfreal + 2.0_rfreal * lhs_alpha) / 256.0_rfreal
!!$ rhs_f = (1.0_rfreal - 2.0_rfreal * lhs_alpha) / 512.0_rfreal
!!$ input%rhs_block1(input%iFilter,6,1:11) = (/ rhs_f, rhs_e, rhs_d, rhs_c, rhs_b, 2.0_rfreal * rhs_a, rhs_b, rhs_c, rhs_d, rhs_e, rhs_f /) / 2.0_rfreal

    Call Mirror_block1_to_block2(input, input%iFilter)

  end subroutine initialize_filter_stencils_standard_explicit_11pt_centered

  subroutine initialize_filter_stencils_DRP_13pt_centered(input)

    USE ModGlobal
    USE ModDataStruct

    type(t_mixt_input), pointer :: input

    ! ... local variables
    real(rfreal) :: sigma_d
    real(rfreal) :: lhs_alpha, rhs_a, rhs_b, rhs_c, rhs_d, rhs_e, rhs_f
    integer :: i

    !----------------------------------------------------------------------
    ! filter stencils
    !----------------------------------------------------------------------
    ! ... initialize
    input%overlap(input%iFilter) = 6 ! ... (13 - 1) / 2
    input%operator_type(input%iFilter) = EXPLICIT
    input%max_operator_width(input%iFilter) = 13
    input%operator_bndry_width(input%iFilter) = 11 ! ... max size of boundary stencil
    input%operator_bndry_depth(input%iFilter) = 6 ! ... (13 - 1) / 2
    input%rhs_interior_stencil_start(input%iFilter) = -6
    input%rhs_interior_stencil_end(input%iFilter)   =  6

!!$ ! ... interior selective filter :: 4th order 13pt DRP (unconstrained L2 minimization)
!!$ input%rhs_interior(input%iFilter,0:6) = (/ 0.19139637117780534_dp, &
!!$                                           -0.17191053896767167_dp, &
!!$                                            0.12373966793390559_dp, &
!!$                                           -0.06976712554095532_dp, &
!!$                                            0.02936614443969654_dp, &
!!$                                           -0.00832233549137301_dp, &
!!$                                            0.00119600203749521_dp /)
!!$ input%rhs_interior(input%iFilter,-1:-6:-1) = input%rhs_interior(input%iFilter,1:6)

    ! ... interior selective filter :: 4th order 13pt DRP (unconstrained L1 minimization)
    input%rhs_interior(input%iFilter,0:6) = (/ 0.190899511506_dp, &
                                              -0.171503832236_dp, &
                                               0.123632891797_dp, &
                                              -0.069975429105_dp, &
                                               0.029662754736_dp, &
                                              -0.008520738659_dp, &
                                               0.001254597714_dp /)
    input%rhs_interior(input%iFilter,-1:-6:-1) = input%rhs_interior(input%iFilter,1:6)

    sigma_d = input%filter_fac
    input%rhs_interior(input%iFilter,-6:6) = - sigma_d * input%rhs_interior(input%iFilter,-6:6)
    input%rhs_interior(input%iFilter, 0:0) = 1.0_dp + input%rhs_interior(input%iFilter, 0:0)

    ! ... zero out the boundary filter
    do i = 1, input%operator_bndry_depth(input%iFilter)
      input%rhs_block1(input%iFilter,i,:) = 0.0_dp
    end do

    ! ... Boundary filter closure proposed by Berland et al. (JCP 2007), or more 
    ! ... conventional approach using reduced size filter stencils all fails 
    ! ... when DRP finite difference, DRP selective filter, and characteristic 
    ! ... BC are used together; instability growing i = 3 and N-2 is responsible 
    ! ... for this crash but the error source is not clearly identified.

!!$ ! ... (i = 1) 4-pt selective filter :: 2nd order 4pt DRP filter, SF03 (Berland et al., JCP 2007)
!!$ input%rhs_block1(input%iFilter,1,1:4) = (/ 0.320882352941_dp, -0.465_dp, 0.179117647059_dp, -0.035_dp /)

!!$ ! ... (i = 2) 7-pt selective filter :: 2nd order 7pt DRP filter, SF15 (Berland et al., JCP 2007)
!!$ input%rhs_block1(input%iFilter,2,1:7) = (/ -0.085777408970_dp, 0.277628171524_dp, -0.356848072173_dp, 0.223119093072_dp, -0.057347064865_dp, -0.000747264596_dp, -0.000027453992_dp /)

!!$ ! ... (i = 3) 7-pt selective filter :: 2nd order 7pt DRP filter, SF24 (Berland et al., JCP 2007)
!!$ input%rhs_block1(input%iFilter,3,1:7) = (/ 0.032649010764_dp, -0.143339502575_dp, 0.273321177980_dp, -0.294622121167_dp, 0.186711738069_dp, -0.062038376258_dp, 0.007318073187_dp /)

!!$ ! ... (i = 3) 5-pt selective filter :: 2nd order 5pt DRP filter, SFo5p :: performs poorer than SF24
!!$ input%rhs_block1(input%iFilter,3,3:5) = (/ 0.3161605461361721_dp, &
!!$                                           -0.25_dp,               &
!!$                                            0.0919197269319139_dp /)
!!$ input%rhs_block1(input%iFilter,3,2:1:-1) = input%rhs_block1(input%iFilter,3,4:5)

!!$ ! ... (i = 4) 7-pt selective filter :: 4th order 7pt DRP filter, SFo7p
!!$ input%rhs_block1(input%iFilter,4,4:7) = (/ 0.2720433688290671_dp, &
!!$                                           -0.2242608422072667_dp, &
!!$                                            0.1139783155854664_dp, &
!!$                                           -0.0257391577927332_dp /)
!!$ input%rhs_block1(input%iFilter,4,3:1:-1) = input%rhs_block1(input%iFilter,4,5:7)

!!$ ! ... (i = 5) 9-pt selective filter :: 4th order 9pt DRP filter, SFo9p (Bogey & Bailly, JCP 2004)
!!$ input%rhs_block1(input%iFilter,5,5:9) = (/ 0.2349470378548639_dp, &
!!$                                           -0.2007289389016465_dp, &
!!$                                            0.1230212673645217_dp, &
!!$                                           -0.0492710610983534_dp, &
!!$                                            0.0095052137080462_dp /)
!!$ input%rhs_block1(input%iFilter,5,4:1:-1) = input%rhs_block1(input%iFilter,5,6:9)

!!$ ! ... (i = 6) 11-pt selective filter :: 2nd order 11pt DRP filter, SFo11p (Bogey & Bailly, JCP 2004)
!!$ input%rhs_block1(input%iFilter,6,6:11) = (/ 0.2072093253120036_dp, &
!!$                                            -0.1830026286009838_dp, &
!!$                                             0.1247305946647882_dp, &
!!$                                            -0.0631267032328720_dp, &
!!$                                             0.0216647426792099_dp, &
!!$                                            -0.0038706681661441_dp /)
!!$ input%rhs_block1(input%iFilter,6,5:1:-1) = input%rhs_block1(input%iFilter,6,7:11)

    ! ... (i = 5) 9-pt selective filter :: 4th order 9pt DRP filter, SFo9p (Bogey & Bailly, JCP 2004)
    input%rhs_block1(input%iFilter,5,5:9) = (/ 0.243527493120_dp, &
                                              -0.204788880640_dp, &
                                               0.120007591680_dp, &
                                              -0.045211119360_dp, &
                                               0.008228661760_dp /)
    input%rhs_block1(input%iFilter,5,4:1:-1) = input%rhs_block1(input%iFilter,5,6:9)

    ! ... (i = 6) 11-pt selective filter :: 2nd order 11pt DRP filter, SFo11p (Bogey & Bailly, JCP 2004)
    input%rhs_block1(input%iFilter,6,6:11) = (/ 0.215044884112_dp, &
                                               -0.187772883589_dp, &
                                                0.123755948787_dp, &
                                               -0.059227575576_dp, &
                                                0.018721609157_dp, &
                                               -0.002999540835_dp /)
    input%rhs_block1(input%iFilter,6,5:1:-1) = input%rhs_block1(input%iFilter,6,7:11)

    sigma_d = input%filter_fac
    do i = 5, 6
      input%rhs_block1(input%iFilter,i,:) = - sigma_d * input%rhs_block1(input%iFilter,i,:)
    end do
    do i = 5, 6
      input%rhs_block1(input%iFilter,i,i) = 1.0_dp + input%rhs_block1(input%iFilter,i,i)
    end do
!!$ Call Mirror_block1_to_block2(input, input%iFilter)

    ! ... As an alternative approach, a working boundary filter closure of Pade 
    ! ... type filter is invoked.  Filter size is reduced by 2 approaching boundary 
    ! ... and to remain as centered, the first and the last points are not filtered.  
    ! ... Also the free parameter alpha_f = 0.0 so that we can incorporate them 
    ! ... into the explicit filter framework.
    ! ... JKim 10/2009
    lhs_alpha = 0.0_rfreal

    ! ... (i = 1) on boundary filter :: no filtering
    input%rhs_block1(input%iFilter,1,1) = 1.0_rfreal

    ! ... (i = 2) boundary filter
    rhs_a = (1.0_rfreal + 2.0_rfreal * lhs_alpha) / 2.0_rfreal
    rhs_b = (1.0_rfreal + 2.0_rfreal * lhs_alpha) / 2.0_rfreal
    input%rhs_block1(input%iFilter,2,1:3) = (/ rhs_b, 2.0_rfreal * rhs_a, rhs_b /) / 2.0_rfreal

    ! ... (i = 3) boundary filter
    rhs_a = 0.625_rfreal + 0.75_rfreal * lhs_alpha
    rhs_b = 0.5_rfreal + lhs_alpha
    rhs_c = -0.125_rfreal + 0.25_rfreal * lhs_alpha
    input%rhs_block1(input%iFilter,3,1:5) = (/ rhs_c, rhs_b, 2.0_rfreal * rhs_a, rhs_b, rhs_c /) / 2.0_rfreal

    ! ... (i = 4) boundary filter
    rhs_a = 11.0_rfreal / 16.0_rfreal + 0.625_rfreal * lhs_alpha
    rhs_b = 15.0_rfreal / 32.0_rfreal + 17.0_rfreal / 16.0_rfreal * lhs_alpha
    rhs_c = -3.0_rfreal / 16.0_rfreal + 0.375_rfreal * lhs_alpha
    rhs_d = 1.0_rfreal / 32.0_rfreal - lhs_alpha / 16.0_rfreal
    input%rhs_block1(input%iFilter,4,1:7) = (/ rhs_d, rhs_c, rhs_b, 2.0_rfreal * rhs_a, rhs_b, rhs_c, rhs_d /) / 2.0_rfreal

!!$ ! ... (i = 5) boundary filter
!!$ rhs_a = (93.0_rfreal + 70.0_rfreal * lhs_alpha ) / 128.0_rfreal
!!$ rhs_b = (7.0_rfreal + 18.0_rfreal * lhs_alpha) / 16.0_rfreal
!!$ rhs_c = (-7.0_rfreal + 14.0_rfreal * lhs_alpha) / 32.0_rfreal
!!$ rhs_d = 1.0_rfreal / 16.0_rfreal - lhs_alpha / 8.0_rfreal
!!$ rhs_e = -1.0_rfreal / 128.0_rfreal + lhs_alpha / 64.0_rfreal
!!$ input%rhs_block1(input%iFilter,5,1:9) = (/ rhs_e, rhs_d, rhs_c, rhs_b, 2.0_rfreal * rhs_a, rhs_b, rhs_c, rhs_d, rhs_e /) / 2.0_rfreal

!!$ ! ... (i = 6) boundary filter
!!$ rhs_a = (193.0_rfreal + 126.0_rfreal * lhs_alpha) / 256.0_rfreal
!!$ rhs_b = (105.0_rfreal + 302.0_rfreal * lhs_alpha) / 256.0_rfreal
!!$ rhs_c = 15.0_rfreal * (-1.0_rfreal + 2.0_rfreal * lhs_alpha) / 64.0_rfreal
!!$ rhs_d = 45.0_rfreal * (1.0_rfreal - 2.0_rfreal * lhs_alpha) / 512.0_rfreal
!!$ rhs_e = 5.0_rfreal * (-1.0_rfreal + 2.0_rfreal * lhs_alpha) / 256.0_rfreal
!!$ rhs_f = (1.0_rfreal - 2.0_rfreal * lhs_alpha) / 512.0_rfreal
!!$ input%rhs_block1(input%iFilter,6,1:11) = (/ rhs_f, rhs_e, rhs_d, rhs_c, rhs_b, 2.0_rfreal * rhs_a, rhs_b, rhs_c, rhs_d, rhs_e, rhs_f /) / 2.0_rfreal

    Call Mirror_block1_to_block2(input, input%iFilter)

  end subroutine initialize_filter_stencils_DRP_13pt_centered

  subroutine initialize_filter_stencils_DRP_11pt_centered(input)

    USE ModGlobal
    USE ModDataStruct

    type(t_mixt_input), pointer :: input

    ! ... local variables
    real(rfreal) :: sigma_d
    real(rfreal) :: lhs_alpha, rhs_a, rhs_b, rhs_c, rhs_d, rhs_e, rhs_f
    integer :: i

    !----------------------------------------------------------------------
    ! filter stencils
    !----------------------------------------------------------------------
    ! ... initialize
    input%overlap(input%iFilter) = 5 ! ... (11 - 1) / 2
    input%operator_type(input%iFilter) = EXPLICIT
    input%max_operator_width(input%iFilter) = 11
    input%operator_bndry_width(input%iFilter) = 9 ! ... max size of boundary stencil
    input%operator_bndry_depth(input%iFilter) = 5 ! ... (11 - 1) / 2
    input%rhs_interior_stencil_start(input%iFilter) = -5
    input%rhs_interior_stencil_end(input%iFilter)   =  5

!!$ ! ... interior selective filter :: 2nd order 11pt DRP (unconstrained L2 minimization)
!!$ input%rhs_interior(input%iFilter,0:5) = (/ 0.2072093253120036_dp, &
!!$                                           -0.1830026286009838_dp, &
!!$                                            0.1247305946647882_dp, &
!!$                                           -0.0631267032328720_dp, &
!!$                                            0.0216647426792099_dp, &
!!$                                           -0.0038706681661441_dp/)
!!$ input%rhs_interior(input%iFilter,-1:-5:-1) = input%rhs_interior(input%iFilter,1:5)

    ! ... interior selective filter :: 2nd order 11pt DRP (unconstrained L1 minimization)
    input%rhs_interior(input%iFilter,0:5) = (/ 0.215044884112_dp, &
                                              -0.187772883589_dp, &
                                               0.123755948787_dp, &
                                              -0.059227575576_dp, &
                                               0.018721609157_dp, &
                                              -0.002999540835_dp/)
    input%rhs_interior(input%iFilter,-1:-5:-1) = input%rhs_interior(input%iFilter,1:5)

    sigma_d = input%filter_fac
    input%rhs_interior(input%iFilter,-5:5) = - sigma_d * input%rhs_interior(input%iFilter,-5:5)
    input%rhs_interior(input%iFilter, 0:0) = 1.0_dp + input%rhs_interior(input%iFilter, 0:0)

    ! ... zero out the boundary filter
    do i = 1, input%operator_bndry_depth(input%iFilter)
      input%rhs_block1(input%iFilter,i,:) = 0.0_dp
    end do

    ! ... Boundary filter closure proposed by Berland et al. (JCP 2007), or more 
    ! ... conventional approach using reduced size filter stencils all fails 
    ! ... when DRP finite difference, DRP selective filter, and characteristic 
    ! ... BC are used together; instability growing i = 3 and N-2 is responsible 
    ! ... for this crash but the error source is not clearly identified.

!!$ ! ... (i = 1) 4-pt selective filter :: 2nd order 4pt DRP filter, SF03 (Berland et al., JCP 2007)
!!$ input%rhs_block1(input%iFilter,1,1:4) = (/ 0.320882352941_dp, -0.465_dp, 0.179117647059_dp, -0.035_dp /)

!!$ ! ... (i = 2) 7-pt selective filter :: 2nd order 7pt DRP filter, SF15 (Berland et al., JCP 2007)
!!$ input%rhs_block1(input%iFilter,2,1:7) = (/ -0.085777408970_dp, 0.277628171524_dp, -0.356848072173_dp, 0.223119093072_dp, -0.057347064865_dp, -0.000747264596_dp, -0.000027453992_dp /)

!!$ ! ... (i = 3) 7-pt selective filter :: 2nd order 7pt DRP filter, SF24 (Berland et al., JCP 2007)
!!$ input%rhs_block1(input%iFilter,3,1:7) = (/ 0.032649010764_dp, -0.143339502575_dp, 0.273321177980_dp, -0.294622121167_dp, 0.186711738069_dp, -0.062038376258_dp, 0.007318073187_dp /)

!!$ ! ... (i = 3) 5-pt selective filter :: 2nd order 5pt DRP filter, SFo5p :: performs poorer than SF24
!!$ input%rhs_block1(input%iFilter,3,3:5) = (/ 0.3161605461361721_dp, &
!!$                                           -0.25_dp,               &
!!$                                            0.0919197269319139_dp /)
!!$ input%rhs_block1(input%iFilter,3,2:1:-1) = input%rhs_block1(input%iFilter,3,4:5)

!!$ ! ... (i = 4) 7-pt selective filter :: 4th order 7pt DRP filter, SFo7p
!!$ input%rhs_block1(input%iFilter,4,4:7) = (/ 0.2720433688290671_dp, &
!!$                                           -0.2242608422072667_dp, &
!!$                                            0.1139783155854664_dp, &
!!$                                           -0.0257391577927332_dp /)
!!$ input%rhs_block1(input%iFilter,4,3:1:-1) = input%rhs_block1(input%iFilter,4,5:7)

!!$ ! ... (i = 5) 9-pt selective filter :: 4th order 9pt DRP filter, SFo9p (Bogey & Bailly, JCP 2004)
!!$ input%rhs_block1(input%iFilter,5,5:9) = (/ 0.2349470378548639_dp, &
!!$                                           -0.2007289389016465_dp, &
!!$                                            0.1230212673645217_dp, &
!!$                                           -0.0492710610983534_dp, &
!!$                                            0.0095052137080462_dp /)
!!$ input%rhs_block1(input%iFilter,5,4:1:-1) = input%rhs_block1(input%iFilter,5,6:9)

!!$ ! ... (i = 6) 11-pt selective filter :: 2nd order 11pt DRP filter, SFo11p (Bogey & Bailly, JCP 2004)
!!$ input%rhs_block1(input%iFilter,6,6:11) = (/ 0.2072093253120036_dp, &
!!$                                            -0.1830026286009838_dp, &
!!$                                             0.1247305946647882_dp, &
!!$                                            -0.0631267032328720_dp, &
!!$                                             0.0216647426792099_dp, &
!!$                                            -0.0038706681661441_dp /)
!!$ input%rhs_block1(input%iFilter,6,5:1:-1) = input%rhs_block1(input%iFilter,6,7:11)

    ! ... (i = 5) 9-pt selective filter :: 4th order 9pt DRP filter, SFo9p (Bogey & Bailly, JCP 2004)
    input%rhs_block1(input%iFilter,5,5:9) = (/ 0.243527493120_dp, &
                                              -0.204788880640_dp, &
                                               0.120007591680_dp, &
                                              -0.045211119360_dp, &
                                               0.008228661760_dp /)
    input%rhs_block1(input%iFilter,5,4:1:-1) = input%rhs_block1(input%iFilter,5,6:9)

!!$ ! ... (i = 6) 11-pt selective filter :: 2nd order 11pt DRP filter, SFo11p (Bogey & Bailly, JCP 2004)
!!$ input%rhs_block1(input%iFilter,6,6:11) = (/ 0.215044884112_dp, &
!!$                                            -0.187772883589_dp, &
!!$                                             0.123755948787_dp, &
!!$                                            -0.059227575576_dp, &
!!$                                             0.018721609157_dp, &
!!$                                            -0.002999540835_dp /)
!!$ input%rhs_block1(input%iFilter,6,5:1:-1) = input%rhs_block1(input%iFilter,6,7:11)

    sigma_d = input%filter_fac
    do i = 5, 5
      input%rhs_block1(input%iFilter,i,:) = - sigma_d * input%rhs_block1(input%iFilter,i,:)
    end do
    do i = 5, 5
      input%rhs_block1(input%iFilter,i,i) = 1.0_dp + input%rhs_block1(input%iFilter,i,i)
    end do
!!$ Call Mirror_block1_to_block2(input, input%iFilter)

    ! ... As an alternative approach, a working boundary filter closure of Pade 
    ! ... type filter is invoked.  Filter size is reduced by 2 approaching boundary 
    ! ... and to remain as centered, the first and the last points are not filtered.  
    ! ... Also the free parameter alpha_f = 0.0 so that we can incorporate them 
    ! ... into the explicit filter framework.
    ! ... JKim 10/2009
    lhs_alpha = 0.0_rfreal

    ! ... (i = 1) on boundary filter :: no filtering
    input%rhs_block1(input%iFilter,1,1) = 1.0_rfreal

    ! ... (i = 2) boundary filter
    rhs_a = (1.0_rfreal + 2.0_rfreal * lhs_alpha) / 2.0_rfreal
    rhs_b = (1.0_rfreal + 2.0_rfreal * lhs_alpha) / 2.0_rfreal
    input%rhs_block1(input%iFilter,2,1:3) = (/ rhs_b, 2.0_rfreal * rhs_a, rhs_b /) / 2.0_rfreal

    ! ... (i = 3) boundary filter
    rhs_a = 0.625_rfreal + 0.75_rfreal * lhs_alpha
    rhs_b = 0.5_rfreal + lhs_alpha
    rhs_c = -0.125_rfreal + 0.25_rfreal * lhs_alpha
    input%rhs_block1(input%iFilter,3,1:5) = (/ rhs_c, rhs_b, 2.0_rfreal * rhs_a, rhs_b, rhs_c /) / 2.0_rfreal

    ! ... (i = 4) boundary filter
    rhs_a = 11.0_rfreal / 16.0_rfreal + 0.625_rfreal * lhs_alpha
    rhs_b = 15.0_rfreal / 32.0_rfreal + 17.0_rfreal / 16.0_rfreal * lhs_alpha
    rhs_c = -3.0_rfreal / 16.0_rfreal + 0.375_rfreal * lhs_alpha
    rhs_d = 1.0_rfreal / 32.0_rfreal - lhs_alpha / 16.0_rfreal
    input%rhs_block1(input%iFilter,4,1:7) = (/ rhs_d, rhs_c, rhs_b, 2.0_rfreal * rhs_a, rhs_b, rhs_c, rhs_d /) / 2.0_rfreal

!!$ ! ... (i = 5) boundary filter
!!$ rhs_a = (93.0_rfreal + 70.0_rfreal * lhs_alpha ) / 128.0_rfreal
!!$ rhs_b = (7.0_rfreal + 18.0_rfreal * lhs_alpha) / 16.0_rfreal
!!$ rhs_c = (-7.0_rfreal + 14.0_rfreal * lhs_alpha) / 32.0_rfreal
!!$ rhs_d = 1.0_rfreal / 16.0_rfreal - lhs_alpha / 8.0_rfreal
!!$ rhs_e = -1.0_rfreal / 128.0_rfreal + lhs_alpha / 64.0_rfreal
!!$ input%rhs_block1(input%iFilter,5,1:9) = (/ rhs_e, rhs_d, rhs_c, rhs_b, 2.0_rfreal * rhs_a, rhs_b, rhs_c, rhs_d, rhs_e /) / 2.0_rfreal

!!$ ! ... (i = 6) boundary filter
!!$ rhs_a = (193.0_rfreal + 126.0_rfreal * lhs_alpha) / 256.0_rfreal
!!$ rhs_b = (105.0_rfreal + 302.0_rfreal * lhs_alpha) / 256.0_rfreal
!!$ rhs_c = 15.0_rfreal * (-1.0_rfreal + 2.0_rfreal * lhs_alpha) / 64.0_rfreal
!!$ rhs_d = 45.0_rfreal * (1.0_rfreal - 2.0_rfreal * lhs_alpha) / 512.0_rfreal
!!$ rhs_e = 5.0_rfreal * (-1.0_rfreal + 2.0_rfreal * lhs_alpha) / 256.0_rfreal
!!$ rhs_f = (1.0_rfreal - 2.0_rfreal * lhs_alpha) / 512.0_rfreal
!!$ input%rhs_block1(input%iFilter,6,1:11) = (/ rhs_f, rhs_e, rhs_d, rhs_c, rhs_b, 2.0_rfreal * rhs_a, rhs_b, rhs_c, rhs_d, rhs_e, rhs_f /) / 2.0_rfreal

    Call Mirror_block1_to_block2(input, input%iFilter)

  end subroutine initialize_filter_stencils_DRP_11pt_centered

  subroutine initialize_filter_stencils_TruncatedGaussian(input)

    USE ModGlobal
    USE ModDataStruct

    type(t_mixt_input), pointer :: input

    ! ... local variables
    real(rfreal) :: sigma_d
    integer :: i

    !----------------------------------------------------------------------
    ! filter stencils
    !----------------------------------------------------------------------
    ! ... initialize
    input%overlap(input%iFilterShockTG) = 5
    input%operator_type(input%iFilterShockTG) = EXPLICIT
    input%max_operator_width(input%iFilterShockTG) = 11
    input%operator_bndry_width(input%iFilterShockTG) = 11
    input%operator_bndry_depth(input%iFilterShockTG) = 4
    input%rhs_interior_stencil_start(input%iFilterShockTG) = -4
    input%rhs_interior_stencil_end(input%iFilterShockTG)   =  4


    ! ... interior filter
    input%rhs_interior(input%iFilterShockTG,-4:4) = (/ 107.0_dp/103680.0_dp, 149.0_dp/12960.0_dp, 1997.0_dp/25920.0_dp, 3091.0_dp/12960.0_dp, 3565.0_dp/10368.0_dp, &
                                                     3091.0_dp/12960.0_dp, 1997.0_dp/25920.0_dp, 149.0_dp/12960.0_dp, 107.0_dp/103680.0_dp /)

    ! ... zero out the boundary filter
    do i = 1, input%operator_bndry_depth(input%iFilterShockTG)
      input%rhs_block1(input%iFilterShockTG,i,:) = 0.0_dp
    end do

    ! ... (i = 1) on boundary filter :: no filtering
    input%rhs_block1(input%iFilterShockTG,1,1) = 1.0_dp

    ! ... (i = 2) 7-pt selective filter
    input%rhs_block1(input%iFilterShockTG,2,1:7) = (/ 0.085777408970_dp, 1.0_dp - 0.277628171524_dp, 0.356848072173_dp, -0.223119093072_dp, 0.057347064865_dp, 0.000747264596_dp, 0.000027453993_dp /)

    ! ... (i = 3) 7-pt selective filter
    input%rhs_block1(input%iFilterShockTG,3,1:7) = (/ -0.032649010764_dp, 0.143339502575_dp, 1.0_dp - 0.273321177980_dp, 0.294622121167_dp, -0.186711738069_dp, 0.062038376258_dp, -0.007318073189_dp /)

    ! ... (i = 4) 11-pt selective filter
    input%rhs_block1(input%iFilterShockTG,4,1:11) = (/ 0.000054596010_dp, &
     -0.042124772446_dp, 0.173103107841_dp, 1.0_dp - 0.299615871352_dp, 0.276543612935_dp, -0.131223506571_dp, 0.023424966418_dp, -0.013937561779_dp, 0.024565095706_dp, -0.013098287852_dp, 0.002308621090_dp /)

    Call Mirror_block1_to_block2(input, input%iFilterShockTG)

  end subroutine initialize_filter_stencils_TruncatedGaussian

  subroutine initialize_adapative_shock_filter_stencils(input)

    USE ModGlobal
    USE ModDataStruct

    type(t_mixt_input), pointer :: input

    ! ... local variables
    real(rfreal) :: sigma_d
    integer :: i

    !----------------------------------------------------------------------
    ! Detection stencils
    !----------------------------------------------------------------------
    ! ... initialize
    input%overlap(input%iShockFilterDetect) = 1
    input%operator_type(input%iShockFilterDetect) = EXPLICIT
    input%max_operator_width(input%iShockFilterDetect) = 3
    input%operator_bndry_width(input%iShockFilterDetect) = 3
    input%operator_bndry_depth(input%iShockFilterDetect) = 1
    input%rhs_interior_stencil_start(input%iShockFilterDetect) = -1
    input%rhs_interior_stencil_end(input%iShockFilterDetect)   =  1


    ! ... Detection stencils : interior scheme
    input%rhs_interior(input%iShockFilterDetect,-1:1) = (/ -0.25_dp, 0.5_dp, -0.25_dp /)

    ! ... Detection stencils : zero out the boundary scheme
    do i = 1, input%operator_bndry_depth(input%iShockFilterDetect)
      input%rhs_block1(input%iShockFilterDetect,i,:) = 0.0_dp
    end do

    ! ... (i = 1) Detection stencils : on boundary scheme
    input%rhs_block1(input%iShockFilterDetect,1,1:3) = (/ -0.25_dp, 0.5_dp, -0.25_dp /)

    Call Mirror_block1_to_block2(input, input%iShockFilterDetect)


    !----------------------------------------------------------------------
    ! 2nd order optimized filter stencils
    ! right-looking operator
    !----------------------------------------------------------------------
    ! ... initialize
    input%overlap(input%iShockFilterFluxForward) = 2
    input%operator_type(input%iShockFilterFluxForward) = EXPLICIT
    input%max_operator_width(input%iShockFilterFluxForward) = 5
    input%operator_bndry_width(input%iShockFilterFluxForward) = 2
    input%operator_bndry_depth(input%iShockFilterFluxForward) = 2
    input%rhs_interior_stencil_start(input%iShockFilterFluxForward) = -2
    input%rhs_interior_stencil_end(input%iShockFilterFluxForward)   =  2


    ! ... right-looking filter stencils : interior scheme
    input%rhs_interior(input%iShockFilterFluxForward,-2:2) = (/ 0.0_dp, -0.039617_dp, 0.210383_dp, -0.210383_dp, 0.039617_dp /)

    ! ... right-looking filter stencils : zero out the boundary scheme
    do i = 1, input%operator_bndry_depth(input%iShockFilterFluxForward)
      input%rhs_block1(input%iShockFilterFluxForward,i,:) = 0.0_dp
    end do

    ! ... (i = 1) right-looking filter : on boundary scheme
    input%rhs_block1(input%iShockFilterFluxForward,1,1:2) = (/ 1.0_rfreal, 0.0_rfreal /)

    ! ... (i = 2) right-looking filter : on boundary scheme
    input%rhs_block1(input%iShockFilterFluxForward,2,1:2) = (/ 0.0_rfreal, 1.0_rfreal /)

    Call Mirror_block1_to_block2(input, input%iShockFilterFluxForward)


    !----------------------------------------------------------------------
    ! 2nd order optimized filter stencils
    ! left-looking operator
    !----------------------------------------------------------------------
    ! ... initialize
    input%overlap(input%iShockFilterFluxBackward) = 2
    input%operator_type(input%iShockFilterFluxBackward) = EXPLICIT
    input%max_operator_width(input%iShockFilterFluxBackward) = 5
    input%operator_bndry_width(input%iShockFilterFluxBackward) = 2
    input%operator_bndry_depth(input%iShockFilterFluxBackward) = 2
    input%rhs_interior_stencil_start(input%iShockFilterFluxBackward) = -2
    input%rhs_interior_stencil_end(input%iShockFilterFluxBackward)   =  2


    ! ... right-looking filter stencils : interior scheme
    input%rhs_interior(input%iShockFilterFluxBackward,-2:2) = (/ -0.039617_dp, 0.210383_dp, -0.210383_dp, 0.039617_dp, 0.0_dp /)

    ! ... right-looking filter stencils : zero out the boundary scheme
    do i = 1, input%operator_bndry_depth(input%iShockFilterFluxBackward)
      input%rhs_block1(input%iShockFilterFluxBackward,i,:) = 0.0_dp
    end do

    ! ... (i = 1) right-looking filter : on boundary scheme
    input%rhs_block1(input%iShockFilterFluxBackward,1,1:2) = (/ 1.0_rfreal, 0.0_rfreal /)

    ! ... (i = 2) right-looking filter : on boundary scheme
    input%rhs_block1(input%iShockFilterFluxBackward,2,1:2) = (/ 0.0_rfreal, 1.0_rfreal /)

    Call Mirror_block1_to_block2(input, input%iShockFilterFluxBackward)

    !----------------------------------------------------------------------
    ! 2nd order optimized filter stencils
    ! right-looking filter coefficient
    !----------------------------------------------------------------------
    ! ... initialize
    input%overlap(input%iShockFilterCoeffForward) = 1
    input%operator_type(input%iShockFilterCoeffForward) = EXPLICIT
    input%max_operator_width(input%iShockFilterCoeffForward) = 3
    input%operator_bndry_width(input%iShockFilterCoeffForward) = 2
    input%operator_bndry_depth(input%iShockFilterCoeffForward) = 1
    input%rhs_interior_stencil_start(input%iShockFilterCoeffForward) = -1
    input%rhs_interior_stencil_end(input%iShockFilterCoeffForward)   =  1


    ! ... right-looking filter stencils : interior scheme
    input%rhs_interior(input%iShockFilterCoeffForward,-1:1) = (/ 0.0_dp, 0.5_dp, 0.5_dp /)

    ! ... right-looking filter stencils : zero out the boundary scheme
    do i = 1, input%operator_bndry_depth(input%iShockFilterCoeffForward)
      input%rhs_block1(input%iShockFilterCoeffForward,i,:) = 0.0_dp
    end do

    ! ... (i = 1) right-looking filter : on boundary scheme
    input%rhs_block1(input%iShockFilterCoeffForward,1,1:2) = (/ 2.0_dp * TINY, 2.0_dp * TINY /)

    Call Mirror_block1_to_block2(input, input%iShockFilterCoeffForward)

    !----------------------------------------------------------------------
    ! 2nd order optimized filter stencils
    ! left-looking filter coefficient
    !----------------------------------------------------------------------
    ! ... initialize
    input%overlap(input%iShockFilterCoeffBackward) = 1
    input%operator_type(input%iShockFilterCoeffBackward) = EXPLICIT
    input%max_operator_width(input%iShockFilterCoeffBackward) = 3
    input%operator_bndry_width(input%iShockFilterCoeffBackward) = 2
    input%operator_bndry_depth(input%iShockFilterCoeffBackward) = 1
    input%rhs_interior_stencil_start(input%iShockFilterCoeffBackward) = -1
    input%rhs_interior_stencil_end(input%iShockFilterCoeffBackward)   =  1


    ! ... right-looking filter stencils : interior scheme
    input%rhs_interior(input%iShockFilterCoeffBackward,-1:1) = (/ 0.5_dp, 0.5_dp, 0.0_dp /)

    ! ... right-looking filter stencils : zero out the boundary scheme
    do i = 1, input%operator_bndry_depth(input%iShockFilterCoeffBackward)
      input%rhs_block1(input%iShockFilterCoeffBackward,i,:) = 0.0_dp
    end do

    ! ... (i = 1) right-looking filter : on boundary scheme
    input%rhs_block1(input%iShockFilterCoeffBackward,1,1:2) =  (/ 2.0_dp * TINY, 2.0_dp * TINY /)

    Call Mirror_block1_to_block2(input, input%iShockFilterCoeffBackward)

    !----------------------------------------------------------------------
    ! 2nd order optimized filter stencils
    ! right-looking shock sensor
    !----------------------------------------------------------------------
    ! ... initialize
    input%overlap(input%iShockFilterSensorForward) = 1
    input%operator_type(input%iShockFilterSensorForward) = EXPLICIT
    input%max_operator_width(input%iShockFilterSensorForward) = 3
    input%operator_bndry_width(input%iShockFilterSensorForward) = 2
    input%operator_bndry_depth(input%iShockFilterSensorForward) = 1
    input%rhs_interior_stencil_start(input%iShockFilterSensorForward) = -1
    input%rhs_interior_stencil_end(input%iShockFilterSensorForward)   =  1

    ! ... right-looking filter stencils : interior scheme
    input%rhs_interior(input%iShockFilterSensorForward,-1:1) = (/ 0.0_dp, 1.0_dp, -1.0_dp /)

    ! ... right-looking filter stencils : zero out the boundary scheme
    do i = 1, input%operator_bndry_depth(input%iShockFilterSensorForward)
      input%rhs_block1(input%iShockFilterSensorForward,i,:) = 0.0_dp
    end do

    ! ... (i = 1) right-looking filter : on boundary scheme
    input%rhs_block1(input%iShockFilterSensorForward,1,1:2) = (/ 1.0_dp, -1.0_dp /)

    Call Mirror_block1_to_block2(input, input%iShockFilterSensorForward)

    !----------------------------------------------------------------------
    ! 2nd order optimized filter stencils
    ! left-looking shock sensor
    !----------------------------------------------------------------------
    ! ... initialize
    input%overlap(input%iShockFilterSensorBackward) = 1
    input%operator_type(input%iShockFilterSensorBackward) = EXPLICIT
    input%max_operator_width(input%iShockFilterSensorBackward) = 3
    input%operator_bndry_width(input%iShockFilterSensorBackward) = 2
    input%operator_bndry_depth(input%iShockFilterSensorBackward) = 1
    input%rhs_interior_stencil_start(input%iShockFilterSensorBackward) = -1
    input%rhs_interior_stencil_end(input%iShockFilterSensorBackward)   =  1

    ! ... right-looking filter stencils : interior scheme
    input%rhs_interior(input%iShockFilterSensorBackward,-1:1) = (/ -1.0_dp, 1.0_dp, 0.0_dp /)

    ! ... right-looking filter stencils : zero out the boundary scheme
    do i = 1, input%operator_bndry_depth(input%iShockFilterSensorBackward)
      input%rhs_block1(input%iShockFilterSensorBackward,i,:) = 0.0_dp
    end do

    ! ... (i = 1) right-looking filter : on boundary scheme
    input%rhs_block1(input%iShockFilterSensorBackward,1,1:2) = (/ -1.0_dp, 1.0_dp /)

    Call Mirror_block1_to_block2(input, input%iShockFilterSensorBackward)


  end subroutine initialize_adapative_shock_filter_stencils

 subroutine initialize_deriv_stencils_forward_diff(input)

    USE ModGlobal
    USE ModDataStruct

    type(t_mixt_input), pointer :: input

    ! ... local variables
    real(rfreal) :: lhs_alpha, lhs_beta
    real(rfreal) :: rhs_a, rhs_b, rhs_c, rhs_d, rhs_e, rhs_f, rhs_g, rhs_h, rhs_i, rhs_j, rhs_k
    real(rfreal) :: rhs_l
    real(rfreal) :: r67, r68, r78
    integer :: i, opID

    opID = input%iForwardDiff

    !----------------------------------------------------------------------
    ! derivative stencils
    !----------------------------------------------------------------------
    ! ... initialize
    input%overlap(opID) = 1
    input%operator_type(opID) = EXPLICIT
    input%max_operator_width(opID) = 2
    input%operator_bndry_width(opID) = 2
    input%operator_bndry_depth(opID) = 1
    input%rhs_interior_stencil_start(opID) = 0
    input%rhs_interior_stencil_end(opID)   = 1

    ! ... interior first derivative :: 2nd
    input%rhs_interior(opID,0:1)  = (/ -1.0_rfreal, 1.0_rfreal /)

    ! ... (i = 1) on boundary first derivative
    input%rhs_block1(opID,1,1:2) = (/ -1.0_rfreal, 1.0_rfreal /)
    input%rhs_block2(opID,1,1:2) = (/ -1.0_rfreal, 1.0_rfreal /)

    Call Mirror_block1_to_block2(input, opID)

    Return

  end subroutine initialize_deriv_stencils_forward_diff

  subroutine initialize_lhs_filter_stencils_PADE(myrank, input)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    integer :: myrank
    type(t_mixt_input), pointer :: input

    ! ... local variables
    real(rfreal) :: lhs_alpha, lhs_beta
    real(rfreal) :: rhs_a, rhs_b, rhs_c, rhs_d, rhs_e, rhs_f, rhs_g, rhs_h, rhs_i, rhs_j, rhs_k
    real(rfreal) :: rhs_l
    integer :: i

    !----------------------------------------------------------------------
    ! filter stencils
    !----------------------------------------------------------------------
    ! ... initialize
    input%overlap(input%iLHS_Filter) = 5
    input%operator_type(input%iLHS_Filter) = IMPLICIT
    input%max_operator_width(input%iLHS_Filter) = 11
    input%operator_bndry_depth(input%iLHS_Filter) = 5
    input%operator_bndry_width(input%iLHS_Filter) = 11
    input%lhs_interior_stencil_start(input%iLHS_Filter) = -1
    input%lhs_interior_stencil_end(input%iLHS_Filter)   =  1
    input%rhs_interior_stencil_start(input%iLHS_Filter) = -5
    input%rhs_interior_stencil_end(input%iLHS_Filter)   =  5

    ! ... filter free parameter
    lhs_beta  = 0.0_rfreal
    lhs_alpha = input%lhs_filter_alphaf

    ! ... interior filter
    rhs_a = (193.0_rfreal + 126.0_rfreal * lhs_alpha) / 256.0_rfreal
    rhs_b = (105.0_rfreal + 302.0_rfreal * lhs_alpha) / 256.0_rfreal
    rhs_c = 15.0_rfreal * (-1.0_rfreal + 2.0_rfreal * lhs_alpha) / 64.0_rfreal
    rhs_d = 45.0_rfreal * (1.0_rfreal - 2.0_rfreal * lhs_alpha) / 512.0_rfreal
    rhs_e = 5.0_rfreal * (-1.0_rfreal + 2.0_rfreal * lhs_alpha) / 256.0_rfreal
    rhs_f = (1.0_rfreal - 2.0_rfreal * lhs_alpha) / 512.0_rfreal
    input%lhs_interior(input%iLHS_Filter,-2:2) = (/ lhs_beta, lhs_alpha, 1.0_rfreal, lhs_alpha, lhs_beta /)
    input%rhs_interior(input%iLHS_Filter,-5:5) = (/ rhs_f, rhs_e, rhs_d, rhs_c, rhs_b, 2.0_rfreal * rhs_a, &
         rhs_b, rhs_c, rhs_d, rhs_e, rhs_f /) / 2.0_rfreal

    ! ... filter free parameter
    lhs_alpha = input%lhs_filter_alphaf

    ! ... (i = 1) on boundary filter :: no filtering
    input%lhs_block1(input%iLHS_Filter,1,0) = 1.0_rfreal
    input%rhs_block1(input%iLHS_Filter,1,1) = 1.0_rfreal

    ! ... (i = 2) boundary filter :: from Visbal report
    rhs_a = (1.0_rfreal + 1022.0_rfreal * lhs_alpha) / 1024.0_rfreal
    rhs_b = (507.0_rfreal + 10.0_rfreal * lhs_alpha) / 512.0_rfreal
    rhs_c = (45.0_rfreal + 934.0_rfreal * lhs_alpha) / 1024.0_rfreal
    rhs_d = 15.0_rfreal * (-1.0_rfreal + 2.0_rfreal * lhs_alpha) / 128.0_rfreal
    rhs_e = 105.0_rfreal * (1.0_rfreal - 2.0_rfreal * lhs_alpha) / 512.0_rfreal
    rhs_f = 63.0_rfreal * (-1.0_rfreal + 2.0_rfreal * lhs_alpha) / 256.0_rfreal
    rhs_g = 105.0_rfreal * (1.0_rfreal - 2.0_rfreal * lhs_alpha) / 512.0_rfreal
    rhs_h = 15.0_rfreal * (-1.0_rfreal + 2.0_rfreal * lhs_alpha) / 128.0_rfreal
    rhs_i = 45.0_rfreal * (1.0_rfreal - 2.0_rfreal * lhs_alpha) / 1024.0_rfreal
    rhs_j = 5.0_rfreal * (-1.0_rfreal + 2.0_rfreal * lhs_alpha) / 512.0_rfreal
    rhs_k = (1.0_rfreal - 2.0_rfreal * lhs_alpha) / 1024.0_rfreal
    input%lhs_block1(input%iLHS_Filter,2,-1:1) = (/ lhs_alpha, 1.0_rfreal, lhs_alpha /)
    input%rhs_block1(input%iLHS_Filter,2,1:11) = (/ rhs_a, rhs_b, rhs_c, rhs_d, rhs_e, rhs_f, rhs_g, rhs_h, rhs_i, rhs_j, rhs_k /)

    ! ... (i = 3) boundary filter :: from Visbal report
    rhs_a = (-1.0_rfreal + 2.0_rfreal * lhs_alpha) / 1024.0_rfreal
    rhs_b = (5.0_rfreal + 502.0_rfreal * lhs_alpha) / 512.0_rfreal
    rhs_c = (979.0_rfreal + 90.0_rfreal * lhs_alpha) / 1024.0_rfreal
    rhs_d = (15.0_rfreal + 98.0_rfreal * lhs_alpha) / 128.0_rfreal
    rhs_e = 105.0_rfreal * (-1.0_rfreal + 2.0_rfreal * lhs_alpha) / 512.0_rfreal
    rhs_f = 63.0_rfreal * (1.0_rfreal - 2.0_rfreal * lhs_alpha) / 256.0_rfreal
    rhs_g = 105.0_rfreal * (-1.0_rfreal + 2.0_rfreal * lhs_alpha) / 512.0_rfreal
    rhs_h = 15.0_rfreal * (1.0_rfreal - 2.0_rfreal * lhs_alpha) / 128.0_rfreal
    rhs_i = 45.0_rfreal * (-1.0_rfreal + 2.0_rfreal * lhs_alpha) / 1024.0_rfreal
    rhs_j = 5.0_rfreal * (1.0_rfreal - 2.0_rfreal * lhs_alpha) / 512.0_rfreal
    rhs_k = (-1.0_rfreal + 2.0_rfreal * lhs_alpha) / 1024.0_rfreal
    input%lhs_block1(input%iLHS_Filter,3,-1:1) = (/ lhs_alpha, 1.0_rfreal, lhs_alpha /)
    input%rhs_block1(input%iLHS_Filter,3,1:11) = (/ rhs_a, rhs_b, rhs_c, rhs_d, rhs_e, rhs_f, rhs_g, rhs_h, rhs_i, rhs_j, rhs_k /)

    ! ... (i = 4) boundary filter :: from Visbal report
    rhs_a = (1.0_rfreal - 2.0_rfreal * lhs_alpha) / 1024.0_rfreal
    rhs_b = 5.0_rfreal * (-1.0_rfreal + 2.0_rfreal * lhs_alpha) / 512.0_rfreal
    rhs_c = (45.0_rfreal + 934.0_rfreal * lhs_alpha) / 1024.0_rfreal
    rhs_d = (113.0_rfreal + 30.0_rfreal * lhs_alpha) / 128.0_rfreal
    rhs_e = (105.0_rfreal + 302.0_rfreal * lhs_alpha) / 512.0_rfreal
    rhs_f = 63.0_rfreal * (-1.0_rfreal + 2.0_rfreal * lhs_alpha) / 256.0_rfreal
    rhs_g = 105.0_rfreal * (1.0_rfreal - 2.0_rfreal * lhs_alpha) / 512.0_rfreal
    rhs_h = 15.0_rfreal * (-1.0_rfreal + 2.0_rfreal * lhs_alpha) / 128.0_rfreal
    rhs_i = 45.0_rfreal * (1.0_rfreal - 2.0_rfreal * lhs_alpha) / 1024.0_rfreal
    rhs_j = 5.0_rfreal * (-1.0_rfreal + 2.0_rfreal * lhs_alpha) / 512.0_rfreal
    rhs_k = (1.0_rfreal - 2.0_rfreal * lhs_alpha) / 1024.0_rfreal
    input%lhs_block1(input%iLHS_Filter,4,-1:1) = (/ lhs_alpha, 1.0_rfreal, lhs_alpha /)
    input%rhs_block1(input%iLHS_Filter,4,1:11) = (/ rhs_a, rhs_b, rhs_c, rhs_d, rhs_e, rhs_f, rhs_g, rhs_h, rhs_i, rhs_j, rhs_k /)

    ! ... (i = 5) boundary filter :: from Visbal report
    rhs_a = (-1.0_rfreal + 2.0_rfreal * lhs_alpha) / 1024.0_rfreal
    rhs_b = 5.0_rfreal * (1.0_rfreal - 2.0_rfreal * lhs_alpha) / 512.0_rfreal
    rhs_c = 45.0_rfreal * (-1.0_rfreal + 2.0_rfreal * lhs_alpha) / 1024.0_rfreal
    rhs_d = (15.0_rfreal + 98.0_rfreal * lhs_alpha) / 128.0_rfreal
    rhs_e = (407.0_rfreal + 210.0_rfreal * lhs_alpha) / 512.0_rfreal
    rhs_f = (63.0_rfreal + 130.0_rfreal * lhs_alpha) / 256.0_rfreal
    rhs_g = 105.0_rfreal * (-1.0_rfreal + 2.0_rfreal * lhs_alpha) / 512.0_rfreal
    rhs_h = 15.0_rfreal * (1.0_rfreal - 2.0_rfreal * lhs_alpha) / 128.0_rfreal
    rhs_i = 45.0_rfreal * (-1.0_rfreal + 2.0_rfreal * lhs_alpha) / 1024.0_rfreal
    rhs_j = 5.0_rfreal * (1.0_rfreal - 2.0_rfreal * lhs_alpha) / 512.0_rfreal
    rhs_k = (-1.0_rfreal + 2.0_rfreal * lhs_alpha) / 1024.0_rfreal
    input%lhs_block1(input%iLHS_Filter,5,-1:1) = (/ lhs_alpha, 1.0_rfreal, lhs_alpha /)
    input%rhs_block1(input%iLHS_Filter,5,1:11) = (/ rhs_a, rhs_b, rhs_c, rhs_d, rhs_e, rhs_f, rhs_g, rhs_h, rhs_i, rhs_j, rhs_k /)

    do i = 1, input%operator_bndry_depth(input%iLHS_Filter)
      input%lhs_block2(input%iLHS_Filter,i,-2:2) = input%lhs_block1(input%iLHS_Filter,input%operator_bndry_depth(input%iLHS_Filter)-(i-1),2:-2:-1)
    end do
    Call Mirror_block1_to_block2(input, input%iLHS_Filter)

    ! ... fill mapping array; must be consistent with stencil definition in hypre.c
    if (associated(input%HYPREStencilIndex) .eqv. .false.) then
      allocate(input%HYPREStencilIndex(MAX_OPERATORS,-2:2,-2:2,-2:2))
      input%HYPREStencilIndex = 0
    end if

    if (lhs_beta /= 0.0_rfreal) then
      call graceful_exit(myrank, 'ERROR: lhs_beta /= 0 in initialize_filter_stencils_PADE.')
    else 
      input%HYPREStencilIndex(input%iLHS_Filter, :, :, :) = 0
      input%HYPREStencilIndex(input%iLHS_Filter, 0, 0, 0) = 0   ! ... self point
      input%HYPREStencilIndex(input%iLHS_Filter,-1, 0, 0) = 1   ! ... i = -1
      input%HYPREStencilIndex(input%iLHS_Filter,+1, 0, 0) = 2   ! ... i = +1
      input%HYPREStencilIndex(input%iLHS_Filter, 0,-1, 0) = 3   ! ... j = -1
      input%HYPREStencilIndex(input%iLHS_Filter, 0,+1, 0) = 4   ! ... j = +1
      input%HYPREStencilIndex(input%iLHS_Filter, 0, 0,-1) = 5   ! ... k = -1
      input%HYPREStencilIndex(input%iLHS_Filter, 0, 0,+1) = 6   ! ... k = +1
    end if

  end subroutine initialize_lhs_filter_stencils_PADE

  subroutine initDerivStencils(region, input, rank_in)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    ! ... incoming variables
    TYPE(t_region), pointer :: region
    TYPE(t_mixt_input), pointer :: input
    TYPE(t_grid), pointer :: grid
    Integer :: rank_in

    ! ... local variables
    integer :: i, j, k

    ! ... maximum number of interpolation points
    region%global%nStencilMax = (input%interpOrder)**(input%ND)

    ! ... initialize the stencils
    input%lhs_interior(:,:) = 0.0_rfreal
    input%rhs_interior(:,:) = 0.0_rfreal
    input%lhs_block1(:,:,:) = 0.0_rfreal
    input%lhs_block2(:,:,:) = 0.0_rfreal
    input%rhs_block1(:,:,:) = 0.0_rfreal
    input%rhs_block2(:,:,:) = 0.0_rfreal
    input%overlap(:) = 0
    input%SBP_Pdiag(:) = 0.0_rfreal
    input%bndry_explicit_deriv(:) = 0.0_rfreal
    input%max_operator_width(:) = 0
    input%operator_bndry_width(:) = 0
    input%operator_bndry_depth(:) = 0
    input%rhs_interior_stencil_start(:) = 0
    input%rhs_interior_stencil_end(:) = 0
    input%lhs_interior_stencil_start(:) = 0
    input%lhs_interior_stencil_end(:) = 0
    input%operator_type(:) = EXPLICIT
    nullify(input%HYPREStencilIndex)

    ! ... spatial derivative stencils
    if (input%spaceDiscr == EXPLICIT) then
      if (input%spaceOrder == 5) then
        if (rank_in == 0) write (*,'(A)') 'PlasComCM: ==> Using diagonal SBP 4-8 derivatives. <=='
        call initialize_deriv_stencils_SBP_4_8_diag(region, input)
      else if (input%spaceOrder == 4) then
        if (rank_in == 0) write (*,'(A)') 'PlasComCM: ==> Using DRP 11pt-13pt derivatives. <=='
        call initialize_deriv_stencils_DRP_11pt_13pt(region%myrank, input)
      else if (input%spaceOrder == 3) then
        if (rank_in == 0) write (*,'(A)') 'PlasComCM: ==> Using diagonal SBP 2-4 derivatives. <=='
        call initialize_deriv_stencils_SBP_2_4_diag(input, input%iFirstDeriv)
        ! call initialize_deriv_stencils_SBP_2_4_diag(input, input%iFirstDerivBiased)
      else if (input%spaceOrder == 2) then
        if (rank_in == 0) write (*,'(A)') 'PlasComCM: ==> Using diagonal SBP 1-2 derivatives. <=='
        call initialize_deriv_stencils_SBP_1_2_diag(input)
      else
        call graceful_exit(region%myrank, 'ERROR: Unknown order for explicit derivatives.')
      end if
    else
      if (input%spaceOrder == 4) then
        if (rank_in == 0) write (*,'(A)') 'PlasComCM: ==> Using Pade 3-4-6 derivatives. <=='
        call initialize_deriv_stencils_PADE(region%myrank, input)
      else
        call graceful_exit(region%myrank, 'ERROR: Unknown order for implicit derivatives.')
      end if
    end if

    ! ... interior filter
    if (input%filter .eqv. .true.) then
      if (input%filterDiscr == EXPLICIT) then
        ! call initialize_filter_stencils_drp_SFs11p(input)
        ! call initialize_filter_stencils_drp_SFo13p(input)
        if (any(input%bndry_filter) .eqv. .true.) then
          if (rank_in == 0) write (*,'(A)') 'PlasComCM: ==> Using DRP filter (biased). <=='
          call initialize_filter_stencils_DRP_7pt_11pt_13pt(input)
          ! call initialize_filter_stencils_2nd(input)
        else
          if (rank_in == 0) write (*,'(A)') 'PlasComCM: ==> Using DRP filter (centered). <=='
         !call initialize_filter_stencils_standard_explicit_11pt_centered(input)
          call initialize_filter_stencils_DRP_11pt_centered(input)
         !call initialize_filter_stencils_DRP_13pt_centered(input)
        end if
      end if
      if (input%filterDiscr == IMPLICIT) then

         ! ... subroutine to apply different filters on different on opposite sides of the domain
         call initialize_filter_stencils_PADE_new(region%myrank, input, rank_in)
         
         
         ! if (any(input%bndry_filter) .eqv. .true.) then
         !    if (rank_in == 0) write (*,'(A)') 'PlasComCM: ==> Using Pade filter (biased). <=='
         !    call initialize_filter_stencils_PADE(input)
         ! else
         !    if (rank_in == 0) write (*,'(A)') 'PlasComCM: ==> Using Pade filter (centered). <=='
         !    call initialize_filter_stencils_Pade_centered(input)
         !    ! call initialize_filter_stencils_PADE_centered_2nd_Order(input)
         ! end if

      end if
    end if

    ! ... LES test filter
    if (input%LES >= 200 .AND. input%LES < 300) then ! We need a test filter for dynamic LES model, JKim 12/2007
      ! call initialize_filter_stencils_PADE_centered_2nd_Order(input, input%iFilterTest, 0.25_8)
      call initialize_filter_stencils_TestFilter(region%myrank, input)
      ! call initialize_filter_stencils_TEST(input)
      ! call initialize_filter_stencils_drp_TFo11p(input)
    end if ! input%LES

    ! ... shock filter
    if (input%shock >= TRUE) then
      ! call initialize_filter_stencils_TruncatedGaussian(input)
      if (input%shock == 3) call initialize_adapative_shock_filter_stencils(input)
      if (input%shock /= 3) call initialize_filter_stencils_GaussianHyperviscosity(input)
    end if

    ! ... spatial stencils for implicit operator
    if (input%timeScheme == BDF_GMRES_IMPLICIT) then
      if (input%spaceOrder == 2)then
         call initialize_deriv_stencils_SBP_1_2_diag_implicit(input)
      else if (input%spaceOrder == 3) then
         call initialize_deriv_stencils_SBP_2_4_diag_implicit(input)
      end if
    end if

    ! ... total overlap
    input%nOverlap = MAXVAL(input%overlap(:))
    if (input%nOverlapOverride > input%nOverlap) then
      if (rank_in == 0) Write(*,'(2(A,I2))') 'PlasComCM: WARNING: Overriding overlap value of ', input%nOverlap, ' with ', input%nOverlapOverride
      input%nOverlap = MAX(input%nOverlap,input%nOverlapOverride)
    end if

    ! ... flag to state whether we should include modified viscosity in timestep
    input%tvCor_in_dt = FALSE
    if (input%shock /= 0 .OR. input%LES /= 0) input%tvCor_in_dt = TRUE

  end subroutine initDerivStencils

END MODULE ModInput
