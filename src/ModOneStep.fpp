! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
!-----------------------------------------------------------------------
!
! ModOnestep.f90
! 
! - Onestep combustion model
! 
!-----------------------------------------------------------------------
MODULE ModOnestep
  
CONTAINS
  
  Subroutine OnestepInit(this, input, first_param_ptr, myrank)
    
    USE ModGlobal
    USE ModDataStruct
    USE ModParam
    USE ModMPI

    Implicit None

    Type(t_onestep), pointer :: this
    Type(t_mixt_input), pointer :: input
    type(t_param), pointer :: first_param_ptr
    Integer, intent(in) :: myrank

    ! ... set the number of species
    this%numSpecies = input%nScalars

    If (this%numSpecies /= 2) Call Graceful_Exit(myrank, 'OnestepInit: only implemented for 2 species')

    ! ... read from the input file
    this%YO0 = get_real_param(myrank,'YO0',first_param_ptr,default=0.233_rfreal)
    this%YF0 = get_real_param(myrank,'YF0',first_param_ptr,default=1.0_rfreal)
    this%sRatio = get_real_param(myrank,'STOICH_RATIO',first_param_ptr,default=4.0_rfreal)
    this%Da = get_real_param(myrank,'DAMKOHLER',first_param_ptr,default=0.0_rfreal)
    this%zelDovich = get_real_param(myrank,'ZEL_DOVICH',first_param_ptr,default=6.0_rfreal)
    this%hRelease = get_real_param(myrank,'HEAT_RELEASE',first_param_ptr,default=0.75_rfreal)

    ! ... onestep reaction: F + sO -> (1 + s) P

    ! ... species indices
    this%iH2 = 1
    this%iO2 = 2
    this%iN2 = this%numSpecies + 1

    ! ... stoichiometric coefficients
    Allocate(this%sCoef(this%numSpecies))
    this%sCoef(this%iH2) = 1.0_rfreal
    this%sCoef(this%iO2) = this%sRatio

    ! ... stoichiometric fuel mass fraction
    this%Yfs = 1.0_rfreal / (1.0_rfreal + this%sRatio * this%YF0 / this%YO0)

    ! ... equation of state
    if (input%gas_dv_model /= DV_MODEL_IDEALGAS) &
         Call Graceful_Exit(myrank, 'OnestepInit: one-step chemistry requires GAS_EQUATION_OF_STATE = 0')
    
  End Subroutine OnestepInit

  Subroutine OnestepSource(this, cv, dv, aux, rhsNS, rhsSP, iblank, input)
    USE ModGlobal
    USE ModMPI
    USE ModDataStruct

    IMPLICIT none

    Type(t_onestep), pointer :: this
    real(rfreal), pointer, dimension(:,:) :: cv, dv, aux
    real(rfreal), dimension(:,:), intent(INOUT) :: rhsNS, rhsSP
    INTEGER, dimension(:), intent(IN) :: iblank
    type(t_mixt_input), pointer :: input

    ! ... local variables
    Integer :: Nc, iEnergy, iRho, Nsp, i, j, k, l, ierr
    Integer :: H2, O2, N2
    Real(rfreal) :: T, alpha, beta, omega, T0, Tf, Da, Yfsi, ibfac, buf1, buf2
    Real(rfreal), dimension(this%numSpecies) :: Y, sCoef

    ! ... number of species / reaction
    Nsp = this%numSpecies

    ! ... return if no species
    If (Nsp == 0) Return

    ! ... get number of grid points
    Nc      = ubound(dv,1)

    ! ... get indices of energy / rho
    iEnergy = ubound(cv,2)
    iRho    = lbound(cv,2)

    ! ... get species indices
    H2 = this%iH2
    O2 = this%iO2
    N2 = this%iN2

    ! ... stoichiometric coefficient
    sCoef = this%sCoef

    ! ... inverse of stoichiometric fuel mass fraction
    Yfsi = 1.0_rfreal/this%Yfs

    ! ... constant prefactor
    Da = this%Da

    ! ... heat release parameter
    alpha = this%hRelease

    ! ... Zel'dovich factor
    beta = this%zelDovich

    ! ... reference temperature
    T0 = 1.0_rfreal / (input%GamRef - 1.0_rfreal)

    ! ... adiabatic flame temperature
    Tf = T0 / (1.0_rfreal - alpha)

    ! .. loop through grid
    Do i = 1, Nc

       ! ... account for walls
       ibfac = dble(MIN(abs(iblank(i)),1))

       ! ... get temperature
       T = dv(i,2)

       ! ... local mass fraction (bounded)
       Do k = 1, Nsp
          Y(k) = max(min(aux(i,k) * dv(i,3), 1.0_rfreal), 0.0_rfreal)
       End Do

       ! ... chemical source term
       omega = Da * cv(i,irho) * Y(H2) * Y(O2) * exp(-beta / alpha * Tf / T) * ibfac

       ! ... add to species RHS
       Do k = 1, Nsp
          rhsSP(i,k) = rhsSP(i,k) - sCoef(k) * omega
       End Do

       ! ... add to energy RHS
       rhsNS(i, iEnergy) = rhsNS(i, iEnergy) + alpha * Tf * Yfsi * omega

    End Do

  End Subroutine OnestepSource
  
End Module ModOnestep
  
