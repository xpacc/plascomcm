! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
!-----------------------------------------------------------------------
!
! ModIOUtil.f90
!
! - some common utilities used in IO
!
!-----------------------------------------------------------------------
MODULE ModIOUtil

CONTAINS

  SUBROUTINE rBufferToBox(grid,rbuf,box)

    USE ModGlobal
    USE ModDataStruct

    IMPLICIT NONE

    ! ... incoming variables
    TYPE(t_grid), POINTER :: grid
    REAL(RFREAL), POINTER :: rbuf(:)
    REAL(RFREAL), POINTER :: box(:)

    ! ... local variables
    INTEGER :: i, j, k, lbox, lr, N(MAX_ND), Nbox(MAX_ND)

    N(:)  = 1
    Nbox(:) = 1

    DO i = 1, grid%ND
      N(i) = grid%ie_unique(i) - grid%is_unique(i) + 1
      Nbox(i) = grid%ie(i) - grid%is(i) + 1
    END DO

    ! ... unpack buffer
    DO k = 1, N(3)
       DO j = 1, N(2)
          DO i = 1, N(1)
            lr   = (k-1)*N(2)*N(1) + (j-1)*N(1) + i
            lbox = (k-1+grid%nGhostRHS(3,1))*Nbox(2)*Nbox(1) + (j-1+grid%nGhostRHS(2,1))*Nbox(1) + i + grid%nGhostRHS(1,1)
            box(lbox) = rbuf(lr)
          END DO
       END DO
    END DO

    RETURN

  END SUBROUTINE rBufferToBox

  subroutine rBoxToBuffer(grid,box,wbuf)

    use ModGlobal
    use ModDataStruct

    implicit none

    ! ... incoming variables
    type(t_grid), pointer :: grid
    real(rfreal), pointer :: box(:)
    real(rfreal), pointer :: wbuf(:)

    ! ... local variables
    integer :: i, j, k, lbox, lw, N(MAX_ND), Nbox(MAX_ND)

    N(:)  = 1
    Nbox(:) = 1

    do i = 1, grid%ND
      N(i) = grid%ie_unique(i) - grid%is_unique(i) + 1
      Nbox(i) = grid%ie(i) - grid%is(i) + 1
    end do

    ! ... pack box
    do k = 1, N(3)
       do j = 1, N(2)
          do i = 1, N(1)
            lw   = (k-1)*N(2)*N(1) + (j-1)*N(1) + i
            lbox = (k-1+grid%nGhostRHS(3,1))*Nbox(2)*Nbox(1) + (j-1+grid%nGhostRHS(2,1))*Nbox(1) + i + grid%nGhostRHS(1,1)
            wbuf(lw) = box(lbox)
          end do
       end do
    end do

    return

  end subroutine rBoxToBuffer

  SUBROUTINE iBufferToBox(grid,rbuf,box)

    USE ModGlobal
    USE ModDataStruct

    IMPLICIT NONE

    ! ... incoming variables
    TYPE(t_grid), POINTER :: grid
    INTEGER, POINTER :: rbuf(:)
    INTEGER, POINTER :: box(:)

    ! ... local variables
    INTEGER :: i, j, k, lbox, lr, N(MAX_ND), Nbox(MAX_ND)

    N(:)  = 1
    Nbox(:) = 1

    DO i = 1, grid%ND
      N(i) = grid%ie_unique(i) - grid%is_unique(i) + 1
      Nbox(i) = grid%ie(i) - grid%is(i) + 1
    END DO

    ! ... unpack buffer
    DO k = 1, N(3)
       DO j = 1, N(2)
          DO i = 1, N(1)
            lr   = (k-1)*N(2)*N(1) + (j-1)*N(1) + i
            lbox = (k-1+grid%nGhostRHS(3,1))*Nbox(2)*Nbox(1) + (j-1+grid%nGhostRHS(2,1))*Nbox(1) + i + grid%nGhostRHS(1,1)
            box(lbox) = rbuf(lr)
          END DO
       END DO
    END DO

    RETURN

  END SUBROUTINE iBufferToBox

  SUBROUTINE iBoxToBuffer(grid,box,wbuf)

    use ModGlobal
    use ModDataStruct

    implicit none

    ! ... incoming variables
    type(t_grid), pointer :: grid
    integer, pointer :: box(:)
    integer, pointer :: wbuf(:)

    ! ... local variables
    integer :: i, j, k, lbox, lw, N(MAX_ND), Nbox(MAX_ND)

    N(:)  = 1
    Nbox(:) = 1

    do i = 1, grid%ND
      N(i) = grid%ie_unique(i) - grid%is_unique(i) + 1
      Nbox(i) = grid%ie(i) - grid%is(i) + 1
    end do

    ! ... pack box
    do k = 1, N(3)
       do j = 1, N(2)
          do i = 1, N(1)
            lw   = (k-1)*N(2)*N(1) + (j-1)*N(1) + i
            lbox = (k-1+grid%nGhostRHS(3,1))*Nbox(2)*Nbox(1) + (j-1+grid%nGhostRHS(2,1))*Nbox(1) + i + grid%nGhostRHS(1,1)
            wbuf(lw) = box(lbox)
          end do
       end do
    end do

    return
    
  end subroutine iBoxToBuffer

  Subroutine InitSponge(region, X, ng_in_file)
    
    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    
    Implicit None

    ! ... input data
    TYPE(t_region), pointer :: region
    REAL(rfreal), pointer :: X(:,:,:,:)
    Integer :: ng_in_file

    ! ... local data
    TYPE(t_mixt_input), pointer :: input
    TYPE(t_patch), pointer :: patch
    TYPE(t_grid), pointer :: grid
    Integer :: npatch, N(MAX_ND), Np(MAX_ND), Nc, sgn, normDir, j, k, l1, l2, ii1, ii2, jj1, jj2, kk1, kk2, N1, N2

    ! ... loop through all patches
    do npatch = 1, region%nPatches

      patch => region%patch(npatch)
      grid => region%grid(patch%gridID)

      ! ... only save information if this grid corresponds to ng_in_file
      if ( grid%iGridGlobal == ng_in_file ) then

        Nc = grid%nCells
        N  = 1; Np = 1;
        do j = 1, grid%ND
          N(j)  = grid%ie(j)-grid%is(j)+1
          Np(j) = patch%ie(j) - patch%is(j) + 1
        end do
        normDir = abs(patch%normDir)
        sgn = normDir / patch%normDir

        ! ... check to see if this is a sponge boundary
        if ( patch%bcType == SPONGE .or. patch%bcType == SPONGE_WITH_LINEARIZED_DISTURBANCE ) then

!!$          print *, '>>> allocating ', patch%normIndex
!!$          print *, '>>> allocating ', grid%is(normDir)
!!$          print *, '>>> allocating ', patch%normIndex-grid%is(normDir)+1
!!$          print *, '>>> allocating ', patch%normIndex+sgn*Np(normDir)-grid%is(normDir)+1

          ! ... allocate memory for sponge boundary coordinates
          allocate(patch%sponge_xs(grid%vds(normDir),grid%ND))
          allocate(patch%sponge_xe(grid%vds(normDir),grid%ND))

          ! ... loop over all of the points and save the coordinate
          do k = 1, grid%vds(normDir)

            ! ... point ON the boundary
            !l1 = grid%vec_ind(k,patch%normIndex-grid%is(normDir)+1,normDir)
            l1 = grid%vec_ind(k,1,normDir)

            ! ... points in from the boundary
            l2 = grid%vec_ind(k,N(normDir),normDir)

            ! ... convert l1, l2 to global indices

            ! ... (ii1, jj1, kk1) are the local indices
            N1 = grid%ie(1)-grid%is(1)+1
            N2 = grid%ie(2)-grid%is(2)+1
            ii1 = mod(l1-1,N1)+1
            jj1 = mod((l1-ii1)/N1,N2)+1
            kk1 = (l1-ii1-(jj1-1)*N1)/(N1*N2)+1

            ! ... (ii1, jj1, kk1) are now the global indices
            ii1 = ii1 + grid%is(1) - 1
            jj1 = jj1 + grid%is(2) - 1
            kk1 = kk1 + grid%is(3) - 1

            ! ... (ii2, jj2, kk2) are the local indices
            ii2 = mod(l2-1,N1)+1
            jj2 = mod((l2-ii2)/N1,N2)+1
            kk2 = (l2-ii2-(jj2-1)*N1)/(N1*N2)+1

            ! ... (ii2, jj2, kk2) are now the global indices
            ii2 = ii2 + grid%is(1) - 1
            jj2 = jj2 + grid%is(2) - 1
            kk2 = kk2 + grid%is(3) - 1

            ! ... one of these indices is wrong, depending on the value of normDir
            SELECT CASE (patch%normDir)

              CASE (1)
                ii1 = MIN(patch%bcData(4),patch%bcData(5))
                ii2 = MAX(patch%bcData(4),patch%bcData(5))
              CASE (-1)
                ii1 = MAX(patch%bcData(4),patch%bcData(5))
                ii2 = MIN(patch%bcData(4),patch%bcData(5))
              CASE (2)
                jj1 = MIN(patch%bcData(6),patch%bcData(7))
                jj2 = MAX(patch%bcData(6),patch%bcData(7))
              CASE (-2)
                jj1 = MAX(patch%bcData(6),patch%bcData(7))
                jj2 = MIN(patch%bcData(6),patch%bcData(7))
              CASE (3)
                kk1 = MIN(patch%bcData(8),patch%bcData(9))
                kk2 = MAX(patch%bcData(8),patch%bcData(9))
              CASE (-3)
                kk1 = MAX(patch%bcData(8),patch%bcData(9))
                kk2 = MIN(patch%bcData(8),patch%bcData(9))

            END SELECT

            if ( ii1 <= 0 .OR. jj1 <= 0 .OR. kk1 <= 0 ) then
              print *, '1: ', ii1, jj1, kk1, patch%normDir
              print *, '1: ', k, l1, N1, N2
              print *, '1: ', patch%bcData(:)
            end if

            if ( ii2 <= 0 .OR. jj2 <= 0 .OR. kk2 <= 0 ) then
              print *, '2: ', ii2, jj2, kk2, patch%normDir
              print *, '2: ', k, l2, N1, N2
              print *, '2: ', patch%bcData(:)
            end if

            ! ... save the coordinates
            patch%sponge_xs(k,1:grid%ND) = X(ii2,jj2,kk2,1:grid%ND) ! ... point in from the boundary
            patch%sponge_xe(k,1:grid%ND) = X(ii1,jj1,kk1,1:grid%ND) ! ... point on the boundary

            if (abs(sqrt(dot_product( patch%sponge_xe(k,1:grid%ND) - patch%sponge_xs(k,1:grid%ND), patch%sponge_xe(k,1:grid%ND) - patch%sponge_xs(k,1:grid%ND)))) <= 1e-10) then
              print *, 'PlasComCM: sponge length too small ', ii1, jj1, kk1, ii2, jj2, kk2
            end if

          End do

        end if

      end if

    end do

  End Subroutine InitSponge

END MODULE ModIOUtil
