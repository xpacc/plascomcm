! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
!-----------------------------------------------------------------------
!
! ModPostProcess.f90
!
! - Postprocess PlasComCM solution files
! - Analyzes turbulent boundary layer
! - Computes mean, RMS, and two-point stats
!
! Revision history
! - 20 Oct 2014 : JSC : initial code
!
!-----------------------------------------------------------------------
MODULE ModPostProcess

CONTAINS

  ! ================================= !
  ! INITIALIZE POSTPROCESSING ROUTINE !
  ! ================================= !
  Subroutine PostProcess(region)

    USE ModGlobal
    USE ModDataStruct
    USE ModIO
    USE ModPLOT3D_IO
#ifdef HAVE_HDF5
    USE ModHDF5_IO
#endif
    USE ModMPI
    USE ModDerivBuildOps
    USE ModDeriv
    USE ModMetrics
    USE ModInterp
    USE ModDataUtils
    USE ModFVSetup
    USE ModEOS
    USE ModPLOT3D_IO

    Implicit None

    ! Global variables
    type(t_region), pointer :: region

    ! Local variables
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    type(t_mixt_input), pointer :: input
    Integer, Dimension(:,:), Pointer :: ND
    Integer :: N(MAX_ND)
    Integer :: ngrid, I, J, K, NDIM, ng, ip, err_sum, ierr
    Real(KIND=8) :: TAU(4),T0
    Integer, allocatable :: err(:)
    Real(KIND=8), Dimension(:), Pointer :: time
    Character(LEN=80) :: fname
    Integer :: startIter, stopIter, skipIter, iter, numfiles, num

    ! Build operators
    ! Simplicity
    input => region%input
    region%global%filtercount(:) = 1
    allocate(err(region%nGrids)); err(:) = 0

    ! Set up the interpolation data once for static grids
    if (input%gridType == CHIMERA) call Setup_Interpolation(region)
       
    ! Output initial data
    do ng = 1, region%nGrids
       grid => region%grid(ng)
       state => region%state(ng)
       call computeDv(region%myrank, grid, state)
    end do

    ! Alternative OPERATOR_SETUP which does not require all processors to have global_iblank at the same time
    If (input%use_lowmem_operator_setup == TRUE) Then
       If (region%myrank == 0) Write (*,'(A)') 'PlasComCM: Building operators using low-memory option.  This may take a while...'
       Do i = 1, region%nGridsGlobal
          Do ng = 1, region%nGrids
             grid => region%grid(ng)
             If (grid%iGridGlobal == i) Then
                Do ip = 0, grid%numproc_inComm-1
                   Call Request_Global_Mask(region, ng, ip)
                   If (grid%myrank_inComm == ip) Call Operator_Setup(region, grid%input, grid, err(ng), ng)
                   Call MPI_Barrier(grid%comm, ierr)
                End Do
             End if
             Call MPI_Barrier(mycomm, ierr)
          End Do
       End Do
       Do ng = 1, region%nGrids
          grid => region%grid(ng)
          Call HYPRE_Pade_Operator_Setup(region, ng)
       End Do
    Else
       ! Compute the derivative matrices in the computational
       ! Coordinates.  These are independent of the mesh coordinates, but depend on IBLANK
       If (region%myrank == 0) Write (*,'(A)') 'PlasComCM: Building operators...'
       Call create_global_mask(region)
       do ng = 1, region%nGrids
          grid => region%grid(ng)
          Call Operator_Setup(region, grid%input, grid, err(ng), ng)
          if (region%input%operator_implicit_solver == GMRES) Call HYPRE_Pade_Operator_Setup(region, ng)
       end do
    End If

    Call MPI_Allreduce(sum(err(:)), err_sum, 1, MPI_INTEGER, MPI_SUM, mycomm, ierr)
    if (err_sum /= 0) Call Output_Rank_As_IBLANK(region)

    ! Compute metrics OUTSIDE loop
    do ng = 1, region%nGrids
       call metrics(region, ng)
    end do
    call MPI_BARRIER(mycomm,ierr)

    !-----------------------------
    ! Finished building operators

    ! Write to screen
    If (region%myrank == 0) write (*,'(A)') 'PlasComCM: ==== BEGIN POSTPROCESS ===='

    ! Find number of files to postprocess
    startIter = input%startIter
    stopIter = input%stopIter
    skipIter = input%skipIter
    numFiles = 0
    Do iter=startIter,stopIter,skipIter
       numFiles = numFiles + 1
    End Do

    ! Loop over all files, store time info
    Allocate(time(numFiles))
    num=0
    Do iter = startIter, stopIter, skipIter
       ! Wait for all procs before reading
       Call mpi_barrier(mycomm,ierr)

       If (input%useHDF5 /= 1) Then
          ! PLOT3D format
          Write(fname,'(A,I8.8,A)') 'RocFlo-CM.', iter, '.q'

          ! ... read solution header to find time
          Call mpi_barrier(mycomm,ierr)
          Call ReadGridSize_P3D(NDIM, ngrid, ND, fname, region%myrank)       
          Call ReadSolutionHeader_P3D(NDIM, ngrid, ND, fname, tau)

          ! ... save time
          num = num + 1
          time(num) = tau(4)
       Else
#ifdef HAVE_HDF5
          ! HDF5 format
          Write(fname,'(A,I8.8,A)') 'RocFlo-CM.', iter, '.h5'

          ! ... read solution header to find time
          Call ReadGridSize_HDF5(NDIM, ngrid, ND, fname)
          Call mpi_barrier(mycomm,ierr)
          Call ReadRestart_HDF5(region, fname)

          ! ... save time
          num = num + 1
          time(num) = state%time(1)
#endif
       End If

       ! Deallocate
       Deallocate(ND)

    End Do
    t0 = time(1)

    ! Select case
    Select Case (input%PP_Case)
    Case (1)
       ! Analyze 3-D boundary layer
       Call PP_BoundaryLayer(region,startIter,stopIter,skipIter,numFiles)
    Case (2)
       ! Boundary layer momentum thickness vs. x
       Call PP_MomentumThickness(region,startIter,stopIter,skipIter,numFiles)
    Case default
       Stop 'Unknown postprocessing case'
    End select

    ! Finalize
    Call MPI_BARRIER(mycomm,ierr)
    If (region%myrank == 0) write (*,'(A)') 'PlasComCM: ==== END POSTPROCESS ===='

    Return
  End Subroutine PostProcess


  ! ========================== !
  ! POSTPROCESS BOUNDARY LAYER !
  ! ========================== !
  Subroutine PP_BoundaryLayer(region,startIter,stopIter,skipIter,numFiles)

    USE ModGlobal
    USE ModDataStruct
    USE ModIO
    USE ModMPI
    USE ModDerivBuildOps
    USE ModDeriv
    USE ModMetrics
    USE ModInterp
    USE ModDataUtils
    USE ModFVSetup
    USE ModEOS
#ifdef HAVE_HDF5
    USE ModHDF5_IO
#endif
    USE ModPLOT3D_IO

    Implicit None

    ! Global variables
    type(t_region), pointer :: region
    Integer, intent(in) :: startIter, stopIter, skipIter, numFiles

    ! Local variables
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    type(t_mixt_input), pointer :: input
    Integer, Dimension(:,:), Pointer :: ND
    Integer :: N(MAX_ND), is(MAX_ND), ie(MAX_ND)
    Integer :: ng, ngrid, ip, iter, num, err_sum, ierr
    Integer :: l0, l, m, I, J, K, NDIM, ii,jj, I1, I2
    Real(KIND=8) :: RHO,U,V,W,P
    Real(KIND=8) :: dimL,dimRho,dimVel,dimVisc
    Integer, allocatable :: err(:)
    Real(rfreal), Dimension(:,:), pointer :: ptr_to_data
    Real(KIND=8), Dimension(:,:,:,:), Pointer :: X
    Real(KIND=8), Dimension(:), Allocatable :: Y, buf1d
    Real(KIND=8), Dimension(:,:), Allocatable :: meanU,meanV,meanW,meanP,eps,cnt,buf2d
    Real(KIND=8), Dimension(:,:), Allocatable :: meanUU,meanVV,meanWW,meanUV,meanPP,meanUVW
    Real(rfreal), Pointer :: StrnRt(:,:)
    Character(LEN=2)  :: prec, gf, vf, ib
    character(len=3) :: var_to_write
    Character(LEN=80) :: fname

    ! Simplicity
    input => region%input
    grid => region%grid(1)
    state => region%state(1)

    ! Read the grid only once
    Call MPI_BARRIER(MYCOMM,ierr)

    Do ng = 1, region%nGrids
       
       grid  => region%grid(ng)
       input => grid%input
       
       if (grid%iGridGlobal == ng) then
          
          ! ... recheck grid size
          call ReadGridSize(NDIM, ngrid, ND, input%grid_fname, region%myrank, ib)
          CALL ReadSingleGrid(region,ng)
       end if
    end do

    Call MPI_BARRIER(MYCOMM,ierr)

    ! ... extent of this grid (in C-counting)
    is(:) = 0; ie(:) = 0; N(:) = 1;
    do j = 1, grid%ND
       is(j) = grid%is(j)
       ie(j) = grid%ie(j)
       N(j) = ie(j) - is(j) + 1
    end do

    ! ... allocate buffer
    Allocate(X(N(1),N(2),N(3),3))

    ! ... pack buffer & find measurement location
    I1 =  999999
    I2 = -999999
    do k = 1, N(3)
       do j = 1, N(2)
          do i = 1, N(1)

             l0 = (k-1)*N(1)*N(2) + (j-1)*N(1) + i

             do m = 1, grid%ND
                X(i,j,k,m) = grid%XYZ(l0,m) 
             end do

             If (X(i,j,k,1).GE.input%X1 .and. X(i,j,k,1).LE.input%X2) Then
                I1=min(I1,I+grid%is(1)-1)
                I2=max(I2,I+grid%is(1)-1)
             End If

          end do
       end do
    end do

    Call MPI_Allreduce(I1, I, 1, MPI_INTEGER, MPI_MIN, mycomm, ierr); I1=I
    Call MPI_Allreduce(I2, I, 1, MPI_INTEGER, MPI_MAX, mycomm, ierr); I2=I
    If (region%myrank==0) write(*,'(A)') ""
    If (region%myrank==0) print *, "Measurement location:",I1,":",I2
    If (region%myrank==0) write(*,'(A)') ""

    ! Allocate 1D arrays
    Allocate(Y(ND(1,2))); Y=HUGE(1.0D0)
    Allocate(buf1d(ND(1,2)))

    ! Allocate 2D arrays
    Allocate(cnt(I1:I2,ND(1,2))); cnt=0.0D0
    Allocate(buf2d(I1:I2,ND(1,2)))  

    ! Get contiguous vertical coordinate
    ! Be careful to avoid deformed grid above trip strip
    Do j = grid%is(2), grid%ie(2)
       Y(j) = X(1,j-grid%is(2)+1,1,2)
    End Do
    Call MPI_Allreduce(Y, buf1d, ND(1,2), MPI_REAL8, MPI_MIN, mycomm, ierr); Y=buf1d

    ! --------------------------------------------------------------
    ! Compute mean/variance velocity statistics (as a function of y)

    ! Write to screen
    If (region%myrank==0) write(*,*) ""
    If (region%myrank==0) print *, 'PlasComCM postprocessing: computing mean stats'
    If (region%myrank==0) write(*,*) ""

    ! Allocate mean arrays
    Allocate(meanU(I1:I2,ND(1,2))); meanU=0.0D0
    Allocate(meanV(I1:I2,ND(1,2))); meanV=0.0D0
    Allocate(meanW(I1:I2,ND(1,2))); meanW=0.0D0
    Allocate(meanUU(I1:I2,ND(1,2))); meanUU=0.0D0
    Allocate(meanVV(I1:I2,ND(1,2))); meanVV=0.0D0
    Allocate(meanWW(I1:I2,ND(1,2))); meanWW=0.0D0
    Allocate(meanUV(I1:I2,ND(1,2))); meanUV=0.0D0

    Do iter = input%startIter, input%stopIter, input%skipIter

       ! Nullify pointers
       nullify(grid,state,ptr_to_data)

       ! Read the solution file
       If (input%useHDF5 == 1) Then
#ifdef HAVE_HDF5
          Write(fname,'(A,I8.8,A)') 'RocFlo-CM.', iter, '.h5'
          If (region%myrank==0) print *, 'PlasComCM postprocessing reading: ',fname
          Call ReadRestart_HDF5(region, fname)

          ! ... this grid's data
          ng=1
          grid  => region%grid(ng)
          state => region%state(ng)
          ptr_to_data => state%cv
#endif
          
       Else
          Write(fname,'(A,I8.8,A)') 'RocFlo-CM.', iter, '.q'

          If (region%myrank==0) print *, 'PlasComCM postprocessing reading: ',fname

          Call mpi_barrier(mycomm, ierr)
          Do ip = 0, numproc-1

             If (region%myrank == ip) then
             
                Do ng = 1, 1!region%nGrids

                   ! ... this grid's data
                   grid  => region%grid(ng)
                   state => region%state(ng)
                   ptr_to_data => state%cv

                   Call read_single_soln_low_mem(NDIM, ND, region, grid, fname, ptr_to_data, grid%iGridGlobal)
                   
                   Call mpi_barrier(mycomm, ierr)

                End do
             
             End if

          End do
       End If
       Call mpi_barrier(mycomm, ierr)

       ! Sum along vertical height within measurement window
       Do k = 1, N(3)
          Do j = 1, N(2)
             Do i = 1, N(1)

                l0 = (k-1)*N(1)*N(2) + (j-1)*N(1) + i

                ! Avoid devision by zero
                If (state%cv(l0,1).gt.0.00001D0) Then

                   ! Check if inside measurement window
                   If (grid%XYZ(l0,1).GE.input%X1 .and. grid%XYZ(l0,1).LE.input%X2) Then

                      ! Get the variables
                      RHO = state%cv(l0,1)
                      U   = state%cv(l0,2)/RHO
                      V   = state%cv(l0,3)/RHO
                      W   = state%cv(l0,4)/RHO

                      ! Global index
                      ii = i + grid%is(1) - 1
                      jj = j + grid%is(2) - 1

                      ! Compute mean velocity
                      meanU(ii,jj) = meanU(ii,jj) + U
                      meanV(ii,jj) = meanV(ii,jj) + V
                      meanW(ii,jj) = meanW(ii,jj) + W

                      ! Compute mean velocity^2
                      meanUU(ii,jj) = meanUU(ii,jj) + U*U
                      meanVV(ii,jj) = meanVV(ii,jj) + V*V
                      meanWW(ii,jj) = meanWW(ii,jj) + W*W
                      meanUV(ii,jj) = meanUV(ii,jj) + U*V

                      ! Update counter
                      cnt(ii,jj) = cnt(ii,jj) + 1.0D0

                   End If
                End If

             End Do
          End Do
       End Do
    End Do
    Call mpi_barrier(mycomm, ierr)

    If (region%myrank==0) write(*,*) ""
    If (region%myrank==0) print *, 'Finished looping through files'
    If (region%myrank==0) write(*,*) ""

    ! Sum data across all procs and take mean as a function of y
    Call MPI_Allreduce(cnt,     buf2d, (I2-I1+1)*ND(1,2), MPI_REAL8, MPI_SUM, mycomm, ierr); cnt     = buf2d
    Call MPI_Allreduce(meanU , buf2d, (I2-I1+1)*ND(1,2), MPI_REAL8, MPI_SUM, mycomm, ierr); meanU  = buf2d/cnt
    Call MPI_Allreduce(meanV , buf2d, (I2-I1+1)*ND(1,2), MPI_REAL8, MPI_SUM, mycomm, ierr); meanV  = buf2d/cnt
    Call MPI_Allreduce(meanW , buf2d, (I2-I1+1)*ND(1,2), MPI_REAL8, MPI_SUM, mycomm, ierr); meanW  = buf2d/cnt
    Call MPI_Allreduce(meanUU, buf2d, (I2-I1+1)*ND(1,2), MPI_REAL8, MPI_SUM, mycomm, ierr); meanUU = buf2d/cnt
    Call MPI_Allreduce(meanVV, buf2d, (I2-I1+1)*ND(1,2), MPI_REAL8, MPI_SUM, mycomm, ierr); meanVV = buf2d/cnt
    Call MPI_Allreduce(meanWW, buf2d, (I2-I1+1)*ND(1,2), MPI_REAL8, MPI_SUM, mycomm, ierr); meanWW = buf2d/cnt
    Call MPI_Allreduce(meanUV, buf2d, (I2-I1+1)*ND(1,2), MPI_REAL8, MPI_SUM, mycomm, ierr); meanUV = buf2d/cnt

    ! ------------------------------------
    ! Compute turbulent dissipation rate
    If (region%myrank==0) write(*,*) ""
    If (region%myrank==0) print *, 'PlasComCM postprocessing: computing dissipation rate'
    If (region%myrank==0) write(*,*) ""

    ! Allocate dissipation rate
    Allocate(eps(I1:I2,ND(1,2))); eps=0.0D0

    If (associated(state%VelGrad1st) .eqv. .true.) deallocate(state%VelGrad1st)
    If (associated(state%flux) .eqv. .true.) deallocate(state%flux)
    If (associated(state%dflux) .eqv. .true.) deallocate(state%dflux)
    Allocate(state%VelGrad1st(grid%nCells,grid%ND*grid%ND))
    Allocate(state%flux(grid%nCells))
    Allocate(state%dflux(grid%nCells))
    Allocate(StrnRt(grid%nCells,grid%ND*(grid%ND+1)/2))

    ! Store mean velocities
    Allocate(meanUVW(grid%nCells,grid%ND)); meanUVW=0.0D0
    do k = grid%is(3), grid%ie(3)
       do j = grid%is(2), grid%ie(2)
          do i = grid%is(1), grid%ie(1)
             l0 = (k-grid%is(3))*(grid%ie(1)-grid%is(1)+1) &
                  *(grid%ie(2)-grid%is(2)+1) &
                  + (j-grid%is(2))*(grid%ie(1)-grid%is(1)+1) + i-grid%is(1)+1
             If (grid%XYZ(l0,1).GE.input%X1 .and. grid%XYZ(l0,1).LE.input%X2) Then
                meanUVW(l0,1) = meanU(i,j)
                meanUVW(l0,2) = meanV(i,j)
                meanUVW(l0,3) = meanW(i,j)
             End If
          end do
       end do
    end do    

    ! Loop through solution files
    Do iter = input%startIter, input%stopIter, input%skipIter

       ! Nullify pointers
       nullify(grid,state,ptr_to_data)
          
       ! Read the solution file
       If (input%useHDF5 == 1) Then
#ifdef HAVE_HDF5
          ! HDF5 format
          Write(fname,'(A,I8.8,A)') 'RocFlo-CM.', iter, '.h5'
          If (region%myrank==0) print *, 'PlasComCM postprocessing reading: ',fname
          Call ReadRestart_HDF5(region, fname)

          ! ... this grid's data
          ng=1
          grid  => region%grid(ng)
          state => region%state(ng)
          ptr_to_data => state%cv
#endif
       Else

          ! PLOT3D format
          Write(fname,'(A,I8.8,A)') 'RocFlo-CM.', iter, '.q'
          If (region%myrank==0) print *, 'PlasComCM postprocessing reading: ',fname

          Call mpi_barrier(mycomm, ierr)
          Do ip = 0, numproc-1

             If (region%myrank == ip) then
             
                Do ng = 1, 1!region%nGrids

                   ! ... this grid's data
                   grid  => region%grid(ng)
                   state => region%state(ng)
                   ptr_to_data => state%cv

                   Call read_single_soln_low_mem(NDIM, ND, region, grid, fname, ptr_to_data, grid%iGridGlobal)

                   Call mpi_barrier(mycomm, ierr)

                End do
             
             End if

          End do
       End If
       Call mpi_barrier(mycomm, ierr)

       ! ... compute the dependent variables
       Call computeDv(region%myrank, grid, state)

       ! ... compute the transport variables
       Call computeTv( grid, state)
       
       ! Compute fluctuating velocity gradient
       Do j = 1, grid%ND
          Do i = 1, grid%nCells
             state%flux(i) = state%cv(i,region%global%vMap(j+1)) * state%dv(i,3) - meanUVW(i,j)
          end do
          do k = 1, grid%ND
             call APPLY_OPERATOR(region, 1, 1, k, state%flux, state%dflux, .FALSE.)
             do i = 1, grid%nCells
                state%VelGrad1st(i,region%global%t2Map(j,k)) = state%dflux(i) ! d(u_j)/d(xi_k) (i, k = 1 ~ 3)
             end do
          end do ! k
       end do ! j
       
       ! Compute strain-rate tensor
       Call mpi_barrier(mycomm, ierr)
       StrnRt = 0.0_rfreal
       Call PP_StrnRt(region, grid, state%VelGrad1st, StrnRt)

       cnt = 0.0D0
       Do k = 1, N(3)
          Do j = 1, N(2)
             Do i = 1, N(1)

                l0 = (k-1)*N(1)*N(2) + (j-1)*N(1) + i

                ! Avoid devision by zero
                If (state%cv(l0,1).gt.0.00001D0) Then

                   ! Check if inside measurement window
                   If (grid%XYZ(l0,1).GE.input%X1 .and. grid%XYZ(l0,1).LE.input%X2) Then

                      ! Global index
                      ii = i + grid%is(1) - 1
                      jj = j + grid%is(2) - 1

                   ! Turbulent dissipation rate: eps = 2*nu*<sij sij>
                   do l=1,grid%ND
                      ! Include diagonal from strain-rate
                      eps(ii,jj) = eps(ii,jj) + &
                           2.0D0*state%tv(l0,1)*state%dv(i,3)*StrnRt(l0,region%global%t2MapSym(l,l))*StrnRt(l0,region%global%t2MapSym(l,l))
                      ! 2x off-diagonal
                      do m=l+1,grid%ND
                         eps(ii,jj) = eps(ii,jj) + &
                              4.0D0*state%tv(l0,1)*state%dv(i,3)*StrnRt(l0,region%global%t2MapSym(l,m))*StrnRt(l0,region%global%t2MapSym(l,m))
                      end do

                      ! Update counter
                      cnt(ii,jj) = cnt(ii,jj) + 1.0D0

                    End Do
                    End If
                 End If
             End Do
          End Do
       End Do
    End Do

    ! Normalize eps
    Call MPI_Allreduce(cnt, buf2d, (I2-I1+1)*ND(1,2), MPI_REAL8, MPI_SUM, mycomm, ierr); cnt=buf2d
    Call MPI_Allreduce(eps, buf2d, (I2-I1+1)*ND(1,2), MPI_REAL8, MPI_SUM, mycomm, ierr); eps  = buf2d/cnt

    ! Dimensionalization factors
    dimL    = input%LengRef
    dimVel  = input%SndSpdRef
    dimRho  = input%DensRef
    dimVisc = input%DensRef*input%SndSpdRef*input%LengRef/input%RE

    ! Write to file
    If (region%myrank == 0) then
       print *, 'Writing ystat.txt'
       open(10,file='ystat.txt',form="formatted",iostat=ierr,status="REPLACE")

       ! Write header
       write(*,*) ""
       write(10,'(7a15)') "y","U","V","uu","uv","uv","eps"
       write(*,*) ""
       
       ! Write statistics (in dimensional form)
       Do J=1,ND(1,2)
          write(10,'(7ES15.6)') Y(J)*dimL, &
               sum(meanU(:,J))/DBLE(I2-I1+1)*dimVel, &
               sum(meanV(:,J))/DBLE(I2-I1+1)*dimVel, &
               sum(meanUU(:,J)-meanU(:,J)*meanU(:,J))/DBLE(I2-I1+1)*dimVel**2, &
               sum(meanVV(:,J)-meanV(:,J)*meanV(:,J))/DBLE(I2-I1+1)*dimVel**2, &
               sum(meanUV(:,J)-meanU(:,J)*meanV(:,J))/DBLE(I2-I1+1)*dimVel**2, &
               sum(eps(:,J))/DBLE(I2-I1+1)*dimVisc/dimRho*dimVel**2/dimL**2
       End Do

       ! Close the file
       close(10)
    End If

    ! Finalize
    Deallocate(X,Y,meanU,meanV,meanUVW,MeanUU,meanVV,meanUV,eps,buf1d,buf2d,cnt)

    Return
  End Subroutine PP_BoundaryLayer


  ! ================================ !
  ! POSTPROCESS BOUNDARY LAYER       !
  ! Compute momentum thickness vs. x !
  ! ================================ !
  Subroutine PP_MomentumThickness(region,startIter,stopIter,skipIter,numFiles)

    USE ModGlobal
    USE ModDataStruct
    USE ModIO
    USE ModMPI
    USE ModDerivBuildOps
    USE ModDeriv
    USE ModMetrics
    USE ModInterp
    USE ModDataUtils
    USE ModFVSetup
    USE ModEOS
#ifdef HAVE_HDF5
    USE ModHDF5_IO
#endif
    USE ModPLOT3D_IO
    Implicit None

    ! Global variables
    type(t_region), pointer :: region
    Integer, intent(in) :: startIter, stopIter, skipIter, numFiles

    ! Local variables
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    type(t_mixt_input), pointer :: input
    Integer, Dimension(:,:), Pointer :: ND
    Integer :: N(MAX_ND), is(MAX_ND), ie(MAX_ND)
    Integer :: ng, ngrid, ip, iter, num, err_sum, ierr
    Integer :: l0, l, m, I, J, K, NDIM, ii,jj
    Real(KIND=8) :: RHO,U,V,W,P,U0
    Real(KIND=8) :: dimL,dimRho,dimVel,dimVisc
    Integer, allocatable :: err(:)
    Real(rfreal), Dimension(:,:), pointer :: ptr_to_data
    Real(KIND=8), Dimension(:,:,:,:), Pointer :: X
    Real(KIND=8), Dimension(:), Allocatable :: X1D,Y, Theta, buf1d
    Real(KIND=8), Dimension(:,:), Allocatable :: meanU,cnt,buf2d
    Character(LEN=2)  :: prec, gf, vf, ib
    character(len=3) :: var_to_write
    Character(LEN=80) :: fname

    ! Simplicity
    input => region%input
    grid => region%grid(1)
    state => region%state(1)

    ! Read the grid only once
    Call MPI_BARRIER(MYCOMM,ierr)

    Do ng = 1, region%nGrids
       
       grid  => region%grid(ng)
       input => grid%input
       
       if (grid%iGridGlobal == ng) then
          
          call ReadGridSize(NDIM, ngrid, ND, input%grid_fname, region%myrank, ib)
          CALL ReadSingleGrid(region,ng)
          
       end if
    end do
    
    Call MPI_BARRIER(MYCOMM,ierr)

    ! ... extent of this grid (in C-counting)
    is(:) = 0; ie(:) = 0; N(:) = 1;
    do j = 1, grid%ND
       is(j) = grid%is(j)
       ie(j) = grid%ie(j)
       N(j) = ie(j) - is(j) + 1
    end do

    ! ... allocate buffer
    Allocate(X(N(1),N(2),N(3),3))

    ! ... pack buffer
    do k = 1, N(3)
       do j = 1, N(2)
          do i = 1, N(1)

             l0 = (k-1)*N(1)*N(2) + (j-1)*N(1) + i

             do m = 1, grid%ND
                X(i,j,k,m) = grid%XYZ(l0,m) 
             end do

          end do
       end do
    end do

    ! Allocate 1D arrays
    Allocate(X1D(ND(1,1))); X1D=HUGE(1.0D0)
    Allocate(Y(ND(1,2))); Y=HUGE(1.0D0)
    Allocate(Theta(ND(1,1)))

    ! Allocate 2D arrays
    Allocate(cnt(ND(1,1),ND(1,2))); cnt=0.0D0
    Allocate(buf2d(ND(1,1),ND(1,2))) 

    ! Get contiguous streamwise coordinate
    Do i = grid%is(1), grid%ie(1)
       X1D(I) = X(i-grid%is(1)+1,1,1,1)
    End Do
    Allocate(buf1d(ND(1,1)))
    Call MPI_Allreduce(X1D, buf1d, ND(1,1), MPI_REAL8, MPI_MIN, mycomm, ierr); X1D=buf1d
    Deallocate(buf1d)

    ! Get contiguous vertical coordinate
    ! Be careful to avoid deformed grid above trip strip
    Allocate(buf1d(ND(1,2)))
    Do j = grid%is(2), grid%ie(2)
       Y(j) = X(1,j-grid%is(2)+1,1,2)
    End Do
    Call MPI_Allreduce(Y, buf1d, ND(1,2), MPI_REAL8, MPI_MIN, mycomm, ierr); Y=buf1d
    Deallocate(buf1d)

    ! --------------------------------------------------------------
    ! Compute mean U (as a function of y)

    ! Write to screen
    If (region%myrank==0) write(*,*) ""
    If (region%myrank==0) print *, 'PlasComCM postprocessing: computing mean stats'
    If (region%myrank==0) write(*,*) ""

    ! Allocate mean arrays
    Allocate(meanU(ND(1,1),ND(1,2))); meanU=0.0D0

    Do iter = input%startIter, input%stopIter, input%skipIter

       ! Nullify pointers
       nullify(grid,state,ptr_to_data)

       ! Read the solution file
       If (input%useHDF5 == 1) Then
#ifdef HAVE_HDF5
          Write(fname,'(A,I8.8,A)') 'RocFlo-CM.', iter, '.h5'
          If (region%myrank==0) print *, 'PlasComCM postprocessing reading: ',fname
          Call ReadRestart_HDF5(region, fname)

          ! ... this grid's data
          ng=1
          grid  => region%grid(ng)
          state => region%state(ng)
          ptr_to_data => state%cv
#endif
          
       Else
          Write(fname,'(A,I8.8,A)') 'RocFlo-CM.', iter, '.q'

          If (region%myrank==0) print *, 'PlasComCM postprocessing reading: ',fname

          Call mpi_barrier(mycomm, ierr)
          Do ip = 0, numproc-1

             If (region%myrank == ip) then
             
                Do ng = 1, 1!region%nGrids

                   ! ... this grid's data
                   grid  => region%grid(ng)
                   state => region%state(ng)
                   ptr_to_data => state%cv

                   Call read_single_soln_low_mem(NDIM, ND, region, grid, fname, ptr_to_data, grid%iGridGlobal)
                   
                   Call mpi_barrier(mycomm, ierr)

                End do
             
             End if

          End do
       End If
       Call mpi_barrier(mycomm, ierr)

       ! Sum along vertical height within measurement window
       Do k = 1, N(3)
          Do j = 1, N(2)
             Do i = 1, N(1)

                l0 = (k-1)*N(1)*N(2) + (j-1)*N(1) + i

                ! Avoid devision by zero
                If (state%cv(l0,1).gt.0.00001D0) Then

                   ! Get the variables
                   RHO = state%cv(l0,1)
                   U   = state%cv(l0,2)/RHO
                   V   = state%cv(l0,3)/RHO
                   W   = state%cv(l0,4)/RHO

                   ! Global index
                   ii = i + grid%is(1) - 1
                   jj = j + grid%is(2) - 1

                   ! Compute mean velocity
                   meanU(ii,jj) = meanU(ii,jj) + U

                   ! Update counter
                   cnt(ii,jj) = cnt(ii,jj) + 1.0D0

                End If

             End Do
          End Do
       End Do
    End Do
    Call mpi_barrier(mycomm, ierr)

    If (region%myrank==0) write(*,*) ""
    If (region%myrank==0) print *, 'Finished looping through files'
    If (region%myrank==0) write(*,*) ""

    ! Sum data across all procs and take mean as a function of y
    Call MPI_Allreduce(cnt,     buf2d, ND(1,1)*ND(1,2), MPI_REAL8, MPI_SUM, mycomm, ierr); cnt     = buf2d
    Call MPI_Allreduce(meanU , buf2d, ND(1,1)*ND(1,2), MPI_REAL8, MPI_SUM, mycomm, ierr); meanU  = buf2d/cnt

    ! Compute momentum thickness
    If (region%myrank==0) Then
       U0 = sum(meanU(:,ND(1,2))); U0=U0/DBLE(ND(1,1))
       print *, 'Bulk velocity=',U0
       Theta = 0.0D0
       Do I=1,ND(1,1)
          U0 = meanU(I,ND(1,2))
          Do J=2,ND(1,2)-1
             Theta(I) = Theta(I) + meanU(I,J)/U0*(1.0D0-meanU(I,J)/U0)*0.5D0*(Y(J+1)-Y(J-1))
          End Do
       End Do

       ! Dimensionalization factors
       dimL    = input%LengRef
       dimVel  = input%SndSpdRef
       dimRho  = input%DensRef
       dimVisc = input%DensRef*input%SndSpdRef*input%LengRef/input%RE

       ! Write to file
       print *, 'Writing theta_x.txt'
       open(10,file='theta_x.txt',form="formatted",iostat=ierr,status="REPLACE")

       ! Write header
       write(*,*) ""
       write(10,'(2a15)') "x","Theta"
       write(*,*) ""
       
       ! Write statistics (in dimensional form)
       Do I=1,ND(1,1)
          write(10,'(2ES15.6)') X1D(I)*dimL, Theta(i)*dimL
       End Do

       ! Close the file
       close(10)
    End If

    ! Finalize
    Deallocate(X,Y,X1D,meanU,Theta,buf2d,cnt)

    Return
  End Subroutine PP_MomentumThickness


  ! =================== !
  ! COMPUTE STRAIN RATE !
  ! =================== !
  Subroutine PP_StrnRt(region, grid, VelGrad1st, StrnRt)

    USE ModGlobal
    USE ModDataStruct

    Implicit None

    ! ... subroutine arguments
    type(t_region), pointer :: region
    real(rfreal), pointer :: VelGrad1st(:,:), StrnRt(:,:)
    type(t_grid), pointer :: grid

    ! ... local variables
    integer :: i, j, k, ii
    integer :: ND

    ! ... simplicity
    ND = grid%ND

    ! ... initialize
    do i = 1, size(StrnRt,2)
       do ii = 1, grid%nCells
          StrnRt(ii,i) = 0.0_rfreal
       end do
    end do

    do i = 1,ND
       do k = 1,ND ! diagonal components first
          do ii = 1, grid%nCells
             StrnRt(ii,region%global%t2MapSym(i,i)) = StrnRt(ii,region%global%t2MapSym(i,i)) + &
                  grid%MT1(ii,region%global%t2Map(k,i)) * VelGrad1st(ii,region%global%t2Map(i,k))
          end do
       end do ! k
       do j = i+1,ND ! upper-half part of strain-rate tensor due to symmetry
          do k = 1,ND
             do ii = 1, grid%nCells
                StrnRt(ii,region%global%t2MapSym(i,j)) = StrnRt(ii,region%global%t2MapSym(i,j)) + &
                     grid%MT1(ii,region%global%t2Map(k,j)) * VelGrad1st(ii,region%global%t2Map(i,k)) + &
                     grid%MT1(ii,region%global%t2Map(k,i)) * VelGrad1st(ii,region%global%t2Map(j,k))
             end do
          end do ! k
          do ii = 1, grid%nCells
             StrnRt(ii,region%global%t2MapSym(i,j)) = 0.5_rfreal * StrnRt(ii,region%global%t2MapSym(i,j))
          end do
       end do ! j
    end do ! i
    do k = 1,size(StrnRt,2)
       do ii = 1, grid%nCells
          StrnRt(ii,k) = grid%JAC(ii) * StrnRt(ii,k)
       end do
    end do ! k

  End Subroutine PP_StrnRt


END MODULE ModPostProcess
