#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <inttypes.h>
#include <assert.h>

#ifdef CMAKE_BUILD
#include "FC.h"
#else /* CMAKE_BUILD */
#include "plascomcmconf.h"
#endif /* CMAKE_BUILD */

#include "mpi.h"
#include "mesh_io.h"

#include "hdf5.h"




int GetOffsets_HDF5(char *fileName,char *groupName,char **dataItemNames,
		    MPI_Comm ioCommunicator,haddr_t *fileOffsets,int *dataPresent,
		    int *dataSize,int *dataOrder,int numDataItems)
{

  hid_t fileID,groupID,datasetID,accessID,typeID;
  /*  H5T_order_t fileOrder; */
  int i;
  hid_t access_type = H5P_FILE_ACCESS;
  int rank;
  int returnStatus = 0;
	   
  MPI_Comm_rank(ioCommunicator,&rank);

  if(rank == 0) {
    fileID = H5Fopen(fileName, H5F_ACC_RDONLY, H5P_DEFAULT);
    if(fileID < 0) returnStatus = 1;
    else {
      groupID = H5Gopen(fileID,groupName, H5P_DEFAULT);
      if(groupID < 0){
	H5Fclose(fileID);
	returnStatus = 2;
      }
    }
    if(!returnStatus){
      for(i = 0;i < numDataItems;i++){
	/* 	if(rank == 0) { */
	/* 	  printf("Searching for %s.\n",dataItemNames[i]); */
	/* 	} */
	if(H5Lexists(groupID,dataItemNames[i],H5P_DEFAULT) <= 0){
	  dataPresent[i] = 0;
	  fileOffsets[i] = HADDR_UNDEF;
	} else {
/* 	  printf("Found %s.\n",dataItemNames[i]); */
	  dataPresent[i] = 1;
	  datasetID      = H5Dopen(groupID,dataItemNames[i],H5P_DEFAULT);
	  typeID         = H5Dget_type(datasetID);
	  dataOrder[i]   = H5Tget_order(typeID);
	  dataSize[i]    = H5Tget_size(typeID);
	  fileOffsets[i] = H5Dget_offset(datasetID);
/* 	  printf("Offset: %lld\n",fileOffsets[i]); */
	  H5Dclose(datasetID);
	  H5Tclose(typeID);
	}
      }
      H5Gclose(groupID);
      H5Fclose(fileID);
    }
  }
  MPI_Bcast(&returnStatus,1,MPI_INT,0,ioCommunicator);
  if(returnStatus > 0){
    return(returnStatus);
  }
  MPI_Bcast(dataPresent,numDataItems,MPI_INT,0,ioCommunicator);
  MPI_Bcast(dataSize,numDataItems,MPI_INT,0,ioCommunicator);
  MPI_Bcast(dataOrder,numDataItems,MPI_INT,0,ioCommunicator);
  MPI_Bcast(fileOffsets,numDataItems,MPI_LONG_LONG_INT,0,ioCommunicator);
  return 0;
}

int GetGridOffsets_HDF5(char *fileName,char *groupName,MPI_Comm ioCommunicator,
			haddr_t *fileOffsets,int *gridPresent,int *iBlankPresent, 
			int *iBlankDataSize,int *dataOrder)
{

  hid_t fileID,groupID,datasetID,accessID,typeID;
  /*  H5T_order_t fileOrder; */
  char *coordinateNames[] = {"X","Y","Z"};
  int i;
  hid_t access_type = H5P_FILE_ACCESS;
  int rank;
  int returnStatus = 0;
  unsigned char gridStatus = 0;
  unsigned char hasGrid    = 1;
  unsigned char hasIBLANK  = 2;
  unsigned char isDouble   = 4;
  unsigned char isMSB      = 8;

  MPI_Comm_rank(ioCommunicator,&rank);

  *iBlankPresent = 0;
  *gridPresent = 0;
  *iBlankDataSize = 4;
  *dataOrder = 0;

  if(rank == 0) {
    fileID = H5Fopen(fileName, H5F_ACC_RDONLY, H5P_DEFAULT);
    if(fileID < 0) returnStatus = 1;
    else {
      groupID = H5Gopen(fileID,groupName, H5P_DEFAULT);
      if(groupID < 0){
	H5Fclose(fileID);
	returnStatus = 2;
      }
    }
    if(!returnStatus){
      if(H5Lexists(groupID,"X",H5P_DEFAULT) <= 0){
	H5Fclose(fileID);
      } else {
	gridStatus |= hasGrid;
	for(i=0;i<3;i++){
	  if(H5Lexists(groupID,coordinateNames[i],H5P_DEFAULT) > 0) {
	    datasetID = H5Dopen(groupID,coordinateNames[i],H5P_DEFAULT);
	    typeID    = H5Dget_type(datasetID);
	    if(H5Tget_order(typeID) == 1){
	      gridStatus |= isMSB;
	    }
	    fileOffsets[i] = H5Dget_offset(datasetID);
	    H5Dclose(datasetID);
	    H5Tclose(typeID);
	  } else {
	    fileOffsets[i] = HADDR_UNDEF;
	  }
	}
	if(H5Lexists(groupID,"IBLANK",H5P_DEFAULT) > 0){
	  gridStatus |= hasIBLANK; 
	  datasetID = H5Dopen(groupID,"IBLANK",H5P_DEFAULT);
	  typeID    = H5Dget_type(datasetID);
	  if(H5Tget_size(typeID) == 8)
	    gridStatus |= isDouble;
	  H5Tclose(typeID);
	  fileOffsets[3] = H5Dget_offset(datasetID);
	  H5Dclose(datasetID);
	} else {
	  fileOffsets[3] = HADDR_UNDEF;
	}
	H5Gclose(groupID);
	H5Fclose(fileID);
      }
    }
  }
  MPI_Bcast(&returnStatus,1,MPI_INT,0,ioCommunicator);
  if(returnStatus > 0){
    return(returnStatus);
  }
  MPI_Bcast(&gridStatus,1,MPI_BYTE,0,ioCommunicator);
  if(gridStatus & hasGrid){
    *gridPresent = 1;
  }
  if(gridStatus & hasIBLANK){
    *iBlankPresent = 1;
    if(gridStatus & isDouble){
      *iBlankDataSize = 8;
    }
  }
  if(gridStatus & isMSB){
    *dataOrder = 1;
  }
  MPI_Bcast(fileOffsets,4,MPI_LONG_LONG_INT,0,ioCommunicator);
  return 0;
}

int GetGridData_HDF5(char *fileName,char *groupName,MPI_Comm gridCommunicator,
		     double *coordinateData,int *iblankData,int numDim,int *fileStarts,
		     int *localSizes,int *meshSizes,int *gridPresent,int *iblankPresent)
{

  haddr_t fileOffsets[4];
  int dataOrder = 0;
  int hasGrid = 0;
  int iDim,i;
  MPI_Info info;
  MPI_File grid_inf;
  int amode = MPI_MODE_RDONLY;
  int rank;
  size_t numPointsGlobal = 1;
  size_t numPointsLocal = 1;
  haddr_t currentOffset = 0;
  int localStarts[3] = {0,0,0};
  int iBlankDataSize = 4;

  *iblankPresent = 0;
  *gridPresent = 0;

  for(iDim=0;iDim < numDim;iDim++){
    numPointsLocal *= localSizes[iDim];
    numPointsGlobal  *= meshSizes[iDim];
  }

  MPI_Info_create(&info);
  MPI_Comm_rank(gridCommunicator, &rank);

  if(GetGridOffsets_HDF5(fileName,groupName,gridCommunicator,
			 fileOffsets,gridPresent,iblankPresent,
			 &iBlankDataSize,&dataOrder)){
    printf("GetGridOffsets_HDF5 failed!!\n");
    return 1;
  }
  
  if(*gridPresent <= 0){
    return 0;
  }

  for(i =0;i < numDim;i++)
    if(fileOffsets[i] != HADDR_UNDEF)
      hasGrid = 1;
  if(hasGrid == 0){
    return 1;
  }

  /*  printf("Rank %d opening %s\n",rank,fname_nullterm); */
  int returnCode = MPI_File_open(gridCommunicator, fileName, amode, info, &grid_inf);
  if(returnCode != MPI_SUCCESS){
    perror("MPI_File_open failed");
    exit(1);
  }

  for(iDim = 0;iDim < numDim;iDim++){
    currentOffset = fileOffsets[iDim];
    if(currentOffset != HADDR_UNDEF){
      returnCode = Mesh_IO_read(grid_inf,currentOffset,MPI_DOUBLE,dataOrder,
				&coordinateData[iDim*numPointsLocal],numDim,
				localSizes,meshSizes,fileStarts,MPI_ORDER_FORTRAN,
				localSizes,localStarts,MPI_ORDER_FORTRAN,gridCommunicator);
      if(returnCode != MPI_SUCCESS){
	perror("Mesh_IO_read failed");
	exit(1);
      }     
    }
  }
  if(*iblankPresent > 0){
    currentOffset = fileOffsets[3];
    if(currentOffset != HADDR_UNDEF){
      if(iBlankDataSize == 4) {
	returnCode = Mesh_IO_read(grid_inf,currentOffset,MPI_INT,dataOrder,&iblankData[0],numDim,
				  localSizes,meshSizes,fileStarts,MPI_ORDER_FORTRAN,
				  localSizes,localStarts,MPI_ORDER_FORTRAN,gridCommunicator);
      } else {
	double *iBlankReadBuf = (double *)calloc(numPointsLocal,sizeof(double));
	returnCode = Mesh_IO_read(grid_inf,currentOffset,MPI_DOUBLE,dataOrder,iBlankReadBuf,numDim,
				  localSizes,meshSizes,fileStarts,MPI_ORDER_FORTRAN,
				  localSizes,localStarts,MPI_ORDER_FORTRAN,gridCommunicator);
	if(returnCode == MPI_SUCCESS){
	  for(i = 0;i < numPointsLocal;i++)
	    iblankData[i] = (int)iBlankReadBuf[i];
	}
	free(iBlankReadBuf);
      } 
    }
    if(returnCode != MPI_SUCCESS){
      perror("Mesh_IO_read iblank failed");
      exit(1);
    }
  }

  MPI_File_close(&grid_inf);
  MPI_Info_free(&info);

  return 0;
}

int GetSolutionData_HDF5(char *fileName,char *groupName,char *solnRootName,MPI_Comm gridCommunicator,
			 double *solutionData,int numDim,int numVar,int *partitionStarts,
			 int *partitionSizes,int *domainSizes)
{

  haddr_t *fileOffsets;
  int     *dataOrder;
  int     *dataSize;
  int     *dataPresent;
  int     *localStarts;
  char    **dataItemNames;

  int iDim,iVar;
  MPI_Info info;
  MPI_File grid_inf;
  int amode = MPI_MODE_RDONLY;
  int rank;
  int hasData = 0;
  size_t numPointsGlobal = 1;
  size_t numPointsLocal = 1;
  haddr_t currentOffset = 0;
  int returnVal = 0;
  double *dataBuffer = NULL;
  int rootLen = strlen(solnRootName);

  fileOffsets   = (haddr_t *)calloc(numVar,sizeof(haddr_t));
  dataOrder     = (int *)calloc(numVar,sizeof(haddr_t));
  dataSize      = (int *)calloc(numVar,sizeof(haddr_t));
  dataPresent   = (int *)calloc(numVar,sizeof(haddr_t));
  localStarts   = (int *)calloc(numVar,sizeof(haddr_t));
  dataItemNames = (char **)calloc(numVar,sizeof(char *));
  
  for(iDim=0;iDim < numDim;iDim++){
    numPointsLocal     *= partitionSizes[iDim];
    numPointsGlobal    *= domainSizes[iDim];
  }
  for(iVar = 0;iVar < numVar;iVar++){
    localStarts[iVar] = 0;
    dataOrder[iVar]   = 0;
    dataSize[iVar]    = 0;
    dataPresent[iVar] = 0;
    dataItemNames[iVar] = (char *)calloc(rootLen+3,sizeof(char));
    sprintf(dataItemNames[iVar],"%s%2.2i",solnRootName,iVar+1);
    dataItemNames[iVar][rootLen+2] = '\0';
    /*    printf("Searching for %s.\n",dataItemNames[iVar]); */
  }

  if(GetOffsets_HDF5(fileName,groupName,dataItemNames,gridCommunicator,
		     fileOffsets,dataPresent,dataSize,dataOrder,numVar)){
    printf("GetOffsets_HDF5 failed!!\n");
    returnVal = 1;
  }
  
  for(iVar = 0;iVar < numVar;iVar++){
    if(dataPresent[iVar]){
      /*      printf("Found %s.\n",dataItemNames[iVar]); */
      hasData = 1;
      if(dataSize[iVar] != sizeof(double)){
	printf("Data has wrong size!\n");
	returnVal++;
      }
    }
    free(dataItemNames[iVar]);
  }  
  free(dataItemNames);
  if(!hasData){
    /*    printf("NO data found!\n"); */
    return(returnVal);
  }
  if(returnVal)
    return(returnVal+1);
  /*  printf("Rank %d opening %s\n",rank,fname_nullterm); */
  MPI_Info_create(&info);
  MPI_Comm_rank(gridCommunicator, &rank);
  int returnCode = MPI_File_open(gridCommunicator, fileName, amode, info, &grid_inf);
  if(returnCode != MPI_SUCCESS){
    perror("MPI_File_open failed");
    exit(1);
  }

  for(iVar = 0;iVar < numVar;iVar++){
    currentOffset = fileOffsets[iVar];
    if(currentOffset != HADDR_UNDEF){
      dataBuffer = &solutionData[iVar*numPointsLocal];
      returnCode = Mesh_IO_read(grid_inf,currentOffset,MPI_DOUBLE,dataOrder[iVar],
				dataBuffer,numDim,partitionSizes,domainSizes,
				partitionStarts,MPI_ORDER_FORTRAN,partitionSizes,
				localStarts,MPI_ORDER_FORTRAN,gridCommunicator);
      if(returnCode != MPI_SUCCESS){
	perror("Mesh_IO_read failed");
	exit(1);
      }     
    }
  }

  MPI_File_close(&grid_inf);
  MPI_Info_free(&info);

  return(returnVal);
}

int WriteSolutionData_HDF5(char *fileName,char *groupName,char **dataItemNames,MPI_Comm gridCommunicator,
			   int *flagData,double *solutionData,int numDim,int numDataItems,
			   int *partitionStarts,int *partitionSizes,int *domainSizes){

  haddr_t *fileOffsets;
  int     *dataOrder;
  int     *dataSize;
  int     *dataPresent;
  int     *localStarts;

  int iDim,iVar;
  MPI_Info info;
  MPI_File grid_inf;
  int amode = MPI_MODE_WRONLY;
  int rank;
  int hasData = 0;
  size_t numPointsGlobal = 1;
  size_t numPointsLocal = 1;
  haddr_t currentOffset = 0;
  int returnVal = 0;
  double *dataBuffer = NULL;
  int    *flagBuffer = NULL;
  int numSolnItems = 0;
  int numFlagItems = 0;
  int i;

  fileOffsets   = (haddr_t *)calloc(numDataItems,sizeof(haddr_t));
  dataOrder     = (int *)calloc(numDataItems,sizeof(int));
  dataSize      = (int *)calloc(numDataItems,sizeof(int));
  dataPresent   = (int *)calloc(numDataItems,sizeof(int));
  localStarts   = (int *)calloc(numDataItems,sizeof(int));
  
  for(iDim=0;iDim < numDim;iDim++){
    numPointsLocal     *= partitionSizes[iDim];
    numPointsGlobal    *= domainSizes[iDim];
  }
  for(iVar = 0;iVar < numDataItems;iVar++){
    localStarts[iVar] = 0;
    dataOrder[iVar]   = 0;
    dataSize[iVar]    = 0;
    dataPresent[iVar] = 0;
  }

  if(GetOffsets_HDF5(fileName,groupName,dataItemNames,gridCommunicator,
		     fileOffsets,dataPresent,dataSize,dataOrder,numDataItems)){
    printf("GetOffsets_HDF5 failed!!\n");
    returnVal = 1;
  }

  for(iVar = 0;iVar < numDataItems;iVar++){
    if(dataPresent[iVar] <= 0){
      returnVal++;
    }
  }  
  if(returnVal)
    return(returnVal);
  /*  printf("Rank %d opening %s\n",rank,fname_nullterm); */
  MPI_Info_create(&info);
  MPI_Comm_rank(gridCommunicator, &rank);
  int returnCode = MPI_File_open(gridCommunicator, fileName, amode, info, &grid_inf);
  if(returnCode != MPI_SUCCESS){
    perror("MPI_File_open failed");
    exit(1);
  }


  numSolnItems = 0;
  numFlagItems = 0;
  for(iVar = 0;iVar < numDataItems;iVar++){
    currentOffset = fileOffsets[iVar];
    if(currentOffset != HADDR_UNDEF){
      if(dataSize[iVar] == sizeof(double)){
	dataBuffer = &solutionData[numSolnItems*numPointsLocal];
	numSolnItems++;
	returnCode = Mesh_IO_write(grid_inf,currentOffset,MPI_DOUBLE,dataOrder[iVar],
				   dataBuffer,numDim,partitionSizes,domainSizes,
				   partitionStarts,MPI_ORDER_FORTRAN,partitionSizes,
				   localStarts,MPI_ORDER_FORTRAN,gridCommunicator);
      } else if (dataSize[iVar] == sizeof(int) ){
	flagBuffer = &flagData[numFlagItems*numPointsLocal];
	numFlagItems++;
	returnCode =  Mesh_IO_write(grid_inf,currentOffset,MPI_INT,dataOrder[iVar],
				    flagBuffer,numDim,partitionSizes,domainSizes,
				    partitionStarts,MPI_ORDER_FORTRAN,partitionSizes,
				    localStarts,MPI_ORDER_FORTRAN,gridCommunicator);
      }
      if(returnCode != MPI_SUCCESS){
	perror("Mesh_IO_write failed");
	exit(1);
      }     
    }
  }

  MPI_File_close(&grid_inf);
  MPI_Info_free(&info);

  return(returnVal);
}

#ifdef CMAKE_BUILD
#define HDF5READGRIDDATA_FC FC_GLOBAL_(hdf5readgriddata,HDF5READGRIDDATA)
#else /* CMAKE_BUILD */
#define HDF5READGRIDDATA_FC FC_FUNC_(hdf5readgriddata,HDF5READGRIDDATA)
#endif /* CMAKE_BUILD */
void HDF5READGRIDDATA_FC(char *fileNameIn,char *groupNameIn,int *communicatorIn,
			 double *coordinateData,int *iblankData,int *numDim,int *fileStarts,
			 int *localSizes,int *meshSizes,int *gridPresent,int *iblankPresent,
			 int *ierror,int fileNameLen,int groupNameLen)
{
  char groupName[64];
  char fileName[128];
  MPI_Comm ioCommunicator;
  int rank;

  ioCommunicator = MPI_Comm_f2c(*communicatorIn);
  MPI_Comm_rank(ioCommunicator,&rank);

  assert(groupNameLen < 64);
  assert(fileNameLen < 128);

  strncpy(fileName,fileNameIn,fileNameLen);
  fileName[fileNameLen] = '\0';

  strncpy(groupName,groupNameIn,groupNameLen);
  groupName[groupNameLen] = '\0';


  *ierror = GetGridData_HDF5(fileName,groupName,ioCommunicator,coordinateData,iblankData,
			     *numDim,fileStarts,localSizes,meshSizes,gridPresent,iblankPresent);
  
  return;
}


#ifdef CMAKE_BUILD
#define HDF5READSOLUTIONDATA_FC FC_GLOBAL_(hdf5readsolutiondata,HDF5READSOLUTIONDATA)
#else /* CMAKE_BUILD */
#define HDF5READSOLUTIONDATA_FC FC_FUNC_(hdf5readsolutiondata,HDF5READSOLUTIONDATA)
#endif /* CMAKE_BUILD */
void HDF5READSOLUTIONDATA_FC(char *fileNameIn,char *groupNameIn,char *rootNameIn,
			     int *communicatorIn,double *solutionData,int *numDim,
			     int *partitionStarts,int *partitionSizes,int *domainSizes,
			     int *numVar,int *ierror,int fileNameLen,int groupNameLen,int rootNameLen)
{
  char groupName[64];
  char fileName[128];
  char rootName[24];
  MPI_Comm ioCommunicator;
  int rank;

  ioCommunicator = MPI_Comm_f2c(*communicatorIn);
  MPI_Comm_rank(ioCommunicator,&rank);

  assert(groupNameLen < 64);
  assert(fileNameLen < 128);
  assert(rootNameLen < 24);

  strncpy(fileName,fileNameIn,fileNameLen);
  fileName[fileNameLen] = '\0';

  strncpy(groupName,groupNameIn,groupNameLen);
  groupName[groupNameLen] = '\0';

  strncpy(rootName,rootNameIn,rootNameLen);
  rootName[rootNameLen] = '\0';

  *ierror = GetSolutionData_HDF5(fileName,groupName,rootName,ioCommunicator,solutionData,
				 *numDim,*numVar,partitionStarts,partitionSizes,domainSizes);
  
  return;
}

#ifdef CMAKE_BUILD
#define HDF5WRITESOLUTIONDATA_FC FC_GLOBAL_(hdf5writesolutiondata,HDF5WRITESOLUTIONDATA)
#else /* CMAKE_BUILD */
#define HDF5WRITESOLUTIONDATA_FC FC_FUNC_(hdf5writesolutiondata,HDF5WRITESOLUTIONDATA)
#endif /* CMAKE_BUILD */
void HDF5WRITESOLUTIONDATA_FC(char *fileNameIn,char *groupNameIn,char *dataItemNameListIn,
			      int *communicatorIn,int *flagData,double *solutionData,int *numDim,
			      int *partitionStarts,int *partitionSizes,int *domainSizes,
			      int *numDataItems,int *ierror,int fileNameLen,int groupNameLen,int nameListLen)
{
  char groupName[64];
  char fileName[128];
  MPI_Comm ioCommunicator;
  int rank;
  int i;

  char *dataItemNameList;
  char **dataItemNames;

  
  ioCommunicator = MPI_Comm_f2c(*communicatorIn);
  MPI_Comm_rank(ioCommunicator,&rank);

  dataItemNameList = (char *)calloc(nameListLen+1,sizeof(char));
  strncpy(dataItemNameList,dataItemNameListIn,nameListLen);
  dataItemNameList[nameListLen] = '\0';

  dataItemNames = (char **)calloc(*numDataItems,sizeof(char *));
  dataItemNames[0] = NULL;
  dataItemNames[0] = strtok(dataItemNameList," ");
  for(i = 1;i < *numDataItems;i++){
    dataItemNames[i] = NULL;
    dataItemNames[i] = strtok(NULL," ");
    if(!dataItemNames[i]) break;
  }
  
  assert(groupNameLen < 64);
  assert(fileNameLen  < 128);

  strncpy(fileName,fileNameIn,fileNameLen);
  fileName[fileNameLen] = '\0';

  strncpy(groupName,groupNameIn,groupNameLen);
  groupName[groupNameLen] = '\0';

  /* if(rank == 0) {  */
  /*   printf("Writing the following data in %s(%s):\n",fileName,groupName);   */
  /*   for(i = 0;i < *numDataItems;i++) */
  /*     if(dataItemNames[i]) */
  /* 	printf("%s\n",dataItemNames[i]); */
  /*     else */
  /* 	printf("MISSING DATA ITEM!!\n"); */
  /* }  */
  /* exit(1); */
  *ierror = WriteSolutionData_HDF5(fileName,groupName,dataItemNames,ioCommunicator,flagData,solutionData,
				   *numDim,*numDataItems,partitionStarts,partitionSizes,domainSizes);


  free(dataItemNames);
  free(dataItemNameList);

  return;
}

