! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
!-----------------------------------------------------------------------
!
! ModEOS.f90
! 
! - basic code for the equation of state
!
! Revision history
! - 21 Dec 2006 : DJB : initial code
!
! $Header: /cvsroot/genx/Codes/RocfloCM/Source/ModEOS.f90,v 1.23 2011/11/05 11:11:43 bodony Exp $
!
!-----------------------------------------------------------------------
Module ModEOS

CONTAINS

  subroutine computeDv_IdealGas(grid, state)

!   optimized by John L. Larson 7/21/2014

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    TYPE(t_grid), pointer :: grid
    TYPE(t_mixt), pointer :: state
    TYPE(t_mixt_input), pointer :: input

    ! ... local variables
    integer :: j, i, ND, Nc
    real(rfreal) :: gamma, gamref, ibfac_local
    real(rfreal) :: gamrefm1, gambygamm1, recipgemrefm1, onemibfac

    ! ... declarations for pointer dereference, WZhang 05/2014
    integer, pointer :: iblank(:)
    integer :: nAuxvars
    real(rfreal), pointer :: cv(:,:), dv(:,:), auxVars(:,:), ibfac(:)
    real(rfreal) :: dvtemp

    ! ... simplicity
    ND = grid%ND
    Nc = grid%nCells
    input => grid%input
    gamref = input%GamRef

    ! ... pointer dereference, WZhang 05/2014
    ibfac   => grid%ibfac
    iblank  => grid%iblank
    cv      => state%cv
    dv      => state%dv
    nAuxVars = input%nAuxVars
    auxVars => state%auxVars

    gamrefm1 = gamref - 1.0_8
    recipgemrefm1 = 1.0_8 / gamrefm1
    gambygamm1 = gamref / gamrefm1

    if( ND == 2 ) then
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
      do i = 1, Nc

        ibfac_local = ibfac(i)
        onemibfac = 1.0_8 - ibfac_local
                              ! ... adjust original values wrt IBLANK 1
        cv(i,1) = ibfac_local * cv(i,1) + onemibfac
                              ! ... specific volume
        dv(i,3) = 1.0_8 / cv(i,1)
                              ! ... adjust original values wrt IBLANK 2
        cv(i,4) = ibfac_local * cv(i,4) + onemibfac * recipgemrefm1
                              ! ... adjust original values wrt IBLANK 3
        cv(i,2) = ibfac_local * cv(i,2)
        cv(i,3) = ibfac_local * cv(i,3)
                              ! ... pressure 1
        dvtemp  = ( cv(i,2) * cv(i,2) &
                  + cv(i,3) * cv(i,3) ) * dv(i,3)
                              ! ... pressure 2
        dv(i,1) = gamrefm1 * ( cv(i,4) - 0.5_rfreal * dvtemp )
                              ! ... temperature
        dv(i,2) = gambygamm1 * dv(i,1) * dv(i,3)

      end do

    else if( ND == 3 ) then
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
      do i = 1, Nc

        ibfac_local = ibfac(i)
        onemibfac = 1.0_8 - ibfac_local
                              ! ... adjust original values wrt IBLANK 1
        cv(i,1) = ibfac_local * cv(i,1) + onemibfac
                              ! specific volume
        dv(i,3) = 1.0_8 / cv(i,1)
                              ! ... adjust original values wrt IBLANK 2
        cv(i,5) = ibfac_local * cv(i,5) + onemibfac * recipgemrefm1
                              ! ... adjust original values wrt IBLANK 3
        cv(i,2) = ibfac_local * cv(i,2)
        cv(i,3) = ibfac_local * cv(i,3)
        cv(i,4) = ibfac_local * cv(i,4)
                              ! ... pressure 1
        dvtemp  = ( cv(i,2) * cv(i,2) &
                  + cv(i,3) * cv(i,3) &
                  + cv(i,4) * cv(i,4) ) * dv(i,3) 
                              ! ... pressure 2
        dv(i,1) = gamrefm1 * ( cv(i,5) - 0.5_rfreal * dvtemp )
                              ! ... temperature
        dv(i,2) = gambygamm1 * dv(i,1) * dv(i,3)

      end do

    end if

    do j = 1, nAuxVars
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
      do i = 1, Nc
        auxVars(i,j) = auxVars(i,j) * ibfac(i)
      end do
    end do

  end subroutine computeDv_IdealGas

  subroutine computeDv_ThermPerfGas_TP(myrank, grid, state)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    integer :: myrank
    TYPE(t_grid), pointer :: grid
    TYPE(t_mixt), pointer :: state

    ! ... local variables
    integer :: j, i, ND
    real(rfreal) :: ibfac, gamma, e, Temp, gamref
    TYPE(t_mixt_input), pointer :: input

    ! ... simplicity
    input => grid%input
    ND = grid%ND
    gamref = input%GamRef

    do i = 1, size(state%cv,1)

      ibfac = grid%ibfac(i)
      gamma = state%gv(i,1)

      ! ... adjust original values wrt IBLANK
      state%cv(i,   1) = ibfac * state%cv(i,1) + (1.0_8 - ibfac)
      state%cv(i,ND+2) = ibfac * state%cv(i,ND+2) + (1.0_8 - ibfac) / (gamma - 1.0_8)
      do j = 1, ND
        state%cv(i,j+1) = ibfac * state%cv(i,j+1)
      end do

      ! specific volume
      state%dv(i,3) = 1.0_8 / state%cv(i,1)

      ! ... energy
      e = state%cv(i,ND+2)
      do j = 1, ND
        e = e - 0.5_rfreal * state%cv(i,j+1)*state%cv(i,j+1)*state%dv(i,3)
      end do
      e = e * state%dv(i,3)

      ! ... temperature and ratio of specific heats, consult table
      ! TPGasLookup(myrank,input,varout,valin,valout,gamma)
      call TPGasLookup(myrank, input, 2, e, Temp, gamma)

      ! ... ratio of specific heats
      state%gv(i,1) = gamma

      ! ... temperature
      state%dv(i,2) = Temp     

      ! ... pressure 
      state%dv(i,1) =  Temp * (gamref - 1.0_rfreal) * state%cv(i,1) / gamref

    end do

  end subroutine computeDv_ThermPerfGas_TP

  subroutine computeTv_PowerLaw(grid, state)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    TYPE(t_grid), pointer :: grid
    TYPE(t_mixt), pointer :: state

    ! ... local variables
    integer :: j, i
    real(rfreal) :: ibfac

    ! ... viscosity
    if (state%RE > 0.0_rfreal) then

      do i = 1, grid%nCells

        if (state%dv(i,2) < 0.0_8) then
          if (grid%ND == 3) then
            write (*, '(3(a,d13.6))') "PlasComCM: WARNING: negative temperature at ", &
              grid%XYZ(i,1), ", ", grid%XYZ(i,2), ", ", grid%XYZ(i,3)
          else
            write (*, '(2(a,d13.6))') "PlasComCM: WARNING: negative temperature at ", &
              grid%XYZ(i,1), ", ", grid%XYZ(i,2)
          end if
        end if

        ! ... mu
        state%tv(i,1) = ((state%gv(i,1) - 1.0_rfreal)*state%dv(i,2))**0.666_rfreal

        ! ... lambda (bulk viscosity = 0.60 mu for air)
        state%tv(i,2) = (0.60_rfreal - 2.0_rfreal/3.0_rfreal) * state%tv(i,1)

        ! ... k
        state%tv(i,3) = state%tv(i,1)

      end do

    else

      do j = 1, size(state%tv,2)
        do i = 1, grid%nCells
          state%tv(i,j) = 0.0_rfreal ! for hyperviscosity & hyperdiffusivity and
                                     ! dynamic LES model, JKim 09/2007
        end do
      end do

    end if

  end subroutine computeTv_PowerLaw

  subroutine computeDv_LinIdealGas(grid, state, mean)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    TYPE(t_grid), pointer :: grid
    TYPE(t_mixt), pointer :: state, mean

    ! ... local variables
    integer :: j, i, ND
    real(rfreal) :: ibfac, gamma, up, U, RHO

    ! ... simplicity
    ND = grid%ND

    do i = 1, size(state%cv,1)

      ibfac = grid%ibfac(i)
      gamma = state%gv(i,1)

      ! ... adjust original values wrt IBLANK
      state%cv(i,   1) = ibfac * state%cv(i,1)
      state%cv(i,ND+2) = ibfac * state%cv(i,ND+2)
      do j = 1, ND
        state%cv(i,j+1) = ibfac * state%cv(i,j+1)
      end do

      ! pressure 
      state%dv(i,1) = state%cv(i,ND+2)
      RHO = mean%cv(i,1)
      do j = 1, grid%ND
        U  = mean%cv(i,1+j) / RHO
        up = ibfac * (state%cv(i,1+j) - state%cv(i,1) * U) / RHO
        state%dv(i,1) = state%dv(i,1) - 0.5_rfreal * (state%cv(i,1+j) * U + mean%cv(i,1+j) * up)
      end do
      state%dv(i,1) = ibfac * (gamma-1.0_rfreal) * state%dv(i,1)

    end do

  end subroutine computeDv_LinIdealGas

  subroutine computeTv_GnH(grid, state)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    TYPE(t_grid), pointer :: grid
    TYPE(t_mixt), pointer :: state

    ! ... local variables
    TYPE(t_mixt_input), pointer :: input
    integer :: j, i
    real(rfreal) :: ibfac, RefVisc, RefCond, gamma

    input => grid%input

    ! ... viscosity
    if (state%RE > 0.0_rfreal) then

      RefVisc = GnHVisc(input%TempRef)
      RefCond = GnHCond(input%TempRef)

      do i = 1, grid%nCells

        gamma = input%GamRef
      
        ! ... mu
        state%tv(i,1) = GnHVisc(state%dv(i,2) * ( gamma - 1.0_rfreal ) * input%TempRef ) / RefVisc

        ! ... lambda (bulk viscosity = 0.60 mu for air)
        state%tv(i,2) = (0.60_rfreal - 2.0_rfreal/3.0_rfreal) * state%tv(i,1)

        ! ... k
        state%tv(i,3) = GnHCond(state%dv(i,2) * ( gamma-1.0_rfreal ) * input%TempRef ) / RefCond

      end do

    else

      do j = 1, size(state%tv,2)
        do i = 1, grid%nCells
          state%tv(i,j) = 0.0_rfreal ! for hyperviscosity & hyperdiffusivity and
                                     ! dynamic LES model, JKim 09/2007
        end do
      end do

    end if

  end subroutine computeTv_GnH

  Real(rfreal) Function GnHVisc(T)
    
    USE ModGlobal
    USE ModDataStruct
    
    Implicit None


    REAL(rfreal) :: T

    ! ... Polynomial fit for viscosity of Air-Methane combustion products
    
    GnHVisc = 6.920966d-7 + 6.476126d-8 * T - 4.636583d-11 * T*T + &
         4.133052d-14 * T*T*T - 3.141739d-17 * T*T*T*T + 1.608260d-20 * T*T*T*T*T - &
         4.460825d-24 * T*T*T*T*T*T + 4.953660d-28 * T*T*T*T*T*T*T

  end function GnHVisc

  Real(rfreal) function GnHCond(T)
    
    USE ModGlobal
    USE ModDataStruct
    
    Implicit None


    REAL(rfreal) :: T

    ! ... Polynomial fit for thermal conductivity of Air-Methane
    ! ... combustion products
    
    ! ... piece-wise polynomial
    if(T .le. 1513.55_rfreal) then
       GnHCond = -1.925790d-2 + 2.867420d-4 * T - 7.397294d-7 * T*T + &
            1.222095d-9 * T*T*T - 1.060406d-12 * T*T*T*T + 4.585186d-16 * T*T*T*T*T - &
            7.811373d-20 * T*T*T*T*T*T
    elseif(T .gt. 1513.55_rfreal) then
       GnHCond = -3.244647d2 + 1.079170_rfreal * T - 1.506205d-3 * T*T + &
            1.138076d-6 * T*T*T - 4.987852d-10 * T*T*T*T + 1.250972d-13 * T*T*T*T*T - &
            1.620831d-17 * T*T*T*T*T*T + 7.914681d-22 * T*T*T*T*T*T*T
    end if

  end function GnHCond

  subroutine computeDv_ThermPerfGas(grid, state)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    TYPE(t_grid), pointer :: grid
    TYPE(t_mixt), pointer :: state
    TYPE(t_mixt_input), pointer :: input

    ! ... local variables
    integer :: j, i, ND
    real(rfreal) :: ibfac, gamma, intE, afit, bfit, cfit, temp, R, rhoRef, CvPerf

    ! ... simplicity
    input => grid%input
    ND = grid%ND

    ! ... air curve fit for e(T)/(Cv)_perf
    afit   = 0.0000742875_8           ! T^2
    bfit   = 1.02429_8                ! T
    cfit   = -21.6559_8               ! 1
    R      = 286.9_8                  ! specific gas constant
    CvPerf = R/(input%GamRef - 1.0_8) ! specific heat at constant volume
    rhoRef = input%PresRef * input%GamRef / (input%SndSpdRef * input%SndSpdRef)

    do i = 1, size(state%cv,1)

      ibfac = grid%ibfac(i)

      ! ... adjust original values wrt IBLANK
      state%cv(i,   1) = ibfac * state%cv(i,1) + (1.0_8 - ibfac)
      state%cv(i,ND+2) = ibfac * state%cv(i,ND+2) + (1.0_8 - ibfac) / (input%GamRef - 1.0_8)
      do j = 1, ND
        state%cv(i,j+1) = ibfac * state%cv(i,j+1)
      end do

      ! specific volume
      state%dv(i,3) = 1.0_8 / state%cv(i,1)

      ! ... compute internal energy (rho E)
      state%dv(i,1) = state%cv(i,ND+2)
      do j = 1, ND
        state%dv(i,1) = state%dv(i,1) - 0.5_rfreal * state%cv(i,1+j) * state%cv(i,1+j) * state%dv(i,3)
      end do

      ! ... compute dimensional internal energy
      intE = state%dv(i,1) * state%dv(i,3) * input%SndSpdRef * input%SndSpdRef

      ! ... compute dimensional temperature from curve fit
      temp = (-bfit + dsqrt(bfit*bfit - 4.0*afit*(cfit - intE/CvPerf))) / (2.0_8 * afit)

      ! ... non-dimensionalize temp
      state%dv(i,2) = temp / (input%TempRef * (input%GamRef-1.0_8))

      ! ... pressure
      state%dv(i,1) = (state%cv(i,1) * rhoRef * R * temp) / (rhoRef * input%SndSpdRef * input%SndSpdRef)

    end do

  end subroutine computeDv_ThermPerfGas


  Subroutine TPGasLookup(myrank, input, varout, valin, valout, gamma)
    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    integer :: myrank
    type(t_mixt_input), pointer :: input
    real(rfreal) :: valin, valout, gamma
    real(rfreal) :: in1, in2, out1, out2, gamma1, gamma2, diff, din
    real(rfreal) :: ratio, guess
    integer :: l1, dl, numits, varin, varout, nEntries

    ! ... temporary
    if(varout == 1) then
      varin = 2 ! ... temperature
    elseif(varout == 2) then
      varin = 1 ! ... energy
    else
      call graceful_exit(myrank, 'TPGasLookup: Wrong variable out...exiting')
    end if

    ! ... make initial guess
    guess = 0.0_rfreal
    guess = input%TPCoeffs(1,varin) * valin + input%TPCoeffs(2,varin)

    ! ... convert guess into integers
    l1 = floor(guess)

    ! ... keep below size of table
    if (l1 .ge. input%nTPTableEntries) then
      gamma = input%TPTable(input%nTPTableEntries,3,varin)
      valout = input%TPTable(input%nTPTableEntries,varout,varin)
      return
    end if

    ! ... access lookup table for regularly spaced varin
    in1 = input%TPTable(l1,varin,varin)
    in2 = input%TPTable(l1+1,varin,varin)
    diff = valin - in1
    numits = 0

    ! ... Newton-Rhapson iteration to find l1
    do while (in1 .gt. valin .or. in2 .lt. valin)
      din = in2 - in1
      dl = floor(diff/din)
      l1 = l1 + dl

      ! ... keep below size of table
      if (l1 .ge. input%nTPTableEntries) then
        gamma = input%TPTable(input%nTPTableEntries,3,varin)
        valout = input%TPTable(input%nTPTableEntries,varout,varin)
        return
      end if

      in1 = input%TPTable(l1,varin,varin)
      in2 = input%TPTable(l1+1,varin,varin)
      diff = valin - in1
      numits = numits + 1
    end do

    ! ... found the bounding indices, now find Temperature, gamma
    out2 = input%TPTable(l1+1,varout,varin)
    out1 = input%TPTable(l1,varout,varin)
    gamma2 = input%TPTable(l1+1,3,varin)
    gamma1 = input%TPTable(l1,3,varin)

    ratio = (valin - in1)/(in2 - in1)
    valout = ratio * (out2 - out1) + out1
    gamma = ratio * (gamma2 - gamma1) + gamma1

  end Subroutine TPGasLookup
        
  subroutine computeTv_highTgasmodel_air(grid, state) ! Valid from 0-5000K. The curve fits were actually from 123-5000K. 
                                                      ! Since the data for 0-123K was not obtained, the same curve fit is 
                                                      ! used. 
  
    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    TYPE(t_grid), pointer :: grid
    TYPE(t_mixt), pointer :: state

    ! ... local variables
    integer :: j, i
    real(rfreal) :: ibfac, RefVisc, RefCond, gamma, dim_temp, dim_temp2, dim_temp3, dim_temp4, dim_temp5, dim_temp6, temp_factor
    TYPE(t_mixt_input), pointer :: input
  
    !Coefficients for the curve fit for viscosity and thermal conductivity

    real(rfreal) :: mu_6, mu_5, mu_4, mu_3, mu_2, mu_1, mu_0, fix_temp, fix_mu
    real(rfreal) :: k_6, k_5, k_4, k_3, k_2, k_1, k_0, k_suth, k_highT, const_alpha, temp_check,temp_check_1,  therm_cond, mu_val

    input => grid%input

    mu_6 = -5.516905244686616D-8 
    mu_5 = 9.607185470285190D-7
    mu_4 = -6.658923511643908D-6
    mu_3 = 2.366336091915009D-5
    mu_2 = -4.755867997774466D-5
    mu_1 =  7.056829737570925D-5
    mu_0 =  8.885769926184669D-07
        

    k_6 = -5.669532238888905D-4
    k_5 = 5.734492765916559D-3
    k_4 = -1.846631993745979D-2
    k_3 = 2.614033933441077D-2
    k_2 =  -3.015908137923779D-2
    k_1 = 7.700390615016030D-2
    k_0 = 3.997966971693797D-03

    temp_factor = input%TempRef**0.5/(1+112_rfreal/input%TempRef) 

    RefVisc = 1.462_rfreal*1D-6*temp_factor
    RefCond = 1.364_rfreal*1.462_rfreal*1D-3*temp_factor

    ! ... viscosity
    if (state%RE > 0.0_rfreal) then
      
      do i = 1, grid%nCells

        gamma = input%GamRef
        
       ! dimensionalize the temperature for calculation of the coefficients of viscosity and 
       ! thermal conductivity
      
        dim_temp = state%dv(i,2)*(gamma-1_rfreal)*input%TempRef
        
        dim_temp = dim_temp/1000_rfreal 

        dim_temp2 = dim_temp*dim_temp
        dim_temp3 = dim_temp2*dim_temp
        dim_temp4 = dim_temp3*dim_temp  
        dim_temp5 = dim_temp4*dim_temp
        dim_temp6 = dim_temp5*dim_temp 
    
        ! ... mu        
        state%tv(i,1) = (mu_6*dim_temp6 + mu_5*dim_temp5 + mu_4*dim_temp4 + mu_3*dim_temp3 + &
                         mu_2*dim_temp2 + mu_1*dim_temp + mu_0)/ RefVisc       
        

        ! ... lambda (bulk viscosity = 0.60 mu for air)
        state%tv(i,2) = (0.60_rfreal - 2.0_rfreal/3.0_rfreal) * state%tv(i,1)

        ! ... k
        state%tv(i,3) = (k_6*dim_temp6 + k_5*dim_temp5 + k_4*dim_temp4 + k_3*dim_temp3 + &
                         k_2*dim_temp2 + k_1*dim_temp + k_0) / RefCond
         
      end do

     
    else

      do j = 1, size(state%tv,2)
        do i = 1, grid%nCells
          state%tv(i,j) = 0.0_rfreal ! for hyperviscosity & hyperdiffusivity and
                                     ! dynamic LES model, JKim 09/2007
        end do
      end do

    end if

  end subroutine computeTv_highTgasmodel_air

  subroutine computeTv(grid, state)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    TYPE(t_grid), pointer :: grid
    TYPE(t_mixt), pointer :: state
    TYPE(t_mixt_input), pointer :: input

    input => grid%input

    Select Case (input%gas_tv_model)
    Case (TV_MODEL_POWERLAW)
      call computeTv_PowerLaw(grid, state)
    Case (TV_MODEL_GLASS_AND_HUNT)
      call computeTv_GnH(grid, state)
    Case (TV_MODEL_LOOKUP_TABLE)
      call computeTv_Spline(grid, state)
    Case (TV_MODEL_EXTERNAL)
       call computeTv_External(grid, state)
    End Select

    Return

  end subroutine computeTv

  subroutine computeDv(myrank, grid, state)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    integer :: myrank
    TYPE(t_grid), pointer :: grid
    TYPE(t_mixt), pointer :: state
    TYPE(t_mixt_input), pointer :: input

    input => grid%input

    Select Case (input%gas_dv_model)
    Case (DV_MODEL_IDEALGAS)
      call computeDv_IdealGas(grid, state)
    Case (DV_MODEL_IDEALGAS_MIXTURE)
      call computeDv_IdealGas_Mixture(myrank, grid, state)
    Case (DV_MODEL_THERMALLY_PERFECT)
      call computeDv_ThermPerfGas_TP_Spline(grid, state)
    End Select

    Return

  end subroutine computeDv

  function evaluateSpline(nxb, dxb, xb, coeffs, nvar, x)

    Use ModGlobal
    Implicit None

    ! ... global variables
    Integer :: nxb, nvar
    Real(RFREAL) :: dxb, x, evaluateSpline, xx
    Real(RFREAL), Pointer :: xb(:), coeffs(:,:,:)

    ! ... local variables
    Integer :: index, k
    Real(RFREAL) :: val

    ! ... find lower bound of breakpoint interval
    index = floor((x-xb(1))/dxb)+1

    ! ... local coordinate
    xx = x - xb(index)

    ! evaluate spline
    val = coeffs(index,1,nvar)
    do k = 2, 4
      val = val * xx + coeffs(index,k,nvar)
    end do

    evaluateSpline = val

  end function evaluateSpline

  subroutine computeDv_ThermPerfGas_TP_Spline(grid, state)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    TYPE(t_grid), pointer :: grid
    TYPE(t_mixt), pointer :: state

    ! ... local variables
    integer :: j, i, ND
    real(rfreal) :: ibfac, gamma, e, Temp, gamref, Z, sndspdref2, invtempref
    TYPE(t_mixt_input), pointer :: input
    TYPE(t_spline), pointer :: spline

    ! ... simplicity
    input => grid%input
    ND = grid%ND
    gamref = input%GamRef
    sndspdref2 = input%sndspdref * input%sndspdref
    invtempref = input%Cpref / sndspdref2

    do i = 1, size(state%cv,1)

      ibfac = grid%ibfac(i)
      gamma = state%gv(i,1)

      ! ... adjust original values wrt IBLANK
      state%cv(i,   1) = ibfac * state%cv(i,1) + (1.0_8 - ibfac)
      state%cv(i,ND+2) = ibfac * state%cv(i,ND+2) + (1.0_8 - ibfac) / (gamma - 1.0_8)
      do j = 1, ND
        state%cv(i,j+1) = ibfac * state%cv(i,j+1)
      end do

      ! specific volume
      state%dv(i,3) = 1.0_8 / state%cv(i,1)

      ! ... internal energy
      e = state%cv(i,ND+2)
      do j = 1, ND
        e = e - 0.5_rfreal * state%cv(i,j+1)*state%cv(i,j+1)*state%dv(i,3)
      end do
      e = e * state%dv(i,3) * sndspdref2

      ! if (e < 0.0_8) STOP 'PlasComCM: ERROR: internal energy negative.'
  
      ! ... obtain the temperature from the T(eint) lookup
      spline => state%dvSpline(2)
      Temp  = evaluateSpline(spline%nxb, spline%dxb, spline%xb, spline%coeffs, 1, e)

      ! ... use temperature to get gamma and compressibility factor
      spline => state%dvSpline(1)
      gamma = evaluateSpline(spline%nxb, spline%dxb, spline%xb, spline%coeffs, 4, Temp)
      Z     = evaluateSpline(spline%nxb, spline%dxb, spline%xb, spline%coeffs, 5, Temp)

      ! ... ratio of specific heats
      state%gv(i,1) = gamma

      ! ... temperature
      Temp = Temp * invtempref
      state%dv(i,2) = Temp     

      ! ... pressure 
      state%dv(i,1) =  Z * Temp * (gamref - 1.0_rfreal) * state%cv(i,1) / gamref

    end do

  end subroutine computeDv_ThermPerfGas_TP_Spline

  subroutine computeTv_External(grid, state)
  
    USE ModGlobal
    USE ModDataStruct
    USE ModCombustion
    USE ModMPI
    Implicit None
    TYPE(t_grid), pointer :: grid
    TYPE(t_mixt), pointer :: state

    Call combustionTv(grid, state)

  end subroutine computeTv_External

  subroutine computeTv_Spline(grid, state)
  
    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    TYPE(t_grid), pointer :: grid
    TYPE(t_mixt), pointer :: state

    ! ... local variables
    integer :: j, i
    real(rfreal) :: gamma, temp, tempref, muref, kref
    TYPE(t_mixt_input), pointer :: input
    TYPE(t_spline), pointer :: tvSpline
  
    !Coefficients for the curve fit for viscosity and thermal conductivity

    real(rfreal) :: mu_6, mu_5, mu_4, mu_3, mu_2, mu_1, mu_0, fix_temp, fix_mu
    real(rfreal) :: k_6, k_5, k_4, k_3, k_2, k_1, k_0, k_suth, k_highT, const_alpha, temp_check,temp_check_1,  therm_cond, mu_val

    input => grid%input
    tvSpline => state%tvSpline
    tempref = input%tempref
    muref   = input%muref
    kref    = input%kapparef

    ! ... viscosity
    if (state%RE > 0.0_rfreal) then
  
      do i = 1, grid%nCells

        ! ... dimensional temperature
        temp = state%dv(i,2) * tempref

        ! ... mu        
        state%tv(i,1) = evaluateSpline(tvSpline%nxb,tvSpline%dxb,tvSpline%xb,tvSpline%coeffs,1,temp) / muref

        ! ... lambda 
        state%tv(i,2) = evaluateSpline(tvSpline%nxb,tvSpline%dxb,tvSpline%xb,tvSpline%coeffs,2,temp) / muref

        ! ... k
        state%tv(i,3) = evaluateSpline(tvSpline%nxb,tvSpline%dxb,tvSpline%xb,tvSpline%coeffs,3,temp) / kref
         
      end do

     
    else

      do j = 1, size(state%tv,2)
        do i = 1, grid%nCells
          state%tv(i,j) = 0.0_rfreal ! for hyperviscosity & hyperdiffusivity and
                                     ! dynamic LES model, JKim 09/2007
        end do
      end do

    end if

  end subroutine computeTv_Spline

  subroutine computeDv_IdealGas_Mixture(myrank, grid, state)
    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    USE ModModel0
    USE ModModel1

    Implicit None

    integer :: myrank
    TYPE(t_grid), pointer :: grid
    TYPE(t_mixt), pointer :: state
    TYPE(t_mixt_input), pointer :: input
    TYPE(t_combustion), pointer :: combustion

    ! ... local variables
    integer :: j, i, ND, Nc
    real(rfreal) :: ibfac, gamma, gamref, tmp,tmp2, MwcoeffRho
    real(rfreal),pointer :: Mwcoeff(:)

    ! Mixture of Calorically perfect gas:: assume cp/R is constant among species

    ! ... simplicity
    ND = grid%ND
    Nc = grid%nCells
    input => grid%input
    gamref = input%GamRef
    combustion => state%combustion

    ! Get the molecular weights
    select case (input%chemistry_model)
    case (MODEL0)
       call Model0Dv(combustion%model0, grid, state, myrank)
       return
    case (MODEL1)
       call Model1Dv(combustion%model1, grid, state, myrank)
       return
    end select

    call graceful_exit(myrank, "ERROR: DV_MODEL_IDEALGAS_MIXTURE currently only works " // &
      "with Model0 and Model1 chemistry models.")

!   This code needs fixing (Mwcoeff and MwcoeffRho are used but never defined)

!     do i = 1, Nc

!       ibfac = grid%ibfac(i)

!       ! ... adjust original values wrt IBLANK
!       state%cv(i,   1) = ibfac * state%cv(i,1) + (1.0_8 - ibfac)
!       state%cv(i,ND+2) = ibfac * state%cv(i,ND+2) + (1.0_8 - ibfac) / (gamref - 1.0_8)
!       do j = 1, ND
!         state%cv(i,j+1) = ibfac * state%cv(i,j+1)
!       end do

!       ! specific volume
!       state%dv(i,3) = 1.0_8 / state%cv(i,1)

!       ! pressure
!       state%dv(i,1) = state%cv(i,ND+2)
!       do j = 1, grid%ND
!         state%dv(i,1) = state%dv(i,1) - 0.5_rfreal * state%cv(i,1+j) * state%cv(i,1+j) * state%dv(i,3)
!       end do
!       tmp = state%dv(i,1)
!       state%dv(i,1) = (gamref-1.0_rfreal) * tmp

!       ! temperature
!       tmp2 = sum(state%auxVars(i,:)*Mwcoeff) + MwcoeffRho*state%cv(i,1) ! (Luca's implementation)
!       !tmp2 = state%auxVars(i,1)*(Cp1-Cpref) + state%cv(i,1)*Cpref ! Jesse's, function of Cp, only for 1 aux var
!       state%dv(i,2) = gamref * tmp/tmp2
!     end do

!     do j = 1, input%nAuxVars
!       do i = 1, Nc
!         ibfac = grid%ibfac(i)
!         state%auxVars(i,j) = state%auxVars(i,j) * ibfac
!       end do
!    end do

  end subroutine computeDv_IdealGas_Mixture

End Module ModEOS
