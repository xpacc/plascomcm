! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
!-----------------------------------------------------------------------
!
! ModFTMCoupling.f90
!
! - code containing coupling routines between fluid that structural domains
!
! Revision history
! - 18 April 2012 : CMO : initial code
!
!-----------------------------------------------------------------------
Module ModFTMCoupling

CONTAINS

  subroutine SetUpFTMCoupling(region)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    Type (t_region), Pointer :: region

    ! ... local variables
    Type(t_sgrid), Pointer :: TMgrid
    Type(t_grid), Pointer :: grid
    Type(t_spatch), Pointer :: TMpatch
    Type(t_patch), Pointer :: patch
    Type(t_mixt_input), Pointer :: input
    Type(t_fsi), Pointer :: ipatch

    Integer, pointer :: NeighTmp(:)
    Integer :: npatch, gridID
    Integer :: cnt, nbc, ND
    Integer :: i, j, k, dir, p, l0, is, ie
    Integer :: patchLenGlb, patchLenLoc, OverLap, e1, e2, Offset, nPtsPatch

    ! ... mpi
    Integer, pointer :: rcvbuf(:,:), sndbuf(:), req(:), stat(:,:)
    Integer :: rank, npiC, ipatchColor, FluidPatchColor

    ! ... Fluid processes (all processes): count number of interacting patches
    cnt = 0

    ! ... simplicity
    input => region%input
    nbc = size(input%bc_data,1)
    ND = input%ND

    do i = 1, nbc
       if(input%bc_data(i,2) == STRUCTURAL_INTERACTING) cnt = cnt + 1
    end do

    region%nInteractingPatches = cnt
    cnt = 0
    ! ... check that the solid and fluid domain inputs are consistent
    do i = 1, size(input%TMbc_data,1)
       if(input%TMbc_data(i,2) == STRUCTURAL_INTERACTING) cnt = cnt + 1
    end do

    if(cnt /= region%nInteractingPatches .and. region%TMColor == TRUE) &
         call graceful_exit(region%myrank, 'PlasComCM: bc.dat and TMbc.dat are inconsistent. Exiting...')

    ! ... create data structure for each interacting patch
    allocate(region%ipatch(region%nInteractingPatches))

    ! ... set ipatchColor = 0, will be set to ipatch number if this process contains that patch
    do i = 1, region%nInteractingPatches
       region%ipatch(i)%ipatchColor = 0
       ! ... -1 flags that this process does not own part of the fluid side of the patch
       region%ipatch(i)%patchID = -1
       ! ... -1 flags that this process does not own part of the solid side of the patch
       region%ipatch(i)%TMpatchID = -1
    end do

    ! ... now loop through the patches in the fluid domain and ID interacting patches
    do npatch = 1, region%nPatches

       ! ... simplicity
       patch => region%patch(npatch)

       cnt = 0
       do i = 1, nbc
          if(input%bc_data(i,2) == STRUCTURAL_INTERACTING) then
             cnt = cnt + 1
             if(patch%iPatchGlobal == i) then
                region%ipatch(cnt)%ipatchColor = cnt
                region%ipatch(cnt)%patchID = npatch

                region%ipatch(cnt)%gridID = patch%gridID
             end if
          end if
       end do ! ... nbc

    end do ! ... npatch

    nbc = size(input%TMbc_data,1)

    ! ... now loop through the patches in the solid domain and ID interacting patches
    do npatch = 1, region%nTMPatches

       ! ... simplicity
       TMpatch => region%TMpatch(npatch)

       cnt = 0
       do i = 1, nbc
          if(input%TMbc_data(i,2) == STRUCTURAL_INTERACTING) then
             cnt = cnt + 1
             if(TMpatch%iPatchGlobal == i) then
                region%ipatch(cnt)%ipatchColor = cnt
                region%ipatch(cnt)%TMpatchID = npatch
                region%ipatch(cnt)%TMgridID = TMpatch%gridID
             end if
          end if
       end do ! ... nbc

    end do ! ... npatch

    ! ... now everyone knows whether or not they have an interacting patch
    ! ... set up communication
    do i = 1, region%nInteractingPatches

       ! ... simplicity
       ipatch => region%ipatch(i)
       ipatchColor = ipatch%ipatchColor

       ! ... create the interacting patch communicator
       Call MPI_COMM_SPLIT(mycomm,ipatchColor, region%myrank, ipatch%Comm, ierr)

       if(ipatchColor == i) then


          Call MPI_COMM_SIZE(ipatch%Comm, ipatch%numproc_inComm, ierr)
          Call MPI_COMM_RANK(ipatch%Comm, ipatch%rank, ierr)

          rank = ipatch%rank
          npiC = ipatch%numproc_inComm

          ! ... also, create patch communicator on fluid side
          ! ... needed to calculate patch average pressure
          FluidPatchColor = 0
          if(ipatch%patchID /= -1) FluidPatchColor = 1
          
          Call MPI_COMM_SPLIT(ipatch%Comm, FluidPatchColor, rank, ipatch%fluid_Comm, ierr)

          ! ... alloacate space to hold each process' extents along the patch
          Allocate(ipatch%FluidExt(npiC, 2*MAX_ND))
          Allocate(ipatch%SolidExt(npiC, 2*MAX_ND))

          ! ... initialize
          ipatch%FluidExt(:,:) = -1
          ipatch%SolidExt(:,:) = -1

          ! ... simplicity
          if(ipatch%patchID /= -1) then
             patch => region%patch(ipatch%patchID)
             grid  => region%grid(ipatch%gridID)

             do dir = 1, ND

                patchLenGlb = patch%global_patch_extent((dir-1)*2 + 2) - patch%global_patch_extent((dir-1)*2 + 1) + 1
                ipatch%FluidExt(rank+1,(dir-1)*2 + 1) = grid%is(dir) - patch%global_patch_extent((dir-1)*2 + 1) + 1
                ! ... if grid starts before patch, adjust
                ipatch%FluidExt(rank+1,(dir-1)*2 + 1) = max(ipatch%FluidExt(rank+1,(dir-1)*2 + 1),1)
                ipatch%FluidExt(rank+1,(dir-1)*2 + 2) = grid%ie(dir) - patch%global_patch_extent((dir-1)*2 + 1) + 1
                ! ... if grid ends after patch, adjust
                ipatch%FluidExt(rank+1,(dir-1)*2 + 2) = min(ipatch%FluidExt(rank+1,(dir-1)*2 + 2),patchLenGlb)

             end do

             if(ND == 2) then
                ipatch%FluidExt(rank+1,5) = 1
                ipatch%FluidExt(rank+1,6) = 1
             end if

          else
             ipatch%FluidExt(rank+1,:) = -1
          end if

          if(ipatch%TMpatchID /= -1) then

             ! ... simplicity
             TMpatch => region%TMpatch(ipatch%TMpatchID)
             TMgrid  => region%TMgrid(ipatch%TMgridID)
             
             do dir = 1, ND
                Offset = 0
                ! ... offset so that we are not doing extra communication of local nodes that are not owned nodes
                !if(TMgrid%cartCoords(dir) > 0) Offset = 1
                patchLenGlb = TMpatch%global_patch_extent((dir-1)*2 + 2) - TMpatch%global_patch_extent((dir-1)*2 + 1) + 1
                ipatch%SolidExt(rank+1,(dir-1)*2 + 1) = TMgrid%is(dir) + Offset - TMpatch%global_patch_extent((dir-1)*2 + 1) + 1
                ! ... if grid starts before patch, adjust
                ipatch%SolidExt(rank+1,(dir-1)*2 + 1) = max(ipatch%SolidExt(rank+1,(dir-1)*2 + 1),1)
                ipatch%SolidExt(rank+1,(dir-1)*2 + 2) = TMgrid%ie(dir) - TMpatch%global_patch_extent((dir-1)*2 + 1) + 1
                ! ... if grid ends after patch, adjust
                ipatch%SolidExt(rank+1,(dir-1)*2 + 2) = min(ipatch%SolidExt(rank+1,(dir-1)*2 + 2),patchLenGlb)

             end do

             if(ND == 2) then
                ipatch%SolidExt(rank+1,5) = 1
                ipatch%SolidExt(rank+1,6) = 1
             end if

          else
             ipatch%SolidExt(rank+1,:) = -1
          end if

          ! ... now communicate the data
          Allocate(sndbuf(2*MAX_ND),rcvbuf(2*MAX_ND,npiC))
          Allocate(req(npiC*2),stat(MPI_STATUS_SIZE,npiC*2))

          ! ... initialize
          do j = 1, 2*MAX_ND
             sndbuf(j) = 0
             do p = 1, npiC
                rcvbuf(j,p) = 0
             end do
          end do
          do j = 1, 2*npiC
             req(j) = 0
             do k = 1, MPI_STATUS_SIZE
                stat(k,j) = 0
             end do
          end do

          ! ... send fluid patch extents
          do j = 1, 2*MAX_ND
             sndbuf(j) = ipatch%FluidExt(rank+1,j)
          end do


          ! ... now send the stuff
          ! ... post the receives
          do p = 0, npiC - 1
             Call MPI_IRECV(rcvbuf(1,p+1),2*MAX_ND,MPI_INTEGER,p,0,ipatch%Comm,req(p+1),ierr)
          end do
          ! ... now the sends
          do p = 0, npiC - 1
             Call MPI_ISEND(sndbuf,2*MAX_ND,MPI_INTEGER,p,0,ipatch%Comm,req(npiC+p+1),ierr)
          end do

          Call MPI_WAITALL(npiC*2,req,stat,ierr)

          ! ... store the fluid data
          do p = 1, npiC
             do j = 1, 2*MAX_ND
                ipatch%FluidExt(p,j) = rcvbuf(j,p)
             end do
          end do

          ! ... initialize
          do j = 1, 2*MAX_ND
             sndbuf(j) = 0
             do p = 1, npiC
                rcvbuf(j,p) = 0
             end do
          end do
          do j = 1, 2*npiC
             req(j) = 0
             do k = 1, MPI_STATUS_SIZE
                stat(k,j) = 0
             end do
          end do

          ! ... send solid patch extents
          do j = 1, 2*MAX_ND
             sndbuf(j) = ipatch%SolidExt(rank+1,j)
          end do

          ! ... now send the stuff
          ! ... post the receives
          do p = 0, npiC - 1

             Call MPI_IRECV(rcvbuf(1,p+1),2*MAX_ND,MPI_INTEGER,p,0,ipatch%Comm,req(p+1),ierr)

          end do

          ! ... now the sends
          do p = 0, npiC - 1

             Call MPI_ISEND(sndbuf,2*MAX_ND,MPI_INTEGER,p,0,ipatch%Comm,req(npiC+p+1),ierr)

          end do

          Call MPI_WAITALL(npiC*2,req,stat,ierr)

          ! ... store the solid data
          do p = 1, npiC
             do j = 1, 2*MAX_ND
                ipatch%SolidExt(p,j) = rcvbuf(j,p)
             end do
          end do

          ! ... fluid processes find solid neighbors and shared patch extents
          ! ... temporary array recording solid neighbors
          Allocate(NeighTmp(npiC))
          cnt = 0
          do p = 1, npiC
             ! ... check whether this process has a solid grid on this patch
             if(ipatch%SolidExt(p,1) == -1) cycle
             Overlap = TRUE
             do dir  = 1, ND

                is = ipatch%FluidExt(rank+1,(dir-1)*2 + 1)
                ie = ipatch%FluidExt(rank+1,(dir-1)*2 + 2)

                if(ipatch%SolidExt(p,(dir-1)*2 + 1) > ie .or. ipatch%SolidExt(p,(dir-1)*2 + 2) < is) Overlap = FALSE
             end do

             ! ... record rank of neighboring process
             if(Overlap == TRUE) then
                cnt = cnt + 1
                NeighTmp(cnt) = p-1
             end if

          end do

          Allocate(ipatch%SolidNeighbor(cnt))
          Allocate(ipatch%SolidNeighExt(cnt,MAX_ND*2))

          do k = 1, cnt
             ipatch%SolidNeighbor(k) = NeighTmp(k)
             NeighTmp(k) = -1
          end do

          ipatch%nSolidNeighbor = cnt

          do k = 1, ipatch%nSolidNeighbor

             p = ipatch%SolidNeighbor(k) + 1

             do dir = 1, ND

                is = ipatch%FluidExt(rank+1,(dir-1)*2 + 1)
                ie = ipatch%FluidExt(rank+1,(dir-1)*2 + 2)

                ! ... local patch length                
                patchLenLoc = ie - is + 1

                ! ... distance between solid neighbor's first index on this patch and this processes
                e1 = ipatch%SolidExt(p,(dir-1)*2 + 1) - is
                ! ... distance between solid neighbor's last index on this patch and this processes first
                e2 = ipatch%SolidExt(p,(dir-1)*2 + 2) - is

                if(e1 >= 0) then
                   ! ... the solid neighbor starts within this processes part of the patch
                   ipatch%SolidNeighExt(k,(dir-1)*2 + 1) = e1 + 1
                elseif(e1 < 0) then
                   ! ... the solid neighbor starts before this processes part of the patch
                   ipatch%SolidNeighExt(k,(dir-1)*2 + 1) = 1
                end if

                if(e2 >= 0) then
                   ! ... the solid neighbor should end within or past this processes patch
                   ipatch%SolidNeighExt(k,(dir-1)*2 + 2) = min(e2 + 1, patchLenLoc)
                else
                   write(*,'(A,I3,A,I3)') ' PlasComCM: ERROR: solid rank ', p-1,&
                        ' does not share patch with fluid rank ', rank
                end if

             end do ! ... ND

             if(ND == 2) then
                ipatch%SolidNeighExt(k,5) = 1
                ipatch%SolidNeighExt(k,6) = 1
             end if

          end do ! ... k = 1, nSolidNeighbor

          ! ... solid processes find fluid neighbors and shared patch extents

          cnt = 0
          do p = 1, npiC
             ! ... check whether this process has a solid grid on this patch
             if(ipatch%FluidExt(p,1) == -1) cycle
             Overlap = TRUE
             do dir  = 1, ND
                is = ipatch%SolidExt(rank+1,(dir-1)*2 + 1)
                ie = ipatch%SolidExt(rank+1,(dir-1)*2 + 2)

                if(ipatch%FluidExt(p,(dir-1)*2 + 1) > ie .or. ipatch%FluidExt(p,(dir-1)*2 + 2) < is) Overlap = FALSE

             end do

             ! ... record rank of neighboring process
             if(Overlap == TRUE) then
                cnt = cnt + 1
                NeighTmp(cnt) = p-1
             end if

          end do

          Allocate(ipatch%FluidNeighbor(cnt))
          Allocate(ipatch%FluidNeighExt(cnt,MAX_ND*2))

          do k = 1, cnt
             ipatch%FluidNeighbor(k) = NeighTmp(k)
          end do

          deallocate(NeighTmp)

          ipatch%nFluidNeighbor = cnt

          do k = 1, ipatch%nFluidNeighbor

             p = ipatch%FluidNeighbor(k) + 1

             do dir = 1, ND

                is = ipatch%SolidExt(rank+1,(dir-1)*2 + 1)
                ie = ipatch%SolidExt(rank+1,(dir-1)*2 + 2)

                ! ... local patch length                
                patchLenLoc = ie - is + 1

                ! ... distance between solid neighbor's first index on this patch and this processes
                e1 = ipatch%FluidExt(p,(dir-1)*2 + 1) - is
                ! ... distance between solid neighbor's last index on this patch and this processes first
                e2 = ipatch%FluidExt(p,(dir-1)*2 + 2) - is

                if(e1 >= 0) then
                   ! ... the solid neighbor starts within this processes part of the patch
                   ipatch%FluidNeighExt(k,(dir-1)*2 + 1) = e1 + 1
                elseif(e1 < 0) then
                   ! ... the solid neighbor starts before this processes part of the patch
                   ipatch%FluidNeighExt(k,(dir-1)*2 + 1) = 1
                end if

                if(e2 >= 0) then
                   ! ... the solid neighbor should end within or past this processes patch
                   ipatch%FluidNeighExt(k,(dir-1)*2 + 2) = min(e2 + 1, patchLenLoc)
                else
                   write(*,'(A,I3,A,I3)') ' PlasComCM: ERROR: fluid rank ', p-1,&
                        ' does not share patch with solid rank ', rank
                end if

             end do ! ... dir = 1, ND
             if(ND == 2) then
                ipatch%FluidNeighExt(k,5) = 1
                ipatch%FluidNeighExt(k,6) = 1
             end if

             ! ... allocate TMpatch space for loads from fluid
             nPtsPatch = 1
             do j = 1, region%input%ND
                nPtsPatch = nPtsPatch*(TMpatch%ie(j) - TMpatch%is(j) + 1)
             end do

             if(input%TMSolve > 1) then
                allocate(TMpatch%FluidStressTns(nPtsPatch,3+(ND-2)*3))
                TMpatch%FluidStressTns(:,:) = 0.0_rfreal
             end if
             if(input%TMSolve == 1 .or. input%TMSolve == 3) then
                allocate(TMpatch%FluidHtFlux(nPtsPatch))
                TMpatch%FluidHtFlux(:) = 0.0_rfreal
             end if

          end do ! ... k = 1, nFluidNeighbor


       end if ! ... ipatchColor == i

       Call MPI_BARRIER(ipatch%Comm, ierr)

    end do ! ... i = 1, region%nInteractingPatches

!     ! ... output
!     do npatch = 1, region%nInteractingPatches
!        ipatch => region%ipatch(npatch)
!        if(ipatch%ipatchColor == npatch) then
!           rank = ipatch%rank
!           npiC = ipatch%numproc_inComm
!           do k = 1, npiC
!              if(rank+1 == k) then
!                 do p = 1, npiC
!                    write(*,'(2(A,I3),(A,6(I3)))') 'myrank = ',rank,', rank = ',p-1,', FluidExt, ',(ipatch%FluidExt(p,j),j=1,6)
!                    write(*,'(2(A,I3),(A,6(I3)))') 'myrank = ',rank,', rank = ',p-1,', SolidExt, ',(ipatch%SolidExt(p,j),j=1,6)
!                 end do
!                 do p = 1, ipatch%nFluidNeighbor
!                    write(*,'(2(A,I3),(A,6(I3)))') 'myrank = ',rank,', rank = ',ipatch%FluidNeighbor(p),&
!                         ' FluidNeighExt ',(ipatch%FluidNeighExt(p,j),j=1,6)
!                 end do
!                 do p = 1, ipatch%nSolidNeighbor
!                    write(*,'(2(A,I3),(A,6(I3)))') 'myrank = ',rank,', rank = ',ipatch%SolidNeighbor(p),&
!                         ' SolidNeighExt ',(ipatch%SolidNeighExt(p,j),j=1,6)
!                 end do
!              end if
!              call MPI_BARRIER(ipatch%Comm,ierr)
!           end do
!        end if
!     end do

  end subroutine SetUpFTMCoupling

  Subroutine FTMExchange(region, ThermalStep, StructuralStep, exchangeDir)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    USE ModNavierStokesRHS
    USE ModTMIO

    Implicit None

    Type(t_region), pointer :: region
    Integer :: ThermalStep, StructuralStep, exchangeDir

    ! ... local variables
    Type(t_mixt_input), pointer :: input
    Type(t_mixt),       pointer :: state
    Type(t_grid),       pointer :: grid
    Type(t_patch),      pointer :: patch
    Type(t_smixt),      pointer :: TMstate
    Type(t_sgrid),      pointer :: TMgrid
    Type(t_spatch),     pointer :: TMpatch
    Type(t_fsi), pointer        :: ipatch

    Real(rfreal) :: Strn1, Strn2, Jcbn, Diag

    Integer, allocatable :: nPtsFluid(:),nPtsSolid(:)
    Integer :: npiC, rank
    Integer :: patchID, gridID, TMpatchID, TMgridID
    Integer :: i, j, k, npatch, m, p, l, lp, l0
    Integer :: is, ie, dir
    Integer :: ND,N(MAX_ND),Np(MAX_ND),Np1(MAX_ND), alloc_stat(2), Nc
    Integer :: nVar2Pass, Offset, nPtsPatch, nipatch

    ! ... MPI
    Real(rfreal), pointer :: sndbuf(:,:), rcvbuf(:,:)
    Integer, pointer :: reqsnd(:), reqrcv(:), statsnd(:,:), statrcv(:,:)

    ! Simplicity
    Integer :: main_ts_loop_index
    main_ts_loop_index = region%global%main_ts_loop_index

    ND = region%input%ND

    if(exchangeDir == SOLID2FLUID) then
       if(region%TMColor == TRUE) then
          ! ... interpolate solid data to plot3D grid points in solid domain
          Call FillP3D(region)
          if(region%input%StructuralTimeScheme == DYNAMIC_SOLN) Call FillP3DRate(region)
       end if
    elseif(exchangeDir == FLUID2SOLID) then
       ! ... do collective operations over all processors
       do gridID = 1, region%nGrids
          grid    => region%grid(gridID)
          state   => region%state(gridID)
          input => grid%input
          Nc = grid%nCells


          ! ... allocate space for arrays
          if(.not.associated( state%flux) .eqv. .true.) allocate( state%flux(Nc), stat=alloc_stat(1));  state%flux(:) = 0.0_rfreal
          if(.not.associated(state%dflux) .eqv. .true.) allocate(state%dflux(Nc), stat=alloc_stat(2)); state%dflux(:) = 0.0_rfreal
          if(sum(alloc_stat(:)) /= 0)  call graceful_exit(region%myrank, 'Unable to allocate flux, dflux in FTMExchange.')

          if(input%TMSolve > 1) then

             if (.not.associated(state%VelGrad1st) .eqv. .true.) then
                allocate(state%VelGrad1st(Nc,ND*ND)); state%VelGrad1st(:,:) = 0.0_rfreal
             end if
             if ( input%shock /= 0 .OR. &
                  (input%LES >= 200 .AND. input%LES < 300)) then
                if (.not.associated(state%MagStrnRt) .eqv. .true.) then
                   allocate(state%MagStrnRt(Nc)); state%MagStrnRt(:) = 0.0_rfreal
                end if
                if (.not.associated(state%tvCor) .eqv. .true.) then
                   allocate(state%tvCor(Nc,input%nTv)); state%tvCor(:,:) = 0.0_rfreal
                end if
             end if ! input%shock

             Call NS_VelGradTensor(region, gridID, VISCOUS_ONLY)
          end if ! ... TMSolve > 1

          ! ... calculate heat flux
          if(input%TMSolve == 1 .or. input%TMSolve == 3) then

             if (.not.associated(state%TempGrad1st) .eqv. .true.) then
                allocate(state%TempGrad1st(Nc,ND)); state%TempGrad1st(:,:) = 0.0_rfreal
             end if

             Call NS_Temperature_Gradient(region, gridID, VISCOUS_ONLY, 0)

          end if
       end do
    end if

    ! ... loop through the interacting patches
    do npatch = 1, region%nInteractingPatches

       ! ... simplicity
       ipatch => region%ipatch(npatch)

       ! ... check to see whether this process ownes part of this interacting patch
       if(ipatch%ipatchColor /= npatch) cycle

       ! ... simplicity
       if(ipatch%patchID /= -1) then
          patchID = ipatch%patchID
          gridID  = ipatch%gridID
          patch   => region%patch(patchID)
          grid    => region%grid(gridID)
          state   => region%state(gridID)
       end if
       if(ipatch%TMpatchID /= -1) then
          TMpatchID = ipatch%TMpatchID
          TMgridID  = ipatch%TMgridID
          TMpatch   => region%TMpatch(TMpatchID)
          TMgrid    => region%TMgrid(TMgridID)
          TMstate   => region%TMstate(TMgridID)
       end if

       call MPI_BARRIER(ipatch%Comm,ierr)

       ! ... pass information from fluid to solid first
       if(exchangeDir == FLUID2SOLID) then
          ! ... variables to pass from the fluid to the structure
          if(input%TMSolve == 1) then
             nVar2Pass = 1
             Offset = 0
          end if
          if(input%TMSolve == 2) then
             nVar2Pass = 3 + (ND-2)*3
             Offset = nVar2Pass
          end if
          if(input%TMSolve == 3) then
             nVar2Pass = 4 + (ND-2)*3
             Offset = 3 + (ND-2)*3
          end if

          ! ... fluid, calculate data to transfer and store in ipatch$%data
          if(ipatch%patchID /= -1) then

             input   => grid%input
             Nc      =  grid%nCells

             N(:) = 1
             Np(:) = 1
             do j = 1, grid%ND
                N(j)  =  grid%ie(j) -  grid%is(j) + 1
                Np(j) = patch%ie(j) - patch%is(j) + 1
             end do

             ! ... number of patch points in this partition
             nPtsPatch = product(Np)

             ! ... allocate space for fluid data
             allocate(ipatch%data(nPtsPatch,nVar2Pass))

             ! ... structural coupling, data needed: sigma + Ip (ND x ND symmetric tensor)
             if(input%TMSolve > 1) then

                Call GetStressTensor(region, npatch)

             end if ! ... TMSolve > 1

             ! ... thermal coupling, data needed: normal heat flux (scalar)
             if(input%TMSolve == 1 .or. input%TMSolve == 3) then

                Call GetHtFlux(region,npatch, Offset)

             end if
          end if

          ! ... fluid loads on structure calculated, now communicate values

          ! ... if this process works on the solid problem, then it will have a fluid neighbor
          ! ... solid procs on this patch: loop through fluid neighbors and post receives
          if(ipatch%TMpatchID /= -1) then
             ! ... determine ammount of data to get from fluid     
             allocate(nPtsFluid(ipatch%nFluidNeighbor))
             do m = 1, ipatch%nFluidNeighbor
                ! ... number of points coming from fluid
                nPtsFluid(m) = 1
                do dir = 1, ND
                   nPtsFluid(m) = nPtsFluid(m)*(ipatch%FluidNeighExt(m,(dir-1)*2+2)-ipatch%FluidNeighExt(m,(dir-1)*2+1)+1)
                end do
             end do

             ! ... allocate receive buffer
             allocate(rcvbuf(maxval(nPtsFluid(:))*nVar2Pass,ipatch%nFluidNeighbor))
             allocate(reqrcv(ipatch%nFluidNeighbor))
             allocate(statrcv(MPI_STATUS_SIZE,ipatch%nFluidNeighbor))

             ! ... initialize
             rcvbuf(:,:) = 0.0_rfreal
             reqrcv(:) = 0
             statrcv(:,:) = 0

             ! ... post receives
             do m = 1, ipatch%nFluidNeighbor
                p = ipatch%FluidNeighbor(m)
                !print*,'Solid rank ',ipatch%rank,'receiving ', nPtsFluid(m)*nVar2Pass,' from fluid rank',p
                Call MPI_IRECV(rcvbuf(1,m),nPtsFluid(m)*nVar2Pass, MPI_DOUBLE_PRECISION, p, 0,ipatch%Comm, reqrcv(m), ierr)
             end do
          end if

          ! ... fluid procs on this patch: loop through solid neighbors and send data
          if(ipatch%patchID /= -1) then

             ! ... determine ammount of data to pass to solid
             allocate(nPtsSolid(ipatch%nSolidNeighbor))
             do m = 1, ipatch%nSolidNeighbor
                ! ... number of points coming from fluid
                nPtsSolid(m) = 1
                do dir = 1, ND
                   nPtsSolid(m) = nPtsSolid(m)*(ipatch%SolidNeighExt(m,(dir-1)*2+2)-ipatch%SolidNeighExt(m,(dir-1)*2+1)+1)
                end do
             end do

             ! ... allocate send buffer
             allocate(sndbuf(maxval(nPtsSolid(:))*nVar2Pass,ipatch%nSolidNeighbor))
             allocate(reqsnd(ipatch%nSolidNeighbor))
             allocate(statsnd(MPI_STATUS_SIZE,ipatch%nSolidNeighbor))

             sndbuf(:,:) = 0.0_rfreal
             reqsnd(:) = 0
             statsnd(:,:) = 0

             ! ... fill the send buffer
             do m = 1, ipatch%nSolidNeighbor
                do j = 1, grid%ND
                   Np1(j) =  ipatch%SolidNeighExt(m,(j-1)*2 + 2) - ipatch%SolidNeighExt(m,(j-1)*2 + 1) + 1
                end do
                Do k = ipatch%SolidNeighExt(m,2*2 + 1), ipatch%SolidNeighExt(m,2*2 + 2) 
                   Do j = ipatch%SolidNeighExt(m,1*2 + 1), ipatch%SolidNeighExt(m,1*2 + 2) 
                      Do i = ipatch%SolidNeighExt(m,0*2 + 1), ipatch%SolidNeighExt(m,0*2 + 2) 
                         lp = (k-1)*Np(1)*Np(2) + (j-1)*Np(1) + (i-1+1)
                         l0 = (k-ipatch%SolidNeighExt(m,2*2 + 1))*Np1(1)*Np1(2) + &
                              (j-ipatch%SolidNeighExt(m,1*2 + 1))*Np1(1) + (i-ipatch%SolidNeighExt(m,0*2 + 1)+1)
                         do l = 1, nVar2Pass
                            sndbuf((l0-1)*nVar2Pass + l, m) = ipatch%data(lp,l)
                         end do
                      end do
                   end do
                end Do
             end do
             deallocate(ipatch%data)

             ! ... send the data
             do m = 1, ipatch%nSolidNeighbor
                p = ipatch%SolidNeighbor(m)
                !print*,'fluid rank ',ipatch%rank,'sending ', nPtsSolid(m)*nVar2Pass,' to solid rank',p
                Call MPI_ISEND(sndbuf(1,m), nPtsSolid(m)*nVar2Pass, MPI_DOUBLE_PRECISION, p, 0,ipatch%Comm, reqsnd(m), ierr)
             end do

          end if

          ! ... solid procs: wait for all receives
          if(ipatch%TMpatchID /= -1) then
             Call MPI_WAITALL(ipatch%nFluidNeighbor,reqrcv,statrcv,ierr)
          end if

          ! ... fluid procs: wait for all sends, then deallocate what is not needed
          if(ipatch%patchID /= -1) then
             Call MPI_WAITALL(ipatch%nSolidNeighbor,reqsnd,statsnd,ierr)
             deallocate(nPtsSolid,sndbuf,reqsnd,statsnd)
          end if

          ! ... unpack the receive buffer
          ! ... only processes working on the solid patch 
          if(ipatch%TMpatchID /= -1) then

             N(:) = 1
             Np(:) = 1
             do j = 1, ND
                N(j)  =  TMgrid%ie(j) - TMgrid%is(j) + 1
                Np(j) = TMpatch%ie(j) - TMpatch%is(j) + 1
             end do

             nPtsPatch = product(Np)

             ! ... store received data in ipatch%data
             allocate(ipatch%data(nPtsPatch,nVar2Pass))
             ipatch%data(:,:) = 0.0_rfreal

             do m = 1, ipatch%nFluidNeighbor
                do j = 1, grid%ND
                   Np1(j) =  ipatch%FluidNeighExt(m,(j-1)*2 + 2) - ipatch%FluidNeighExt(m,(j-1)*2 + 1) + 1
                end do
                Do k = ipatch%FluidNeighExt(m,2*2 + 1), ipatch%FluidNeighExt(m,2*2 + 2) 
                   Do j = ipatch%FluidNeighExt(m,1*2 + 1), ipatch%FluidNeighExt(m,1*2 + 2) 
                      Do i = ipatch%FluidNeighExt(m,0*2 + 1), ipatch%FluidNeighExt(m,0*2 + 2) 
                         lp = (k-1)*Np(1)*Np(2) + (j-1)*Np(1) + (i-1+1)
                         l0 = (k-ipatch%FluidNeighExt(m,2*2 + 1))*Np1(1)*Np1(2) + &
                              (j-ipatch%FluidNeighExt(m,1*2 + 1))*Np1(1) + (i-ipatch%FluidNeighExt(m,0*2 + 1)+1)
                         do l = 1, nVar2Pass
                            ipatch%data(lp,l) = rcvbuf((l0-1)*nVar2Pass + l, m)
                         end do
                      end do
                   end do
                end Do
             end do

             ! ... now deallocate what is not needed
             deallocate(nPtsFluid,rcvbuf,reqrcv,statrcv)


             ! ... store the data received from the fluid
             ! ... fill stress tensor, if needed
             if(input%TMSolve > 1) then
                do lp = 1, nPtsPatch
                   do i = 1, Offset
                      TMpatch%FluidStressTns(lp,i) = ipatch%data(lp,i)
                      !write(*,'(A,I3,x,I3,x,D13.6)') 'Tensor',lp,i,TMpatch%FluidStressTns(lp,i)
                   end do
                end do
             end if
             ! ... fill heat flux, if needed
             if(input%TMSolve == 1 .or. input%TMSolve == 3) then
                do lp = 1, nPtsPatch
                   TMpatch%FluidHtFlux(lp) = ipatch%data(lp,Offset + 1)
                end do
             end if
             ! ... ipatch%data no longer needed
             deallocate(ipatch%data)
          end if

          !print*,'rank ',myrank,' patch rank ',ipatch%rank,' is done with fluid --> solid communication'
          
          call MPI_BARRIER(ipatch%Comm,ierr)
          
      elseif(exchangeDir == SOLID2FLUID) then
          
          ! ... now exchange from solid to fluid domain
          ! ... variables to pass from solid to fluid
          if(input%TMSolve == 1) then
             ! ... only temperature
             nVar2Pass = 1
             Offset = 0
          end if
          if(input%TMSolve == 2) then
             ! ... displacement
             nVar2Pass = ND
             ! ... dynamic: displacement, velocity, and acceleration
             if(input%StructuralTimeScheme == DYNAMIC_SOLN) nVar2Pass = ND*3
          end if
          if(input%TMSolve == 3) then
             if(input%StructuralTimeScheme == DYNAMIC_SOLN) then
                ! ... dynamic: displacement, velocity, acceleration, and temperature
                nVar2Pass = 1 + ND*3
                Offset =  ND*3
             else
                ! ... displacement and temperature
                nVar2Pass = 1 + ND
                Offset = ND
             end if
          end if

          ! ... only processes working on the structural patch
          if(ipatch%TMpatchID /= -1) then

             do m = 1, grid%ND
                N(m)  = TMgrid%ie(m) - TMgrid%is(m) + 1
                Np(m) = TMpatch%ie(m) - TMpatch%is(m) + 1
             end do

             nPtsPatch = product(Np)

             ! ... fill with patch data
             allocate(ipatch%data(nPtsPatch,nVar2Pass))
             ! ... initialize
             ipatch%data(:,:) = 0.0_rfreal

             Do k = TMpatch%is(3), TMpatch%ie(3)
                Do j = TMpatch%is(2), TMpatch%ie(2)
                   Do i = TMpatch%is(1), TMpatch%ie(1)

                      l0 = (k-TMgrid%is(3))*N(1)*N(2) + (j-TMgrid%is(2))*N(1) + i-TMgrid%is(1)+1

                      lp = (k-TMpatch%is(3))*Np(1)*Np(2) &
                           + (j-TMpatch%is(2))*Np(1) + (i-TMpatch%is(1)+1)

                      if(input%TMSolve > 1) then
                         do m = 1, ND
                            ipatch%data(lp,m) = TMstate%P3D(l0,m)
                         end do
                         if(input%StructuralTimeScheme == DYNAMIC_SOLN) then
                            do m = ND+1,3*ND
                               ipatch%data(lp,m) = TMstate%P3Ddot(l0,m-ND)
                            end do
                         end if
                      end if
                      if(input%TMSolve == 1 .or. input%TMSolve == 3) then
                         ipatch%data(lp,Offset+1) = TMstate%P3D(l0,TMstate%nVars)
                      end if
                   end Do
                end do
             end Do
          end if
          
          call MPI_BARRIER(ipatch%Comm,ierr)
          

          ! ... only processes working on the fluid patch
          ! ... determine ammount of data to get from solid
          if(ipatch%patchID /= -1) then
             allocate(nPtsSolid(ipatch%nSolidNeighbor))
             do m = 1, ipatch%nSolidNeighbor
                ! ... number of points coming from fluid
                nPtsSolid(m) = 1
                do dir = 1, ND
                   nPtsSolid(m) = nPtsSolid(m)*(ipatch%SolidNeighExt(m,(dir-1)*2+2)-ipatch%SolidNeighExt(m,(dir-1)*2+1)+1)
                end do
             end do

             ! ... allocate receive buffer
             allocate(rcvbuf(maxval(nPtsSolid(:))*nVar2Pass,ipatch%nSolidNeighbor))
             allocate(reqrcv(ipatch%nSolidNeighbor))
             allocate(statrcv(MPI_STATUS_SIZE,ipatch%nSolidNeighbor))

             ! ... initialize
             rcvbuf(:,:) = 0.0_rfreal
             reqrcv(:) = 0
             statrcv(:,:) = 0

             ! ... post receives
             do m = 1, ipatch%nSolidNeighbor
                p = ipatch%SolidNeighbor(m)
                !print*,'fluid rank ',ipatch%rank,'receiving ', nPtsSolid(m)*nVar2Pass,' from solid rank',p
                Call MPI_IRECV(rcvbuf(1,m), nPtsSolid(m)*nVar2Pass, MPI_DOUBLE_PRECISION, p, 0, ipatch%Comm, reqrcv(m), ierr)
             end do
          end if

          ! ... only processes working on the solid patch
          if(ipatch%TMpatchID /= -1) then
             ! ... determine ammount of data to pass to fluid
             allocate(nPtsFluid(ipatch%nFluidNeighbor))
             do m = 1, ipatch%nFluidNeighbor
                ! ... number of points coming from fluid
                nPtsFluid(m) = 1
                do dir = 1, ND
                   nPtsFluid(m) = nPtsFluid(m)*(ipatch%FluidNeighExt(m,(dir-1)*2+2)-ipatch%FluidNeighExt(m,(dir-1)*2+1)+1)
                end do
             end do

             ! ... allocate send buffer
             allocate(sndbuf(maxval(nPtsFluid(:))*nVar2Pass,ipatch%nFluidNeighbor))
             allocate(reqsnd(ipatch%nFluidNeighbor))
             allocate(statsnd(MPI_STATUS_SIZE,ipatch%nFluidNeighbor))

             sndbuf(:,:) = 0.0_rfreal
             reqsnd(:) = 0
             statsnd(:,:) = 0

             ! ... fill the send buffer
             do m = 1, ipatch%nFluidNeighbor
                do j = 1, grid%ND
                   Np1(j) =  ipatch%FluidNeighExt(m,(j-1)*2 + 2) - ipatch%FluidNeighExt(m,(j-1)*2 + 1) + 1
                end do
                Do k = ipatch%FluidNeighExt(m,2*2 + 1), ipatch%FluidNeighExt(m,2*2 + 2) 
                   Do j = ipatch%FluidNeighExt(m,1*2 + 1), ipatch%FluidNeighExt(m,1*2 + 2) 
                      Do i = ipatch%FluidNeighExt(m,0*2 + 1), ipatch%FluidNeighExt(m,0*2 + 2) 
                         lp = (k-1)*Np(1)*Np(2) + (j-1)*Np(1) + (i-1+1)
                         l0 = (k-ipatch%FluidNeighExt(m,2*2 + 1))*Np1(1)*Np1(2) + &
                              (j-ipatch%FluidNeighExt(m,1*2 + 1))*Np1(1) + (i-ipatch%FluidNeighExt(m,0*2 + 1)+1)
                         do l = 1, nVar2Pass
                            sndbuf((l0-1)*nVar2Pass + l, m) = ipatch%data(lp,l)
                            !                      if(region%myrank == 1) write(*,'(A,F13.6,I4,I4,I4,I4,I4,I,I)'),'rank 1,1',ipatch%data(lp,l),i,j,k,lp,l0,l,m
                         end do
                      end do
                   end do
                end Do
             end do
             ! ... ipatch%data not needed anymore
             deallocate(ipatch%data)

             ! ... send the data
             do m = 1, ipatch%nFluidNeighbor
                p = ipatch%FluidNeighbor(m)
                !print*,'Solid rank ',ipatch%rank,'sending ', nPtsFluid(m)*nVar2Pass,' to fluid rank',p
                Call MPI_ISEND(sndbuf(1,m), nPtsFluid(m)*nVar2Pass, MPI_DOUBLE_PRECISION, p, 0,ipatch%Comm, reqsnd(m), ierr)
             end do

          end if ! ... TMpatchID /= -1

          !print*,'rank ',region%myrank,' patch rank ',ipatch%rank,' is done with solid --> fluid communication'
          
          call MPI_BARRIER(ipatch%Comm,ierr)
          
          ! ... solid patch procs, wait for sends to complete, then deallocate
          if(ipatch%TMpatchID /= -1) then
             Call MPI_WAITALL(ipatch%nFluidNeighbor,reqsnd,statsnd,ierr)
             deallocate(reqsnd, statsnd, sndbuf, nPtsFluid)
          end if

          ! ... fluid patch procs, wait for receives
          if(ipatch%patchID /= -1) then
             Call MPI_WAITALL(ipatch%nSolidNeighbor,reqrcv,statrcv,ierr)
          end if

          ! ... fluid procs, unpack the receive buffer
          if(ipatch%patchID /= -1) then

             N(:) = 1
             Np(:) = 1
             do j = 1, grid%ND
                N(j)  =  grid%ie(j) - grid%is(j) + 1
                Np(j) = patch%ie(j) - patch%is(j) + 1
             end do

             nPtsPatch = product(Np)

             ! ... store received data in ipatch%data
             allocate(ipatch%data(nPtsPatch,nVar2Pass))

             ipatch%data(:,:) = 0.0_rfreal

             Do m = 1, ipatch%nSolidNeighbor
                do j = 1, grid%ND
                   Np1(j) =  ipatch%SolidNeighExt(m,(j-1)*2 + 2) - ipatch%SolidNeighExt(m,(j-1)*2 + 1) + 1
                end do
                Do k = ipatch%SolidNeighExt(m,2*2 + 1), ipatch%SolidNeighExt(m,2*2 + 2) 
                   Do j = ipatch%SolidNeighExt(m,1*2 + 1), ipatch%SolidNeighExt(m,1*2 + 2) 
                      Do i = ipatch%SolidNeighExt(m,0*2 + 1), ipatch%SolidNeighExt(m,0*2 + 2) 
                         lp = (k-1)*Np(1)*Np(2) + (j-1)*Np(1) + (i-1+1)
                         l0 = (k-ipatch%SolidNeighExt(m,2*2 + 1))*Np1(1)*Np1(2) + &
                              (j-ipatch%SolidNeighExt(m,1*2 + 1))*Np1(1) + (i-ipatch%SolidNeighExt(m,0*2 + 1)+1)
                         do l = 1, nVar2Pass
                            ipatch%data(lp,l) = rcvbuf((l0-1)*nVar2Pass + l, m)
                            !                     if(region%myrank == 1) write(*,'(A,F13.6,I4,I4,I4,I4,I4,I,I)'),'rank 1',ipatch%data(lp,l),i,j,k,lp,l0,l,m
                         end do
                      end do
                   end do
                end Do
             end do

             ! ... deallocate what is no longer needed
             deallocate(nPtsSolid,rcvbuf,reqrcv,statrcv)

             ! ... fill patch data with new values
             ! ... fill temperature data
             if((input%TMSolve == 1 .or. input%TMSolve == 3) .and. ThermalStep == TRUE) then
                do lp = 1, nPtsPatch
                   patch%CellTemp(lp) = Patch%CellTemp(lp) + patch%dCellTemp(lp)*dble(input%ThermalSolnFreq)
                   
                   !print*,patch%CellTemp(1)*300.0*.4,patch%dCellTemp(1)*300.0*.4
                   
                   ! ... non-dimensionalize before using in fluid solver
                   patch%dCellTemp(lp) = (ipatch%data(lp,Offset+1) * input%Cpref / input%SndSpdRef / input%SndSpdRef - patch%CellTemp(lp))!/(input%GamRef-1.0_rfreal)/input%TempRef
                   
                   !print*,patch%CellTemp(1)*300.0*.4,patch%dCellTemp(1)*300.0*.4
                   !patch%CellTemp(lp) = ipatch%data(lp,Offset+1) * input%Cpref / input%SndSpdRef / input%SndSpdRef
                end do
             end if

             ! ... fill surface motion data
             if(input%TMSolve > 1 .and. StructuralStep == TRUE) then
                ! ... dynamic data available in dynamic structural simulation
                if(input%StructuralTimeScheme == DYNAMIC_SOLN) then
                   Offset = ND
                   Do k = patch%is(3), patch%ie(3)
                      Do j = patch%is(2), patch%ie(2)
                         Do i = patch%is(1), patch%ie(1)

                            l0 = (k- grid%is(3))* N(1)* N(2) + (j- grid%is(2))* N(1) + (i- grid%is(1)+1)
                            lp = (k-patch%is(3))*Np(1)*Np(2) + (j-patch%is(2))*Np(1) + (i-patch%is(1)+1)

                            ! ... will be added to grid%XYZ_TAU over StructSolnFreq intervals
                            do m = 1, ND

                               ! ... if this is not a the initial timestep 
                               if (main_ts_loop_index /= input%nstepi) then
                                  patch%dXYZ_TAU(lp,m) = (ipatch%data(lp,m+Offset)/input%SndSpdRef - grid%XYZ_TAU(l0,m)) &
                                       / input%StructSolnFreq
                               elseif (main_ts_loop_index == input%nstepi) then
                                  ! ... if it is the intial timestep, need to move the grid to match the solid grid
                                  patch%dXYZ_TAU(lp,m) = (ipatch%data(lp,m+Offset)/input%SndSpdRef - grid%XYZ_TAU(l0,m))
                                  
                               end if
                               !                            print*,'vel',i,j,k,m,grid%XYZ_TAU(l0,m)
                            end do
                            
                            ! ... test mesh motion, prescribed velocity
                            !patch%dXYZ_TAU(lp,2) = (0.1*sin(dble(i-patch%is(1))*TWOPI/2.0_rfreal/(dble(Np(1)-1)))/input%SndSpdRef - grid%XYZ_TAU(l0,2))/input%StructSolnFreq

!                            print*,lp,patch%dXYZ(lp,2)
                            !! ... will be added to grid%XYZ_TAU2 over StructSolnFreq intervals
                            !do m = 1, ND
                            !   patch%dXYZ_TAU2(lp,m) = (ipatch%data(lp,m+2*Offset)/input%SndSpdRef/input%SndSpdRef*input%LengRef - grid%XYZ_TAU2(l0,m)) / input%StructSolnFreq
                            !   !                            print*,'accel',i,j,k,m,grid%XYZ_TAU2(l0,m)
                            !end do

                         end Do
                      end Do
                   end Do
                   ! ... calculate dynamic data in static structural simulation
                elseif(input%StructuralTimeScheme == STEADY_SOLN) then
                   Do k = patch%is(3), patch%ie(3)
                      Do j = patch%is(2), patch%ie(2)
                         Do i = patch%is(1), patch%ie(1)

                            l0 = (k- grid%is(3))* N(1)* N(2) + (j- grid%is(2))* N(1) + (i- grid%is(1)+1)
                            lp = (k-patch%is(3))*Np(1)*Np(2) + (j-patch%is(2))*Np(1) + (i-patch%is(1)+1)

                            do m = 1, ND
                               patch%dXYZ_TAU(lp,m) = (ipatch%data(lp,m)-grid%XYZ(l0,m))/input%dt/input%SndSpdRef
                            end do
                            !do m = 1, ND
                            !   patch%dXYZ_TAU2(lp,m) = 0.0_rfreal
                            !end do

                         end Do
                      end Do
                   end Do
                end if
                ! ... update grid coordinates
                Do k = patch%is(3), patch%ie(3)
                   Do j = patch%is(2), patch%ie(2)
                      Do i = patch%is(1), patch%ie(1)

                         l0 = (k- grid%is(3))* N(1)* N(2) + (j- grid%is(2))* N(1) + (i- grid%is(1)+1)
                         lp = (k-patch%is(3))*Np(1)*Np(2) + (j-patch%is(2))*Np(1) + (i-patch%is(1)+1)
                         !                      patch%dXYZ(lp,2) = 0.1_rfreal*sin((i-patch%is(1))*TWOPI/2.0_rfreal/(Np(1)-1))
                         do m = 1, ND
                            ! ... record change in grid boundary for use in transfinite interpolation
                            ! ... will be added to grid%XYZ over StructSolnFreq intervals
                            
                            if (main_ts_loop_index /= input%nstepi) then
                               ! ... if it is not the initial timestep, then increment input%StructSolnFreq times
                               patch%dXYZ(lp,m) = (ipatch%data(lp,m)/input%LengRef - grid%XYZ(l0,m))/input%StructSolnFreq
                            
                            elseif (main_ts_loop_index == input%nstepi) then
                               ! ... if it is the intial timestep, need to move the grid to match the solid grid
                               patch%dXYZ(lp,m) = (ipatch%data(lp,m)/input%LengRef - grid%XYZ(l0,m))

                            end if
                            !grid%XYZ(l0,m) = ipatch%data(lp,m)/input%LengRef
                            !                         print*,'xyz',i,j,k,m,grid%XYZ(l0,m)
                            !print*,ipatch%data(lp,m)
                         end do ! ... m
                         !if (j==1 .and. (i >= 4 .and. i<=6)) then
                         !   write(*,'(A,2(I2,x),3(D13.6,x))') 'dXYZ',i,j,(patch%dXYZ(lp,m), m = 1, ND)
                         !end if
                         ! ... test mesh motion: prescribe displacement
                         !patch%dXYZ(lp,2) = (0.1*sin(dble(i-patch%is(1))*TWOPI/2.0_rfreal/(dble(Np(1)-1))) - grid%XYZ(l0,2))/input%StructSolnFreq
                      end Do ! ... i
                   end Do ! ... j
                end Do ! ... k
             end if ! ... StructuralStep

             deallocate(ipatch%data)

          end if ! ... patchID /= -1
       end if ! ... exchangeDir
       
    end do ! ... npatch = 1, region%nInteractingPatches


    !        do j = 1, grid%ND
    !           N(j)  =  grid%ie(j) - grid%is(j) + 1
    !           Np(j) = patch%ie(j) - patch%is(j) + 1
    !        end do
    !        Do k = patch%is(3), patch%ie(3)
    !           Do j = patch%is(2), patch%ie(2)
    !              Do i = patch%is(1), patch%ie(1)

    !                 l0 = (k-grid%is(3))*N(1)*N(2) + (j-grid%is(2))*N(1) + i-grid%is(1)+1

    !                 lp = (k-patch%is(3))*Np(1)*Np(2) &
    !                      + (j-patch%is(2))*Np(1) + (i-patch%is(1)+1)

    !                 print*,region%myrank,i,j,k,sqrt(grid%XYZ(l0,1)**2+grid%XYZ(l0,3)**2),ipatch%data(lp,1)
    !              end Do
    !           end do
    !       end Do


  end Subroutine FTMExchange

  subroutine GetStressTensor(region, nipatch)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    Type(t_region), pointer :: region
    Integer :: nipatch

    ! ... local variables
    Type(t_fsi), pointer :: ipatch
    Type(t_patch), pointer :: patch
    Type(t_grid), pointer :: grid
    Type(t_mixt), pointer :: state
    Type(t_mixt_input), pointer :: input

    Real(rfreal) :: Strn1, Strn2, Diag, Jcbn, DimFac, AvgPres, AvgPresAux
    Integer :: N(MAX_ND),Np(MAX_ND),l0,i,j,k,lp,ii,jj,kk, nPtsPatchGlobal
    Integer :: ND, npatch

    ! ... simplicity
    ipatch => region%ipatch(nipatch)
    npatch = ipatch%patchID
    patch => region%patch(npatch)
    grid => region%grid(patch%gridID)
    input => grid%input
    state => region%state(patch%gridID)
    ND    =  grid%ND

    DimFac = input%DensRef*input%SndSpdRef*input%SndSpdRef

    ! ... grid size, patch size, and global number of points in patch
    nPtsPatchGlobal = 1
    do j = 1, grid%ND
       N(j)  =  grid%ie(j) -  grid%is(j) + 1
       Np(j) = patch%ie(j) - patch%is(j) + 1
       nPtsPatchGlobal = nPtsPatchGlobal*(patch%global_patch_extent(2*j)-patch%global_patch_extent(2*j-1) + 1)
    end do

    !print*,'There are ',nPtsPatchGlobal,' points in this patch'

    ! ... initialize average pressure
    ! ... needed to subtract from load on surface
    ! ... only want to pass fluctuations
    AvgPres = 0.0_rfreal
    AvgPresAux = 0.0_rfreal

    ! ... calculate the Cauchy stress tensor - pressure at each point on the patch
    Do k = patch%is(3), patch%ie(3)
       Do j = patch%is(2), patch%ie(2)
          Do i = patch%is(1), patch%ie(1)

             l0 = (k- grid%is(3))* N(1)* N(2) + (j- grid%is(2))* N(1) + (i- grid%is(1)+1)
             lp = (k-patch%is(3))*Np(1)*Np(2) + (j-patch%is(2))*Np(1) + (i-patch%is(1)+1)

             if(grid%metric_type /= CARTESIAN) Jcbn = grid%JAC(l0)
             Diag = 0.0_rfreal
             ! ... divergence of velocity
             do ii = 1, ND

                if(grid%metric_type /= CARTESIAN) then
                   do kk = 1, ND
                      Diag = Diag + state%VelGrad1st(l0,t2map(ii,kk))*grid%MT1(l0,t2map(kk,ii))
                   end do
                else
                   Diag = Diag + state%VelGrad1st(l0,t2map(i,i))
                end if
             end do
             Diag = -1.0_rfreal * state%dv(l0,1) + Diag * state%tv(l0,2)*state%REInv * Jcbn

             AvgPresAux = AvgPresAux + state%dv(l0,1)

             ! ... calculate strains and fill the stress tensor
             do jj = 1, ND
                do ii = 1, ND
                   Strn1 = 0.0_rfreal
                   Strn2 = 0.0_rfreal
                   if(grid%metric_type /= CARTESIAN) then
                      do kk = 1, ND
                         Strn1 = Strn1 + state%VelGrad1st(l0,t2map(ii,kk)) * grid%MT1(l0,t2map(kk,jj))
                         Strn2 = Strn2 + state%VelGrad1st(l0,t2map(jj,kk)) * grid%MT1(l0,t2map(kk,ii))
                      end do
                   else
                      Strn1 = state%VelGrad1st(l0,t2map(ii,jj))
                      Strn2 = state%VelGrad1st(l0,t2map(jj,ii))
                   end if
                   ipatch%data(lp,region%global%t2MapSym(ii,jj)) = state%tv(l0,1)*state%REInv * (Strn1 + Strn2) * Jcbn + region%global%delta(ii,jj) * Diag
                   ipatch%data(lp,region%global%t2MapSym(ii,jj)) = ipatch%data(lp,region%global%t2MapSym(ii,jj))*DimFac

                end do
             end do

          end Do ! ... i
       end Do  ! ... j
    end Do  ! ... k

    ! ... communicate average pressure
    Call MPI_AllReduce(AvgPresAux, AvgPres, 1, MPI_DOUBLE_PRECISION, MPI_SUM, ipatch%fluid_comm, ierr)

    ! ... take the average
    AvgPres = AvgPres/dble(nPtsPatchGlobal)

    ! ... remove the average pressure from the load
    Do k = patch%is(3), patch%ie(3)
       Do j = patch%is(2), patch%ie(2)
          Do i = patch%is(1), patch%ie(1)

             !write(*,'(A,3(I,x))')'At i,j,k ',i,j,k
             do jj = 1, ND

                lp = (k-patch%is(3))*Np(1)*Np(2) + (j-patch%is(2))*Np(1) + (i-patch%is(1)+1)
                !write(*,'(A,2(D13.6,x),A,D13.6)')'before->',(ipatch%data(lp,region%global%t2MapSym(jj,ii)),ii=1,ND),'AvgPres*DimFac ',AvgPres*DimFac
                ipatch%data(lp,region%global%t2MapSym(jj,jj)) = ipatch%data(lp,region%global%t2MapSym(jj,jj)) + AvgPres*DimFac
                !write(*,'(A,2(D13.6,x),A,D13.6)')'after->',(ipatch%data(lp,region%global%t2MapSym(jj,ii)),ii=1,ND),'AvgPres*DimFac ',AvgPres*DimFac
             end do
          end Do
       end Do
    end Do

   !  ! ... remove the average pressure from the load
   ! ! print*,'is,ie',patch%is,patch%ie
   !  Do k = patch%is(3), patch%ie(3)
   !     Do j = patch%is(2), patch%ie(2)
   !        Do i = patch%is(1), patch%ie(1)
   !           !write(*,'(A,3(I2,x))') 'At i,j,k = ',i,j,k
   !           l0 = (k- grid%is(3))* N(1)* N(2) + (j- grid%is(2))* N(1) + (i- grid%is(1)+1)
   !           !write(*,'(A,3(D13.6,x))') 'Grid(i,j,k) = ',(grid%XYZ(l0,jj), jj=1,ND)
   !           !write(*,'(A,3(D13.6,x))') 'Grid_TAU(i,j,k) = ',(grid%XYZ_TAU(l0,jj), jj=1,ND)
   !        end Do
   !     end Do
   !  end Do


  end subroutine GetStressTensor

  subroutine GetHTFlux(region,nipatch, Offset)

    USE ModGlobal
    USE ModDataStruct
    USE ModMetrics
    USE ModEOS
    USE ModDeriv
    USE ModIO
    USE ModMPI

    IMPLICIT NONE

    TYPE (t_region), POINTER :: region
    Integer :: nipatch, Offset

    ! ... local variables
    type(t_fsi), pointer :: ipatch
    type(t_patch), pointer :: patch
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    type(t_mixt_input), pointer :: input

    integer :: patchsize, npatch
    integer :: N(3),Np(3),l0,i,j,k,ii,i2,j2,k2,lp,m,jj,l1,dir
    real(rfreal) :: NormFlux
    real(rfreal) :: factor, dsgn, gamma_inf
    integer ::  normDir, sgn, ND

    real(rfreal) :: XI_X, XI_Y, XI_Z, denom1
    real(rfreal) :: XI_X_TILDE, XI_Y_TILDE, XI_Z_TILDE
    real(rfreal) :: SurfNorm(3), TempGradCart(3)

    ! ... dimensionalization factor
    ! ... In RocFlo-CM:
    ! ... q = -mu/(RePr)*dT/dx (all non-dimensional), so
    ! ... q = q*/[{RHORef}*({C_inf}*)^3] 
    ! ...   = q* / [(gamma_inf)(C_inf*)(PresRef*)]
    ! ... PresRef* on a case by case basis (Pa)
    ! ... C_inf* = gamma*R*T where R = 286.9 J/kg-K, free-stream values
    gamma_inf = region%input%GamRef

    factor = (gamma_inf*region%input%SndSpdRef*region%input%PresRef)

    ! .. number of dimensions
    ND = region%input%ND

    ! ... simplicity
    ipatch => region%ipatch(nipatch)
    npatch = ipatch%patchID
    patch => region%patch(npatch)
    grid => region%grid(patch%gridID)
    state => region%state(patch%gridID)

    patchsize=1

    normDir = abs(patch%normDir)
    sgn = normDir / patch%normDir
    dsgn = dble(sgn)

    do m = 1, grid%ND
       N(m)  = grid%ie(m) - grid%is(m) + 1
       Np(m) = patch%ie(m) - patch%is(m) + 1
       patchsize = patchsize*Np(m)
    end do

    Do k = patch%is(3), patch%ie(3)
       Do j = patch%is(2), patch%ie(2)
          Do i = patch%is(1), patch%ie(1)

             l0 = (k-grid%is(3))*N(1)*N(2) + (j-grid%is(2))*N(1) + i-grid%is(1)+1

             lp = (k-patch%is(3))*Np(1)*Np(2) &
                  + (j-patch%is(2))*Np(1) + (i-patch%is(1)+1)

             ! ... construct the normalized metrics
             XI_X = grid%MT1(l0,region%global%t2Map(normDir,1)); XI_Y = 0.0_rfreal; XI_Z = 0.0_rfreal
             if (ND >= 2) XI_Y = grid%MT1(l0,region%global%t2Map(normDir,2))
             if (ND == 3) XI_Z = grid%MT1(l0,region%global%t2Map(normDir,3))

             ! ... multiply by the Jacobian
             XI_X = XI_X * grid%JAC(l0)
             XI_Y = XI_Y * grid%JAC(l0)
             XI_Z = XI_Z * grid%JAC(l0)
             denom1 = 1.0_rfreal/sqrt(XI_X**2+XI_Y**2+XI_Z**2)
             XI_X_TILDE = XI_X * denom1
             XI_Y_TILDE = XI_Y * denom1
             XI_Z_TILDE = XI_Z * denom1
             ! ... the unit normal is (XI_X_TILDE, XI_Y_TILDE, XI_Z_TILDE)
             ! ... and points into the domain on + boundaries
             ! ... and points out of the domain on - boundaries

             ! ... now just need to take dot product of normal and temperature gradient
             SurfNorm(:) = 0.0_rfreal
             SurfNorm(1) = XI_X_TILDE
             if (ND >= 2) SurfNorm(2) = XI_Y_TILDE
             if (ND == 3) SurfNorm(3) = XI_Z_TILDE

             TempGradCart(:) = 0.0_rfreal
             ! ... need to convert from computational to cartesian coordinates
             do dir = 1, ND
                if(grid%metric_type /= CARTESIAN) then
                   do m = 1, ND
                      TempGradCart(dir) = TempGradCart(dir) + state%TempGrad1st(l0,m) * grid%MT1(l0,region%global%t2Map(m,dir))
                   end do
                   ! ... multiply by the Jacobian
                   TempGradCart(dir) = TempGradCart(dir) * grid%JAC(l0)
                else
                   TempGradCart(dir) = state%TempGrad1st(l0,dir)
                end if

             end do

             NormFlux = DOT_PRODUCT(TempGradCart(:),SurfNorm(:))
!             print*,'Tgrad',NormFlux,lp,SurfNorm(:),TempGradCart(:)

             !do dir = 1, l0
             !   print*,'cte',dir,state%TempGrad1st(l0,2)
             !end do

             ! ... Heat flux leaving through this patch (going into thermal domain) q_out = k * (dt/dn) 
             ! ... k*dT/dn (dimensional) = dT/dn[nd] *  1/(RePr) * k[nd] * (dimensionalization factor)
             NormFlux = Normflux/(region%input%Re * region%input%Pr) * state%tv(l0,3)

             ! ... update the heat flux
             ipatch%data(lp,Offset+1) = NormFlux*factor
!             print*,'fluid out',lp,NormFlux*factor

          end do
       end do
    end do

  end subroutine GetHTFlux

end Module ModFTMCoupling
