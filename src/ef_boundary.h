#ifndef EF_BOUNDARY_H
#define EF_BOUNDARY_H

#include "ef_bc.h"


/**
 * Contains all boundary conditions for a grid.
 */
typedef struct ef_boundary {
	ef_bc **bc;
	int num_bc;       /**< number of boundary conditions */
	ef_level *level; /**< data needed on each level */
	ef_dmap *slv_dmap;/**< efield solver's datamapping object */
	ef_state *state;  /**< state data for efield solver */
	int axisymmetric; /**< flag to indicate axisymmetric operator */
	ef_fd *fd;        /**< finite difference coefficients */
} ef_boundary;


/**
 * Creates the boundary object for a grid.
 *
 * @param ef_bnd location to put newly created object
 * @param levels array of data needed on each level
 * @param nlevels number of levels in multilevel solve
 * @param dmap efield solver's data mapping object
 * @param state state data for efield solver
 * @param fd finite difference coefficients
 */
PetscErrorCode ef_boundary_create(ef_boundary **ef_bnd, ef_level *level,
                                  ef_dmap *dmap, ef_state *state, ef_fd *fd);


/**
 * Adds boundary conditions to matrix.
 *
 * @param bnd boundary object
 * @param A matrix to add boundary conditions
 * @param da data structure for structured grid
 */
PetscErrorCode ef_boundary_apply(ef_boundary *bnd, Mat A, DM da);


/**
 * Adds a boundary condition to a grid's boundary.
 *
 * @param bnd boundary object
 * @param btype type of boundary condition (Dirichlet, Neumann, ...)
 * @param norm_dir Direction of outward normal
 * @param is global index of this processors starting grid point in each dimension
 * @param ie global index of this processors ending grid point in each dimension
 * @param Dirichlet values of Dirichlet boundary (ignored if btype is not Dirichlet)
 */
PetscErrorCode ef_boundary_add(ef_boundary *bnd, ef_bctype btype, int norm_dir,
                               int is[], int ie[], double *dirichlet);


/**
 * Adds boundary conditions to the right hand side.
 *
 * Depending on the boundary condition and choice of implementation,
 * the matrix and right hand side may need to be modified.
 * @param bnd boundary object
 * @param da data structure for structured grid
 * @param rhs right hand side vector
 */
PetscErrorCode ef_boundary_apply_rhs(ef_boundary *bnd, DM da, Vec rhs);


/**
 * Destructor for boundary.
 */
PetscErrorCode ef_boundary_destroy(ef_boundary *bnd);


#endif
